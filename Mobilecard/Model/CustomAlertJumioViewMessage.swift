
import UIKit


protocol CustomAlertJumioDelegate{
    func didAcceptButtonJumioComplete()
}

class CustomAlertJumioViewMessage: UIViewController {
    

    
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var cancelButton: UIButton!

    
    var mensaje = ""

    
    var delegate : CustomAlertJumioDelegate?
    
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.messageTextView.text = self.mensaje
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
        //cancelButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
        //cancelButton.addBorder(side: .Right, color: alertViewGrayColor, width: 1)
        //okButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
    }
    
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    override func viewDidLayoutSubviews() {
        self.messageTextView.setContentOffset(.zero, animated: false)
    }
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        
        
            self.dismiss(animated: false, completion: nil)
            self.delegate?.didAcceptButtonJumioComplete()
       
    }
    
    
    

}




//extension para mandar mensaje mas facilmente con view personalizada
extension UIViewController {
    func alertJumioComplete(message:String, cancel:String) {
        
        
        
        
        let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
        let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertJumioID") as! CustomAlertJumioViewMessage
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.mensaje = message
       
        self.present(customAlert, animated: true, completion: nil)
        
        
        
    }
    
    
   
}



