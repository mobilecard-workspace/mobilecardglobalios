

import Foundation
import UIKit




//objeto que almacena la informacion
class CardPressent: NSObject{
    
    
    
    //cuerpo del objeto
    var cantidad : String?
    var comision : String?
    var concepto : String?
    var propina : Double? = 0.0
    var nombreTarjetaHabitante : String?
    var apellidosTarjetaHabitante : String?
    var numeroTarjeta : String?
    var mes : String?
    var anio : String?
    var cvv : String?
    var tipoTarjeta : String?
    var franquicia : String?
    var imagenTarjeta : UIImage?
    var firma : UIImage?
    var firmaBase64 : String!
    var newTransaction :Int = 0
    var lat : String?
    var long : String?
    var dateTime : CLong?
    var idTransaccion : Int?
    var authNumber : String?
    var mail : String = ""
    var phone : String = ""
    
    
    func cleanData(){
        self.mail = ""
        self.phone = ""
        self.cantidad = nil
        self.concepto = nil
        self.propina = 0.0
        self.nombreTarjetaHabitante = nil
        self.apellidosTarjetaHabitante = nil
        self.numeroTarjeta = nil
        self.mes = nil
        self.anio = nil
        self.cvv = nil
        self.tipoTarjeta = nil
        self.firma = nil
        self.imagenTarjeta = nil
        self.franquicia = nil
        self.comision = nil
        self.firmaBase64 = nil
        self.lat = nil
        self.long = nil
        self.dateTime = nil
        self.idTransaccion = nil
        self.authNumber = nil
        
    }
    

 
}





