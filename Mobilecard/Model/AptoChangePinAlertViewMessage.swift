
import UIKit


protocol AptoPinDelegate {
    
    func didChangePin(pin:String)
    
}

class AptoChangePinAlertViewMessage: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
  
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var cancelButton: UIButton!

    @IBOutlet weak var pinTextField: UITextField!
    
    @IBOutlet weak var confirmTextField: UITextField!
    
     var delegate : AptoPinDelegate?
    
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
        //cancelButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
        //cancelButton.addBorder(side: .Right, color: alertViewGrayColor, width: 1)
        //okButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
    }
    
    
    @IBAction func closeAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        
        if self.pinTextField.text! == ""{
                  self.alert(title: "¡Aviso!", message: "Debes agregar un pin para continuar.", cancel: "OK")
            return;
        }
        
        if self.pinTextField.text!.count < 4 || self.pinTextField.text!.count > 4{
            
            self.alert(title: "¡Aviso!", message: "El pin debe contener 4 digitos para continuar.", cancel: "OK")
            return;
        }
    
        
        if self.pinTextField.text! != self.confirmTextField.text! {
                  self.alert(title: "¡Aviso!", message: "Los dos campos deben tener el mismo pin para continuar.", cancel: "OK")
            return;
        }
        
        print("listo delegamos la acción")
        self.delegate?.didChangePin(pin: self.pinTextField.text!)
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    

}







