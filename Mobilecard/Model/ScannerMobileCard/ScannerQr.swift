

import UIKit
import AVFoundation


@objc protocol ScannerQrDelegate{
    
    
    func backActionQr()
    func successQr(qr:String)
    
    
}

class ScannerQr: UIViewController{
    
    var isFromServices : BooleanLiteralType = false

    @objc var delegate : ScannerQrDelegate?
    var aux = 0
   
    var qrExist = 0
    
    //por si regresa a la vista activar nuevamente
    var returnValueAux = 0
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var instructionsLabel: UILabel!
    
    //referencia de la imagen de fondo que muestra el cuadro del qr por defecto esta oculta
    @IBOutlet weak var scaneerQRBackgroundImageView: UIImageView!
    //variable que contiene el estado del flash
    var flashActive = false
   var captureDevice: AVCaptureDevice!
    
     var captureSession = AVCaptureSession()
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    //los codigos soportados
    
    
    private var supportedCodeTypes = [AVMetadataObject.ObjectType.qr]
    
    
        
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        if self.isFromServices == true{
            self.instructionsLabel.text = "Alinea el código con este marco."
            self.flashButton.setTitle("      Ilumina el código", for: .normal)
            self.supportedCodeTypes = [AVMetadataObject.ObjectType.code128,AVMetadataObject.ObjectType.code39,AVMetadataObject.ObjectType.code93,AVMetadataObject.ObjectType.code39Mod43]
            
            
            
        }
       
        //iphone x cambia la imagen del cuadro de qr
        if  UIScreen.main.bounds.height == 812 && UIScreen.main.bounds.width == 375 {
            
            self.scaneerQRBackgroundImageView.image = #imageLiteral(resourceName: "scaneerQRIphoneX")
            
        }
        
       
      
        
        //si es arriba de iphone 6 utiliza la dualcamera si no la puede recuperar usa la que se usaba anteriormente
        var deviceDiscoverySession: AVCaptureDevice.DiscoverySession
        
        if AVCaptureDevice.default(.builtInDualCamera, for: AVMediaType.video, position: .back) != nil {
            
            deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .back)
             captureDevice = deviceDiscoverySession.devices.first
            
        } else {
            
            deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back)
            captureDevice = deviceDiscoverySession.devices.first
            
        }
        
        
        //si no se puede acceder a la camara (no aceptaron el permiso)
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            alert(title: "¡Aviso!", message: "No se pudo acceder a la cámara del dispositivo.", cancel: "OK")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
      //  se ponen las vistas arriba de la grabacion de video en este caso la barra de navegacion y el boton de ingresar manualmente una bicicleta
        self.scaneerQRBackgroundImageView.isHidden = false
        view.bringSubviewToFront(self.scaneerQRBackgroundImageView)
        view.bringSubviewToFront(self.flashButton)
        view.bringSubviewToFront(self.topView)
        view.bringSubviewToFront(self.instructionsLabel)
        
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1)
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
        
        
    }
    
    

    
  
    
//metodo que se ejecuta al presionar el boton de regresar
    @IBAction func backButtonAction(_ sender: UIButton) {
      
        self.delegate?.backActionQr()
        self.dismiss(animated: true, completion: nil)
        
    }
    

    
    

   
    
    
    
    @IBAction func flashAction(_ sender: UIButton) {
        
        if flashActive == false{
        flashOn(device: captureDevice)
        }else if flashActive == true{
            flashOff(device: captureDevice)
        }
        
        }
    
    
    //metodo que enciende el flash
    private func flashOn(device:AVCaptureDevice)
    {
        do{
            if (device.hasTorch)
            {
                try device.lockForConfiguration()
                device.torchMode = .on
                self.flashActive = true
                self.flashButton.setTitleColor(#colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1), for: .normal)
                self.flashButton.setImage(#imageLiteral(resourceName: "linternaAmarilla"), for: .normal)
              //  device.flashMode = .on
                device.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
        }
    }
    
    //metodo que apaga el flash
    private func flashOff(device:AVCaptureDevice)
    {
        do{
            if (device.hasTorch){
                try device.lockForConfiguration()
                device.torchMode = .off
                self.flashActive = false
                self.flashButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                self.flashButton.setImage(#imageLiteral(resourceName: "linterna"), for: .normal)
               // device.flashMode = .off
                
                device.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
        }
    }

    
    
    
   
}



extension ScannerQr: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
           print("no se detecto codigo ")
            // self.delegate?.backActionQr()
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                //este es el codigo qr que se detecto
                if aux == 0{
                    
                    //aqui detecta el codigo
                // self.codeBike = metadataObj.stringValue!
                self.aux = 1
     
              //  print("se detecto codigo qr es \(metadataObj.stringValue)")
                    
                    self.delegate?.successQr(qr: metadataObj.stringValue!)
                    self.dismiss(animated: true, completion: nil)
              //  let params : NSMutableDictionary = [
             //       "token":DataUser.TOKEN!
             //   ]
               // WebBridge.send(type: .POST, url: "api/ride/status", params: params as NSDictionary, message: "Verificando", delegate: self)
                
                }
                //aqui validaremos si es valido este codigo qr o no
                
               
               // let vc = ConfirmPayViewController()
               // vc.codeBike = metadataObj.stringValue!
               // self.navigationController?.pushViewController(vc, animated: true)
                //detiene la captura de video de qr
             //   captureSession.stopRunning()
               //avisa que salio de esta vista por qr para reiniciar todo al regresar con el boton de back
                //self.returnValueAux = 1
            }
        }
    }
    
    

    
    /*
     self.returnValueAux = 1
      captureSession.stopRunning()
     
     
     */
    
    
    
    
    

    

    
}



    
    


