

import UIKit
import JumioCore
import Netverify

@objcMembers class DataUser: NSObject{
    
    

    
    static func encryptString(string : String) -> String{
        let plainData = string.data(using: .utf8)
        
        let stringSha = NSData.sha512(plainData)
        let base64String = stringSha?.base64EncodedString(options: [])
        
        return base64String!
    }
    
    
    /*
    +(NSString*) decryptHard:(NSString *) string{
    
    NSLog(@"Este valor llego");
    NSLog(@"%@",string);
    
    //
    decriptManager = [[pootEngine alloc] init];
    NSString *stringDecrypt = (NSString*)[decriptManager decryptedStringOfString:string withSensitive:NO];
    
    NSLog(@"Este valor se va");
    NSLog(@"%@",stringDecrypt);
    
    return stringDecrypt;
    }*/

    
    private static func data(property:String, newValue:NSDictionary!) {
        
       
        let defaults = UserDefaults.standard
        if (newValue == nil) {
            defaults.set(nil, forKey: property);
            defaults.synchronize();
            return;
        }
        
        let dictionary = NSMutableDictionary(dictionary: newValue);
        
        for key in dictionary.allKeys {
            if (dictionary[key as! String] is NSNull) {
                dictionary.setObject("", forKey: key as! String as NSCopying);
            }
        }
        
        defaults.set(dictionary, forKey: property);
        defaults.synchronize();
        
    }
    
    
   @objc static func clean() {
        DataUser.NAME = nil
        DataUser.LASTNAME = nil
    DataUser.LASTNAMEMOTHER = nil
        DataUser.TOKEN      = nil
        DataUser.DATA       = nil
        DataUser.COUNTRY = nil
        DataUser.EMAIL = nil
        DataUser.PASS = nil
      //  DataUser.IMEI = nil
        DataUser.APPID = nil
        DataUser.USERTYPE = nil
        DataUser.OS = nil
      //  DataUser.COUNTRYID = nil
        DataUser.MANOFACTURER = nil
        DataUser.STATELOGIN = nil
        DataUser.USERIDD = nil
        DataUser.COMISION = nil
        DataUser.QREMPRESA = nil
        DataUser.COUNTRYIDD = nil
    DataUser.TIPO_PERSONA = nil
    DataUser.JUMIOSTATUS = nil
    DataUser.SCANREFERENCE = nil
    DataUser.USERINFO = nil
    
        
        
        let dataObjectiv1 = UserDefaults.standard
        dataObjectiv1.set(nil, forKey: "userDetails");
        dataObjectiv1.synchronize();
        
        let dataObjectiv2 = UserDefaults.standard
        dataObjectiv2.set(nil, forKey: "usrPwd");
        dataObjectiv2.synchronize();
   
    
    
    }
    
    
    @objc  static func setBackgroundColor(color:UIColor,vista:UIView) -> UIView{
        
        vista.backgroundColor = #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1)
        
return vista
    }
    
    
    @objc  static var STATELOGIN: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.statelogin");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.statelogin");
            defaults.synchronize();
        }
    }
    
    @objc  static var TOKENFIREBASE: String! {
           get {
               let defaults:UserDefaults = UserDefaults.standard;
               let info:String? = defaults.string(forKey: "tokenFirestore");
               return info;
           }
           set {
               let defaults = UserDefaults.standard
               defaults.set(newValue, forKey: "tokenFirestore");
               defaults.synchronize();
           }
       }
    
    @objc  static var SERIALQRWELCOMEPACK: String! {
           get {
               let defaults:UserDefaults = UserDefaults.standard;
               let info:String? = defaults.string(forKey: "serialqrwelcomepack");
               return info;
           }
           set {
               let defaults = UserDefaults.standard
               defaults.set(newValue, forKey: "serialqrwelcomepack");
               defaults.synchronize();
           }
       }
    
    
    @objc  static var JUMIOSTATUS: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.jumiostatus");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.jumiostatus");
            defaults.synchronize();
        }
    }
    
    @objc  static var SCANREFERENCE: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.scanreference");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.scanreference");
            defaults.synchronize();
        }
    }
    
    @objc  static var APTOCONNECT: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptoconnect");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptoconnect");
            defaults.synchronize();
        }
    }
    
    @objc  static var COMISION: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.comision");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.comision");
            defaults.synchronize();
        }
    }
    
    @objc  static var QREMPRESA: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.qrempresa");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.qrempresa");
            defaults.synchronize();
        }
    }
    
    
    
    @objc  static var WHITELISTJUMIO: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.whitelistjumio");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.whitelistjumio");
            defaults.synchronize();
        }
    }
    
    @objc  static var IDTARJETACO: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.idtarjetaco");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.idtarjetaco");
            defaults.synchronize();
        }
    }
    
    
    
  @objc  static var EMAIL: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.email");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.email");
            defaults.synchronize();
        }
    }
    
    
    @objc  static var STATUSMC: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.statusmc");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.statusmc");
            defaults.synchronize();
        }
    }
    
    
    @objc  static var MESSAGEMC: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.messagemc");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.messagemc");
            defaults.synchronize();
        }
    }
    
    
    @objc  static var REGISTERTUTOENABLE: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.registertutoenable");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.registertutoenable");
            defaults.synchronize();
        }
    }
    
    @objc  static var TIPO_PERSONA: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.tipopersona");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.tipopersona");
            defaults.synchronize();
        }
    }
    
   @objc static var PASS: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.pass");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.pass");
            defaults.synchronize();
        }
    }
    
  @objc  static var IMEI: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.imei");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.imei");
            defaults.synchronize();
        }
    }
    
   @objc static var USERTYPE: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.usertype");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.usertype");
            defaults.synchronize();
        }
    }
    
  @objc  static var OS: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.os");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.os");
            defaults.synchronize();
        }
    }
    
   @objc static var COUNTRYID: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.countryid");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.countryid");
            defaults.synchronize();
        }
    }
    
    @objc static var COUNTRYIDD: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.countryidd");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.countryidd");
            defaults.synchronize();
        }
    }
    
    @objc static var USERIDD: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.userid");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.userid");
            defaults.synchronize();
        }
    }
    
    //variables para reanudar el registro en caso de ser necesario guarda el numero de telefono y el email
    
    @objc static var PHONE_TEMP_REGISTER: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.phonetempregister");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.phonetempregister");
            defaults.synchronize();
        }
    }
    
    
    @objc static var MAIL_TEMP_REGISTER: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.mailtempregister");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.mailtempregister");
            defaults.synchronize();
        }
    }
    
    
    
    //estos tres son para el registro de apto estados unidos no estan en el clean porque se borran al finalizar el proceso
    
    @objc static var VERIFICATIONID_SMS: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.verificationidsms");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.verificationidsms");
            defaults.synchronize();
        }
    }
    
    
    @objc static var VERIFICATIONID_MAIL: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.verificationidmail");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.verificationidmail");
            defaults.synchronize();
        }
    }
    
    @objc static var APTOTOKEN: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptotoken");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptotoken");
            defaults.synchronize();
        }
    }
    
    @objc static var APTOACCOUNTID: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptoaccountid");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptoaccountid");
            defaults.synchronize();
        }
    }
    
    @objc static var APTONAMEPERSON: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptonameperson");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptonameperson");
            defaults.synchronize();
        }
    }
    
    @objc static var IDUSERAPTO: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.iduserapto");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.iduserapto");
            defaults.synchronize();
        }
    }
    
    
    
    @objc static var PHONE_APTO: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.phoneapto");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.phoneapto");
            defaults.synchronize();
        }
    }
    
    @objc  static var APTOTESTSESION: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptotestsesion");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptotestsesion");
            defaults.synchronize();
        }
    }
    
    @objc  static var APTOADRESS: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptoadress");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptoadress");
            defaults.synchronize();
        }
    }
    
    @objc  static var APTOCARD: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptocard");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptocard");
            defaults.synchronize();
        }
    }
    
    @objc  static var APTOCODIGO: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptocodigo");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptocodigo");
            defaults.synchronize();
        }
    }
    
    @objc  static var APTOVIGENCIA: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.aptovigencia");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.aptovigencia");
            defaults.synchronize();
        }
    }
    
    
    
   @objc static var MANOFACTURER: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.manofacturer");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.manofacturer");
            defaults.synchronize();
        }
    }
    
    
    
   @objc static var APPID: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.appid");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.appid");
            defaults.synchronize();
        }
    }
    
    static var TOKEN: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.token");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.token");
            defaults.synchronize();
        }
    }
    
    
    @objc static var COUNTRY: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.country");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.country");
            defaults.synchronize();
        }
    }
    
    static var NAME: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.name");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.name");
            defaults.synchronize();
        }
    }
    
    
    
    static var LASTNAME: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.lastname");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.lastname");
            defaults.synchronize();
        }
    }
    
    static var LASTNAMEMOTHER: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.lastnamemother");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.lastnamemother");
            defaults.synchronize();
        }
    }

 

   @objc static var DATA: NSDictionary! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info = defaults.dictionary(forKey: "DataUser.data");
            return (info! as NSDictionary);
        }
        set {
            DataUser.data(property: "DataUser.data", newValue: newValue);
        }
    }
    
    @objc static var USERINFO: NSDictionary! {
         get {
             let defaults:UserDefaults = UserDefaults.standard;
             let info = defaults.dictionary(forKey: "userDetails");
             return (info! as NSDictionary);
         }
         set {
             DataUser.data(property: "userDetails", newValue: newValue);
         }
     }
    
    
    
    /* ------------------ */
    /* GETTERS // SETTERS */
    
    static func exist(prop:String) -> Bool {
        return DataUser.DATA[prop] != nil;
    }
    
    static func dictionary(prop:String) -> Dictionary<String, AnyObject> {
        if (DataUser.DATA[prop] == nil || DataUser.DATA[prop] is NSNull) {
            return Dictionary();
        }
        if DataUser.DATA[prop] is Dictionary<String, AnyObject> {
            return DataUser.DATA[prop] as! Dictionary<String, AnyObject>;
        }
        return Dictionary();
    }
    
    static func array(prop:String)->Array<AnyObject> {
        if (DataUser.DATA[prop] == nil || DataUser.DATA[prop] is NSNull) {
            return [];
        }
        if DataUser.DATA[prop] is Array<AnyObject> {
            return DataUser.DATA[prop] as! Array<AnyObject>;
        }
        return [];
    }
    
    static func string(prop:String)->String {
        
        var obj:Dictionary<String, AnyObject>!;
        var ppp:String!;
    
        if (prop.contains(".")) {
            let cmp = prop.components(separatedBy: ".")
            obj = dictionary(prop: cmp[0])
            ppp = cmp[1];
        } else {
            obj = (DataUser.DATA as! Dictionary<String, AnyObject>);
            ppp = prop;
        }
        
        if (obj[ppp] == nil || obj[ppp] is NSNull) {
            return "";
        }
        if obj[prop] is Double {
            return String(obj[ppp] as! Double);
        }
        return obj[ppp] as! String;
    }
    
    static func bool(prop:String)->Bool {
        
        var obj:Dictionary<String, AnyObject>!;
        var ppp:String!;
        
        if (prop.contains(".")) {
            let cmp = prop.components(separatedBy: ".")
            obj = dictionary(prop: cmp[0])
            ppp = cmp[1];
        } else {
            obj = (DataUser.DATA as! Dictionary<String, AnyObject>);
            ppp = prop;
        }
        
        if (obj[ppp] == nil || obj[ppp] is NSNull) {
            return false;
        }
        if obj[ppp] is String {
            return (obj[ppp] as! String) == "true" || (obj[ppp] as! String) == "1";
        }
        if obj[ppp] is Int {
            return obj[ppp] as! Int == 1;
        }
        if obj[prop] is Double {
            return obj[ppp] as! Double == 1;
        }
        return obj[ppp] as! Bool;
        
    }
    
    static func double(prop:String)->Double {
        
        var obj:Dictionary<String, AnyObject>!;
        var ppp:String!;
        
        if (prop.contains(".")) {
            let cmp = prop.components(separatedBy: ".")
            obj = dictionary(prop: cmp[0])
            ppp = cmp[1];
        } else {
            obj = (DataUser.DATA as! Dictionary<String, AnyObject>);
            ppp = prop;
        }
        
        
        if (obj[ppp] == nil || obj[ppp] is NSNull) {
            return 0;
        }
        if obj[ppp] is String {
            return Double(obj[ppp] as! String)!;
        }
        if obj[ppp] is Int {
            return Double(obj[ppp] as! Int);
        }
        
        return obj[ppp] as! Double;
    }
    
    static func int(prop:String)->Int {
        
        var obj:Dictionary<String, AnyObject>!;
        var ppp:String!;
        
        if (prop.contains(".")) {
            let cmp = prop.components(separatedBy: ".")
            obj = dictionary(prop: cmp[0])
            ppp = cmp[1];
        } else {
            obj = (DataUser.DATA as! Dictionary<String, AnyObject>);
            ppp = prop;
        }
        
        
        
        
        if (obj[ppp] == nil || obj[ppp] is NSNull) {
            return 0;
        }
        if obj[ppp] is String {
            return Int(obj[ppp] as! String)!;
        }
        if obj[ppp] is Double {
            return Int(obj[ppp] as! Double);
        }
        
        return obj[ppp] as! Int;
    }
    
    

    
    


    
    
}
