

import Foundation
import UIKit






//objeto que almacena la informacion de una transaccion
public class TransactionApto{
    
    
    
    //cuerpo del objeto
    var type :String! = "transaccion"
    var month :String! = "transaccion"
    var nameTransaction : String!
    var dateTransaction : String!
    var amountTransaction : String!
    var statusTransaction : String!
    var categoryTransaction : String!
    var deviceTypeTransaction : String!
    var typeTransaction : String!
    var idTransaction : String!
    var toYouAccountAmount : String!
    var toYouAccountId : String!
    var toYouAccountTypeChange : String!
    var fromMobileCardAmount : String!
    var fromMobileCardId : String!
    var fromMobileCardTypeChange : String!
    var lat : String!
    var long : String!

    
    

  //constructor transaccion
    init(nameTransaction:String,dateTransaction:String,amountTransaction:String,statusTransaction:String,categoryTransaction:String,deviceTypeTransaction:String,typeTransaction:String,idTransaction:String,toYouAccountAmount:String,toYouAccountId:String,toYouAccountTypeChange:String,fromMobileCardAmount:String,fromMobileCardId:String,fromMobileCardTypeChange:String,lat:String,long:String){
        
        
        
        self.nameTransaction = nameTransaction
        self.dateTransaction = dateTransaction
        self.amountTransaction = amountTransaction
        self.statusTransaction = statusTransaction
        self.categoryTransaction = categoryTransaction
        self.deviceTypeTransaction = deviceTypeTransaction
        self.typeTransaction = typeTransaction
        self.idTransaction = idTransaction
        self.toYouAccountAmount = toYouAccountAmount
        self.toYouAccountId = toYouAccountId
        self.toYouAccountTypeChange = toYouAccountTypeChange
        self.fromMobileCardAmount = fromMobileCardAmount
        self.fromMobileCardId = fromMobileCardId
        self.fromMobileCardTypeChange = fromMobileCardTypeChange
        self.lat = lat
        self.long = long
        
    
        
}
    
    
    //contructor cuando es fecha
    init(type:String,month:String){
        self.type = type
        self.month = month
        
    }
}





