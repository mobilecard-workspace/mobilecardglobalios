import UIKit
import Alamofire




@objc public class SendTokenFirestore: UIViewController ,ServicesDelegate,instruccionsSuccessDelegate{


  
   static  var services : Services!

    //variable que guarda la imagen de perfil del usuario
   static var imageProfile : UIImage? = nil

    static var isFrom = 0
    static var viewGlobal : RegisterNew_Step2!
     static var viewGlobalTwo : Register_Commerce_Step10!
    static var viewGlobalThree : MainMenu_Ctrl!
    static var viewGlobalFour : MainMenu_Commerce!
    static var picker:UIImagePickerController!
    static var typeWhiteList : String!
   @objc static func send(idUser:String,typeUser:String,idPais:String){
        

        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
        
    
    let defaults:UserDefaults = UserDefaults.standard;
    let info:String? = defaults.string(forKey: "tokenFirestore");
    
       
    
        if let token  = info {
            
          SendTokenFirestore.services = Services()
        
            let params : [String : Any] = ["id_usuario":idUser,"tipoUsuario":typeUser,"idioma":NSLocalizedString("lang", comment: ""),"idPais":idPais,"idApp":1,"token":token]
        
       
        SendTokenFirestore.services.send(type: .POST, url: "PushNotifications/saveToken", params: params, header: headers, message: "",animate: false)
            
        }
        

    }
    
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("esto responde \(response)")
        
        if DataUser.USERIDD != nil{
        
        if endpoint == "Tokenizer/1/\(DataUser.COUNTRYID!)/es/whiteList?idUsuario=\(DataUser.USERIDD!)"{
            
            let code = response["code"] as! Int
          
            
            if SendTokenFirestore.typeWhiteList == "scan"{
            if code == 0{
                //estas en la whitelist
                let story = UIStoryboard(name: "scanNPay", bundle: nil)
                let controller = story.instantiateViewController(withIdentifier: "scanNPayIdentifierUsaMex")
                SendTokenFirestore.viewGlobalThree.navigationController?.pushViewController(controller, animated: true)
                
                //se activa el whiteList de jumio
                DataUser.WHITELISTJUMIO = "1"
         
                
                
            }else{
            //no estas en la white list
                let story = UIStoryboard(name: "scanNPay", bundle: nil)
                let controller = story.instantiateViewController(withIdentifier: "scanNPayIdentifierUsaMex")
                
               SendTokenFirestore.viewGlobalThree.navigationController?.pushViewController(controller, animated: true)
            }
            }else if SendTokenFirestore.typeWhiteList == "transfer"{
                
                
                if code == 0{
                   //si esta
                    
                    let story = UIStoryboard(name: "Viamericas", bundle: nil)
                    let controller = story.instantiateViewController(withIdentifier: "transferViaAmericaId")
                    
                    SendTokenFirestore.viewGlobalThree.navigationController?.pushViewController(controller, animated: true)
                    //se activa el whiteList de jumio
                    DataUser.WHITELISTJUMIO = "1"
                    
                }else{
                    //no esta
                    
                    let story = UIStoryboard(name: "Viamericas", bundle: nil)
                    let controller = story.instantiateViewController(withIdentifier: "transferViaAmericaId")
                    
                    SendTokenFirestore.viewGlobalThree.navigationController?.pushViewController(controller, animated: true)
                    
                  
                    
                }
                
                
                
            }
            
        }
     
        
        if endpoint == "Wallet/1/getCustomCard?idUsuario=\(DataUser.USERIDD!)&idioma=es&pais=1"{
           
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                
                 let story = UIStoryboard(name: "myMC", bundle: nil)
                 let controller = story.instantiateViewController(withIdentifier: "mcpanel_view") as! MCPanel_Ctrl
                
                
                controller.mcCardInfo = response["card"] as? [AnyHashable : Any]
                SendTokenFirestore.viewGlobalThree.navigationController?.pushViewController(controller, animated: true)
                
                
                
                
            }else{
                 SendTokenFirestore.viewGlobalThree.alert(title: "¡Aviso!", message: "Debes agregar una mobilecard desde tu cartera.", cancel: "OK")
                
            }
            
            
            
        }
        
        print("este es el response \(response)")
        if endpoint == "Wallet/1/getCards/with_movements?idUsuario=\(DataUser.USERIDD!)&idioma=es"{
            
         let idError = response["idError"] as! Int
            if idError == 0{
                
                let hasMobileCard = response["hasMobilecard"] as! BooleanLiteralType
                
                
                if DataUser.COUNTRYID == "3"{
                    //es usa
                    if hasMobileCard{
                        
                        let tarjetas = response["tarjetas"] as! NSArray
                        for element in tarjetas{
                            
                            let card = element as! NSDictionary
                            
                            let account_id = card["account_id"] as! String
                            
                            if account_id != ""{
                                DataUser.APTOACCOUNTID = account_id
                               
                            }
                            
                            
                        }
                        
                        let story = UIStoryboard(name: "Apto", bundle: nil)
                        let controller = story.instantiateViewController(withIdentifier: "AptoHome")
                        
                        SendTokenFirestore.viewGlobalThree.navigationController?.pushViewController(controller, animated: true)
                        
                    }else{
                        let story = UIStoryboard(name: "Apto", bundle: nil)
                        let controller = story.instantiateViewController(withIdentifier: "AptoWelcome")
                        
                        SendTokenFirestore.viewGlobalThree.navigationController?.pushViewController(controller, animated: true)
                        
                        
                    }
                    
                    
                    
                    
                }else if DataUser.COUNTRYID == "1"{
                    //Mexico
                    
                    if hasMobileCard{
                        //tiene
                        //llamamos al servicio donde la obtenemos
                        
                        let headers  : HTTPHeaders = [:]
                        let params : [String : Any] = [:]
                        SendTokenFirestore.services.send(type: .POST, url: "Wallet/1/getCustomCard?idUsuario=\(DataUser.USERIDD!)&idioma=es&pais=1", params: params, header: headers, message: "Obteniendo",animate: true)
                        
                        
                
                        
                    }else{
                        //no tiene
                        
                        let story = UIStoryboard(name: "previvale", bundle: nil)
                        let navigation : UINavigationController = story.instantiateViewController(withIdentifier: "user") as! UINavigationController
                        
                       
                        
                        SendTokenFirestore.viewGlobalThree.navigationController?.present(navigation, animated: true, completion: nil)
                        
                        
                    }
                    
                    
                }else{
                   //cualquier otro pais
                    SendTokenFirestore.viewGlobalThree.alert(title: "¡Aviso!", message: "Próximamente Enero 2020.", cancel: "OK")
                }
            
           
                
            }
            
        }
        
            
               }//termina if de si tenemos userid
        
        if endpoint == "PushNotifications/saveToken"{
            
            print("token actualizado correctamente")
        }
        
        
        if endpoint == "Usuarios/1/privacidad/\(DataUser.COUNTRYID!)/es"{
            
            print(response)
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let termino = response["termino"] as! String
                
                if SendTokenFirestore.isFrom == 0{
                SendTokenFirestore.viewGlobal.alertPrivacy(title: "Aviso de privacidad", message: termino, cancel: "Aceptar")
                }else if SendTokenFirestore.isFrom == 1{
                     SendTokenFirestore.viewGlobalTwo.alertPrivacy(title: "Aviso de privacidad", message: termino, cancel: "Aceptar")
                }
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
            }
            
        }
        
    
        
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    
    func errorService(endpoint: String) {
        print("error")
        
        if endpoint == "Tokenizer/1/\(DataUser.COUNTRYID!)/es/whiteList?idUsuario=\(DataUser.USERIDD!)"{
            SendTokenFirestore.viewGlobalThree.navigationController?.popViewController(animated: true)
            
        }
        
    }
    
    
    
    
    @objc static func AlertMcMex(view :MainMenu_Commerce){
        
        SendTokenFirestore.viewGlobalFour = view
        let customAlert = InstructionsMobilecard()
        customAlert.delegateInstruction = SendTokenFirestore()
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
       
        view.present(customAlert, animated: true, completion: nil)
        
    }
    
    @objc static func privacyMex(view :RegisterNew_Step2){
        
        SendTokenFirestore.viewGlobal = view
        SendTokenFirestore.isFrom = 0
        let headers  : HTTPHeaders = ["client":"f0b5f378b80597da0572d38cd881c9cda584a0ae4a363b33e157ac32059f0ae5"]
        let params : [String : Any] = [:]
        
       
         SendTokenFirestore.services = Services()
        SendTokenFirestore.services.delegate = SendTokenFirestore()
        SendTokenFirestore.services.send(type: .GET, url: "Usuarios/1/privacidad/\(DataUser.COUNTRYID!)/es", params: params, header: headers, message: "Obteniendo",animate: true)
        
        
        
        
        
    }
    
    @objc static func privacyMexTwo(view :Register_Commerce_Step10){
        
        SendTokenFirestore.viewGlobalTwo = view
          SendTokenFirestore.isFrom = 1
        let headers  : HTTPHeaders = ["client":"f0b5f378b80597da0572d38cd881c9cda584a0ae4a363b33e157ac32059f0ae5"]
        let params : [String : Any] = [:]
        
        SendTokenFirestore.services = Services()
        SendTokenFirestore.services.delegate = SendTokenFirestore()
        SendTokenFirestore.services.send(type: .GET, url: "Usuarios/1/privacidad/\(DataUser.COUNTRYID!)/es", params: params, header: headers, message: "Obteniendo",animate: true)
        
        
        
        
        
    }
    
    
    @objc static func openWhatsapp(view :contact_Ctrl){
        
            var urlWhats = ""
        switch DataUser.COUNTRYID!{
            
        case "1":
            //Mexico
                urlWhats = "whatsapp://send?phone=525552510001"
            break
            
        case "2":
            //Colombia
                          urlWhats = "whatsapp://send?phone=5715800822"
            break
            
            case "3":
                       //USA
                                     urlWhats = "whatsapp://send?phone=12134230411"
                       break
            
        case "4":
            //PERU
            urlWhats = "whatsapp://send?phone=5116449082"
            break
            
        default:
            break
        }
        
    
        
        
        
        
        
          if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
              if let whatsappURL = URL(string: urlString) {
                  if UIApplication.shared.canOpenURL(whatsappURL){
                      if #available(iOS 10.0, *) {
                          UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                      } else {
                          UIApplication.shared.openURL(whatsappURL)
                      }
                  }
                  else {
                    //mensaje no tiene whats
                    
                    view.alert(title: "¡Aviso!", message: "Debes tener instalado WhatsApp en tu dispositivo para utilizar esta opción.", cancel: "OK")
                    
                  }
              }
          }
        
        
    }
    
    @objc static func miMobilecard(view :MainMenu_Ctrl){
        
        SendTokenFirestore.viewGlobalThree = view
        
        let headers  : HTTPHeaders = [:]
        let params : [String : Any] = [:]
        
        
        SendTokenFirestore.services = Services()
        SendTokenFirestore.services.delegate = SendTokenFirestore()
        SendTokenFirestore.services.send(type: .POST, url: "Wallet/1/getCards/with_movements?idUsuario=\(DataUser.USERIDD!)&idioma=es", params: params, header: headers, message: "Obteniendo",animate: true)
        
        
        
        
        
        
    }
    
    //type "scan" type "transfer"
    @objc static func whiteList(view :MainMenu_Ctrl,type:String){
        
        SendTokenFirestore.viewGlobalThree = view
        
        let headers  : HTTPHeaders = [:]
        let params : [String : Any] = [:]
        
        
        SendTokenFirestore.services = Services()
        SendTokenFirestore.services.delegate = SendTokenFirestore()
        SendTokenFirestore.typeWhiteList = type
        SendTokenFirestore.services.send(type: .GET, url: "Tokenizer/1/\(DataUser.COUNTRYID!)/es/whiteList?idUsuario=\(DataUser.USERIDD!)", params: params, header: headers, message: "Obteniendo",animate: true)
        
        
        
        
        
        
    }

    @objc static func updateDataUser(base64 :NSString,dataUser:NSMutableDictionary,idUser:String)->NSDictionary{
        
        dataUser["img"] = base64
        
        
        
        
        
        
        
      //  print("este es el base64 \(base64)")
        print("este es el idUser\(idUser)")
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*"]
        let params : [String : Any] = ["idUser":idUser,"photo":base64]
        
       // print("se llama a esta url Usuarios/1/\(DataUser.COUNTRYID!)/es/user/photo/update")
       // print("se manda esto \(params)")
        
        SendTokenFirestore.services = Services()
        SendTokenFirestore.services.delegate = SendTokenFirestore()
        SendTokenFirestore.services.send(type: .POST, url: "Usuarios/1/\(DataUser.COUNTRYID!)/es/user/photo/update", params: params, header: headers, message: "Guardando",animate: true)
        
        
        return dataUser
        
    }
    
    @objc static func getPhoto(view :MainMenu_Ctrl){
        SendTokenFirestore.viewGlobalThree = view
        
        let alert:UIAlertController = UIAlertController(title: "¿Desde dónde quieres obtener la foto?", message: nil, preferredStyle: UIAlertController.Style.actionSheet);
        let cancel:UIAlertAction    = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil);
        let photo:UIAlertAction     = UIAlertAction(title: "Desde la galería", style: UIAlertAction.Style.default) { (UIAlertAction) -> Void in
            self.photo(sourceType: .photoLibrary);
        }
        let picture:UIAlertAction   = UIAlertAction(title: "Desde la cámara", style: UIAlertAction.Style.default) { (UIAlertAction) -> Void in
            self.photo(sourceType: .camera);
        }
        
        alert.addAction(photo);
        alert.addAction(picture);
        alert.addAction(cancel);
        view.present(alert, animated: true, completion: nil);
        
        
        
        
    }
    
   
    
    
    //metodo que llama la galeria o la camara dependiendo el valor que reciba
    static func photo(sourceType: UIImagePickerController.SourceType) {
        
        SendTokenFirestore.picker = UIImagePickerController()
        SendTokenFirestore.picker.delegate = SendTokenFirestore.viewGlobalThree
         SendTokenFirestore.picker.sourceType = sourceType;
       // picker.allowsEditing = true;
        picker.allowsEditing = false;
        SendTokenFirestore.viewGlobalThree.present(picker, animated: true, completion: nil)
        
    }
    
   
    
    
    
    //metodo delegado presentacion mobilecard en home comercio mexico
    public func successComplete() {
        print("accion exito")
        
        if DataUser.STATUSMC == "False"{
            
            SendTokenFirestore.viewGlobalFour.performSegue(withIdentifier: "showWalletIdentifier", sender: nil)
            
                 
            
            DataUser.STATUSMC = nil
               DataUser.MESSAGEMC = nil
        }else{
            
         
            DataUser.STATUSMC = nil
               DataUser.MESSAGEMC = nil
        }
 
        
    }
   
    
    
    

    
}
