

import UIKit
import AVFoundation

@objc public protocol welcomePackDelegate {
    func welcomeRegister()
    func registerNormal()
    func welcomePackBackAction()
}

@objc class WelcomePackInitial: UIViewController {

    
  
    @IBOutlet weak var alertView: UIView!
  @objc  var delegate : welcomePackDelegate!
    
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        
        //self.messageLabel.text = DataUser.MESSAGEMC!
  
      
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupView()
        animateView()
        
    }
    
    func setupView() {
        
        
        alertView.layer.cornerRadius = 15
        
       
        
        
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    
    @IBAction func successAction(_ sender: UIButton) {
        
        
        self.dismiss(animated: true, completion: nil)
           
        self.delegate.welcomeRegister()
    }
    
    
    @IBAction func registerNormalAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
                
             self.delegate.registerNormal()
        
    }
    
    
    
    @IBAction func closeAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
                      
                   self.delegate.welcomePackBackAction()
    }
    

}
