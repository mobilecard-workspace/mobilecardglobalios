

import UIKit
import Alamofire

class WelcomePackInstructionsViewController: UIViewController ,ScannerQrDelegate,ServicesDelegate{
    
    
    var services : Services!
    var serialQR : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.services = Services()
        self.services.delegate = self
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //metodo que se ejecuta al presionar el boton de escanear
    @IBAction func escanearAction(_ sender: UIButton) {
        
           let vc = ScannerQr()
            
         vc.delegate = self
            
        
        vc.modalPresentationStyle = .fullScreen
           self.present(vc, animated: true, completion: nil)
    }
    
    
    //metodos delegados de el scanner
    func successQr(qr: String) {
        print("\(qr)")
        
         
             let result = qr
               
                   
        // ponemos si hay algun espacio como +
                let encodeString = result.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
                  
                   self.dismiss(animated: false, completion: nil)
                   //desencriptamos
                   let decryptString = mT_commonController.decryptHard(encodeString)
         
              
               if decryptString == nil{
               self.alert(title: "¡Aviso!", message: "Código QR no valido", cancel: "OK")
               return;
               }
                   
                   
                   //si tenemos datos en el string desencriptado
                   if decryptString != nil{
                   
               //lo convertimos y pasamos a json nuestro string desencriptado
                   let jsonData = decryptString!.data(using: .utf8)
                  
                   var error = ""
                   if (jsonData!.count > 0){
                       do {
                          
                         
                           if let jsonData = jsonData {
                              let responseID = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                                 print("aaa \(responseID)")
                            error = ""
                           
                              let dictio = responseID as! NSDictionary
                               print("este es el diccionario final \(dictio)")
                               let serialQR = dictio["serialQR"] as! Int
                            
                            
                            let header : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
                            
                            let params : [String : Any] =  ["serialQR":serialQR]
                            
                            self.serialQR = String(serialQR)
                            
                            print("PARAMS: \(params)")
                            self.services.send(type: .POSTURLENCODED, url: "CuantoTeDebo/1/1/es/QRVerifyWP", params: params, header: header, message: "Obteniendo", animate: true)
                              
                               
                           }
                       } catch _ {
                           error = "1"
                       }
                       
                       if error == "1" {
                           self.alert(title: "¡Aviso!", message: "Código QR no válido", cancel: "OK")
                        
                           return
                       }
                       
                       
                       
                       
                   }else{
                       self.alert(title: "¡Aviso!", message: "Codigo qr no compatible , intente nuevamente.", cancel: "OK")
                       }

                   }
        
    }

    func backActionQr() {}
  
    
    //metodos delegados de los servicios
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        if endpoint == "CuantoTeDebo/1/1/es/QRVerifyWP"{
            
            let idError = response["idError"] as! Int
            
            if idError  == 0{
                
                let estatus = response["estatus"] as! Int
                
                
                if estatus != 1{
                      //   self.alert(title: "¡Aviso!", message: "Codigo correcto puedes continuar", cancel: "OK")
                    //mandamos  a continuar el registro pero con el serialQR
                    
                    
                    
                    DataUser.SERIALQRWELCOMEPACK = self.serialQR
                    let nextStory = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                    
                    
                    let nextVc = nextStory.instantiateViewController(withIdentifier: "smsRegister") as! RegisterSmsViewController
              
                             //se va con comercio mexico por welcom pack (cambiando el isFrom para saber de donde viene)
                    
                        nextVc.isFrom = 3
                    nextVc.serialQR = self.serialQR
                    self.navigationController?.pushViewController(nextVc, animated: true)
                    
                               
                           
                      
                    
                    
                    
                }else{
                    
                    let mensaje = response["mensajeError"] as! String
                    self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                }
                
           
            }else{
                
                let mensaje = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
            
            
        }
        
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor", cancel: "OK")
    }
    
    
}
