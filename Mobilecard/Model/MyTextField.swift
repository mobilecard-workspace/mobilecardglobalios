

import Foundation
import UIKit


protocol MyTextFieldDelegate: class {
    func textFieldDidEnterBackspace(_ textField: MyTextField)
}

//objeto que almacena la informacion
class MyTextField: UITextField{
    
    
    weak var myTextFieldDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        if text?.isEmpty ?? false {
            myTextFieldDelegate?.textFieldDidEnterBackspace(self)
        }
        
        super.deleteBackward()
    }

 
}





