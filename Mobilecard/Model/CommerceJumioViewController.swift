import UIKit
import Alamofire
import JumioCore
import Netverify
import AVFoundation



@objc class CommerceJumioViewController: UIViewController ,NetverifyViewControllerDelegate,ServicesDelegate,CustomAlertPrivacyDelegate,CustomAlertJumioDelegate{

     var scanReferenceAux = ""
     @objc var email = ""
    
    
    
    var services : Services!
    //jumio controller
     var netverifyViewController:NetverifyViewController?
     var customUIController:NetverifyUIController?

    
    
    public override func viewDidLoad() {
       
      
        //delegamos los servicios
        self.services = Services()
        self.services.delegate = self
        
        self.generateNetverify()
    }
    
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
       
        print("esto responde \(response)")

        if endpoint == "\(ServicesLinks.MXUpdateReference)\(self.email)\(ServicesLinks.PEUpdateReferenceComplex)\(self.scanReferenceAux)"{
            
            
            
            let error = response["idError"] as! Int
            
            if error == 0{
                
                let mensaje = response["mensajeError"] as! String
                
                
                let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertJumioID") as! CustomAlertJumioViewMessage
                customAlert.providesPresentationContextTransitionStyle = true
                customAlert.definesPresentationContext = true
                customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                customAlert.mensaje = mensaje
                customAlert.delegate = self
                
                self.present(customAlert, animated: true, completion: nil)
                
            }else{
                
                let mensaje = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
            }
            
            
        }
        
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    
    func errorService(endpoint: String) {
        
        
        if endpoint == "\(ServicesLinks.MXUpdateReference)\(self.email)\(ServicesLinks.PEUpdateReferenceComplex)\(self.scanReferenceAux)"{
            
            
            
            
            let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
            let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertPrivacyID") as! CustomAlertPrivacyViewMessage
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.titulo = "¡Aviso!"
            customAlert.mensaje = "Inicia sesión en MobileCard para completar tu proceso de validación de documentos."
            customAlert.delegate = self
            
            self.present(customAlert, animated: true, completion: nil)
            
            
        }else{
            self.alert(title: "¡Aviso!", message: "No fue posible conectarse con el servidor, favor de revisar su conexión a internet.", cancel: "OK")
        }
        
    }
    

    
    func didAcceptButtonJumioComplete(){
        DataUser.STATELOGIN = "0"
        
        self.netverifyViewController?.destroy()
        self.netverifyViewController = nil
        
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RegisterNew_Step1.self) {
                LoaderView.hide(); self.navigationController?.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
  
    //metodo que configura y lanza el netverify de jumio
func generateNetverify(){
        
        if JumioDeviceInfo.isJailbrokenDevice() {
            
            self.alert(title: "¡Aviso!", message: "No es posible acceder a esta sección con el dispositivo modificado, contacte a soporte.", cancel: "OK")
            return
        }
        
        var config:NetverifyConfiguration = NetverifyConfiguration()
        //Provide your API token
        config.apiToken = "6623acf1-641c-43af-b62a-44ab4eb878ad"
        //Provide your API secret
        config.apiSecret = "gYjTrv8IMH2UaUIw5b0mSXFKubLSkvXj"
    
        config.delegate = self
    
    
    
    let defaults:UserDefaults = UserDefaults.standard;
    let idComercio:CLong? = defaults.integer(forKey: "idComercio");
   
    
    config.customerInternalReference = "N-\(idComercio!)"
       config.userReference = "N-\(idComercio!)"
    if (ServicesLinks.cuaCallBackJumio == true){
    config.callbackUrl = "https://www.mobilecard.mx/JumioQA/callback"
    }
    
    if DataUser.COUNTRYID != nil{
 
        switch DataUser.COUNTRYID {
     
        case "1":
            config.preselectedCountry = "MEX";
            break
        case "2":
            config.preselectedCountry = "COL";
            break
        case "3":
            config.preselectedCountry = "USA";
            break
        case "4":
            config.preselectedCountry = "PER";
            break
            
        default:
            break
        }
    }
    
       NetverifyDocumentSelectionButton.jumioAppearance().setBackgroundColor(#colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1), for: UIControl.State.normal)
        NetverifyDocumentSelectionButton.jumioAppearance().setIconColor(UIColor.white, for: UIControl.State.normal)
        NetverifyDocumentSelectionButton.jumioAppearance().setTitleColor(UIColor.white, for: UIControl.State.normal)
        
        UINavigationBar.jumioAppearance().tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        UINavigationBar.jumioAppearance().barTintColor = #colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1)
        UINavigationBar.jumioAppearance().titleTextAttributes = [kCTForegroundColorAttributeName: UIColor.white] as [NSAttributedString.Key : Any]
        
        self.netverifyViewController = NetverifyViewController(configuration: config)
        
        
        if let netverifyVC = self.netverifyViewController {
            self.present(netverifyVC, animated: true, completion: nil)
        } else {
            self.alert(title: "Netverify Mobile SDK", message: "NetverifyViewController is nil", cancel: "OK")
            
        }
        
        
    }
    
    
    
    
    
    
    //metodo delegado jumio se ejecuta cuando inicia el proceso puede contener un error
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didFinishInitializingWithError error: NetverifyError?) {
        print("NetverifyViewController did finish initializing")
    }
    
    
    //metodo delegado jumio se ejecuta cuando finaliza el escaneo viene con los resultados
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didFinishWith documentData: NetverifyDocumentData, scanReference: String) {
        
        
        //ponemos la referencia global
        self.scanReferenceAux = scanReference
        
        print("NetverifyViewController finished successfully with scan reference: %@", scanReference)
        // Share the scan reference for the Authentication SDK
        UserDefaults.standard.set(scanReference, forKey: "enrollmentTransactionReference")
        
        let selectedCountry:String = documentData.selectedCountry
        let selectedDocumentType:NetverifyDocumentType = documentData.selectedDocumentType
        var documentTypeStr:String
        switch (selectedDocumentType) {
        case .driverLicense:
            documentTypeStr = "DL"
            break;
        case .identityCard:
            documentTypeStr = "ID"
            break;
        case .passport:
            documentTypeStr = "PP"
            break;
        case .visa:
            documentTypeStr = "Visa"
            break;
        default:
            documentTypeStr = ""
            break;
        }
        
        //id
        let idNumber:String? = documentData.idNumber
        let personalNumber:String? = documentData.personalNumber
        let issuingDate:Date? = documentData.issuingDate
        let expiryDate:Date? = documentData.expiryDate
        let issuingCountry:String? = documentData.issuingCountry
        let optionalData1:String? = documentData.optionalData1
        let optionalData2:String? = documentData.optionalData2
        
        //person
        let lastName:String? = documentData.lastName
        let firstName:String? = documentData.firstName
        let dateOfBirth:Date? = documentData.dob
        let gender:NetverifyGender = documentData.gender
        var genderStr:String;
        switch (gender) {
        case .unknown:
            genderStr = "Unknown"
            
        case .F:
            genderStr = "female"
            
        case .M:
            genderStr = "male"
            
        case .X:
            genderStr = "Unspecified"
            
        default:
            genderStr = "Unknown"
        }
        
        let originatingCountry:String? = documentData.originatingCountry
        
        //address
        let street:String? = documentData.addressLine
        let city:String? = documentData.city
        let state:String? = documentData.subdivision
        let postalCode:String? = documentData.postCode
        
        // Raw MRZ data
        let mrzData:NetverifyMrzData? = documentData.mrzData
        
        let message:NSMutableString = NSMutableString.init()
        message.appendFormat("Selected Country: %@", selectedCountry)
        message.appendFormat("\nDocument Type: %@", documentTypeStr)
        if (idNumber != nil) { message.appendFormat("\nID Number: %@", idNumber!) }
        if (personalNumber != nil) { message.appendFormat("\nPersonal Number: %@", personalNumber!) }
        if (issuingDate != nil) { message.appendFormat("\nIssuing Date: %@", issuingDate! as CVarArg) }
        if (expiryDate != nil) { message.appendFormat("\nExpiry Date: %@", expiryDate! as CVarArg) }
        if (issuingCountry != nil) { message.appendFormat("\nIssuing Country: %@", issuingCountry!) }
        if (optionalData1 != nil) { message.appendFormat("\nOptional Data 1: %@", optionalData1!) }
        if (optionalData2 != nil) { message.appendFormat("\nOptional Data 2: %@", optionalData2!) }
        if (lastName != nil) { message.appendFormat("\nLast Name: %@", lastName!) }
        if (firstName != nil) { message.appendFormat("\nFirst Name: %@", firstName!) }
        if (dateOfBirth != nil) { message.appendFormat("\ndob: %@", dateOfBirth! as CVarArg) }
        message.appendFormat("\nGender: %@", genderStr)
        if (originatingCountry != nil) { message.appendFormat("\nOriginating Country: %@", originatingCountry!) }
        if (street != nil) { message.appendFormat("\nStreet: %@", street!) }
        if (city != nil) { message.appendFormat("\nCity: %@", city!) }
        if (state != nil) { message.appendFormat("\nState: %@", state!) }
        if (postalCode != nil) { message.appendFormat("\nPostal Code: %@", postalCode!) }
        if (mrzData != nil) {
            if (mrzData?.line1 != nil) {
                message.appendFormat("\nMRZ Data: %@\n", (mrzData?.line1)!)
            }
            if (mrzData?.line2 != nil) {
                message.appendFormat("%@\n", (mrzData?.line2)!)
            }
            if (mrzData?.line3 != nil) {
                message.appendFormat("%@\n", (mrzData?.line3)!)
            }
        }
        
        
        
        
        
        
        //Dismiss the SDK
        self.dismiss(animated: true, completion: {
            print(message)
            
            
            //  self.alert(title: "¡Aviso!", message: message as String, cancel: "OK")
            //    self.showAlert(withTitle: "Netverify Mobile SDK", message: message as String)
            
            //llamamos al updateReference
            print("si llama al servicio de referencia")
            
            let headers  : HTTPHeaders = [:]
            let params : [String : Any] = [:]
            self.services.send(type: .POST, url: "\(ServicesLinks.MXUpdateReference)\(self.email)\(ServicesLinks.PEUpdateReferenceComplex)\(scanReference)", params: params, header: headers, message: "Registrando",animate: true)
            
            
            
            
        })
    }
    
    
    //metodo delegado de jumio se ejecuta cuando se cancela el escaneo
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didCancelWithError error: NetverifyError?, scanReference: String?) {
        print("NetverifyViewController cancelled with error: \(error?.message ?? "") scanReference: \(scanReference ?? "")")
        
        //Dismiss the SDK
      //  self.dismiss(animated: true) {
   //         self.netverifyViewController?.destroy()
   //         self.netverifyViewController = nil
      //  }
    }
    
    
    
    //metodo delegado se ejecuta al presionar aceptar en la vista de error CustomAlertPrivacyDelegate
    func didAcceptButton() {
        DataUser.STATELOGIN = "0"
        self.netverifyViewController?.destroy()
        self.netverifyViewController = nil
        
        
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RegisterNew_Step1.self) {
                LoaderView.hide(); self.navigationController?.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
  
    
}
