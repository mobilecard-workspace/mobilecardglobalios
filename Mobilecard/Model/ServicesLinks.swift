

import Foundation
import UIKit




//objeto que almacena la informacion
 @objc class ServicesLinks: NSObject{
    
    //MANTENER MISMA SINTAXIS NORMALMENTE SOLO CAMBIAR LA IP DE SERVER POR LA DE CUA LOS OTROS PARAMETROS SON IGUALES
   

    //PRODUCCION
    /*
    @objc static let IPServer = "https://www.mobilecard.mx/"
     static let aptoApi = "https://api.ux.8583.io"
   
    
    static let aptoVaul = "https://vault.ux.8583.io"
    static let aptoApiKey = "KCR+DBqSu3M4a1+IZMCz9ffADr3EzwaBhF0WwcAQ0nVqtQlX28xZQdfoMAIcGEXI"
*/
    
    // CUA  ------   produccion = false , cua = true
    
    @objc static let cuaCallBackJumio = true
    
   
    
    @objc static let IPServer = "http://199.231.160.203/"
    static let aptoApi = "https://api.ux.sbx.aptopayments.com"
    static let aptoVaul = "https://vault.ux.sbx.aptopayments.com"
    static let aptoApiKey = "vSGISmhIDNBJR6UMsgy7v9NjeAJUo/88G82i++lRPXckgAt+BgeWgcc6+B5lByYC"
 
    
    
    //PERU
    static let PEGetDeparamentos = "Catalogos/1/4/es/getDepartamentos"

    static let PEGetProvincias = "Catalogos/1/4/es/getProvincias?codigo="
    
    static let PEGetDistritos = "Catalogos/1/4/es/getDistritos?codigo="
    
    static let PEPrivacyTerms = "Usuarios/1/privacidad/4/es"
    
    static let PETermsCon = "Usuarios/1/terminos/4/es"
    
    static let PERegisterCommerce = "Usuarios/1/es/user/insertv2"
    
    //estos dos son del mismo servicio
    static let PEUpdateReference = "Usuarios/1/4/es/commerce/update/reference?login="
    static let PEUpdateReferenceComplex = "&scanReference="
    
    static let MXUpdateReference = "Usuarios/1/1/es/commerce/update/reference?login="
    
    static let PEBanksCodes = "Usuarios/1/4/es/bankCodes"
    
    
    static let VisaQRWalletDecode = "VisaQRWalletService/qr/decode/hexadecimal"
    
    static let PETokenizer = "Tokenizer/1/4/es/getToken"
    
    //estos dos son del mismo servicio
    static let PEPayCardPresent = "CuantoTeDebo/1/4/es/NEGOCIO/"
    static let PEPayCardPresentComplex = "/pagoBP"
    
    
    //MX
    
    static let MXBanksCodes = "Usuarios/1/1/es/bankCodes"
}





