//
//  contact_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 11/7/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "contact_Ctrl.h"
#import <Mobilecard-Swift.h>
@interface contact_Ctrl ()
{
    NSMutableArray *menuItems;
}

@end

@implementation contact_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([__fromPE  isEqual: @"1"]){
        _buttonMenu.enabled = NO;
        _buttonMenu.tintColor = [UIColor clearColor];;
        
    }
    
    
    menuItems = [[NSMutableArray alloc] init];
    
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"60", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"callus", kIdentifierKey, @"85", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"0", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sendemail", kIdentifierKey, @"85", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"0", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"webpage", kIdentifierKey, @"85", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"0", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"whatsapp", kIdentifierKey, @"85", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"0", kHeightKey, nil]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma Mark UITableView delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuItems[indexPath.row][kIdentifierKey]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
   // NSString *country =[[userData objectForKey:kUserIdCountry] stringValue];
    
    NSString *country = DataUser.COUNTRYID;
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"whatsapp"]) {
      [SendTokenFirestore openWhatsappWithView:self];
        
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"callus"]) {
    
     
        switch ([country intValue]) {
                NSLog(@"SI entro a la seleccion por country");
            case 1:
                //mexico
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://018009255001"]];
                
                break;
            case 2:
                //colombia
                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://15800822"]];
                
                break;
            case 3:
                //usa
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://2134230411"]];
                
                break;
            case 4:
                //peru
                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://080080102"]];
                break;
                
                
        }
        
    }
    
    @try {
        if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"sendemail"]) {
            NSString *emailTitle = @"Contacto Addcel - iOS";
            NSString *messageBody = @"";
            NSArray *toRecipents = [NSArray arrayWithObject:@"soporte@addcel.com"];
            
            switch ([country intValue]) {
                    NSLog(@"SI entro a la seleccion por country");
                case 1:
                    //mexico
                    
                   toRecipents = [NSArray arrayWithObject:@"soporte@mobilecardmx.com"];
                    
                    break;
                case 2:
                    //colombia
                    toRecipents = [NSArray arrayWithObject:@"soporte@mobilecardco.com"];
                    
                    break;
                case 3:
                    //usa
                    toRecipents = [NSArray arrayWithObject:@"support@mobilecardusa.com"];
                    
                    break;
                case 4:
                    //peru
                    toRecipents = [NSArray arrayWithObject:@"soporte@mobilecardpe.com"];
                    break;
                    
                    
            }
            
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            
            // Present mail view controller on screen
            [self presentViewController:mc animated:YES completion:NULL];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    ////////////////////  correccion url web   Raul Mendez ///////////////////////////
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"webpage"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.mobilecardcorp.com/"]];
    }
}

- (IBAction)twitter_Action:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.twitter.com/mobilecardmx"]];
}

- (IBAction)facebook_Action:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/mobilecardmx/"]];
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}






@end
