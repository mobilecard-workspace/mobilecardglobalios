//
//  ViewCellPaymentNetwok.m
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "ViewCellPaymentNetwok.h"

@implementation ViewCellPaymentNetwok

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

        if (selected) {
            [_btn_selected_payment setHidden:NO];
        } else {
            [_btn_selected_payment setHidden:YES];
        }
    [self setBackgroundColor:[UIColor whiteColor]];
}
@end
