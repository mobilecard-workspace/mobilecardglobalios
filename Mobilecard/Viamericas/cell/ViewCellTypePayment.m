//
//  ViewCellTypePayment.m
//  Mobilecard
//
//  Created by Raul Mendez on 15/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "ViewCellTypePayment.h"

@implementation ViewCellTypePayment

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
        if (selected) {
            _radio_button_select.selected =true;
        } else {
            _radio_button_select.selected =false;
        }
    [self setBackgroundColor:[UIColor whiteColor]];
}

@end
