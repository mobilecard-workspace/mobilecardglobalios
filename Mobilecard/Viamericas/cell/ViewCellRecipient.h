//
//  ViewCellRecipient.h
//  Mobilecard
//
//  Created by Raul Mendez on 22/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewCellRecipient : UITableViewCell

@property (strong, nonatomic)  IBOutlet UILabel *lbl_title;
@property (strong, nonatomic)  IBOutlet UILabel *lbl_description;
@property (strong, nonatomic)  IBOutlet UIButton *btn_edit;


@end

NS_ASSUME_NONNULL_END
