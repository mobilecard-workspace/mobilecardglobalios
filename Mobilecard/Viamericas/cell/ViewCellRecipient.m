//
//  ViewCellRecipient.m
//  Mobilecard
//
//  Created by Raul Mendez on 22/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "ViewCellRecipient.h"

@implementation ViewCellRecipient

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
