//
//  ViewCellBankDelivery.h
//  Mobilecard
//
//  Created by Raul Mendez on 05/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewCellBankDelivery : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_payment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name_payment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_number_account_payment;
@property (weak, nonatomic) IBOutlet UIButton *btn_selected_payment;
@end

NS_ASSUME_NONNULL_END
