//
//  ViewCellTypePayment.h
//  Mobilecard
//
//  Created by Raul Mendez on 15/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//
#import "DLRadioButton.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewCellTypePayment : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_type_payment;
@property (weak, nonatomic) IBOutlet DLRadioButton *radio_button_select;

@end

NS_ASSUME_NONNULL_END
