//
//  ViewCellPaymentNetwok.h
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewCellPaymentNetwok : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_payment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sub_title_payment;
@property (weak, nonatomic) IBOutlet UIButton *btn_selected_payment;

@end

NS_ASSUME_NONNULL_END
