//
//  ViewCellAttachBank.h
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewCellAttachBank : UITableViewCell

@property (strong, nonatomic)  IBOutlet UILabel *lbl_name_bank;
@property (strong, nonatomic)  IBOutlet UILabel *lbl_account;
@property (strong, nonatomic)  IBOutlet UILabel *lbl_number_card;

@end

NS_ASSUME_NONNULL_END
