//
//  banksModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface banksModel : NSObject
@property (nonatomic, copy)      NSString  *bank_account_id,*bank_account_number,*bank_account_type,*bank_account_type_name,*nick_name,*payment_network_id,*payment_network_name,*recipient_id,*routing_number,*sender_id;
@end

NS_ASSUME_NONNULL_END
