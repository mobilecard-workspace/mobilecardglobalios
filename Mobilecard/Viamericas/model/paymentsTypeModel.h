//
//  paymentsTypeModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface paymentsTypeModel : NSObject{
    NSString  *_idTypePayment;
    NSString  *_nameTypePyment;
}
@property (nonatomic, copy)      NSString  *idTypePayment,*nameTypePyment;
-(NSDictionary *)dictionary;
@end

NS_ASSUME_NONNULL_END
