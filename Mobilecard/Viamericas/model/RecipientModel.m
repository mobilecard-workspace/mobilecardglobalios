//
//  RecipientModel.m
//  Mobilecard
//
//  Created by Raul Mendez on 22/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "RecipientModel.h"

@implementation RecipientModel
@synthesize  id_recipient,id_sender,country_id,state_id,city_id;
@synthesize firstName,secondname,lastname, middle_name , address1, address2, phone1, phone2 ,zip_code, email;
@end
