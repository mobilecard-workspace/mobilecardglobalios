//
//  locationsPaymentModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface locationsPaymentModel : NSObject
@property (nonatomic, copy)      NSString  *logo_name,*name_group,*number_of_location,*phoneBranch,*idBranchPayment;
@property (nonatomic, assign)    BOOL routing;

@end

NS_ASSUME_NONNULL_END
