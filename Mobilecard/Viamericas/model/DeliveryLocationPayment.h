//
//  DeliveryLocationPayment.h
//  Mobilecard
//
//  Created by Raul Mendez on 06/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryLocationPayment : NSObject
@property (nonatomic, copy)      NSString  *addressPaymentLocation,*businessHours,*idPaymentLocation,*latitude,*longitude,*namePayer,*namePaymentLocation,*idCountry,*idState,*idCity;
@end

NS_ASSUME_NONNULL_END
