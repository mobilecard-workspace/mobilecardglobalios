//
//  attachModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface attachModel : NSObject
@property (nonatomic, copy)      NSString  *recipient_id,*routing_number,*bank_account_number,*bank_account_type,*payment_network_id,*nick_name;

@end

NS_ASSUME_NONNULL_END
