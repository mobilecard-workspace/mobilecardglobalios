//
//  exchangeRateModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 07/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface exchangeRateModel : NSObject
@property (nonatomic, copy)      NSString  *dateRequested,* amount,*recivedAmount,*balance;
@property (nonatomic, assign)    double  exchangeRate,identificationLimit,maximumToSend;

@end

NS_ASSUME_NONNULL_END
