//
//  cityModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 05/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface cityModel : NSObject
@property (nonatomic, copy)      NSString  *idCity,*nameCity;
@end

NS_ASSUME_NONNULL_END
