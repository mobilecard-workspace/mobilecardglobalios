//
//  paymentsTypeModel.m
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "paymentsTypeModel.h"

@implementation paymentsTypeModel
@synthesize  idTypePayment=_idTypePayment;
@synthesize  nameTypePyment=_nameTypePyment;

-(NSDictionary *)dictionary {
    return [NSDictionary dictionaryWithObjectsAndKeys:self.idTypePayment,@"typePayment",self.nameTypePyment,@"namePayment", nil];
}
@end
