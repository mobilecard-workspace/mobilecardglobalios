//
//  stateModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 26/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface stateModel : NSObject
@property (nonatomic, copy)      NSString  *idState,*nameState;
@end

NS_ASSUME_NONNULL_END

