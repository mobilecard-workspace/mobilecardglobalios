//
//  RecipientModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 22/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecipientModel : NSObject
@property (nonatomic, assign)    NSString  *id_recipient,*id_sender,*country_id,*state_id,*city_id;
@property (nonatomic, copy)      NSString  *firstName,*secondname,*lastname, *middle_name,*address1, *address2, *phone1, *phone2,*zip_code, *email,*birthday;
@end

NS_ASSUME_NONNULL_END
