//
//  countrysModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 26/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface countrysModel : NSObject
@property (nonatomic, copy)      NSString  *idCountry,*nameCountry;


@end

NS_ASSUME_NONNULL_END
