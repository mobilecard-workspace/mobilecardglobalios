//
//  DeliveryLocationPayment.m
//  Mobilecard
//
//  Created by Raul Mendez on 06/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "DeliveryLocationPayment.h"

@implementation DeliveryLocationPayment
@synthesize
addressPaymentLocation,businessHours,idPaymentLocation,latitude,longitude,namePayer,namePaymentLocation,idCountry,idState,idCity;;
- (NSString *)searchTerms {
  return [NSString stringWithFormat:@"%@",self.namePayer];
}
@end
