//
//  orderPaymentModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 08/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface orderPaymentModel : NSObject
@property (nonatomic, copy)    NSString  *idBranch,* idReceiver,*idSender,*idTransaction,*transactionDate,*totalPayReceiver,*totalReceiver,*idBitacora,*idBitacoraTransfer,*passwordReceiver;
@end

NS_ASSUME_NONNULL_END
