//
//  RecipientController.m
//  Mobilecard
//
//  Created by Raul Mendez on 22/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "RecipientController.h"

@interface RecipientController (){
    pootEngine * endPointTermsCondition;
    pootEngine * endPointGetRecipients;
}
@end

@implementation RecipientController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    listRecipient = [[NSMutableArray alloc] init];
    //_statusTerms =FALSE;
    [self callEndPointGetRecipients];
}
#pragma mark - Ciclelive
-(void)viewWillAppear:(BOOL)animated
{
    self.TableViewRecipient.delegate = self;
    self.TableViewRecipient.dataSource = self;
    
    [super viewWillAppear:YES];
}
#pragma mark - Endpoints
- (void) validateProfileUser{
    if (_Profile != 0) {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"senderReg" bundle:nil];
        UINavigationController *nav = [nextStory instantiateInitialViewController];

        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }else{
        
    }
}
- (void) callEndPointGetRecipients{
    endPointGetRecipients = [[pootEngine alloc] init];
    [endPointGetRecipients setDelegate:self];
    [endPointGetRecipients setShowComments:developing];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    ///////////// Userdefault para obtener idUser
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
    
    [params setObject:userData[kUserIdCountry] forKey:@"idCountry"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [endPointGetRecipients startRequestWithURL:ViamericasGetRecipients withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        //[self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Server returns nil"];
        return;
    }
    if (manager == endPointTermsCondition) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
        }
    }
    if (manager == endPointGetRecipients) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            
            NSArray * ArrayRecipientsJson = [response objectForKey:@"recipients"];
            for (NSDictionary *ItemRecipient in ArrayRecipientsJson) {
                
                RecipientModel *Addcustom =[[RecipientModel alloc] init];
                Addcustom.id_sender = [ItemRecipient objectForKey:@"idSender"];
                Addcustom.city_id= [ItemRecipient objectForKey:@"idCity"];
                Addcustom.id_recipient= [ItemRecipient objectForKey:@"idRecipient"];
                Addcustom.country_id= [ItemRecipient objectForKey:@"idCountry"];
                Addcustom.state_id= [ItemRecipient objectForKey:@"idState"];
                Addcustom.firstName =[ItemRecipient objectForKey:@"firstName"];
                Addcustom.middle_name =[ItemRecipient objectForKey:@"middleName"];
                Addcustom.address1 =[ItemRecipient objectForKey:@"address1"];
                Addcustom.address2 =[ItemRecipient objectForKey:@"address2"];
                Addcustom.phone1 =[ItemRecipient objectForKey:@"phone1"];
                Addcustom.phone2 =[ItemRecipient objectForKey:@"phone2"];
                Addcustom.zip_code =[ItemRecipient objectForKey:@"zipCode"];
                Addcustom.lastname =[ItemRecipient objectForKey:@"lastName"];
                Addcustom.email =[ItemRecipient objectForKey:@"email"];
                Addcustom.birthday=[ItemRecipient objectForKey:@"birthDate"];
                [listRecipient addObject:Addcustom];
            }
            [self validCountListRecipients:(int)listRecipient.count];
            [self.TableViewRecipient reloadData];
        }
    }
}
#pragma mark UITableViewController LoadItems
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
//    [label setFont:[UIFont boldSystemFontOfSize:16]];
//    NSString *string =@"Beneficiario";
//    [label setText:string];
//    [view addSubview:label];
//    return view;
//}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listRecipient.count;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell ;
    @try
    {
        if (listRecipient == nil || [listRecipient count] == 0) {
            NSString *CellIdentifier = @"recipient";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            return cell;
        }
        
        RecipientModel *item =[listRecipient objectAtIndex:indexPath.row];
        NSString *CellIdentifier = @"recipient";
        ViewCellRecipient *cell = (ViewCellRecipient *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"recipient" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.lbl_title setText:item.firstName];
        [cell.lbl_description setText:item.address1];
        [cell.btn_edit setTag:indexPath.row];
        [cell.btn_edit addTarget:self action:@selector(clikedEdit:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    } @catch (NSException *exception)
    {
        // NSLog( @"NameException: %@", exception.name);
        // NSLog( @"ReasonException: %@", exception.reason );
        return cell;
    }
    @finally
    {
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
     itemSelected =[listRecipient objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"segue_confirm_payment" sender:self];
}

-(void)validCountListRecipients:(int) count
{
    if (count == 0) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Mobilecard"
                                     message:@"aún no tienes destinatarios"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"agregar destinatario"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
            
           // [self performSegueWithIdentifier:@"segue_new" sender:self];
            [self touch_addrecipient:nil];
        }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Cancelar"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
        }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}
#pragma mark - Actions Method
-(void) clikedEdit:(id)sender {
    
    NSInteger i = [sender tag];
    itemSelected =[listRecipient objectAtIndex:i];
 //   [self performSegueWithIdentifier:@"segue_edit" sender:self];
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Viamericas" bundle:nil];
    EditRecipientsController *detailVC =
            [nextStory instantiateViewControllerWithIdentifier:@"edit_recipient"];
    [detailVC setObj_edit:self];
    [detailVC setRecipient:itemSelected];
    [self.navigationController pushViewController:detailVC animated:NO];
}

- (IBAction)touch_backButton:(id)sender {
    if (_Profile != 0) {
        [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)touch_addrecipient:(id)sender {
   // [self performSegueWithIdentifier:@"segue_new" sender:self];
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Viamericas" bundle:nil];
    AddRecipientController *detailVC =
            [nextStory instantiateViewControllerWithIdentifier:@"add_recipient"];
    [detailVC setObj:self];
    [self.navigationController pushViewController:detailVC animated:NO];
}
-(void)recipientCreationResult:(BOOL) result{
    [self MessageAlert:@"Haz agregado un nuevo beneficiario"];
    listRecipient = [[NSMutableArray alloc] init];
    [self callEndPointGetRecipients];
}
- (void) recipientUpdateResult:(BOOL)result{
    [self MessageAlert:@"Haz actualizado los datos de tú beneficiario"];
    listRecipient = [[NSMutableArray alloc] init];
    [self callEndPointGetRecipients];
}
- (void) recipientDeleteResult:(BOOL)result{
    [self MessageAlert:@"Haz eliminado un beneficiario"];
    listRecipient = [[NSMutableArray alloc] init];
    [self callEndPointGetRecipients];
}
-(void) MessageAlert:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                    alertControllerWithTitle:@"Mobilecard"
                                    message:message
                                    preferredStyle:UIAlertControllerStyleAlert];
       
       UIAlertAction* yesButton = [UIAlertAction
                                   actionWithTitle:@"entendido"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
           
       }];
       [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"segue_confirm_payment"]){
        DeliveryPaymentController *next = (DeliveryPaymentController *)[segue destinationViewController];
        [next setRecipient:itemSelected];
    }
//    if([[segue identifier] isEqualToString:@"segue_confirm_payment"]){
//           DeliveryTypePaymentController *next = (DeliveryTypePaymentController *)[segue destinationViewController];
//           [next setRecipient:itemSelected];
//       }

}
@end
