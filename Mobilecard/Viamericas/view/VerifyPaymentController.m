//
//  VerifyPaymentController.m
//  Mobilecard
//
//  Created by Raul Mendez on 06/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//
#define Second 0.6


#import "VerifyPaymentController.h"

@interface VerifyPaymentController (){
    pootEngine *currentverifyManager;
    pootEngine *currentTypeManager;
    pootEngine *exchangeRateManager;
    pootEngine *statusProfile;
    pootEngine *LocationNetworkManager;
}

@end

@implementation VerifyPaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [self endPointStatusProfile];
}
-(void)endPointVerifiTypeCurrency{
    currentTypeManager = [[pootEngine alloc] init];
    [currentTypeManager setDelegate:self];
    [currentTypeManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:self->_LocationDelivery.idCountry==nil?self->_recipient.country_id:self->_LocationDelivery.idCountry forKey:@"idCountry"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [currentTypeManager startRequestWithURL:ViamericasGetCurrency withPost:post];
    //   [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
-(void)endPointStatusProfile{
    statusProfile = [[pootEngine alloc] init];
    [statusProfile setDelegate:self];
    [statusProfile setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    
    [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [statusProfile startRequestWithURL:ViamericasStatusProfile withPost:post];
    //   [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
-(void)endPointVerifiCurrency:(NSString *) cardId{
    ///////////////////// Queda pendiente por parte de Horacio el endPoint
    
    currentverifyManager = [[pootEngine alloc] init];
    [currentverifyManager setDelegate:self];
    [currentverifyManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:cardId forKey:@"idCard"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [currentverifyManager startRequestWithURL:ViamericasStatusCardBalance withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
-(void)endPointLocationNetwork{
    LocationNetworkManager = [[pootEngine alloc] init];
    [LocationNetworkManager setDelegate:self];
    [LocationNetworkManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:_PaymentModeId forKey:@"idPaymentMode"];
    [params setObject:self->_LocationDelivery.idCountry==nil?self->_recipient.country_id:self->_LocationDelivery.idCountry forKey:@"idPayBranch"];
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [LocationNetworkManager startRequestWithURL:ViamericasGetPaymentLocation withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
-(void)endPointexchangeRate{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->exchangeRateManager = [[pootEngine alloc] init];
        [self->exchangeRateManager setDelegate:self];
        [self->exchangeRateManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:self->_PaymentModeId forKey:@"modePayment"];
        [params setObject:self->_LocationDelivery.idPaymentLocation==nil?self->_itemBank.payment_network_id:self->_LocationDelivery.idPaymentLocation forKey:@"idPayBranch"];
        [params setObject:[self extractNumberFromText:self->amountText.text] forKey:@"monto"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->exchangeRateManager startRequestWithURL:ViamericasGetExchangeRate withPost:post];
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    if (json == nil) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Server returns nil"];
        return;
    }
    
    if (manager == LocationNetworkManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    
    if (manager == currentverifyManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            currenBalanceArray =[response objectForKey:@"balances"][0];
            
//            [lbl_name_recipient setText:[NSString stringWithFormat:@"De: %@",_recipient.firstName]];
//            currectBalance =[[currenBalanceArray objectForKey:@"total_amount"] doubleValue];
//            [balanceText setText:[NSString stringWithFormat:@"$ %.2f USD", currectBalance]];
//            InfoexchangeRate.hidden=YES;
//            lbl_saldo.hidden=YES;
            [self endPointVerifiTypeCurrency];
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    if (manager == currentTypeManager) {
           NSMutableDictionary *response = (NSMutableDictionary*)json;
           response = [self cleanDictionary:response];
           if ([[response objectForKey:kIDError] intValue] == 0) {
               
               currencyArray =[response objectForKey:@"currency"][0];
               
               [lbl_name_recipient setText:[NSString stringWithFormat:@"De: %@",_recipient.firstName]];
               currectBalance =[[currenBalanceArray objectForKey:@"total_amount"] floatValue];
               [balanceText setText:[NSString stringWithFormat:@"$ %.2f USD", currectBalance]];
               InfoexchangeRate.hidden=YES;
               lbl_saldo.hidden=YES;
               
           } else {
               [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
           }
       }
    
    if (manager == statusProfile) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            IdUser =[response objectForKey:@"idUsuario"];
            [self endPointVerifiCurrency:[response objectForKey:@"idCard"]];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    
    if (manager == exchangeRateManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            NSDictionary * itemExchange  = response[@"exchangeRateRes"][0];

            selectedExchangeRateModel =[[exchangeRateModel alloc] init];
            selectedExchangeRateModel.dateRequested =[itemExchange objectForKey:@"exchangeRate"];
            selectedExchangeRateModel.exchangeRate =[[itemExchange objectForKey:@"exchangeRate"] doubleValue];
            selectedExchangeRateModel.identificationLimit =[[itemExchange objectForKey:@"identificationLimit"] doubleValue];
            selectedExchangeRateModel.maximumToSend =[[itemExchange objectForKey:@"maximumToSend"] doubleValue];
            selectedExchangeRateModel.maximumToSend =[[itemExchange objectForKey:@"minimumToSend"] doubleValue];
            selectedExchangeRateModel.amount=[self extractNumberFromText:amountText.text] ;
            NSRange range = NSMakeRange (0, selectedExchangeRateModel.amount.length-2);
            selectedExchangeRateModel.amount=[selectedExchangeRateModel.amount substringWithRange:range];
            selectedExchangeRateModel.recivedAmount=recivedText.text;
            selectedExchangeRateModel.balance=balanceText.text;
            
          
            
            [recivedText setText:[NSString stringWithFormat:@"$ %.2f %@", [self validateSendAmountRecived:[itemExchange objectForKey:@"exchangeRate"] Amount:[[self extractNumberFromText:selectedExchangeRateModel.amount] doubleValue]],[[currencyArray objectForKey:@"currency_id"] uppercaseString]]];
            
            [InfoexchangeRate setTitle:[NSString stringWithFormat:@"1 USD=%@ %@",[itemExchange objectForKey:@"exchangeRate"],[[currencyArray objectForKey:@"currency_id"] uppercaseString]] forState:UIControlStateNormal];
            InfoexchangeRate.hidden=NO;

           
            [self validateBalanceAccount:[selectedExchangeRateModel.amount  doubleValue]];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
}
-(void)AlertviewCustom:(NSString *)title Message:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        
        
    }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(BOOL)validateBalanceAccount:(double) amount{
    if ( currectBalance < amount) {
        
        amountText.textColor = [UIColor redColor];
        recivedText.textColor = [UIColor redColor];
        lbl_saldo.hidden=NO;
        return FALSE;
    }else{
        amountText.textColor = [UIColor blackColor];
        recivedText.textColor = [UIColor blackColor];
        lbl_saldo.hidden=YES;
        return TRUE;
    }
    return TRUE;
}
-(double)validateSendAmountRecived:(NSString *)exchangeRate Amount:(double) amount{
    double result=0.0;
    result = amount * [exchangeRate doubleValue];
    return result;
}
- (IBAction)touch_backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)touch_amountEditingBegin:(id)sender {
    [amountText setText:@""];
}

- (IBAction)touch_amountEditingEnd:(id)sender {
    [amountText setText:[NSString stringWithFormat:@"$ %.2f USD", [amountText.text floatValue]]];
    [self endPointexchangeRate];
}

- (IBAction)touch_orderPayment:(id)sender {
    @try {
        if ([amountText.text isEqualToString:@""] || [recivedText.text isEqualToString:@""]) {
            [self AlertviewCustom:@"Informacion" Message:@"Ingresa la cantidad de envio"];
            return;
        }
        if ([self validateBalanceAccount:[selectedExchangeRateModel.amount doubleValue]]) {
          [self performSegueWithIdentifier:@"segue_confirm" sender:self];
        }
      /*  if ([self validateBalanceAccount:[[self extractNumberFromText:amountText.text] doubleValue]]) {
            [self performSegueWithIdentifier:@"segue_confirm" sender:self];
        }*/
    } @catch (NSException *exception) {
        NSLog(@"Exepcion:  %@",exception);
    } @finally {
        
    }
}
- (NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"segue_confirm"]){
        ConfirmController *next = (ConfirmController *)[segue destinationViewController];
        [next setRecipient:_recipient];
        [next setItemBank:_itemBank];
        [next setIdUsuarioEndPoint:IdUser];
        [next setLocationDelivery:_LocationDelivery];
        [next setPaymentModeId:_PaymentModeId];
        [next setExchangeRate:selectedExchangeRateModel];
        
    }
}

@end
