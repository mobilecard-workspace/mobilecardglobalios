//
//  SelectedLocationPaymentController.h
//  Mobilecard
//
//  Created by Raul Mendez on 19/01/20.
//  Copyright © 2020 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"
#import "NSPredicateSearch/NSPredicate+Search.h"

#import "VerifyPaymentController.h"
#import "DeliveryLocationPayment.h"
#import "ViewCellPaymentNetwok.h"
#import "RecipientModel.h"
#import "countrysModel.h"
#import "stateModel.h"
#import "cityModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SelectedLocationPaymentController : mT_commonController<UITableViewDelegate,UITableViewDataSource>{
    
    
    DeliveryLocationPayment *selectLocationDelivery;
    NSMutableArray *objDeliveryPayment;
    NSMutableArray *DeliveryPaymentLocal;
    NSMutableArray *StringDeliveryPayment;
    NSArray *searchResults;


}
@property (nonatomic, retain) RecipientModel *recipient;
@property (nonatomic, assign)  NSString* PaymentModeId;
@property (nonatomic, assign)  cityModel* SelectedCity;
@property (nonatomic, assign)  stateModel * SelectedState;
@property (nonatomic, assign)  countrysModel* SelectedCountry;


@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *TableViewPaymentNetwork;


- (IBAction)touch_continue:(id)sender;

@end

NS_ASSUME_NONNULL_END
