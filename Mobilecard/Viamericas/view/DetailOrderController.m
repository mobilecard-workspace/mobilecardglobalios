//
//  DetailOrderController.m
//  Mobilecard
//
//  Created by Raul Mendez on 08/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "DetailOrderController.h"

@interface DetailOrderController ()

@end

@implementation DetailOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [info_recipient setText:[NSString stringWithFormat:@"Se crea su solicitud para enviar dinero a %@", _recipìent.firstName]];
    
    [idTransacction setText:[NSString stringWithFormat:@" %@ ", _OrderPayment.idTransaction]];
    [numberFolio setText:[NSString stringWithFormat:@" %@ ", _OrderPayment.passwordReceiver]];
    [mountSed setText:[NSString stringWithFormat:@" %@", _OrderPayment.totalReceiver]];
    [lbl_date setText:[NSString stringWithFormat:@" %@", _OrderPayment.transactionDate]];
}
-(NSString *)equivalenceMount:(NSString *) amount{
    @try {
        double equivalence= [amount doubleValue] * _exchangeRate.exchangeRate;
        return [NSString stringWithFormat:@"%.2f",equivalence];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    return @"";
}

- (IBAction)touch_understood:(id)sender {
      NSArray *array = [self.navigationController viewControllers];
      [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
}
@end
