//
//  DeliveryPaymentController.h
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"

#import "countrysModel.h"
#import "stateModel.h"
#import "cityModel.h"
#import "banksModel.h"
#import "DLRadioButton.h"
#import "DownPicker.h"
#import "RecipientModel.h"

#import "ViewCellPaymentNetwok.h"
#import "DeliveryLocationPayment.h"
#import "ViewCellBankDelivery.h"


#import "VerifyPaymentController.h"
#import "SelectedLocationPaymentController.h"
NS_ASSUME_NONNULL_BEGIN

@interface DeliveryPaymentController : mT_commonController<UITableViewDelegate,UITableViewDataSource>{
    
   __weak IBOutlet UITextField_Validations *countryText;
   __weak IBOutlet UITextField_Validations *stateText;
   __weak IBOutlet UITextField_Validations *cityText;
   __weak IBOutlet UILabel *exchangeRateLabel;
    
    __weak IBOutlet UILabel *lbl_deposit;
    __weak IBOutlet UILabel *lbl_cash_payment;
    __weak IBOutlet UILabel *lbl_delivery_home;
    
    NSMutableArray *objCountrys;
    NSMutableArray *objCountrysState;
    NSMutableArray *objCountrysCity;
    NSMutableArray *objDeliveryPayment;
    
    countrysModel * itemSelectedCountry;
    stateModel * itemSelectedCountryState;
    cityModel * itemSelectedCountryCity;
    
    NSMutableArray * listAttachBank;
    banksModel *itemSelectedBank;
    DeliveryLocationPayment *selectLocationDelivery;
    
    IBOutlet DLRadioButton *radiobutton_deposit;
    IBOutlet DLRadioButton *radiobutton_cashPikup;
    IBOutlet DLRadioButton *radiobutton_homedelivery;
    
    NSString* PaymentModeId;
    IBOutlet UIButton *countinuebank;
    __weak IBOutlet UIButton *continuePayment;
}

@property (nonatomic, retain) RecipientModel *recipient;

@property (strong, nonatomic) DownPicker *downPickerCountry;
@property (strong, nonatomic) DownPicker *downPickerState;
@property (strong, nonatomic) DownPicker *downPickerCity;

- (IBAction)touch_radio_deposit:(id)sender;
- (IBAction)touch_radio_cash:(id)sender;
- (IBAction)touch_radio_home_delivery:(id)sender;

- (IBAction)touchBeginCountryText:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *TableViewAttachBank;
@property (strong, nonatomic) IBOutlet UITableView *TableViewPaymentNetwork;

- (IBAction)touch_continue:(id)sender;


@end

NS_ASSUME_NONNULL_END
