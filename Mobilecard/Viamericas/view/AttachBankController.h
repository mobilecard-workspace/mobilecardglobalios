//
//  AttachBankController.h
//  Mobilecard
//
//  Created by Raul Mendez on 27/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"
#import "paymentsTypeModel.h"
#import "RecipientModel.h"
#import "countrysModel.h"
#import "locationsPaymentModel.h"
#import "ViewCellPaymentNetwok.h"
#import "banksModel.h"

#import "DownPicker.h"

NS_ASSUME_NONNULL_BEGIN
@protocol BankDelegate <NSObject>

@required
- (void) addbankResult:(BOOL)result;
- (void) bankUpdateResult:(BOOL)result;
- (void) banktDeleteResult:(BOOL)result;

@end
@interface AttachBankController : mT_commonController<UITextFieldDelegate,pootEngineDelegate,UITableViewDelegate,UITableViewDataSource>{
    
    __weak IBOutlet UITextField_Validations *routing_number;
    __weak IBOutlet UITextField_Validations *banckAccount;
    __weak IBOutlet UITextField_Validations *nickName;
    __weak IBOutlet UITextField_Validations *bankAccountType;
    __weak IBOutlet UITextField_Validations *countryText;
    
    NSMutableArray * listAttachBank;
    NSMutableArray * listMethodPayments;
    NSMutableArray * listLocationPayments;
    NSMutableArray * listPaymentString;
    
    NSMutableArray *objCountrys;
    NSMutableArray *objCountrysState;
    NSMutableArray *objCountrysCity;
    
    countrysModel * itemSelectedCountry;
    paymentsTypeModel *paymentSelect;
    locationsPaymentModel *itemlocationSelect;
    
    __weak IBOutlet UIButton *btn_addBank;
    
    __weak IBOutlet UIButton *btn_editBank;
}
@property (strong, nonatomic) DownPicker *downPickerCountry;
@property (strong, nonatomic) DownPicker *downPickerBankType;


@property (nonatomic, assign) BOOL statusEdit;
@property (nonatomic, retain) RecipientModel *recipient;
@property (nonatomic, retain) banksModel *itemBank;

@property (nonatomic, assign) id <BankDelegate> obj_bank;
@property (nonatomic, strong) IBOutlet UITableView *TableViewPaymentNetwork;
- (IBAction)touch_add_attachpayment:(id)sender;
- (IBAction)touch_edit_bank:(id)sender;
- (IBAction)touch_editingAlias:(id)sender;

- (IBAction)touch_delete_bank:(id)sender;

@end

NS_ASSUME_NONNULL_END
