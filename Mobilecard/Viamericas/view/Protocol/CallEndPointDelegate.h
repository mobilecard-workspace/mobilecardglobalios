//
//  CallEndPointDelegate.h
//  Mobilecard
//
//  Created by Raul Mendez on 01/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface CallEndPointDelegate: NSObject
+(void)makeRequestUrl:(NSString *)UrlName andParams:(NSString *)params Method:(NSString *) MethodName User:(NSString *)user Password:(NSString *) password completion:(void (^)(NSString *))completion;
+(NSString *) makePOSTRequestUrlAutenticationBase:(NSString *)methodName andParams:(NSString *)params  User:(NSString *)user Password:(NSString *) password completion:(void (^)(NSString *))completion;
+(NSString *) makeGETRequestUrlAutenticationBase:(NSString *)methodName andParams:(NSString *)params  User:(NSString *)user Password:(NSString *) password completion:(void (^)(NSString *))completion;
@end


NS_ASSUME_NONNULL_END
