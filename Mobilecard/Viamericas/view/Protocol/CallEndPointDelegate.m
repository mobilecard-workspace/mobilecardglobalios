//
//  CallEndPointDelegate.m
//  Mobilecard
//
//  Created by Raul Mendez on 01/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "CallEndPointDelegate.h"

@implementation CallEndPointDelegate
NSString *respuesta;
+(void)makeRequestUrl:(NSString *)UrlName andParams:(NSString *)params Method:(NSString *) MethodName User:(NSString *)user Password:(NSString *) password completion:(void (^)(NSString *))completion{
   
        dispatch_queue_t queue1 = dispatch_get_global_queue(0, 0);
        dispatch_async(queue1, ^{
            NSString *strURL = UrlName;
//            [Utils LogString:__LINE__ NameClass:NSStringFromClass([self class]) Message:[NSString stringWithFormat:@"Url:  %@",strURL]];
//            [Utils LogString:__LINE__ NameClass:NSStringFromClass([self class]) Message:[NSString stringWithFormat:@"Params:  %@",params]];
            
            NSData *aData = [params dataUsingEncoding:NSUTF8StringEncoding];
            NSMutableURLRequest *aRequest = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strURL]];
            [aRequest setHTTPMethod:MethodName];
            [aRequest setHTTPBody:aData];
            
            NSString *authStr = [NSString stringWithFormat:@"%@:%@", user, password];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
            NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];

            [aRequest setValue:authValue forHTTPHeaderField:@"Authorization"];
            [aRequest setValue:[NSString stringWithFormat: @"iOS:%@",build] forHTTPHeaderField:@"plataforma"];
        
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
            [aRequest setHTTPBody:aData];
            
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:aRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                // NSLog(@"response status code server: %ld", (long)[httpResponse statusCode]);
                if ((long)[httpResponse statusCode] == 200) {
                    if (error ==nil) {
                        NSString *aStr  = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            [Utils LogString:__LINE__ NameClass:NSStringFromClass([self class]) Message:[FBEncryptorAES decryptBase64String:aStr keyString:[dict objectForKey:@"key"]]];
                            //                            completion([FBEncryptorAES decryptBase64String:aStr keyString:[dict objectForKey:@"key"]]);
                            completion(aStr);
                        });
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(@"");
                            NSInteger statusCode = error.code;
                            NSLog(@"Error: %ld %@",statusCode,error.userInfo);
                            //                            [Utils LogString:__LINE__ NameClass:NSStringFromClass([self class]) Message:[NSString stringWithFormat:@"error:  %@",error.userInfo]];
                            //                             [Utils LogString:__LINE__ NameClass:NSStringFromClass([self class]) Message:[NSString stringWithFormat:@"code error:  %ld",statusCode]];
                        });
                    }
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(@"");
                        NSInteger statusCode = error.code;
                        NSLog(@"Error: %ld %@",statusCode,error.userInfo);
                    });
                    //                     [Utils LogString:__LINE__ NameClass:NSStringFromClass([self class]) Message:[NSString stringWithFormat:@"code error:  %ld",statusCode]];
                }
            }];
            [postDataTask resume];
        });
}

+(NSString *) makeGETRequestUrlAutenticationBase:(NSString *)methodName andParams:(NSString *)params  User:(NSString *)user Password:(NSString *) password completion:(void (^)(NSString *))completion{
    __block NSString *response;
    [self  makeRequestUrl:methodName andParams:params Method:@"GET" User:user Password:password  completion:^(NSString *responseDict){
//        [Utils LogString:__LINE__ NameClass:NSStringFromClass([self class]) Message:[NSString stringWithFormat:@"Response:  %@",responseDict]];

        completion(responseDict);
        response =responseDict;
    }];
 return response;
}

+(NSString *) makePOSTRequestUrlAutenticationBase:(NSString *)methodName andParams:(NSString *)params  User:(NSString *)user Password:(NSString *) password completion:(void (^)(NSString *))completion{
    __block NSString *response;
    [self  makeRequestUrl:methodName andParams:params Method:@"POST" User:user Password:password completion:^(NSString *responseDict){
//        [Utils LogString:__LINE__ NameClass:NSStringFromClass([self class]) Message:[NSString stringWithFormat:@"Response:  %@",responseDict]];

        completion(responseDict);
        response =responseDict;
    }];
    return response;
}


@end
