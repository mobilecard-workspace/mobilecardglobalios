//
//  DeliveryPaymentController.m
//  Mobilecard
//
//  Created by Raul Mendez on 28/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "DeliveryPaymentController.h"

@interface DeliveryPaymentController (){

}

@end

@implementation DeliveryPaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [self.navigationController.navigationBar setTranslucent:NO];
    //    [self initializePickersForView:self.navigationController.view];
    //
    [countryText setUpTextFieldAs:textFieldTypeName];
    [countryText setRequired:YES];
    [stateText setUpTextFieldAs:textFieldTypeName];
    [stateText setRequired:YES];
    [cityText setUpTextFieldAs:textFieldTypeName];
    [cityText setRequired:YES];
    
    [self addValidationTextField:countryText];
    [self addValidationTextField:stateText];
    [self addValidationTextField:cityText];
    
    [self addValidationTextFieldsToDelegate];
    
    
    lbl_delivery_home.hidden =YES;
    radiobutton_homedelivery.hidden=YES;
    
    lbl_deposit.hidden =YES;
    radiobutton_deposit.hidden=YES;
    
    lbl_cash_payment.hidden =YES;
    radiobutton_cashPikup.hidden=YES;
    
    PaymentModeId=@"";
   
    
    self.TableViewPaymentNetwork.hidden =YES;
    self.TableViewAttachBank.hidden =YES;
    
    countinuebank.hidden=YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.TableViewAttachBank.delegate = self;
    self.TableViewAttachBank.dataSource = self;
    
    self.TableViewPaymentNetwork.delegate = self;
    self.TableViewPaymentNetwork.dataSource = self;
    
    itemSelectedBank =[[banksModel alloc] init];
    selectLocationDelivery=[[DeliveryLocationPayment alloc] init];
    

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [self endPointPaymentMode];
    });
    
    [super viewWillAppear:YES];
}
#pragma mark - Endpoints
-(void)endPointPaymentMode{

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:_recipient.country_id forKey:@"idCountry"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
   // [payTypeManager startRequestWithURL:ViamericasGetPaymentMode withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasGetPaymentMode andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                    NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                    
                    NSDictionary *json=[NSJSONSerialization
                                        JSONObjectWithData:data
                                        options:NSJSONReadingMutableLeaves
                                        error:nil];
                    
                    if ([json isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *response = (NSDictionary*)json;
                         if ([[response objectForKey:kIDError] intValue] == 0) {
                                   NSArray *paymentMode = response[@"paymentModes"];
                                   for (NSDictionary* key in paymentMode) {
                                       if ([[key objectForKey:@"paymentModeId"] isEqualToString:@"a"]) {
                                           self->lbl_delivery_home.hidden =NO;
                                           self->radiobutton_homedelivery.hidden=NO;
                                           if ([self->PaymentModeId isEqualToString:@""]) {
                                               self->PaymentModeId =@"a";
                                               [self touch_radio_home_delivery:self];
                                           }
                                       }else if ([[key objectForKey:@"paymentModeId"] isEqualToString:@"c"]) {
                                           
                                           self->lbl_deposit.hidden =NO;
                                           [self->lbl_deposit setUserInteractionEnabled:YES];

                                           UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touch_radio_deposit:)];
                                           [self->lbl_deposit addGestureRecognizer:gesture];
                                           self->radiobutton_deposit.hidden=NO;
                                           if ([self->PaymentModeId isEqualToString:@""]) {
                                               self->PaymentModeId =@"c";
                                               [self touch_radio_deposit:self];
                                               
                                           }
                                       }else if ([[key objectForKey:@"paymentModeId"] isEqualToString:@"p"]) {
                                           self->lbl_cash_payment.hidden =NO;
                                           [self->lbl_cash_payment setUserInteractionEnabled:YES];
                                           UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touch_radio_cash:)];
                                           [self->lbl_cash_payment addGestureRecognizer:gesture];
                                           self->radiobutton_cashPikup.hidden=NO;
                                           if ([self->PaymentModeId isEqualToString:@""]) {
                                               self->PaymentModeId =@"p";
                                               [self touch_radio_cash:self];
                                           }
                                       }
                                   }
                               } else {
                                   [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
                               }
                    }
                }];
            });
        });
    [self unLockView];
}
//- (void)endPointpaymentLocations
//{
//    paymentLocations = [[pootEngine alloc] init];
//    [paymentLocations setDelegate:self];
//    [paymentLocations setShowComments:developing];
//
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//
//    [params setObject:PaymentModeId forKey:@"payMode"];
//    [params setObject:itemSelectedCountryCity.idCity forKey:@"idCity"];
//    [params setObject:itemSelectedCountry.idCountry forKey:@"idCountry"];
//
//    NSString *post = nil;
//    NSError *JSONError;
//    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
//    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
//    post = [NSString stringWithFormat:@"json=%@", JSONString];
//
//    [paymentLocations startRequestWithURL:ViamericasGetLocations withPost:post];
//
//    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
//}
-(void)endPointAttach{

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:_recipient.id_recipient forKey:@"recipient_id"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
  //  [attachrecipientManager startRequestWithURL:ViamericasGetAttachBank withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
           

                [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasGetAttachBank andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                    NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                    
                    NSDictionary *json=[NSJSONSerialization
                                        JSONObjectWithData:data
                                        options:NSJSONReadingMutableLeaves
                                        error:nil];
                    
                    if ([json isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *response = (NSDictionary*)json;
                       if ([[response objectForKey:kIDError] intValue] == 0) {
                                   
                                   NSArray  *itemJsonBank=[response objectForKey:@"banks"];
                                   
                                   if ([itemJsonBank count] == 0) {
                                       [self AlertviewCustom:@"Información" Message:@"Este beneficiario no cuenta con cuentas bancarias selecciona otra opción"];
                                   }else{
                                       self->listAttachBank =[[NSMutableArray alloc] init];
                                       for (NSDictionary* key in itemJsonBank) {
                                           
                                           banksModel *itemAttach = [[banksModel alloc] init];
                                           itemAttach.bank_account_id =[key objectForKey:@"bank_account_id"];
                                           itemAttach.bank_account_number =[key objectForKey:@"bank_account_number"];
                                           itemAttach.bank_account_type =[key objectForKey:@"bank_account_type"];
                                           itemAttach.bank_account_type_name =[key objectForKey:@"bank_account_type_name"];
                                           itemAttach.nick_name =[key objectForKey:@"nick_name"];
                                           itemAttach.payment_network_id =[key objectForKey:@"payment_network_id"];
                                           itemAttach.payment_network_name =[key objectForKey:@"payment_network_name"];
                                           itemAttach.recipient_id =[key objectForKey:@"recipient_id"];
                                           itemAttach.routing_number =[key objectForKey:@"routing_number"];
                                           itemAttach.sender_id =[key objectForKey:@"sender_id"];
                                           
                                           [self->listAttachBank addObject:itemAttach];
                                       }
                                       self->itemSelectedCountry= nil;
                                       self->itemSelectedCountryCity= nil;
                                       self->itemSelectedCountryState= nil;
                                       [self.TableViewAttachBank reloadData];
                                       self.TableViewAttachBank.hidden=NO;
                                   }
                                   
                               } else {
                                   [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
                               }
                    }
                }];
            });
             
        });
    [self unLockView];
}
-(void)endPointCountry{
    self->itemSelectedCountryCity= nil;
 //   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        self->countryManager = [[pootEngine alloc] init];
//        [self->countryManager setDelegate:self];
//        [self->countryManager setShowComments:developing];
//
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:@"NO" forKey:@"operation"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
       // [self->countryManager startRequestWithURL:ViamericasGetCountries withPost:post];
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                  

                    [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasGetCountries andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                        NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                        
                        NSDictionary *json=[NSJSONSerialization
                                            JSONObjectWithData:data
                                            options:NSJSONReadingMutableLeaves
                                            error:nil];
                        
                        if ([json isKindOfClass:[NSDictionary class]]) {
                            NSDictionary *response = (NSDictionary*)json;
                            ///////////////////// Raul Mendez ID_SENDER
                            if ([[response objectForKey:kIDError] intValue] == 0) {
                                
                                self->objCountrys = [[NSMutableArray alloc] init];
                                NSArray * ArrayCountrys =[response objectForKey:@"countrys"];
                                NSMutableArray *listCountryString = [[NSMutableArray alloc] init];
                                for (int i = 0; i<[ArrayCountrys count]; i++ ) {
                                    
                                    
                                    countrysModel *itemCountry = [[countrysModel alloc] init];
                                    itemCountry.idCountry=[ArrayCountrys[i] objectForKey:@"idCountry"];
                                    itemCountry.nameCountry=[ArrayCountrys[i] objectForKey:@"nameCountry"];
                                    
                                    [self->objCountrys addObject:itemCountry];
                                    [listCountryString addObject:itemCountry.nameCountry];
                                }
                                self.downPickerCountry = [[DownPicker alloc] initWithTextField:self->countryText withData:listCountryString];
                                [self.downPickerCountry setPlaceholder:@"Pais"];
                                [self.downPickerCountry setPlaceholderWhileSelecting:@"Pais"];
                                int x =0;
                                for (countrysModel *itemCountry in self->objCountrys) {
                                    if (![itemCountry.idCountry isEqualToString:self->_recipient.country_id]) {
                                        [self.downPickerCountry setSelectedIndex:0];
                                        
                                    }else{
                                        [self.downPickerCountry setSelectedIndex:x];
                                        [self country_Selected:nil];
                                        //[self endPointState:[objCountrys objectAtIndex:[self.downPickerCountry selectedIndex]]];
                                        break;
                                    }
                                    x++;
                                }
                                [self.downPickerCountry addTarget:self action:@selector(country_Selected:) forControlEvents:UIControlEventValueChanged];
                                
                                self.downPickerState = [[DownPicker alloc] initWithTextField:self->stateText withData:@[@""]];
                                [self.downPickerState setPlaceholder:@"Estado"];
                                [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
                                
                                self.downPickerCity = [[DownPicker alloc] initWithTextField:self->cityText withData:@[@""]];
                                [self.downPickerCity setPlaceholder:@"Ciudad"];
                                [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
                                
                                                                            
                                                                        
                            } else {
                                [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
                            }
                        }
                    }];
                });
            });
          [self unLockView];
  //  });
}

-(void)endPointState:(countrysModel *) model{
     self->itemSelectedCountryCity= nil;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        self->stateManager = [[pootEngine alloc] init];
//        [self->stateManager setDelegate:self];
//        [self->stateManager setShowComments:developing];
//
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:model.idCountry forKey:@"idCountry"];
        [params setObject:@"NO" forKey:@"operation"];
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
      //  [self->stateManager startRequestWithURL:ViamericasGetStates withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{

                    [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasGetStates andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                        NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                        
                        NSDictionary *json=[NSJSONSerialization
                                            JSONObjectWithData:data
                                            options:NSJSONReadingMutableLeaves
                                            error:nil];
                        
                        if ([json isKindOfClass:[NSDictionary class]]) {
                            NSDictionary *response = (NSDictionary*)json;
                             if ([[response objectForKey:kIDError] intValue] == 0) {
                                       
                                       NSArray * ArrayStates =[response objectForKey:@"countryStates"];
                                       if ([ArrayStates count] == 0) {
                                           stateModel *itemState = [[stateModel alloc] init];
                                           [self->objCountrysState addObject:itemState];
                                           
                                           self.downPickerState = [[DownPicker alloc] initWithTextField:self->stateText withData:@[@""]];
                                           [self.downPickerState setPlaceholder:@"Estado"];
                                           [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
                                           [self.downPickerState setSelectedIndex:0];
                                           
                                           [self AlertviewCustom:@"Informacion" Message:@"Este pais no cuenta con estados disponibles"];
                                       }
                                       if ([ArrayStates count] == 1) {
                                           [self city_Selected:nil];
                                       }
                                       
                                 self->objCountrysState = [[NSMutableArray alloc] init];
                                       NSMutableArray *listStateString = [[NSMutableArray alloc] init];
                                       
                                       for (NSDictionary* keyState in ArrayStates) {
                                           stateModel *itemState = [[stateModel alloc] init];
                                           itemState.idState =[keyState objectForKey:@"idState"];
                                           itemState.nameState =[keyState objectForKey:@"nameState"];
                                           
                                           [self->objCountrysState addObject:itemState];
                                           [listStateString addObject:itemState.nameState];
                                       }
                                 self.downPickerState = [[DownPicker alloc] initWithTextField:self->stateText withData:listStateString];
                                       [self.downPickerState setPlaceholder:@"Estado"];
                                       [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
                                       int x =0;
                                 for (stateModel *itemstate in self->objCountrysState) {
                                     if (![itemstate.idState isEqualToString:self->_recipient.state_id]) {
                                               [self.downPickerState setSelectedIndex:0];
                                               
                                           }else{
                                               [self.downPickerState setSelectedIndex:x];
                                               [self state_Selected:nil];
                                               break;
                                           }
                                           x++;
                                       }
                                       [self.downPickerState addTarget:self action:@selector(state_Selected:) forControlEvents:UIControlEventValueChanged];
                                       
                                       
                                 self.downPickerCity = [[DownPicker alloc] initWithTextField:self->cityText withData:@[@""]];
                                       [self.downPickerCity setPlaceholder:@"Ciudad"];
                                       [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
                                       [self.downPickerCity setSelectedIndex:0];
                                 
                                       self->itemSelectedCountryCity= nil;
                                       
                                   } else {
                                       [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
                                   }
                                 
                        }
                    }];
                });
            });
         [self unLockView];
    });
     
}
-(void)endPointCity:(stateModel *) model{
     self->itemSelectedCountryCity= nil;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        self->cityManager = [[pootEngine alloc] init];
//        [self->cityManager setDelegate:self];
//        [self->cityManager setShowComments:developing];
//
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:self->itemSelectedCountry.idCountry forKey:@"idCountry"];
        [params setObject:self->itemSelectedCountryState.idState forKey:@"idState"];
        [params setObject:@"NO" forKey:@"operation"];
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        //[self->cityManager startRequestWithURL:ViamericasGetCities withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                  

                    [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasGetCities andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                        NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                        
                        NSDictionary *json=[NSJSONSerialization
                                            JSONObjectWithData:data
                                            options:NSJSONReadingMutableLeaves
                                            error:nil];
                        
                        if ([json isKindOfClass:[NSDictionary class]]) {
                            NSDictionary *response = (NSDictionary*)json;
                            if ([[response objectForKey:kIDError] intValue]==0) {
                                NSArray * ArrayCity =[response objectForKey:@"city"];
                                
                                if ([ArrayCity count] == 0) {
                                    cityModel *itemCity = [[cityModel alloc] init];
                                    [self->objCountrysCity addObject:itemCity];
                                    // [self AlertviewCustom:@"Informacion" Message:@"Este pais no cuenta con estados disponibles"];
                                }
                                
                                self->objCountrysCity = [[NSMutableArray alloc] init];
                                NSMutableArray *listCityString = [[NSMutableArray alloc] init];
                                for (NSDictionary* keyCity in ArrayCity) {
                                    cityModel *itemCity = [[cityModel alloc] init];
                                    itemCity.idCity =[keyCity objectForKey:@"idCity"];
                                    itemCity.nameCity =[keyCity objectForKey:@"nameCity"];
                                    
                                    [self->objCountrysCity addObject:itemCity];
                                    [listCityString addObject:itemCity.nameCity];
                                }
                                
                                self.downPickerCity = [[DownPicker alloc] initWithTextField:self->cityText withData:listCityString];
                                [self.downPickerCity setPlaceholder:@"Ciudad"];
                                [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
                                int x =0;
                                for (cityModel *itemcity in self->objCountrysCity) {
                                    if (![itemcity.idCity isEqualToString:self->_recipient.city_id]) {
                                         [self.downPickerCity setSelectedIndex:0];
                                          [self city_Selected:nil];
                                        
                                    }else{
                                        [self.downPickerCity setSelectedIndex:x];
                                        [self city_Selected:nil];
                                        break;
                                    }
                                    x++;
                                }
                                [self.downPickerCity addTarget:self action:@selector(city_Selected:) forControlEvents:UIControlEventValueChanged];
                                
                                
                            } else {
                                [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
                            }
                        }
                    }];
                });
            });
         [self unLockView];
    });
}
-(void)managerFailed:(pootEngine *)manager withKey:(NSString *)key errorCode:(pootEngineErrorCode)error{
    [self unLockView];
}
//- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
//{
//    if (json == nil) {
//        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Server returns nil"];
//        return;
//    }
//    ////////////////////////////////      END POINT PAYMENT MODE      ///////////////////////////////////
//    if (manager == payTypeManager) {
//        NSMutableDictionary *response = (NSMutableDictionary*)json;
//        response = [self cleanDictionary:response];
//        
//        if ([[response objectForKey:kIDError] intValue] == 0) {
//            NSArray *paymentMode = response[@"paymentModes"];
//            for (NSDictionary* key in paymentMode) {
//                if ([[key objectForKey:@"paymentModeId"] isEqualToString:@"a"]) {
//                    lbl_delivery_home.hidden =NO;
//                    radiobutton_homedelivery.hidden=NO;
//                    if ([PaymentModeId isEqualToString:@""]) {
//                        PaymentModeId =@"a";
//                        [self touch_radio_home_delivery:self];
//                    }
//                }else if ([[key objectForKey:@"paymentModeId"] isEqualToString:@"c"]) {
//                    
//                    lbl_deposit.hidden =NO;
//                    [lbl_deposit setUserInteractionEnabled:YES];
//
//                    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touch_radio_deposit:)];
//                    [lbl_deposit addGestureRecognizer:gesture];
//                    radiobutton_deposit.hidden=NO;
//                    if ([PaymentModeId isEqualToString:@""]) {
//                        PaymentModeId =@"c";
//                        [self touch_radio_deposit:self];
//                        
//                    }
//                }else if ([[key objectForKey:@"paymentModeId"] isEqualToString:@"p"]) {
//                    lbl_cash_payment.hidden =NO;
//                    [lbl_cash_payment setUserInteractionEnabled:YES];
//                    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touch_radio_cash:)];
//                    [lbl_cash_payment addGestureRecognizer:gesture];
//                    radiobutton_cashPikup.hidden=NO;
//                    if ([PaymentModeId isEqualToString:@""]) {
//                        PaymentModeId =@"p";
//                        [self touch_radio_cash:self];
//                    }
//                }
//            }
//        } else {
//            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
//        }
//        [self unLockView];
//    }
//    ////////////////////////////////      END POINT COUNTRY        ///////////////////////////////////
//    if (manager == countryManager) {
//        NSMutableDictionary *response = (NSMutableDictionary*)json;
//        response = [self cleanDictionary:response];
//        
//        if ([[response objectForKey:kIDError] intValue] == 0) {
//            
//            objCountrys = [[NSMutableArray alloc] init];
//            NSArray * ArrayCountrys =[response objectForKey:@"countrys"];
//            NSMutableArray *listCountryString = [[NSMutableArray alloc] init];
//            for (int i = 0; i<[ArrayCountrys count]; i++ ) {
//                
//                
//                countrysModel *itemCountry = [[countrysModel alloc] init];
//                itemCountry.idCountry=[ArrayCountrys[i] objectForKey:@"idCountry"];
//                itemCountry.nameCountry=[ArrayCountrys[i] objectForKey:@"nameCountry"];
//                
//                [objCountrys addObject:itemCountry];
//                [listCountryString addObject:itemCountry.nameCountry];
//            }
//            self.downPickerCountry = [[DownPicker alloc] initWithTextField:countryText withData:listCountryString];
//            [self.downPickerCountry setPlaceholder:@"Pais"];
//            [self.downPickerCountry setPlaceholderWhileSelecting:@"Pais"];
//            int x =0;
//            for (countrysModel *itemCountry in objCountrys) {
//                if (![itemCountry.idCountry isEqualToString:_recipient.country_id]) {
//                    [self.downPickerCountry setSelectedIndex:0];
//                    
//                }else{
//                    [self.downPickerCountry setSelectedIndex:x];
//                    [self country_Selected:nil];
//                    //[self endPointState:[objCountrys objectAtIndex:[self.downPickerCountry selectedIndex]]];
//                    break;
//                }
//                x++;
//            }
//            [self.downPickerCountry addTarget:self action:@selector(country_Selected:) forControlEvents:UIControlEventValueChanged];
//            
//            self.downPickerState = [[DownPicker alloc] initWithTextField:stateText withData:@[@""]];
//            [self.downPickerState setPlaceholder:@"Estado"];
//            [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
//            
//            self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:@[@""]];
//            [self.downPickerCity setPlaceholder:@"Ciudad"];
//            [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
//            
//        } else {
//            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
//        }
//        [self unLockView];
//    }
//    ////////////////////////////////      END POINT STATE        ///////////////////////////////////
//    if (manager == stateManager) {
//        NSMutableDictionary *response = (NSMutableDictionary*)json;
//        response = [self cleanDictionary:response];
//        
//        if ([[response objectForKey:kIDError] intValue] == 0) {
//            
//            NSArray * ArrayStates =[response objectForKey:@"countryStates"];
//            if ([ArrayStates count] == 0) {
//                stateModel *itemState = [[stateModel alloc] init];
//                [objCountrysState addObject:itemState];
//                
//                self.downPickerState = [[DownPicker alloc] initWithTextField:stateText withData:@[@""]];
//                [self.downPickerState setPlaceholder:@"Estado"];
//                [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
//                [self.downPickerState setSelectedIndex:0];
//                
//                [self AlertviewCustom:@"Informacion" Message:@"Este pais no cuenta con estados disponibles"];
//                
//                
//            }
//            if ([ArrayStates count] == 1) {
//                [self city_Selected:nil];
//            }
//            
//            objCountrysState = [[NSMutableArray alloc] init];
//            NSMutableArray *listStateString = [[NSMutableArray alloc] init];
//            
//            for (NSDictionary* keyState in ArrayStates) {
//                stateModel *itemState = [[stateModel alloc] init];
//                itemState.idState =[keyState objectForKey:@"idState"];
//                itemState.nameState =[keyState objectForKey:@"nameState"];
//                
//                [objCountrysState addObject:itemState];
//                [listStateString addObject:itemState.nameState];
//            }
//            self.downPickerState = [[DownPicker alloc] initWithTextField:stateText withData:listStateString];
//            [self.downPickerState setPlaceholder:@"Estado"];
//            [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
//            int x =0;
//            for (stateModel *itemstate in objCountrysState) {
//                if (![itemstate.idState isEqualToString:_recipient.state_id]) {
//                    [self.downPickerState setSelectedIndex:0];
//                    
//                }else{
//                    [self.downPickerState setSelectedIndex:x];
//                    [self state_Selected:nil];
//                    break;
//                }
//                x++;
//            }
//            [self.downPickerState addTarget:self action:@selector(state_Selected:) forControlEvents:UIControlEventValueChanged];
//            
//            
//            self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:@[@""]];
//            [self.downPickerCity setPlaceholder:@"Ciudad"];
//            [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
//            [self.downPickerCity setSelectedIndex:0];
//            
//        } else {
//            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
//        }
//        [self unLockView];
//
//    }
//    ////////////////////////////////      END POINT CIUDAD        ///////////////////////////////////
//    
//    if (manager == cityManager) {
//        NSMutableDictionary *response = (NSMutableDictionary*)json;
//        response = [self cleanDictionary:response];
//        
//        if ([[response objectForKey:kIDError] intValue]==0) {
//            NSArray * ArrayCity =[response objectForKey:@"city"];
//            
//            if ([ArrayCity count] == 0) {
//                cityModel *itemCity = [[cityModel alloc] init];
//                [objCountrysCity addObject:itemCity];
//                // [self AlertviewCustom:@"Informacion" Message:@"Este pais no cuenta con estados disponibles"];
//            }
//            
//            objCountrysCity = [[NSMutableArray alloc] init];
//            NSMutableArray *listCityString = [[NSMutableArray alloc] init];
//            for (NSDictionary* keyCity in ArrayCity) {
//                cityModel *itemCity = [[cityModel alloc] init];
//                itemCity.idCity =[keyCity objectForKey:@"idCity"];
//                itemCity.nameCity =[keyCity objectForKey:@"nameCity"];
//                
//                [objCountrysCity addObject:itemCity];
//                [listCityString addObject:itemCity.nameCity];
//            }
//            
//            self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:listCityString];
//            [self.downPickerCity setPlaceholder:@"Ciudad"];
//            [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
//            int x =0;
//            for (cityModel *itemcity in objCountrysCity) {
//                if (![itemcity.idCity isEqualToString:_recipient.city_id]) {
//                    // [self.downPickerCity setSelectedIndex:0];
//                    //  [self city_Selected:nil];
//                    
//                }else{
//                    [self.downPickerCity setSelectedIndex:x];
//                    [self city_Selected:nil];
//                    break;
//                }
//                x++;
//            }
//            [self.downPickerCity addTarget:self action:@selector(city_Selected:) forControlEvents:UIControlEventValueChanged];
//            
//            
//        } else {
//            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
//        }
//        [self unLockView];
//
//    }
//    ////////////////////////////////      END POINT ATTACH        ///////////////////////////////////
//    if (manager == attachrecipientManager) {
//        NSMutableDictionary *response = (NSMutableDictionary*)json;
//        response = [self cleanDictionary:response];
//        if ([[response objectForKey:kIDError] intValue] == 0) {
//            
//            NSArray  *itemJsonBank=[response objectForKey:@"banks"];
//            
//            if ([itemJsonBank count] == 0) {
//                [self AlertviewCustom:@"Información" Message:@"Este beneficiario no cuenta con cuentas bancarias selecciona otra opción"];
//            }else{
//                listAttachBank =[[NSMutableArray alloc] init];
//                for (NSDictionary* key in itemJsonBank) {
//                    
//                    banksModel *itemAttach = [[banksModel alloc] init];
//                    itemAttach.bank_account_id =[key objectForKey:@"bank_account_id"];
//                    itemAttach.bank_account_number =[key objectForKey:@"bank_account_number"];
//                    itemAttach.bank_account_type =[key objectForKey:@"bank_account_type"];
//                    itemAttach.bank_account_type_name =[key objectForKey:@"bank_account_type_name"];
//                    itemAttach.nick_name =[key objectForKey:@"nick_name"];
//                    itemAttach.payment_network_id =[key objectForKey:@"payment_network_id"];
//                    itemAttach.payment_network_name =[key objectForKey:@"payment_network_name"];
//                    itemAttach.recipient_id =[key objectForKey:@"recipient_id"];
//                    itemAttach.routing_number =[key objectForKey:@"routing_number"];
//                    itemAttach.sender_id =[key objectForKey:@"sender_id"];
//                    
//                    [listAttachBank addObject:itemAttach];
//                }
//                itemSelectedCountry= nil;
//                itemSelectedCountryCity= nil;
//                itemSelectedCountryState= nil;
//                [self.TableViewAttachBank reloadData];
//                self.TableViewAttachBank.hidden=NO;
//            }
//            
//        } else {
//            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
//        }
//        [self unLockView];
//
//    }
//    if (manager == paymentLocations) {
//        NSMutableDictionary *response = (NSMutableDictionary*)json;
//        response = [self cleanDictionary:response];
//        if ([[response objectForKey:kIDError] intValue] == 0) {
//            
//            NSArray  *itemLocations=[response objectForKey:@"locations"];
//            objDeliveryPayment =[[NSMutableArray alloc] init];
//            
//            if ([itemLocations count] == 0) {
//                DeliveryLocationPayment *itemdelivery =[[DeliveryLocationPayment alloc] init];
//                [objDeliveryPayment addObject:itemdelivery];
//                
//                [self AlertviewCustom:@"Informacion" Message:@"Esta seleccion no cuenta con cuenta bancaria ! Intenta Nuevamente !"];
//                
//                [self.TableViewPaymentNetwork reloadData];
//            }else{
//                for (NSDictionary* key in itemLocations) {
//                    DeliveryLocationPayment *itemdelivery =[[DeliveryLocationPayment alloc] init];
//                    itemdelivery.addressPaymentLocation =[key objectForKey:@"addressPaymentLocation"];
//                    itemdelivery.businessHours =[key objectForKey:@"businessHours"];
//                    itemdelivery.idPaymentLocation =[key objectForKey:@"idPaymentLocation"];
//                    itemdelivery.latitude =[key objectForKey:@"latitude"];
//                    itemdelivery.longitude =[key objectForKey:@"longitude"];
//                    itemdelivery.namePayer =[key objectForKey:@"namePayer"];
//                    itemdelivery.namePaymentLocation =[key objectForKey:@"namePaymentLocation"];
//                    
//                    [objDeliveryPayment addObject:itemdelivery];
//                }
//                [self.TableViewPaymentNetwork reloadData];
//                self.TableViewPaymentNetwork.hidden=NO;
//            }
//            
//            
//        } else {
//            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
//        }
//    }
//    [self unLockView];
//
//}
#pragma mark UITableViewController LoadItems
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.TableViewAttachBank)
        return 55;
    else if(tableView == self.TableViewPaymentNetwork)
        return 55;
    return 0;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.TableViewAttachBank)
        return listAttachBank.count;
    else if(tableView == self.TableViewPaymentNetwork)
        return objDeliveryPayment.count;
    return 0;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell ;
    @try
    {
        if(tableView == self.TableViewAttachBank){
            if (listAttachBank == nil || [listAttachBank count] == 0) {
                NSString *CellIdentifier = @"ViewCellBankDelivery";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                return cell;
            }
            
            banksModel *item =[listAttachBank objectAtIndex:indexPath.row];
            NSString *CellIdentifier = @"ViewCellBankDelivery";
            ViewCellBankDelivery *cell = (ViewCellBankDelivery *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil) {
                NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ViewCellBankDelivery" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            [cell.lbl_title_payment setText:item.payment_network_name];
            [cell.lbl_name_payment setText:item.bank_account_type_name];
            
            [cell.lbl_number_account_payment setText:[NSString stringWithFormat:@"***** %@", [item.bank_account_number substringWithRange:NSMakeRange(item.bank_account_number.length-4, [item.bank_account_number length]==item.bank_account_number.length?4:4)]]];
            
            return cell;
        }
        if(tableView == self.TableViewPaymentNetwork){
            if (objDeliveryPayment == nil || [objDeliveryPayment count] == 0) {
                NSString *CellIdentifier = @"ViewCellPaymentLocation";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                return cell;
            }
            
            DeliveryLocationPayment *item =[objDeliveryPayment objectAtIndex:indexPath.row];
            NSString *CellIdentifier = @"ViewCellPaymentLocation";
            ViewCellPaymentNetwok *cell = (ViewCellPaymentNetwok *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil) {
                NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ViewCellPaymentLocation" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            [cell.lbl_title_payment setText:item.namePayer];
            [cell.lbl_sub_title_payment setText:item.addressPaymentLocation];
            return cell;
            
        }
        
        return cell;
    } @catch (NSException *exception)
    {
        // NSLog( @"NameException: %@", exception.name);
        // NSLog( @"ReasonException: %@", exception.reason );
        return cell;
    }
    @finally
    {
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.TableViewAttachBank){
        itemSelectedBank =[listAttachBank objectAtIndex:indexPath.row];
    }else  if(tableView == self.TableViewPaymentNetwork){
        selectLocationDelivery=[objDeliveryPayment objectAtIndex:indexPath.row];
        selectLocationDelivery.idCountry=itemSelectedCountry.idCountry;
        selectLocationDelivery.idCity=itemSelectedCountryCity.idCity;
        selectLocationDelivery.idState=itemSelectedCountryState.idState;
    }
}
#pragma mark - PickerView
-(void)country_Selected:(id)dp {
    itemSelectedCountry  =[objCountrys objectAtIndex:[self.downPickerCountry selectedIndex]];
    [self endPointState:itemSelectedCountry];
    self.TableViewPaymentNetwork.hidden =YES;
    self.TableViewAttachBank.hidden =YES;
}
-(void)state_Selected:(id)dp {
    itemSelectedCountryState  = [objCountrysState objectAtIndex:[self.downPickerState selectedIndex]];
    [self endPointCity:itemSelectedCountryState];
    self.TableViewPaymentNetwork.hidden =YES;
    self.TableViewAttachBank.hidden =YES;
}
-(void)city_Selected:(id)dp {
    //objCountrysCity =[self.downPickerCity selectedIndex];
    if (objCountrysCity !=  nil) {
            itemSelectedCountryCity= [objCountrysCity objectAtIndex:[self.downPickerCity selectedIndex]];
    }
   // [self endPointpaymentLocations];
}
- (IBAction)touch_backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)AlertviewCustom:(NSString *)title Message:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        
        
    }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)touch_radio_deposit:(id)sender {
    [exchangeRateLabel setText:@"Selecciona cuenta de banco"];
    PaymentModeId =@"c";
    
    ////////////// VALIDACION PARA CONSUMIR EL MISMO ENPOINT QUE LOS OTROS DOS SERVICIOS
    //    countryText.hidden =NO;
    //    stateText.hidden =NO;
    //    cityText.hidden=NO;
    //
    //    [self endPointCountry];
    
    ////////////// VALIDACION PARA CONSUMIR EL  ENPOINT ATTACHBANK
    
    countryText.hidden =YES;
    stateText.hidden =YES;
    cityText.hidden=YES;
    countinuebank.hidden=NO;
    continuePayment.hidden=YES;
    
    [self endPointAttach];
    
    radiobutton_deposit.selected =true;
    radiobutton_cashPikup.selected =false;
    radiobutton_homedelivery.selected =false;
    
    self.TableViewPaymentNetwork.hidden =YES;
    self.TableViewAttachBank.hidden =NO;
    
}

- (IBAction)touch_radio_cash:(id)sender {
    
    [exchangeRateLabel setText:@"Selecciona cuenta de banco"];
    PaymentModeId =@"p";
    
    countryText.hidden =NO;
    stateText.hidden =NO;
    cityText.hidden=NO;
    
    countinuebank.hidden=YES;
    continuePayment.hidden=NO;
    
    radiobutton_cashPikup.selected =true;
    radiobutton_deposit.selected =false;
    radiobutton_homedelivery.selected =false;
    self.TableViewPaymentNetwork.hidden =NO;
    self.TableViewAttachBank.hidden =YES;
    
    [self endPointCountry];
}

- (IBAction)touch_radio_home_delivery:(id)sender {
    [exchangeRateLabel setText:@"Entrega a domicilio"];
    PaymentModeId =@"a";
    
    countryText.hidden =NO;
    stateText.hidden =NO;
    cityText.hidden=NO;
    
    countinuebank.hidden=YES;
    continuePayment.hidden=NO;
    
    radiobutton_homedelivery.selected =true;
    radiobutton_deposit.selected =false;
    radiobutton_cashPikup.selected =false;
    
    self.TableViewPaymentNetwork.hidden =NO;
    self.TableViewAttachBank.hidden =YES;
    
    [self endPointCountry];
}

- (IBAction)touchBeginCountryText:(id)sender {
    //    if ([PaymentModeId isEqualToString:@""]) {
    //           [self AlertviewCustom:@"Informacion" Message:@"Selecciona un metodo de pago para continuar"];
    //           return;
    //       }
    //    [self.view endEditing:YES];
}
- (IBAction)touch_continue:(id)sender {
    
    if ([PaymentModeId isEqualToString:@""]) {
        [self AlertviewCustom:@"Informacion" Message:@"Selecciona un metodo de pago"];
        return;
    }
    if (itemSelectedCountryCity== nil) {
        if ([PaymentModeId isEqualToString:@"p"]) {
             [self AlertviewCustom:@"Informacion" Message:@"Selecciona una ciudad para continuar"];
            return;
        }else{
            if (itemSelectedBank.bank_account_id == nil) {
                [self AlertviewCustom:@"Informacion" Message:@"Selecciona un cuenta de banco"];
                return;
            }else{
                [self performSegueWithIdentifier:@"segue_verifi_payment" sender:self];
            }
        }
    }else{
        [self performSegueWithIdentifier:@"segue_selected_location_payment" sender:self];
    }
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"segue_verifi_payment"]){
        VerifyPaymentController *next = (VerifyPaymentController *)[segue destinationViewController];
        [next setRecipient:_recipient];
        [next setItemBank:itemSelectedBank];
        [next setLocationDelivery:selectLocationDelivery];
        [next setPaymentModeId:PaymentModeId];
    }
    if([[segue identifier] isEqualToString:@"segue_selected_location_payment"]){
           SelectedLocationPaymentController *next = (SelectedLocationPaymentController *)[segue destinationViewController];
           [next setRecipient:_recipient];
           [next setSelectedCity:itemSelectedCountryCity];
           [next setSelectedCountry:itemSelectedCountry];
           [next setSelectedState:itemSelectedCountryState];
           [next setPaymentModeId:PaymentModeId];
       }
}

@end
