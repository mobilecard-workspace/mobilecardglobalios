//
//  RecipientController.h
//  Mobilecard
//
//  Created by Raul Mendez on 22/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewCellRecipient.h"
#import "RecipientModel.h"
#import "mT_commonController.h"
#import "TermsConditionsController.h"

#import "EditRecipientsController.h"
#import "AddRecipientController.h"
#import "DeliveryPaymentController.h"
#import "sender_Ctrl.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecipientController : mT_commonController<pootEngineDelegate,UITableViewDelegate,UITableViewDataSource,addRecipientDelegate,editRecipientDelegate>{ 
    
      NSMutableArray * listRecipient;
      RecipientModel *itemSelected;
}

- (IBAction)touch_backButton:(id)sender;
- (IBAction)touch_addrecipient:(id)sender;

@property(nonatomic, assign) BOOL statusTerms;
@property(nonatomic, assign) int Profile;
@property (nonatomic, strong) IBOutlet UITableView *TableViewRecipient;

@end

NS_ASSUME_NONNULL_END
