//
//  TermsConditionsController.h
//  Mobilecard
//
//  Created by Raul Mendez on 21/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewCellRecipient.h"
#import "mT_commonController.h"
#import "WebViewController.h"
#import "RecipientController.h"

#import "sender_Ctrl.h"
NS_ASSUME_NONNULL_BEGIN
@protocol TermsConditionDelegate <NSObject>

@required
- (void) TermsConditionResult:(BOOL)result;

@end
@interface TermsConditionsController : mT_commonController<pootEngineDelegate,senderDelegate>{
    
    __weak IBOutlet UILabel *lbl_texto;
    
    __weak IBOutlet UITextView *privacy_policy;
    
    __weak IBOutlet UITextView *terms_condition;
    
     int valueTerms;
    int valueProfile;
}
@property (nonatomic, assign) id <TermsConditionDelegate> obj_condition;
- (IBAction)touch_acceptTerms:(id)sender;


@end

NS_ASSUME_NONNULL_END
