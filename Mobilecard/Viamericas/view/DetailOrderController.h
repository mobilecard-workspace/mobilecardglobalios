//
//  DetailOrderController.h
//  Mobilecard
//
//  Created by Raul Mendez on 08/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "orderPaymentModel.h"
#import "exchangeRateModel.h"
#import "RecipientModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailOrderController : UIViewController{
    
    __weak IBOutlet UILabel *info_recipient;
    __weak IBOutlet UILabel *mountSed;
    __weak IBOutlet UILabel *numberFolio;
    
    __weak IBOutlet UILabel *lbl_date;
    __weak IBOutlet UILabel *idTransacction;

    
}
- (IBAction)touch_understood:(id)sender;

@property (nonatomic, retain)  orderPaymentModel *OrderPayment;
@property (nonatomic, retain)  exchangeRateModel *exchangeRate;
@property (nonatomic, retain)  RecipientModel *recipìent;

@end

NS_ASSUME_NONNULL_END
