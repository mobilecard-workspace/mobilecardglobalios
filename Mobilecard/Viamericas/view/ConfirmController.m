//
//  ConfirmController.m
//  Mobilecard
//
//  Created by Raul Mendez on 07/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "ConfirmController.h"

@interface ConfirmController (){
    
    pootEngine *orderPaymentManager;
    pootEngine *orderFeeManager;
}

@end

@implementation ConfirmController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    
    NSMutableDictionary *attributesDictionary = [NSMutableDictionary dictionary];
    [attributesDictionary setObject:[UIFont systemFontOfSize:14] forKey:NSFontAttributeName];
    //[attributesDictionary setObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:@"Al hacer clic en confirmar, acepta el Vianex Términos y condiciones y a la Política de privacidad." attributes:attributesDictionary];

    [attributedString enumerateAttribute:(NSString *) NSFontAttributeName
                                inRange:NSMakeRange(0, [attributedString length])
                                options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired
                             usingBlock:^(id value, NSRange range, BOOL *stop) {
                                 NSLog(@"Attribute: %@, %@", value, NSStringFromRange(range));
                                }];

     NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:@"Términos y condiciones y a la Política de privacidad"];

     [attributedString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:NSUnderlineStyleDouble]
                            range:NSMakeRange(45, attributedStr.length)];

     [attributedString addAttribute:NSForegroundColorAttributeName
                            value:[UIColor blackColor]
                            range:NSMakeRange(44,attributedStr.length)];

     TermsText.attributedText = attributedString;//_attriLbl (of type UILabel) added in storyboard
    TermsText.userInteractionEnabled = YES;
           UITapGestureRecognizer *ta = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ActionTerms:)];
           [TermsText addGestureRecognizer:ta];
           ta.numberOfTapsRequired = 1;
           ta.cancelsTouchesInView = NO;
    
    [self endPointOrderFee];
}
-(void)endPointOrderPaymenty{
    btn_confirm.enabled=NO;
//    orderPaymentManager = [[pootEngine alloc] init];
//    [orderPaymentManager setDelegate:self];
//    [orderPaymentManager setShowComments:developing];
//
//
//
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
    [params setObject:_recipient.id_recipient forKey:@"idRecipient"];
    [params setObject:_PaymentModeId forKey:@"modePayReciever"];
    [params setObject:_itemBank.bank_account_id==nil?@"":_itemBank.bank_account_id forKey:@"bankAccountIdReceiver"];
    [params setObject:_LocationDelivery.idCountry==nil?_recipient.country_id:_LocationDelivery.idCountry forKey:@"idCountryReciever"];
    [params setObject:_LocationDelivery.idCity==nil?_recipient.city_id:_LocationDelivery.idCity forKey:@"idCityRecivier"];
    [params setObject:_LocationDelivery.idState==nil?_recipient.state_id:_LocationDelivery.idState forKey:@"idStateReceiver"];
    [params setObject:_LocationDelivery.idPaymentLocation==nil?_itemBank.payment_network_id:_LocationDelivery.idPaymentLocation forKey:@"idPaymentLocation"];
    [params setObject:_exchangeRate.amount forKey:@"amount"];
    [params setObject:[NSString stringWithFormat:@"%f",totalFees] forKey:@"comision"];
    [params setObject:_recipient.address1 forKey:@"addressReceiver"];
    [params setObject:@"com.ironbit.mc.dev" forKey:@"appVersion"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"longitude"];
    [params setObject:@"iOS" forKey:@"phoneType"];
    [params setObject:@"" forKey:@"placeName"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
    
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    NSLog(@"POST value -> %@",post);
    //[orderPaymentManager startRequestWithURL:ViamericasCreateNewOrder withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasCreateNewOrder andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                [self unLockView];
                NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                
                NSDictionary *json=[NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:NSJSONReadingMutableLeaves
                                    error:nil];
                
                if ([json isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *response = (NSDictionary*)json;
                    if ([[response objectForKey:kIDError] intValue] == 0) {
                        
                        self->itemOrder = [[orderPaymentModel alloc] init];
                        self->itemOrder.idBranch =[response objectForKey:@"idBranch"];
                        self->itemOrder.idReceiver =[response objectForKey:@"idReceiver"];
                        self->itemOrder.idSender =[response objectForKey:@"idSender"];
                        self->itemOrder.idTransaction =[response objectForKey:@"idTransaction"];
                        self->itemOrder.transactionDate =[response objectForKey:@"transactionDate"];
                        self->itemOrder.totalPayReceiver =[response objectForKey:@"totalPayReceiver"];
                        self->itemOrder.passwordReceiver =[response objectForKey:@"passwordReceiver"];
                        self->itemOrder.totalReceiver =[self validTotalPayment];
                        self->itemOrder.idBitacora =[response objectForKey:@"idBitacora"];
                        self->itemOrder.idBitacoraTransfer =[response objectForKey:@"idBitacoraTransfer"];
                        
                        [self performSegueWithIdentifier:@"segue_result" sender:self];
                        
                    } else {
                        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
                    }
                     btn_confirm.enabled=YES;
                }
            }];
        });
    });
   
}
-(void)endPointOrderFee{
    
    orderFeeManager = [[pootEngine alloc] init];
    [orderFeeManager setDelegate:self];
    [orderFeeManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:_PaymentModeId forKey:@"idModePay"];
    [params setObject:_LocationDelivery.idPaymentLocation==nil?_itemBank.payment_network_id:_LocationDelivery.idPaymentLocation  forKey:@"idBranchPay"];
    [params setObject:_exchangeRate.amount forKey:@"netAmount"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [orderFeeManager startRequestWithURL:ViamericasGetOrderFees withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
-(NSString *) validTotalPayment{
    
    double result;
    result= [_exchangeRate.amount doubleValue] + transferFee+transferTaxes;
    return [NSString stringWithFormat:@"$%.2f USD",result];
    
}
-(NSString *) validateModePayment:(NSString *) exchanged{
    NSString* valueExchanged=@"";
    if ([exchanged isEqualToString:@"c"] || [exchanged isEqualToString:@"C"]) {
        valueExchanged=@"Deposito bancario";
    } else if ([exchanged isEqualToString:@"a"] || [exchanged isEqualToString:@"A"]) {
        valueExchanged=@"Entrega a domicilio";
    }else if ([exchanged isEqualToString:@"p"] || [exchanged isEqualToString:@"P"]) {
        valueExchanged=@"Retiro de efectivo";
    }
    return valueExchanged;
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    [locationManager stopUpdatingLocation];
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error){
        
        if (error == nil && [placemarks count]>0) {
            
        }
    }];
}
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
 
    if (json == nil) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Server returns nil"];
        return;
    }
    if (manager == orderFeeManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            NSArray * ArrayFee =[response objectForKey:@"orders"];
            for (NSDictionary* key in ArrayFee) {
                if ([[key objectForKey:@"customerFixFee"] isEqualToString:@"customer_fix_fee"]) {
                    transferFee=[[key objectForKey:@"customerPercentageFee"] doubleValue];
                }
                if ([[key objectForKey:@"customerFixFee"] isEqualToString:@"customer_percentage_fee"]) {
                    transferTaxes=[[key objectForKey:@"customerPercentageFee"] doubleValue];
                }
            }
            totalFees=[[response objectForKey:@"totalFee"] doubleValue];
            [lbl_name_recipient setText:_recipient.firstName];
            [lbl_phone_recipient setText:_recipient.phone1];
            [lbl_email_recipient setText:_recipient.email];
           [lbl_amount_transfer setText:[NSString stringWithFormat:@"$%.2f USD", [_exchangeRate.amount floatValue]]];
          
            [lbl_saldo_recipient setText:_exchangeRate.balance];
            if (_LocationDelivery.namePayer == nil ||[_LocationDelivery.namePayer isEqualToString:@""]) {
                [lbl_network_payment setText:_itemBank.bank_account_type_name];
            }else{
                [lbl_network_payment setText:_LocationDelivery.namePayer];
            }
            [lbl_method_payment setText:[self validateModePayment:_PaymentModeId]];
            [lbl_exchangedRate setText:[NSString stringWithFormat:@"$1 USD = %.2f MXN",_exchangeRate.exchangeRate]];
            [lbl_transferFee setText:[NSString stringWithFormat:@"$%.2f USD",transferFee]];
            [lbl_transferTaxes setText:[NSString stringWithFormat:@"$%.2f USD",transferTaxes]];
            [lbl_total_pay setText:[self validTotalPayment]];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
        
    }
    if (manager == orderPaymentManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            itemOrder = [[orderPaymentModel alloc] init];
            itemOrder.idBranch =[response objectForKey:@"idBranch"];
            itemOrder.idReceiver =[response objectForKey:@"idReceiver"];
            itemOrder.idSender =[response objectForKey:@"idSender"];
            itemOrder.idTransaction =[response objectForKey:@"idTransaction"];
            itemOrder.transactionDate =[response objectForKey:@"transactionDate"];
            itemOrder.totalPayReceiver =[response objectForKey:@"totalPayReceiver"];
            itemOrder.passwordReceiver =[response objectForKey:@"passwordReceiver"];
            itemOrder.totalReceiver =[self validTotalPayment];
            itemOrder.idBitacora =[response objectForKey:@"idBitacora"];
            itemOrder.idBitacoraTransfer =[response objectForKey:@"idBitacoraTransfer"];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
        
       
        
         [self performSegueWithIdentifier:@"segue_result" sender:self];

    }
       [self unLockView];
}
- (void) ActionTerms: (UITapGestureRecognizer *) gr {
    // Define actions here
    [self performSegueWithIdentifier:@"segue_webview" sender:self];
}
-(void)AlertviewCustom:(NSString *)title Message:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        
        
    }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)touch_backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)touch_confirm:(id)sender {
    
    [self endPointOrderPaymenty];
    
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"segue_result"]){
        DetailOrderController *next = (DetailOrderController *)[segue destinationViewController];
        [next setOrderPayment:itemOrder];
        [next setExchangeRate:_exchangeRate];
        [next setRecipìent:_recipient];
    }
    if([[segue identifier] isEqualToString:@"segue_terms"]){
           WebViewController *next = (WebViewController *)[segue destinationViewController];
           [next setType_term:1];
       }
}

@end
