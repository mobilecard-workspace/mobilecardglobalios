//
//  AttachBankController.m
//  Mobilecard
//
//  Created by Raul Mendez on 27/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define Second 0.6


#import "AttachBankController.h"

@interface AttachBankController (){
    pootEngine *countryManager;
    pootEngine *paymentManager;
    pootEngine *addattachManager;
    pootEngine *deleteattachManager;
    pootEngine *updateattachManager;
}

@end

@implementation AttachBankController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self initializePickersForView: self.navigationController.view];
//
//    [routing_number setUpTextFieldAs:textFieldTypeCellphone];
//    [routing_number setRequired:NO];
//    [banckAccount setUpTextFieldAs:textFieldTypeCellphone];
////    [nickName setUpTextFieldAs:textFieldTypeName];
////    [nickName setRequired:NO];
////    [bankAccountType setUpTextFieldAs:textFieldTypeRequiredCombo];
////    [bankAccountType setRequired:YES];
////    [countryText setUpTextFieldAs:textFieldTypeRequiredCombo];
////    [countryText setRequired:YES];
////
////
//    [self addValidationTextField:banckAccount];
//    [self addValidationTextField:bankAccountType];
//    [self addValidationTextField:countryText];
//
//    [self addValidationTextFieldsToDelegate];
    
    listMethodPayments = [[NSMutableArray alloc] init];
    listPaymentString = [[NSMutableArray alloc] init];
    listLocationPayments = [[NSMutableArray alloc] init];
    
    paymentsTypeModel *itempayment1 =[[paymentsTypeModel alloc] init];
    itempayment1.idTypePayment=@"c";
    itempayment1.nameTypePyment=@"Checking";
    [listMethodPayments addObject:itempayment1];
    [listPaymentString addObject:itempayment1.nameTypePyment];
    
    paymentsTypeModel *itempayment2 =[[paymentsTypeModel alloc] init];
    itempayment2.idTypePayment=@"c";
    itempayment2.nameTypePyment=@"Saving";
    [listMethodPayments addObject:itempayment2];
    [listPaymentString addObject:itempayment2.nameTypePyment];

    
    
    
    self.downPickerBankType = [[DownPicker alloc] initWithTextField:bankAccountType withData:listPaymentString];
    [self.downPickerBankType setPlaceholder:@"Tipo de cuenta bancaria"];
    [self.downPickerBankType setPlaceholderWhileSelecting:@"Tipo de cuenta bancaria"];
    [self.downPickerBankType setSelectedIndex:0];
    [self.downPickerBankType addTarget:self action:@selector(PaymentType_Selected:) forControlEvents:UIControlEventValueChanged];
    [self PaymentType_Selected:nil];

    self.downPickerCountry = [[DownPicker alloc] initWithTextField:countryText withData:@[@""]];
    [self.downPickerCountry setPlaceholder:@"Pais"];
    [self.downPickerCountry setPlaceholderWhileSelecting:@"Pais"];
    

    
    if (_statusEdit) {
        btn_addBank.hidden=YES;
        btn_editBank.hidden=NO;
        [self loadItems];
     //   [self endPointCountry];
    }else{
        self.navigationItem.rightBarButtonItem = nil;
        btn_addBank.hidden=NO;
        btn_editBank.hidden=YES;
    }
}
-(void) loadItems{
    [routing_number setText:_itemBank.routing_number];
    [banckAccount setText:_itemBank.bank_account_number];
    [nickName setText:_itemBank.nick_name];
    [bankAccountType setText:_itemBank.bank_account_type_name];
}
#pragma mark - Ciclelive
-(void)viewWillAppear:(BOOL)animated
{
    self.TableViewPaymentNetwork.delegate = self;
    self.TableViewPaymentNetwork.dataSource = self;
    [super viewWillAppear:YES];
}
#pragma mark - PickerView
-(void)PaymentType_Selected:(id)dp {
    paymentSelect = [ listMethodPayments objectAtIndex:[self.downPickerBankType selectedIndex]];
    if (itemSelectedCountry == nil) {
        [self endPointPayments:paymentSelect Country:_recipient.country_id];
    }else{
        [self endPointPayments:paymentSelect Country:itemSelectedCountry.idCountry];
    }
    [self endPointCountry];
}
-(void)country_Selected:(id)dp {
   itemSelectedCountry  =[objCountrys objectAtIndex:[self.downPickerCountry selectedIndex]];
   [self endPointPayments:paymentSelect Country:itemSelectedCountry.idCountry];
}

-(void)endPointPayments:(paymentsTypeModel *)model Country:(NSString *) countryid{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->paymentManager = [[pootEngine alloc] init];
        [self->paymentManager setDelegate:self];
        [self->paymentManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:countryid forKey:@"idCountry"];
        [params setObject:model.idTypePayment forKey:@"idPaymentMode"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->paymentManager startRequestWithURL:ViamericasGetPaymentLocation withPost:post];
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}
-(void)endPointCountry{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->countryManager = [[pootEngine alloc] init];
        [self->countryManager setDelegate:self];
        [self->countryManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:@"NO" forKey:@"operation"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->countryManager startRequestWithURL:ViamericasGetCountries withPost:post];
        //    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}
-(void)endPointAddAttchPayment{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->addattachManager = [[pootEngine alloc] init];
        [self->addattachManager setDelegate:self];
        [self->addattachManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:self->_recipient.id_recipient forKey:@"recipient_id"];
        [params setObject:self->routing_number.text forKey:@"routing_number"];
        [params setObject:self->banckAccount.text forKey:@"bank_account_number"];
        if ([self->paymentSelect.nameTypePyment isEqualToString:@"Checking"]) {
            [params setObject:@"c" forKey:@"bank_account_type"];
        }else{
            [params setObject:@"s" forKey:@"bank_account_type"];
        }
        [params setObject:self->itemlocationSelect.idBranchPayment forKey:@"payment_network_id"];
        [params setObject:self->nickName.text forKey:@"nick_name"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->addattachManager startRequestWithURL:ViamericasAttachBankRecipient withPost:post];
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}
-(void)endPointUpdateAttchPayment{
    updateattachManager = [[pootEngine alloc] init];
    [updateattachManager setDelegate:self];
    [updateattachManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:_recipient.id_recipient forKey:@"recipient_id"];
    [params setObject:routing_number.text forKey:@"routing_number"];
    [params setObject:banckAccount.text forKey:@"bank_account_number"];
    if ([self->paymentSelect.nameTypePyment isEqualToString:@"Checking"]) {
               [params setObject:@"c" forKey:@"bank_account_type"];
           }else{
               [params setObject:@"s" forKey:@"bank_account_type"];
           }
    [params setObject:itemlocationSelect.idBranchPayment forKey:@"payment_network_id"];
    [params setObject:nickName.text forKey:@"nick_name"];
    [params setObject:_itemBank.bank_account_id forKey:@"bank_account_id"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [updateattachManager startRequestWithURL:ViamericasUpdateAttachBankRecipient withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
-(void)endPointDeleteAttchPayment{
    deleteattachManager = [[pootEngine alloc] init];
    [deleteattachManager setDelegate:self];
    [deleteattachManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:_recipient.id_recipient forKey:@"recipient_id"];
    [params setObject:_itemBank.bank_account_id forKey:@"bank_account_id"];
    [params setObject:_itemBank.sender_id forKey:@"sender_id"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [deleteattachManager startRequestWithURL:ViamericasDeleteAttachBankRecipient withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

#pragma mark pootEngine
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    if (json == nil) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Server returns nil"];
        return;
    }
    ////////////////////////////////      END POINT COUNTRY        ///////////////////////////////////
              if (manager == countryManager) {
                  NSMutableDictionary *response = (NSMutableDictionary*)json;
                  response = [self cleanDictionary:response];
                              
                  if ([[response objectForKey:kIDError] intValue] == 0) {
                      
                      objCountrys = [[NSMutableArray alloc] init];
                      NSArray * ArrayCountrys =[response objectForKey:@"countrys"];
                      NSMutableArray *listCountryString = [[NSMutableArray alloc] init];
                      for (int i = 0; i<[ArrayCountrys count]; i++ ) {
                          

                          countrysModel *itemCountry = [[countrysModel alloc] init];
                          itemCountry.idCountry=[ArrayCountrys[i] objectForKey:@"idCountry"];
                          itemCountry.nameCountry=[ArrayCountrys[i] objectForKey:@"nameCountry"];
                          
                          [objCountrys addObject:itemCountry];
                          [listCountryString addObject:itemCountry.nameCountry];
                      }
                      self.downPickerCountry = [[DownPicker alloc] initWithTextField:countryText withData:listCountryString];
                      [self.downPickerCountry setPlaceholder:@"Pais"];
                      [self.downPickerCountry setPlaceholderWhileSelecting:@"Pais"];
                      int x =0;
                      for (countrysModel *itemCountry in objCountrys) {
                          if (![itemCountry.idCountry isEqualToString:_recipient.country_id]) {
                              [self.downPickerCountry setSelectedIndex:0];
                              
                          }else{
                               [self.downPickerCountry setSelectedIndex:0];
                              if (_statusEdit) {
                               // [self.downPickerCountry setSelectedIndex:x];
                               // [self country_Selected:nil];
                                //[self endPointState:[objCountrys objectAtIndex:[self.downPickerCountry selectedIndex]]];
                              }
                           
                              break;
                          }
                          x++;
                      }
                      [self.downPickerCountry addTarget:self action:@selector(country_Selected:) forControlEvents:UIControlEventValueChanged];
                      

                  } else {
                      [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
                  }
              }
    if (manager == paymentManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
             if (itemSelectedCountry != nil) {
                NSArray * Arraylacationspayment =[response objectForKey:@"locations"];
                if ([Arraylacationspayment count] == 0) {
                    listLocationPayments = [[NSMutableArray alloc] init];
                    [self.TableViewPaymentNetwork reloadData];
                }
                for (NSDictionary * itemlocation in Arraylacationspayment) {
                    
                    locationsPaymentModel *itemlocationpaymentModel =[[locationsPaymentModel alloc] init];
                    itemlocationpaymentModel.idBranchPayment =[itemlocation objectForKey:@"idbranchpaymentlocation"];
                    itemlocationpaymentModel.logo_name =[itemlocation objectForKey:@"logoName"];
                    itemlocationpaymentModel.name_group =[itemlocation objectForKey:@"nameGroup"];
                    itemlocationpaymentModel.number_of_location =[itemlocation objectForKey:@"numberOfLocations"];
                    itemlocationpaymentModel.phoneBranch =[itemlocation objectForKey:@"phoneBranch"];
                    itemlocationpaymentModel.routing =[[itemlocation objectForKey:@"routingRequired"] boolValue];
                    
                    [listLocationPayments addObject:itemlocationpaymentModel];
                }
                [self.TableViewPaymentNetwork reloadData];
            }
            
            
        } else {
            listLocationPayments = [[NSMutableArray alloc] init];
            [self.TableViewPaymentNetwork reloadData];
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    if (manager == addattachManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            [self.navigationController popViewControllerAnimated:YES];
            [_obj_bank addbankResult:YES];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    if (manager == updateattachManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            [self.navigationController popViewControllerAnimated:YES];
            [_obj_bank bankUpdateResult:YES];
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    if (manager == deleteattachManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            [self.navigationController popViewControllerAnimated:YES];
            [_obj_bank banktDeleteResult:YES];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    
    
}

#pragma mark UITableViewController LoadItems
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listLocationPayments.count;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell ;
    @try
    {
        if (listLocationPayments == nil || [listLocationPayments count] == 0) {
            NSString *CellIdentifier = @"ViewCellPaymentLocation";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            return cell;
        }
        
        locationsPaymentModel *item =[listLocationPayments objectAtIndex:indexPath.row];
        NSString *CellIdentifier = @"ViewCellPaymentLocation";
        ViewCellPaymentNetwok *cell = (ViewCellPaymentNetwok *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ViewCellPaymentLocation" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.lbl_title_payment setText:item.name_group];
      //  [cell.lbl_sub_title_payment setText:item.];
        
        return cell;
    } @catch (NSException *exception)
    {
        // NSLog( @"NameException: %@", exception.name);
        // NSLog( @"ReasonException: %@", exception.reason );
        return cell;
    }
    @finally
    {
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    itemlocationSelect =[listLocationPayments objectAtIndex:indexPath.row];
}

-(void)AlertviewCustom:(NSString *)title Message:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        
        
    }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
      NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];

      NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];

      return [string isEqualToString:filtered];
}
- (IBAction)touch_backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)touch_add_attachpayment:(id)sender {
    if ([banckAccount.text isEqualToString:@""]) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Ingresa una cuanta bancaria"];
    }
    if ([nickName.text isEqualToString:@""]) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Ingresa alias a tu cuenta bancaria"];
    }
    if (itemlocationSelect == nil) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Elige una cuenta de banco"];
    }else{
        if ([self validateFieldsInArray:[self getValidationTextFields]]) {
            [self endPointAddAttchPayment];
        }
    }
}

- (IBAction)touch_edit_bank:(id)sender {
    if ([banckAccount.text isEqualToString:@""]) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Ingresa una cuanta bancaria"];
    }
    if ([nickName.text isEqualToString:@""]) {
           [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Ingresa alias a tu cuenta bancaria"];
       }
    if (itemlocationSelect == nil) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Elige una cuenta de banco"];
    }else{
        if ([self validateFieldsInArray:[self getValidationTextFields]]) {
            [self endPointUpdateAttchPayment];
            
        }
    }
}

- (IBAction)touch_editingAlias:(id)sender {
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    if ([nickName.text rangeOfCharacterFromSet:set].location == NSNotFound)
    {
    }
    else
    {
        NSArray *arr = [nickName.text componentsSeparatedByCharactersInSet:
                  [NSCharacterSet characterSetWithCharactersInString:@"0123456789!·$%%&/()=?¿@"]];
        NSLog(@"%@", arr);
        [nickName setText:arr[0]];
    }
    
    
}

- (IBAction)touch_delete_bank:(id)sender {
    UIAlertController * alert = [UIAlertController
                                    alertControllerWithTitle:NSLocalizedString(@"Información", nil)
                                    message:@"¿Deseas eliminar este cuenta de banco?"
                                    preferredStyle:UIAlertControllerStyleAlert];
       
       UIAlertAction* yesButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
              [self endPointDeleteAttchPayment];

           
           
       }];
       UIAlertAction* noButton = [UIAlertAction
                                  actionWithTitle:@"Cancelar"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
           
           
       }];
       [alert addAction:noButton];
       [alert addAction:yesButton];
       
       [self presentViewController:alert animated:YES completion:nil];
}
@end
