//
//  SelectedLocationPaymentController.m
//  Mobilecard
//
//  Created by Raul Mendez on 19/01/20.
//  Copyright © 2020 David Poot. All rights reserved.
//

#import "SelectedLocationPaymentController.h"

@interface SelectedLocationPaymentController ()

@end

@implementation SelectedLocationPaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_searchBar setTintColor:[UIColor darkMCOrangeColor]];
    [self endPointpaymentLocations];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.TableViewPaymentNetwork.delegate = self;
    self.TableViewPaymentNetwork.dataSource = self;
    selectLocationDelivery=[[DeliveryLocationPayment alloc] init];
    
    [super viewWillAppear:YES];
}
- (IBAction)touch_backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) endPointpaymentLocations{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:_PaymentModeId forKey:@"payMode"];
    [params setObject:_SelectedCity.idCity forKey:@"idCity"];
    [params setObject:_SelectedCountry.idCountry forKey:@"idCountry"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasGetLocations andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                [self unLockView];
                NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                
                NSDictionary *json=[NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:NSJSONReadingMutableLeaves
                                    error:nil];
                
                if ([json isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *response = (NSDictionary*)json;
                    if ([[response objectForKey:kIDError] intValue] == 0) {
                        
                        NSArray  *itemLocations=[response objectForKey:@"locations"];
                        self->objDeliveryPayment =[[NSMutableArray alloc] init];
                        
                        if ([itemLocations count] == 0) {
                            DeliveryLocationPayment *itemdelivery =[[DeliveryLocationPayment alloc] init];
                            [self->objDeliveryPayment addObject:itemdelivery];
                            
                            [self AlertviewCustom:@"Informacion" Message:@"Esta seleccion no cuenta con cuenta bancaria ! Intenta Nuevamente !"];
                            
                            [self.TableViewPaymentNetwork reloadData];
                        }else{
                            for (NSDictionary* key in itemLocations) {
                                DeliveryLocationPayment *itemdelivery =[[DeliveryLocationPayment alloc] init];
                                itemdelivery.addressPaymentLocation =[key objectForKey:@"addressPaymentLocation"];
                                itemdelivery.businessHours =[key objectForKey:@"businessHours"];
                                itemdelivery.idPaymentLocation =[key objectForKey:@"idPaymentLocation"];
                                itemdelivery.latitude =[key objectForKey:@"latitude"];
                                itemdelivery.longitude =[key objectForKey:@"longitude"];
                                itemdelivery.namePayer =[key objectForKey:@"namePayer"];
                                itemdelivery.namePaymentLocation =[key objectForKey:@"namePaymentLocation"];
                                
                                [self->objDeliveryPayment addObject:itemdelivery];
                                self->DeliveryPaymentLocal = [[NSMutableArray alloc] initWithArray:self->objDeliveryPayment];
                            }
                            [self.TableViewPaymentNetwork reloadData];
                        }
                        
                    } 
                }
            }];
        });
    });
}
#pragma mark - Search Bar Delegate
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        DeliveryPaymentLocal = [[NSMutableArray alloc] initWithArray:objDeliveryPayment];
        [self.TableViewPaymentNetwork reloadData];
        return;
    }
    NSPredicate *predicate = [NSPredicate predicateWithSearch:searchText searchTerm:@"searchTerms"];
    DeliveryPaymentLocal = [[NSMutableArray alloc] initWithArray:[objDeliveryPayment filteredArrayUsingPredicate:predicate]];
    [self.TableViewPaymentNetwork reloadData];
    searchBar.showsCancelButton =YES;
  //  [searchBar resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar performSelector: @selector(resignFirstResponder)
    withObject: nil
    afterDelay: 0.1];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar performSelector: @selector(resignFirstResponder)
    withObject: nil
    afterDelay: 0.1];
}
#pragma mark UITableViewController LoadItems
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [DeliveryPaymentLocal count];
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell ;
    @try
    {
        if(tableView == self.TableViewPaymentNetwork){
            if (DeliveryPaymentLocal == nil || [DeliveryPaymentLocal count] == 0) {
                NSString *CellIdentifier = @"ViewCellPaymentLocation";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                return cell;
            }
            
            DeliveryLocationPayment *item =[DeliveryPaymentLocal objectAtIndex:indexPath.row];
            NSString *CellIdentifier = @"ViewCellPaymentLocation";
            ViewCellPaymentNetwok *cell = (ViewCellPaymentNetwok *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell == nil) {
                NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ViewCellPaymentLocation" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            [cell.lbl_title_payment setText:item.namePayer];
            [cell.lbl_sub_title_payment setText:item.addressPaymentLocation];
            return cell;
            
        }
        
        return cell;
    } @catch (NSException *exception)
    {
        // NSLog( @"NameException: %@", exception.name);
        // NSLog( @"ReasonException: %@", exception.reason );
        return cell;
    }
    @finally
    {
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
     if (DeliveryPaymentLocal == nil || [DeliveryPaymentLocal count] == 0) {
   
    selectLocationDelivery=[objDeliveryPayment objectAtIndex:indexPath.row];
         
     }else{
         
         selectLocationDelivery = [DeliveryPaymentLocal objectAtIndex: indexPath.row];
     }
    
    
    
    selectLocationDelivery.idCountry=_SelectedCountry.idCountry;
    selectLocationDelivery.idCity=_SelectedCity.idCity;
    selectLocationDelivery.idState=_SelectedState.idState;
    [self.view endEditing:YES];
    
}
-(void)AlertviewCustom:(NSString *)title Message:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        
        
    }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)touch_continue:(id)sender {
    if (selectLocationDelivery.namePayer == nil) {
        [self AlertviewCustom:@"Informacion" Message:@"Selecciona un cuenta de banco"];
        return;
    }else{
        [self performSegueWithIdentifier:@"segue_verifi_payment" sender:self];
    }
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"segue_verifi_payment"]){
        VerifyPaymentController *next = (VerifyPaymentController *)[segue destinationViewController];
        [next setRecipient:_recipient];
        [next setLocationDelivery:selectLocationDelivery];
        [next setPaymentModeId:_PaymentModeId];
    }
}
@end
