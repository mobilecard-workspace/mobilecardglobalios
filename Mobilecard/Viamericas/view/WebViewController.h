//
//  WebViewController.h
//  Mobilecard
//
//  Created by Raul Mendez on 23/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface WebViewController :  UIViewController{
    
    __weak IBOutlet UIWebView *webView;
    
    
}

@property (nonatomic, assign) NSInteger type_term;


@end

NS_ASSUME_NONNULL_END
