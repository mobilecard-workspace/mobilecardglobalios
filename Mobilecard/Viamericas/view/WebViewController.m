//
//  WebViewController.m
//  Mobilecard
//
//  Created by Raul Mendez on 23/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString * Url = @"";
    switch (_type_term) {
        case 0:
            Url =@"https://s3.amazonaws.com/cdn.govianex.com/privacy-policy/www/viamericas_privacy-policy.pdf";
            break;
        case 1:
            Url =@"https://s3.amazonaws.com/cdn.govianex.com/terms-and-conditions/www/viamericas_terms-and-conditions.pdf";
            break;
    }
   NSURL *targetURL = [NSURL URLWithString:Url];
   NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
   [webView loadRequest:request];
}
 
- (IBAction)touch_backButton:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
@end
