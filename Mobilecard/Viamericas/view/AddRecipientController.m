//
//  AddRecipientController.m
//  Mobilecard
//
//  Created by Raul Mendez on 24/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//
#define Second 0.6
#import "AddRecipientController.h"

@interface AddRecipientController (){
    pootEngine *countryManager;
    pootEngine *stateManager;
    pootEngine *cityManager;
    pootEngine *recipientManager;
}

@end

@implementation AddRecipientController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializePickersForView: self.navigationController.view];
    
    [firstText setUpTextFieldAs:textFieldTypeName];
    [middlenameText setUpTextFieldAs:textFieldTypeName];
    [middlenameText setRequired:NO];
    
    [lastNameText setUpTextFieldAs:textFieldTypeLastName];
    
    [emailText setUpTextFieldAs:textFieldTypeEmail];
    [emailText setRequired:NO];
    
    [addressText setUpTextFieldAs:textFieldTypeAddress];
    [countryText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [cityText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [birthdateText setUpTextFieldAs:textFieldTypeBirthdate];
    [birthdateText setRequired:NO];
    [zipText setUpTextFieldAs:textFieldTypeGeneralNumericNoRequired];
    [zipText setRequired:NO];
    
    [self addValidationTextField:firstText];
    [self addValidationTextField:middlenameText];
    [self addValidationTextField:lastNameText];
    [self addValidationTextField:emailText];
    [self addValidationTextField:phoneText];
    [self addValidationTextField:addressText];
    [self addValidationTextField:countryText];
    [self addValidationTextField:stateText];
    [self addValidationTextField:cityText];
    [self addValidationTextField:birthdateText];
    [self addValidationTextField:phoneText];
    [self addValidationTextField:zipText];
    
    [self addValidationTextFieldsToDelegate];
    
    [self endPointCountry];
    
}
#pragma mark - Endpoints
-(void)endPointCountry{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->countryManager = [[pootEngine alloc] init];
        [self->countryManager setDelegate:self];
        [self->countryManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:@"NO" forKey:@"operation"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->countryManager startRequestWithURL:ViamericasGetCountries withPost:post];
        //  [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}
-(void)endPointState:(countrysModel *) model{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->stateManager = [[pootEngine alloc] init];
        [self->stateManager setDelegate:self];
        [self->stateManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:model.idCountry forKey:@"idCountry"];
        [params setObject:@"NO" forKey:@"operation"];
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->stateManager startRequestWithURL:ViamericasGetStates withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}
-(void)endPointCity:(stateModel *) model{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->cityManager = [[pootEngine alloc] init];
        [self->cityManager setDelegate:self];
        [self->cityManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:self->itemSelectedCountry.idCountry forKey:@"idCountry"];
        [params setObject:self->itemSelectedCountryState.idState forKey:@"idState"];
        [params setObject:@"NO" forKey:@"operation"];
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->cityManager startRequestWithURL:ViamericasGetCities withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}

-(void)endPointAddRecipient{
    recipientManager = [[pootEngine alloc] init];
    [recipientManager setDelegate:self];
    [recipientManager setShowComments:developing];
    
    NSString *post = nil;
    NSError *JSONError;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *birthdate = [dateFormatter dateFromString:[birthdateText text]];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[[lastNameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                               locale:[NSLocale systemLocale]] forKey:@"last_name"];
    [params setObject:[[middlenameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                 locale:[NSLocale systemLocale]] forKey:@"middleName"];
    [params setObject:[zipText text] forKey:@"zip_code"];
    [params setObject:[[addressText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                              locale:[NSLocale systemLocale]] forKey:@"address1"];
    [params setObject:@"" forKey:@"address2"];
    [params setObject:[[birthdateText text] length]==0?@"":[dateFormatter stringFromDate:birthdate] forKey:@"birthDate"];
    [params setObject:[emailText text] forKey:@"email"];
    [params setObject:[[firstText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                            locale:[NSLocale systemLocale]] forKey:@"firstName"];
    [params setObject:itemSelectedCountryCity.idCity forKey:@"idCity"];
    [params setObject:itemSelectedCountry.idCountry forKey:@"idCountry"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
    [params setObject:itemSelectedCountryState.idState forKey:@"idState"];
    [params setObject:[[lastNameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                               locale:[NSLocale systemLocale]] forKey:@"lastName"];

    //    [params setObject:[NSString stringWithFormat:@"%@+%@",[ladaText.text stringByReplacingOccurrencesOfString:@"+"
    //    withString:@""],[phoneText text] ] forKey:@"phone1"];
    [params setObject:[phoneText text] forKey:@"phone1"];
    
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [recipientManager startRequestWithURL:ViamericasCreateRecipient withPost:post];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    if (json == nil) {
        [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Server returns nil"];
        return;
    }
    ////////////////////////////////      END POINT COUNTRY        ///////////////////////////////////
    if (manager == countryManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            objCountrys = [[NSMutableArray alloc] init];
            NSArray * ArrayCountrys =[response objectForKey:@"countrys"];
            NSMutableArray *listCountryString = [[NSMutableArray alloc] init];
            for (int i = 0; i<[ArrayCountrys count]; i++ ) {
                
                
                countrysModel *itemCountry = [[countrysModel alloc] init];
                itemCountry.idCountry=[ArrayCountrys[i] objectForKey:@"idCountry"];
                itemCountry.nameCountry=[ArrayCountrys[i] objectForKey:@"nameCountry"];
                
                [objCountrys addObject:itemCountry];
                [listCountryString addObject:itemCountry.nameCountry];
            }
            self.downPickerCountry = [[DownPicker alloc] initWithTextField:countryText withData:listCountryString];
            [self.downPickerCountry setPlaceholder:@"Pais"];
            [self.downPickerCountry setPlaceholderWhileSelecting:@"Pais"];
            [self.downPickerCountry addTarget:self action:@selector(country_Selected:) forControlEvents:UIControlEventValueChanged];
            
            self.downPickerState = [[DownPicker alloc] initWithTextField:stateText withData:@[@""]];
            [self.downPickerState setPlaceholder:@"Estado"];
            [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
            
            self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:@[@""]];
            [self.downPickerCity setPlaceholder:@"Ciudad"];
            [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    ////////////////////////////////      END POINT STATE        ///////////////////////////////////
    if (manager == stateManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            NSArray * ArrayStates =[response objectForKey:@"countryStates"];
            objCountrysState = [[NSMutableArray alloc] init];
            NSMutableArray *listStateString = [[NSMutableArray alloc] init];
            
            for (NSDictionary* keyState in ArrayStates) {
                stateModel *itemState = [[stateModel alloc] init];
                itemState.idState =[keyState objectForKey:@"idState"];
                itemState.nameState =[keyState objectForKey:@"nameState"];
                
                [objCountrysState addObject:itemState];
                [listStateString addObject:itemState.nameState];
            }
            self.downPickerState = [[DownPicker alloc] initWithTextField:stateText withData:listStateString];
            [self.downPickerState setPlaceholder:@"Estado"];
            [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
            [self.downPickerState addTarget:self action:@selector(state_Selected:) forControlEvents:UIControlEventValueChanged];
            
            self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:@[@""]];
            [self.downPickerCity setPlaceholder:@"Ciudad"];
            [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    
    ////////////////////////////////      END POINT CIUDAD        ///////////////////////////////////
    
    if (manager == cityManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue]==0) {
            NSArray * ArrayCity =[response objectForKey:@"city"];
            objCountrysCity = [[NSMutableArray alloc] init];
            NSMutableArray *listCityString = [[NSMutableArray alloc] init];
            for (NSDictionary* keyCity in ArrayCity) {
                cityModel *itemCity = [[cityModel alloc] init];
                itemCity.idCity =[keyCity objectForKey:@"idCity"];
                itemCity.nameCity =[keyCity objectForKey:@"nameCity"];
                
                [objCountrysCity addObject:itemCity];
                [listCityString addObject:itemCity.nameCity];
            }
            
            self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:listCityString];
            [self.downPickerCity setPlaceholder:@"Ciudad"];
            [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
            [self.downPickerCity addTarget:self action:@selector(city_Selected:) forControlEvents:UIControlEventValueChanged];
            
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    
    ////////////////////////////////      END POINT RECIPIENTS        ///////////////////////////////////
    if (manager == recipientManager ) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            [self.navigationController popViewControllerAnimated:YES];
            [_obj recipientCreationResult:YES];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    
    
}
#pragma mark - PickerView
-(void)country_Selected:(id)dp {
    itemSelectedCountry  =[objCountrys objectAtIndex:[self.downPickerCountry selectedIndex]];
    [self endPointState:itemSelectedCountry];
}
-(void)state_Selected:(id)dp {
    itemSelectedCountryState  = [objCountrysState objectAtIndex:[self.downPickerState selectedIndex]];
    [self endPointCity:itemSelectedCountryState];
}
-(void)city_Selected:(id)dp {
    
    itemSelectedCountryCity= [objCountrysCity objectAtIndex:[self.downPickerCity selectedIndex]];
}
#pragma mark - Actions Method
-(void)AlertviewCustom:(NSString *)title Message:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        
        
    }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)touch_addbank:(id)sender {
    [self performSegueWithIdentifier:@"segue_attach" sender:self];
}

- (IBAction)touch_addRecipient:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        [self endPointAddRecipient];
    }
}

- (IBAction)touch_backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
