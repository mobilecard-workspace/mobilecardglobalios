//
//  ConfirmController.h
//  Mobilecard
//
//  Created by Raul Mendez on 07/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"

#import "DeliveryLocationPayment.h"
#import "banksModel.h"
#import "RecipientModel.h"
#import "exchangeRateModel.h"
#import "orderPaymentModel.h"

#import "DetailOrderController.h"
#import "WebViewController.h"
#import <CoreLocation/CoreLocation.h>
NS_ASSUME_NONNULL_BEGIN

@interface ConfirmController : mT_commonController{
    
     __weak IBOutlet UILabel *lbl_name_recipient;
     __weak IBOutlet UILabel *lbl_phone_recipient;
     __weak IBOutlet UILabel *lbl_email_recipient;
     __weak IBOutlet UILabel *lbl_amount_transfer;
     __weak IBOutlet UILabel *lbl_saldo_recipient;
     __weak IBOutlet UILabel *lbl_network_payment;
     __weak IBOutlet UILabel *lbl_method_payment;
     __weak IBOutlet UILabel *lbl_exchangedRate;
     __weak IBOutlet UILabel *lbl_transferFee;
     __weak IBOutlet UILabel *lbl_transferTaxes;
     __weak IBOutlet UILabel *lbl_total_pay;

    __weak IBOutlet UITextView *TermsText;
    __weak IBOutlet UIButton *btn_confirm;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    
    orderPaymentModel *itemOrder;
    
    double transferFee;
    double transferTaxes;
    double totalFees;
    
    __weak IBOutlet UITextView *terms_condition;
    
}

- (IBAction)touch_confirm:(id)sender;

@property (nonatomic, assign)  NSString* IdUsuarioEndPoint;
@property (nonatomic, assign)  NSString* PaymentModeId;
@property (nonatomic, retain)  DeliveryLocationPayment *LocationDelivery;
@property (nonatomic, retain)  banksModel *itemBank;
@property (nonatomic, retain)  RecipientModel *recipient;
@property (nonatomic, retain)  exchangeRateModel *exchangeRate;


@end 

NS_ASSUME_NONNULL_END
