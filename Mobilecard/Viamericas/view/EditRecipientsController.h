//
//  EditRecipientsController.h
//  Mobilecard
//
//  Created by Raul Mendez on 27/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"
#import "attachModel.h"
#import "ViewCellAttachBank.h"
#import "banksModel.h"

#import "countrysModel.h"
#import "stateModel.h"
#import "cityModel.h"

#import "DownPicker.h"
#import "RecipientModel.h"
#import "AttachBankController.h"
NS_ASSUME_NONNULL_BEGIN
@protocol editRecipientDelegate <NSObject>

@required
- (void) recipientUpdateResult:(BOOL)result;
- (void) recipientDeleteResult:(BOOL)result;

@end
@interface EditRecipientsController : mT_commonController<pootEngineDelegate,UITableViewDelegate,UITableViewDataSource,BankDelegate>{
    
    __weak IBOutlet UITextField_Validations *firstText;
    __weak IBOutlet UITextField_Validations *middlenameText;
    __weak IBOutlet UITextField_Validations *lastNameText;
    __weak IBOutlet UITextField_Validations *secondlastText;
    __weak IBOutlet UITextField_Validations *addressText;
    __weak IBOutlet UITextField_Validations *countryText;
    __weak IBOutlet UITextField_Validations *stateText;
    __weak IBOutlet UITextField_Validations *zipText;
    __weak IBOutlet UITextField_Validations *cityText;
    __weak IBOutlet UITextField_Validations *phoneText;
    __weak IBOutlet UITextField_Validations *ladaText;
    __weak IBOutlet UITextField_Validations *emailText;
    __weak IBOutlet UITextField_Validations *birthdateText;
    
    NSMutableArray *objCountrys;
    NSMutableArray *objCountrysState;
    NSMutableArray *objCountrysCity;
    NSString *IdRecipient;
    
    countrysModel * itemSelectedCountry;
    stateModel * itemSelectedCountryState;
    cityModel * itemSelectedCountryCity;
    
    
    NSMutableArray * listAttachBank;
    banksModel *itemSelectedBank;
    BOOL statusEdit;
}
@property (strong, nonatomic) DownPicker *downPickerCountry;
@property (strong, nonatomic) DownPicker *downPickerState;
@property (strong, nonatomic) DownPicker *downPickerCity;


@property (nonatomic, strong) IBOutlet UITableView *TableViewAttachBank;
@property (nonatomic, assign) id <editRecipientDelegate> obj_edit;
@property (nonatomic, retain) RecipientModel *recipient;

- (IBAction)touch_delete_recipient:(id)sender;
- (IBAction)touch_update_recipient:(id)sender;

@end

NS_ASSUME_NONNULL_END
