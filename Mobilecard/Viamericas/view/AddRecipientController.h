//
//  AddRecipientController.h
//  Mobilecard
//
//  Created by Raul Mendez on 24/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"
#import "countrysModel.h"
#import "stateModel.h"
#import "cityModel.h"

#import "DownPicker.h"

//#import "RecipientController.h"
NS_ASSUME_NONNULL_BEGIN

@protocol addRecipientDelegate <NSObject>

@required
- (void) recipientCreationResult:(BOOL)result;

@end
@interface AddRecipientController : mT_commonController<UITextFieldDelegate>{
    
    __weak IBOutlet UITextField_Validations *firstText;
    __weak IBOutlet UITextField_Validations *middlenameText;
    __weak IBOutlet UITextField_Validations *lastNameText;
    __weak IBOutlet UITextField_Validations *addressText;
    __weak IBOutlet UITextField_Validations *countryText;
    __weak IBOutlet UITextField_Validations *stateText;
    __weak IBOutlet UITextField_Validations *zipText;
    __weak IBOutlet UITextField_Validations *cityText;
    __weak IBOutlet UITextField_Validations *phoneText;
    __weak IBOutlet UITextField_Validations *ladaText;
    __weak IBOutlet UITextField_Validations *emailText;
    __weak IBOutlet UITextField_Validations *birthdateText;
    
    
    NSMutableArray *objCountrys;
    NSMutableArray *objCountrysState;
    NSMutableArray *objCountrysCity;
    
    countrysModel * itemSelectedCountry;
    stateModel * itemSelectedCountryState;
    cityModel * itemSelectedCountryCity;
    
}

@property (strong, nonatomic) DownPicker *downPickerCountry;
@property (strong, nonatomic) DownPicker *downPickerState;
@property (strong, nonatomic) DownPicker *downPickerCity;


@property (nonatomic, assign) id <addRecipientDelegate> obj;

@property (strong, nonatomic) NSDictionary *userData;

- (IBAction)touch_addbank:(id)sender;

- (IBAction)touch_addRecipient:(id)sender;

- (IBAction)touch_backButton:(id)sender;



@end

NS_ASSUME_NONNULL_END
