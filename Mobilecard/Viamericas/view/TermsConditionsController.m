//
//  TermsConditionsController.m
//  Mobilecard
//
//  Created by Raul Mendez on 21/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "TermsConditionsController.h"
#import "CallEndPointDelegate.h"

@interface TermsConditionsController (){
 
//    pootEngine * endPointTermsCondition;
    
}
@end
@implementation TermsConditionsController

- (void)viewDidLoad {
    [super viewDidLoad];

     ///////////// Userdefault codigo para realizar hiperlinktext
//  NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:@"Google"];
//  [str addAttribute: NSLinkAttributeName value: @"http://www.google.com" range: NSMakeRange(0, str.length)];
//  hiperlink.attributedText = str;
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    privacy_policy.attributedText = [[NSAttributedString alloc] initWithString:privacy_policy.text
                                                           attributes:underlineAttribute];
    privacy_policy.textAlignment = NSTextAlignmentCenter;
    
    privacy_policy.userInteractionEnabled = YES;
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ActionPrivacy:)];
    [privacy_policy addGestureRecognizer:gr];
    gr.numberOfTapsRequired = 1;
    gr.cancelsTouchesInView = NO;
    
    terms_condition.userInteractionEnabled = YES;
       UITapGestureRecognizer *ta = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ActionTerms:)];
       [terms_condition addGestureRecognizer:ta];
       ta.numberOfTapsRequired = 1;
       ta.cancelsTouchesInView = NO;
}
#pragma mark - Endpoints
- (void) callEndPointTermsCondition{
//    endPointTermsCondition = [[pootEngine alloc] init];
//    [endPointTermsCondition setDelegate:self];
//    [endPointTermsCondition setShowComments:developing];
//
    ///////////// Userdefault para obtener idUser
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
//    [endPointTermsCondition startRequestWithURL:ViamericasacceptTermsCondition withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
          dispatch_async(dispatch_get_main_queue(), ^{
              [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasacceptTermsCondition andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                  NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];

                  NSDictionary *json=[NSJSONSerialization
                                      JSONObjectWithData:data
                                      options:NSJSONReadingMutableLeaves
                                      error:nil];

                  if ([json isKindOfClass:[NSDictionary class]]) {
                      NSDictionary *response = (NSDictionary*)json;
                      if ([response[kIDError] intValue] == 0) {
                          if ([[response objectForKey:@"userTerms"] boolValue]) {
                              [self unLockView];
//                              if ([self.obj_condition conformsToProtocol:@protocol(TermsConditionDelegate)]&&[self.obj_condition respondsToSelector:@selector(TermsConditionResult:)]) {
//                                  [self.obj_condition TermsConditionResult:YES];
//
//                                  [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
//
//                              }
                              [self callendPointProfileManager];
                          }
                      }
                  }
              }];
          });
      });
}
-(void) callendPointProfileManager{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasSenderProfile andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                
                NSDictionary *json=[NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:NSJSONReadingMutableLeaves
                                    error:nil];
                
                if ([json isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *response = (NSDictionary*)json;
                    ///////////////////// Raul Mendez ID_SENDER
                    if ([response[kIDError] intValue]!=0){
                        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"senderReg" bundle:nil];

                        UINavigationController *nav = [nextStory instantiateInitialViewController];
                        sender_Ctrl *nextView = [nav childViewControllers].firstObject;
                        [nextView setDelegate:self];
                        [self.navigationController presentViewController:nav animated:YES completion:nil];
                        
                    } else {
//                        [[NSUserDefaults standardUserDefaults] setObject:response[kUserSenderId] forKey:(NSString*)kUserSenderId];
//
//                        [self sendTransfersToOrderView];
                        [self performSegueWithIdentifier:@"segue_recipient" sender:self];

                    }
                }
            }];
        });
    });
    
}
#pragma mark - ActionMethod
- (void) senderCreationResult:(id)result{
    if (result) {
      //  [self dismissViewControllerAnimated:YES completion:^{
            [[NSUserDefaults standardUserDefaults] setObject:result forKey:(NSString*)kUserSenderId];
            [self performSegueWithIdentifier:@"segue_recipient" sender:self];
       // }];
    }else{
        [self callendPointProfileManager];
    }
}
- (void) ActionPrivacy: (UITapGestureRecognizer *) gr {
    // Define actions here
    [self performSegueWithIdentifier:@"segue_webview" sender:self];
    valueTerms = 0;
}
- (void) ActionTerms: (UITapGestureRecognizer *) gr {
    // Define actions here
    [self performSegueWithIdentifier:@"segue_webview" sender:self];
    valueTerms = 1;
}
- (IBAction)touch_acceptTerms:(id)sender {
    [self callEndPointTermsCondition];
}
- (IBAction)touch_backButton:(id)sender {
 //   [self.navigationController popViewControllerAnimated:YES];
     [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"segue_terms"]){
        WebViewController *next = (WebViewController *)[segue destinationViewController];
        [next setType_term:valueTerms];;
    }
    if([[segue identifier] isEqualToString:@"segue_recipient"]){
        RecipientController *next = (RecipientController *)[segue destinationViewController];
        [next setStatusTerms:YES];
        [next setProfile:1];
    }
}
@end
