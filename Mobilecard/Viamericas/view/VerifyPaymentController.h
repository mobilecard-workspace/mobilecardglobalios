//
//  VerifyPaymentController.h
//  Mobilecard
//
//  Created by Raul Mendez on 06/12/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"

#import "DeliveryLocationPayment.h"
#import "banksModel.h"
#import "RecipientModel.h"
#import "exchangeRateModel.h"

#import "ConfirmController.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerifyPaymentController : mT_commonController{
    
    __weak IBOutlet UILabel *lbl_name_recipient;
    __weak IBOutlet UILabel *lbl_saldo;
    
    __weak IBOutlet UITextField_Validations *balanceText;
    __weak IBOutlet UITextField_Validations *amountText;
    __weak IBOutlet UITextField_Validations *recivedText;
    
    __weak IBOutlet UIButton *InfoexchangeRate;
    __weak IBOutlet UIButton *btn_continue;
    
    double currectBalance;
    exchangeRateModel *selectedExchangeRateModel;
    NSString *IdUser;
    NSDictionary *currenBalanceArray ;
    NSDictionary *currencyArray;
}
@property (nonatomic, assign)  NSString* PaymentModeId;
@property (nonatomic, retain)  DeliveryLocationPayment *LocationDelivery;
@property (nonatomic, retain)  banksModel *itemBank;
@property (nonatomic, retain)  RecipientModel *recipient;
- (IBAction)touch_amountEditingBegin:(id)sender;

- (IBAction)touch_amountEditingEnd:(id)sender;

- (IBAction)touch_orderPayment:(id)sender;


@end

NS_ASSUME_NONNULL_END
