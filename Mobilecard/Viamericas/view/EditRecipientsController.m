//
//  EditRecipientsController.m
//  Mobilecard
//
//  Created by Raul Mendez on 27/11/19.
//  Copyright © 2019 David Poot. All rights reserved.
//
#define Second 0.0

#import "EditRecipientsController.h"

@interface EditRecipientsController (){
         pootEngine *countryManager;
         pootEngine *stateManager;
         pootEngine *cityManager;
         pootEngine *recipientManager;
         pootEngine *deleterecipientManager;
         pootEngine *attachrecipientManager;
}

@end

@implementation EditRecipientsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializePickersForView: self.navigationController.view];
    
    [firstText setUpTextFieldAs:textFieldTypeName];
    [middlenameText setUpTextFieldAs:textFieldTypeName];
    
    [lastNameText setUpTextFieldAs:textFieldTypeLastName];
    [secondlastText setUpTextFieldAs:textFieldTypeLastName];
    [secondlastText setRequired:NO];
    
    [emailText setUpTextFieldAs:textFieldTypeEmail];
    [emailText setRequired:NO];
    
    [addressText setUpTextFieldAs:textFieldTypeAddress];
    [countryText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [cityText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [birthdateText setUpTextFieldAs:textFieldTypeBirthdate];
    [birthdateText setRequired:NO];
    [zipText setUpTextFieldAs:textFieldTypeGeneralNumericNoRequired];
    [zipText setRequired:NO];
    
    [self addValidationTextField:firstText];
    [self addValidationTextField:lastNameText];
    [self addValidationTextField:secondlastText];
    [self addValidationTextField:emailText];
    [self addValidationTextField:phoneText];
    [self addValidationTextField:addressText];
    [self addValidationTextField:countryText];
    [self addValidationTextField:stateText];
    [self addValidationTextField:cityText];
    [self addValidationTextField:birthdateText];
    [self addValidationTextField:phoneText];
    [self addValidationTextField:zipText];
    
    [self addValidationTextFieldsToDelegate];

    statusEdit =FALSE;
    [self loadFieldsItems];
}
#pragma mark - Ciclelive
-(void)viewWillAppear:(BOOL)animated
{
    self.TableViewAttachBank.delegate = self;
    self.TableViewAttachBank.dataSource = self;
    [super viewWillAppear:YES];
}
#pragma mark - Endpoints
-(void)endPointAttach{
    attachrecipientManager = [[pootEngine alloc] init];
    [attachrecipientManager setDelegate:self];
    [attachrecipientManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:IdRecipient forKey:@"recipient_id"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [attachrecipientManager startRequestWithURL:ViamericasGetAttachBank withPost:post];
 //   [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
-(void)endPointCountry{
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->countryManager = [[pootEngine alloc] init];
        [self->countryManager setDelegate:self];
        [self->countryManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:@"NO" forKey:@"operation"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->countryManager startRequestWithURL:ViamericasGetCountries withPost:post];
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
//    });
}
-(void)endPointState:(countrysModel *) model{
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->stateManager = [[pootEngine alloc] init];
        [self->stateManager setDelegate:self];
        [self->stateManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:model.idCountry forKey:@"idCountry"];
        [params setObject:@"NO" forKey:@"operation"];
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->stateManager startRequestWithURL:ViamericasGetStates withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
//    });
}
-(void)endPointCity:(stateModel *) model{
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(Second * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->cityManager = [[pootEngine alloc] init];
        [self->cityManager setDelegate:self];
        [self->cityManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:self->itemSelectedCountry.idCountry forKey:@"idCountry"];
        [params setObject:self->itemSelectedCountryState.idState forKey:@"idState"];
        [params setObject:@"NO" forKey:@"operation"];
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->cityManager startRequestWithURL:ViamericasGetCities withPost:post];
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
//    });
}

-(void)endPointUpdateRecipient{
    recipientManager = [[pootEngine alloc] init];
    [recipientManager setDelegate:self];
    [recipientManager setShowComments:developing];
    
    NSString *post = nil;
    NSError *JSONError;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *birthdate = [dateFormatter dateFromString:[birthdateText text]];
    
    if (birthdate == nil) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        birthdate = [dateFormatter dateFromString:[birthdateText text]];
    }
    
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:_recipient.id_sender forKey:@"idSender"];
    [params setObject:_recipient.id_recipient forKey:@"idRecipient"];
    [params setObject:[[lastNameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                               locale:[NSLocale systemLocale]] forKey:@"last_name"];
    [params setObject:[[middlenameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                 locale:[NSLocale systemLocale]] forKey:@"middleName"];
    [params setObject:[zipText text] forKey:@"zip_code"];
    [params setObject:[[addressText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                              locale:[NSLocale systemLocale]] forKey:@"address1"];
    [params setObject:@"" forKey:@"address2"];
    [params setObject:[[birthdateText text] length]==0?@"":[dateFormatter stringFromDate:birthdate] forKey:@"birthDate"];
    [params setObject:[emailText text] forKey:@"email"];
    [params setObject:[[firstText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                            locale:[NSLocale systemLocale]] forKey:@"firstName"];
    [params setObject:itemSelectedCountryCity.idCity forKey:@"idCity"];
    [params setObject:itemSelectedCountry.idCountry forKey:@"idCountry"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
    [params setObject:itemSelectedCountryState.idState forKey:@"idState"];
    [params setObject:[[lastNameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                               locale:[NSLocale systemLocale]] forKey:@"lastName"];
//    [params setObject:[NSString stringWithFormat:@"%@+%@",[ladaText.text stringByReplacingOccurrencesOfString:@"+"
//    withString:@""],[phoneText text] ] forKey:@"phone1"];
    [params setObject:[phoneText text] forKey:@"phone1"];
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];

    [recipientManager startRequestWithURL:ViamericasUpdateRecipient withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
-(void)endPointDeleteRecipient{
    deleterecipientManager = [[pootEngine alloc] init];
    [deleterecipientManager setDelegate:self];
    [deleterecipientManager setShowComments:developing];
    
    NSString *post = nil;
    NSError *JSONError;

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:IdRecipient forKey:@"idRecipient"];

    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];

    [deleterecipientManager startRequestWithURL:ViamericasDeleteRecipient withPost:post];

    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
   [self unLockView];
       if (json == nil) {
           [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:@"Server returns nil"];
           return;
       }
        ////////////////////////////////      END POINT COUNTRY        ///////////////////////////////////
           if (manager == countryManager) {
               NSMutableDictionary *response = (NSMutableDictionary*)json;
               response = [self cleanDictionary:response];
                           
               if ([[response objectForKey:kIDError] intValue] == 0) {
                   
                   objCountrys = [[NSMutableArray alloc] init];
                   NSArray * ArrayCountrys =[response objectForKey:@"countrys"];
                   NSMutableArray *listCountryString = [[NSMutableArray alloc] init];
                   for (int i = 0; i<[ArrayCountrys count]; i++ ) {
                       

                       countrysModel *itemCountry = [[countrysModel alloc] init];
                       itemCountry.idCountry=[ArrayCountrys[i] objectForKey:@"idCountry"];
                       itemCountry.nameCountry=[ArrayCountrys[i] objectForKey:@"nameCountry"];
                       
                       [objCountrys addObject:itemCountry];
                       [listCountryString addObject:itemCountry.nameCountry];
                   }
                   self.downPickerCountry = [[DownPicker alloc] initWithTextField:countryText withData:listCountryString];
                   [self.downPickerCountry setPlaceholder:@"Pais"];
                   [self.downPickerCountry setPlaceholderWhileSelecting:@"Pais"];
                   int x =0;
                   for (countrysModel *itemCountry in objCountrys) {
                       if (![itemCountry.idCountry isEqualToString:_recipient.country_id]) {
                           [self.downPickerCountry setSelectedIndex:0];
                           
                       }else{
                           [self.downPickerCountry setSelectedIndex:x];
                           [self country_Selected:nil];
                           //[self endPointState:[objCountrys objectAtIndex:[self.downPickerCountry selectedIndex]]];
                           break;
                       }
                       x++;
                   }
                   [self.downPickerCountry addTarget:self action:@selector(country_Selected:) forControlEvents:UIControlEventValueChanged];
                   
                   self.downPickerState = [[DownPicker alloc] initWithTextField:stateText withData:@[@""]];
                   [self.downPickerState setPlaceholder:@"Estado"];
                   [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
                   
                   self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:@[@""]];
                   [self.downPickerCity setPlaceholder:@"Ciudad"];
                   [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
                   
                   [self endPointAttach];

     
               } else {
                   [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
               }
           }
       ////////////////////////////////      END POINT STATE        ///////////////////////////////////
       if (manager == stateManager) {
           NSMutableDictionary *response = (NSMutableDictionary*)json;
           response = [self cleanDictionary:response];
           
           if ([[response objectForKey:kIDError] intValue] == 0) {
               
               NSArray * ArrayStates =[response objectForKey:@"countryStates"];
               objCountrysState = [[NSMutableArray alloc] init];
               NSMutableArray *listStateString = [[NSMutableArray alloc] init];
               
               for (NSDictionary* keyState in ArrayStates) {
                   stateModel *itemState = [[stateModel alloc] init];
                   itemState.idState =[keyState objectForKey:@"idState"];
                   itemState.nameState =[keyState objectForKey:@"nameState"];
                   
                   [objCountrysState addObject:itemState];
                   [listStateString addObject:itemState.nameState];
               }
               self.downPickerState = [[DownPicker alloc] initWithTextField:stateText withData:listStateString];
               [self.downPickerState setPlaceholder:@"Estado"];
               [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
               int x =0;
               for (stateModel *itemstate in objCountrysState) {
                   if (![itemstate.idState isEqualToString:_recipient.state_id]) {
                       [self.downPickerState setSelectedIndex:0];
                       
                   }else{
                       [self.downPickerState setSelectedIndex:x];
                       [self state_Selected:nil];
                       break;
                   }
                   x++;
               }
               [self.downPickerState addTarget:self action:@selector(state_Selected:) forControlEvents:UIControlEventValueChanged];
               
               self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:@[@""]];
               [self.downPickerCity setPlaceholder:@"Ciudad"];
               [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
               
           } else {
               [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
           }
       }
       
       ////////////////////////////////      END POINT CIUDAD        ///////////////////////////////////
       
       if (manager == cityManager) {
           NSMutableDictionary *response = (NSMutableDictionary*)json;
           response = [self cleanDictionary:response];
                   
           if ([[response objectForKey:kIDError] intValue]==0) {
               NSArray * ArrayCity =[response objectForKey:@"city"];
               objCountrysCity = [[NSMutableArray alloc] init];
               NSMutableArray *listCityString = [[NSMutableArray alloc] init];
               for (NSDictionary* keyCity in ArrayCity) {
                   cityModel *itemCity = [[cityModel alloc] init];
                   itemCity.idCity =[keyCity objectForKey:@"idCity"];
                   itemCity.nameCity =[keyCity objectForKey:@"nameCity"];
                   
                   [objCountrysCity addObject:itemCity];
                   [listCityString addObject:itemCity.nameCity];
               }
               
               self.downPickerCity = [[DownPicker alloc] initWithTextField:cityText withData:listCityString];
               [self.downPickerCity setPlaceholder:@"Ciudad"];
               [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
               int x =0;
               for (cityModel *itemcity in objCountrysCity) {
                   if (![itemcity.idCity isEqualToString:_recipient.city_id]) {
                       [self.downPickerCity setSelectedIndex:0];
                       
                   }else{
                       [self.downPickerCity setSelectedIndex:x];
                       [self city_Selected:nil];
                       break;
                   }
                   x++;
               }
               [self.downPickerCity addTarget:self action:@selector(city_Selected:) forControlEvents:UIControlEventValueChanged];

               
           } else {
               [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
           }
       }
    
     
    ////////////////////////////////      END POINT ATTACH        ///////////////////////////////////
    if (manager == attachrecipientManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        if ([[response objectForKey:kIDError] intValue] == 0) {
            
            NSArray  *itemJsonBank=[response objectForKey:@"banks"];
            listAttachBank =[[NSMutableArray alloc] init];
            
             for (NSDictionary* key in itemJsonBank) {
                 
                 banksModel *itemAttach = [[banksModel alloc] init];
                 itemAttach.bank_account_id =[key objectForKey:@"bank_account_id"];
                 itemAttach.bank_account_number =[key objectForKey:@"bank_account_number"];
                 itemAttach.bank_account_type =[key objectForKey:@"bank_account_type"];
                 itemAttach.bank_account_type_name =[key objectForKey:@"bank_account_type_name"];
                 itemAttach.nick_name =[key objectForKey:@"nick_name"];
                 itemAttach.payment_network_id =[key objectForKey:@"payment_network_id"];
                 itemAttach.payment_network_name =[key objectForKey:@"payment_network_name"];
                 itemAttach.recipient_id =[key objectForKey:@"recipient_id"];
                 itemAttach.routing_number =[key objectForKey:@"routing_number"];
                 itemAttach.sender_id =[key objectForKey:@"sender_id"];
                 
                 [listAttachBank addObject:itemAttach];
             }
         
            [self.TableViewAttachBank reloadData];
            
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
       
    }
    ////////////////////////////////      END POINT UPDATE RECIPIENTS        ///////////////////////////////////
    if (manager == recipientManager ) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];

        if ([[response objectForKey:kIDError] intValue] == 0) {
           
            [self.navigationController popViewControllerAnimated:YES];
            [_obj_edit recipientUpdateResult:YES];
          
           
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }
    ////////////////////////////////      END POINT DELETE RECIPIENTS        ///////////////////////////////////
    if (manager == deleterecipientManager ) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];

        if ([[response objectForKey:kIDError] intValue] == 0) {
           
            [self.navigationController popViewControllerAnimated:YES];
            [_obj_edit recipientDeleteResult:YES];
           
        } else {
            [self AlertviewCustom:NSLocalizedString(@"Información", nil) Message:response[kErrorMessage]];
        }
    }


}
#pragma mark UITableViewController LoadItems
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listAttachBank.count;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell ;
    @try
    {
        if (listAttachBank == nil || [listAttachBank count] == 0) {
            NSString *CellIdentifier = @"ViewCellAttachBank";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            return cell;
        }
        
        banksModel *item =[listAttachBank objectAtIndex:indexPath.row];
        NSString *CellIdentifier = @"ViewCellAttachBank";
        ViewCellAttachBank *cell = (ViewCellAttachBank *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ViewCellAttachBank" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.lbl_name_bank setText:item.payment_network_name];
        [cell.lbl_account setText:item.bank_account_type_name];
        
        [cell.lbl_number_card setText:[NSString stringWithFormat:@"***** %@", [item.bank_account_number substringWithRange:NSMakeRange(item.bank_account_number.length-4, [item.bank_account_number length]==item.bank_account_number.length?4:4)]]];
        return cell;
    } @catch (NSException *exception)
    {
        // NSLog( @"NameException: %@", exception.name);
        // NSLog( @"ReasonException: %@", exception.reason );
        return cell;
    }
    @finally
    {
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    statusEdit=YES;
     itemSelectedBank =[listAttachBank objectAtIndex:indexPath.row];
    [self touch_addbank:nil];
   // [self.view endEditing:YES];
}
#pragma mark - PickerView
-(void)country_Selected:(id)dp {
   itemSelectedCountry  =[objCountrys objectAtIndex:[self.downPickerCountry selectedIndex]];
    [self.view endEditing:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self endPointState:self->itemSelectedCountry];
    });
}
-(void)state_Selected:(id)dp {
    itemSelectedCountryState  = [objCountrysState objectAtIndex:[self.downPickerState selectedIndex]];
     [self.view endEditing:YES];
       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self endPointCity:self->itemSelectedCountryState];
    });
}
-(void)city_Selected:(id)dp {
    
    itemSelectedCountryCity= [objCountrysCity objectAtIndex:[self.downPickerCity selectedIndex]];
}
#pragma mark - Actions Method
-(void) loadFieldsItems{
    IdRecipient= _recipient.id_recipient;
    [firstText setText:_recipient.firstName];
    [secondlastText setText:_recipient.secondname];
    [middlenameText setText:_recipient.middle_name];
    [lastNameText setText:_recipient.lastname];
    
    [addressText setText:_recipient.address1];
    [phoneText setText:_recipient.phone1];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *birthdate = [dateFormatter dateFromString:_recipient.birthday];
    
    if (birthdate == nil) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        birthdate = [dateFormatter dateFromString:_recipient.birthday];
        
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        [birthdateText setText:[dateFormatter stringFromDate:birthdate]];
    }else{
        
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        [birthdateText setText:[dateFormatter stringFromDate:birthdate]];
    }
    [zipText setText:_recipient.zip_code];
    [emailText setText:_recipient.email];
    
    
    [self endPointCountry];
}
- (void) addbankResult:(BOOL)result{
    [self MessageAlert:@"Haz agregado una nueva cuenta bancaria"];
    listAttachBank =[[NSMutableArray alloc] init];
    [self endPointAttach];
}
- (void) bankUpdateResult:(BOOL)result{
    [self MessageAlert:@"Haz actualizado tu cuenta bancaria"];
    listAttachBank =[[NSMutableArray alloc] init];
    [self endPointAttach];
}
- (void) banktDeleteResult:(BOOL)result{
    [self MessageAlert:@"Haz eliminado tu cuenta bancaria"];
    listAttachBank =[[NSMutableArray alloc] init];
    [self endPointAttach];
}
-(void) MessageAlert:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Mobilecard"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"entendido"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
    }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)AlertviewCustom:(NSString *)title Message:(NSString *)message{
    UIAlertController * alert = [UIAlertController
                                        alertControllerWithTitle:title
                                        message:message
                                        preferredStyle:UIAlertControllerStyleAlert];
           UIAlertAction* yesButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
           }];
           [alert addAction:yesButton];
           [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)touch_addbank:(id)sender {
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Viamericas" bundle:nil];
    AttachBankController *detailVC =
    [nextStory instantiateViewControllerWithIdentifier:@"bank_recipient"];
    

    if (statusEdit) {
        [detailVC setObj_bank:self];
        [detailVC setRecipient:_recipient];
        [detailVC setItemBank:itemSelectedBank];
        [detailVC setStatusEdit:YES];
    }else{
        [detailVC setObj_bank:self];
        [detailVC setRecipient:_recipient];
        itemSelectedBank =[[banksModel alloc] init];
        [detailVC setItemBank:itemSelectedBank];
        [detailVC setStatusEdit:NO];
    }
    
    [self.navigationController pushViewController:detailVC animated:NO];
    statusEdit =FALSE;
}
- (IBAction)touch_backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)touch_delete_recipient:(id)sender {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:NSLocalizedString(@"Información", nil)
                                 message:@"¿Deseas eliminar este beneficiario?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        [self endPointDeleteRecipient];
        
        
    }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
        
        
    }];
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)touch_update_recipient:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        [self endPointUpdateRecipient];
    }
}

@end
