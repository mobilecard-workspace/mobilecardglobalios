//
//  Ingo_Step10.h
//  Mobilecard
//
//  Created by David Poot on 8/18/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Wallet_Ctrl.h"

@protocol ingoEnrollmentDelegate

@required
- (void)ingoEnrollResult:(NSDictionary*)enrollData;

@end

@interface Ingo_Step10 : mT_commonTableViewController

@property (assign, nonatomic) id <ingoEnrollmentDelegate, NSObject> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastnameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *ssnText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *birthdateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;

@property (weak, nonatomic) IBOutlet UIButton *register_Button;



@end
