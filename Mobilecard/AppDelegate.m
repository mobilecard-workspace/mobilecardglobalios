//
//  AppDelegate.m
//  Mobilecard
//
//  Created by David Poot on 10/2/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "AppDelegate.h"
#import <IngoSDK/IngoSDK.h>
#import "iovation.h"
#import "Secure3D_Ctrl.h"
#import "LGSideMenuController.h"
#import "mT_commonTableViewController.h"
#import <IQKeyboardManager.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>



@import Firebase;


#define mainNavController (((AppDelegate*)[[UIApplication sharedApplication] delegate]).navController)

@interface AppDelegate ()

    {
        pootEngine *loginManager;
        UIAlertView *alertMsg;
        
        NSDictionary *urlData;
        
    }
    
    @end

@implementation AppDelegate




//se ejecuta cuando presionas la push
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    
    NSLog( @"Contenido de la push" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", userInfo);
    
       NSLog(@"realizamos una accion");
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Fabric with:@[[Crashlytics class]]];

    [Fabric.sharedSDK setDebug:YES];
   
    //configuracion para las push notifications
    [FIRApp configure];
    //////  Facebook SDK Integrate
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    self.canRunIngoSdk = [IngoSdkManager initIngoSdk:[iovation ioBegin]];
    developing?NSLog(@"canRunIngoSdk: %i", self.canRunIngoSdk):nil;
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];

    [FIRMessaging messaging].delegate = self;
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error fetching remote instance ID: %@", error);
        } else {
            NSLog(@"Este es el token device: %@", result.token);
            
           
            if (result.token != nil){
            
            
                NSDictionary *dataDict = [NSDictionary dictionaryWithObject:result.token forKey:@"token"];
            [[NSNotificationCenter defaultCenter] postNotificationName:
             @"FCMToken" object:nil userInfo:dataDict];
                [[NSUserDefaults standardUserDefaults] setObject:result.token forKey:(NSString*)@"tokenFirestore"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
    
                
          
            }
            
        }
    }];
    
    return YES;
}


//se ejecuta cuando llega una push y esta la aplicacion en primer plano
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert);
    
    

    
    
    
}


- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"Este es el token device: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    
    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:(NSString*)@"tokenFirestore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}





    //////  Facebook SDK Integrate
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    return handled;
}
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}
    
    
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}
    
    
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}
    
    
    
- (void)applicationDidBecomeActive:(UIApplication *)application {
    //////  Facebook SDK Integrate
    [FBSDKAppEvents activateApp];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"getFacebookData" object:self];
    ////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////// Implementacion de verifi incompleta Raul mendez  ///////////////////*nota: esta seccion esta validada para cuando el idUsrStatus sea 100 no envia automaticamente el sms cada que ingresa a la aplicacion *esta incompleto el metodo pero ya no truena
 /*   if ([self verifyStatus]) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey] && [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] && ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue]==99 || [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue]==100)) {
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
                loginManager = [[pootEngine alloc] init];
                [loginManager setDelegate:self];
                [loginManager setShowComments:developing];
                
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userDetails[kUserEmail] forKey:@"usrLoginOrEmail"];
                [params setObject:[loginManager encryptJSONString:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] withPassword:nil] forKey:kUserPassword];
                
                [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
                [params setObject:@"Apple" forKey:@"manufacturer"];
                [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
                [params setObject:@"iOS" forKey:@"platform"];
               // [params setObject:@"USUARIO" forKey:@"tipoUsuario"];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
                
                
                alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Procesando solicitud...", nil) message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                [alertMsg show];
            } else {
                mT_commonTableViewController *helper = [[mT_commonTableViewController alloc] init];
                
                loginManager = [[pootEngine alloc] init];
                [loginManager setDelegate:self];
                [loginManager setShowComments:developing];
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userDetails[@"email"] forKey:@"usrLoginOrEmail"];
                [params setObject:[helper encryptString:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword]] forKey:kUserPassword];
                //MUST HAVE PARAMETERS
                [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
                [params setObject:@"Apple" forKey:@"manufacturer"];
                [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
                [params setObject:@"iOS" forKey:@"platform"];
              //  [params setObject:@"NEGOCIO" forKey:@"tipoUsuario"];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
                
                alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Procesando solicitud...", nil) message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                [alertMsg show];
            }
            
        }
    }*/
}
- (BOOL)verifyStatus
    {
        if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 99)) {
            //            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
            //            [alertMsg setTag:99];
            //            [alertMsg show];
            // [[self window] addSubview:alertMsg];
            return NO;
        }
        
        if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 100)) {
            //            UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            //            topWindow.rootViewController = [UIViewController new];
            //            topWindow.windowLevel = UIWindowLevelAlert + 1;
            //
            //            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            //                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
            //                UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
            //                Register_Step30 *nextView = [[nav viewControllers] firstObject];
            //
            //                [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
            //                [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
            //
            //                [topWindow.rootViewController presentViewController:nav animated:YES completion:nil];
            //               // [[self window] addSubview:nav.view];
            //               // [self addChildViewController:vc];
            //                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            //            }];
            //
            //            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            //            }];
            //
            //            [alertMsg addAction:ok];
            //            [alertMsg addAction:cancel];
            
            //  [topWindow makeKeyAndVisible];
            //  [topWindow.rootViewController presentViewController:alertMsg animated:YES completion:nil];
            //) [[self window] addSubview:alertMsg];
            // [self presentViewController:alertMsg animated:YES completion:nil];
            
            return NO;
        }
        return YES;
    }
    
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
    {
        [alertMsg dismissWithClickedButtonIndex:0 animated:YES];
        
        NSDictionary *response = (NSDictionary*)json;
        response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
        
        [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
    }
    
    
- (NSMutableDictionary*)cleanDictionary:(NSMutableDictionary*)respons
    {
        NSError * err;
        NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:respons options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
        
        NSString *final = [myString stringByReplacingOccurrencesOfString:@",null" withString:@""];
        
        NSData *data =[final dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *response;
        if(data!=nil){
            response = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
        }
        
        for (id key in [response allKeys]) {
            if ([[response valueForKey:key] isKindOfClass:[NSDictionary class]]) {
                for (id key2 in [[response valueForKey:key] allKeys]) {
                    if ([[[response valueForKey:key] valueForKey:key2] isKindOfClass:[NSNull class]]) {
                        [[response valueForKey:key] setObject:@"" forKey:key2];
                    }
                }
            } else {
                if ([[response valueForKey:key] isKindOfClass:[NSNull class]]) {
                    [response setObject:@"" forKey:key];
                }
                if ([[response valueForKey:key] isKindOfClass:[NSArray class]]) {
                    for (int i = 0; i<[[response valueForKey:key] count]; i++) {
                        [[response valueForKey:key] replaceObjectAtIndex:i withObject:[self cleanDictionary:[[response valueForKey:key] objectAtIndex:i]]];
                    }
                }
            }
        }
        
        return response;
    }


- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
 
 
    
    
    UIViewController *controller  = [AppDelegate getTopMostViewController];
   
     
    NSString *nombreVC = NSStringFromClass([controller class]);
        
       
    
        if ( [nombreVC isEqualToString:@"CNCameraViewController"]){
            //if ([navigationController.viewControllers.lastObject isKindOfClass:[IngoSDK class]])
            //  {
            return UIInterfaceOrientationMaskAll;
        }
        else
            return UIInterfaceOrientationMaskPortrait;
        
    
    

    
    
 
   
}

+ (UIViewController*) getTopMostViewController
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(window in windows) {
            if (window.windowLevel == UIWindowLevelNormal) {
                break;
            }
        }
    }
    
    for (UIView *subView in [window subviews])
    {
        UIResponder *responder = [subView nextResponder];
        
        //added this block of code for iOS 8 which puts a UITransitionView in between the UIWindow and the UILayoutContainerView
        if ([responder isEqual:window])
        {
            //this is a UITransitionView
            if ([[subView subviews] count])
            {
                UIView *subSubView = [subView subviews][0]; //this should be the UILayoutContainerView
                responder = [subSubView nextResponder];
            }
        }
        
        if([responder isKindOfClass:[UIViewController class]]) {
            return [self topViewController: (UIViewController *) responder];
        }
    }
    
    return nil;
}

+ (UIViewController *) topViewController: (UIViewController *) controller
{
    BOOL isPresenting = NO;
    do {
        // this path is called only on iOS 6+, so -presentedViewController is fine here.
        UIViewController *presented = [controller presentedViewController];
        isPresenting = presented != nil;
        if(presented != nil) {
            controller = presented;
        }
        
    } while (isPresenting);
    
    return controller;
}

    @end
