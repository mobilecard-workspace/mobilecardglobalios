//
//  shift_Step00.h
//  Mobilecard
//
//  Created by David Poot on 9/26/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol shiftCreationDelegate <NSObject>

@required

- (void)shiftCreation_Result:(NSDictionary*)result;

@end

@interface shift_Step00 : mT_commonTableViewController

@property (assign, nonatomic) id <shiftCreationDelegate, NSObject> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastnameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *intNumberText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *ssnText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *birthdateText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;


@end
