//
//  main.m
//  Mobilecard
//
//  Created by David Poot on 10/2/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
