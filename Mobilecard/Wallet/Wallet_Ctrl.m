//
//  Wallet_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 10/31/17.
//  Copyright © 2017 David Poot. All rights reserved.
//
#define _rowIcondelete 50
#define _rowButtonDelete 100
#define _space 3.7
#define _spaceSelect 5

#import "AuthenticationStartViewController.h"
#import "Wallet_Ctrl.h"
#import "InsetCell.h"
#import "shadowed_insetCARDCell.h"

@interface Wallet_Ctrl ()
    {
        NSMutableArray *walletItems;
        
        NSDictionary *userData;
        NSMutableArray *cardArray;
        NSArray *transactionsArray;
        pootEngine *cardManager;
         pootEngine *cardUpdateManager;
        pootEngine *removeManager;
        pootEngine *setFavoritesManager;
        pootEngine *mcCardManager;
        
        int selectedCardID;
        int selectedCell;
        int rowSelected;
        NSNumberFormatter *numFormatter;
    }
    @end

@implementation Wallet_Ctrl



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    TableWallet.delegate = self;
    TableWallet.dataSource = self;
    
    
    if (_type == walletViewTypeSelection ) {
        TableWallet.frame=CGRectMake(0,0, self.view.bounds.size.width, self.view.bounds.size.height-120);
    }else{
        TableWallet.frame=CGRectMake(0,0, self.view.bounds.size.width, self.view.bounds.size.height-200);
    }
    //NSDictionary * Diccionarydata = DataUser.DATA;
    
   // NSLog(@"variable @%",_modelScan.propine) amount
    
    //DataUser.data

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(walletUpdateNotification:)
                                                 name:@"WalletUpdate"
                                               object:nil];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"Amount"] != nil){
        _type =walletViewTypeSelection;
      //  [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Amount"];

    }
    

    if(__COPhone.length != 0 || __COTelepe.length != 0 || __COSoat.length != 0 || __COCards.length != 0 || __COServices.length != 0){
         _type =walletViewTypeSelection;
        
    }
    
    
    
    userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    cardArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserCardArray]];
    
    for (int i = 0; i< [cardArray count]; i++) {
        NSMutableDictionary *element = [[NSMutableDictionary alloc] initWithDictionary:cardArray[i]];
        
        if ([element[kUserCardTransactions] isKindOfClass:[NSString class]]) {
            [element setObject:[[NSArray alloc] init] forKey:kUserCardTransactions];
            
            [cardArray replaceObjectAtIndex:i withObject:element];
        }
    }
    
    
    //mostrar mensaje en caso de que este en seleccion de tarjetas y no tenga tarjetas disponibles
    if (cardArray.count == 0){
        if (_type == walletViewTypeSelection || _type == walletViewTypeSelectionOnlyDebit) {
            
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil)
                                        message:NSLocalizedString(@"Debes agregar una tarjeta en el wallet para utilizar esta sección",nil)
                                                              delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    
    
    cardManager = [[pootEngine alloc] init];
    [cardManager setDelegate:self];
    [cardManager setShowComments:developing];
    
    
    setFavoritesManager = [[pootEngine alloc] init];
    [setFavoritesManager setDelegate:self];
    [setFavoritesManager setShowComments:developing];
    
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    selectedCardID = 0;
    rowSelected = 1;
    
    walletItems = [[NSMutableArray alloc] init];
    ValidationShowIconWallet = FALSE;
    ValidationShowButtonWallet = FALSE;
    
    [self updateWalletItems];
  
    
    switch (_type) {
        case walletViewTypeSelection:
        case walletViewTypeSelectionOnlyDebit:
        {
            [_sideMenu_Button setImage:nil];
            [_sideMenu_Button setEnabled:NO];
            
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"←" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss_Action:)];
            
            [self.navigationItem setLeftBarButtonItem:backButton];
            [self setTitle:NSLocalizedString(@"MÉTODO DE PAGO", nil)];
        }
        break;
        
        default:
        break;
    }
    
   
    if (_type ==  walletViewTypeSelection || _type ==  walletViewTypeSelectionOnlyDebit){
    
        //CGRect rect = CGRectMake(100.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
            //    [TableWallet setBounds:rect];
         
         }else{
    
    [self setupMenu];
    }
}

//metodo que se ejecuta al llamar al wallet update notification
- (void) walletUpdateNotification:(NSNotification *) notification
{

    
    @try {
        cardUpdateManager = [[pootEngine alloc] init];
        [cardUpdateManager setDelegate:self];
        [cardUpdateManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardUpdateManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
        
        //   UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        //  [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
 
}

- (void)dismiss_Action:(id)sender
    {
        if([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"Amount"] != nil || __COPhone.length != 0 || __COTelepe.length != 0 || __COSoat.length != 0 || __COCards.length != 0 || __COServices.length != 0){
            // [self.navigationController popViewControllerAnimated:YES];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
            
        }else{
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                if ([self->_delegate conformsToProtocol:@protocol(cardSelectionDelegate)]&&[self->_delegate respondsToSelector:@selector(cardSelectionAborted)]) {
                    [self->_delegate cardSelectionAborted];
                }
            }];
            
        }
    }
    
- (IBAction)back_Action:(id)sender {

//    [self.navigationController popViewControllerAnimated:YES];
 //   [self dismiss_Action:nil];
    
    
     if(__isBackDissmis.length != 0){
         [self dismissViewControllerAnimated:TRUE completion:nil];
         return;
     }

    if ([_delegate conformsToProtocol:@protocol(cardSelectionDelegate)]&&[_delegate respondsToSelector:@selector(cardSelectionAborted)]) {
        [_delegate cardSelectionAborted];
    }
     [self.navigationController popViewControllerAnimated:YES];
   
}


- (void)viewDidAppear:(BOOL)animated
{
    [TableWallet selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedCell inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    @try {
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"NewTarjet"] isEqualToString:@"1"]){
             [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"NewTarjet"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             [self updateCardResponse:nil];
             }
             if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"AutenticationJumio"] isEqualToString:@"1"]){
                 //esta la validacion del rulo para irse al detalle de peru de pago con qr
                 
                  [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"AutenticationJumio"];
                  [[NSUserDefaults standardUserDefaults] synchronize];
                
                 
                 if(__COPhone.length != 0){
                     
                     NSLog(@"Este es el id de la tarjeta amigo %d",selectedCardID);
                     NSString *idTarjeta = [@(selectedCardID) stringValue];
                     [[NSUserDefaults standardUserDefaults] setObject:idTarjeta forKey:(NSString*)@"DataUser.idtarjetaco"];
                     
                     
                     [self performSegueWithIdentifier:@"detail_paymentCO" sender:self];
                     
                     
                 
                     
                     return;
                 }
                 
                 
                 if(__COServices.length != 0){
                                  
                                  NSLog(@"Este es el id de la tarjeta amigo %d",selectedCardID);
                                  NSString *idTarjeta = [@(selectedCardID) stringValue];
                                  [[NSUserDefaults standardUserDefaults] setObject:idTarjeta forKey:(NSString*)@"DataUser.idtarjetaco"];
                                  
                                  
                                  [self performSegueWithIdentifier:@"detail_servicesCO" sender:self];
                                  
                                  
                              
                                  
                                  return;
                              }
                 
                 if(__COTelepe.length != 0){
                     
                     NSLog(@"Este es el id de la tarjeta amigo %d",selectedCardID);
                     NSString *idTarjeta = [@(selectedCardID) stringValue];
                     [[NSUserDefaults standardUserDefaults] setObject:idTarjeta forKey:(NSString*)@"DataUser.idtarjetaco"];
                     
                     
                     [self performSegueWithIdentifier:@"detail_paymentTeleCO" sender:self];
                     
                     
                     
                     
                     return;
                 }
                 
                 if(__COSoat.length != 0){
                     
                     NSLog(@"Este es el id de la tarjeta amigo %d",selectedCardID);
                     NSString *idTarjeta = [@(selectedCardID) stringValue];
                     [[NSUserDefaults standardUserDefaults] setObject:idTarjeta forKey:(NSString*)@"DataUser.idtarjetaco"];
                     
                     
                     [self performSegueWithIdentifier:@"detail_paymentSoatCO" sender:self];
                     
                     
                     
                     
                     return;
                 }
                 
                 if(__COCards.length != 0){
                                     
                                     
                                     NSString *idTarjeta = [@(selectedCardID) stringValue];
                                     [[NSUserDefaults standardUserDefaults] setObject:idTarjeta forKey:(NSString*)@"DataUser.idtarjetaco"];
                                     
                                     
                                     [self performSegueWithIdentifier:@"detail_paymentCardCO" sender:self];
                                     
                                     
                                     
                                     
                                     return;
                                 }
                 
                 
                  if([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"Amount"] != nil){
                      [self performSegueWithIdentifier:@"detail_paymentPE" sender:self];
                  }else{
                      //esto es lo que hace cuando no tiene amount quiere decir que viene del otro paisn no peru segun rulo
                      [self.navigationController dismissViewControllerAnimated:YES completion:^{
                          if ([self->_delegate conformsToProtocol:@protocol(cardSelectionDelegate)] && [self->_delegate respondsToSelector:@selector(cardSelectionResult:)]) {
                              [self->_delegate cardSelectionResult:[NSDictionary dictionaryWithDictionary:self->cardArray[self->selectedCardID]]];
                          }
                      }];
                  }
                  }
                  }
                  @catch (NSException * e) {
                    //  NSLog(@"Exception: %@", e);
                  }
    

   
    

}
#pragma Mark updateCardDelegate Handling
    
- (void)updateCardResponse:(NSDictionary *)response
    {
        
        
        //  [self.tableView setNeedsLayout];
        
        userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        cardArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserCardArray]];
        
        [self updateWalletItems];
        
        [TableWallet reloadData];
        
    }
- (void)updateWalletItems
    {
        [walletItems removeAllObjects];
        if (_type != walletViewTypeSelection) {
            [walletItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"cardsList_Header", kDescriptionKey, @"70", kHeightKey, nil]];
        }
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"Mobilecard"] boolValue]) {
            
            //agregado para que no salga la mobile card en caso de no tener en el seleccionador de tarjetas
            if (_type == walletViewTypeSelection || _type == walletViewTypeSelectionOnlyDebit) {
                
            }else{
                
                //si es peru ahorita no sale la mobile card 
                if (([userData[kUserIdCountry] intValue])  != 4){
                
            [walletItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"mobilecard", kDescriptionKey, @"100", kHeightKey, nil]];
            [walletItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kDescriptionKey, @"5", kHeightKey, nil]];
                  }
                
                
            }
        }
        NSString *identifier = [[NSString alloc] init];
        
        NSString *cardHeight = @"100";
        
        for (int i = 0; i<[cardArray count]; i++) {
            
            //        cardHeight = @"100";
            //        if ([cardArray[i][@"tipo"] isEqualToString:@"VISA"]) {
            //            if ([cardArray[i][@"tipoTarjeta"] isEqualToString:@"CREDITO"]) {
            //                if (_type == walletViewTypeSelectionOnlyDebit) {
            //                    continue;
            //                }
            //                identifier = @"visacredit";
            //            } else {
            //                identifier = @"visadebit";
            //            }
            //        } else if ([cardArray[i][@"tipo"] isEqualToString:@"MasterCard"]) {
            //            if ([cardArray[i][@"mobilecard"] boolValue]) {
            //                identifier = @"mymc";
            //                cardHeight = @"130";
            //            } else {
            //                if ([cardArray[i][@"tipoTarjeta"] isEqualToString:@"CREDITO"]) {
            //                    if (_type == walletViewTypeSelectionOnlyDebit) {
            //                        continue;
            //                    }
            //                    identifier = @"mccredit";
            //                } else {
            //                    identifier = @"mcdebit";
            //                }
            //            }
            //        } else if ([cardArray[i][@"tipo"] isEqualToString:@"Carnet"]) {
            //            if ([cardArray[i][@"tipoTarjeta"] isEqualToString:@"CREDITO"]) {
            //                if (_type == walletViewTypeSelectionOnlyDebit) {
            //                    continue;
            //                }
            //                identifier = @"mymccarnet";
            //                cardHeight = @"130";
            //            } else {
            //                identifier = @"mymccarnet";
            //                cardHeight = @"130";
            //            }
            //        } else {
            //            identifier = @"amex";
            //        }
            identifier = @"visadebit";
            
            [walletItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:identifier, kDescriptionKey, cardHeight, kHeightKey, [NSString stringWithFormat:@"%d", i], kIDKey, nil]];
            [walletItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kDescriptionKey, @"10", kHeightKey, nil]];
            
        }
        if (_type == walletViewTypeSelection || _type == walletViewTypeSelectionOnlyDebit) {
            
            
            
          
                
            
            [walletItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"select", kDescriptionKey, @"74", kHeightKey, nil]];
            [walletItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kDescriptionKey, @"50", kHeightKey, nil]];
                
            

        }
        
       
        //     [walletItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kDescriptionKey, @"155", kHeightKey, nil]];
        
    }
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - Table view data source
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _type == walletViewTypeRegular?2:1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        return [walletItems count];
        break;
        
        default:
        {
            if ([cardArray count]!=0) {
                //if ([cardArray[selectedCardID][kUserCardTransactions] count]==0) {
                 //   transactionsArray = [[NSArray alloc] init];
                //} else {
                  //  transactionsArray = [[NSArray alloc] initWithArray:cardArray[selectedCardID][kUserCardTransactions]];
                //}
                
                //if ([transactionsArray count]>0) {
                //    return ([transactionsArray count]*2)+1;
              //  } else {
                //    return 1;
               // }
                return 0;
            } else {
                return 0;
            }
        }
        break;
    }
    
}
- (void) finishSelection:(id)sender
{
    if ((_type == walletViewTypeSelection || _type == walletViewTypeSelectionOnlyDebit) && [cardArray count]==0) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Para continuar, debes seleccionar una tarjeta",nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    
    [self performSegueWithIdentifier:@"Autentication" sender:self];
    /////  Raul mendez implementacion AutenticationStarVerify
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        switch (indexPath.section) { //CARD SELECTION SECTION
            case 0:
            {
                if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"cardsList_Header"]) { //HEADER CONFIGURATION
                    InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:walletItems[indexPath.row][kDescriptionKey]];
                    
                    UIButton *button_addCard= (UIButton*)[cell viewWithTag:1];
                    [button_addCard addTarget:self action:@selector(addNewCard:) forControlEvents:UIControlEventTouchUpInside];
                    
                    button_editCard= (UIButton*)[cell viewWithTag:2];
                    [button_editCard addTarget:self action:@selector(editCardWallet:) forControlEvents:UIControlEventTouchUpInside];
                    if (ValidationShowIconWallet) {
                        [button_editCard setImage:[UIImage imageNamed:@"btn_save_wallet"] forState:UIControlStateNormal];
                    }else{
                        [button_editCard setImage:[UIImage imageNamed:@"btn_edit_wallet"] forState:UIControlStateNormal];
                    }
                    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                    return cell;
                }
                
                
                
                if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"space"]) {  //SPACE CONFIGURATION
                    InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"space"];
                       [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                    return cell;
                }
                
                if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"select"]) { //SELECT BUTTON
                    InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:walletItems[indexPath.row][kDescriptionKey]];
                    
                    UIButton *button = cell.contentView.subviews[0];
                    //[self addShadowToView:button];
                    [button addTarget:self action:@selector(finishSelection:) forControlEvents:UIControlEventTouchUpInside];
                       [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                    return cell;
                }
                //CARD CONFIGURATION
                
                shadowed_insetCARDCell *cell = [tableView dequeueReusableCellWithIdentifier:walletItems[indexPath.row][kDescriptionKey]];
                
                if (![walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]) {  //
                    
                    
                    
                    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"img_short"]]];
                    [cell backgroundImagecard].image = [UIImage imageWithData: imageData];
                    if ([cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"tipo"] isEqualToString:@"AmEx"]) {
                        [cell backgroundImagecard].image = [UIImage imageNamed:@"card_short_amex"];
                    }
                    
                   
                    
              /*      if ([cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"img_short"]   isEqualToString: @"http://199.231.160.203/images/cards/short/Mobilecard.png"]){
                        
                        [cell backgroundImagecard].alpha =0.5;
                        [[cell favoriteIndicator] setHidden:YES];
                        
                    }*/
                    
                }else{
                    
                 
                    
                   // [cell backgroundImagecard].alpha =0.5;
                    [[cell panText] setText:@""];
                  
                    [cell backgroundImagecard].image =  [UIImage imageNamed:@"obtenTarjetaImagen"];
                    
                    [[cell favoriteIndicator] setHidden:YES];
                    
                    //Test apto quitar alpha
                    NSString *aptotest =  [[NSUserDefaults standardUserDefaults] objectForKey:@"DataUser.aptotestsesion"];
                    if( aptotest != nil && [userData[kUserIdCountry] intValue] == 3){
                        [cell backgroundImagecard].alpha =1;
                
                        [[cell favoriteIndicator] setHidden:NO];
                    }
                    
                    
                }
                
                if (cardArray.count != 0) {
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setBool:FALSE forKey:@"highlightView"];
                    [defaults synchronize];
                    [[cell favoriteIndicator] setHidden:![cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"determinada"] boolValue]];
                    // [[cell highlightView] setHidden:YES];
                    if (_type == walletViewTypeSelection || _type == walletViewTypeSelectionOnlyDebit) {
                        [[cell favoriteIndicator] setHidden:YES];
                        // [[cell highlightView] setHidden:NO];
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setBool:TRUE forKey:@"highlightView"];
                        [defaults synchronize];
                    }
                    
                    
                    if (ValidationShowIconWallet) {
                        if (indexPath.row == 3) {
                            [[cell deleteIconButton] setHidden:[cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"determinada"] boolValue]];
                        }
                        else{
                            if (![walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]) {  //
                                [[cell deleteIconButton] setHidden:!ValidationShowIconWallet];
                                [[cell deleteButton] setHidden:!ValidationShowButtonWallet];
                            }
                        }
                    }else{
                        [[cell deleteIconButton] setHidden:!ValidationShowIconWallet];
                        [[cell deleteButton] setHidden:!ValidationShowButtonWallet];
                    }
                    [[cell deleteIconButton] setTag:indexPath.row];
                    [[cell deleteIconButton] addTarget:self action:@selector(showdeleteIconWallet:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    // [[cell deleteButton]  setTag:indexPath.row+_rowButtonDelete];
                    [[cell deleteButton] setTag:indexPath.row];
                    [[cell deleteButton] addTarget:self action:@selector(showdeleteItemWallet:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if (ValidationShowIconWallet) {
                        if (indexPath.row == 3) {
                            [[cell favoritesIconButton] setHidden:[cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"determinada"] boolValue]];
                        }
                        else{
                            if (![walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]) {  //
                                [[cell favoritesIconButton] setHidden:!ValidationShowIconWallet];
                                [[cell favoritesButton] setHidden:!ValidationShowButtonWallet];
                            }
                        }
                    }else{
                        [[cell favoritesIconButton] setHidden:!ValidationShowIconWallet];
                        [[cell favoritesButton] setHidden:!ValidationShowButtonWallet];
                        
                    }
                    
                    [[cell favoritesIconButton] setTag:indexPath.row];
                    [[cell favoritesIconButton] addTarget:self action:@selector(showFavoritesItemWallet:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [[cell favoritesButton] setTag:indexPath.row];
                    [[cell favoritesButton] addTarget:self action:@selector(addFavoritesItemWallet:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if (selectedCardID == 0 && [cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"determinada"] boolValue]) {
                        selectedCell = (int)indexPath.row;
                    }
                    [cell.editButton setHidden:YES];
                    //[cell.editButton setHidden:(_type == walletViewTypeSelection || _type == walletViewTypeSelectionOnlyDebit)?YES:NO];
                    
                    //   if (![cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"mobilecard"] boolValue]||[cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"tipo"] isEqualToString:@"Carnet"]) {
                    //    if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"Mobilecard"] boolValue]) {
                    
                    NSString *cardNumber = (NSString*)[cardManager decryptedStringOfString:cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"pan"] withSensitive:NO];
                    [[cell panText] setText:[NSString stringWithFormat:@"%@ **** **** %@", [cardNumber substringWithRange:NSMakeRange(0, 4)], [cardNumber substringWithRange:NSMakeRange(12, [cardNumber length]==15?3:4)]]];
                    
                    if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]) {  //
                        [[cell balanceText] setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"balance"] floatValue]]]];
                        [cell.editButton setHidden:YES]; //HIDDEN TEMPORARY
                        [cell.favoriteIndicator setHidden:YES]; //HIDDEN TEMPORARY
                        [[cell panText] setText:@""];
                        
                    }
                    
                  
                    
                }
                
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                return cell;
            }
            break;
            
            default: //TRANSACTION SECTION
            {
                switch (indexPath.row) {
                    case 0:
                    {
                        //  InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"transaction_Header"];
                        InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"space"];
                           [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                        return cell;
                    }
                    break;
                    
                    default:
                    {
                        if (indexPath.row%2) {
                            InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"space"];
                            //                        NSString *CellIdentifier = @"space";
                            //                        shadowed_insetCARDCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                            //
                            //                        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                            //
                            //                        for (id element in cell.contentView.subviews) {
                            //                            if ([element isKindOfClass:[UILabel class]]) {
                            //                                UILabel *label = (UILabel*)element;
                            //                                switch (label.tag) {
                            //                                    case 1:
                            //                                        [label setText:transactionsArray[indexPath.row/2][@"ticket"]];
                            //                                        break;
                            //                                    case 2:
                            //                                        [label setText:transactionsArray[indexPath.row/2][@"date"]];
                            //                                        break;
                            //                                    case 3:
                            //                                        [label setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[transactionsArray[indexPath.row/2][@"total"] floatValue]]]];
                            //                                        break;
                            //                                    default:
                            //                                        break;
                            //                                }
                            //                            }
                            //                        }
                               [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                            return cell;
                        } else{
                            InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"space"];
                               [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                            return cell;
                        }
                    }
                    break;
                }
            }
            break;
        }
    }
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        switch (indexPath.section) {
            case 0:
            {
                return [walletItems[indexPath.row][kHeightKey] floatValue];
            }
            break;
            
            default:
            {
                switch (indexPath.row) {
                    case 0:
                    return 70;
                    break;
                    
                    default:
                    if (indexPath.row%2) {
                        return 55;
                    } else {
                        return 5;
                    }
                    break;
                }
            }
            break;
        }
    }
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        switch (indexPath.section) {
            case 0:
            {
                switch (indexPath.row) {
                    case 0:
                    //   [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedCell inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                    break;
                    
                    default:
                    {
                        selectedCardID = [walletItems[indexPath.row][kIDKey] intValue];
                        selectedCell = (int)indexPath.row;
                        
                        
                        //ESTADOS UNIDOS PRUEBAS
                        
                        switch ([userData[kUserIdCountry] intValue]) {
                            case 3: //USA
                            {
                             //codigo de prueba apto al seleccionar mobilcard mande a otro lado
                                
                                NSString *aptotest =  [[NSUserDefaults standardUserDefaults] objectForKey:@"DataUser.aptotestsesion"];

                                NSLog(@"Esto vale el data %@",aptotest);
                                
                                if (_type == walletViewTypeSelection || _type == walletViewTypeSelectionOnlyDebit) {
                                    return;
                                    nil;
                                }
                                
                            
                                //aqui
                               
                                if ([cardArray count] >= 1){
                                    
                                    if ([cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"previvale"] boolValue]) {  //
                                        
                                        [self openMyMC];
                                        
                                        nil;
                                    }
                                
                                
                                if(([cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"mobilecard"] boolValue] == YES)){
                                    
                                    // if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]){
                                    
                                    
                                    //guardamos la tarjeta como no se pueden vincular las clases swift a esta clase
                                    NSString *cardNumber = (NSString*)[cardManager decryptedStringOfString:cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"pan"] withSensitive:NO];
                                    NSString *codigo = (NSString*)[cardManager decryptedStringOfString:cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"codigo"] withSensitive:NO];
                                    
                                    NSString *vigencia = (NSString*)[cardManager decryptedStringOfString:cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"vigencia"] withSensitive:NO];
                                    
                                     NSString *accountIDApto = (NSString*)cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"account_id"];
                                    
                                    [[NSUserDefaults standardUserDefaults] setObject:cardNumber forKey:(NSString*)@"DataUser.aptocard"];
                                    
                                    [[NSUserDefaults standardUserDefaults] setObject:codigo forKey:(NSString*)@"DataUser.aptocodigo"];
                                    
                                    [[NSUserDefaults standardUserDefaults] setObject:vigencia forKey:(NSString*)@"DataUser.aptovigencia"];
                                    
                                     [[NSUserDefaults standardUserDefaults] setObject:accountIDApto forKey:(NSString*)@"DataUser.aptoaccountid"];
                                    
                                    [self performSegueWithIdentifier:@"usaAptoHome" sender:self];
                                   //  }else{
                                  //        [self editCard:nil];
                                 //    }
                                    
                                }else{
                                    
                                    
                                     if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]){
                                    [self performSegueWithIdentifier:@"usaAptoRegister" sender:self];
                                     
                                     
                                     }else{
                                         
                                          [self editCard:nil];
                                     }
                                    
                                }
                                
                                }else{
                                    
                                    if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]){
                                        [self performSegueWithIdentifier:@"usaAptoRegister" sender:self];
                                    }
                                    
                                }
                                
                              
                                
                                
                            }
                                break;
                                //hasta aqui termina el codigo de prueba apto
                                
                                
                                //LOS DEMAS PAISES
                            default:
                            {
                                
                                
                                
                                if (_type == walletViewTypeSelection || _type == walletViewTypeSelectionOnlyDebit) {
                
                                    nil;
                                } else {
                                  
                                    if ([cardArray count] > 0) {
                                        if ([cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"previvale"] boolValue]) {  //
                                           
                                            [self openMyMC];
                                        }else{
                                            ////////// reemplazo de codigo para visualizar el detalle de tarjeta Raul mendez  //////////////
                                            //   [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationLeft];
                                            if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]) {  //
                                                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"previvale" bundle:nil];
                                                UINavigationController *nav;
                                                if (![[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
                                                    nav = [nextStory instantiateViewControllerWithIdentifier:@"user"];
                                                } else {
                                                    nav = [nextStory instantiateInitialViewController];
                                                }
                                                
                                                // previvale_Step10 *nextView = [[nav childViewControllers] firstObject];
                                                // [nextView setDelegate:self];
                                                [[self navigationController] presentViewController:nav animated:YES completion:nil];
                                                
                                            }else{
                                                [self editCard:nil];
                                            }
                                    }
                                    }else{
                                        if ([walletItems[indexPath.row][kDescriptionKey] isEqualToString:@"mobilecard"]) {  //
                                            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"previvale" bundle:nil];
                                            UINavigationController *nav;
                                            if (![[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
                                                nav = [nextStory instantiateViewControllerWithIdentifier:@"user"];
                                            } else {
                                                nav = [nextStory instantiateInitialViewController];
                                            }
                                            
                                            // previvale_Step10 *nextView = [[nav childViewControllers] firstObject];
                                            // [nextView setDelegate:self];
                                            [[self navigationController] presentViewController:nav animated:YES completion:nil];
                                            
                                        }else{
                                            [self editCard:nil];
                                        }
                                    }
                                    
                                }
                                
                                
                                
                                
                            }
                                break;
                                
                        }
                        
                        
                        
                      
                        
                        
                        
                        
                        
                        
                    }
                    break;
                }
            }
            break;
            
            default:
            break;
        }
    }
    
#pragma Mark segue handling
    
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        
        if ([[segue identifier] isEqualToString:@"Autentication"]) {
            
            //si tenemos whiteList la mandamos
            if (__whiteList != nil && ![__whiteList  isEqual: @""]){
            
            AuthenticationStartViewController *controller = segue.destinationViewController;
           
            [controller set_whiteList:__whiteList];
            }
       
            
        }
        
        if ([[segue identifier] isEqualToString:@"addCard"]) {
           
            UINavigationController *navController = segue.destinationViewController;
            updateCard_Ctrl *destinationView = [navController childViewControllers].firstObject;
            [destinationView setDelegate:self];
            [destinationView setType:updateCardTypeNew];
        }
        
        
        if ([[segue identifier] isEqualToString:@"updateCard"]) {
            UINavigationController *navController = segue.destinationViewController;
            updateCard_Ctrl *destinationView = [navController childViewControllers].firstObject;
            [destinationView setDelegate:self];
            [destinationView setType:updateCardTypeExisting];
            [destinationView setCardToUpdateID:selectedCardID];
        }
        
        if ([[segue identifier] isEqualToString:@"detail_paymentPE"]) {
            DetailPaymentPEController *next = (DetailPaymentPEController *)[segue destinationViewController];
            [next setSelec_tarjet:selectedCardID];
        }
        
        
    
        
        
    }
    
    
    ///////////////  SECTION DELETE ITEM WALLET  /////////////
    //- (IBAction)delete_Action:(id)sender {
    //    UIActionSheet *actionMsg = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"¿Está seguro de eliminar esta tarjeta?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) destructiveButtonTitle:NSLocalizedString(@"Eliminar tarjeta", nil) otherButtonTitles: nil];
    //    [actionMsg showInView:self.view];
    //
    //    if ([_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[_delegate respondsToSelector:@selector(removeCardWithID:)]) {
    //        [_delegate removeCardWithID:selectedCardID];
    //    }
    //}
- (void)addFavoritesItemWallet:(id)sender
    {
        NSInteger i = [sender tag];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        selectedCardID = [walletItems[indexPath.row][kIDKey] intValue];
        selectedCell = (int)indexPath.row;
        NSString *cardNumber = (NSString*)[cardManager decryptedStringOfString:cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"pan"] withSensitive:NO];
        NSString * value =[NSString stringWithFormat:@"%@",  [cardNumber substringWithRange:NSMakeRange(12, [cardNumber length]==15?3:4)]];
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Deseas agregar a favoritos a la tarjeta terminación %@ ",value] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Sí", nil), nil];
        [alertMsg setTag:200];
        [alertMsg show];
        
    }
- (void)showFavoritesItemWallet:(id)sender
    {
        NSInteger i = [sender tag];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        shadowed_insetCARDCell *cell = [TableWallet cellForRowAtIndexPath:indexPath];
        [cell favoritesButton].hidden = NO;
        
        [cell deleteIconButton].hidden =YES;
        
    }
- (void)showdeleteItemWallet:(id)sender
    {
        NSInteger i = [sender tag];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        selectedCardID = [walletItems[indexPath.row][kIDKey] intValue];
        selectedCell = (int)indexPath.row;
        NSString *cardNumber = (NSString*)[cardManager decryptedStringOfString:cardArray[[walletItems[indexPath.row][kIDKey] intValue]][@"pan"] withSensitive:NO];
        NSString * value =[NSString stringWithFormat:@"%@",  [cardNumber substringWithRange:NSMakeRange(12, [cardNumber length]==15?3:4)]];
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Deseas eliminar tarjeta terminación %@ ",value] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Sí", nil), nil];
        [alertMsg setTag:100];
        [alertMsg show];
        
    }
- (void)showdeleteIconWallet:(id)sender
    {
        NSInteger i = [sender tag];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        shadowed_insetCARDCell *cell = [TableWallet cellForRowAtIndexPath:indexPath];
        [cell deleteButton].hidden = NO;
        
        [cell favoritesIconButton].hidden =YES;
        
    }
- (void)removeCardWithID:(int)cardID
    {
        removeManager = [[pootEngine alloc] init];
        [removeManager setDelegate:self];
        [removeManager setShowComments:developing];
        
        [removeManager startRequestWithURL:[NSString stringWithFormat:@"%@?idTarjeta=%@&idUsuario=%@&idioma=%@", WSWalletRemoveCard, cardArray[cardID][@"idTarjeta"],userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }
    
#pragma Mark pootEngine Handler
    
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
    {
        [self unLockView];
        
        if ( mcCardManager == manager){
            NSMutableDictionary *response = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary*)json];
            [self cleanDictionary:response];
            [self previvaleRegisterResponse:response];
        }
        
        if (cardUpdateManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                ValidationShowIconWallet = FALSE;
                ValidationShowButtonWallet = FALSE;
                
                userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                cardArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserCardArray]];
                
                for (int i = 0; i< [cardArray count]; i++) {
                    NSMutableDictionary *element = [[NSMutableDictionary alloc] initWithDictionary:cardArray[i]];
                    
                    if ([element[kUserCardTransactions] isKindOfClass:[NSString class]]) {
                        [element setObject:[[NSArray alloc] init] forKey:kUserCardTransactions];
                        
                        [cardArray replaceObjectAtIndex:i withObject:element];
                    }
                }
                
                [self updateWalletItems];
                [TableWallet reloadData];
                
                
                [TableWallet selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedCell inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
        
        
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
        
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                ValidationShowIconWallet = FALSE;
                ValidationShowButtonWallet = FALSE;
                
                [self updateWalletItems];
                [TableWallet reloadData];
                
                
                [TableWallet selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedCell inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
        
        if (removeManager == manager) {
            NSMutableDictionary *response = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary*)json];
            [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                if (selectedCardID == [cardArray count]-1) {
                    selectedCardID = (int)[response[kUserCardArray] count]-1;
                }
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alertMsg show];
                
                ValidationShowIconWallet = FALSE;
                ValidationShowButtonWallet = FALSE;
                [self updateCardResponse:nil];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
        if (setFavoritesManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                ValidationShowButtonWallet = FALSE;
                ValidationShowIconWallet = FALSE;
                [self updateCardResponse:nil];
                [TableWallet reloadData];
                
                [TableWallet selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedCell inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
        
    }
- (BOOL)verifyStatus
    {
        if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 99)) {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
            [alertMsg setTag:99];
            [alertMsg show];
            return NO;
        }
        
        if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 100)) {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
                UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
                Register_Step30 *nextView = [[nav viewControllers] firstObject];
                
                [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                
                [self.navigationController presentViewController:nav animated:YES completion:nil];
                
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            }];
            
            [alertMsg addAction:ok];
            [alertMsg addAction:cancel];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
            
            return NO;
        }
        return YES;
    }
#pragma Mark UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
    {
        switch (alertView.tag) {
            case 100:
            {
                switch (buttonIndex) {
                    case 0:
                    {
                        
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedCell inSection:0];
                        shadowed_insetCARDCell *cell = [TableWallet cellForRowAtIndexPath:indexPath];
                        [cell deleteIconButton].hidden = YES;
                        [cell deleteButton].hidden = YES;
                        [cell favoritesIconButton].hidden =YES;
                        [cell favoritesButton].hidden = YES;
                    }
                    break;
                    case 1:
                    {
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedCell inSection:0];
                        shadowed_insetCARDCell *cell = [TableWallet cellForRowAtIndexPath:indexPath];
                        [cell deleteIconButton].hidden = YES;
                        [cell deleteButton].hidden = YES;
                        [cell favoritesIconButton].hidden =YES;
                        [cell favoritesButton].hidden = YES;
                        if (![cardArray[[walletItems[selectedCell][kIDKey] intValue]][@"determinada"] boolValue]) {
                            [self removeCardWithID:selectedCardID];
                        }
                        
                    }
                    break;
                    
                    default:
                    break;
                }
                break;
            } case 200:
            {
                switch (buttonIndex) {
                    case 0:
                    {
                        
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedCell inSection:0];
                        shadowed_insetCARDCell *cell = [TableWallet cellForRowAtIndexPath:indexPath];
                        [cell deleteIconButton].hidden = YES;
                        [cell deleteButton].hidden = YES;
                        [cell favoritesIconButton].hidden =YES;
                        [cell favoritesButton].hidden = YES;
                    }
                    break;
                    case 1:
                    {
                        ///////// self favorites callws
                        
                        [self setFavoritesCardWithID:selectedCardID];
                    }
                    break;
                    
                    default:
                    break;
                }
                break;
            }
        }
        
    }
#pragma Mark Special actions
    
- (void)addNewCard:(id)sender
    {
        //  [self performSegueWithIdentifier:@"addCard" sender:nil];
        UINavigationController *nav = [[UIStoryboard storyboardWithName:@"registerTarjet" bundle:nil] instantiateViewControllerWithIdentifier:@"tarjet"];
        //     updateCard_Ctrl *nextView = [nav childViewControllers].firstObject;
        //     [nextView setType:updateCardTypeRegistry];
        [[self navigationController] presentViewController:nav animated:YES completion:nil];
    }
    
- (void)editCard:(id)sender
    {
        UIButton *button = sender;
        [self performSegueWithIdentifier:@"updateCard" sender:button];
    }
- (void)editCardWallet:(id)sender
    {
        if (!ValidationShowIconWallet) {
            ValidationShowIconWallet = TRUE;
            // [button_editCard setImage:[UIImage imageNamed:@"btn_save_wallet"] forState:UIControlStateNormal];
        }else{
            if (!ValidationShowIconWallet) {
                ValidationShowIconWallet = TRUE;
                // [button_editCard setImage:[UIImage imageNamed:@"btn_save_wallet"] forState:UIControlStateNormal];
            }else{
                ValidationShowIconWallet = FALSE;
                // [button_editCard setImage:[UIImage imageNamed:@"btn_edit_wallet"] forState:UIControlStateNormal];
            }
        }
        [TableWallet reloadData];
    }
- (void)setFavoritesCardWithID:(int)cardID
    {
        setFavoritesManager = [[pootEngine alloc] init];
        [setFavoritesManager setDelegate:self];
        [setFavoritesManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:cardArray[cardID][@"idTarjeta"] forKey:@"idCard"];
        [params setObject:userData[kUserIDKey] forKey:@"idUser"];
        
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        [setFavoritesManager startJSONRequestWithURL:WSWalletSetFavoritesCard withPost:JSONString];
        //    [setFavoritesManager startRequestWithURL:[NSString stringWithFormat:@"%@?idCard=%@&idUser=%@", WSWalletSetFavoritesCard, cardArray[cardID][@"idTarjeta"],userData[kUserIDKey]]];
        // NSLocalizedString(@"lang", nil)
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }




- (void) setupMenu {
    UIView* footerView = nil;
    if (_type == walletViewTypeSelection ) {
        footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-self.view.bounds.size.height/_spaceSelect, self.view.bounds.size.width, 10)];
    }else{
     //   footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-self.view.bounds.size.height/_space, self.view.bounds.size.width, 120)];
    //    footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-self.view.bounds.size.height/_space, self.view.bounds.size.width, 40)];
     //   footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-self.view.bounds.size.height/4.85, self.view.bounds.size.width, 40)];
         footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height/2+self.view.bounds.size.height/4.5, self.view.bounds.size.width, 120)];
    }
    
    [footerView setBackgroundColor:[UIColor whiteColor]];
    UIButton * homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * favoritesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * myMCButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIStackView *stackView = [[UIStackView alloc] init];
    
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    switch ([userData[kUserIdCountry] intValue]) {
        case 1:  case 2: case 3: case 4:  //MEXICO //COLOMBIA //USA // PERU
        [homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
        [homeButton setContentMode:UIViewContentModeScaleAspectFill];
        [homeButton addTarget:self action:@selector(HomeAction:) forControlEvents:UIControlEventTouchUpInside];
        [homeButton.heightAnchor constraintEqualToConstant:75].active = true;
        [homeButton.widthAnchor constraintEqualToConstant:75].active = true;
        
        
        [walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        [walletButton setContentMode:UIViewContentModeScaleAspectFill];
        // [walletButton addTarget:self action:@selector(showWallet:) forControlEvents:UIControlEventTouchUpInside];
        [walletButton.heightAnchor constraintEqualToConstant:75].active = true;
        [walletButton.widthAnchor constraintEqualToConstant:75].active = true;
        
        [favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        [favoritesButton setContentMode:UIViewContentModeScaleAspectFill];
        [favoritesButton addTarget:self action:@selector(showFavorites:) forControlEvents:UIControlEventTouchUpInside];
        [favoritesButton.heightAnchor constraintEqualToConstant:75].active = true;
        [favoritesButton.widthAnchor constraintEqualToConstant:75].active = true;
        
        [myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_history", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        [myMCButton setContentMode:UIViewContentModeScaleAspectFill];
        [myMCButton addTarget:self action:@selector(showHistory:) forControlEvents:UIControlEventTouchUpInside];
        [myMCButton.heightAnchor constraintEqualToConstant:75].active = true;
        [myMCButton.widthAnchor constraintEqualToConstant:75].active = true;
        
        
        //Stack View
        //stackView.axis = UILayoutConstraintAxisVertical;
        stackView.axis = UILayoutConstraintAxisHorizontal;
        // stackView.distribution = UIStackViewDistributionEqualSpacing;
        stackView.distribution = UIStackViewDistributionFillEqually;
        // stackView.alignment = UIStackViewAlignmentCenter;
        stackView.alignment = UIStackViewAlignmentFill;
        stackView.spacing = 8;
        
        [stackView addArrangedSubview:homeButton];
        [stackView addArrangedSubview:walletButton];
        [stackView addArrangedSubview:favoritesButton];
        [stackView addArrangedSubview:myMCButton];
        
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        [footerView addSubview:stackView];
        
        [stackView.centerXAnchor constraintEqualToAnchor:footerView.centerXAnchor].active = true;
        [stackView.centerYAnchor constraintEqualToAnchor:footerView.centerYAnchor].active = true;
        
        [self.view addSubview:footerView];
        
        break;
        
        //
        //            [homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
        //            [homeButton setContentMode:UIViewContentModeScaleAspectFill];
        //            [homeButton.heightAnchor constraintEqualToConstant:75].active = true;
        //            [homeButton.widthAnchor constraintEqualToConstant:75].active = true;
        //
        //
        //            [walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        //            [walletButton setContentMode:UIViewContentModeScaleAspectFill];
        //            [walletButton addTarget:self action:@selector(showWallet) forControlEvents:UIControlEventTouchUpInside];
        //            [walletButton.heightAnchor constraintEqualToConstant:75].active = true;
        //            [walletButton.widthAnchor constraintEqualToConstant:75].active = true;
        //
        //            [favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        //            [favoritesButton setContentMode:UIViewContentModeScaleAspectFill];
        //            [favoritesButton addTarget:self action:@selector(startFavorites) forControlEvents:UIControlEventTouchUpInside];
        //            [favoritesButton.heightAnchor constraintEqualToConstant:75].active = true;
        //            [favoritesButton.widthAnchor constraintEqualToConstant:75].active = true;
        //
        //
        //            //Stack View
        //            //stackView.axis = UILayoutConstraintAxisVertical;
        //            stackView.axis = UILayoutConstraintAxisHorizontal;
        //            // stackView.distribution = UIStackViewDistributionEqualSpacing;
        //            stackView.distribution = UIStackViewDistributionFillEqually;
        //            // stackView.alignment = UIStackViewAlignmentCenter;
        //            stackView.alignment = UIStackViewAlignmentFill;
        //            stackView.spacing = 8;
        //
        //            [stackView addArrangedSubview:homeButton];
        //            [stackView addArrangedSubview:walletButton];
        //            [stackView addArrangedSubview:favoritesButton];
        //
        //            stackView.translatesAutoresizingMaskIntoConstraints = false;
        //            [footerView addSubview:stackView];
        //
        //            [stackView.centerXAnchor constraintEqualToAnchor:footerView.centerXAnchor].active = true;
        //            [stackView.centerYAnchor constraintEqualToAnchor:footerView.centerYAnchor].active = true;
        //            [self.view addSubview:footerView];
        //            break;
    }
    
}
#pragma mark previvale handler
- (void)previvaleRegisterResponse:(NSDictionary *)response
    {
        if (response) {
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
            MCPanel_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"mcpanel_view"];
            [nextView setMcCardInfo:response[@"card"]];
            
            [[self navigationController] pushViewController:nextView animated:YES];
        }
    }
    
- (void) showFavorites:(id)sender
    {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *nextView = [nextStory instantiateViewControllerWithIdentifier:@"contactIdentifier"];
        [self.navigationController pushViewController:nextView animated:YES];
    }
    
- (void) showHistory:(id)sender
    {
        if ([self verifyStatus]) {
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"statement" bundle:nil];
            [self.navigationController pushViewController:[nextStory instantiateInitialViewController] animated:YES];
        }
    }
- (void) HomeAction:(id)sender
    {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
    }
-(void)openMyMC{
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    switch ([userData[kUserIdCountry] intValue]) {
        case 1: //MEXICO
        {
            //[self openWallet_Action:nil];
            mcCardManager = [[pootEngine alloc] init];
            [mcCardManager setDelegate:self];
            [mcCardManager setShowComments:developing];
            
            [mcCardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@&pais=%@", WSWalletGetCustomCard,userData[kUserIDKey], NSLocalizedString(@"lang", nil), userData[kUserIdCountry]] withPost:nil];
            
            [self lockViewWithMessage:nil];
        }
        break;
        case 3: //USA
        {
            
            //validacion test para ingresar al home o aptoregister
            
           /* NSString *aptotest =  [[NSUserDefaults standardUserDefaults] objectForKey:@"DataUser.aptotestsesion"];
            
            
           
            
            
            if( aptotest != nil){
                

            [self performSegueWithIdentifier:@"usaAptoHome" sender:self];
                
            }else{
                
                 [self performSegueWithIdentifier:@"usaAptoRegister" sender:self];
                
            }*/
            
            
            //            shiftManager = [[pootEngine alloc] init];
            //            [shiftManager setDelegate:self];
            //            [shiftManager setShowComments:developing];
            //
            //            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            //
            //            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            //            [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
            //            [params setObject:@"IOS" forKey:@"plataforma"];
            //
            //            NSError *JSONError;
            //            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            //            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            //
            //            NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:[self encryptString:[NSString stringWithFormat:@"%@%d%@", userData[kUserIDKey], idApp, @"IOS"]], @"token", nil];
            //
            //            [shiftManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/Shift/%d/%@/%@/CheckUser?idioma=%@&plataforma=%@", simpleServerURL, idApp, userData[kUserIDKey], userData[kUserIdCountry], NSLocalizedString(@"lang", nil), @"iOS"] withPost:JSONString andHeader:header];
            //
            //            [self lockViewWithMessage:nil];
            
            return;
            
        }
        break;
        default:
            //[self openWallet_Action:nil];
            mcCardManager = [[pootEngine alloc] init];
            [mcCardManager setDelegate:self];
            [mcCardManager setShowComments:developing];
            
            [mcCardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@&pais=%@", WSWalletGetCustomCard,userData[kUserIDKey], NSLocalizedString(@"lang", nil), userData[kUserIdCountry]] withPost:nil];
            
            [self lockViewWithMessage:nil];
        break;
    }
}
    
    @end

