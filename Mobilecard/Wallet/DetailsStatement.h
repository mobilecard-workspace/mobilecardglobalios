//
//  DetailsStatement.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/19/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "statementModel.h"
#import "cardStatementModel.h"
#import "mT_commonController.h"
#import "MC_Colors.h"
NS_ASSUME_NONNULL_BEGIN

@interface DetailsStatement : mT_commonController{
    
    __weak IBOutlet UILabel *lbl_trnasacction;
    __weak IBOutlet UIImageView *img_status;
    __weak IBOutlet UILabel *lbl_date;
    __weak IBOutlet UILabel *lbl_ticket;
    __weak IBOutlet UILabel *lbl_mount_ticket;
    
    __weak IBOutlet UILabel *lbl_importe;
    __weak IBOutlet UILabel *lbl_comision;
    __weak IBOutlet UILabel *lbl_total;
    
    __weak IBOutlet UILabel *lbl_numberTarjet;
    __weak IBOutlet UILabel *lbl_nameTarjet;
    
    UIButton *_homeButton;
    UIButton *_myMCButton;
    UIButton *_favoritesButton;
    UIButton *_walletButton;
}


@property (nonatomic, assign) statementModel *Item;
@end

NS_ASSUME_NONNULL_END
