//
//  DetailPaymentPEController.m
//  Mobilecard
//
//  Created by Raul Mendez on 7/20/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "DetailPaymentPEController.h"

@interface DetailPaymentPEController (){
    CLLocationManager *locationManager;
    CLLocation *currentLocation; 
    CLGeocoder *geocoder;
}

@end

@implementation DetailPaymentPEController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    selectManager = [[pootEngine alloc] init];
    UserDataPE = [[NSUserDefaults standardUserDefaults] objectForKey:@"DataUser.data"];
    cardToUpdate = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserCardArray];

    
    [lbl_monto setText:[UserDataPE objectForKey:@"amount"]];
    [lbl_propina setText:[UserDataPE objectForKey:@"propina"]];
    [lbl_comision setText:[UserDataPE objectForKey:@"comision"]];
    [lbl_concepto setText:[UserDataPE objectForKey:@"concept"]];
    [lbl_establecimiento setText:[UserDataPE objectForKey:@"businessName"]];
    [lbl_total setText:[@([self getTotal]) stringValue]];
  
    NSString *cardNumberEncrypt =cardToUpdate[_selec_tarjet][@"pan"];
    NSString * cardNumber = [selectManager decryptedStringOfString:cardNumberEncrypt withSensitive:NO];
    [lbl_numberTarjet setText:[NSString stringWithFormat:@"%@ **** **** %@", [cardNumber substringWithRange:NSMakeRange(0, 4)], [cardNumber substringWithRange:NSMakeRange(12, [cardNumber length]==15?3:4)]]];
    
    
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:cardToUpdate[_selec_tarjet][@"img_short"]]];
    [imgTarjet setImage:[UIImage imageWithData: imageData]];
  
 
}

-(float) getTotal{
    
    float total =([[UserDataPE objectForKey:@"amount"] floatValue] + [[UserDataPE objectForKey:@"propina"] floatValue] + [[UserDataPE objectForKey:@"comision"] floatValue]);
    return total ;
    
}

-(void) for_paymetPE{
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"lat"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"lon"];
    
    
    [params setObject:@"" forKey:@"ct"];
    [params setObject:@"" forKey:@"vigencia"];
    [params setObject:@"" forKey:@"nombre"];
    [params setObject:@"" forKey:@"apellido"];

    [params setObject:[UserDataPE objectForKey:@"amount"] forKey:@"amount"];
    [params setObject:@"" forKey:@"tarjeta"];
    [params setObject:userInfo[kUserIDKey] forKey:@"idUser"];
    [params setObject:[UserDataPE objectForKey:@"propina"] forKey:@"propina"];
    [params setObject:[UserDataPE objectForKey:@"qrBase64"] forKey:@"qrBase64"];
    [params setObject:[UserDataPE objectForKey:@"concept"] forKey:@"concept"];
    [params setObject:@"" forKey:@"tipoTarjeta"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
    [params setObject:@"0" forKey:@"establecimientoId"];    ///// VERIFICAR
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:[UserDataPE objectForKey:@"comision"] forKey:@"comision"];
    [params setObject:@"0" forKey:@"msi"];
    [params setObject:@"0" forKey:@"idBitacora"];
    [params setObject:@"IOS" forKey:@"modelo"];
    [params setObject:@"" forKey:@"referenciaNeg"];
    [params setObject:cardToUpdate[_selec_tarjet][@"idTarjeta"] forKey:@"idCard"];



    developing?NSLog(@"PARAMS -> %@", params):nil;
    Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
    [nextView setPurchaseInfo:params];
    [nextView setSecure3DURL:WSGetANTADIAVEPayment];
    [nextView setType:serviceTypePaymentPE];
    [nextView setSelectedCardInfo:[NSMutableDictionary dictionaryWithDictionary:cardToUpdate[_selec_tarjet]]];
    [nextView setDelegate:self];
    [self.navigationController pushViewController:nextView animated:YES];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}
- (void)PaymentWithThreatMetrixResponse:(NSDictionary *)response
{
    if (response) {
        [self performSegueWithIdentifier:@"result" sender:response];
        
        if ([response[@"code"] intValue] == 0) {
            
        };
    }
}
- (void)Secure3DResponse:(NSDictionary *)response{
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"result"]) {
        paymentGeneralResult *nextView = [segue destinationViewController];
        NSDictionary *resultData = (NSDictionary*)sender;
        [nextView setResultData:resultData];
    }
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)Actioncontinue:(id)sender {
       [self for_paymetPE];
}
@end
