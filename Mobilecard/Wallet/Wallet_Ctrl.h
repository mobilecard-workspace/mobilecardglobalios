//
//  Wallet_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 10/31/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "MNMRemoteImageView.h"
#import "updateCard_Ctrl.h"
#import "Register_Step30.h"
#import "previvale_Step10.h"
#import "MCPanel_Ctrl.h"
#import "DetailPaymentPEController.h"


@protocol cardSelectionDelegate <NSObject>

@required
- (void)cardSelectionResult:(NSDictionary *)selectedCard;

@optional
- (void)cardSelectionAborted;
@end


@interface Wallet_Ctrl : mT_commonViewController <updateCardDelegate, UITableViewDataSource,UITableViewDelegate>{
    UIButton *_homeButton;
    UIButton *_myMCButton;
    UIButton *_favoritesButton;
    UIButton *_walletButton;
    
    UIButton * button_editCard;
    
    BOOL ValidationShowIconWallet;
    BOOL ValidationShowButtonWallet;
    
    __weak IBOutlet UITableView *TableWallet;
    
}

@property (nonatomic, assign) id <cardSelectionDelegate, updateCardDelegate, NSObject> delegate;

@property (nonatomic, assign) walletViewType type;
@property (strong,nonatomic) NSString *_COPhone;
@property (strong,nonatomic) NSString *_COServices;
    @property (strong,nonatomic) NSString *_COTelepe;
  @property (strong,nonatomic) NSString *_COSoat;
@property (strong,nonatomic) NSString *_COCards;
@property (strong,nonatomic) NSString *_whiteList;
@property (strong,nonatomic) NSString *_isBackDissmis;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideMenu_Button;
@property (weak, nonatomic) IBOutlet UIView *viewBotMenu;

//@property (weak, nonatomic) IBOutlet ScanQrModel *modelScan;

@end
