//
//  statement.m
//  MobileCard_X
//
//  Created by David Poot on 1/13/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "statement.h"

@interface statement ()
{
    
    pootEngine *transactionManager;
    NSMutableArray *currentData;
    
    NSDictionary *allTransactionData;
}
@end

@implementation statement

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self addShadowToView:_segment_View];
    
    !transactionManager?transactionManager = [[pootEngine alloc]init]:nil;
    [transactionManager setDelegate:self];
    [transactionManager setShowComments:developing];
    
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    [transactionManager startRequestWithURL:[NSString stringWithFormat:@"%@/movements?idUsuario=%@&idioma=%@", walletManagementURL, userInfo[kUserIDKey], NSLocalizedString(@"lang", nil)]];
     
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0], NSFontAttributeName, [UIColor darkMCGreyColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    [[UISegmentedControl appearance] setBackgroundImage:[self imageFromColor:[UIColor darkMCGreyColor]] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:[self imageFromColor:[UIColor clearColor]] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentedValueUpdated_Action:(id)sender {
    UISegmentedControl *segment = sender;
    
    [currentData removeAllObjects];
    if (!([_transactionsTable numberOfSections] == 0)) {
        [_transactionsTable deleteSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
    }
    
    switch ([segment selectedSegmentIndex]) {
        case 0: //Today
            currentData = [NSMutableArray arrayWithArray:allTransactionData[@"today"]];
            break;
        case 1: //Week
            currentData = [NSMutableArray arrayWithArray:allTransactionData[@"week"]];
            break;
        case 2: //Month
            currentData = [NSMutableArray arrayWithArray:allTransactionData[@"month"]];
            break;
        case 3: //Previous Month
            currentData = [NSMutableArray arrayWithArray:allTransactionData[@"previous_month"]];
            break;
        default:
            break;
    }
    
    [self reloadDataInTable];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)reloadDataInTable
{
    if ([currentData count]==0) {
        [_iconView setAlpha:1.0];
        [_iconText setAlpha:1.0];
        
        if (!([_transactionsTable numberOfSections] == 0)) {
            [_transactionsTable deleteSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
        }
    } else {
        if ([_transactionsTable numberOfSections] == 0) {
            [_transactionsTable insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
            [_iconView setAlpha:0.0];
            [_iconText setAlpha:0.0];
        }
    }
}

//TABLEVIEW
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [currentData count]==0?0:1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [currentData count]*2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row%2) {
        NSString *CellIdentifier = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
                switch (label.tag) {
                    case 100:
                        [label setText:[[currentData objectAtIndex:indexPath.row/2] objectForKey:@"ticket"]];
                        break;
                    case 101:
                        [label setText:[[currentData objectAtIndex:indexPath.row/2] objectForKey:@"date"]];
                        break;
                    case 102:
                        [label setText:[NSString stringWithFormat:@"$ %@", [[currentData objectAtIndex:indexPath.row/2] objectForKey:@"total"]]];
                        break;
                    case 103:
                        [label setText:[[currentData objectAtIndex:indexPath.row/2] objectForKey:@"id"]];
                        break;
                    default:
                        break;
                }
            }
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    } else {
        NSString *CellIdentifier = @"space";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row%2) {
        return 105;
    } else {
        return 20;
    }
}


- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == transactionManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            
            allTransactionData = [[NSDictionary alloc] initWithDictionary:response];
            switch ([_segment selectedSegmentIndex]) {
                case 0: //Today
                    currentData = [NSMutableArray arrayWithArray:response[@"today"]];
                    break;
                case 1: //Week
                    currentData = [NSMutableArray arrayWithArray:response[@"week"]];
                    break;
                case 2: //Month
                    currentData = [NSMutableArray arrayWithArray:response[@"month"]];
                    break;
                case 3: //Previous Month
                    currentData = [NSMutableArray arrayWithArray:response[@"previous_month"]];
                    break;
                default:
                    break;
            }
            
            [self reloadDataInTable];
        }
    }
}


@end
