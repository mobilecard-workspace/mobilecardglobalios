//
//  DetailPaymentPEController.h
//  Mobilecard
//
//  Created by Raul Mendez on 7/20/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonViewController.h"
#import "Secure3D_Ctrl.h"
#import <CoreLocation/CoreLocation.h>
#import "paymentGeneralResult.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailPaymentPEController : mT_commonController<Secure3DDelegate>{

__weak IBOutlet UIImageView *imgTarjet;
__weak IBOutlet UILabel *lbl_monto;
__weak IBOutlet UILabel *lbl_propina;
__weak IBOutlet UILabel *lbl_comision;
__weak IBOutlet UILabel *lbl_concepto;
__weak IBOutlet UILabel *lbl_establecimiento;
__weak IBOutlet UILabel *lbl_total;
    
__weak IBOutlet UILabel *lbl_numberTarjet;
    
    NSDictionary *UserDataPE;
    pootEngine *selectManager;
    NSArray* cardToUpdate;
}
- (IBAction)Actioncontinue:(id)sender;
@property int selec_tarjet;
//@property (weak, nonatomic) NSString *establecimiento;
//@property (weak, nonatomic) NSString *comision;
//@property (weak, nonatomic) NSString *amount;
//@property (weak, nonatomic) NSString *concepto;
//@property (weak, nonatomic) NSString *propina;
//@property (weak, nonatomic) NSString *total;

@end

NS_ASSUME_NONNULL_END
