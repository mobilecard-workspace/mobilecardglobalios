//
//  statement.h
//  MobileCard_X
//
//  Created by David Poot on 1/13/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonController.h"

@interface statement : mT_commonController

@property (weak, nonatomic) IBOutlet UITableView *transactionsTable;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *iconText;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet UIView *segment_View;
@end
