//
//  statement_loockfell.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/17/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "statementModel.h"
#import "cardStatementModel.h"
#import "mT_commonController.h"

#import "DetailsStatement.h"
#import "Wallet_Ctrl.h"
NS_ASSUME_NONNULL_BEGIN

@interface statement_loockfell : mT_commonController<UITableViewDelegate,UITableViewDataSource,pootEngineDelegate>{ 
    __weak IBOutlet UITableView *tableviewStatements;
    UIButton *_homeButton;
    UIButton *_myMCButton;
    UIButton *_favoritesButton;
    UIButton *_walletButton;
}

@end

NS_ASSUME_NONNULL_END
