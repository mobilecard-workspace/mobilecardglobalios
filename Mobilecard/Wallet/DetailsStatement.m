//
//  DetailsStatement.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/19/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "DetailsStatement.h"

@interface DetailsStatement (){
    pootEngine *cardManager;
}
    
@end

@implementation DetailsStatement

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     cardManager = [[pootEngine alloc] init];
    
    if (_Item.status == 1) {
        [lbl_trnasacction setText:@"EXITOSA"];
        [lbl_trnasacction setTextColor:[UIColor MCGreenColor]];
        [img_status setImage:[UIImage imageNamed:@"icon_exitoso"]];
    }else{
        [lbl_trnasacction setText:@"RECHAZADA"];
         [lbl_trnasacction setTextColor:[UIColor MCRedColor]];
        [img_status setImage:[UIImage imageNamed:@"icon_rechazado"]];
    }
    
     [lbl_date setText:_Item.date];
     [lbl_ticket setText:_Item.ticket];
    
    
    switch (_Item.idPais) {
           case 1: {  //MEXICO
               
               [lbl_mount_ticket setText:[NSString stringWithFormat:@"$%ld MXN",_Item.importe]];
                  [lbl_importe setText:[NSString stringWithFormat:@"$%ld MXN",_Item.importe]];
                  [lbl_comision setText:[NSString stringWithFormat:@"$%ld MXN",_Item.comision]];
                  [lbl_total setText:[NSString stringWithFormat:@"$%ld MXN",_Item.total]];
             
               break;
           }
               
           case 2:{//COLOMBIA
               
               [lbl_mount_ticket setText:[NSString stringWithFormat:@"$%ld COP",_Item.importe]];
               [lbl_importe setText:[NSString stringWithFormat:@"$%ld COP",_Item.importe]];
               [lbl_comision setText:[NSString stringWithFormat:@"$%ld COP",_Item.comision]];
               [lbl_total setText:[NSString stringWithFormat:@"$%ld COP",_Item.total]];
             
               
               break;
           }
               
           case 3:{//USA
               
               [lbl_mount_ticket setText:[NSString stringWithFormat:@"$%ld USD",_Item.importe]];
               [lbl_importe setText:[NSString stringWithFormat:@"$%ld USD",_Item.importe]];
               [lbl_comision setText:[NSString stringWithFormat:@"$%ld USD",_Item.comision]];
               [lbl_total setText:[NSString stringWithFormat:@"$%ld USD",_Item.total]];
              
               
               break;
           }
               
           case 4:{//PERU
               [lbl_mount_ticket setText:[NSString stringWithFormat:@"S/%ld PEN",_Item.importe]];
               [lbl_importe setText:[NSString stringWithFormat:@"S/%ld PEN",_Item.importe]];
               [lbl_comision setText:[NSString stringWithFormat:@"S/%ld PEN",_Item.comision]];
               [lbl_total setText:[NSString stringWithFormat:@"S/%ld PEN",_Item.total]];
         
               break;
           }
               
           default :{
                [lbl_mount_ticket setText:[NSString stringWithFormat:@"$%ld",_Item.importe]];
                  [lbl_importe setText:[NSString stringWithFormat:@"$%ld",_Item.importe]];
                  [lbl_comision setText:[NSString stringWithFormat:@"$%ld",_Item.comision]];
                  [lbl_total setText:[NSString stringWithFormat:@"$%ld",_Item.total]];
               break;
           
           }
               
       }
    
    
   
    
    
    cardStatementModel *ItemCardtatement =[_Item.dataCard objectAtIndex:0];
    NSString *cardNumber = (NSString*)[cardManager decryptedStringOfString:ItemCardtatement.pan withSensitive:NO];
     [lbl_numberTarjet setText:[NSString stringWithFormat:@"%@ **** **** %@", [cardNumber substringWithRange:NSMakeRange(0, 4)], [cardNumber substringWithRange:NSMakeRange(12, [cardNumber length]==15?3:4)]]];
    
    [lbl_nameTarjet setText:ItemCardtatement.nombre];
    
    
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height/2+self.view.bounds.size.height/4,self.view.bounds.size.width, 150)];
    footerView.backgroundColor =[UIColor whiteColor];
    // [footerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"profile"]]];
    
    _homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
    [_homeButton addTarget:self action:@selector(back_Action:) forControlEvents:UIControlEventTouchUpInside];
    _walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
    
    _favoritesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
    // [_favoritesButton addTarget:self action:@selector(showFavorites:) forControlEvents:UIControlEventTouchUpInside];
    
    _myMCButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_history", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
    // [_myMCButton addTarget:self action:@selector(showHistory:) forControlEvents:UIControlEventTouchUpInside];
    
    [_homeButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*1-40+10, 20, 58, 60)];
    [_walletButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*2.3-40+10, 20, 58, 60)];
    [_favoritesButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*3.6-40+10, 20, 60, 60)];
    [_myMCButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*5-40+10, 20, 60, 60)];
    
    [footerView addSubview:_walletButton];
    [footerView addSubview:_homeButton];
    [footerView addSubview:_favoritesButton];
    [footerView addSubview:_myMCButton];
    [self.view addSubview:footerView];
    
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
