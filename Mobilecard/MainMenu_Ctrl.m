//
//  MainMenu_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 10/26/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#define _rowHeigth @"75"

#import "MainMenu_Ctrl.h"
#import "Wallet_Ctrl.h"


#import "AppDelegate.h"
#import <IngoSDK/IngoSDK.h>
#import <IngoSDK/IngoBranding.h>
#import "payment_recarga_mobilecard_Toll.h"

#import "NSDictionary (keychain).h"

#import "colombia_TopUp_Step00.h"

#import "MCPanel_Ctrl.h"
#import <Mobilecard-Swift.h>


//DEBUG
//#define PartnerConnectId @"Api.ConnectId.IOS.050516.192907@chexar.com"
//#define PartnerConnectToken @"NDRhY2E5MGItZTE4My00YjVjLWIzZTQtZGE4MDZhODI0NTZi"

//PRODUCCION
#define PartnerConnectId @"Api.ConnectId.IOS.020817.163428@ingomoney.com"
#define PartnerConnectToken @"MmJjOWVjMTktMGI2Yi00NDEzLTk0YjktOGQxNWE3ZDM5Mzc3"


@interface MainMenu_Ctrl ()
    {
        NSMutableArray *menuItems;
        
        pootEngine *requestSessionData;
        pootEngine *cardManager;
        pootEngine *cardManagerUpdate;
        pootEngine *IngoCardManager;
        pootEngine *linkCardManager;
        pootEngine *profileManager;
        pootEngine *resendManager;
        pootEngine *mcCardManager;
        pootEngine *shiftManager;
        pootEngine *shiftCredentialsManager;
        pootEngine *colManager;
        pootEngine *USALocManager;
        pootEngine *loginManager;
        pootEngine *amountManager;
        
         pootEngine *endPointTermsCondition;
        
        NSMutableArray *ArrayToSend;
        NSDictionary *sessionData;
        
        NSString *phoneNumber;
        NSString *customerID;
        
        //ShiftSession *session;
        
        NSDictionary *userDataPoints;
        NSDictionary *colombiaServicesData;
        
        UIView* tutorialview;
    }
    
    @property (strong, nonatomic) NSMutableData *responseData;
    
    @end

@implementation MainMenu_Ctrl
    
- (void)viewDidLoad {
    [super viewDidLoad];
    

    //parche para evitar que se combinen las sesiones de usuario y comercio
    if (![DataUser.USERTYPE  isEqual: @"USUARIO"]){
        
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        
        [DataUser clean];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
        
    }//termina parche
    

    
    // [self addShadowToView:self.navigationController.navigationBar];
    
    menuItems = [[NSMutableArray alloc] init];
    
    [self updateMenu];
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate setCurrentNav:self.navigationController];
    
   
    
    //  [self verifyStatus];
    
  //  [self setupMenu];
    
    //actualiza las tarjetas desde que entramos , fix para cuando cambian de cuenta que podias usar las pasadas
    cardManagerUpdate = [[pootEngine alloc] init];
    [cardManagerUpdate setDelegate:self];
    [cardManagerUpdate setShowComments:developing];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    __CardsInit = @"active";
    [cardManagerUpdate startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
    
  //  [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    
}
- (void)viewDidAppear:(BOOL)animated
    {
        
      
        
        

        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:(NSString*)kUSALocation];
           [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        
       [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Amount"];
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        
        
        
        
        
        //registramos el token device de firestore para las push
        NSString *userId = [userData[@"ideUsuario"] stringValue];
        
        NSString *name = userData[@"usrNombre"];
        NSString *lastName = userData[@"usrApellido"];
        NSString *lastNameMother = userData[@"usrMaterno"];
        NSString *userEmail = userData[@"eMail"];
       
        
        NSString *typeUser = @"USUARIO";
        

        
        DataUser.COUNTRYID = [NSString stringWithFormat:@"%d",[[userData objectForKey:kUserIdCountry] intValue]];
                   
        // [[userData objectForKey:kUserIdCountry] stringValue];
        
        [SendTokenFirestore sendWithIdUser:userId typeUser:typeUser idPais:[NSString stringWithFormat:@"%@", DataUser.COUNTRYID]];
        
        DataUser.NAME = name;
        DataUser.LASTNAME = lastName;
        DataUser.LASTNAMEMOTHER = lastNameMother;
        DataUser.USERIDD = userId;
        DataUser.EMAIL = userEmail;
       
        
        switch ([[userData objectForKey:kUserIdCountry] intValue]) {
            case 2: //COLOMBIA
            {
                colManager = [[pootEngine alloc] init];
                [colManager setDelegate:self];
                [colManager setShowComments:developing];
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                [params setObject:[NSNumber numberWithInt:idApp] forKey:@"idAplicacion"];
                [params setObject:@"0" forKey:@"idProducto"];
                [params setObject:@"0" forKey:@"idProveedor"];
                [params setObject:@"0" forKey:@"idRecarga"];
                [params setObject:@"0" forKey:@"idServicio"];
                [params setObject:@"0" forKey:@"idUsuario"];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:colHeader, @"Authorization", nil];
                
                [colManager startJSONRequestWithURL:WSColombiaGetData withPost:JSONString andHeader:header];
                
                [self lockViewWithMessage:nil];
            }
            break;
            
            case 3:
            {
              
            }
            break;
            
            default:
            break;
        }
        
        [super viewDidAppear:animated];
    }
    
- (void)updateMenu
    {
     //   [_walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
      //  [_myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_myMC_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
      //  [_favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        //PERSISTENT KEYCHAIN USAGE!
        //NSDictionary *dataToStore = [NSDictionary dictionaryWithDictionary:userData];
        //[dataToStore deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
        //[dataToStore storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
        //NSLog(@"PERSISTENT ACTIVATED!");
        
        
        [menuItems removeAllObjects];
        switch ([userData[kUserIdCountry] intValue]) {
            case 1: //MEXICO
            {
                [_myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_history", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
                
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"165", kHeightKey, nil]];
                
                 [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPay", kIdentifierKey, _rowHeigth, kHeightKey, nil]];
                   [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_MXtransfers", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                
                 [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                
                //[menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                //[menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_CheckOut", kIdentifierKey, @"54", kHeightKey, nil]];
             
            
                 [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_mobiletag", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
               
                
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_mobilecard", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                
                 [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"30", kHeightKey,nil]];
                
                [_myMCButton setHidden:NO];
                //            [_walletButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*1-40+10, 10, 80, 80)];
                //            [_myMCButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*3-40+10, 10, 80, 80)];
                //            [_favoritesButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*5-40+10, 10, 80, 80)];
                
                [_homeButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*1-40+10, 20, 58, 60)];
                [_walletButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*2.3-40+10, 20, 58, 60)];
                [_favoritesButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*3.6-40+10, 20, 60, 60)];
                [_myMCButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*5-40+10, 20, 60, 60)];
                
                //[_myMCButton setHidden:YES];
                //[_walletButton setFrame:CGRectMake((self.view.frame.size.width/2)-100, 10, 80, 80)];
                //[_favoritesButton setFrame:CGRectMake((self.view.frame.size.width/2)+20, 10, 80, 80)];
            }
            break;
            case 2: //COLOMBIA
            {
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"165", kHeightKey, nil]];
             //   [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPay", kIdentifierKey, _rowHeigth, kHeightKey, nil]];
              //  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
              //  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_transfers", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
              //  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
             //   [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_recargaTeCO", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
             //   [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_soatCO", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_serviciosCO", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                                          [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_prepagadasCO", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                                         [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
           
        
                
                                  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_recargaTiCO", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                
                    
                
           
                
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_mobilecard", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                
                 [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"30", kHeightKey,nil]];
                
                [_myMCButton setHidden:YES];
                [_walletButton setFrame:CGRectMake((self.view.frame.size.width/2)-100, 10, 80, 80)];
                [_favoritesButton setFrame:CGRectMake((self.view.frame.size.width/2)+20, 10, 80, 80)];
            }
            break;
            case 3: //USA
            {
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"165", kHeightKey, nil]];
                
                    //  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPay", kIdentifierKey, _rowHeigth, kHeightKey, nil]];
                  // [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                
             //   if ([[NSUserDefaults standardUserDefaults] boolForKey:(NSString*)kUSALocation]) {
                    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_cash", kIdentifierKey, _rowHeigth, kHeightKey, nil]];
                    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
               // }
             
          
                
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_transfers", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                //[menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_transfers", kIdentifierKey, @"54", kHeightKey,nil]];
                //[menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_billpayUSA", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_mobilecard", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                 [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"30", kHeightKey,nil]];
                /*
                 [_myMCButton setHidden:NO];
                 [_walletButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*1-40+10, 10, 80, 80)];
                 [_myMCButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*3-40+10, 10, 80, 80)];
                 [_favoritesButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*5-40+10, 10, 80, 80)];
                 */
                
                [_myMCButton setHidden:YES];
                [_walletButton setFrame:CGRectMake((self.view.frame.size.width/2)-100, 10, 80, 80)];
                [_favoritesButton setFrame:CGRectMake((self.view.frame.size.width/2)+20, 10, 80, 80)];
                
            }
            break;
            case 4: //PERU
            {
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"165", kHeightKey, nil]];
                //[menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_transfers", kIdentifierKey, @"54", kHeightKey,nil]];
                //[menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPayPE", kIdentifierKey, _rowHeigth, kHeightKey, nil]];

                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_transfers", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                
                
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_mobilecard", kIdentifierKey, _rowHeigth, kHeightKey,nil]];
                
                 [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"30", kHeightKey,nil]];
                /*
                 [_myMCButton setHidden:NO];
                 [_walletButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*1-40+10, 10, 80, 80)];
                 [_myMCButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*3-40+10, 10, 80, 80)];
                 [_favoritesButton setFrame:CGRectMake(((self.view.frame.size.width-20)/6)*5-40+10, 10, 80, 80)];
                 */
                
                [_myMCButton setHidden:YES];
                [_walletButton setFrame:CGRectMake((self.view.frame.size.width/2)-100, 10, 80, 80)];
                [_favoritesButton setFrame:CGRectMake((self.view.frame.size.width/2)+20, 10, 80, 80)];
                
            }
            default:
                break;
        }
        
        [_menuTableView reloadData];
    }
    
- (void)viewWillAppear:(BOOL)animated
    {
        [self updateMenu];
        [super viewWillAppear:YES];
        
      
            
        //solo aparece el tutorial cuando tiene valor (cuando se registran)
        if (DataUser.REGISTERTUTOENABLE != nil){
            
            DataUser.REGISTERTUTOENABLE = nil;
            tutorialview = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width, self.view.bounds.size.height)];
            UIImage *image = [UIImage imageNamed:@"Img_tutorial"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            imageView.frame =CGRectMake(0,0, self.view.bounds.size.width, self.view.bounds.size.height);
            [tutorialview addSubview:imageView];
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageViewDetected)];
            singleTap.numberOfTapsRequired = 1;
            [imageView setUserInteractionEnabled:YES];
            [imageView addGestureRecognizer:singleTap];
            
            [self.view addSubview:tutorialview];
            
            
           
            
        }
            
        
    }
-(void)tapImageViewDetected{
    NSLog(@"single Tap on imageview");
    [tutorialview removeFromSuperview];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"register_look_and_feel" bundle:nil];
    UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"register_step_3"];
    [self.navigationController pushViewController:controller animated:YES];
    
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    
#pragma Mark UITableView Implementation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        return [menuItems count];
    }
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        return 1;
    }
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        return [menuItems[indexPath.row][kHeightKey] floatValue];
    }
    
- (InsetCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:menuItems[indexPath.row][kIdentifierKey]];
        
        
        // [self addShadowToView:cell];
        
        if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"Header"]) {
            UILabel *label = cell.contentView.subviews[0];
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];;
            [label setText:[NSString stringWithFormat:NSLocalizedString(@"Bienvenido %@", nil), userData[kUserName]]];
            
          //  UIButton *button = cell.contentView.subviews[1];
        //    [button addTarget:self action:@selector(profileAction:) forControlEvents:UIControlEventTouchUpInside];
            
             UIImageView *flagImageView = cell.contentView.subviews[3];
            flagImageView.layer.cornerRadius = flagImageView.frame.size.width / 2;
            flagImageView.clipsToBounds = true;
            
            switch ([[userData objectForKey:kUserIdCountry] intValue]) {
                case 1: //Mexico
                {
            
                  [flagImageView setImage:[UIImage imageNamed:@"Mex_flag"]];
            
                }
                    break;
                case 2: //Colombia
                {
                    
                    [flagImageView setImage:[UIImage imageNamed:@"Col_flag"]];
                    
                }
                    break;
                case 3: //Estado Unidos
                {
                    
                    [flagImageView setImage:[UIImage imageNamed:@"Usa_flag"]];
                    
                }
                    break;
                case 4: //Peru
                {
                    
                    [flagImageView setImage:[UIImage imageNamed:@"Per_flag"]];
                    
                }
                    break;
                default:
                {
                    
                }
                    break;
            }
                    
            UIImageView *profileImageView = cell.contentView.subviews[1];
            //   [profileImageView setImage:[UIImage imageNamed:@"profile"]];
            //                    NSData* data = [[NSData alloc] initWithBase64EncodedString:userData[kImgUser] options:0];
            //                    UIImage* image = [UIImage imageWithData:data];
            //                    [profileImageView setImage:image];
            if (userData[kImgUser] != NULL &&  ![userData[kImgUser]  isEqual: @""]){
            [profileImageView setImage:[self decodeBase64ToImage:userData[kImgUser]]];
                profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2;
               profileImageView.clipsToBounds = YES;
           // profileImageView.clipsToBounds = true;
            }else{
                [profileImageView setImage:[UIImage imageNamed:@"icon_perfil"]];
            }
            
            
        }
        
        return cell;
    }
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"closeViewBar" object:nil];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        InsetCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        
        selectedCellItem = selectedCell;
        
        
        if (indexPath.row == 0){
            NSLog(@"se presiono la imagen de perfil");
             [SendTokenFirestore getPhotoWithView:self];
        }else{
        [self CallEndpointSession];
        }
        
       /* if ([self verifyStatus]) {
            if ([[selectedCell reuseIdentifier] isEqualToString:@"side_billpay"]) {
                [self startBillPay];
            }
            if ([[selectedCell reuseIdentifier] isEqualToString:@"side_topup"]) {
                [self startTopUp];
            }
            if ([[selectedCell reuseIdentifier] isEqualToString:@"side_cash"]) {
                [self startCash];
            }
            if ([[selectedCell reuseIdentifier] isEqualToString:@"side_transfers"]) {
                [self startTransfersModule];
            }
            if ([[selectedCell reuseIdentifier] isEqualToString:@"side_MXtransfers"]) {
                [self startMXTransfersModule];
            }
            if ([[selectedCell reuseIdentifier] isEqualToString:@"side_CheckOut"]) {
                [self startCheckOutModule];
            }
            if ([[selectedCell reuseIdentifier] isEqualToString:@"side_ScanNPay"]) {
                [self startScanNPay];
            }
            if ([[selectedCell reuseIdentifier] isEqualToString:@"side_ScanNPayPE"]) {
                [self startScanNPay];
            }
            
            
        }*/
    }
    
-(void)CallEndpointSession{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
        
        [self.menuTableView reloadData];
        
        
        
        if ([self verifyStatus]) {
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_billpay"] || [[selectedCellItem reuseIdentifier] isEqualToString:@"side_billpayUSA"] ) {
                [self startBillPay];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_topup"]) {
                [self startTopUp];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_cash"]) {
                [self startCash];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_transfers"]) {
                [self startTransfersModule];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_MXtransfers"]) {
                [self startMXTransfersModule];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_EUAtransfers"]) {
                
                
                /*
                 UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Estamos mejorando esta funcionalidad. No está disponible temporalmente", nil) preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                 
                 }];
                 
                 [alertMsg addAction:ok];
                 
                 [self presentViewController:alertMsg animated:YES completion:nil];
                 */
                
                [self  sendTransfersToOrderView];
                
                
                
                
            }
            
            
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_CheckOut"]) {
                [self startCheckOutModule];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_ScanNPay"]) {
                [self startScanNPay];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_ScanNPayPE"]) {
                [self startScanNPay];
            }
            //aqui se agregan las de colombia
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_recargaTiCO"]) {
                [self startRecargaTiempoCO];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_recargaTeCO"]) {
                [self startRecargaTeleCO];
            }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_soatCO"]) {
                [self startSoatCO];
            }
            
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_mobilecard"]) {
                [self startMiMobilecard];
            }
            
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_serviciosCO"]) {
                           [self startServicesCO];
                       }
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_prepagadasCO"]) {
                           [self startPrepageCO];
                       }
            
            
            if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_mobiletag"]) {
                [self startMiMobileTag];
            }
            
            
            
        }
        
        
        /*
        loginManager = [[pootEngine alloc] init];
        [loginManager setDelegate:self];
        [loginManager setShowComments:developing];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        [params setObject:userDetails[kUserEmail] forKey:@"usrLoginOrEmail"];
        [params setObject:[loginManager encryptJSONString:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] withPassword:nil] forKey:kUserPassword];
        [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
        [params setObject:@"Apple" forKey:@"manufacturer"];
        [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
        [params setObject:@"iOS" forKey:@"platform"];
        [params setObject:@"USUARIO" forKey:@"tipoUsuario"];
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    
    */
    }
}


- (void)cashCheque
{
    
    

     if ([[NSUserDefaults standardUserDefaults] boolForKey:(NSString*)kUSALocation] == NO) {
              
              UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Para hacer uso de esta funcionalidad se requiere contar con conexión de Internet o Datos de Estados Unidos." preferredStyle:UIAlertControllerStyleAlert];
                       
                       UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                      
                       }];
                       
                       [alertMsg addAction:ok];
                        [self presentViewController:alertMsg animated:YES completion:nil];
              
              
              return;
          }
          
    [self verifyStatus]?[self callIngo]:nil;
    
    
}

    
- (void)startCash
    {
        
        USALocManager = [[pootEngine alloc] init];
                      [USALocManager setDelegate:self];
                      [USALocManager setShowComments:developing];
                      [USALocManager setTag:987];
                      
                      [USALocManager startWithoutPostRequestWithURL:@"https://api.spykemobile.net/Ingo.Server.Status.Service/api/SystemAvailability"];
                      [self lockViewWithMessage:@"Procesando solicitud..."];
        
        
     
        
      
        /*
         UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Estamos mejorando esta funcionalidad. No está disponible temporalmente", nil) preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
         [alertMsg dismissViewControllerAnimated:YES completion:nil];
         }];
         
         [alertMsg addAction:ok];
         
         [self presentViewController:alertMsg animated:YES completion:nil];
         */
        
        
      
    }
- (void)profileAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
    UITableViewController *scrollview =(UITableViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ProfileUser"];
    [self.navigationController pushViewController:scrollview animated:YES];
}
- (void)startTopUp
    {
        if ([self verifyStatus]) {
            
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            switch ([[userData objectForKey:kUserIdCountry] intValue]) {
                case 2: //COLOMBIA
                {
                    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"colombia_TopUp" bundle:nil];
                    colombia_TopUp_Step00 *nextView = [nextStory instantiateViewControllerWithIdentifier:@"topup"];
                    colombiaServicesData?[nextView setServiceData:colombiaServicesData[@"RECARGAS"][0]]:nil;
                    
                    [self.navigationController pushViewController:nextView animated:YES];
                }
                return;
                break;
                case 4://PERU
                {
                    
                    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                    }];
                    
                    [alertMsg addAction:ok];
                    
                    [self presentViewController:alertMsg animated:YES completion:nil];
                    
                }
                    return;
                    break;
                case 3: {//USA
                    
                   UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Febrero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
                                   
                                   UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                       
                                   }];
                                   
                                   [alertMsg addAction:ok];
                                   
                                   [self presentViewController:alertMsg animated:YES completion:nil];
                    
                     return;
                [self performSegueWithIdentifier:@"side_topup_USA" sender:nil];
                    
                }
                   return;
                break;
                default: //MX & all
                [self performSegueWithIdentifier:@"side_topup" sender:nil];
                break;
            }
        }
        
    }
    
- (void)startBillPay
    {
        if ([self verifyStatus]) {
            
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            switch ([[userData objectForKey:kUserIdCountry] intValue]) {
                case 2: //COLOMBIA
                {
                    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"colombia_TopUp" bundle:nil];
                    colombia_TopUp_Step00 *nextView = [nextStory instantiateViewControllerWithIdentifier:@"services"];
                    colombiaServicesData?[nextView setServiceData:colombiaServicesData]:nil;
                    
                    [self.navigationController pushViewController:nextView animated:YES];
                }
                break;
                    
                case 4://PERU
                {
                    
                    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                    }];
                    
                    [alertMsg addAction:ok];
                    
                    [self presentViewController:alertMsg animated:YES completion:nil];
                    
                    
            }
                    break;
                case 3:{
                    

                    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Febrero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
                                   
                                   UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                       
                                   }];
                                   
                                   [alertMsg addAction:ok];
                                   
                                   [self presentViewController:alertMsg animated:YES completion:nil];
                    
                    return;
                        [self performSegueWithIdentifier:@"side_billpay" sender:nil];
                    
                }
                    
                    break;
                default:
                {
                    [self performSegueWithIdentifier:@"side_billpay" sender:nil];
                }
                break;
            }
        }
    }

- (BOOL)verifyStatus
    {
     /*
        ///// kUserDetailsKey  Raul mendez Validando Jumio en verifyStatus
        NSDictionary *response = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        switch ([response[kIDJumio] intValue]) {
            case 0:
            {
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"register_look_and_feel" bundle:nil];
                UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"OnBoarding"];
                [self.navigationController pushViewController:controller animated:YES];
                
                return NO;
            }
            break;
            
            case 1:
            {
                switch ([response[kIDStatus] intValue]) {
                    case 99:
                    {
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
                        [alertMsg setTag:99];
                        [alertMsg show];
                        return NO;
                    }
                    break;
                    ////////////////////  Validación de idUsrStatus   Raul Mendez ///////////////////////////
                    case 100:
                    {
                        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
                            UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
                            Register_Step30 *nextView = [[nav viewControllers] firstObject];
                            
                            [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                            [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                            
                            [self.navigationController presentViewController:nav animated:YES completion:nil];
                            
                            [alertMsg dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                        }];
                        
                        [alertMsg addAction:ok];
                        [alertMsg addAction:cancel];
                        
                        [self presentViewController:alertMsg animated:YES completion:nil];
                        return NO;
                    }
                    break;
                }
                
            }
            break;
            case 2:
            {
                //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
                
                return NO;
            }
            break;
            case 3:
            {
                [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
                return NO;
            }
            break;
        }
        
        //    if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 99)) {
        //        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
        //        [alertMsg setTag:99];
        //        [alertMsg show];
        //        return NO;
        //    }
        //
        //    if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 100)) {
        //        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
        //            UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
        //            Register_Step30 *nextView = [[nav viewControllers] firstObject];
        //
        //            [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
        //            [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
        //
        //            [self.navigationController presentViewController:nav animated:YES completion:nil];
        //
        //            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        //        }];
        //
        //        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        //        }];
        //
        //        [alertMsg addAction:ok];
        //        [alertMsg addAction:cancel];
        //
        //        [self presentViewController:alertMsg animated:YES completion:nil];
        //
        //        return NO;
        //    }*/
        return YES;
    }
    
- (void) callIngo
    {
        
        
        
    
        
        
        requestSessionData = [[pootEngine alloc] init];
        [requestSessionData setShowComments:developing];
        [requestSessionData setDelegate:self];
        
        NSString *userpass = [requestSessionData encryptJSONString:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] withPassword:nil];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        //[params setObject:userpass forKey:@"pass"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
        [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"deviceId"];
        [params setObject:@"IOS" forKey:@"plataforma"];
        //[params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
        
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"%@", JSONString];
        
           [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        
        [requestSessionData startBasicAuthWithUsername:@"guest" password:@"guest" andParameters:post forWS:WSGetSessionData];
        
     
    }
    
#pragma Mark Actions

- (IBAction)homeAction:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"closeViewBar" object:nil];
}


- (IBAction)openWallet_Action:(id)sender {
    if ([self verifyStatus]) {
        @try {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"closeViewBar" object:nil];
            
            cardManager = [[pootEngine alloc] init];
            [cardManager setDelegate:self];
            [cardManager setShowComments:developing];
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
            
         //   UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
         //  [alertMsg show];
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
            [DataUser clean];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
    
- (IBAction)openMyMC_Action:(id)sender {
    if ([self verifyStatus]) {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"statement" bundle:nil];
        
        [self.navigationController pushViewController:[nextStory instantiateInitialViewController] animated:YES];
    }
    //    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    //    switch ([userData[kUserIdCountry] intValue]) {
    //        case 1: //MEXICO
    //        {
    //            //[self openWallet_Action:nil];
    //            mcCardManager = [[pootEngine alloc] init];
    //            [mcCardManager setDelegate:self];
    //            [mcCardManager setShowComments:developing];
    //
    //            [mcCardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@&pais=%@", WSWalletGetCustomCard,userData[kUserIDKey], NSLocalizedString(@"lang", nil), userData[kUserIdCountry]] withPost:nil];
    //
    //            [self lockViewWithMessage:nil];
    //        }
    //            break;
    //        case 3: //USA
    //        {
    //            shiftManager = [[pootEngine alloc] init];
    //            [shiftManager setDelegate:self];
    //            [shiftManager setShowComments:developing];
    //
    //            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    //
    //            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    //            [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
    //            [params setObject:@"IOS" forKey:@"plataforma"];
    //
    //            NSError *JSONError;
    //            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    //            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    //
    //            NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:[self encryptString:[NSString stringWithFormat:@"%@%d%@", userData[kUserIDKey], idApp, @"IOS"]], @"token", nil];
    //
    //            [shiftManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/Shift/%d/%@/%@/CheckUser?idioma=%@&plataforma=%@", simpleServerURL, idApp, userData[kUserIDKey], userData[kUserIdCountry], NSLocalizedString(@"lang", nil), @"iOS"] withPost:JSONString andHeader:header];
    //
    //            //
    //
    //            [self lockViewWithMessage:nil];
    //
    //            return;
    //
    //            //---------- PREVIOUS IMPLEMENTATION
    //            /*
    //            @try {
    //                mcCardManager = [[pootEngine alloc] init];
    //                [mcCardManager setDelegate:self];
    //                [mcCardManager setShowComments:developing];
    //
    //                NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    //
    //                [mcCardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
    //
    //                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    //            } @catch (NSException *exception) {
    //                NSLog(@"%@", exception.description);
    //
    //                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    //
    //                [alertMsg show];
    //
    //                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
    //                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
    //                [[NSUserDefaults standardUserDefaults] synchronize];
    //
    //                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    //            }
    //             */
    //        }
    //            break;
    //        default:
    //            break;
    //    }
}
    
- (void) showMyMCSetup
    {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
        UINavigationController *nav = [nextStory instantiateInitialViewController];
        myMC_Step1 *nextView = [[nav childViewControllers] firstObject];
        [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
        
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    
- (void) showMyMCPanel
    {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
        myMC_Step1 *nextView = [nextStory instantiateViewControllerWithIdentifier:@"mcpanel_view"];
        
        [self.navigationController pushViewController:nextView animated:YES];
    }
    
- (void) showWallet
    {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
        
        Wallet_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"wallet_view"];
        
        [self.navigationController pushViewController:nextView animated:YES];
    }
    
- (void) startTransfersModule
    {
        if ([self verifyStatus]) {
            
                  NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            
            //si es colombia y peru no tienen transferencias aun
            if ([[countryInfo objectForKey:kUserIdCountry] intValue] == 2 || [[countryInfo objectForKey:kUserIdCountry] intValue] == 4) {
                
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
                
            }else{
                
                
                /*
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Febrero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                 
                             }];
                             
                             [alertMsg addAction:ok];
                             
                             [self presentViewController:alertMsg animated:YES completion:nil];
                return;
                
                */
            
            
                      [self callEndPointTermsCondition];
                /*
            profileManager = [[pootEngine alloc] init];
            [profileManager setDelegate:self];
            [profileManager setShowComments:developing];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
            
            NSString *post = nil;
            NSError *JSONError;
            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            post = [NSString stringWithFormat:@"json=%@", JSONString];
            
            [profileManager startRequestWithURL:ViamericasSenderProfile withPost:post];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                 */
        }
            
        }
    }
    
- (void) startMXTransfersModule
    {
        
        if ([self verifyStatus]) {
            
            
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"MXTransfers" bundle:nil];
            MXTransfers_Step1 *nextView = [nextStory instantiateInitialViewController];
            
            [self.navigationController pushViewController:nextView animated:YES];
        }
    }


- (void) startEUATransfersModule
{
    
    if ([self verifyStatus]) {
        
        
     /*
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Estamos mejorando esta funcionalidad. No está disponible temporalmente", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        */
             [self  sendTransfersToOrderView];
   
        
    }
}

- (void) startStatement
    {
        if ([self verifyStatus]) {
            
         
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"statement" bundle:nil];
            
            [self.navigationController pushViewController:[nextStory instantiateInitialViewController] animated:YES];
        }
    }
    
- (void) startCheckOutModule
    {
        if ([self verifyStatus]) {
            
            
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"LaCuenta" bundle:nil];
            
            [self.navigationController pushViewController:[nextStory instantiateInitialViewController] animated:YES];
        }
    }
    
- (void) startScanNPay
    {
        if ([self verifyStatus]) {
            
            
            NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            if ([[countryInfo objectForKey:kUserIdCountry] intValue] == 4) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                PECardPressentStep1CommerceViewController *ViewSCANPAY =(PECardPressentStep1CommerceViewController *)[storyboard instantiateViewControllerWithIdentifier:@"cardpressent"];
                [ViewSCANPAY setFromView:2];
                [self.navigationController pushViewController:ViewSCANPAY animated:YES];
            }else if ([[countryInfo objectForKey:kUserIdCountry] intValue] == 2){
                //colombia
                
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
                
                
            }else if ([[countryInfo objectForKey:kUserIdCountry] intValue] == 3){
                //ESTADOS UNIDOS (PARA HABILITAR QUITAR ESTE IF Y DEJAR EL ELSE DE ABAJO)
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
                               
                               UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                   
                               }];
                               
                               [alertMsg addAction:ok];
                               
                               [self presentViewController:alertMsg animated:YES completion:nil];
                
                
            }else{
                
                
                
                
                [SendTokenFirestore whiteListWithView:self type:@"scan"];
                
                //scannea y paga para eua y mex
                //llamamos el servicio que puede dejar pasar a jumio
                
                
             /*   UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"scanNPay" bundle:nil];
                [self.navigationController pushViewController:[nextStory instantiateInitialViewController] animated:YES];*/
            }
        
           
        }
    }
//ahora el de favoritos manda a contacto
- (IBAction)startFavoritesMenu:(UIButton *)sender {
    
    if ([self verifyStatus]) {
               
               [[NSNotificationCenter defaultCenter] postNotificationName:@"closeViewBar" object:nil];
          [self performSegueWithIdentifier:@"side_contact" sender:self];
              // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
               //UITableViewController *scrollview =(UITableViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Favorites"];
              // [self.navigationController pushViewController:scrollview animated:YES];
           }
}

//ahora ya no es favoritos es la seccion de contacto
- (void) startFavorites
    {
        if ([self verifyStatus]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"closeViewBar" object:nil];
            

            [self performSegueWithIdentifier:@"side_contact" sender:self];
          //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
          //  UITableViewController *scrollview =(UITableViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Favorites"];
           // [self.navigationController pushViewController:scrollview animated:YES];
        }
    }

- (void) startRecargaTiempoCO
{
    if ([self verifyStatus]) {
      /*
        //DESACTIVADO TEMPORALMENTE
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        return;
        //TERMINA EL DESACTIVADO TEMPORALMENTE BORRAR PARA ACTIVAR
        */
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ColombiaUserServices"bundle:nil];
        UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"recargasCoMenuIdentifier"];
        [self.navigationController pushViewController:scrollview animated:YES];
    }
}


- (void) startRecargaTeleCO
{
    if ([self verifyStatus]) {
        
        //DESACTIVADO TEMPORALMENTE
     
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        return;
        //TERMINA EL DESACTIVADO TEMPORALMENTE BORRAR PARA ACTIVAR
      
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ColombiaUserServices"bundle:nil];
        UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"recargasTeCoMenuIdentifier"];
        [self.navigationController pushViewController:scrollview animated:YES];
    }
}

- (void) startSoatCO
{
    if ([self verifyStatus]) {
        
        //DESACTIVADO TEMPORALMENTE
       /*
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        return;
        //TERMINA EL DESACTIVADO TEMPORALMENTE BORRAR PARA ACTIVAR
       */
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ColombiaUserServices"bundle:nil];
        UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"soatCoMenuIdentifier"];
        [self.navigationController pushViewController:scrollview animated:YES];
    }
}


- (void) startServicesCO
{
    if ([self verifyStatus]) {
        
        //DESACTIVADO TEMPORALMENTE
       /*
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        return;
        //TERMINA EL DESACTIVADO TEMPORALMENTE BORRAR PARA ACTIVAR
       */
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ColombiaUserServices"bundle:nil];
        UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"serviciosCoMenuIdentifier"];
        [self.navigationController pushViewController:scrollview animated:YES];
    }
}

- (void) startPrepageCO
{
    if ([self verifyStatus]) {
        
        //DESACTIVADO TEMPORALMENTE
       /*
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        return;
        //TERMINA EL DESACTIVADO TEMPORALMENTE BORRAR PARA ACTIVAR
       */
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ColombiaUserServices"bundle:nil];
        UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"prepagoCoMenuIdentifier"];
        [self.navigationController pushViewController:scrollview animated:YES];
    }
}

- (void) startMiMobilecard
{
    if ([self verifyStatus]) {
        
        //aqui checamos que hacer y a donde ir al presionar mimobilecard
        NSLog(@"Se presiono mimobilecard");
        
        
        [SendTokenFirestore miMobilecardWithView:self];
    
        /*
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ColombiaUserServices"bundle:nil];
        UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"soatCoMenuIdentifier"];
        [self.navigationController pushViewController:scrollview animated:YES];
         */
    }
}


- (void) startMiMobileTag
{
    if ([self verifyStatus]) {
        
        //DESACTIVADO TEMPORALMENTE
     /*
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Próximamente Enero 2020.", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        return;
        //TERMINA EL DESACTIVADO TEMPORALMENTE BORRAR PARA ACTIVAR
        */
        //aqui checamos que hacer y a donde ir al presionar mimobilecard
        NSLog(@"Se presiono mimobiletag");
        
        amountManager = [[pootEngine alloc] init];
        [amountManager setDelegate:self];
        [amountManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
      
       // if ([DataUser.COUNTRYID intValue] == 3 ){
            int number = 8;
            [params setObject:[NSNumber numberWithInt:number] forKey:@"idServicio"];
            
        //}else{
            
         //   [params setObject:[NSNumber numberWithInt:idValue] forKey:@"idServicio"];
       // }
        [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
        [params setObject:[NSNumber numberWithInt:[userData[kUserIdCountry] intValue]] forKey:@"idPais"];
        
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        [amountManager startJSONRequestWithURL:WSGetCatalogRecargaMontos withPost:JSONString];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        
        
       
        
        /*
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ColombiaUserServices"bundle:nil];
         UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"soatCoMenuIdentifier"];
         [self.navigationController pushViewController:scrollview animated:YES];
         */
    }
}

    
- (void) callWalletSelection
    {
        [self showCardOptions];
    }
    
- (void) launchIngo
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        if(appDelegate.canRunIngoSdk)
        {
            [IngoSdkManager checkIngoSystemStatus:^(bool isAvailable)
             {
                 //NSLog(@"isAvailable: %i", isAvailable);
                 
                 if(isAvailable)
                 {
                     NSString *ssoToken = self->sessionData[@"ssoToken"];
                     self->customerID = self->sessionData[@"customerId"];
                     
                     [[IngoSdkManager getInstance] beginWithHostViewController:self customerId:self->customerID ssoToken:ssoToken andOnTransactionApprovedHandler:nil];
                     
                 }
                 else
                 {
                     NSLog(@"Ingo is down! Try again later.");
                 }
             }];
        }
        else
        {
            //NSLog(@"Your device cannot run the Ingo SDK! Check the logs for more details.");
        }
    }
    
    
- (void) showCardOptions
    {
        @try {
            IngoCardManager = [[pootEngine alloc] init];
            [IngoCardManager setDelegate:self];
            [IngoCardManager setShowComments:developing];
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
               [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
            
            [IngoCardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetDebitCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
            
         
        } @catch (NSException *exception) {
          //  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
         //   [alertMsg show];
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
            [DataUser clean];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
#pragma Mark pootEngine delegates
    
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
    {
        [self unLockView];
        
        
        if (manager == amountManager) {
            NSDictionary *response = (NSDictionary*)json;
            switch (amountManager.tag) {
                case 10:
                 //   ArrayToSend = [NSMutableArray arrayWithArray:response[@"productos"]];
                    
                  //  [self performSegueWithIdentifier:@"payment_recarga" sender:buttonSender];
                    break;
                    
                default:
                {
                    if ([response[kIDError] intValue] == 0) {
                        ArrayToSend = [NSMutableArray arrayWithArray:response[@"montos"]];
                       
                            
                             [self performSegueWithIdentifier:@"miMobileTagSegue" sender:nil];
                         
                        
                    }
                }
                    break;
            }
        }

        
        
        
        
        if (manager == loginManager) {
            
            
            [self unLockView];
            
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            response  = [self cleanDictionary:response];
            
            [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
            
            [self.menuTableView reloadData];
            
            
            
            if ([self verifyStatus]) {
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_billpay"] || [[selectedCellItem reuseIdentifier] isEqualToString:@"side_billpayUSA"] ) {
                    [self startBillPay];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_topup"]) {
                    [self startTopUp];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_cash"]) {
                    [self startCash];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_transfers"]) {
                    [self startTransfersModule];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_MXtransfers"]) {
                    [self startMXTransfersModule];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_EUAtransfers"]) {
                    
                    
                    /*
                    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Estamos mejorando esta funcionalidad. No está disponible temporalmente", nil) preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                      
                    }];
                    
                    [alertMsg addAction:ok];
                    
                    [self presentViewController:alertMsg animated:YES completion:nil];
                     */
                    
                         [self  sendTransfersToOrderView];
                    
                   
                    
                    
                }
                
                
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_CheckOut"]) {
                    [self startCheckOutModule];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_ScanNPay"]) {
                    [self startScanNPay];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_ScanNPayPE"]) {
                    [self startScanNPay];
                }
                //aqui se agregan las de colombia
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_recargaTiCO"]) {
                    [self startRecargaTiempoCO];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_recargaTeCO"]) {
                    [self startRecargaTeleCO];
                }
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_soatCO"]) {
                    [self startSoatCO];
                }
                
                if ([[selectedCellItem reuseIdentifier] isEqualToString:@"side_mobilecard"]) {
                    [self startMiMobilecard];
                }
                
                
                
            }
            
            
            
        }
        if (requestSessionData == manager) {
            [self unLockView];
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            switch ([response[@"errorCode"] intValue]) {
                case 0:
                {
                    sessionData = [NSDictionary dictionaryWithDictionary:response];
                    
                    [self callWalletSelection];
                }
                break;
                
                case 90:
                {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Atención", nil) message:response[@"errorMessage"] delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Obtener tarjeta", nil),NSLocalizedString(@"Cancelar", nil), nil];
                    [alertMsg setTag:901];
                    [alertMsg show];
                }
                break;
                
                case 91:
                {
                    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Ingo" bundle:nil];
                    
                    UINavigationController *nav = [nextStory instantiateInitialViewController];
                    Ingo_Step10 *nextView = [nav.viewControllers firstObject];
                    [nextView setDelegate:self];
                    
                    [self.navigationController presentViewController:nav animated:YES completion:nil];
                }
                break;
                
                case 92:
                {
                    NSString *myString = response[@"errorMessage"];
                    
                    NSString *myRegex = @"\\d{11}";
                    NSRange range = [myString rangeOfString:myRegex options:NSRegularExpressionSearch];
                    
                    phoneNumber = nil;
                    if (range.location != NSNotFound) {
                        phoneNumber = [myString substringWithRange:range];
                    } else {
                        //NSLog(@"No phone number found");
                    }
                    
                    NSString *finalMsg = response[@"errorMessage"];
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:finalMsg delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Activar ahora", nil), nil];
                    [alertMsg setDelegate:self];
                    [alertMsg setTag:92];
                    [alertMsg show];
                }
                break;
                
                default:
                {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[@"errorMessage"] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
                break;
            }
            
        }
        
        
        
        if (cardManagerUpdate == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
              [self unLockView];
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                if ([(NSMutableDictionary *)response[kUserCardArray] count]==0) {
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                   // UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                   // [alertMsg setTag:90];
                   // [alertMsg show];
                } else {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                    [[NSUserDefaults standardUserDefaults
                      ] synchronize];
                    
                    if  (__CardsInit.length != 0){
                        __CardsInit = @"";
                    }else{
                    [self showWallet];
                    }
                }
            } else {
              //  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
               // [alertMsg show];
            }
        }
        
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
              [self unLockView];
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                if ([(NSMutableDictionary *)response[kUserCardArray] count]==0) {
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg setTag:90];
                    [alertMsg show];
                } else {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                    [[NSUserDefaults standardUserDefaults
                      ] synchronize];
                    
                    if  (__CardsInit.length != 0){
                        __CardsInit = @"";
                    }else{
                    [self showWallet];
                    }
                }
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
        
        if (mcCardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [self previvaleRegisterResponse:response];
            } else {
                double delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                });
                
                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"previvale" bundle:nil];
                UINavigationController *nav;
                if (![[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
                    nav = [nextStory instantiateViewControllerWithIdentifier:@"user"];
                } else {
                    nav = [nextStory instantiateInitialViewController];
                }
                
                previvale_Step10 *nextView = [[nav childViewControllers] firstObject];
                [nextView setDelegate:self];
                [[self navigationController] presentViewController:nav animated:YES completion:nil];
            }
            
            //PREVIOUS IMPLEMENTATION
            /*
             NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
             
             response = [self cleanDictionary:response];
             
             if ([response[kIDError] intValue]==0) {
             if ([response[kUserCardArray] count]==0) {
             [self showMyMCSetup];
             } else {
             [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             for (NSDictionary *cardElement in response[kUserCardArray]) {
             if ([cardElement[@"mobilecard"] boolValue]) {
             [self showMyMCPanel];
             return;
             }
             }
             
             [self showMyMCSetup];
             }
             } else {
             UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
             [alertMsg show];
             }
             */
        }
        
        /*
         if (shiftManager == manager) {
         NSDictionary *response = (NSDictionary*)json;
         
         switch ([response[kIDError] intValue]) {
         case 99: //USER NOT REGISTERED
         {
         UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"shift" bundle:nil];
         UINavigationController *nav = [nextStory instantiateInitialViewController];
         shift_Step00 *nextView = [[nav.navigationController viewControllers] firstObject];
         [nextView setDelegate:self];
         
         [self.navigationController presentViewController:nav animated:YES completion:nil];
         }
         break;
         case 0: //USER REGISTERED
         {
         NSDictionary *response = (NSDictionary*)json;
         
         userDataPoints = [[NSDictionary alloc] initWithDictionary:response[@"data_points"]];
         
         shiftCredentialsManager = [[pootEngine alloc] init];
         
         [shiftCredentialsManager setDelegate:self];
         [shiftCredentialsManager setShowComments:developing];
         
         [shiftCredentialsManager startRequestWithURL:[NSString stringWithFormat:@"%@/Shift/credentials", simpleServerURL]];
         
         [self lockViewWithMessage:nil];
         }
         break;
         default: //ERROR
         {
         UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
         [alertMsg dismissViewControllerAnimated:YES completion:nil];
         }];
         
         [alertMsg addAction:ok];
         
         [self presentViewController:alertMsg animated:YES completion:nil];
         }
         break;
         }
         }
         */
        
        if (manager == profileManager) {
             [self unLockView];
            NSMutableDictionary *response = (NSMutableDictionary*)json;
            response = [self cleanDictionary:response];
            
            
           if ([response[kIDError] intValue]!=0){
              // if ([[response[@"idSender"] stringValue]  isEqual: @""]) {
                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"senderReg" bundle:nil];
                
                UINavigationController *nav = [nextStory instantiateInitialViewController];
                sender_Ctrl *nextView = [nav childViewControllers].firstObject;
                
                [nextView setDelegate:self];
                
                [self.navigationController presentViewController:nav animated:YES completion:nil];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserSenderId] forKey:(NSString*)kUserSenderId];
                
                [self sendTransfersToOrderView];
            }
        }
        
        if (manager == resendManager) {
            NSMutableDictionary *response = (NSMutableDictionary*)json;
            response = [self cleanDictionary:response];
            
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
        
        /*
         if (manager == shiftCredentialsManager) {
         NSDictionary *response = (NSMutableDictionary*)json;
         
         //[[ShiftPlatform defaultManager] initializeWithDeveloperKey:response[@"headerDeveloper"] projectKey:response[@"headerProject"] environment:ShiftPlatformEnvironmentSandbox];
         [ShiftPlatform.defaultManager initializeWithApiKey:response[@"headerProject"] environment:ShiftPlatformEnvironmentSandbox];
         
         if (!session) {
         session = [[ShiftSession alloc] init];
         }
         
         DataPointList *datapoints = [[DataPointList alloc] init];
         
         PersonalName *nameDataPoint = [[PersonalName alloc] initWithFirstName:userDataPoints[@"data"][2][@"first_name"] lastName:userDataPoints[@"data"][2][@"last_name"] verified:NO];
         PhoneNumber *numberDataPoint = [[PhoneNumber alloc] initWithCountryCode:[userDataPoints[@"data"][0][@"country_code"] intValue] phoneNumber:userDataPoints[@"data"][0][@"phone_number"] verified:YES];
         Email *emailDataPoint = [[Email alloc] initWithEmail:userDataPoints[@"data"][1][@"email"] verified:NO notSpecified:YES];
         Address *addressDataPoint = [[Address alloc] initWithAddress:userDataPoints[@"data"][5][@"address"] apUnit:userDataPoints[@"data"][5][@"apt"] countryCode:@"1" countryName:@"USA" city:userDataPoints[@"data"][5][@"city"] region:userDataPoints[@"data"][5][@"state"] zip:userDataPoints[@"data"][5][@"zip"] verified:NO];
         IdDocument *idDocDataPoint = [[IdDocument alloc] initWithDocumentType:IdDocumentTypeSsn value:userDataPoints[@"data"][4][@"ssn"] verified:NO notSpecified:NO];
         
         NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
         [dateFormatter setDateFormat:@"MM-dd-yyyy"];
         
         NSDate *date = [dateFormatter dateFromString:userDataPoints[@"data"][3][@"date"]];
         BirthDate *birthDateDataPoint = [[BirthDate alloc] initWithDate:date verified:NO];
         
         
         [datapoints addWithDataPoint:nameDataPoint];
         [datapoints addWithDataPoint:numberDataPoint];
         [datapoints addWithDataPoint:emailDataPoint];
         [datapoints addWithDataPoint:addressDataPoint];
         [datapoints addWithDataPoint:idDocDataPoint];
         [datapoints addWithDataPoint:birthDateDataPoint];
         
         [self lockViewWithMessage:@"Opening..."];
         
         [session startCardFlowFrom:self mode:ShiftCardModuleModeEmbedded initialUserData:datapoints options:nil completion:^(UIModule *module, NSError *error){
         [self unLockView];
         }];
         }
         */
        if (manager == colManager) {
            NSDictionary *response = (NSDictionary*)json;
            
            colombiaServicesData = [NSDictionary dictionaryWithDictionary:response];
        }
        
        if (manager == IngoCardManager) {
            if (IngoCardManager == manager) {
                [self unLockView];
                NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
                
                response = [self cleanDictionary:response];
                
                if ([response[kIDError] intValue]==0) {
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [self showWalletWithType:walletViewTypeSelectionOnlyDebit];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [self unLockView];
                    [alertMsg show];
                }
            }
        }
        
        if (manager == linkCardManager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
          //   if ([[NSUserDefaults standardUserDefaults]//aqui boolForKey:(NSString*)kUSALocation]) {
            
            response = [self cleanDictionary:response];
            
            if ([response[@"errorCode"] intValue]  == 0) {
                 [self unLockView];
                [self launchIngo];
          /*  } else {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[@"errorMessage"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertMsg addAction:ok];
               [self unLockView];
                [self presentViewController:alertMsg animated:YES completion:nil];
            }*/
                 
             }else{
                 
                 NSString *mensaje = [response[@"errorMessage"] stringValue];
                 [self unLockView];
                 UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:mensaje preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                 }];
                 
                 [alertMsg addAction:ok];
                 [self unLockView];
                  [self presentViewController:alertMsg animated:YES completion:nil];
                 
             }
            
        }
        
        if (manager == USALocManager) {
              [self unLockView];
            NSDictionary *response = (NSDictionary*) json;
            
            if (response[@"serviceAvailable"]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:(NSString*)kUSALocation];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self cashCheque];
            } else {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:(NSString*)kUSALocation];
                [[NSUserDefaults standardUserDefaults] synchronize];
                 [self cashCheque];
            }
            [self updateMenu];
        }
    }
    
#pragma Mark storyBoards
- (void)showWalletWithType:(walletViewType)viewType;
    {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
        UINavigationController *nav = [nextStory instantiateInitialViewController];
        Wallet_Ctrl *view = nav.viewControllers.firstObject;
        [view setType:viewType];
        [view setDelegate:self];
        
        [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
    {
        if ([self verifyStatus]) {
            return YES;
        } else {
            return NO;
        }
    }
    
#pragma mark previvale handler
- (void)previvaleRegisterResponse:(NSDictionary *)response
    {
        if (response) {
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
            MCPanel_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"mcpanel_view"];
            [nextView setMcCardInfo:response[@"card"]];
            
            [[self navigationController] pushViewController:nextView animated:YES];
        }
    }


- (void) callEndPointTermsCondition{
     ///////////// Userdefault para obtener idUser
     NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];

     NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

     [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];

     NSString *post = nil;
     NSError *JSONError;
     NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
     NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
     post = [NSString stringWithFormat:@"json=%@", JSONString];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
            [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasgetTermsCondition andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];

                NSDictionary *json=[NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:NSJSONReadingMutableLeaves
                                    error:nil];

                if ([json isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *response = (NSDictionary*)json;
                    if ([response[kIDError] intValue] == 0) {
                        if (![[response objectForKey:@"userTerms"] boolValue]) {

                            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Viamericas" bundle:nil];

                            UINavigationController *nav = [nextStory instantiateInitialViewController];
                            TermsConditionsController *nextView = [nav childViewControllers].firstObject;
                            [nextView setObj_condition:self];

                            [self.navigationController presentViewController:nav animated:YES completion:nil];
                        }else{
                            [self callendPointProfileManager];
                        }
                    }
                }
            [self unLockView];
            }];
        });
    });
}
    

-(void) callendPointProfileManager{
    //    profileManager = [[pootEngine alloc] init];
    //    [profileManager setDelegate:self];
    //    [profileManager setShowComments:developing];
    //
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    // [profileManager startRequestWithURL:ViamericasSenderProfile withPost:post];
    //    [profileManager startBasicAuthWithUsername:MobilcerdUser password:MobilcardPassword andParameters:post forWS:ViamericasSenderProfile];
    //
    //    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [CallEndPointDelegate makePOSTRequestUrlAutenticationBase:ViamericasSenderProfile andParams:post User:MobilcerdUser Password:MobilcardPassword completion:^(NSString *responseDict){
                NSData* data = [responseDict dataUsingEncoding:NSUTF8StringEncoding];
                
                NSDictionary *json=[NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:NSJSONReadingMutableLeaves
                                    error:nil];
                
                if ([json isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *response = (NSDictionary*)json;
                    NSLog(@"Json: %@",response);
                    ///////////////////// Raul Mendez ID_SENDER
                    if ([response[kIDError] intValue]!=0){
                        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"senderReg" bundle:nil];
                        
                        UINavigationController *nav = [nextStory instantiateInitialViewController];
                        sender_Ctrl *nextView = [nav childViewControllers].firstObject;
                        
                        [nextView setDelegate:self];
                        
                        [self.navigationController presentViewController:nav animated:YES completion:nil];
                    } else {
                        [[NSUserDefaults standardUserDefaults] setObject:response[kUserSenderId] forKey:(NSString*)kUserSenderId];
                        
                        [self sendTransfersToOrderView];
                    }
                }
            }];
        });
    });
    
}

#pragma Mark card delegate Manager
    
- (void)cardSelectionResult:(NSDictionary *)selectedCard
    {
        linkCardManager = [[pootEngine alloc] init];
        [linkCardManager setDelegate:self];
        [linkCardManager setShowComments:developing];
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [params setObject:selectedCard[@"domAmex"] forKey:@"addressLine1"];
        [params setObject:@"" forKey:@"addressLine2"];
        [params setObject:[NSString stringWithFormat:@"%@_%@", userData[kUserName], selectedCard[@"tipo"]] forKey:@"cardNickname"]; //DE DONDE SALE?
        [params setObject:selectedCard[@"pan"] forKey:@"cardNumber"];
        [params setObject:@"" forKey:@"city"]; //DE DONDE SALE?
        [params setObject:sessionData[@"customerId"] forKey:@"customerId"];
        [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"diviceId"];
        [params setObject:selectedCard[@"vigencia"] forKey:@"expirationMonthYear"];
        [params setObject:selectedCard[@"nombre"] forKey:@"nameOnCard"];
        [params setObject:userData[@"usrIdEstado"] forKey:@"state"]; //DE DONDE SALE?
        [params setObject:selectedCard[@"cpAmex"] forKey:@"zip"];
        
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        [linkCardManager startJSONRequestWithURL:WSSetEnrollCardToCustomer withPost:JSONString];
        
      //  [self lockViewWithMessage:nil];
    }
    
- (void)cardSelectionAborted
    {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Para usar el módulo 'Cambia tu cheque' es necesario ligar una tarjeta de débito", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        
       
    }
    
#pragma mark UIAlertView delegate
    
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
    {
        switch (alertView.tag) {
            case 90:
            {
                [self showWallet];
                return;
            }
            break;
            case 901:
            {
                switch (buttonIndex) {
                    case 0:
                    {
                        [self showMyMCSetup];
                    }
                    break;
                    
                    default:
                    break;
                }
                break;
            }
            case 92:
            {
                switch (buttonIndex) {
                    case 0:
                    break;
                    case 1:
                    {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:+%@", phoneNumber]]];
                    }
                    break;
                    
                    default:
                    break;
                }
            }
            break;
            case 99:
            {
                switch (buttonIndex) {
                    case 0:
                    break;
                    case 1:
                    {
                        resendManager = [[pootEngine alloc] init];
                        [resendManager setDelegate:self];
                        [resendManager setShowComments:developing];
                        
                        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                        
                        [resendManager startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/", userData[kUserIDKey], @"/activate"]];
                        
                        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                    }
                    break;
                    
                    default:
                    break;
                }
            }
            
            default:
            break;
        }
    }
    
#pragma mark ShiftDelegate
    
- (void)shiftCreation_Result:(NSDictionary *)result
    {
        userDataPoints = result[@"data_points"];
        
        shiftCredentialsManager = [[pootEngine alloc] init];
        
        [shiftCredentialsManager setDelegate:self];
        [shiftCredentialsManager setShowComments:developing];
        
        [shiftCredentialsManager startRequestWithURL:[NSString stringWithFormat:@"%@/Shift/credentials", simpleServerURL]];
        
        [self lockViewWithMessage:nil];
    }


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"miMobileTagSegue"]) {
        payment_recarga_mobilecard_Toll *nextView = [segue destinationViewController];
        
        UIButton *button = sender;
        [nextView setType:button.tag==0?serviceTypeIAVE:serviceTypePASE];
      //  [nextView setDataSource:categoriesSubmenuInfo[button.tag]];
        [nextView setAmountDataSource:[NSArray arrayWithArray:ArrayToSend]];
    }
    
}
#pragma mark senderDelegate
    
- (void)senderCreationResult:(id)result
    {
        if (result) {
            [[NSUserDefaults standardUserDefaults] setObject:result forKey:(NSString*)kUserSenderId];
            
            [self sendTransfersToOrderView];
            //[self performSegueWithIdentifier:@"transfers" sender:button];
        }
    }
    
- (void)sendTransfersToOrderView
    {
        
        [SendTokenFirestore whiteListWithView:self type:@"transfer"];
       
    }
    
#pragma mark ingoEnrollment Delegate
- (void)ingoEnrollResult:(NSDictionary *)enrollData
    {
        sessionData = enrollData;
        
        [self callWalletSelection];
    }
    
    
#pragma mark <NSURLConnectionDelegate>
    
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
    {
        //NSLog(@"Failed: %@", error);
    }
    
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
    {
        _responseData = [NSMutableData data];
    }
    
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
    {
        [_responseData appendData:data];
    }
    
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
    {
        [self processResponseData:_responseData];
    }
    
- (void)processResponseData:(NSData *)data
    {
        NSError *error;
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if(!jsonResponse[@"ssoToken"] || [jsonResponse[@"ssoToken"] length] == 0)
        {
            //NSLog(@"Missing sso token: %@", jsonResponse);
            return;
        }
        
        NSString *ssoToken = jsonResponse[@"ssoToken"];
        
        [[IngoSdkManager getInstance] beginWithHostViewController:self customerId:customerID ssoToken:ssoToken andOnTransactionApprovedHandler:nil];
    }
    
    
- (NSString*) encryptString:(NSString*)string
    {
        NSData *plainData = [string dataUsingEncoding:NSUTF8StringEncoding];
        
        NSData *stringSha = [NSData sha512:plainData];
        NSString *base64String = [stringSha base64EncodedStringWithOptions:0];
        
        return base64String;
    }


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
   // let a = CGSize(width: 500, height: 500)
    
  
    
NSData *imageData = UIImageJPEGRepresentation(chosenImage,0.3);
  //  NSData *imageData = UIImagePNGRepresentation(chosenImage);
    
    
    
    NSString *base64String = [imageData base64EncodedStringWithOptions:0];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    NSMutableDictionary *m = [userData mutableCopy];
    
    NSString *idEst = [NSString stringWithFormat:@"%@",[userData[@"ideUsuario"] stringValue]];
    NSDictionary *data = [SendTokenFirestore updateDataUserWithBase64:base64String dataUser:m idUser: idEst];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:(NSString*)kUserDetailsKey];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
 
    [_menuTableView reloadData];
    
    
    
    
  /*  [profileImageView setImage:[self decodeBase64ToImage:userData[kImgUser]]];
    profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2;
    profileImageView.clipsToBounds = true;*/
    
    
}
    
    
    
- (void) setupMenu {
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height/2+self.view.bounds.size.height/4.5, self.view.bounds.size.width, 120)];
    
    [footerView setBackgroundColor:[UIColor whiteColor]];
    UIButton * homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * favoritesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * myMCButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIStackView *stackView = [[UIStackView alloc] init];
    
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    switch ([userData[kUserIdCountry] intValue]) {
        case 1:  case 2: case 3: case 4: //MEXICO  //USA
        [homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
        [homeButton setContentMode:UIViewContentModeScaleAspectFill];
        [homeButton.heightAnchor constraintEqualToConstant:75].active = true;
        [homeButton.widthAnchor constraintEqualToConstant:75].active = true;
        
        
        [walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        [walletButton setContentMode:UIViewContentModeScaleAspectFill];
        [walletButton addTarget:self action:@selector(openWallet_Action:) forControlEvents:UIControlEventTouchUpInside];
        [walletButton.heightAnchor constraintEqualToConstant:75].active = true;
        [walletButton.widthAnchor constraintEqualToConstant:75].active = true;
        
        [favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        [favoritesButton setContentMode:UIViewContentModeScaleAspectFill];
        [favoritesButton addTarget:self action:@selector(startFavorites) forControlEvents:UIControlEventTouchUpInside];
        [favoritesButton.heightAnchor constraintEqualToConstant:75].active = true;
        [favoritesButton.widthAnchor constraintEqualToConstant:75].active = true;
        
        [myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_history", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        [myMCButton setContentMode:UIViewContentModeScaleAspectFill];
        [myMCButton addTarget:self action:@selector(openMyMC_Action:) forControlEvents:UIControlEventTouchUpInside];
        [myMCButton.heightAnchor constraintEqualToConstant:75].active = true;
        [myMCButton.widthAnchor constraintEqualToConstant:75].active = true;
        
        
        //Stack View
        //stackView.axis = UILayoutConstraintAxisVertical;
        stackView.axis = UILayoutConstraintAxisHorizontal;
        // stackView.distribution = UIStackViewDistributionEqualSpacing;
        stackView.distribution = UIStackViewDistributionFillEqually;
        // stackView.alignment = UIStackViewAlignmentCenter;
        stackView.alignment = UIStackViewAlignmentFill;
        stackView.spacing = 8;
        
        [stackView addArrangedSubview:homeButton];
        [stackView addArrangedSubview:walletButton];
        [stackView addArrangedSubview:favoritesButton];
        [stackView addArrangedSubview:myMCButton];
        
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        [footerView addSubview:stackView];
        
        [stackView.centerXAnchor constraintEqualToAnchor:footerView.centerXAnchor].active = true;
        [stackView.centerYAnchor constraintEqualToAnchor:footerView.centerYAnchor].active = true;
        [self.view addSubview:footerView];
        
        break;
        
        //
        //            [homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
        //            [homeButton setContentMode:UIViewContentModeScaleAspectFill];
        //            [homeButton.heightAnchor constraintEqualToConstant:75].active = true;
        //            [homeButton.widthAnchor constraintEqualToConstant:75].active = true;
        //
        //
        //            [walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        //            [walletButton setContentMode:UIViewContentModeScaleAspectFill];
        //            [walletButton addTarget:self action:@selector(showWallet) forControlEvents:UIControlEventTouchUpInside];
        //            [walletButton.heightAnchor constraintEqualToConstant:75].active = true;
        //            [walletButton.widthAnchor constraintEqualToConstant:75].active = true;
        //
        //            [favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
        //            [favoritesButton setContentMode:UIViewContentModeScaleAspectFill];
        //            [favoritesButton addTarget:self action:@selector(startFavorites) forControlEvents:UIControlEventTouchUpInside];
        //            [favoritesButton.heightAnchor constraintEqualToConstant:75].active = true;
        //            [favoritesButton.widthAnchor constraintEqualToConstant:75].active = true;
        //
        //
        //            //Stack View
        //            //stackView.axis = UILayoutConstraintAxisVertical;
        //            stackView.axis = UILayoutConstraintAxisHorizontal;
        //            // stackView.distribution = UIStackViewDistributionEqualSpacing;
        //            stackView.distribution = UIStackViewDistributionFillEqually;
        //            // stackView.alignment = UIStackViewAlignmentCenter;
        //            stackView.alignment = UIStackViewAlignmentFill;
        //            stackView.spacing = 8;
        //
        //            [stackView addArrangedSubview:homeButton];
        //            [stackView addArrangedSubview:walletButton];
        //            [stackView addArrangedSubview:favoritesButton];
        //
        //            stackView.translatesAutoresizingMaskIntoConstraints = false;
        //            [footerView addSubview:stackView];
        //
        //            [stackView.centerXAnchor constraintEqualToAnchor:footerView.centerXAnchor].active = true;
        //            [stackView.centerYAnchor constraintEqualToAnchor:footerView.centerYAnchor].active = true;
        //            [self.view addSubview:footerView];
        //            break;
    }
    
}
    -(UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
        return [UIImage imageWithData:data];
    }


- (void)managerFailed:(pootEngine *)manager withKey:(NSString *)key errorCode:(pootEngineErrorCode)error{

    [self unLockView];
    
  if   (manager == USALocManager) {
      [[NSUserDefaults standardUserDefaults] setBool:NO forKey:(NSString*)kUSALocation];
            [[NSUserDefaults standardUserDefaults] synchronize];
        [self cashCheque];
    }
    
}

    @end
