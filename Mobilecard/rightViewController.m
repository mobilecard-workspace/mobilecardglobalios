//
//  rightViewController.m
//  Mobilecard
//
//  Created by David Poot on 11/5/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "rightViewController.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "MainMenu_Ctrl.h"
#import "NSDictionary (keychain).h"
#import <Mobilecard-Swift.h>

#define cellHeight @"40"

@interface rightViewController ()
{
    NSMutableArray *menuItems;
       ServicesObjc *services;
}
@end

@implementation rightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
 

    services = [[ServicesObjc alloc] init];
    services.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeView:)
                                                 name:@"closeViewBar" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateName:)
                                                 name:@"updateNameComplete" object:nil];
    
    menuItems = [[NSMutableArray alloc] init];
    
    [_profileName setText:[NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName], [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLastName]]];
    
    [_versionLabel setText:[NSString stringWithFormat:NSLocalizedString(@"VERSIÓN: %@", nil), [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]]];
    
    [self updateMenu];
}

- (void)viewWillAppear:(BOOL)animated
{
    
    
    
    [self.sideMenuController setDelegate:self];
    
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated{
     [_profileName setText:[NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName], [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLastName]]];
    
}

- (void)updateName:(NSNotification *)note {
    
      [_profileName setText:[NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName], [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLastName]]];
    
}

- (void)closeView:(NSNotification *)note {
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    NSLog(@"se cierrra la barra");
  //  UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
  //  MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
    
   // [nav popToRootViewControllerAnimated:YES];
   // [nextView startSoatCO];
    
    [mainViewController hideRightViewAnimated:YES completionHandler:nil];
    
}

- (void)updateMenu
{
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    [menuItems removeAllObjects];
    switch ([userData[kUserIdCountry] intValue]) {
            case 1: //MEXICO
        {
            
            
 
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]){	
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_home", kIdentifierKey, cellHeight, kHeightKey,  nil]];
                  [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPay", kIdentifierKey, cellHeight, kHeightKey, nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_MXtransfers", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, cellHeight, kHeightKey,  nil]];
       
          
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_wallet", kIdentifierKey, cellHeight, kHeightKey,  nil]];
           // [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_favorites", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_Statement", kIdentifierKey, cellHeight, kHeightKey,  nil]];
                
                 [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_MobileTag", kIdentifierKey, cellHeight, kHeightKey, nil]];
                
                [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_MiMobilecard", kIdentifierKey, cellHeight, kHeightKey, nil]];
            }
            
        }
            break;
        case 2: //COLOMBIA
        {
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_home", kIdentifierKey, cellHeight, kHeightKey,  nil]];
           
            //    [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPay", kIdentifierKey, cellHeight, kHeightKey, nil]];
          //  [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_EUAtransfers", kIdentifierKey, cellHeight, kHeightKey,  nil]];
          //  [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"RECARGA TELEPEAJE", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"SOAT", kIdentifierKey, cellHeight, kHeightKey,  nil]];
             [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_serviciosCO", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            
             [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_prepagadasCO", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"RECARGA TIEMPO AIRE", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            
            
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_MiMobilecard", kIdentifierKey, cellHeight, kHeightKey, nil]];
            
            
            /*[menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, cellHeight, kHeightKey,  nil]];
             */
        }
            break;
        case 3: //USA
        {
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_home", kIdentifierKey, cellHeight, kHeightKey,  nil]];
           
            //se oculto escanea y paga en usa
           //  [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPay", kIdentifierKey, cellHeight, kHeightKey, nil]];
            
           // if ([[NSUserDefaults standardUserDefaults] boolForKey:(NSString*)kUSALocation]) {
                [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_cash", kIdentifierKey, cellHeight, kHeightKey,  nil]];
          //  }
            
             [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_EUAtransfers", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            //[menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_transfers", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_wallet", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_Statement", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_MiMobilecard", kIdentifierKey, cellHeight, kHeightKey, nil]];
            break;
        }
            case 4: //PERU
        {
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_home", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPE", kIdentifierKey, cellHeight, kHeightKey, nil]];
             [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_EUAtransfers", kIdentifierKey, cellHeight, kHeightKey,  nil]];
             [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, cellHeight, kHeightKey,  nil]];
                 [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_wallet", kIdentifierKey, cellHeight, kHeightKey,  nil]];
          //  [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_favorites", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_Statement", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_MiMobilecard", kIdentifierKey, cellHeight, kHeightKey, nil]];
        }
            break;
        default:
            break;
    }
    
    [_menuTableView reloadData];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Menu",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}




- (void)willShowRightView:(UIView *)rightView sideMenuController:(LGSideMenuController *)sideMenuController
{
    [self updateMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (IBAction)profile_Action:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
     [mainViewController hideRightViewAnimated:YES completionHandler:nil];
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"profile" sender:self];
    
    
}

- (IBAction)contact_Action:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
     [mainViewController hideRightViewAnimated:YES completionHandler:nil];
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"side_contact" sender:self];
    
   
}

- (IBAction)logout_Action:(id)sender {
    
    //SERVICIO PARA BORRAR EL TOKEN DE PUSH notifications
    NSDictionary *header = @{@"Content-Type":@"application/json",@"authorization":@"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"};
    
    if ([DataUser.USERTYPE  isEqual: @"NEGOCIO"]){
        
        //si es negocio
        
        NSDictionary *params = @{@"idUsuario":DataUser.USERIDD,@"token":DataUser.TOKENFIREBASE,@"tipoUsuario":@"ESTABLECIMIENTO"};
        
        NSLog(@"PARAMS: %@",params);
        [services sendWithType:@"DELETE" url:[NSString stringWithFormat:@"%@PushNotifications/removeToken", ServicesLinks.IPServer] params:params header:header message:@"" animate:NO];
        
    }else{
        //si es usuario
        NSDictionary *params = @{@"idUsuario":DataUser.USERIDD,@"token":DataUser.TOKENFIREBASE,@"tipoUsuario":@"USUARIO"};
              NSLog(@"PARAMS: %@",params);
             [services sendWithType:@"DELETE" url:[NSString stringWithFormat:@"%@PushNotifications/removeToken", ServicesLinks.IPServer] params:params header:header message:@"" animate:NO];
        
    }
    

    

       
     
    
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:FALSE forKey:@"enable_tutorial"]; 
    [[NSUserDefaults standardUserDefaults] synchronize];
    [DataUser clean];
    
    NSDictionary *logout = [[NSDictionary alloc] init];
    [logout deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
   
    [nav dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)commerce_ChangePass:(id)sender {
    NSLog(@"SI SE EJECUTO EL CAMBIO DE PASS");
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
     [mainViewController hideRightViewAnimated:YES completionHandler:nil];
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"side_passchange" sender:self];
    
   
}

- (IBAction)commerce_Statement:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
     [mainViewController hideRightViewAnimated:YES completionHandler:nil];
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"side_statement_commerce" sender:self];
    
   
}

//metodo que se ejecuta comercio INICIO
- (IBAction)homeCommerceAction:(UIButton *)sender {
   MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController hideRightViewAnimated:YES completionHandler:nil];
}

//metodo que se ejecuta comercio TARJETA PRESENTE
- (IBAction)cardPressentAction:(UIButton *)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
     [mainViewController hideRightViewAnimated:YES completionHandler:nil];
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"side_card" sender:self];
    
   
    
    
}
//metodo que se ejecuta comercio generar codigo qr
- (IBAction)generateCodeCommerceAction:(UIButton *)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
     [mainViewController hideRightViewAnimated:YES completionHandler:nil];
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"side_qrcode" sender:self];
    
   
}


#pragma mark UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *label = cell.contentView.subviews[0];
    [label setText:NSLocalizedString(menuItems[indexPath.row][kIdentifierKey], nil)];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:menuItems[0][kIdentifierKey]]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        
        [nav popToRootViewControllerAnimated:YES];
        
       
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_cash"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startCash];
        
       
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_wallet"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView openWallet_Action:self];
        
       
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_transfers"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startTransfersModule];
        
      
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_MXtransfers"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startMXTransfersModule];
        
       
        return;
    }
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_EUAtransfers"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView  startTransfersModule];
        
       
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_Statement"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startStatement];
        
       
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_MobileTag"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startMiMobileTag];
        
        
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_MiMobilecard"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startMiMobilecard];
        
        
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_ScanNPay"] || [menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_ScanNPE"]) {
        MainViewController *mainViewController = (MainViewController* )self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController* )mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startScanNPay];
        
        
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_billpay"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startBillPay];
        
     
        return;
    }
    
    //colombia
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"RECARGA TELEPEAJE"]) {
        NSLog(@"Vamos a recarga telepeaje");
        
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startRecargaTeleCO];
        
       
        
        
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"SOAT"]) {
        NSLog(@"Vamos a SOAT");
        
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startSoatCO];
        
       
        
        
        return;
    }
    
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_serviciosCO"]) {
           NSLog(@"Vamos a Servicios Colombia");
           
           MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
            [mainViewController hideRightViewAnimated:YES completionHandler:nil];
           
           UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
           MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
           
           [nav popToRootViewControllerAnimated:YES];
           [nextView startServicesCO];
           
          
           
           
           return;
       }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_prepagadasCO"]) {
           NSLog(@"Vamos a tarjetas prepagadas Colombia");
           
           MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
            [mainViewController hideRightViewAnimated:YES completionHandler:nil];
           
           UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
           MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
           
           [nav popToRootViewControllerAnimated:YES];
           [nextView startPrepageCO];
           
          
           
           
           return;
       }
    
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"RECARGA TIEMPO AIRE"]) {
        NSLog(@"Vamos a recarga de tiempo aire");
        
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startRecargaTiempoCO];
        
      
        
        
        
        
        return;
    }
    
    //termina colombia
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_topup"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startTopUp];
        
      
        return;
    }
    ////////////////////////////  Codigo reemplazado para el nuevo look and feel   Raul Mendez /////////////////////////////////////
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_favorites"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
         [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startFavorites];
        
    
        return;
    }
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
     [mainViewController hideRightViewAnimated:YES completionHandler:nil];
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    if ([nextView shouldPerformSegueWithIdentifier:menuItems[indexPath.row][kIdentifierKey] sender:self]) {
        [nextView performSegueWithIdentifier:menuItems[indexPath.row][kIdentifierKey] sender:self];
    }
    
    
}


//DELEGADOS SERVICIOS

//delegados de los servicios
- (void)responseServiceWithType:(NSString *)type endpoint:(NSString *)endpoint response:(NSDictionary *)response{
    
    NSLog(@"RESPONSE: %@",response);
      
     
}

- (void)responseArrayServiceWithType:(NSString *)type endpoint:(NSString *)endpoint response:(NSArray *)response{
    
    
    NSLog(@"RESPONSE: %@",response);
    
   
    
    
}



- (void)errorServiceWithEndpoint:(NSString *)endpoint{
    
   /*UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"No fue posible establecer conexión con el servidor.", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
      
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
        */

}

@end
