//
//  MXTransfers_Step1.h
//  Mobilecard
//
//  Created by David Poot on 11/16/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Wallet_Ctrl.h"
#import "MXTransfers_New.h"
#import "MXTransfers_edit.h"
//#import <TrustDefender/TrustDefender.h>
#import "Secure3D_Ctrl.h"
#import <CoreLocation/CoreLocation.h>

@interface MXTransfers_Step1 : mT_commonTableViewController <cardSelectionDelegate, updateCardDelegate, MXTransfersNew_Protocol, MXTransfers_edit_Protocol, Secure3DDelegate, updateCardDelegate, cardSelectionDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UITextField_Validations *recipientText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *amountText;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@property (weak, nonatomic) IBOutlet UILabel *feeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@end
