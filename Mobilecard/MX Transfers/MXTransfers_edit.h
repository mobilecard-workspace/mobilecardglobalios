//
//  MXTransfers_edit.h
//  Mobilecard
//
//  Created by David Poot on 1/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol MXTransfers_edit_Protocol

@required
- (void)MXTransfers_edit_Result;

@end

@interface MXTransfers_edit : mT_commonTableViewController

@property (nonatomic, assign) id <NSObject, MXTransfers_edit_Protocol> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *aliasText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;

@property (strong, nonatomic) NSDictionary *recipientInfo;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
