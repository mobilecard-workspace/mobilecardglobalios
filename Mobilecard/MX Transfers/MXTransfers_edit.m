//
//  MXTransfers_edit.m
//  Mobilecard
//
//  Created by David Poot on 1/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "MXTransfers_edit.h"

@interface MXTransfers_edit ()
{
    pootEngine *updateManager;
}
@end

@implementation MXTransfers_edit

- (void)viewDidLoad {
    [super viewDidLoad];
    
 //   [self addShadowToView:self.navigationController.navigationBar];
 //   [self addShadowToView:_continue_Button];
    
    [_aliasText setUpTextFieldAs:textFieldTypeUserName];
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_phoneText setUpTextFieldAs:textFieldTypeCellphone];
    
    [self addValidationTextField:_aliasText];
    [self addValidationTextField:_emailText];
    [self addValidationTextField:_phoneText];
    
    [self addValidationTextFieldsToDelegate];
    
    [_aliasText setText:_recipientInfo[@"alias"]];
    [_emailText setText:_recipientInfo[@"correo"]];
    [_phoneText setText:_recipientInfo[@"telefono"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)continue_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        updateManager = [[pootEngine alloc] init];
        [updateManager setDelegate:self];
        [updateManager setShowComments:developing];
        
        NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:_aliasText.text forKey:@"alias"];
        [params setObject:_emailText.text forKey:@"email"];
        [params setObject:_phoneText.text forKey:@"telefono"];
        [params setObject:_recipientInfo[@"id"] forKey:@"idAccount"];
        [params setObject:userDetails[kUserIDKey] forKey:@"idUsuario"];
        [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
        
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        [updateManager startJSONRequestWithURL:H2HUpdateAccount withPost:JSONString];
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    if (manager == updateManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue]==0) {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                if ([_delegate conformsToProtocol:@protocol(MXTransfers_edit_Protocol)]&&[_delegate respondsToSelector:@selector(MXTransfers_edit_Result)]) {
                    [_delegate MXTransfers_edit_Result];
                }
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
                [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
}

@end
