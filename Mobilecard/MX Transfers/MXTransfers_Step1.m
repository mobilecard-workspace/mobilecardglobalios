//
//  MXTransfers_Step1.m
//  Mobilecard
//
//  Created by David Poot on 11/16/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "MXTransfers_Step1.h"
#import "Secure3D_Ctrl.h"
#import "paymentGeneralResult.h"

@interface MXTransfers_Step1 ()
{
    NSNumberFormatter *numFormatter;
    
    float finalAmount;
    float finalComission;
    
    pootEngine *recipientManager;
    pootEngine *cardManager;
    pootEngine *paymentManager;
    pootEngine *deleteManager;
    
   // THMTrustDefender *profile;
    
    NSMutableDictionary *threatMetrixInfo;
    NSDictionary *selectedCardInfo;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
}
@end

@implementation MXTransfers_Step1

- (void)viewDidLoad {
    [super viewDidLoad];
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
//    [self addShadowToView:_addButton];
//    [self addShadowToView:_continue_Button];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [self initializePickersForView:self.navigationController.view];
    
    [_recipientText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_amountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_amountText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]];
    [_amountText setKeyboardType:UIKeyboardTypeDecimalPad];
    
    [self addValidationTextField:_recipientText];
    [self addValidationTextField:_amountText];
    
    [self addValidationTextFieldsToDelegate];
    
    finalComission = 0.0;
    finalAmount = 0.0;
    
    [_feeLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalComission]]]];
    [_totalLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount]]]];
    
    [_amountText setEnabled:NO];
    [_amountText setText:@""];
    
    [self callRecipients];
}

- (void)callRecipients
{
    recipientManager = [[pootEngine alloc] init];
    [recipientManager setDelegate:self];
    [recipientManager setShowComments:developing];
    
    
    [recipientManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", H2HGetAccounts, [[[NSUserDefaults standardUserDefaults] objectForKey:kUserDetailsKey] objectForKey:kUserIDKey], NSLocalizedString(@"lang", nil)]];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self subPickerView:pickerView didSelectRow:row inComponent:component];
    [self updateTotalNComissionWithTotal:[[_recipientText infoArray] count]>0?[[_amountText text] floatValue]:0.00];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _amountText) {
        [self updateTotalNComissionWithTotal:[[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue]];
    }
    return YES;
}

- (void) updateTotalNComissionWithTotal:(float)total
{
    float fixed = [[_recipientText infoArray] count]>0?[[_recipientText infoArray][[_recipientText selectedID]][@"comision_fija"] floatValue]:0.00;
    float amountWithFixed = [[_recipientText infoArray] count]>0?total+[[_recipientText infoArray][[_recipientText selectedID]][@"comision_fija"] floatValue]:0.00;
    float percentage =[[_recipientText infoArray] count]>0?[[_recipientText infoArray][[_recipientText selectedID]][@"comision_porcentaje"] floatValue]:0.00;
    
    
    finalComission = fixed+(amountWithFixed*percentage);
    finalAmount = total+finalComission;
    
    [_feeLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalComission]]]];
    [_totalLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount]]]];
}


- (IBAction)continue_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        @try {
            cardManager = [[pootEngine alloc] init];
            [cardManager setDelegate:self];
            [cardManager setShowComments:developing];
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        } @catch (NSException *exception) {
            //NSLog(@"%@", exception.description);
            
          //  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
          //  [alertMsg show];
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (IBAction)delete_Action:(id)sender {
    if ([[_recipientText infoArray] count] == 0) {
        return;
    }
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:NSLocalizedString(@"¿Deseas eliminar el recipiente: %@?", nil), [_recipientText text]] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Si", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        deleteManager = [[pootEngine alloc] init];
        [deleteManager setDelegate:self];
        [deleteManager setShowComments:developing];
        
        [deleteManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@&idAccount=%@", H2HDeleteAccount, userDetails[kUserIDKey], NSLocalizedString(@"lang", nil), [_recipientText infoArray][[_recipientText selectedID]][[_recipientText idLabel]]]];
        
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    [alertMsg addAction:cancel];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}


- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == recipientManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            [_recipientText setInfoArray:response[@"accounts"]];
            [_recipientText setDescriptionLabel:@"alias"];
            [_recipientText setIdLabel:@"id"];
            
            if ([[_recipientText infoArray] count]>0) {
                [_recipientText setSelectedID:0];
                [_recipientText setText:[_recipientText infoArray][[_recipientText selectedID]][[_recipientText descriptionLabel]]];
                [_amountText setEnabled:YES];
                [_amountText setText:@""];
            } else {
                [_amountText setEnabled:NO];
                [_amountText setText:@""];
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (cardManager == manager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0 && [response[kUserCardArray] count]>0) {
            [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
            [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
            UINavigationController *nav = [nextStory instantiateInitialViewController];
            Wallet_Ctrl *view = nav.viewControllers.firstObject;
            [view setType:walletViewTypeSelection];
            [view setDelegate:self];
            
            [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (deleteManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue]==0) {
            [self callRecipients];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
}

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    if (selectedCard) {
        selectedCardInfo = selectedCard;
         NSLog(@"SI SE EJECUTA ESTOAA");
     
   
         [self performPayment:selectedCardInfo];
        //[self performProfileCheck];
    }
}

- (void) performPayment:(NSDictionary*)cardInfo
{
    paymentManager = [[pootEngine alloc] init];
    [paymentManager setDelegate:self];
    [paymentManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    [params setObject:userInfo[kUserIDKey] forKey:@"idUser"];
    [params setObject:cardInfo[@"idTarjeta"] forKey:@"idCard"];
    [params setObject:[_recipientText infoArray][[_recipientText selectedID]][[_recipientText idLabel]] forKey:@"accountId"];
    [params setObject:[_amountText text] forKey:@"amount"];
    [params setObject: [NSString stringWithFormat:@"%.02f",finalComission] forKey:@"comision"];
 //   [params setObject:[_recipientText infoArray][[_recipientText selectedID]][[_recipientText descriptionLabel]] forKey:@"concept"];
    
     [params setObject:@"Transferencia eletronica" forKey:@"concept"];
    
    [params setObject:[NSNumber numberWithInt:48] forKey:@"idProvider"];
    [params setObject:[NSNumber numberWithInt:48] forKey:@"idProduct"];
    [params setObject:[_recipientText infoArray][[_recipientText selectedID]][[_recipientText idLabel]] forKey:@"referencia"];
    [params setObject:@"" forKey:@"emisor"];
    
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"lat"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"lon"];
    
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"modelo"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
    
    
    Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
    [nextView setPurchaseInfo:params];
    [nextView setSecure3DURL:H2HEnqueuePayment];
    [nextView setType:serviceTypeMXTransfers];
     [nextView setSelectedCardInfo:[NSMutableDictionary dictionaryWithDictionary:cardInfo]];
    [nextView setThreatMetrixInfo:threatMetrixInfo];
    [nextView setDelegate:self];
    
    [self.navigationController pushViewController:nextView animated:YES];
}

- (void)MXTransfersNew_Result
{
    [self callRecipients];
}

- (void)MXTransfers_edit_Result
{
    [self callRecipients];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"add"]) {
        UINavigationController *nav = [segue destinationViewController];
        MXTransfers_New *nextView = [nav childViewControllers].firstObject;
        
        [nextView setDelegate:self];
    }
    
    if ([[segue identifier] isEqualToString:@"edit"]) {
        UINavigationController *nav = [segue destinationViewController];
        MXTransfers_edit *nextView = [nav childViewControllers].firstObject;
        
        [nextView setRecipientInfo:[_recipientText infoArray][[_recipientText selectedID]]];
        
        [nextView setDelegate:self];
    }
    
    if ([[segue identifier] isEqualToString:@"result"]) {
        paymentGeneralResult *nextView = [segue destinationViewController];
        NSDictionary *resultData = (NSDictionary*)sender;
        [nextView setResultData:resultData];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"edit"]) {
        if ([[_recipientText infoArray] count]>0) {
            return YES;
        } else {
            return NO;
        }
    }
    
    return YES;
}





#pragma Mark TrustDefender Handling
/*
- (void) performProfileCheck
{
    [self lockViewWithMessage:nil];
    
    profile = [THMTrustDefender sharedInstance];
    
    [profile configure:@{
                         THMOrgID:@"c2ggoqh1",
                         THMFingerprintServer:@"h-sdk.online-metrix.net",
                         }];
    
    [profile doProfileRequestWithCallback:^(NSDictionary *result)
     {
         [self unLockView];
         developing?NSLog(@"PROFILE RESULT -> %@", result):nil;
         self->threatMetrixInfo = [NSMutableDictionary dictionaryWithDictionary:result];
         
         THMStatusCode statusCode = [[result valueForKey:THMProfileStatus] integerValue];
         
         if (statusCode == THMStatusCodeOk) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"SI SE EJECUTA ESTOAA");
                 [self performPayment:selectedCardInfo];
             });
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:@"%s", statusCode == THMStatusCodeNetworkTimeoutError ? "Timed out"                                                                                                                                                                : statusCode == THMStatusCodeConnectionError     ? "Connection Error"                                                                                                                                                           : statusCode == THMStatusCodeHostNotFoundError   ? "Host not found error"                                                                                                                                                : statusCode == THMStatusCodeInternalError       ? "Internal Error"                                                                                                                                                        : statusCode == THMStatusCodeInterruptedError    ? "Interrupted"                                                                                                                                                           : "other"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [alertMsg dismissViewControllerAnimated:YES completion:nil];
                 }];
                 
                 [alertMsg addAction:ok];
                 
                 [self presentViewController:alertMsg animated:YES completion:nil];
             });
         }
         
         
     }];
}*/

- (void)PaymentWithThreatMetrixResponse:(NSDictionary *)response
{
    if (response) {
        [self performSegueWithIdentifier:@"result" sender:response];
        
        if ([response[@"code"] intValue] == 0) {
            
        };
    }
}








#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}

@end
