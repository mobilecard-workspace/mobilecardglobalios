//
//  MXTransfers_New.m
//  Mobilecard
//
//  Created by David Poot on 11/16/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "MXTransfers_New.h"

@interface MXTransfers_New ()
{
    pootEngine *addManager;
    pootEngine *bankManager;
}

@end

@implementation MXTransfers_New

- (void)viewDidLoad {
    [super viewDidLoad];
    
 //   [self addShadowToView:_continue_Button];
 //   [self addShadowToView:self.navigationController.navigationBar];
    
    [self initializePickersForView:self.navigationController.view];

    [_aliasText setUpTextFieldAs:textFieldTypeName];
    [_holderNameText setUpTextFieldAs:textFieldTypeName];
    [_holderLastName setUpTextFieldAs:textFieldTypeLastName];
    [_holderLastName setMinLength:2];
    [_holderSecondLastName setUpTextFieldAs:textFieldTypeLastName];
    [_phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    
    [_bankText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_accountTypeText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_accountTypeText setInfoArray:[NSArray arrayWithObjects:
                                    //[NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Account number", nil), kDescriptionKey, @"ACT", kIDKey, nil],
                                    [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"CLABE number", nil), kDescriptionKey, @"CLABE", kIDKey, nil], nil]];
    
    [_accountTypeText setDescriptionLabel:(NSString*)kDescriptionKey];
    [_accountTypeText setIdLabel:(NSString*)kIDKey];
    [_accountTypeText setSelectedID:0];
    [_accountTypeText setText:[_accountTypeText infoArray][[_accountTypeText selectedID]][[_accountTypeText idLabel]]];
    [_accountTypeText setEnabled:YES];
    
    [_clabeText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_clabeText setMaxLength:18];
    [_clabeText setMinLength:18];
    [_clabeText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]];

    [_clabeText setPlaceholder:[_accountTypeText infoArray][[_accountTypeText selectedID]][[_accountTypeText descriptionLabel]]];

    
   
    
    [self addValidationTextField:_aliasText];
    [self addValidationTextField:_holderNameText];
    
    [self addValidationTextField:_holderLastName];
    [self addValidationTextField:_holderSecondLastName];
    [self addValidationTextField:_phoneText];
    [self addValidationTextField:_emailText];
    
    [self addValidationTextField:_bankText];
    [self addValidationTextField:_accountTypeText];
    [self addValidationTextField:_clabeText];
    
    [self addValidationTextFieldsToDelegate];
    
    bankManager = [[pootEngine alloc] init];
    [bankManager setDelegate:self];
    [bankManager setShowComments:developing];
    
    [bankManager startWithoutPostRequestWithURL:H2HGetBanks];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    /*
    if (_bankText == pickerView.textField) {
        if ([[_bankText infoArray][row][[_bankText idLabel]] isEqualToString:@"072"]) {
            [_accountTypeText setSelectedID:0];
            [_clabeText setMaxLength:11];
            [_clabeText setMinLength:10];
        } else {
            [_accountTypeText setSelectedID:1];
            [_clabeText setMaxLength:18];
            [_clabeText setMinLength:18];
        }
        [_accountTypeText setText:[_accountTypeText infoArray][[_accountTypeText selectedID]][[_accountTypeText descriptionLabel]]];
        [_clabeText setPlaceholder:[_accountTypeText infoArray][[_accountTypeText selectedID]][[_accountTypeText descriptionLabel]]];
    }
     */
    [self subPickerView:picker didSelectRow:row inComponent:component];
}

- (IBAction)continue_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        NSMutableString *rfc = [[NSMutableString alloc] init];
        
        [rfc appendString:[_holderLastName.text substringWithRange:NSMakeRange(0, 2)]];
        [rfc appendString:[_holderSecondLastName.text substringWithRange:NSMakeRange(0, 1)]];
        [rfc appendString:[_holderNameText.text substringWithRange:NSMakeRange(0, 1)]];
        [rfc appendString:@"000000"];
        [rfc appendString:[NSString stringWithFormat:@"%d%d%d", rand()%9, rand()%9, rand()%9]];
        [rfc setString:[rfc uppercaseString]];
        
        NSData *asciiEncoded = [rfc dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *cleanRFC = [[NSString alloc] initWithData:asciiEncoded encoding:NSASCIIStringEncoding];
        
        addManager = [[pootEngine alloc] init];
        [addManager setDelegate:self];
        [addManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
        [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
        [params setObject:_aliasText.text forKeyedSubscript:@"alias"];
        [params setObject:[NSString stringWithFormat:@"%@ %@ %@", _holderNameText.text, _holderLastName.text, _holderSecondLastName.text] forKeyedSubscript:@"holderName"];
        [params setObject:cleanRFC forKey:@"rfc"];
        [params setObject:_phoneText.text forKey:@"phone"];
        [params setObject:[NSString stringWithFormat:@"%@ %@", _holderNameText.text, _holderLastName.text] forKey:@"contact"];
        [params setObject:_emailText.text forKey:@"email"];
        [params setObject:[_accountTypeText infoArray][[_accountTypeText selectedID]][[_accountTypeText idLabel]] forKey:@"actType"];
        
        [params setObject:[_bankText infoArray][[_bankText selectedID]][[_bankText idLabel]] forKey:@"bankCode"];
        [params setObject:_clabeText.text forKey:@"actNumber"];
        
        
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        [addManager startJSONRequestWithURL:H2HAddAccount withPost:JSONString];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == bankManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            [_bankText setInfoArray:response[@"banks"]];
            [_bankText setDescriptionLabel:@"nombre_corto"];
            [_bankText setIdLabel:@"clave"];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == addManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            [[self navigationController] dismissViewControllerAnimated:YES completion:^{
                if ([_delegate conformsToProtocol:@protocol(MXTransfersNew_Protocol)] && [_delegate respondsToSelector:@selector(MXTransfersNew_Result)]) {
                    [_delegate MXTransfersNew_Result];
                }
            }];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}
@end
