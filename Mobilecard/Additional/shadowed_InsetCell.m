//
//  shadowed_InsetCell.m
//  Mobilecard
//
//  Created by David Poot on 10/25/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "shadowed_InsetCell.h"

#define inset 20

@implementation shadowed_InsetCell

- (void)setFrame:(CGRect)frame {
    frame.origin.x += inset;
    frame.size.width -= 2 * inset;
    [super setFrame:frame];
    
//    
//    self.layer.masksToBounds = false;
//    self.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.layer.shadowOffset = CGSizeMake(0, 5.0);
//    self.layer.shadowOpacity = 0.34;
//    self.layer.shadowRadius = 5.0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
