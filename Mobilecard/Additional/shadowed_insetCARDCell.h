//
//  shadowed_insetCARDCell.h
//  Mobilecard
//
//  Created by David Poot on 10/31/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNMRemoteImageView.h"

@interface shadowed_insetCARDCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *highlightView;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteIndicator;
@property (weak, nonatomic) IBOutlet MNMRemoteImageView *backgroundImagecard;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UILabel *panText;
@property (weak, nonatomic) IBOutlet UILabel *balanceText;

@property (weak, nonatomic) IBOutlet UIButton *favoritesIconButton;
@property (weak, nonatomic) IBOutlet UIButton *favoritesButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteIconButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@end
