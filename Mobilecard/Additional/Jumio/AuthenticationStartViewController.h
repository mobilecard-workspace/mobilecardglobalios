//
//  AuthenticationStartViewController.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//
#import "mT_commonController.h"
#import "StartViewController.h"

@interface AuthenticationStartViewController: mT_commonController <pootEngineDelegate>{
    
}

@property (weak, nonatomic) IBOutlet UITextField *transactionReferenceTextField;

- (IBAction)startAuthentication;
- (IBAction)transactionRefernece_onDone;
@property (strong,nonatomic) NSString *_whiteList;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end
