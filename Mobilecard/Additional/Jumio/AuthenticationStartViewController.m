//
//  AuthenticationStartViewController.m
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import "AuthenticationStartViewController.h"
@import JumioCore;
@import NetverifyFace;
#import <Mobilecard-Swift.h>

@interface AuthenticationStartViewController () <AuthenticationControllerDelegate>{
     pootEngine *whiteListData;
}
@property (nonatomic, strong) AuthenticationController *authenticationController;
@property (nonatomic, strong) UIViewController *authenticationScanViewController;

@end

@implementation AuthenticationStartViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:(NSString*)@"AutenticationJumio"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //Destroy the instance to properly clean up the SDK
               // [self.authenticationController destroy];
                //self.authenticationController = nil;
                [self.navigationController popViewControllerAnimated:FALSE];
    
  // [_activityIndicator startAnimating];
   // [_activityIndicator setHidden:NO];
    
    //whiteListData = [[pootEngine alloc] init];
    //[whiteListData setShowComments:developing];
    //[whiteListData setDelegate:self];
    

   // [whiteListData startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@", whiteListURL,DataUser.COUNTRYID,@"/es/whiteList?idUsuario=",DataUser.USERIDD]];
    
   //[self lockViewWithMessage:NSLocalizedString(@"Cargando", nil)];

    
    
  /*  //codigo inicializador
    if  (![__whiteList  isEqual: @""] && __whiteList != nil){
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:(NSString*)@"AutenticationJumio"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController popViewControllerAnimated:YES];
        
     
    }else{
        [self createAuthenticationController];
      
    }
    //termina codigo inicializador*/
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSString *transactionReference = [[NSUserDefaults standardUserDefaults] stringForKey:@"enrollmentTransactionReference"];
    if (transactionReference) {
      //  [self.transactionReferenceTextField setText:transactionReference];
    }
}

- (void) createAuthenticationController: (NSString *)a completion:(void (^)(BOOL success))completionBlock {
    //Prevent SDK to be initialized on Jailbroken devices
    if ([JumioDeviceInfo isJailbrokenDevice]) {
        return;
    }
   
    
    // Setup the Configuration for Authentication
    AuthenticationConfiguration *config = [self createAuthenticationConfiguration];
    
    @try {
        _authenticationController = [[AuthenticationController alloc] initWithConfiguration:config];
    } @catch (NSException *exception) {
        
        
       NSString *message = @"Error de red.";
        [self.authenticationScanViewController dismissViewControllerAnimated:YES completion:^{
            
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                                                           message:message
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      
                                                                      NSLog(@"%@", message);
                                                                      // [self showAlertWithTitle:@"Authentication Mobile SDK" message:message];
                                                                      [self.navigationController popViewControllerAnimated:YES];
                                                                      [self.authenticationController destroy];
                                                                      self.authenticationController = nil;
                                                                      
                                                                  }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }];
        
        //        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:exception.name message:exception.reason preferredStyle:UIAlertControllerStyleAlert];
        //        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        //        [self presentViewController:alertController animated:YES completion:nil];
    }
      completionBlock(YES);
}

- (AuthenticationConfiguration*) createAuthenticationConfiguration {
    AuthenticationConfiguration *config = [AuthenticationConfiguration new];
    
    //Provide your API token
    config.apiToken = @"6623acf1-641c-43af-b62a-44ab4eb878ad";
    //Provide your API secret
    config.apiSecret = @"gYjTrv8IMH2UaUIw5b0mSXFKubLSkvXj";
    
    //Set the delegate that implements AuthenticationControllerDelegate
    config.delegate = self;
    
    //Use the following property to reference the Authentication transaction to a specific Netverify user identity
    //config.enrollmentTransactionReference = @"ENROLLMENT_TRANSACTION_REFERENCE";
    // config.enrollmentTransactionReference = [self.transactionReferenceTextField text];
    NSString *transactionReference = [[NSUserDefaults standardUserDefaults] stringForKey:@"enrollmentTransactionReference"];
    NSLog(@"ESTO ES LA VARIABLE RARA %@",transactionReference);
    if (transactionReference && transactionReference != nil && ![transactionReference  isEqual: @""]) {
        config.enrollmentTransactionReference = transactionReference;
    }
    //Set the dataCenter; default is JumioDataCenterUS
    //config.dataCenter = JumioDataCenterEU;
    
    //You can also set a customer identifier (max. 100 characters). Note: The customer ID should not contain sensitive data like PII (Personally Identifiable Information) or account login.
    //config.userReference = @"USER_REFERENCE";
    
    //Callback URL (max. 255 characters) for the confirmation after the authentication is completed. This setting overrides your Jumio account settings.
    //config.callbackUrl = @"https://www.example.com";
    
    //Configure your desired status bar style
    //config.statusBarStyle = UIStatusBarStyleLightContent;
    
    //Localizing labels
    //All label texts and button titles can be changed and localized using the Localizable-Authentication.strings file. Just adapt the values to your required language and use this file in your app.
    
    //Customizing look and feel
    //The SDK can be customized via UIAppearance to fit your application’s look and feel.
    //Please note, that only the below listed UIAppearance selectors are supported and taken into consideration. Experimenting with other UIAppearance or not UIAppearance selectors may cause unexpected behaviour or crashes both in the SDK or in your application. This is best avoided.
    
    // - Navigation bar: tint color, title color, title image
    //[[UINavigationBar jumioAppearance] setTintColor:[UIColor yellowColor]];
    //[[UINavigationBar jumioAppearance] setBarTintColor:[UIColor redColor]];
    //[[UINavigationBar jumioAppearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    //[[JumioNavigationBarTitleImageView jumioAppearance] setTitleImage: [UIImage imageNamed:@"<your-navigation-bar-title-image>"]];
    
    // - Custom general appearance - deactivate blur
    //[[JumioBaseView jumioAppearance] setDisableBlur:@YES];
    
    // - Custom general appearance - background color
    //[[JumioBaseView jumioAppearance] setBackgroundColor: [UIColor grayColor]];
    
    // - Custom general appearance - foreground color (text-elements and icons)
    //[[JumioBaseView jumioAppearance] setForegroundColor: [UIColor redColor]];
    
    // - Custom Positive Button Background Colors, custom class has to be imported (the same applies to JumioNegativeButton)
    //[[JumioPositiveButton jumioAppearance] setBackgroundColor:[UIColor cyanColor] forState:UIControlStateNormal];
    //[[JumioPositiveButton jumioAppearance] setBackgroundColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    
    //Custom Positive Button Background Image, custom class has to be imported
    //[[JumioPositiveButton jumioAppearance] setBackgroundImage:[UIImage imageNamed:@"<your-custom-image>"] forState:UIControlStateNormal];
    //[[JumioPositiveButton jumioAppearance] setBackgroundImage:[UIImage imageNamed:@"<your-custom-image>"] forState:UIControlStateHighlighted];
    
    //Custom Positive Button Title Colors, custom class has to be imported
    //[[JumioPositiveButton jumioAppearance] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    //[[JumioPositiveButton jumioAppearance] setTitleColor:[UIColor magentaColor] forState:UIControlStateHighlighted];
    
    //Custom Positive Button Title Colors, custom class has to be imported
    //[[JumioPositiveButton jumioAppearance] setBorderColor: [UIColor greenColor]];
    
    // Color for the face oval outline
    //[[JumioScanOverlayView jumioAppearance] setFaceOvalColor: [UIColor orangeColor]];
    // Color for the progress bars
    //[[JumioScanOverlayView jumioAppearance] setFaceProgressColor: [UIColor purpleColor]];
    // Color for the background of the feedback view
    //[[JumioScanOverlayView jumioAppearance] setFaceFeedbackBackgroundColor: [UIColor yellowColor]];
    // Color for the text of the feedback view
    //[[JumioScanOverlayView jumioAppearance] setFaceFeedbackTextColor: [UIColor brownColor]];
    
    // - Custom general appearance - font
    //The font has to be loaded upfront within the mainBundle before initializing the SDK
    //[[JumioBaseView jumioAppearance] setCustomLightFontName: @"<your-font-name-loaded-in-your-app>"];
    //[[JumioBaseView jumioAppearance] setCustomRegularFontName: @"<your-font-name-loaded-in-your-app>"];
    //[[JumioBaseView jumioAppearance] setCustomMediumFontName: @"<your-font-name-loaded-in-your-app>"];
    //[[JumioBaseView jumioAppearance] setCustomBoldFontName: @"<your-font-name-loaded-in-your-app>"];
    //[[JumioBaseView jumioAppearance] setCustomItalicFontName: @"<your-font-name-loaded-in-your-app>"];
    
    //You can get the current SDK version using the method below.
    //NSLog(@"%@", [AuthenticationController sdkVersion]);
    
    return config;
}

- (IBAction)startAuthentication {
    [self createAuthenticationController :@"" completion :^(BOOL success) {
        
        if (success){
            // fue exito
            [self unLockView];
        }else{
            //no fue exito
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
    }];
    
}

/**
 * Implement the following delegate method to receive the scanViewController to present, after initialization was finished successfully.
 * @param authenticationController the AuthenticationController instance
 * @param scanViewController UIViewController object to present
 **/
- (void)authenticationController:(nonnull AuthenticationController *)authenticationController didFinishInitializingScanViewController:(nonnull UIViewController *)scanViewController {
    NSLog(@"AuthenticationController did finish initializing");
    self.authenticationScanViewController = scanViewController;
    [self presentViewController:self.authenticationScanViewController animated:YES completion:nil];
}

/**
 * Implement the following delegate method to receive the final AuthenticationResult.
 * Dismiss the SDK view in your app once you received the result.
 * @param authenticationController the AuthenticationController instance
 * @param authenticationResult contains final authentication result (success or failed)
 * @param transactionReference the unique identifier of the scan session
 **/
- (void)authenticationController:(nonnull AuthenticationController *)authenticationController didFinishWithAuthenticationResult:(AuthenticationResult)authenticationResult transactionReference:(nonnull NSString *)transactionReference {
    NSLog(@"AuthenticationController finished successfully with transaction reference: %@", transactionReference);
    
    NSString *message = @"";
    
    
    switch (authenticationResult) {
        case AuthenticationResultSuccess:
        {
            message = @"Authentication process was successful";
            //Dismiss the SDK
            [self.authenticationScanViewController dismissViewControllerAnimated:YES completion:^{
                NSLog(@"%@", message);
                // [self showAlertWithTitle:@"Authentication Mobile SDK" message:message];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:(NSString*)@"AutenticationJumio"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //Destroy the instance to properly clean up the SDK
               // [self.authenticationController destroy];
                //self.authenticationController = nil;
                [self.navigationController popViewControllerAnimated:YES];
                
               
                
            }];
            break;
        }
        case AuthenticationResultFailed:
        default:
            message = @"Reconocimiento facial no exitoso, intenta de nuevo.";
            [self.authenticationScanViewController dismissViewControllerAnimated:YES completion:^{
                
                
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                                                               message:message
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          
                                                                          NSLog(@"%@", message);
                                                                          // [self showAlertWithTitle:@"Authentication Mobile SDK" message:message];
                                                                          [self.navigationController popViewControllerAnimated:YES];
                                                                          
                                                                          
                                                                      }];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }];
            break;
    }
    
  
    
    

   
   

 
}

/**
 * Implement the following delegate method for successful scans and user cancellation notifications.
 * Dismiss the SDK view in your app once you received the result.
 * @param authenticationController the AuthenticationController instance
 * @param error holds more detailed information about the error reason
 * @param transactionReference the unique identifier of the scan session
 **/
- (void)authenticationController:(nonnull AuthenticationController *)authenticationController didFinishWithError:(nonnull AuthenticationError *)error transactionReference:(NSString * _Nullable)transactionReference {
  //  NSString *message = [NSString stringWithFormat:@"AuthenticationController finished with error: %@, transactionReference: %@", error.message, transactionReference];
    
    //Dismiss the SDK
    void (^errorCompletion)(void) = ^{
      //  NSLog(@"%@", message);
        // [self showAlertWithTitle:@"Authentication Mobile SDK" message:message];
        [self performSegueWithIdentifier:@"ErrorFacial" sender:nil];
        
        //Destroy the instance to properly clean up the SDK
       // [self.authenticationController destroy];
       // self.authenticationController = nil;
    };
    
    if (self.authenticationScanViewController) {
        [self.authenticationScanViewController dismissViewControllerAnimated:YES completion:errorCompletion];
    } else {
        errorCompletion();
    }
    
}
//Helper methods
- (IBAction)transactionRefernece_onDone {
    [self.view endEditing:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
   
    
    
    if (manager == whiteListData) {
        
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        
        if([response count] > 0 ){
        response = [self cleanDictionary:response];
        NSLog(@"%@",response);
        if ([response[@"code"] intValue] == 0){
            
            
            //codigo inicializador
          
                 [self unLockView];
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:(NSString*)@"AutenticationJumio"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self.navigationController popViewControllerAnimated:YES];
                
                
           
            //termina codigo inicializador
            
        }else{
            
            
     
            [self createAuthenticationController :@"" completion :^(BOOL success) {
                
                if (success){
                   // fue exito
                     [self unLockView];
                }else{
                    //no fue exito
                [self.navigationController popViewControllerAnimated:YES];
                }
                
                
            }];
          
          
            
                

            
         
        }
            
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
     
    }
}

- (void)managerFailed:(pootEngine *)manager withKey:(NSString *)key errorCode:(pootEngineErrorCode)error{
    
    
    if (manager == whiteListData) {
        
        [self createAuthenticationController :@"" completion :^(BOOL success) {
            
            if (success){
                // fue exito
                [self unLockView];
            }else{
                //no fue exito
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            
        }];
    }
    
}

@end
