//
//  CNLoadCheckTransactionInfo.h
//  IngoSDK
//
//  Created by Stephen Gowen on 4/28/14.
//  Copyright (c) 2014 Ingo Money, Inc. All rights reserved.
//

@interface CNLoadCheckTransactionInfo : NSObject

@property (strong, nonatomic) NSString *transactionId;
@property (assign, nonatomic) int transactionType;
@property (assign, nonatomic) long checkAmountInPennies;
@property (assign, nonatomic) long loadAmountAfterFeeInPennies;
@property (strong, nonatomic) NSString *chosenCardNickname;
@property (assign, nonatomic) bool voidedCheckImageSubmitted;

- (NSString *)generateTitle;

- (NSString *)generateMessageBodyTop;

- (NSString *)generateMessageBodyBottom;

@end
