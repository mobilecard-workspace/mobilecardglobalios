//
//  iobegin.h
//  libiovation
//
//  Created by Greg Crow on 9/9/13.
//  Copyright (c) 2013 iovation, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import <AdSupport/AdSupport.h>
#import <CoreLocation/CoreLocation.h>

@interface iovation : NSObject

+ (NSString *) ioBegin;

@end
