//
//  3DSecure_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 1/15/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "Secure3D_Ctrl.h"
#import "NSDictionary (keychain).h"
#import <Mobilecard-Swift.h>

@interface Secure3D_Ctrl ()
{
    pootEngine *encrypter;
    pootEngine *tokenManager;
    pootEngine *userInfoManager;
    pootEngine *paymentManager;
    
    int numReloads;
    NSString *errorURL;
     NSString *errorAutURL;
    NSString *succesURL;
     NSString *formURL;
    
}
@end

@implementation Secure3D_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    errorURL = @"";
    errorAutURL = @"";
    succesURL = @"";
    
    [self initializeBlockerArray];
    
    numReloads = 0;
    _didFinishPurchase = NO;
    
    
    encrypter = [[pootEngine alloc] init];
    
    [_finishButton.layer setCornerRadius:6.0];
    
    [_webView setDelegate:self];
    _webView.contentMode = UIViewContentModeScaleAspectFit;
    [NSHTTPCookieStorage sharedHTTPCookieStorage].cookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    
    tokenManager = [[pootEngine alloc] init];
    [tokenManager setDelegate:self];
    [tokenManager setShowComments:developing];
    
    [_finishBar setHidden:YES];
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(backAction:)];
    self.navigationItem.leftBarButtonItem = newBackButton;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [_webView setFrame:CGRectMake(0, _webView.frame.origin.y, _webView.frame.size.width, self.view.frame.size.height-_webView.frame.origin.y)];
    
    
    switch (_type) {
        case serviceTypeIAVE:
        case serviceTypePASE:
        {
            NSString *data = [encrypter encryptJSONWithParams:_purchaseInfo withPassword:@"SJI74cm2dF"];
            
            developing?NSLog(@"data -> %@", _purchaseInfo):nil;
            
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"json", data,
                                             nil];
            [self UIWebViewWithPost:_webView url:_Secure3DURL params:webViewParams];
            
        }
            break;
            
        case serviceTypeMXTransfers:
        case serviceTypeLaCuenta:
        case serviceTypeCuantoTeDebo:
        case serviceTypeCuantoTeDeboByID:
        case serviceTypeTAE:
        case serviceTypeServices:
        case serviceTypePaymentPE:
        case serviceTypeMobileTag:
        case serviceCardPressentMx:
        {
            [self callTokenRequest];
            /*
             NSMutableArray *webViewParams = [[NSMutableArray alloc] init];
             
             for (NSString *key in _purchaseInfo) {
             [webViewParams addObject:key];
             [webViewParams addObject:_purchaseInfo[key]];
             }
             
             [self UIWebViewWithPost:_webView url:_Secure3DURL params:webViewParams];
             */
        }
            
            break;
            
      
        default:
        {
            [params setObject:@"diestelUsuario" forKey:@"usuario"];
            [params setObject:@"ym@J!ZHL" forKey:@"password"];
            
            [tokenManager startRequestWithValues:params forWS:WSGetDiestelToken withUncryption:YES];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        }
            break;
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)callTokenRequest
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    developing?NSLog(@"DATA INJECTED TO TOKEN -> %@", _purchaseInfo):nil;
    
    
    
    NSString *data = [encrypter encryptJSONWithParams:_purchaseInfo withPassword:@"SJI74cm2dF"];
    
    [params setObject:data forKey:@"Authorization"];
    
  //  if (_threatMetrixInfo) {
        
  //      [params setObject:_threatMetrixInfo[@"session_id"] forKey:@"Profile"];
 //   }
     [params setValue:@("232323") forKey:@"Profile"];
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithObjectsAndKeys:@"1", @"1", nil] options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [tokenManager startJSONRequestWithURL:WSGetToken withPost:JSONString andHeader:params];
    
    [self lockViewWithMessage:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)UIWebViewWithPost:(UIWebView *)uiWebView url:(NSString *)url params:(NSMutableArray *)params
{
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    [s appendString: [NSString stringWithFormat:@"<html><body onload=\"document.forms[0].submit()\">"
                      "<form method=\"post\" action=\"%@\">", url]];
    if([params count] % 2 == 1) { NSLog(@"UIWebViewWithPost error: params don't seem right"); return; }
    for (int i=0; i < [params count] / 2; i++) {
        [s appendString: [NSString stringWithFormat:@"<input type=\"hidden\" name=\"%@\" value=\"%@\" >", [params objectAtIndex:i*2], [params objectAtIndex:(i*2)+1]]];
    }
    [s appendString: @"</input></form></body></html>"];
    
    if (developing) {
        NSLog(@"URL to load -> %@", s);
    }
    
    [uiWebView loadHTMLString:s baseURL:nil];
    
    return;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self unLockView];
    
    numReloads++;
    
    developing?NSLog(@"FINISHED -> %@", [[[webView request] URL] absoluteString]):nil;
    developing?NSLog(@"num reloads -> %d", numReloads):nil;
    
  
    
    if (_type == serviceTypeIAVE || _type == serviceTypePASE){
        //es iave o pase
 
        if (numReloads == 3) {
            if (walletEnabled) {
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                userInfoManager = [[pootEngine alloc] init];
                [userInfoManager setShowComments:developing];
                
                NSString *cardNumber = [userInfoManager decryptedStringOfString:_selectedCardInfo[@"pan"] withSensitive:NO];
                
                if (!userInfo[@"materno"]) {
                    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    NSString *error = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('cc_name')[0].value = '%@'; document.getElementsByName('cc_number')[0].value = '%@'; document.getElementsByName('_cc_expmonth')[0].value = '%@'; document.getElementsByName('_cc_expyear')[0].value = '%@'; document.getElementsByName('cc_cvv2')[0].value = '%@'; document.getElementsByName('cc_type')[0].value = '%@'", [NSString stringWithFormat:@"%@", userInfo[kUserName]], cardNumber, [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] componentsSeparatedByString:@"/"][0], [NSString stringWithFormat:@"20%@", [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] componentsSeparatedByString:@"/"][1]], [userInfoManager decryptedStringOfString:_selectedCardInfo[@"codigo"] withSensitive:NO], [[cardNumber substringWithRange:NSMakeRange(0, 1)] intValue]==4?@"Visa":[[cardNumber substringWithRange:NSMakeRange(0, 1)] intValue]==5?@"Mastercard":@""]];
                } else {
                    NSString *error = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('cc_name')[0].value = '%@'; document.getElementsByName('cc_number')[0].value = '%@'; document.getElementsByName('_cc_expmonth')[0].value = '%@'; document.getElementsByName('_cc_expyear')[0].value = '%@'; document.getElementsByName('cc_cvv2')[0].value = '%@'; document.getElementsByName('cc_type')[0].value = '%@'", [NSString stringWithFormat:@"%@ %@ %@", userInfo[@"nombre"], userInfo[@"apellido"], userInfo[@"materno"]], cardNumber, [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] componentsSeparatedByString:@"/"][0], [NSString stringWithFormat:@"20%@", [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] componentsSeparatedByString:@"/"][1]], [userInfoManager decryptedStringOfString:_selectedCardInfo[@"codigo"] withSensitive:NO], [[cardNumber substringWithRange:NSMakeRange(0, 1)] intValue]==4?@"Visa":[[cardNumber substringWithRange:NSMakeRange(0, 1)] intValue]==5?@"Mastercard":@""]];
                }
                
                
                
            } else {
                userInfoManager = [[pootEngine alloc] init];
                [userInfoManager setDelegate:self];
                [userInfoManager setShowComments:developing];
                
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                [params setObject:userInfo[kUserLogin] forKey:@"login"];
                [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] forKey:@"password"];
                
                /*
                 [userInfoManager startProtectedRequestWithValues:params forWS:WSGetUserInfo withPass:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] Automated:NO];
                 */
                
                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
            }
            
        }
        
    }else{
    
        
        NSURL* url = [webView.request URL];
        NSString *StringURl = [url absoluteString];
       
        if ([StringURl rangeOfString:formURL options:NSCaseInsensitiveSearch].length>0){
     //if (numReloads == 1) {
        if (walletEnabled) {
            NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            userInfoManager = [[pootEngine alloc] init];
            [userInfoManager setShowComments:developing];
            
            NSString *cardNumber = [userInfoManager decryptedStringOfString:_selectedCardInfo[@"pan"] withSensitive:NO];
            
            if (!userInfo[@"materno"]) {
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                
                NSString *newVigencia = [userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO];
                NSLog(@"ESTO VALE NEWVIGENCIA %@",newVigencia);
                newVigencia = [newVigencia stringByReplacingOccurrencesOfString:@"/" withString:@""];
                
                if (_type == serviceCardPressentMx){
                    
                    NSString *error = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('name')[0].value = '%@'; document.getElementsByName('card')[0].value = '%@'; document.getElementsByName('period')[0].value = '%@'; document.getElementsByName('name')[0].readOnly=true; document.getElementsByName('card')[0].readOnly=true; document.getElementsByName('period')[0].readOnly=true; document.getElementsByName('cvv')[0].readOnly=true;  document.getElementsByName('cvv')[0].value = '%@'", _selectedCardInfo[@"nombre"], cardNumber, newVigencia, [userInfoManager decryptedStringOfString:_selectedCardInfo[@"codigo"] withSensitive:NO]]];
                    
                }else{
                NSString *error = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('name')[0].value = '%@ %@ %@';  document.getElementsByName('card')[0].value = '%@'; document.getElementsByName('period')[0].value = '%@'; document.getElementsByName('name')[0].readOnly=true; document.getElementsByName('card')[0].readOnly=true; document.getElementsByName('period')[0].readOnly=true; document.getElementsByName('cvv')[0].readOnly=true;  document.getElementsByName('cvv')[0].value = '%@'", [NSString stringWithFormat:@"%@", userInfo[kUserName]],[NSString stringWithFormat:@"%@", userInfo[kUserLastName]],[NSString stringWithFormat:@"%@", userInfo[kUserMotherLastName]] , cardNumber, newVigencia, [userInfoManager decryptedStringOfString:_selectedCardInfo[@"codigo"] withSensitive:NO]]];
                }
            } else {
                
                NSRange range = NSMakeRange(0,3);
                NSString *newVigencia = [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] stringByReplacingCharactersInRange:range withString:@" "];
                
                if (_type == serviceCardPressentMx){
                  
                   NSString *error = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('cc_name')[0].value = '%@'; document.getElementsByName('cc_number')[0].value = '%@'; document.getElementsByName('_cc_expmonth')[0].value = '%@'; document.getElementsByName('_cc_expyear')[0].value = '%@'; document.getElementsByName('cc_cvv2')[0].value = '%@'; document.getElementsByName('cc_name')[0].readOnly=true; document.getElementsByName('cc_number')[0].readOnly=true; document.getElementsByName('_cc_expmonth')[0].readOnly=true; document.getElementsByName('_cc_expyear')[0].readOnly=true; document.getElementsByName('cc_cvv2')[0].readOnly=true; document.getElementsByName('cc_type')[0].value = '%@'", _selectedCardInfo[@"nombre"] , cardNumber, [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] componentsSeparatedByString:@"/"][0], [NSString stringWithFormat:@"20%@", [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] componentsSeparatedByString:@"/"][1]], [userInfoManager decryptedStringOfString:_selectedCardInfo[@"codigo"] withSensitive:NO], [[cardNumber substringWithRange:NSMakeRange(0, 1)] intValue]==4?@"Visa":[[cardNumber substringWithRange:NSMakeRange(0, 1)] intValue]==5?@"Mastercard":@""]];
                    
                }else{
                    
                NSString *error = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('cc_name')[0].value = '%@'; document.getElementsByName('cc_number')[0].value = '%@'; document.getElementsByName('_cc_expmonth')[0].value = '%@'; document.getElementsByName('_cc_expyear')[0].value = '%@'; document.getElementsByName('cc_cvv2')[0].value = '%@'; document.getElementsByName('cc_name')[0].readOnly=true; document.getElementsByName('cc_number')[0].readOnly=true; document.getElementsByName('_cc_expmonth')[0].readOnly=true; document.getElementsByName('_cc_expyear')[0].readOnly=true; document.getElementsByName('cc_cvv2')[0].readOnly=true; document.getElementsByName('cc_type')[0].value = '%@'", [NSString stringWithFormat:@"%@ %@ %@", userInfo[@"nombre"], userInfo[@"apellido"], userInfo[@"materno"]], cardNumber, [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] componentsSeparatedByString:@"/"][0], [NSString stringWithFormat:@"20%@", [[userInfoManager decryptedStringOfString:_selectedCardInfo[@"vigencia"] withSensitive:NO] componentsSeparatedByString:@"/"][1]], [userInfoManager decryptedStringOfString:_selectedCardInfo[@"codigo"] withSensitive:NO], [[cardNumber substringWithRange:NSMakeRange(0, 1)] intValue]==4?@"Visa":[[cardNumber substringWithRange:NSMakeRange(0, 1)] intValue]==5?@"Mastercard":@""]];
                }
            }

            
            
        } else {
            userInfoManager = [[pootEngine alloc] init];
            [userInfoManager setDelegate:self];
            [userInfoManager setShowComments:developing];
            
            NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:userInfo[kUserLogin] forKey:@"login"];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] forKey:@"password"];
            
            /*
            [userInfoManager startProtectedRequestWithValues:params forWS:WSGetUserInfo withPass:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] Automated:NO];
             */
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        }
        
    }
        }//termina la validacion de si no es o si iave y pase arriba no es
    
    NSURL* url = [webView.request URL];
    NSString *StringURl = [url absoluteString];
    
    if ([StringURl rangeOfString:@"respuestaProsa" options:NSCaseInsensitiveSearch].length > 0 || [StringURl rangeOfString:@"payworks2RecRespuesta" options:NSCaseInsensitiveSearch].length > 0) {
        _didFinishPurchase = YES;
        [_finishBar setHidden:NO];
        [_webView setFrame:CGRectMake(0, _webView.frame.origin.y, _webView.frame.size.width, self.view.frame.size.height-_webView.frame.origin.y-44)];
    }
    
    if ((_type == serviceCardPressentMx || _type == serviceTypeTAE || _type == serviceTypeServices || _type == serviceTypeMXTransfers || _type == serviceTypeCuantoTeDebo) && ([StringURl rangeOfString:succesURL options:NSCaseInsensitiveSearch].length>0)){
       //le hice dos splits al contenido del html para conseguir el json string
          NSString *allcontent = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        NSArray *items = [allcontent componentsSeparatedByString:@">"];
        NSString *jsonContent=[items objectAtIndex:5];
        NSArray *itemsTwo = [jsonContent componentsSeparatedByString:@"<"];
         NSString *jsonContentFinish=[itemsTwo objectAtIndex:0];
        //resultado jsonstring
        NSLog(@"JsonFinish: %@",jsonContentFinish);
        //se paso a data y a json 
        NSData *data = [jsonContentFinish dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"RESPONSE ANTES DE SALIR %@",response);
        if ([_delegate conformsToProtocol:@protocol(Secure3DDelegate)]&&[_delegate respondsToSelector:@selector(PaymentWithThreatMetrixResponse:)]) {
            [self.navigationController popViewControllerAnimated:YES];
            [[self delegate] PaymentWithThreatMetrixResponse:response];
        }
        
    }
    
    if (_type == serviceTypeLaCuenta && ([StringURl rangeOfString:@"LCPFServices/payworks/enqueuePayment" options:NSCaseInsensitiveSearch].length>0 || [StringURl rangeOfString:@"LCPFServices/payworks/payworksRec3DRespuesta" options:NSCaseInsensitiveSearch].length>0 || [StringURl rangeOfString:@"LCPFServices/payworks/payworks2RecRespuesta" options:NSCaseInsensitiveSearch].length>0 || [StringURl rangeOfString:@"/LCPFServices/payworks/error_previo_pago" options:NSCaseInsensitiveSearch].length>0) ) {
        
        NSString *rawJSON = [webView stringByEvaluatingJavaScriptFromString:
                          @"document.body.innerHTML"];
        
        NSData *data = [rawJSON dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if ([_delegate conformsToProtocol:@protocol(Secure3DDelegate)]&&[_delegate respondsToSelector:@selector(Secure3DResponse:)]&&response) {
            [self.navigationController popViewControllerAnimated:YES];
            
            [_delegate Secure3DResponse:response];
        }
    }
    
    if (_type == serviceTypeCuantoTeDebo && ([StringURl rangeOfString:@"CuantoTeDebo/payworks/error_previo_pago" options:NSCaseInsensitiveSearch].length>0 || [StringURl rangeOfString:@"CuantoTeDebo/3d/secure/response" options:NSCaseInsensitiveSearch].length>0 || [StringURl rangeOfString:@"CuantoTeDebo/payworks/response" options:NSCaseInsensitiveSearch].length>0) ) {
        
        NSString *rawJSON = [webView stringByEvaluatingJavaScriptFromString:
                             @"document.body.innerHTML"];
        
        NSData *data = [rawJSON dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if ([_delegate conformsToProtocol:@protocol(Secure3DDelegate)]&&[_delegate respondsToSelector:@selector(Secure3DResponse:)]&&response) {
            [self.navigationController popViewControllerAnimated:YES];
            
            [_delegate Secure3DResponse:response];
        }
    }
    
    
    
    if ((_type == serviceTypeMXTransfers) && ([StringURl rangeOfString:@"/H2HPayment/payworks/payworks2RecRespuesta" options:NSCaseInsensitiveSearch].length>0 || [StringURl rangeOfString:@"/H2HPayment/payworks/payworksRec3DRespuesta" options:NSCaseInsensitiveSearch].length>0)) {
        
        NSString *rawJSON = [webView stringByEvaluatingJavaScriptFromString:
                             @"document.body.innerHTML"];
        
        NSDictionary *response = [[NSDictionary alloc] initWithObjectsAndKeys:rawJSON, kDescriptionKey, nil];
        
        [response storeToKeychainWithKey:kKeychainResultKey andGroup:kKeychainGroup];
    }
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSURL* url = [webView.request URL];
    NSString *StringURl = [url absoluteString];
    
    [self lockViewWithMessage:@"Cargando..."];
}

-(BOOL)webView:(UIWebView *)mainWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    
    NSString *StringURl = request.URL.absoluteString;
    if ([StringURl rangeOfString:errorAutURL options:NSCaseInsensitiveSearch].length>0 || [StringURl rangeOfString:errorURL options:NSCaseInsensitiveSearch].length>0){
        NSLog(@"EXISTE UN ERROR EN EL PAGO");
        
        
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        RechazadaGeneralViewController *nextView = [nextStory instantiateViewControllerWithIdentifier:@"RechazoGeneral"];
        
        [[self navigationController] pushViewController:nextView animated:YES];
        
        return NO;
    }else if ([StringURl rangeOfString:succesURL options:NSCaseInsensitiveSearch].length>0){
        NSLog(@"EXITO EN EL PAGO TOMAMOS ACCION");
        
        //para que si lo mande y jalemos el json
        if (_type == serviceCardPressentMx || _type == serviceTypeTAE || _type == serviceTypeServices || _type == serviceTypeMXTransfers || _type == serviceTypeCuantoTeDebo){
            return YES;
            
        }else{
            //vista generica de exito
      
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AprobadaGeneralViewController *nextView = [nextStory instantiateViewControllerWithIdentifier:@"ExitoGeneral"];
    
        [[self navigationController] pushViewController:nextView animated:YES];
        }

        
        return NO;
    }
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
     
        
        
        [_webView loadRequest:request];
        
        return NO;
    }
    
    
    developing?NSLog(@"STARTING... -> %@", [[request URL] absoluteString]):nil;
    developing?NSLog(@"Request Headers -> %@", [request allHTTPHeaderFields]):nil;
    developing?NSLog(@"Request Body -> %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]):nil;

    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self unLockView];
    
    developing?NSLog(@"%@",error.description):nil;
    
    if (developing) {
        //UIAlertView *alerty = [[UIAlertView alloc] initWithTitle:@"Error" message:developing?error.description:@"Un error se ha producido, favor de intentar más tarde..." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        //[alerty show];
    }
    
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == tokenManager) {
        
        NSDictionary *resp = (NSDictionary*)json;
        
        
        
        if ([[resp objectForKey:@"code"] intValue]==0) {
            
            //THREATMETRIX IMPLEMENTATION
            //if (_threatMetrixInfo) {
                if (![resp[@"secure"] boolValue]) {
                    
                    paymentManager = [[pootEngine alloc] init];
                    [paymentManager setDelegate:self];
                    [paymentManager setShowComments:developing];
                    
                    NSMutableDictionary *threadPurchaseInfo = [NSMutableDictionary dictionaryWithDictionary:_purchaseInfo];
                    
                    developing?NSLog(@"Thread info -> %@", threadPurchaseInfo):nil;
                    
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    
                    [params setObject:resp[@"token"] forKey:@"Authorization"];
                   // [params setObject:_threatMetrixInfo[@"session_id"] forKey:@"Profile"];
                    
                    NSError *JSONError;
                    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:threadPurchaseInfo options:0 error:&JSONError];
                    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                    
                    switch (_type) {
                            
                   
                      
                        case serviceTypeCuantoTeDeboByID:
                        {
                            NSString *post = [NSString stringWithFormat:@"idUser=%@&idCard=%@&idBitacora=%@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey], _purchaseInfo[@"idCard"], _purchaseInfo[@"idBitacora"]];
                         
                            [paymentManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/%@/paymentByIdBP", WSSandPPayment, resp[@"accountId"]]  withPost:post andHeader:params andContentType:@"application/x-www-form-urlencoded"];
                            
                            [self lockViewWithMessage:nil];
                            return;
                        }
                            break;
                        case serviceTypeCuantoTeDebo:
                        case serviceCardPressentMx:
                        case serviceTypePaymentPE:
                        {
                            [paymentManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/%@/%@/pagoBP", WSSandPPayment, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType], resp[@"accountId"]] withPost:JSONString andHeader:params];
                            
                    
                            
                            
                            [self lockViewWithMessage:nil];
                            return;
                        }
                            break;
                        case serviceTypeMXTransfers:
                        {
                            [paymentManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/%@/pagoBP", H2HServiceURLPayment, resp[@"accountId"]] withPost:JSONString andHeader:params];
                            [self lockViewWithMessage:nil];
                            return;
                        }
                            break;
                            
                        default: //TAE & services
                        {
                            [paymentManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/%@/pagoBP", WSSetPaymentThreatMetrix, resp[@"accountId"]] withPost:JSONString andHeader:params];
                            [self lockViewWithMessage:nil];
                            return;
                        }
                            break;
                    }
                }
           // }
            //END OF THREATMETRIX IMPLEMENTATION
            
            
            //TEMRMINA IF DE SECURE
            
            
            
            if (resp[@"token"]) {
                [_purchaseInfo setObject:resp[@"token"] forKey:@"token"];
            }
            
            NSDictionary *dict = resp[@"paths"];
            NSString *start = dict[@"start"];
            errorAutURL = dict[@"authError"];
            errorURL = dict[@"error"];
            succesURL = dict[@"success"];
            formURL = dict[@"form"];
            
            
            developing?NSLog(@"purchaseInfo -> %@", _purchaseInfo):nil;
            
            NSString *originalData = [encrypter encryptJSONWithParams:_purchaseInfo withPassword:@"SJI74cm2dF"];
            
            NSString *data = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                   NULL,
                                                                                                   (CFStringRef)originalData,
                                                                                                   NULL,
                                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                   kCFStringEncodingUTF8 ));
            
          //  NSURL *url = [NSURL URLWithString:[_Secure3DURL stringByReplacingOccurrencesOfString:@"HERE" withString:resp[@"accountId"]]];
            
           
            NSURL *url = [NSURL URLWithString:start];
           
            /*
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"json", data,
                                             nil];
            [self UIWebViewWithPost:_webView url:[_Secure3DURL stringByReplacingOccurrencesOfString:@"HERE" withString:resp[@"accountId"]] params:webViewParams];
            */
            
            //developing?NSLog(@"Data -> %@", data):nil;
            
           // NSString *body = [NSString stringWithFormat:@"json=%@", data];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL: url];
            [request setHTTPMethod: @"GET"];
         //   [request setHTTPBody: [body dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setValue:resp[@"token"] forHTTPHeaderField:@"Authorization"];
            
            [_webView loadRequest: request];
            
        } else {
          //  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"Información" message:NSLocalizedString(@"Hubo un error en la obtención del permiso para pago. Por favor, intente más tarde.", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
          //  [alertMsg show];
            
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:resp[@"message"] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                           [alertMsg show];
            
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
    if (manager == paymentManager) {
        
        
     
        
        NSDictionary *response = (NSDictionary*)json;
        
        NSMutableDictionary *resp = [NSMutableDictionary dictionaryWithDictionary:response];
        
        //empieza validacion mobiletag
        if (_type == serviceTypeMobileTag){
          
            if ([[resp objectForKey:@"code"] intValue]==0) {
            
              
                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                AprobadaGeneralViewController *nextView = [nextStory instantiateViewControllerWithIdentifier:@"ExitoGeneral"];
                
                [[self navigationController] pushViewController:nextView animated:YES];

            
            }else{
                
                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                RechazadaGeneralViewController *nextView = [nextStory instantiateViewControllerWithIdentifier:@"RechazoGeneral"];
                
                [[self navigationController] pushViewController:nextView animated:YES];
            }
                
            
            
            //termina mobiletag
                
            }else{
        
        resp = [self cleanDictionary:resp];
        
        if ([_delegate conformsToProtocol:@protocol(Secure3DDelegate)]&&[_delegate respondsToSelector:@selector(PaymentWithThreatMetrixResponse:)]) {
            [self.navigationController popViewControllerAnimated:YES];
            [[self delegate] PaymentWithThreatMetrixResponse:resp];
        }
            
        }
    }
    
    if (manager == userInfoManager) {
        NSDictionary *resp = (NSDictionary*)json;
        
        if (!resp[@"materno"]) {
            NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            NSString *error = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('cc_name')[0].value = '%@'; document.getElementsByName('cc_number')[0].value = '%@'; document.getElementsByName('_cc_expmonth')[0].value = '%@'; document.getElementsByName('_cc_expyear')[0].value = '%@'; document.getElementsByName('cc_cvv2')[0].value = '%@'", [NSString stringWithFormat:@"%@", userInfo[kUserName]], resp[@"tarjeta"], [userInfo[kCardValid] componentsSeparatedByString:@"/"][0], [NSString stringWithFormat:@"20%@", [userInfo[kCardValid] componentsSeparatedByString:@"/"][1]], @""]];
        } else {
            NSString *error = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('cc_name')[0].value = '%@'; document.getElementsByName('cc_number')[0].value = '%@'; document.getElementsByName('_cc_expmonth')[0].value = '%@'; document.getElementsByName('_cc_expyear')[0].value = '%@'; document.getElementsByName('cc_cvv2')[0].value = '%@'", [NSString stringWithFormat:@"%@ %@ %@", resp[@"nombre"], resp[@"apellido"], resp[@"materno"]], resp[@"tarjeta"], [resp[@"vigencia"] componentsSeparatedByString:@"/"][0], [NSString stringWithFormat:@"20%@", [resp[@"vigencia"] componentsSeparatedByString:@"/"][1]], @""]];
        }
    }
    /*
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                UIActionSheet *actionMsg = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Seleccione la tarjeta de pago que desee usar:", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) destructiveButtonTitle:nil otherButtonTitles: nil];
                
                for (NSDictionary *cardInfo in response[kUserCardArray]) {
                    NSString *cardString = [cardManager decryptedStringOfString:cardInfo[@"pan"] withSensitive:NO];
                    [actionMsg addButtonWithTitle:[NSString stringWithFormat:@"XXXX XXXX XXXX %@", [cardString substringWithRange:NSMakeRange(12, [cardString length]==15?3:4)]]];
                }
                
                [actionMsg showInView:self.view];
                
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
    }*/
}

- (IBAction)backAction:(id)sender {
    
    if (_didFinishPurchase) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        UIAlertView *alerty = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información importante", nil) message:NSLocalizedString(@"Si ya iniciaste el proceso de pago podría aplicarse el cargo bancario\n¿Estás seguro de ir a la pantalla anterior?", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"NO", NSLocalizedString(@"Sí, deseo regresar", nil), nil];
        [alerty setTag:900];
        [alerty show];
    }
}

- (IBAction)finishAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 900:
        {
            if (buttonIndex == 1) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                nil;
            }
        }
            break;
            
        default:
            //[self subAlertView:alertView didDismissWithButtonIndex:buttonIndex];
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //NSLog(@"Button pressed -> %ld", (long)buttonIndex);
}

@end
