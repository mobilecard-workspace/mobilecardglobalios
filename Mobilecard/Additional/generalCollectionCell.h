//
//  generalCollectionCell.h
//  MobileCard_X
//
//  Created by David Poot on 12/20/16.
//  Copyright © 2016 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface generalCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *leftMargin;
@property (weak, nonatomic) IBOutlet UIView *topMargin;
@property (weak, nonatomic) IBOutlet UIView *bottomMargin;
@property (weak, nonatomic) IBOutlet UIView *rightMargin;
@property (weak, nonatomic) IBOutlet UILabel *cellText;
@property (weak, nonatomic) IBOutlet UIButton *cellButton;

- (void) selectCell;
- (void) deSelectCell;
- (void) updateState;
@end
