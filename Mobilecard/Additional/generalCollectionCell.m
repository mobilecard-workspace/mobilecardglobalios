//
//  generalCollectionCell.m
//  MobileCard_X
//
//  Created by David Poot on 12/20/16.
//  Copyright © 2016 David Poot. All rights reserved.
//

#import "generalCollectionCell.h"


@implementation generalCollectionCell
{
    BOOL selected;
}

- (void) selectCell
{
    selected = YES;
    [self updateState];
}

- (void) deSelectCell
{
    selected = NO;
    [self updateState];
}

- (void) updateState
{
    if (selected) {
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.20]];
    } else {
        [self setBackgroundColor:[UIColor clearColor]];
    }
}
@end
