//
//  RecargasModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/21/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecargasModel : NSObject
@property (nonatomic, assign)    NSInteger id_recarga;
@property (nonatomic, copy)      NSString  *description_recarga;
@property (nonatomic, assign)     BOOL  esAntad;
@end

NS_ASSUME_NONNULL_END
