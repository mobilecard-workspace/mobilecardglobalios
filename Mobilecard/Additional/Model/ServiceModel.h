//
//  ServiceModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/20/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceModel : NSObject
@property (nonatomic, assign)    NSInteger id_service;
@property (nonatomic, copy)      NSString  *description_service;
@end

NS_ASSUME_NONNULL_END
