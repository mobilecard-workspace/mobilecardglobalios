//
//  login_UserTypeSelection.h
//  Mobilecard
//
//  Created by David Poot on 9/8/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface login_UserTypeSelection : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *selectedCountry;

@end
