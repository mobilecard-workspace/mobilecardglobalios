//
//  login_UserTypeSelection.m
//  Mobilecard
//
//  Created by David Poot on 9/8/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "login_UserTypeSelection.h"

@interface login_UserTypeSelection ()

@end

@implementation login_UserTypeSelection

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else  if (indexPath.row == 5) {
        [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
@end
