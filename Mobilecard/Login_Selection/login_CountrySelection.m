//
//  login_CountrySelection.m
//  Mobilecard
//
//  Created by David Poot on 9/8/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "login_CountrySelection.h"
#import "login_UserTypeSelection.h"
#import "SignIn_Ctrl.h"
#import <Mobilecard-Swift.h>
#import <Crashlytics/Crashlytics.h>
#import "LGSideMenuController.h"
#import <Mobilecard-Swift.h>
#import <Alamofire/Alamofire-umbrella.h>
@interface login_CountrySelection ()
{
    pootEngine *countryManager;
    NSMutableArray *countryArray;
    ServicesObjc *services;
    
    
    int selectedId;
}
@end

@implementation login_CountrySelection

- (void)viewDidLoad {
    [super viewDidLoad];
    

 
 services = [[ServicesObjc alloc] init];
     services.delegate = self;
    self.tableView.delegate = self;
    
    //DELETE
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserType];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    developing?NSLog(@"User -> %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]):nil;
    developing?NSLog(@"UserType -> %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType]):nil;
    //DELETE
    
    
    
 
    
    
    
     DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
        //autologin Alejandro
        if(DataUser.PASS != nil){
            //pais
        if( [DataUser.COUNTRY isEqualToString:@"PERU"] || [DataUser.COUNTRY isEqualToString:@"PE"]){
            DataUser.COUNTRY = @"PE";
            
            //negocio
            if([DataUser.USERTYPE isEqualToString:@"NEGOCIO"]){
            
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                UIViewController *viewcontroller =(ContainerSliderPEViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Container"];
                
                UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                
                [navs setNavigationBarHidden:YES animated:NO];
                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:navs animated:YES completion:nil];
            
                
            }else{
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                
                UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                
                [navs setNavigationBarHidden:YES animated:NO];
                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:navs animated:YES completion:nil];
             
                
                
                //[self performSegueWithIdentifier:@"startMain" sender:nil];
            }
        
        }else{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]) {
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
                    //[self performSegueWithIdentifier:@"startMain" sender:nil];
                    ////// reemplazo de codigo para ingresar al view de log-in Raul mendez ///////////
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                    
                    //[self performSegueWithIdentifier:@"startMain" sender:nil];
                } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
                   
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainCommerce"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuCommerce"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                    
                    //   [self performSegueWithIdentifier:@"startMain_Commerce" sender:nil];
                } else {
                 
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                   
                    
                    // [self performSegueWithIdentifier:@"startMain" sender:nil];
                }
            }
        }
            
            
        }else{
    
    //termina login
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
            //[self performSegueWithIdentifier:@"startMain" sender:nil];
            ////// reemplazo de codigo para ingresar al view de log-in Raul mendez ///////////
            [self performSegueWithIdentifier:@"startMain" sender:nil];
           
        } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
          
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainCommerce"bundle:nil];
            UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuCommerce"];
            
            UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
            
            [navs setNavigationBarHidden:YES animated:NO];
            
            navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            navs.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:navs animated:YES completion:nil];
           
            //  [self performSegueWithIdentifier:@"startMain_Commerce" sender:nil];
        } else {
            [self performSegueWithIdentifier:@"startMain" sender:nil];
            
        }
    }
            
        }//termina el if del pass != nil que lo otro era el autologin pasado
    countryArray = [[NSMutableArray alloc] init];
    NSDictionary *header = @{};
    NSDictionary *params = @{};
    [services sendWithType:@"GET" url:WSCatalogGetCountries params:params header:header message:@"Obteniendo" animate:YES];
    
    /*
  
    
    countryManager = [[pootEngine alloc] init];
    [countryManager setDelegate:self];
    [countryManager setShowComments:developing];
    
    [countryManager startWithoutPostRequestWithURL:WSCatalogGetCountries];
    
    [self lockViewWithMessage:nil];*/
  
}
    
 

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [countryArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [countryArray[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:countryArray[indexPath.row][kIdentifierKey] forIndexPath:indexPath];
    
 
    
    if ([countryArray[indexPath.row][kIdentifierKey] isEqualToString:@"data"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
                [label setText:countryArray[indexPath.row][kDescriptionKey][@"nombre"]];
            }
            if ([element isKindOfClass:[UIImageView class]]) {
                UIImageView *image = element;
                if (image.tag == 1) {
                    pootEngine *imageManager;
                    imageManager = [[pootEngine alloc] init];
                    
                    
                    [imageManager startImageRequestWithURL:countryArray[indexPath.row][kDescriptionKey][@"url"] andImageView:image];
                    
                    
                  
                }
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([countryArray[indexPath.row][kIdentifierKey] isEqualToString:@"reload"]) {
       /* countryArray = [[NSMutableArray alloc] init];
        
        countryManager = [[pootEngine alloc] init];
        [countryManager setDelegate:self];
        [countryManager setShowComments:developing];
        
        [countryManager startWithoutPostRequestWithURL:WSCatalogGetCountries];
        
        [self lockViewWithMessage:nil];*/
        
        NSDictionary *header = @{};
          NSDictionary *params = @{};
          [services sendWithType:@"GET" url:WSCatalogGetCountries params:params header:header message:@"Obteniendo" animate:YES];
        
        return;
    }
    selectedId = (int)indexPath.row;
    
    [[NSUserDefaults standardUserDefaults] setObject:countryArray[selectedId][kDescriptionKey] forKey:(NSString*)kUserIdCountry];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
NSLog(@"%@", countryArray[indexPath.row][kDescriptionKey][@"codigo"]);

    NSString *a = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"];
    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
    NSLog(@"%s","adkasndssaodosa");
    NSLog(@"%@",a);
    
    DataUser.COUNTRY = countryArray[selectedId][kDescriptionKey][@"codigo"];
    DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
    NSLog(@"Este es %@",DataUser.COUNTRYID);
     DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    
    if ([countryArray[indexPath.row][kDescriptionKey][@"codigo"] isEqualToString:@"MX"] || [countryArray[indexPath.row][kDescriptionKey][@"codigo"] isEqualToString:@"PE"]) {
        [self performSegueWithIdentifier:@"userSelection" sender:nil];
    } else {
        [self performSegueWithIdentifier:@"startMain" sender:nil];
    }
}


#pragma mark pootEngine Handler
/*
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (countryManager == manager) {
        [countryArray removeAllObjects];
        
        [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"120", kHeightKey, @"header", kIdentifierKey, nil]];
        
        NSArray *countryRaw = (NSArray*)json;
        
        for (NSDictionary *element in countryRaw) {
            [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"100", kHeightKey, @"data", kIdentifierKey, element, kDescriptionKey, nil]];
            [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"15", kHeightKey, @"space", kIdentifierKey, nil]];
        }
        
        [self.tableView reloadData];
    }
}*/
/*
- (void)managerFailed:(pootEngine *)manager withKey:(NSString *)key errorCode:(pootEngineErrorCode)error{
     [countryArray removeAllObjects];
    

    [self unLockView];
    
      [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"70", kHeightKey, @"space", kIdentifierKey, nil]];
    
      [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"90", kHeightKey, @"reload", kIdentifierKey, nil]];
     [self.tableView reloadData];
    
}*/



//delegados de los servicios
- (void)responseServiceWithType:(NSString *)type endpoint:(NSString *)endpoint response:(NSDictionary *)response{
    
    
}

- (void)responseArrayServiceWithType:(NSString *)type endpoint:(NSString *)endpoint response:(NSArray *)response{
    
    
    NSLog(@"RESPONSE: %@",response);
   
    if ([endpoint isEqualToString:WSCatalogGetCountries]) {
   
          [countryArray removeAllObjects];
          
          [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"120", kHeightKey, @"header", kIdentifierKey, nil]];
        
        
      
        
          for (NSDictionary *element in response) {
         
             
              [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"100", kHeightKey, @"data", kIdentifierKey, element, kDescriptionKey, nil]];
              [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"15", kHeightKey, @"space", kIdentifierKey, nil]];
            
                 
          }
          
       
       
        
          [self.tableView reloadData];
      }
    
}



- (void)errorServiceWithEndpoint:(NSString *)endpoint{
    
    
 if ([endpoint isEqualToString:WSCatalogGetCountries]) {
        [countryArray removeAllObjects];
        [self unLockView];
                
             [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"70", kHeightKey, @"space", kIdentifierKey, nil]];
           
             [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"90", kHeightKey, @"reload", kIdentifierKey, nil]];
            [self.tableView reloadData];
           
        
    }
    
}

@end
