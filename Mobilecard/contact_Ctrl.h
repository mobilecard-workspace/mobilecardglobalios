//
//  contact_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 11/7/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonController.h"
#import <MessageUI/MessageUI.h>

@interface contact_Ctrl : mT_commonController <UITableViewDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonMenu;
@property (weak, nonatomic) IBOutlet UITableView *dataTable;

@property (weak, nonatomic) NSString *_fromPE;


@end
