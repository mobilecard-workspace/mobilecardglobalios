//
//  order_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 4/28/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "order_Ctrl.h"

@interface order_Ctrl ()
{
    
    pootEngine *countryManager;
    pootEngine *currencyManager;
    pootEngine *payTypeManager;
    pootEngine *branchManager;
    pootEngine *exchangeManager;
    pootEngine *orderManager;
    pootEngine *cardManager;
    pootEngine *senderManager;
    pootEngine *confirmationManager;
    
    NSNumberFormatter *numFormatter;
    
    NSDictionary *selectedCardBackup;
    NSString *orderReceiverId;
    NSDictionary *confirmationResult;
    
    NSMutableArray *payTypeArray;
    NSMutableArray *cellArray;
    
    NSMutableDictionary *currentCurrency;
    NSMutableDictionary *currentExchangeRate;
    
    orderStage stage;
    
    int selectedID;
    
    BOOL firstLoad;
    
    float finalAmount;
}

@end

@implementation order_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    
    //WHITELISTJUMIO
    
    //recuperamos el valor
    NSString * val = [[NSUserDefaults standardUserDefaults] objectForKey: @"DataUser.whitelistjumio"];
    
    //seteamo el valor
    __whiteList = val;
    
    //borramos el valor
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:nil forKey:@"DataUser.whitelistjumio"];
    [userDefaults synchronize];
    

    
    //termina whitelistjumio
    
 //   [self addShadowToView:_continue_Button];
    
    firstLoad = NO;
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [self resetView:orderStageNew];
    
    cellArray = [[NSMutableArray alloc] init];
    payTypeArray = [[NSMutableArray alloc] init];
    currentCurrency = [[NSMutableDictionary alloc] init];
    currentExchangeRate = [[NSMutableDictionary alloc] init];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self initializePickersForView:self.navigationController.view];
    
    
    [_countryText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_branchText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_amountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    
    [_branchText setAllowedCharacters:[[NSCharacterSet characterSetWithCharactersInString:@"!"] invertedSet]];
    [_amountText setKeyboardType:UIKeyboardTypeDecimalPad];
    [_amountText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]];
    
    [self addValidationTextField:_countryText];
    [self addValidationTextField:_branchText];
    [self addValidationTextField:_amountText];
    
    [self addValidationTextFieldsToDelegate];
    
    countryManager = [[pootEngine alloc] init];
    [countryManager setDelegate:self];
    [countryManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@"NO" forKey:@"operation"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
   

    
    [countryManager startRequestWithURL:ViamericasGetCountries withPost:post];
   
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continue_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        
        if ([[_amountText text] floatValue]>20.00) {
            
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"El monto máximo permitido por transacción es de $20.00 USD" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
            
            return;
        }
        
        [self performSegueWithIdentifier:@"recipient" sender:self];
    }
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Tsf1",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"recipient"]) {
        Receiver_Selection_Ctrl *nextView = [segue destinationViewController];
        nextView._whiteList = __whiteList;
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setObject:payTypeArray[selectedID][@"paymentModeId"] forKey:@"idModePay"];
        [params setObject:currentCurrency[@"ID"] forKey:@"modeCurrency"];
        [params setObject:[_amountText text] forKey:@"netAmount"];
        [params setObject:[_branchText infoArray][[_branchText selectedID]][[_branchText idLabel]] forKey:@"idBranchPay"];
        [params setObject:@"" forKey:@"idStateFrom"];
        [params setObject:@"K" forKey:@"idPayment"];
        [params setObject:[_countryText infoArray][[_countryText selectedID]][[_countryText idLabel]] forKey:@"idCountry"];
        [params setObject:currentCurrency[@"NAME"] forKey:@"NAME"];
        [params setObject:currentExchangeRate[@"exchangeRate"] forKey:@"exchangeRate"];
        [params setObject:[_branchText infoArray][[_branchText selectedID]][@"nameGroup"] forKey:@"nameGroup"];
        [params setObject:payTypeArray[selectedID][@"paymentModeName"] forKey:@"paymentModeName"];
        
        
        [nextView setUserData:params];
    }
}


- (void)currencySearchWithIdCountry:(NSString*)idCountry
{
    currencyManager = [[pootEngine alloc] init];
    [currencyManager setDelegate:self];
    [currencyManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:idCountry forKey:@"idCountry"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [currencyManager startRequestWithURL:ViamericasGetCurrency withPost:post];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

- (void)paymentTypeSearchWithIdCountry:(NSString*)idCountry AndIdCurrency:(NSString*)idCurrency
{
    if ([currentCurrency count]>0) {
        payTypeManager = [[pootEngine alloc] init];
        [payTypeManager setDelegate:self];
        [payTypeManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:idCountry forKey:@"idCountry"];
        [params setObject:idCurrency forKey:@"idCurrency"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [payTypeManager startRequestWithURL:ViamericasGetPaymentMode withPost:post];
        
        [self lockViewWithMessage:nil];
    }
}


- (void)branchSearch
{
    if ([payTypeArray count]>0) {
        branchManager = [[pootEngine alloc] init];
        [branchManager setDelegate:self];
        [branchManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[_countryText infoArray][[_countryText selectedID]][[_countryText idLabel]] forKey:@"idCountry"];
        [params setObject:currentCurrency[@"ID"] forKey:@"idModePayCurrency"];
        [params setObject:payTypeArray[selectedID][@"paymentModeId"] forKey:@"idPaymentMode"];
        [params setObject:@"0" forKey:@"amount"];
        [params setObject:@"C" forKey:@"onlyNetwork"];
        [params setObject:@"true" forKey:@"withCost"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [branchManager startRequestWithURL:ViamericasGetPaymentLocation withPost:post];
        
        [self lockViewWithMessage:nil];
    }
}

- (void)exchangeRateUpdate
{
    if ([[_branchText infoArray] count]>0) {
        exchangeManager = [[pootEngine alloc] init];
        [exchangeManager setDelegate:self];
        [exchangeManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:payTypeArray[selectedID][@"paymentModeId"] forKey:@"modePayment"];
        [params setObject:currentCurrency[@"ID"] forKey:@"modeCurrency"];
        [params setObject:[_branchText infoArray][[_branchText selectedID]][[_branchText idLabel]] forKey:@"idPayBranch"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [exchangeManager startRequestWithURL:ViamericasGetExchangeRate withPost:post];
        
        [self lockViewWithMessage:nil];
    }
}

#pragma Mark UITextField delegate

- (BOOL)textField:(UITextField_Validations *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _amountText) {
        finalAmount = [[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue];

        [_receiveText setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount*[currentExchangeRate[@"exchangeRate"] floatValue]]]];
    }
    return YES;
}

- (void)dismissPickers
{
    if ([picker textField] == _countryText) {
        if ([_countryText infoArray]) {
            [payTypeArray removeAllObjects];
            [currentCurrency removeAllObjects];
            [_branchText setText:@""];
            
            stage = orderStageNew;
            [_typeCollection reloadData];
            [self.tableView reloadData];
            
            [self currencySearchWithIdCountry:[_countryText infoArray][[picker selectedRowInComponent:0]][[_countryText idLabel]]];
        }
    }
    
    if ([picker textField] == _branchText) {
        [self exchangeRateUpdate];
    }
    
    [self subDismissPickers];
}

- (void)dismissAllTextFields
{
    for (UITextField_Validations *element in [self getValidationTextFields]) {
        [element resignFirstResponder];
    }
}




#pragma mark setting up Collection view

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [payTypeArray count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width-30)/2, 66);
}

- (generalCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"Cell_amount";
    
    generalCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.tag = indexPath.row;
    cell.cellButton.tag = indexPath.row;
    [cell.cellText setText:payTypeArray[indexPath.row][@"paymentModeName"]];
    [cell.cellButton setEnabled:YES];
    [cell.cellButton addTarget:self action:@selector(cellSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    for (generalCollectionCell *cellInArray in cellArray) {
        if (cellInArray == cell) {
            return cell;
        }
    }
    
    [cell updateState];
    
    [cellArray addObject:cell];
    
    return cell;
}

- (void) cellSelected:(id)sender
{
    UIButton *cellButton = sender;
    
    selectedID = (int)cellButton.tag;
    
    for (generalCollectionCell *cell in cellArray) {
        if (cell.tag == cellButton.tag) {
            [cell selectCell];
        } else {
            [cell deSelectCell];
        }
    }
    
    [_branchText setText:@""];
    [_branchText setSelectedID:0];
    [_branchText setInfoArray:nil];
    
    if (!firstLoad) {
        [self branchSearch];
    }
}

#pragma Mark UITable

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return 90;
            break;
        case 1:
            return 125;
            break;
        case 2: //Type
            return (stage & orderStagePayMethod)?55:0;
            break;
       // case 3: //Type
            
           // return (stage & orderStagePayMethod)?66:0;
           // break;
            
        case 3:
        {
            if (payTypeArray.count > 1){
                if ((payTypeArray.count % 2 ) == 0){
                return ((payTypeArray.count /2) * 73);
                }else{
                   return (((payTypeArray.count +1) /2) * 73);
                }
                
                
            }else{
                return 66;
            }
            
            
        }
          
            break;
            
        case 4: //Destination
            return (stage & orderStageDestination)?25:0;
            break;
        case 5: //Destination
            return (stage & orderStageDestination)?75:0;
            break;
        case 6: //Currency
            return (stage & orderStageCurrency)?25:0;
            break;
        case 7: //Currency
            return (stage & orderStageCurrency)?205:0;
            break;
        case 8: //Button
            return 150;
            break;
            
        default:
            return 65;
            break;
    }
}


- (void)resetView:(orderStage)type
{
    switch (type) {
        case orderStageNew: //NEW
            stage = orderStageNew;
            selectedID = 0;
            [payTypeArray removeAllObjects];
            [_branchText setText:@""];
            [_branchText setInfoArray:nil];
            [_amountText setText:@""];
            [_receiveText setText:@""];
            [_receiveText setPlaceholder:@""];
            [_exchangeRateLabel setText:@"- = -"];
            
            [_destinationView setHidden:YES];
            [_currencyView setHidden:YES];
            
            [_typeCollection reloadData];
            [self.tableView reloadData];
            break;
            
        case orderStagePayMethod: // when payment ends
            stage = orderStageNew;
            stage |= orderStagePayMethod;
            selectedID = 0;
            [_branchText setText:@""];
            [_branchText setInfoArray:nil];
            [_amountText setText:@""];
            [_receiveText setText:@""];
            [_receiveText setPlaceholder:@""];
            [_exchangeRateLabel setText:@"- = -"];
            
            [_destinationView setHidden:YES];
            [_currencyView setHidden:YES];
            
            [_typeCollection reloadData];
            [self.tableView reloadData];
            break;
        
        case orderStageDestination: // when destination ends
            stage = orderStageNew;
            stage |= orderStagePayMethod;
            stage |= orderStageDestination;
            [_amountText setText:@""];
            [_receiveText setText:@""];
            [_receiveText setPlaceholder:@""];
            [_exchangeRateLabel setText:@"- = -"];
            
            [_destinationView setHidden:NO];
            [_currencyView setHidden:YES];
            
            [_typeCollection reloadData];
            [self.tableView reloadData];
            break;
            
        case orderStageCurrency:
            stage = orderStageNew;
            stage |= orderStagePayMethod;
            stage |= orderStageDestination;
            stage |= orderStageCurrency;
            [_receiveText setText:@""];
            [_receiveText setPlaceholder:@""];
            
            [_destinationView setHidden:NO];
            [_currencyView setHidden:NO];
            
            [_typeCollection reloadData];
            [self.tableView reloadData];
            break;
            
        default:
            break;
    }
}


#pragma mark pootEngine

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        firstLoad = NO;
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (manager == countryManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            [_countryText setInfoArray:response[@"countrys"]];
            [_countryText setIdLabel:@"idCountry"];
            [_countryText setDescriptionLabel:@"nameCountry"];
            
            int idMex;
            
            for (int i = 0; i<[_countryText.infoArray count]; i++) {
                if ([_countryText.infoArray[i][@"idCountry"] isEqualToString:@"MEX"]) {
                    idMex = i;
                    [_countryText setText:[_countryText infoArray][idMex][_countryText.descriptionLabel]];
                    [_countryText setSelectedID:idMex];
                    [self currencySearchWithIdCountry:[_countryText infoArray][idMex][[_countryText idLabel]]];
                  
                    break;
                }
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    
    if (manager == currencyManager) {
        [self resetView:orderStageNew];
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue] == 0) {
            if ([response[@"currency"] count]>0) {
                for (NSDictionary *element in response[@"currency"]) {
                    if ([response[@"currency"] count]==1) {
                        [currentCurrency setDictionary:element];
                    } else {
                        if (!([element[@"NAME"] isEqualToString:@"USD"])) {
                            [currentCurrency setDictionary:element];
                        }
                    }
                }
                
                [self paymentTypeSearchWithIdCountry:[_countryText infoArray][[_countryText selectedID]][[_countryText idLabel]] AndIdCurrency:currentCurrency[@"ID"]];
            } else {
                [_typeCollection reloadData];
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == payTypeManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue] == 0) {
            [self resetView:orderStagePayMethod];
            if ([response[@"paymentModes"] count]>0) {
                payTypeArray = response[@"paymentModes"];
                
                [_typeCollection reloadData];
                
                
                firstLoad = YES;
                selectedID = 0;
                [self branchSearch];
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    
    if (manager == branchManager) {
        if (firstLoad) {
            UIButton *button = [[UIButton alloc] init];
            [button setTag:0];
            
            [self cellSelected:button];
            firstLoad = NO;
        }
        
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue] == 0) {
            [self resetView:orderStageDestination];
            
            [_branchText setInfoArray:response[@"locations"]];
            [_branchText setIdLabel:@"idbranchpaymentlocation"];
            [_branchText setDescriptionLabel:@"nameGroup"];
            
            if ([[_branchText infoArray] count]>0) {
                [_branchText setSelectedID:0];
                [_branchText setText:[_branchText infoArray][[_branchText selectedID]][[_branchText descriptionLabel]]];
                
                [self exchangeRateUpdate];
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == exchangeManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue] == 0) {
            [self resetView:orderStageCurrency];
            [currentExchangeRate setDictionary:response[@"exchangeRateRes"][0]];
            
            [_receiveText setPlaceholder:[NSString stringWithFormat:NSLocalizedString(@"Recibir (%@)", nil), currentCurrency[@"NAME"]]];
            [_exchangeRateLabel setText:[NSString stringWithFormat:@"%@ USD = %@ %@", [numFormatter stringFromNumber:[NSNumber numberWithInt:1]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[currentExchangeRate[@"exchangeRate"] floatValue]]], currentCurrency[@"NAME"]]];
        }
    }
}

@end
