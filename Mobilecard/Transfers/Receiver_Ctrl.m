//
//  Receiver_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 4/24/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "Receiver_Ctrl.h"

@interface Receiver_Ctrl ()
{
    
    NSMutableArray *countryArray;
    NSMutableArray *stateArray;
    NSMutableArray *cityArray;
    
    pootEngine *countryManager;
    pootEngine *stateManager;
    pootEngine *cityManager;
    pootEngine *recipientManager;
    
    NSDictionary *newRecipient;
}
@end

@implementation Receiver_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializePickersForView:self.navigationController.view];
    
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_middlenameText setUpTextFieldAs:textFieldTypeName];
    [_middlenameText setRequired:NO];
    
    [_lastNameText setUpTextFieldAs:textFieldTypeLastName];
    [_motherlastText setUpTextFieldAs:textFieldTypeLastName];
    [_motherlastText setRequired:NO];
    
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_emailText setRequired:NO];
    
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    //[_addressAdditionalText setUpTextFieldAs:textFieldTypeAddress];
    //[_addressAdditionalText setRequired:NO];
    //[_addressAdditionalText setPlaceholder:@""];
    
    [_countryText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_cityText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [_birthdateText setUpTextFieldAs:textFieldTypeBirthdate];
    [_birthdateText setRequired:NO];
    [_zipText setUpTextFieldAs:textFieldTypeGeneralNumericNoRequired];
    [_zipText setRequired:NO];
    
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_middlenameText];
    [self addValidationTextField:_lastNameText];
    [self addValidationTextField:_motherlastText];
    [self addValidationTextField:_emailText];
    [self addValidationTextField:_phoneText];
    [self addValidationTextField:_addressText];
    //[self addValidationTextField:_addressAdditionalText];
    [self addValidationTextField:_countryText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_birthdateText];
    [self addValidationTextField:_phoneText];
    [self addValidationTextField:_zipText];
    
    [self addValidationTextFieldsToDelegate];
    
    countryManager = [[pootEngine alloc] init];
    [countryManager setDelegate:self];
    [countryManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:@"NO" forKey:@"operation"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
  
    
    [countryManager startRequestWithURL:ViamericasGetCountries withPost:post];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addReceiver_Action:(id)sender {
    
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *birthdate = [dateFormatter dateFromString:[_birthdateText text]];
        [dateFormatter setDateFormat:@"yyyy/MM/dd"];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setObject:[[_lastNameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                    locale:[NSLocale systemLocale]] forKey:@"LName"];
        [params setObject:[[_middlenameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                      locale:[NSLocale systemLocale]] forKey:@"MName"];
        [params setObject:[[_motherlastText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                      locale:[NSLocale systemLocale]] forKey:@"SLName"];
        [params setObject:[_zipText text] forKey:@"ZIP"];
        [params setObject:[[_addressText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                   locale:[NSLocale systemLocale]] forKey:@"address"];
        //[params setObject:[_addressAdditionalText text] forKey:@"address2"];
        [params setObject:@"" forKey:@"address2"];
        [params setObject:[[_birthdateText text] length]==0?@"":[dateFormatter stringFromDate:birthdate] forKey:@"birthDate"];
        [params setObject:[_emailText text] forKey:@"email"];
        [params setObject:[[_nameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                locale:[NSLocale systemLocale]] forKey:@"firstName"];
        [params setObject:[_cityText infoArray][[_cityText selectedID]][[_cityText idLabel]] forKey:@"idCity"];
        [params setObject:[_countryText infoArray][[_countryText selectedID]][[_countryText idLabel]] forKey:@"idCountry"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
        [params setObject:[_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]] forKey:@"idState"];
        [params setObject:[[_lastNameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                    locale:[NSLocale systemLocale]] forKey:@"lastName"];
        [params setObject:[_phoneText text] forKey:@"phone1"];
        
        [self dismissViewControllerAnimated:YES completion:^{
            if ([_delegate conformsToProtocol:@protocol(receiverDelegate)]&&[_delegate respondsToSelector:@selector(receiverResponse:)]) {
                [_delegate receiverResponse:[NSDictionary dictionaryWithDictionary:params]];
            }
        }];
    }
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Tsf2",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

#pragma Mark PickerView

-(NSString *)pickerView:(UIPickerView_Automated *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self subPickerView:pickerView titleForRow:row forComponent:component];
}

- (void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self subPickerView:pickerView didSelectRow:row inComponent:component];
    
    if (pickerView.textField == _countryText) {
        stateManager = [[pootEngine alloc] init];
        [stateManager setDelegate:self];
        [stateManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[_countryText infoArray][[_countryText selectedID]][[_countryText idLabel]] forKey:[_countryText idLabel]];
         [params setObject:@"NO" forKey:@"operation"];
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [stateManager startRequestWithURL:ViamericasGetStates withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }
    
    if (pickerView.textField == _stateText) {
        cityManager = [[pootEngine alloc] init];
        [cityManager setDelegate:self];
        [cityManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[_countryText infoArray][[_countryText selectedID]][[_countryText idLabel]] forKey:[_countryText idLabel]];
        [params setObject:[_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]] forKey:[_stateText idLabel]];
        [params setObject:@"NO" forKey:@"operation"];
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [cityManager startRequestWithURL:ViamericasGetCities withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }
}


#pragma Mark pootEngine Manager

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (manager == recipientManager ) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            newRecipient = response;
            UIButton *button = [[UIButton alloc] init];
            button.tag = 1;
            [self performSegueWithIdentifier:@"fees" sender:button];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == countryManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        [self cleanCountry];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            [_stateText setText:@""];
            [_cityText setText:@""];
            [_countryText setInfoArray:response[@"countrys"]];
            [_countryText setIdLabel:@"idCountry"];
            [_countryText setDescriptionLabel:@"nameCountry"];
            
            UIPickerView_Automated *pick = [[UIPickerView_Automated alloc] init];
            [pick setTextField:_countryText];
            for (int i = 0; i<[[_countryText infoArray] count]; i++ ) {
                if ([[_countryText infoArray][i][[_countryText idLabel]] isEqualToString:_userData[@"idCountry"]]) {
                    [_countryText setSelectedID:i];
                }
            }
            [self pickerView:pick didSelectRow:[_countryText selectedID] inComponent:0];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == stateManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        [self cleanState];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            [_stateText setInfoArray:response[@"countryStates"]];
            [_stateText setIdLabel:@"idState"];
            [_stateText setDescriptionLabel:@"nameState"];
            
            UIPickerView_Automated *pick = [[UIPickerView_Automated alloc] init];
            [pick setTextField:_stateText];
            if ([[_stateText infoArray]count]>0) {
                [self pickerView:pick didSelectRow:0 inComponent:0];
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == cityManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        [self cleanCity];
        
        if ([[response objectForKey:kIDError] intValue]==0) {
            [_cityText setInfoArray:response[@"city"]];
            [_cityText setIdLabel:@"idCity"];
            [_cityText setDescriptionLabel:@"nameCity"];
            
            UIPickerView_Automated *pick = [[UIPickerView_Automated alloc] init];
            [pick setTextField:_cityText];
            if ([[_cityText infoArray]count]>0) {
                [self pickerView:pick didSelectRow:0 inComponent:0];
            }
            
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}

- (void) cleanCountry
{
    [_countryText setInfoArray:[[NSArray alloc] init]];
    [_stateText setInfoArray:[[NSArray alloc] init]];
    [_cityText setInfoArray:[[NSArray alloc] init]];
    
    [_countryText setText:@""];
    [_stateText setText:@""];
    [_cityText setText:@""];
}

- (void) cleanState
{
    [_stateText setInfoArray:[[NSArray alloc] init]];
    [_cityText setInfoArray:[[NSArray alloc] init]];
    
    [_stateText setText:@""];
    [_cityText setText:@""];
}

- (void) cleanCity
{
    [_cityText setInfoArray:[[NSArray alloc] init]];
    
    [_cityText setText:@""];
}

@end
