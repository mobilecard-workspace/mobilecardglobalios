//
//  orderFees_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 5/20/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "orderFees_Ctrl.h"

@interface orderFees_Ctrl ()
{
    pootEngine *feeManager;
    pootEngine *cardManager;
    pootEngine *senderManager;
    pootEngine *orderManager;
    pootEngine *confirmationManager;
    
    NSNumberFormatter *numFormatter;
    
    NSString *orderReceiverId;
    
    NSDictionary *currentFee;
    NSDictionary *selectedCardBackup;
    NSDictionary *confirmationResult;

    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *placeName;
}

@end

@implementation orderFees_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
 //   [self addShadowToView:_continue_Button];
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    currentFee = [[NSDictionary alloc] init];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [_accountName setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_accountNumber setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    
    [_headerView setHidden:YES];
    [_dataView setHidden:YES];
    
    if ([self validateDeposit]) {
        [self addValidationTextField:_accountNumber];
        [self addValidationTextField:_accountName];
        
        [_headerView setHidden:NO];
        [_dataView setHidden:NO];
        
        [_amount setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[_userData[@"netAmount"] floatValue]]]];
        
        [_accountName setText:[NSString stringWithFormat:@"%@ %@", _recipientInfo[@"FName"], _recipientInfo[@"LName"]]];
        
        [self addValidationTextFieldsToDelegate];
    }
    
    
    feeManager = [[pootEngine alloc] init];
    [feeManager setDelegate:self];
    [feeManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setDictionary:_userData];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [feeManager startRequestWithURL:ViamericasGetOrderFees withPost:post];
    
    [self lockViewWithMessage:nil];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error){
        
        if (error == nil && [placemarks count]>0) {
            placemark = [placemarks lastObject];
            
            placeName = [NSString stringWithFormat:@"%@ %@",
                         placemark.thoroughfare,
                         placemark.subThoroughfare];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)validateDeposit
{
    if ([_userData[@"idModePay"] isEqualToString:@"C"]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)dismissAllTextFields
{
    for (UITextField_Validations *element in [self getValidationTextFields]) {
        [element resignFirstResponder];
    }
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    if ([self validateDeposit]) {
        if (!([self validateFieldsInArray:[self getValidationTextFields]])) {
            return;
        }
    }
    
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
        //NSLog(@"%@", exception.description);
        
       // UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        //[alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)proceedPayment
{
    orderManager = [[pootEngine alloc] init];
    [orderManager setDelegate:self];
    [orderManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[self validateDeposit]?[_accountNumber text]:@"" forKey:@"accountReceiver"];
    [params setObject:_recipientInfo[@"address"] forKey:@"addressReceiver"];
    [params setObject:_userData[@"netAmount"] forKey:@"amount"];
    [params setObject:_userData[@"idBranchPay"] forKey:@"idBranchReciever"];
    [params setObject:_recipientInfo[@"idCity"] forKey:@"idCityRecivier"];
    [params setObject:_recipientInfo[@"idCountry"] forKey:@"idCountryReciever"];
    [params setObject:_recipientInfo[@"idRecipient"] forKey:@"idRecipient"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
    [params setObject:_recipientInfo[@"idState"] forKey:@"idStateReceiver"];
    [params setObject:_userData[@"modeCurrency"] forKey:@"modPayCurrencyReceiver"];
    [params setObject:_userData[@"idModePay"] forKey:@"modePayReciever"];
    [params setObject:_recipientInfo[@"FName"] forKey:@"recipientFirstName"];
    [params setObject:_recipientInfo[@"LName"] forKey:@"recipientLastName"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
    
    //New fields
    [params setObject:@"com.ironbit.mc.dev" forKey:@"appVersion"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"longitude"];
    [params setObject:@"iOS" forKey:@"phoneType"];
    [params setObject:placeName?placeName:@"" forKey:@"placeName"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [orderManager startRequestWithURL:ViamericasCreateNewOrder withPost:post];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

#pragma Mark Tableview

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return [self validateDeposit]?150:0;
            break;
        case 1:
            return [self validateDeposit]?250:0;
            break;
        case 2:
            return 240;
            break;
            
        default:
            return 65;
            break;
    }
}

#pragma Mark pootEngine

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (manager == feeManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
     
        if ([response[kIDError] intValue] == 0) {
            currentFee = response;
            
            [_amountLabel setText:[NSString stringWithFormat:@"%@ USD", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[response[@"totalFee"] floatValue]]]]];
        }
    }
    
    if (cardManager == manager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0 && [response[kUserCardArray] count]>0) {
            [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
            [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
            UINavigationController *nav = [nextStory instantiateInitialViewController];
            Wallet_Ctrl *view = nav.viewControllers.firstObject;
            view._whiteList = __whiteList;
            [view setType:walletViewTypeSelection];
            [view setDelegate:self];
            
            [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == orderManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue] == 0) {
            orderReceiverId = response[@"idReceiver"];
            
            confirmationManager = [[pootEngine alloc] init];
            [confirmationManager setDelegate:self];
            [confirmationManager setShowComments:developing];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:selectedCardBackup[@"idTarjeta"] forKey:@"idTarjeta"];
            [params setObject:[selectedCardBackup[@"nombre"] componentsSeparatedByString:@" "][0] forKey:@"cardFirstName"];
            [params setObject:[[selectedCardBackup[@"nombre"] componentsSeparatedByString:@" "] count]>1?[selectedCardBackup[@"nombre"] componentsSeparatedByString:@" "][1]:@"" forKey:@"cardLastName"];
            [params setObject:(NSString*)[cardManager decryptedStringOfString:selectedCardBackup[@"pan"] withSensitive:NO] forKey:@"cardNumber"];
            [params setObject:(NSString*)[cardManager decryptedStringOfString:selectedCardBackup[@"codigo"] withSensitive:NO] forKey:@"cvv"];
            [params setObject:[(NSString*)[cardManager decryptedStringOfString:selectedCardBackup[@"vigencia"] withSensitive:NO] stringByReplacingOccurrencesOfString:@"/" withString:@""] forKey:@"expDate"];
            [params setObject:[selectedCardBackup[@"tipoTarjeta"] isEqualToString:@"CREDITO"]?@"K":@"D" forKey:@"paymentType"];
            [params setObject:orderReceiverId forKey:@"idReciever"];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLogin] forKey:@"nickName"];
            [params setObject:response[@"idBitacora"] forKey:@"idBitacora"];
            
            
            NSString *post = nil;
            NSError *JSONError;
            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            post = [NSString stringWithFormat:@"json=%@", JSONString];
            
            [confirmationManager startRequestWithURL:ViamericasConfirmTransactionCard withPost:post];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == senderManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue] == 0) {
            confirmationManager = [[pootEngine alloc] init];
            [confirmationManager setDelegate:self];
            [confirmationManager setShowComments:developing];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:[selectedCardBackup[@"nombre"] componentsSeparatedByString:@" "][0] forKey:@"cardFirstName"];
            [params setObject:[[selectedCardBackup[@"nombre"] componentsSeparatedByString:@" "] count]>1?[selectedCardBackup[@"nombre"] componentsSeparatedByString:@" "][1]:@"" forKey:@"cardLastName"];
            [params setObject:(NSString*)[cardManager decryptedStringOfString:selectedCardBackup[@"pan"] withSensitive:NO] forKey:@"cardNumber"];
            [params setObject:(NSString*)[cardManager decryptedStringOfString:selectedCardBackup[@"codigo"] withSensitive:NO] forKey:@"cvv"];
            [params setObject:[(NSString*)[cardManager decryptedStringOfString:selectedCardBackup[@"vigencia"] withSensitive:NO] stringByReplacingOccurrencesOfString:@"/" withString:@""] forKey:@"expDate"];
            [params setObject:orderReceiverId forKey:@"idReciever"];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLogin] forKey:@"nickName"];
            [params setObject:[selectedCardBackup[@"tipoTarjeta"] isEqualToString:@"CREDITO"]?@"K":@"D" forKey:@"paymentType"];
            
            NSString *post = nil;
            NSError *JSONError;
            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            post = [NSString stringWithFormat:@"json=%@", JSONString];
            
            [confirmationManager startRequestWithURL:ViamericasConfirmTransactionCard withPost:post];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
            
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == confirmationManager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0) {
            confirmationResult = response;
            
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            orderResult_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"orderResult_Ctrl"];
            [nextView setType:paymentTypeViamericas];
            [nextView setConfirmationResult:confirmationResult];
            
            [self.navigationController pushViewController:nextView animated:YES];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"confirmation"]) {
        UINavigationController *nav = [segue destinationViewController];
        orderSummary *nextView = [nav childViewControllers].firstObject;
        [nextView setDelegate:self];
        [nextView setUserData:_userData];
        [nextView setRecipientInfo:_recipientInfo];
        [nextView setFeeInfo:currentFee];
        [nextView setCardInfo:selectedCardBackup];
    }
}


- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    selectedCardBackup = selectedCard;
    
    [self performSegueWithIdentifier:@"confirmation" sender:nil];
}

- (void)orderSummaryResult
{
    [self proceedPayment];
}

@end
