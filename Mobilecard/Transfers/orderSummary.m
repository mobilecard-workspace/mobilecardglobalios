//
//  orderSummary.m
//  MobileCard_X
//
//  Created by David Poot on 5/25/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "orderSummary.h"

@interface orderSummary ()
{
    NSNumberFormatter *numFormatter;
    pootEngine *decrypter;
}

@end

@implementation orderSummary

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
    NSLog(@"USERDATA -> %@", _userData);
    NSLog(@"RECIPIENT -> %@", _recipientInfo);
    NSLog(@"FEEINFO -> %@", _feeInfo);
    NSLog(@"USERDATA -> %@", _userData);
    NSLog(@"CARDINFO -> %@", _cardInfo);
     */
    
 //   [self addShadowToView:_cancel_Button];
 //   [self addShadowToView:_continue_Button];
    
    decrypter = [[pootEngine alloc] init];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    NSString *cardNumber = (NSString*)[decrypter decryptedStringOfString:_cardInfo[@"pan"] withSensitive:NO];
    
    [_recipientLabel setText:[NSString stringWithFormat:@"%@ %@", _recipientInfo[@"FName"], _recipientInfo[@"LName"]]];
    [_amountSendLabel setText:[NSString stringWithFormat:@"%@ USD", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_userData[@"netAmount"] floatValue]]]]];
    [_amountReceivedLabel setText:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_userData[@"netAmount"] floatValue]*[_userData[@"exchangeRate"] floatValue]]], _userData[@"NAME"]]];
    [_receiveNetworkLabel setText:_userData[@"nameGroup"]];
    [_paymentLabel setText:_userData[@"paymentModeName"]];
    
    [_cardLabel setText:[NSString stringWithFormat:@"XXXX XXXX XXXX %@", [cardNumber substringWithRange:NSMakeRange(12, [cardNumber length]==15?3:4)]]];
    [_addressCardLabel setText:_cardInfo[@"domAmex"]];
    
    [_rateLabel setText:[NSString stringWithFormat:@"%@ USD = %@ %@", [numFormatter stringFromNumber:[NSNumber numberWithInt:1]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_userData[@"exchangeRate"] floatValue]]], _userData[@"NAME"]]];
    [_feeLabel setText:[NSString stringWithFormat:@"%@ USD", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_feeInfo[@"totalFee"] floatValue]]]]];
    [_otherFeeLabel setText:[NSString stringWithFormat:@"%@ USD", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_feeInfo[@"stateTax"] floatValue]/[_userData[@"exchangeRate"] floatValue]]]]];
    [_totalLabel setText:[NSString stringWithFormat:@"%@ USD", [numFormatter stringFromNumber:[NSNumber numberWithFloat:([_feeInfo[@"totalFee"] floatValue])+[_userData[@"netAmount"] floatValue]]]]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)continue_Action:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([_delegate conformsToProtocol:@protocol(orderSummaryDelegate)]&&[_delegate respondsToSelector:@selector(orderSummaryResult)]) {
            [_delegate orderSummaryResult];
        }
    }];
}
- (IBAction)cancel_Action:(id)sender {
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}


@end
