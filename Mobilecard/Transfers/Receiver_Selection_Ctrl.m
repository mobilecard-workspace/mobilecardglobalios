//
//  Receiver_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 4/24/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "Receiver_Selection_Ctrl.h"

@interface Receiver_Selection_Ctrl ()
{
    pootEngine *countryManager;
    pootEngine *stateManager;
    pootEngine *cityManager;
    pootEngine *recipientManager;
    
    pootEngine *getRecipientManager;
    
    NSDictionary *newRecipient;
}
@end

@implementation Receiver_Selection_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
 //   [self addShadowToView:_add_Button];
 //   [self addShadowToView:_continue_Button];
    
    [self initializePickersForView: self.navigationController.view];
    
    [_regReceiversText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_regReceiversText setDelegate:self];
    
    [self addValidationTextField:_regReceiversText];
    
    
    getRecipientManager = [[pootEngine alloc] init];
    [getRecipientManager setDelegate:self];
    [getRecipientManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserSenderId] forKey:kUserSenderId];
   
    [params setObject:_userData[@"idCountry"] forKey:@"idCountry"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [getRecipientManager startRequestWithURL:ViamericasGetRecipients withPost:post];
    
    [self lockViewWithMessage:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)addReceiver_Action:(id)sender {
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"receiverReg" bundle:nil];
    UINavigationController *nav = nextStory.instantiateInitialViewController;
    Receiver_Ctrl *nextView = [nav childViewControllers].firstObject;
    
    [nextView setDelegate:self];
    
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    UIButton *button = [[UIButton alloc] init];
    button.tag = 2;
    [self performSegueWithIdentifier:@"fees" sender:button];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"fees"]) {
        UIButton *button = (UIButton*)sender;
        
        switch (button.tag) {
            case 1:
            {
                orderFees_Ctrl *nextView = segue.destinationViewController;
                nextView._whiteList = __whiteList;
                [nextView setRecipientInfo:newRecipient];
                [nextView setUserData:_userData];
            }
                break;
            case 2:
            {
                orderFees_Ctrl *nextView = segue.destinationViewController;
                nextView._whiteList = __whiteList;
                [nextView setRecipientInfo:[_regReceiversText infoArray][[_regReceiversText selectedID]]];
                [nextView setUserData:_userData];
            }
                break;
            default:
                break;
        }
    }
}

#pragma Mark receiverDelegate

- (void)receiverResponse:(NSDictionary *)receiverData
{
    if (receiverData) {
        recipientManager = [[pootEngine alloc] init];
        [recipientManager setDelegate:self];
        [recipientManager setShowComments:developing];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:receiverData options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [recipientManager startRequestWithURL:ViamericasCreateRecipient withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }
}

#pragma Mark pootEngine Manager

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (manager == recipientManager ) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            newRecipient = response;
            UIButton *button = [[UIButton alloc] init];
            button.tag = 1;
            [self performSegueWithIdentifier:@"fees" sender:button];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == getRecipientManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            [_regReceiversText setInfoArray:response[@"recipients"]];
            [_regReceiversText setIdLabel:@"idRecipient"];
            [_regReceiversText setDescriptionLabel:@"FName"];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}

@end
