//
//  sender_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 4/27/17.
//  Copyright © 2017 David Poot. All rights reserved.
//
#import "sender_Ctrl.h"

@interface sender_Ctrl ()
{
    NSMutableArray *countryArray;
    NSMutableArray *stateArray;
    NSMutableArray *cityArray;
    
    pootEngine *stateManager;
    pootEngine *cityManager;
    pootEngine *zipManager;
    
    pootEngine *createManager;
}
@end

@implementation sender_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //   [self addShadowToView:self.navigationController.navigationBar];
    //   [self addShadowToView:_continue_Button];
    
    [self initializePickersForView:self.navigationController.view];
    
    
    [_terms_button.titleLabel setNumberOfLines:2];
    [_privacy_Button.titleLabel setNumberOfLines:2];
    
    [_emailtext setUpTextFieldAs:textFieldTypeEmail];
    [_nameText  setUpTextFieldAs:textFieldTypeName];
    [_middlenameText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
    [_lastnameText setUpTextFieldAs:textFieldTypeLastName];
    [_motherlastText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
    [_mobileText setUpTextFieldAs:textFieldTypeCellphone];
    [_birthdateText setUpTextFieldAs:textFieldTypeBirthdate];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_addressAdditionalText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_cityText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_zipText setUpTextFieldAs:textFieldTypeRequiredCombo];
    
    [_whereToText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
    
    [self addValidationTextField:_emailtext];
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_lastnameText];
    [self addValidationTextField:_mobileText];
    [self addValidationTextField:_birthdateText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_zipText];
    
    [self addValidationTextFieldsToDelegate];
    
    [_emailtext setText:[[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserEmail] uppercaseString]];
    [_nameText setText:[[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName] uppercaseString]];
    [_lastnameText setText:[[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLastName] uppercaseString]];
    [_mobileText setText:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserPhone]];
    
    stateManager = [[pootEngine alloc] init];
    [stateManager setDelegate:self];
    [stateManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@"USA" forKey:@"idCountry"];
    [params setObject:@"RS" forKey:@"operation"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [stateManager startRequestWithURL:ViamericasGetStates withPost:post];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continue_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        if (!_terms_switch.on || !_privacy_switch.on) {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Error políticas", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
            return;
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *birthdate = [dateFormatter dateFromString:[_birthdateText text]];
        [dateFormatter setDateFormat:@"yyyy/MM/dd"];
        
        
        createManager = [[pootEngine alloc] init];
        [createManager setDelegate:self];
        [createManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLogin] forKey:@"nickName"];
        [params setObject:[[_lastnameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                    locale:[NSLocale systemLocale]] forKey:@"last_name"];
        [params setObject:[[_middlenameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                      locale:[NSLocale systemLocale]] forKey:@"middleName"];
        [params setObject:[[_motherlastText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                      locale:[NSLocale systemLocale]] forKey:@"second_name"];
        [params setObject:_zipText.text forKey:@"zipCode"];
        [params setObject:[[_addressText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                   locale:[NSLocale systemLocale]] forKey:@"address1"];
        [params setObject:[[_addressAdditionalText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                             locale:[NSLocale systemLocale]] forKey:@"address2"];
        [params setObject:[dateFormatter stringFromDate:birthdate] forKey:@"birthDate"];
        [params setObject:[_emailtext text] forKey:@"email"];
        [params setObject:[[_nameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                locale:[NSLocale systemLocale]] forKey:@"firstName"];
        [params setObject:itemCitySelected.idCity forKey:@"idCity"];
        [params setObject:@"USA" forKey:@"idCountry"];
        [params setObject:itemStateSelected.idState forKey:@"idState"];
        [params setObject:[[_lastnameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                    locale:[NSLocale systemLocale]] forKey:@"lastName"];
        [params setObject:[_mobileText text] forKey:@"phone1"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [createManager startRequestWithURL:ViamericasCreateSender withPost:post];
        
        [self lockViewWithMessage:nil];
    }
}

- (IBAction)showPrivacy_Action:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://www.mobilecard.mx/AddcelContent/files/docs/viamericas_privacy.pdf"];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)showTerms_Action:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://www.mobilecard.mx/AddcelContent/files/docs/viamericas_terms.pdf"];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)cancel_Action:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self->_delegate conformsToProtocol:@protocol(senderDelegate)]&&[self->_delegate respondsToSelector:@selector(senderCreationResult:)]) {
            [self->_delegate senderCreationResult:nil];
        }
    }];
}
-(NSArray *)MutableArrayToArray:(NSMutableArray *)list{
    NSMutableArray *objetArray = [NSMutableArray new];
    
    for (NSDictionary * ItemList in list) {
        
        NSString * Itemstring = [ItemList objectForKey:@"nameState"];
        [objetArray addObject:Itemstring];
    }
    return objetArray;
}
#pragma Mark PickerView
- (void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self subPickerView:pickerView didSelectRow:row inComponent:component];
    
    if (pickerView.textField == _stateText) {
        
    }
    
    if (pickerView.textField == _cityText) {
        
    }
}

#pragma Mark pootEngine Manager
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    if (json == nil) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (manager == stateManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {

            NSArray * ArrayStates =[response objectForKey:@"countryStates"];
            objCountrysState = [[NSMutableArray alloc] init];
            NSMutableArray *listStateString = [[NSMutableArray alloc] init];
            for (NSDictionary* keyState in ArrayStates) {
                stateModel *itemState = [[stateModel alloc] init];
                itemState.idState =[keyState objectForKey:@"idState"];
                itemState.nameState =[keyState objectForKey:@"nameState"];
                
                [objCountrysState addObject:itemState];
                [listStateString addObject:itemState.nameState];
            }
            self.downPickerState = [[DownPicker alloc] initWithTextField:self.stateText withData:listStateString];
            [self.downPickerState setPlaceholder:@"Estado"];
            [self.downPickerState setPlaceholderWhileSelecting:@"Estado"];
            [self.downPickerState addTarget:self action:@selector(state_Selected:) forControlEvents:UIControlEventValueChanged];
            self.downPickerCity = [[DownPicker alloc] initWithTextField:self.cityText withData:@[@""]];
            [self.downPickerCity setPlaceholder:@"Ciudad"];
            [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
            
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == cityManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue]==0) {
            NSArray * ArrayCity =[response objectForKey:@"city"];
            objCountrysCity = [[NSMutableArray alloc] init];
            NSMutableArray *listCityString = [[NSMutableArray alloc] init];
            for (NSDictionary* keyCity in ArrayCity) {
                cityModel *itemCity = [[cityModel alloc] init];
                itemCity.idCity =[keyCity objectForKey:@"idCity"];
                itemCity.nameCity =[keyCity objectForKey:@"nameCity"];
                
                [objCountrysCity addObject:itemCity];
                [listCityString addObject:itemCity.nameCity];
            }
            
            
            self.downPickerCity = [[DownPicker alloc] initWithTextField:self.cityText withData:listCityString];
            [self.downPickerCity setPlaceholder:@"Ciudad"];
            [self.downPickerCity setPlaceholderWhileSelecting:@"Ciudad"];
            [self.downPickerCity addTarget:self action:@selector(city_Selected:) forControlEvents:UIControlEventValueChanged];
            
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == zipManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue]==0) {
            [_zipText setInfoArray:response[@"stateCities"]];
            [_zipText setIdLabel:@"zipCode"];
            [_zipText setDescriptionLabel:@"zipCode"];
            
            UIPickerView_Automated *pick = [[UIPickerView_Automated alloc] init];
            [pick setTextField:_zipText];
            [self pickerView:pick didSelectRow:0 inComponent:0];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == createManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue]==0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:^{
                    if ([self->_delegate conformsToProtocol:@protocol(senderDelegate)]&&[self->_delegate respondsToSelector:@selector(senderCreationResult:)]) {
                        [self->_delegate senderCreationResult:response[kUserSenderId]];
                    }
                }];
            });
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}
-(void)state_Selected:(id)dp {
    itemStateSelected = [objCountrysState objectAtIndex:(int)self.downPickerState.selectedIndex];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->cityManager = [[pootEngine alloc] init];
        [self->cityManager setDelegate:self];
        [self->cityManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:@"USA" forKey:@"idCountry"];
        [params setObject:self->itemStateSelected.idState forKey:@"idState"];
        [params setObject:@"RS" forKey:@"operation"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->cityManager startRequestWithURL:ViamericasGetCities withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}
-(void)city_Selected:(id)dp {
    
    itemCitySelected = [objCountrysCity objectAtIndex:(int)self.downPickerCity.selectedIndex];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self->zipManager = [[pootEngine alloc] init];
        [self->zipManager setDelegate:self];
        [self->zipManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:self->itemCitySelected.idCity forKey:@"idCity"];
        //[params setObject:@"RS" forKey:@"operation"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [self->zipManager startRequestWithURL:ViamericasGetZips withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    });
}
@end
