//
//  favorites_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 1/6/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Register_Step30.h"

@interface favorites_Ctrl : mT_commonTableViewController{
    UIButton * buttonEdit;
    NSMutableDictionary *IndexDeleteItem;
    
    UIButton * buttonDelete;
    UIButton * buttonSelection;
    
    
    UIButton *_homeButton;
    UIButton *_myMCButton;
    UIButton *_favoritesButton;
    UIButton *_walletButton;
    
    BOOL ValidationShowIconFavorites;
}

@end
