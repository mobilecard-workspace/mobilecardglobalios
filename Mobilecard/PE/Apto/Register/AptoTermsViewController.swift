


import UIKit
import AVFoundation





class AptoTermsViewController:UIViewController{

    @IBOutlet weak var iAgreeButton: UIButton!
    @IBOutlet weak var iDisagreButton: UIButton!
    
    
    var email = ""
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24.0),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    var attributedString = NSMutableAttributedString(string:"")
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
      
        print("estos son los verificationsid")
        print("de sms \(DataUser.VERIFICATIONID_SMS)")
        print("de correo \(DataUser.VERIFICATIONID_MAIL)")
        
        
        self.attrs[NSAttributedString.Key.font] = self.iAgreeButton.titleLabel?.font
        self.underlineButton(button: self.iAgreeButton)
        self.underlineButton(button: self.iDisagreButton)
        
        
        

        
        
       

        
        
    }

    
    
    //metodo que le pone una underline a un botón
    func underlineButton(button:UIButton){
        
        self.attributedString =  NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:button.titleLabel!.text!, attributes:attrs)
        attributedString.append(buttonTitleStr)
        
        button.setAttributedTitle(attributedString, for: .normal)
        
    }


    
    @IBAction func closeAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "termsAcceptSegue"{
            
            
            // nos llevamos el numero a la siguiente clase
            let destinoViewController = segue.destination as! AptoPersonalInfoOneViewController
            
            destinoViewController.email = self.email
            
            
        }
  
        
    }
    
    
    
    //metodo que se ejecuta al presionar acepto
    @IBAction func iAgreeAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "termsAcceptSegue", sender: nil)
    }
    
  
   
}









