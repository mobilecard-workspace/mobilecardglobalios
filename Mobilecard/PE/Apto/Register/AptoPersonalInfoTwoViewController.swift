


import UIKit
import AVFoundation





class AptoPersonalInfoTwoViewController:UIViewController{

    
    //variables que recibimos de la clase pasada
    var firstName = ""
    var lastName = ""
    var email = ""

     @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var documentNumberTextField: UITextField!
    
    //para la fecha
    var birthDatePicker: UIDatePicker!
    
    //variable que guarda la fecha seleccionada
    var currentBirthDate:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      


        
        // Date picker
        let toolDateBar = UIToolbar()
        toolDateBar.sizeToFit()
        let doneDateBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(setDate(_:)))
        toolDateBar.setItems([doneDateBtn], animated: false)
        
        self.birthDatePicker = UIDatePicker()
        let loc = Locale(identifier: "es")
        self.birthDatePicker.locale = loc
        self.birthDatePicker.datePickerMode = .date
        self.ageTextField.inputAccessoryView = toolDateBar
        self.ageTextField.inputView = self.birthDatePicker
        
        
       

        
        
    }

    
    //metodo que le da el formato a la fecha
    
    
    @objc func setDate(_ sender: UIButton){
        let newDateFormat = DateFormatter()
        newDateFormat.dateStyle = .medium
        newDateFormat.timeStyle = .none
        newDateFormat.dateFormat = "dd/MM/yyyy"
        
        self.ageTextField.text = newDateFormat.string(from: birthDatePicker.date)
        
        let newDate = newDateFormat.date(from: ageTextField.text!)
        newDateFormat.dateFormat = "yyyy-MM-dd"
        currentBirthDate = newDateFormat.string(from: newDate!)
        print("selecciono esta \(self.currentBirthDate)")
        
        self.view.endEditing(true)
        
    }


    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
  
        if segue.identifier == "PersonalInfoSecoundSegue"{
            
            
            
            // nos llevamos los datos a la siguiente clase
            let destinoViewController = segue.destination as! AptoPersonalInfoThreeViewController
            
            destinoViewController.firstName = self.firstName
            destinoViewController.lastName = self.lastName
            destinoViewController.email = self.email
            destinoViewController.fechaNacimiento = self.currentBirthDate
            destinoViewController.numeroDocumento = self.documentNumberTextField.text!
            
            
            
        }
        
    }
    
    
    
    //metodo que se ejecuta al presionar el boton de continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        if self.currentBirthDate == ""{
               alert(title: "¡Aviso!", message: "Agrega una fecha de nacimiento para continuar.", cancel: "OK");
            
            return;
        }
        
        
        if (self.documentNumberTextField.text?.count)! < 9 || (self.documentNumberTextField.text?.count)! > 9{
            alert(title: "¡Aviso!", message: "Agregue un número de documento válido para continuar.", cancel: "OK");
            return;
        }
        
      //  if self.isValidSsn(self.documentNumberTextField.text!) == false{
            
       //     alert(title: "¡Notice!", message: "Add a valid document number to continue.", cancel: "OK");
      //      return;
      //  }
        
        self.performSegue(withIdentifier: "PersonalInfoSecoundSegue", sender: nil)
        
    }
    
    
    //metodo que verifica que es un ssn valido
    func isValidSsn(_ ssn: String) -> Bool {
        let ssnRegext = "^(?!(000|666|9))\\d{3}(?!00)\\d{2}(?!0000)\\d{4}$"
        return ssn.range(of: ssnRegext, options: .regularExpression, range: nil, locale: nil) != nil
    }
  
   
}









