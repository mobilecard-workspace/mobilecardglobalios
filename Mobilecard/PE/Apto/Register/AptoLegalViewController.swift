


import UIKit
import AVFoundation
import Alamofire




class AptoLegalViewController:UIViewController,UITextViewDelegate,ServicesDelegate{

    @IBOutlet weak var legalTextView: UITextView!
    

    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var accountID = ""
    var pan = ""
    var cvv = ""
    var vigencia = ""
    
    let services = Services()
   
    let urlDiscoser = URL(string: "https://www.mobilecard.mx/docs/Disclosure.pdf")
     let urlCardholder = URL(string: "https://www.mobilecard.mx/docs/CardholderAgreement.pdf")
     let privacyPolicy = URL(string: "https://www.mobilecard.mx/docs/MCBPrivacyPolicyNotice.pdf")
     let mobilecardSignAgreement = URL(string: "https://www.mobilecard.mx/docs/MobileCardSignAgreement.pdf")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.isHidden = true
        
        self.services.delegate = self
      self.legalTextView.delegate = self

        
        let attributedString = NSMutableAttributedString(string: "By clicking 'Request Card' you accept the MobileCard Disclosures and Cardholder Agreement. You further consent to Metropolitan Commercial Bank´s Privacy Policy.You also consent to receive communications electronically via text message ('SMS') and consent to the E-Sign Agreement.")
        let urlDisclouse = URL(string: "https://www.google.com.mx")!
        let urlECardHolder = URL(string: "https://www.google.com.mx")!
        let urlPrivacyPolicy = URL(string: "https://www.google.com.mx")!
          let urlESign = URL(string: "https://www.google.com.mx")!
        attributedString.setAttributes([.link: urlDisclouse], range: NSMakeRange(53, 11 ))
       attributedString.setAttributes([.link: urlECardHolder], range: NSMakeRange(69, 21 ))
            attributedString.setAttributes([.link: urlPrivacyPolicy], range: NSMakeRange(145, 15 ))
        attributedString.setAttributes([.link: urlESign], range: NSMakeRange(262, 17 ))
        
        
        
        let fullRange = NSMakeRange(0, attributedString.length)
        
        attributedString.addAttribute(NSAttributedString.Key.font, value: self.legalTextView.font!, range: fullRange)
        
        
        self.legalTextView.attributedText = attributedString
        self.legalTextView.isUserInteractionEnabled = true
        self.legalTextView.isEditable = false
        
       
        self.legalTextView.linkTextAttributes = [
            .foregroundColor: #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
       

        
        
    }

    
    //metodo delegado de textview para poder tomar una accion cuando presionen una url en una ubicacion de caracteres especifica
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        
        switch characterRange{
        case NSMakeRange(53, 11 ) :
            
            
           
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(self.urlDiscoser!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(self.urlDiscoser!)
            }
            print("se presiono disclouseres")
            break
            
        case NSMakeRange(69, 21 ):
            
            
           
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(self.urlCardholder!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(self.urlCardholder!)
            }
            print("se presiono Cardholder agreement")
            break
            
        case NSMakeRange(145, 15 ):
            
           
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(self.privacyPolicy!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(self.privacyPolicy!)
            }
            print("se presiono Privacy Policy")
            break
            
        case NSMakeRange(262, 17 ):
            
         
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(self.mobilecardSignAgreement!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(self.mobilecardSignAgreement!)
            }
            print("se presiono E-Sign Agreement")
            break
            
            
        default:
            break
            
        }
        
      
       // navigationController?.pushViewController(webViewController, animated: true)
        
        return false // Changed line.
    }

    
    @IBAction func closeAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        if segue.identifier == "requestCompleteCardSegue"{
          
          //  let destinoViewController = segue.destination as! AptoThxViewController
       
            
       //destinoViewController.accountID = self.accountID
         //   destinoViewController.pan = self.pan
           // destinoViewController.cvv = self.cvv
            //destinoViewController.vigencia = self.vigencia
            
            
        }
  
        
    }
    
    
    
    //metodo que se ejecuta al prersionar el botón de request card crea el usuario en Apto
    @IBAction func requestAction(_ sender: UIButton) {
    
    
        
        
       
        
        
        //llamamos el servicio  de crear tarjeta
        
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
         let params : [String : Any] = ["issuer":"shift"]
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Cargando", animated: true)
        
        
        Alamofire.request("\(ServicesLinks.aptoVaul)/v1/user/accounts/issuecard", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
                    print("este es el codigo\(response.response?.statusCode)")
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                        self.accountID = dic!["account_id"] as! String
                     let kyc_status = dic!["kyc_status"] as! String
                        
                        if kyc_status == "passed"{
                       
                        
             //llamamos el servicio de activar Tarjeta
                        self.updateAccountIdServices()
                        }else{
                            
                            let alertController = UIAlertController(title: "¡Aviso!", message: "Se le ha enviado un correo electrónico de verificación ya que algunos campos no pudieron ser validados.", preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: MainMenu_Ctrl.self) {
                                        LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                                
                            }
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                        
                        
                    }else{
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                        if code == 3031{
                            
                            let alertController = UIAlertController(title: "¡Aviso!", message: "El token expiró.", preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: MainMenu_Ctrl.self) {
                                        LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                                
                                
                            }
                            
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                       
                        }
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
        }
        
        
       
        
        
        
    
    }
    
  
    //metodos de servicios internos de nosotros
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        if endpoint == "Wallet/1/3/en/saveAptoInfo"{
            
            
            let code = response["idError"] as! Int
            
            if code == 0{
                
                DataUser.APTOACCOUNTID = self.accountID
                self.performSegue(withIdentifier: "verifyCodeCardAptoSegue", sender: nil)
                
            }else{
                
                self.alert(title: "¡Aviso!", message: "Error desconocido contacte a un administrador.", cancel: "OK")
            }
            
        }
      
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
            self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
    }
    
    
    
    //metodo que actualiza el account_id
    func updateAccountIdServices(){
       
        let headers  : HTTPHeaders = [:]
        let params : [String : Any] = ["id_user":DataUser.USERIDD!,"token":DataUser.APTOTOKEN!,"account_id":self.accountID]
        self.services.send(type: .PUT, url:"Wallet/1/3/en/saveAptoInfo", params: params, header: headers, message: "Guardando",animate: true)
        
    }
   
}









