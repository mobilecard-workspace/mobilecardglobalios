


import UIKit
import AVFoundation





class AptoThxViewController:UIViewController{

    var accountID = ""
    var pan = ""
    var cvv = ""
    var vigencia = ""
    
    
   
    
    @IBOutlet weak var questionTextView: UITextView!
    @IBOutlet weak var adressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        if DataUser.APTOADRESS != nil{
 self.adressLabel.text = DataUser.APTOADRESS!
        }
        
        
        

        
        let attributedString = NSMutableAttributedString(string: "Have questions? Please contact Customer \nSupport")
        let urlSupport = URL(string: "https://www.google.com.mx")!
        
        
        attributedString.setAttributes([.link: urlSupport], range: NSMakeRange(31, 17 ))

        
        let fullRange = NSMakeRange(0, attributedString.length)
        
        attributedString.addAttribute(NSAttributedString.Key.font, value: self.questionTextView.font!, range: fullRange)
        
        
        self.questionTextView.attributedText = attributedString
        self.questionTextView.isUserInteractionEnabled = true
     
        
        
        self.questionTextView.linkTextAttributes = [
            .foregroundColor: #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        
        
    }



    
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
  
        
    }
    
    
  
   
}









