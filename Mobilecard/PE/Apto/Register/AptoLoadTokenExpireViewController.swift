


import UIKit
import AVFoundation
import Alamofire






class AptoLoadTokenExpireViewController:UIViewController,ServicesDelegate{

    let services = Services()
  
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var principalAmount : Double = 0
    var spendableAmount :Double = 0
    
    @IBOutlet weak var backButtonItem: UIBarButtonItem!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
   
           
        
      
        self.services.delegate = self
    
        
  
self.loginApto()
        
        
    }


//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: Wallet_Ctrl.self) {
                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
          
    }
    
    
    //metodo que logea en apto
    func loginApto(){
        
        
        
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)"]
        
        
        var data :[[String:Any]] = []
        
        let smsDataPoint : [String : Any] = ["verification_id":DataUser.VERIFICATIONID_SMS!]
        
        let mailDataPoint : [String : Any] = ["verification_id":DataUser.VERIFICATIONID_MAIL!]
        
      

        
        data.append(smsDataPoint)
        data.append(mailDataPoint)
        
        let verifications : [String : Any] = ["type":"list","data":data]
        
        let params : [String : Any] = ["verifications":verifications]
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Cargando", animated: true)
        
        print("estos son los parametros \(params)")
        
        Alamofire.request("\(ServicesLinks.aptoApi)/v1/user/login", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        
                        DispatchQueue.main.async {
                            LoaderView.hide()
                        }
                        print("error con servicio")
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: Wallet_Ctrl.self) {
                                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                        let user_id = dic!["user_id"] as! String
                        let user_token = dic!["user_token"] as! String
                        
                        DataUser.IDUSERAPTO = user_id
                        DataUser.APTOTOKEN = user_token
                        
                        
                        print("este es el nuevo token \(DataUser.APTOTOKEN)")
                        
                        let headers  : HTTPHeaders = [:]
                        let params : [String : Any] = ["id_user":DataUser.USERIDD!,"id_verification_1":DataUser.VERIFICATIONID_SMS!,"id_verification_2":DataUser.VERIFICATIONID_MAIL!,"token":DataUser.APTOTOKEN!,"id_usuario_apto":DataUser.IDUSERAPTO!]
                        self.services.send(type: .PUT, url:"Wallet/1/3/en/saveAptoInfo", params: params, header: headers, message: "Guardando",animate: true)
                        
                    }else{
                        
                        
                        DispatchQueue.main.async {
                            LoaderView.hide()
                        }
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: Wallet_Ctrl.self) {
                                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                
                //error con servicio
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: Wallet_Ctrl.self) {
                        LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }
        
    }

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        //para cuando el token expira
        if segue.identifier == "tokenExpireSegue"{
            
            
                let destinoViewController = segue.destination as! AptoNumberViewController
            
            destinoViewController.fromView = 1
            
            
        }
        
        
    
        //para cuando la sesion esta activa token valido
        if segue.identifier == "loadHomeCompleteToken"{
            
                 let destinoViewController = segue.destination as! HomeAptoViewController
            destinoViewController.principalAmount = self.principalAmount
            destinoViewController.spendableAmount = spendableAmount
            
        }
  
        
    }
    
    
    
 //metodos de servicios
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
      
        if endpoint == "Wallet/1/3/en/saveAptoInfo"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                //checamos el balance ya como paso final
               self.balanceAction()
                
            }else{
           //regresamos al wallet hay algo mal con el servicio
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: Wallet_Ctrl.self) {
                        LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
            }
            
            
        }
   
        
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    

    func errorService(endpoint: String) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: Wallet_Ctrl.self) {
                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
  
   
    
    
    //metodo que llama el servicio de balance para ver si aun es valido el token y para guardar la cantidad
    func balanceAction(){
        
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
       
       
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.backButtonItem.isEnabled = false
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Obteniendo", animated: true)

        Alamofire.request("\(ServicesLinks.aptoApi)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)/balance", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
               
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        DispatchQueue.main.async {
                            LoaderView.hide()
                        }
                        print("error con servicio")
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: Wallet_Ctrl.self) {
                                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                    
                        let balance = dic!["balance"] as! NSDictionary
                        let principalAmount = balance["amount"] as! Double
                        let amountSpendable = dic!["amount_spendable"] as! NSDictionary
                        let spendableAmount = amountSpendable["amount"] as! Double
                        
                        self.principalAmount = principalAmount
                        self.spendableAmount = spendableAmount
                        
                                                //el token no cambio nos vamos directo a la otra vista
                        
                        LoaderView.hide()
                        self.performSegue(withIdentifier: "loadHomeCompleteToken", sender: nil)
                        
                        
                    }else{
                        
                        DispatchQueue.main.async {
                            LoaderView.hide()
                        }
                        print("error con servicio")
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: Wallet_Ctrl.self) {
                                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }

                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    DispatchQueue.main.async {
                        LoaderView.hide()
                    }
                    print("error con servicio")
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: Wallet_Ctrl.self) {
                            LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                print("error con servicio")
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: Wallet_Ctrl.self) {
                        LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
            
             
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                 self.backButtonItem.isEnabled = true
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
            
        }
        
        
        
        
    }
    
 
    
}









