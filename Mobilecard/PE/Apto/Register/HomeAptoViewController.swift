


import UIKit
import AVFoundation
import Alamofire




class HomeAptoViewController:UIViewController,UITableViewDataSource,UITableViewDelegate{


    //variable que controla la tableView
    var transactions : [TransactionApto] = []
    
    var indexTransaction = 0
    
    var stateModel  : StateModel!
    
    @IBOutlet weak var namePersonCardLabel: UILabel!
    //referencias de componentes visuales
    @IBOutlet weak var principalAmountLabel: UILabel!
    @IBOutlet weak var spendableAmountLabel: UILabel!
    @IBOutlet weak var numberCardLabel: UILabel!
    @IBOutlet weak var expirationLabel: UILabel!
    @IBOutlet weak var showButton: UIButton!
    @IBOutlet weak var tarjetaReversoImageView: UIImageView!
    @IBOutlet weak var numberCvvLabel: UILabel!
    @IBOutlet weak var hideButton: UIButton!
    
    @IBOutlet weak var viewCard: UIView!
    
    
    //variables que pueden llegar de la vista pasada
    
    var principalAmount : Double = 0
    var spendableAmount : Double = 0
  
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.stateModel = StateModel()
        
        if DataUser.APTOCODIGO != nil{
            self.numberCvvLabel.text = DataUser.APTOCODIGO!
            
            
        }
        
        if DataUser.APTOCARD != nil{
            let characters = Array(DataUser.APTOCARD!)
            self.numberCardLabel.text = "\(characters[0])\(characters[1])\(characters[2])\(characters[3]) \(characters[4])\(characters[5])\(characters[6])\(characters[7]) \(characters[8])\(characters[9])\(characters[10])\(characters[11]) \(characters[12])\(characters[13])\(characters[14])\(characters[15])"
        }
        
        if DataUser.APTOVIGENCIA != nil{
            self.expirationLabel.text = DataUser.APTOVIGENCIA!
        }
        
        self.tarjetaReversoImageView.isHidden = true
        self.numberCvvLabel.isHidden = true
        self.hideButton.isHidden = true
      self.hideButton.isUserInteractionEnabled = false
        self.principalAmountLabel.text = "$\(self.principalAmount)"
        
        self.spendableAmountLabel.text = "$\(self.spendableAmount)"
      
//metodo que obtiene el balance
        self.balanceAction()
        self.transactionsAction()
        self.statusAction()
        //agregamos manualmente transacciones de prueba
        
        
       // let trans1 = TransactionApto(nameTransaction: "Amazon Mx México Df Mex", dateTransaction: "February 26, 8:33 PM", amountTransaction: "+ MXN 0.00", statusTransaction: "Complete", categoryTransaction: "ATM", deviceTypeTransaction: "eCommerce", typeTransaction: "Reversal", idTransaction: "txn_7e630e1fsdfosdoi", toYouAccountAmount: "$0.05", toYouAccountId: "ID: 4858984", toYouAccountTypeChange: "1 $ = MXN 1.00", fromMobileCardAmount: "$0.0", fromMobileCardId: "ID: 48589323", fromMobileCardTypeChange: "1 $ = MXN 1.00", lat: "19.481835", long: "-99.230661")
        
      //   let trans2 = TransactionApto(nameTransaction: "Amazon Mx México Df Mex", dateTransaction: "January 26, 8:33 PM", amountTransaction: "+ MXN 0.00", statusTransaction: "Complete", categoryTransaction: "ATM", deviceTypeTransaction: "eCommerce", typeTransaction: "Reversal", idTransaction: "txn_7e630e1fsdfosdoi", toYouAccountAmount: "$0.05", toYouAccountId: "ID: 4858984", toYouAccountTypeChange: "1 $ = MXN 1.00", fromMobileCardAmount: "$0.0", fromMobileCardId: "ID: 48589323", fromMobileCardTypeChange: "1 $ = MXN 1.00", lat: "19.481835", long: "-99.230661")
        
      //  let monthTrans = TransactionApto(type: "month", month: "February")
      //  let monthTransTwo = TransactionApto(type: "month", month: "January")
        
        
     //   self.transactions.append(monthTrans)
       // self.transactions.append(trans1)
         // self.transactions.append(monthTransTwo)
        //self.transactions.append(trans2)
        //self.transactions.append(trans2)
        //self.transactions.append(trans2)
        //self.transactions.append(trans2)
        //self.transactions.append(trans2)
       // self.transactions.append(trans2)
       // self.transactions.append(trans2)
        //self.transactions.append(trans2)
    
        //self.tableView.reloadData()
        
        
        
       

        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.balanceAction()
    }

    
    @IBAction func configAction(_ sender: UIBarButtonItem) {
        
        
        if self.stateModel.nameCard != ""{
            self.performSegue(withIdentifier: "configSegue", sender: nil)
            
        }else{
            self.alert(title: "¡Aviso!", message: "No es posible entrar a las configuraciones actualmenente,disculpe las molestias.", cancel: "OK")
        }
        
    }
    

//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MainMenu_Ctrl.self) {
                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
          
    }
    
    
    
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "configSegue"{
            // nos llevamos los datos a la siguiente clase
            let destinoViewController = segue.destination as! AptoConfigViewController
            destinoViewController.stateModel = self.stateModel
            
        }
  
        if segue.identifier == "detailTransactionApto"{
            
        
            // nos llevamos los datos a la siguiente clase
            let destinoViewController = segue.destination as! TransactionAptoViewController
        
            //mandamos el objeto a la otra vista
            destinoViewController.transaction = self.transactions[self.indexTransaction]
            
        }
        
    }
    
    
    
    
    //metodos datasource tableview
    
    
    //metodos del datasource tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transactions.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        //celda que muestra el mes
        if self.transactions[indexPath.row].type == "month"{
            
            
            let celdaID = "MonthCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: celdaID,for: indexPath) as! AptoMonthCell
            
           cell.monthLabel.text = self.transactions[indexPath.row].month
            
            
            cell.selectionStyle = .none
            
            
            
            return cell
            
        }else{
        
            //celda que muestra una transaccion
        let celdaID = "TransactionCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: celdaID,for: indexPath) as! AptoTransactionCell
        
        
        
        cell.amountLabel.text = self.transactions[indexPath.row].amountTransaction
        cell.nameLabel.text = self.transactions[indexPath.row].nameTransaction
        cell.dateLabel.text = self.transactions[indexPath.row].dateTransaction
        
        
        cell.selectionStyle = .none
        
        
        
        return cell
            
        }
        
    }
    
    
    
    //metodo delegato tableview
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.transactions[indexPath.row].type == "month"{
            
            return 30
        }else{
        
        return 60
        }
    }
    
    
    //cuando se presiona una celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if self.transactions[indexPath.row].type != "month"
        {
            
            
            self.indexTransaction = indexPath.row
            self.performSegue(withIdentifier: "detailTransactionApto", sender: nil)
            
        }
    }
  
   
    //metodo que se ejecuta al presionar el botón de ver la tarjeta
    @IBAction func showCvvButtonAction(_ sender: UIButton) {
        
          UIView.transition(with: self.viewCard, duration: 0.4, options: .transitionFlipFromRight, animations: {
        self.tarjetaReversoImageView.isHidden = false
        self.numberCvvLabel.isHidden = false
        self.hideButton.isHidden = false
        self.hideButton.isUserInteractionEnabled = true
        self.showButton.isUserInteractionEnabled = false
          })
    }
    
    //metodo que se ejecuta al presionar el botón de esconder tarjeta
    @IBAction func hideButtonAction(_ sender: UIButton) {
        
        UIView.transition(with: self.viewCard, duration: 0.4, options: .transitionFlipFromLeft, animations: {
            self.tarjetaReversoImageView.isHidden = true
            self.numberCvvLabel.isHidden = true
            self.hideButton.isHidden = true
            self.hideButton.isUserInteractionEnabled = false
            self.showButton.isUserInteractionEnabled = true
        })
        
       
        
        
    }
    
    
    
    //metodo que llama el servicio de balance
    func balanceAction(){
        
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
        
        
        
  
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Obteniendo", animated: true)
        
        Alamofire.request("\(ServicesLinks.aptoApi)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)/balance", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                        let balance = dic!["balance"] as! NSDictionary
                        let principalAmount = balance["amount"] as! Double
                        let amountSpendable = dic!["amount_spendable"] as! NSDictionary
                        let spendableAmount = amountSpendable["amount"] as! Double
                        
                        self.principalAmount = principalAmount
                        self.spendableAmount = spendableAmount
                        
                        self.principalAmountLabel.text = "$\(self.principalAmount)"
                        
                        self.spendableAmountLabel.text = "$\(self.spendableAmount)"
                        
               
                        
                        
    
                        
                    }else{
                        
                     
                            
                            self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
           
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
         
            }
            
        }
        
        
        
        
    }
    
    
    //metodo que obtiene las transacciones
    func transactionsAction(){
    
    
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
        
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Obteniendo", animated: true)
        
        Alamofire.request("\(ServicesLinks.aptoApi)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)/transactions", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                        self.transactions = []
                        
                        let data = dic!["data"] as! NSArray
                        
                        for element in data{
                            let dictio = element as! NSDictionary
                            let nameTransaction = dictio["description"] as? String ?? ""
                           
                            
                            let dateTransaction = dictio["created_at"] as? Double ?? 0
                            let local_amount = dictio["local_amount"] as! NSDictionary
                            let local_amountRound = Double(round(1000*(local_amount["amount"] as? Double ?? 0))/1000)
                            let amountTransaction = "\(local_amount["currency"] as? String ?? "") \(String(local_amountRound))"
                            let statusTransaction =  dictio["state"] as? String ?? ""
                            let store = dictio["store"] as! NSDictionary
                            let location = store["location"] as! NSDictionary
                            let latitud = location["latitude"] as? Double ?? 0.0
                            let longitud = location["longitude"] as? Double ?? 0.0
                            let categoryTransaction = store["type"] as? String ?? ""
                            let ecommerce = dictio["ecommerce"] as? BooleanLiteralType ?? false
                            var ecommerceSend = ""
                            
                            if ecommerce == true{
                                
                                ecommerceSend = "eCommerce"
                            }else{
                                ecommerceSend = ""
                            }
                            let typeTransaction = dictio["transaction_type"] as? String ?? ""
                            let idTransaction = dictio["id"] as? String ?? ""
                            
                            let dateTrans = Date(timeIntervalSince1970: dateTransaction)
                            
                            let dateFormatter = DateFormatter()
                           // dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            dateFormatter.dateFormat = "MMMM dd',' h:mm a '-' yyyy"
                            dateFormatter.amSymbol = "AM"
                            dateFormatter.pmSymbol = "PM"
                            dateFormatter.locale = Locale(identifier: "es_MX")
                             //dateFormatter.locale = NSLocale(localeIdentifier: "es") as Locale?
                            let dateString = dateFormatter.string(from: dateTrans)
                           
                           
                            
                            
                            let trans1 = TransactionApto(nameTransaction: nameTransaction, dateTransaction: dateString, amountTransaction: amountTransaction, statusTransaction: statusTransaction, categoryTransaction: categoryTransaction, deviceTypeTransaction: ecommerceSend, typeTransaction: typeTransaction, idTransaction: idTransaction, toYouAccountAmount: "$0.05", toYouAccountId: "ID: 4858984", toYouAccountTypeChange: "1 $ = MXN 1.00", fromMobileCardAmount: "$0.0", fromMobileCardId: "ID: 48589323", fromMobileCardTypeChange: "1 $ = MXN 1.00", lat: String(latitud), long: String(longitud))
                            
                            self.transactions.append(trans1)
                        }
                        
                //    self.transactions =  self.transactions.sorted(by: { $0.dateTransaction > $1.dateTransaction })
                        
                        
                  self.tableView.reloadData()
                        
                        
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                    
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                
            }
            
        }
        
        
        
    }
    
    
    //metodo que obtiene el nombre del dueño se la tarjeta y el estatus de la misma
    func statusAction(){
        
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
        
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Obteniendo", animated: true)
        
        Alamofire.request("\(ServicesLinks.aptoVaul)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                        let nameOfCard = dic!["name_on_card"] as! String
                        self.namePersonCardLabel.text = nameOfCard
                 
                        
                        let state = dic!["state"] as! String
                        
                    self.stateModel.nameCard = nameOfCard
                        
                        if state == "active"{
                        self.stateModel.state = true
                        }else{
                            self.stateModel.state = false
                        }
                        
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                    
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                
            }
            
        }
        
        
        
    }
    
}









