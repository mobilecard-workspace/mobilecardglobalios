


import UIKit
import AVFoundation
import Alamofire






class AptoLoadRegisterViewController:UIViewController,ServicesDelegate{

    let services = Services()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
        DataUser.APTOTOKEN = nil
        DataUser.APTONAMEPERSON = nil
        DataUser.APTOADRESS = nil
        DataUser.APTOACCOUNTID = nil
        DataUser.APTOTESTSESION = nil
        DataUser.PHONE_APTO = nil
        DataUser.IDUSERAPTO = nil
        DataUser.VERIFICATIONID_MAIL = nil
        DataUser.VERIFICATIONID_SMS = nil
       */
           
        
      
        self.services.delegate = self
    
        
        let headers  : HTTPHeaders = [:]
        let params : [String : Any] = [:]
        self.services.send(type: .GET, url:"Wallet/1/3/en/\(DataUser.USERIDD!)/getInfoApto", params: params, header: headers, message: "Cargando",animate: true)
       

        
        
    }


//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: Wallet_Ctrl.self) {
                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
          
    }
    
    
    
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
  
        
    }
    
    
    
 //metodos de servicios
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print(response)
        if endpoint == "Wallet/1/3/en/\(DataUser.USERIDD!)/getInfoApto"{
            
            
            let code = response["code"] as! Int
            
            if code ==  0{
                
                //guardamos los valores temporalmente
                
                let idVerificationOne = response["id_verification_1"] as! String
                let idVerificationTwo = response["id_verification_2"] as! String
                let token = response["token"] as! String
                let idUserApto = response["id_usuario_apto"] as! String
                
                DataUser.VERIFICATIONID_SMS = idVerificationOne
                DataUser.VERIFICATIONID_MAIL = idVerificationTwo
                DataUser.APTOTOKEN = token
                DataUser.IDUSERAPTO = idUserApto
                
                
                var account_id  = ""
            
                
                if response["account_id"] is NSNull{
                    
                    
                }else{
                let account_idd = response["account_id"] as! String
                    account_id = account_idd
                }
                
                
                //aun no tiene account_id fata crear la tarjeta
                if account_id == ""{
                    
                    
                

                self.performSegue(withIdentifier: "continueRegisterAptoSegue", sender: nil)
                
                }else{
                    
                    DataUser.APTOACCOUNTID = account_id
                    //ya tiene account_id procedemos a activar la tarjeta
                   self.performSegue(withIdentifier: "activationCardCodeAptoSegue", sender: nil)
                    
                    
                    
                }
                    
            }else{
                
                self.performSegue(withIdentifier: "registerAptoSegue", sender: nil)
                
            }
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    

    func errorService(endpoint: String) {
        self.navigationController?.popViewController(animated: true)
    }
  
   
}









