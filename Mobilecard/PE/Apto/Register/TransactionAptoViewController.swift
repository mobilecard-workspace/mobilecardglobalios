


import UIKit
import AVFoundation
import MapKit




class TransactionAptoViewController:UIViewController,MKMapViewDelegate{


    
    //objeto que recibimos de la clase pasada
    
    var transaction : TransactionApto!
    
    
    //referencias graficas
    @IBOutlet weak var transactionAmountLabel: UILabel!
    @IBOutlet weak var nameTransactionLabel: UILabel!
    @IBOutlet weak var dateTransactionLabel: UILabel!
    @IBOutlet weak var statusTransactionLabel: UILabel!
    @IBOutlet weak var categoryTransactionLabel: UILabel!
    @IBOutlet weak var deviceTypeTransactionLabel: UILabel!
    @IBOutlet weak var typeTransactionLabel: UILabel!
    @IBOutlet weak var idTransactionLabel: UILabel!
    @IBOutlet weak var toYouAccountAmountLabel: UILabel!
    @IBOutlet weak var toYouAccountIdLabel: UILabel!
    @IBOutlet weak var toYouAccountTypeChangeLabel: UILabel!
    @IBOutlet weak var fromYouMobileCardAmountLabel: UILabel!
    @IBOutlet weak var fromYouMobileCardIdLabel: UILabel!
    @IBOutlet weak var fromnYouMobileCardTypeChangeLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    
        self.mapView.delegate = self
        
    
        
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = CLLocationCoordinate2D(latitude: Double(self.transaction.lat)!, longitude: Double(self.transaction.long)!)
        
        
        self.mapView.showAnnotations([annotation], animated: true)
        self.mapView.selectAnnotation(annotation, animated: true)
       

        
        self.transactionAmountLabel.text = self.transaction.amountTransaction!
        self.nameTransactionLabel.text = self.transaction.nameTransaction!
        self.dateTransactionLabel.text = self.transaction.dateTransaction!
        self.statusTransactionLabel.text = self.transaction.statusTransaction!
        self.categoryTransactionLabel.text = self.transaction.categoryTransaction!
        self.deviceTypeTransactionLabel.text = self.transaction.deviceTypeTransaction!
        self.typeTransactionLabel.text = self.transaction.typeTransaction!
        self.idTransactionLabel.text = self.transaction.idTransaction!
        self.toYouAccountAmountLabel.text = self.transaction.toYouAccountAmount!
        self.toYouAccountIdLabel.text = self.transaction.toYouAccountId!
        self.toYouAccountTypeChangeLabel.text = self.transaction.toYouAccountTypeChange!
        self.fromYouMobileCardAmountLabel.text = self.transaction.fromMobileCardAmount!
        self.fromYouMobileCardIdLabel.text = self.transaction.fromMobileCardId!
        self.fromnYouMobileCardTypeChangeLabel.text = self.transaction.fromMobileCardTypeChange!
        
        
       

        
        
    }


//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
       self.navigationController?.popViewController(animated: true)
          
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        
        let identifier = "MyPin"
        if annotation is MKUserLocation {
            return nil
        }
        
        
       
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage(named: "markerApto")
            
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
  
        
    }
    
    
  
   
}









