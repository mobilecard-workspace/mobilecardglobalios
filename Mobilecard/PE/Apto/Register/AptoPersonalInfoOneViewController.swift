


import UIKit
import AVFoundation





class AptoPersonalInfoOneViewController:UIViewController{


    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      

        
        self.emailTextField.text = self.email
        self.emailTextField.isUserInteractionEnabled = false
        
        
       

        
        
    }



    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "PersonalInfoFirstSegue"{
            
            
            
            // nos llevamos los datos a la siguiente clase
            let destinoViewController = segue.destination as! AptoPersonalInfoTwoViewController
            
            destinoViewController.firstName = self.firstNameTextField.text!
            destinoViewController.lastName = self.lastNameTextField.text!
            destinoViewController.email = self.emailTextField.text!
            
        }
  
        
    }
    
    
    //metodo que se ejecuta al presionar el boton de continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        if (self.firstNameTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un nombre válido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.lastNameTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un apellido válido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.emailTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un correo electrónico válido para continuar.", cancel: "OK");
            return;
        }
        
        
        
        
        
        
        self.performSegue(withIdentifier: "PersonalInfoFirstSegue", sender: nil)
        
        
        
    }
    
    
    
  
   
}









