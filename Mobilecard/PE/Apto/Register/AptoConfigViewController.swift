
import UIKit
import Alamofire

class AptoConfigViewController: UIViewController ,AptoPinDelegate{
    
    //objeto que recibimos de la clase pasada
       var stateModel  : StateModel!

    @IBOutlet weak var cardSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if self.stateModel.state == true{
            
             self.cardSwitch.isOn = false
            
        }else{
         self.cardSwitch.isOn = true
        }

    }
    
    
    
    //metodo que se ejecuta al presionar el boton de cambiar pin
    @IBAction func changePinAction(_ sender: UIButton) {
  
        let storyboard = UIStoryboard(name: "Apto", bundle: nil)
        let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAptoAlertAceptarID") as! AptoChangePinAlertViewMessage
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        
        self.present(customAlert, animated: true, completion: nil)
    
    
    }
    
    
    //metodo que cierra la vista
    @IBAction func closeAction(_ sender: UIButton) {
    
    self.dismiss(animated: true, completion: nil)
    
    }
    
    
    
    
    //metodo delegado de cambiar pin , se ejecuta cuando el usuario quiere cambiar su pin (aqui llamamos al servicio)
    func didChangePin(pin: String) {
        
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
        
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Cambiando", animated: true)
        
          let params : [String : Any] = ["pin":pin]
        Alamofire.request("\(ServicesLinks.aptoVaul)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)/pin", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error al cambiar el pin.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                      
                        self.alert(title: "¡Aviso!", message: "Pin cambiado exitosamente.", cancel: "OK")
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        self.alert(title: "¡Aviso!", message: "Error al cambiar el pin.", cancel: "OK")
                        
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                    
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                
            }
            
        }
        
        
        
        
        
    }
    
    
    
    
    @IBAction func cardSwitchAction(_ sender: UISwitch) {
        
        if self.stateModel.state == true{
            //desactivamos tarjeta
            self.disableCard()
        }else{
            //activamos tarjeta
          self.enableCard()
            
        }
        
        
    }
    
    
    //metodo que deshabilita la tarjeta
    func disableCard(){
        
        
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
        
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Guardando", animated: true)
        
        Alamofire.request("\(ServicesLinks.aptoVaul)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)/disable", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                         self.cardSwitch.isOn = false
                        self.alert(title: "¡Aviso!", message: "No fue posible desactivar la tarjeta.", cancel: "OK")
                       
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                     
                        
                        
                        self.alert(title: "¡Aviso!", message: "La tarjeta ha sido bloqueada exitosamente.", cancel: "OK")
                        self.stateModel.state = false
                        
                      
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        self.cardSwitch.isOn = false
                        self.alert(title: "¡Aviso!", message: "No fue posible desactivar la tarjeta.", cancel: "OK")
                        
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                    
                }
                self.cardSwitch.isOn = false
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                
            }
            
        }
        
        
        
        
    }
    
    //metodo que habilita la tarjeta
    func enableCard(){
        
        
        
        
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
        
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Guardando", animated: true)
        
        Alamofire.request("\(ServicesLinks.aptoVaul)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)/enable", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.cardSwitch.isOn = true
                        self.alert(title: "¡Aviso!", message: "No fue posible activar la tarjeta.", cancel: "OK")
                        
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                        
                        
                        
                        self.alert(title: "¡Aviso!", message: "La tarjeta ha sido desbloqueada exitosamente.", cancel: "OK")
                        self.stateModel.state = true
                        
                        
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        self.cardSwitch.isOn = true
                        self.alert(title: "¡Aviso!", message: "No fue posible activar la tarjeta.", cancel: "OK")
                        
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                    
                }
                self.cardSwitch.isOn = true
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                
            }
            
        }
        
        
    }
    


}
