


import UIKit
import AVFoundation
import Alamofire




class AptoNumberViewController:UIViewController,UITextFieldDelegate{

    
     @IBOutlet weak var phoneTextField: UITextField!
      @IBOutlet weak var nextButton: UIBarButtonItem!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    var verification_id = ""
    
    
    //si es 0 viene normal agregando tarjeta apto si es 1 viene con token expirado
    var fromView = 0
    
    var sessionManager : SessionManager = {
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "\(ServicesLinks.aptoApi)": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: configuration,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    let challengeHandler: ((URLSession, URLAuthenticationChallenge) -> (URLSession.AuthChallengeDisposition, URLCredential?))? = { result, challenge in
        return (.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sessionManager.delegate.sessionDidReceiveChallenge = challengeHandler
        
        
        //targets
        self.phoneTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        //delegados
        self.phoneTextField.delegate = self

        
        
        if fromView == 1{
            self.backButton.isEnabled = false
            self.backButton.tintColor = UIColor.clear
        }
        

        
        
    }

    
    //metodo delegado cuando empieza a editar el textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.text = ""
   
       
      
        self.nextButton.isEnabled = false
        
    }


    @IBAction func closeAction(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
    }
    
  
    
    

 
    //metodo que checa en todo momento el contenido del textfield
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField.text!.count >= 10{
            
            
            self.nextButton.isEnabled = true
              textField.endEditing(false)
        }
        
    }
    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
  
        if segue.identifier == "codeSendSegue"{
        
            
            
            // nos llevamos el numero a la siguiente clase
            let destinoViewController = segue.destination as! AptoVerifyNumberViewController
            destinoViewController.tokenExpire = self.fromView
            destinoViewController.numberSend = self.phoneTextField.text!
            destinoViewController.verification_id =  self.verification_id
            
        }
        
    }
    
    @IBAction func nextButtonAction(_ sender: UIBarButtonItem) {
        
        
        self.callServiceSms()
        
        
        
    }
    
    
    
    
    
    //metodo que llama al servicio de envio de sms
    func callServiceSms(){
        
        
        
        
        DispatchQueue.main.async {
        
            var config:LoaderView.Config = LoaderView.Config()
            config.size = 100;
            config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
            config.spinnerColor    = UIColor.white;
            config.titleTextColor  = UIColor.white;
            LoaderView.setConfig(config: config);
            
            LoaderView.show(title: "Obteniendo", animated: true)
        }
        
        
        
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)"]
        let datapoint : [String : Any] = ["country_code":1,"phone_number":Int(self.phoneTextField.text!)!]
        var params : [String : Any] = ["datapoint_type":"phone"]
        params["datapoint"] = datapoint
        
        
        
        
        self.sessionManager.request("\(ServicesLinks.aptoApi)/v1/verifications/start", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                 // print(dic)
               
                   self.verification_id = dic!["verification_id"] as! String
                    
                    
                    
                    //guardamos el verification id del sms
                        DataUser.VERIFICATIONID_SMS = self.verification_id
                  
                    
                    self.performSegue(withIdentifier: "codeSendSegue", sender: nil)
                }else{
                    
                  //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                  //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                  self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
        }
        
        
        
        
        
        
        
    }
  
   
}









