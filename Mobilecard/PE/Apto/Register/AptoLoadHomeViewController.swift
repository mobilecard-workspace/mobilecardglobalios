


import UIKit
import AVFoundation
import Alamofire






class AptoLoadHomeViewController:UIViewController,ServicesDelegate,CustomAlertPrivacyDelegate{

    let services = Services()
  
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var principalAmount : Double = 0
    var spendableAmount :Double = 0
    
    @IBOutlet weak var backButtonItem: UIBarButtonItem!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
   
           
        
      
        self.services.delegate = self
    
        
        let headers  : HTTPHeaders = [:]
        let params : [String : Any] = [:]
        self.services.send(type: .GET, url:"Wallet/1/3/en/\(DataUser.USERIDD!)/getInfoApto", params: params, header: headers, message: "Cargando",animate: true)
       

        
        
    }


//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: Wallet_Ctrl.self) {
                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
          
    }
    
    
    
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        //para cuando el token expira
        if segue.identifier == "tokenExpireSegue"{
            
            
                let destinoViewController = segue.destination as! AptoNumberViewController
            
            destinoViewController.fromView = 1
            
            
        }
        
        //para cuando la sesion esta activa token valido
        if segue.identifier == "loadHomeComplete"{
            
                 let destinoViewController = segue.destination as! HomeAptoViewController
            destinoViewController.principalAmount = self.principalAmount
            destinoViewController.spendableAmount = spendableAmount
            
        }
  
        
    }
    
    
    
 //metodos de servicios
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print(response)
        if endpoint == "Wallet/1/3/en/\(DataUser.USERIDD!)/getInfoApto"{
            
            
            
            let code = response["code"] as! Int
            
            if code ==  0{
                
                //guardamos los valores temporalmente
                
                let idVerificationOne = response["id_verification_1"] as! String
                let idVerificationTwo = response["id_verification_2"] as! String
                let token = response["token"] as! String
                let idUserApto = response["id_usuario_apto"] as! String
                
                DataUser.VERIFICATIONID_SMS = idVerificationOne
                DataUser.VERIFICATIONID_MAIL = idVerificationTwo
                DataUser.APTOTOKEN = token
                DataUser.IDUSERAPTO = idUserApto

                //metodo que llama el servicio de obtener balance ahi consultamos si el token es valido aun
                
                self.balanceAction()
                
                
            
                
            }else{
                
                //no tiene tarjeta apto , esto no deberia de ser posible si entra es problema de back que viene una tarjeta apto cuando no hay
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: MainMenu_Ctrl.self) {
                        LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
            }
            
        }
        
        
   
        
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    

    func errorService(endpoint: String) {
        self.navigationController?.popViewController(animated: true)
    }
  
   
    //metodo que se ejecuta al presionar el boton de aceptar despues de mostrar la informacion de que el token expiro
    func didAcceptButton() {
        
        
        self.performSegue(withIdentifier: "tokenExpireSegue", sender: nil)
        
        
        
    }
   
    
    
    //metodo que llama el servicio de balance para ver si aun es valido el token y para guardar la cantidad
    func balanceAction(){
        
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
       print("este es el token \(DataUser.APTOTOKEN!)")
       
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.backButtonItem.isEnabled = false
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Cargando", animated: true)

        Alamofire.request("\(ServicesLinks.aptoApi)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)/balance", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                print("este es el user id \(DataUser.APTOACCOUNTID!)")
                
                print("este es el response \(response.result.value)")
                
               
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                 
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                    
                        let balance = dic!["balance"] as! NSDictionary
                        let principalAmount = balance["amount"] as! Double
                        let amountSpendable = dic!["amount_spendable"] as! NSDictionary
                        let spendableAmount = amountSpendable["amount"] as! Double
                        
                        self.principalAmount = principalAmount
                        self.spendableAmount = spendableAmount
                        
                                                //el token no cambio nos vamos directo a la otra vista
                            self.performSegue(withIdentifier: "loadHomeComplete", sender: nil)
                            
                        
            
                        
                        
                        
                        
                        
                    }else{
                        
                        if code == 3031{
                            //el token ha expirado nos vamos a que se registren nuevamente con el nuevo camino
                            
                            let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                            let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertPrivacyID") as! CustomAlertPrivacyViewMessage
                            customAlert.providesPresentationContextTransitionStyle = true
                            customAlert.definesPresentationContext = true
                            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            customAlert.titulo = "Información"
                            customAlert.mensaje = "Para su seguridad, necesitamos validar sus datos nuevamente. gracias por entender."
                            customAlert.delegate = self
                            
                            self.present(customAlert, animated: true, completion: nil)
                        
                            
                            
                        }else{
                        
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: MainMenu_Ctrl.self) {
                                    LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                     self.backButtonItem.isEnabled = true
                    self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimating()
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                 self.backButtonItem.isEnabled = true
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
            
        }
        
        
        
        
    }
    
 
    
}









