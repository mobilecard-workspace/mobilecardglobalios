


import UIKit
import AVFoundation
import Alamofire




class AptoMailViewController:UIViewController,UITextFieldDelegate{

    @IBOutlet weak var emailTextField: UITextField!
    
      @IBOutlet weak var nextButton: UIBarButtonItem!
    
    var verification_id = ""
     //0 aun no expira 1 viene de token expirado
    var tokenExpire = 0
    
    var sessionManager : SessionManager = {
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "\(ServicesLinks.aptoApi)": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: configuration,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    let challengeHandler: ((URLSession, URLAuthenticationChallenge) -> (URLSession.AuthChallengeDisposition, URLCredential?))? = { result, challenge in
        return (.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if self.tokenExpire == 1{
            self.verification_id = DataUser.VERIFICATIONID_MAIL
            self.performSegue(withIdentifier: "checkEmailSegue", sender: nil)
            return;
        }
        self.sessionManager.delegate.sessionDidReceiveChallenge = challengeHandler
        
        
        //targets
        self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        //delegados
        self.emailTextField.delegate = self

        
        
       

        
        
    }

    
    //metodo delegado cuando empieza a editar el textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.text = ""
   
       
      
        self.nextButton.isEnabled = false
        
    }



  
    
    

 
    //metodo que checa en todo momento el contenido del textfield
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField.text!.count >= 3{
            
            
            self.nextButton.isEnabled = true
             
        }
        
    }
    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
  
        if segue.identifier == "checkEmailSegue"{
        
            
            
            // nos llevamos el numero a la siguiente clase
            let destinoViewController = segue.destination as! AptoVerifyNumberViewController
            destinoViewController.tokenExpire = self.tokenExpire
            destinoViewController.fromView = 2
            destinoViewController.verification_id =
                self.verification_id
            destinoViewController.email = self.emailTextField.text!
        }
        
    }
    
    @IBAction func nextButtonAction(_ sender: UIBarButtonItem) {
        
        
        self.callServiceEmail()
        
        
        
    }
    
    
    
    
    
    //metodo que llama al servicio de envio de email
    func callServiceEmail(){
        
        
        
        
        DispatchQueue.main.async {
        
            var config:LoaderView.Config = LoaderView.Config()
            config.size = 100;
            config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
            config.spinnerColor    = UIColor.white;
            config.titleTextColor  = UIColor.white;
            LoaderView.setConfig(config: config);
            
            LoaderView.show(title: "Obteniendo", animated: true)
        }
        
        
        
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)"]
        let datapoint : [String : Any] = ["email":self.emailTextField.text!]
        var params : [String : Any] = ["datapoint_type":"email"]
        params["datapoint"] = datapoint
        
        
        
        
        self.sessionManager.request("\(ServicesLinks.aptoApi)/v1/verifications/start", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                  print(dic)
               
                   self.verification_id = dic!["verification_id"] as! String
                    
                   //guardamos el verificationid del mail
                        DataUser.VERIFICATIONID_MAIL = self.verification_id
                        
               
                    
                    self.performSegue(withIdentifier: "checkEmailSegue", sender: nil)
                }else{
                    
                  //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                  //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                  self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
        }
        
        
        
        
        
        
        
    }
  
   
}









