


import UIKit
import AVFoundation
import Alamofire




class AptoVerifyNumberViewController:UIViewController,UITextFieldDelegate,MyTextFieldDelegate{

  
    //numero que se recibe de la vista pasada y codigo de verificacion
    var numberSend = ""
    var verification_id = ""
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    //variable que controla de que vista viene si es 1 viene de sms si es 2 viene de email
    var fromView = 1
    //0 aun no expira 1 viene de token expirado
    var tokenExpire = 0
    var email = ""
    
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBOutlet weak var textResendCodeLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    
    
    @IBOutlet weak var textTopLabel: UILabel!
    
    @IBOutlet weak var verifyOne: MyTextField!
    @IBOutlet weak var verifyTwo: MyTextField!
    @IBOutlet weak var verifyThree: MyTextField!
    @IBOutlet weak var verifyFour: MyTextField!
    @IBOutlet weak var verifyFive: MyTextField!
    @IBOutlet weak var verifySix: MyTextField!
    
 
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24.0),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
     var attributedString = NSMutableAttributedString(string:"")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        if self.fromView == 1{
        self.numberLabel.text = "+1 \(self.numberSend)"
        }else if self.fromView == 2{
            
             self.numberLabel.text = self.email
        }
        
        if self.email == "" && self.tokenExpire == 1 && self.fromView == 2{
            self.numberLabel.text = ""
            self.textTopLabel.text = "Se envió un código a su correo electrónico."
            self.backButton.tintColor = UIColor.clear
            self.backButton.isEnabled = false
            self.textResendCodeLabel.text = ""
            self.resendButton.isHidden = true
            self.resendButton.isUserInteractionEnabled = false
        }
        
         //targets
        self.verifyOne.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifyTwo.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifyThree.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifyFour.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifyFive.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifySix.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
      
        self.attrs[NSAttributedString.Key.font] = self.resendButton.titleLabel?.font
        self.underlineButton(button: self.resendButton)

        
          //delegados
        self.verifyOne.delegate = self
        self.verifyTwo.delegate = self
        self.verifyThree.delegate = self
        self.verifyFour.delegate = self
        self.verifyFive.delegate = self
        self.verifySix.delegate = self
        self.verifyOne.myTextFieldDelegate = self
        self.verifyTwo.myTextFieldDelegate = self
        self.verifyThree.myTextFieldDelegate = self
        self.verifyFour.myTextFieldDelegate = self
        self.verifyFive.myTextFieldDelegate = self
        self.verifySix.myTextFieldDelegate = self
       

        
        
    }
    
    
    
    //metodo que checa en todo momento el contenido del textfield
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        switch textField.tag {
            
            
        case 1:
            
            if textField.text!.count >= 1 {
                self.verifyTwo.becomeFirstResponder()
            }
            
            break
            
        case 2:
            
            
            if textField.text!.count >= 1 {
                
                self.verifyThree.becomeFirstResponder()
            }
            
            break
            
        case 3:
            if textField.text!.count >= 1 {
                
                self.verifyFour.becomeFirstResponder()
                
            }
            
            break
            
        case 4:
            if textField.text!.count >= 1 {
                self.verifyFive.becomeFirstResponder()
                
                
            }
            
            break
            
        case 5:
            if textField.text!.count >= 1 {
                self.verifySix.becomeFirstResponder()
            }
            
            break
            
        case 6:
            if textField.text!.count >= 1 {
                
                print("termino la accion")
                textField.endEditing(true)
                
                if self.verifyOne.text == "" || self.verifyTwo.text == "" || self.verifyThree.text == "" || self.verifyFour.text == "" || self.verifyFive.text == "" || self.verifySix.text == ""{
                    self.alert(title: "¡Aviso!", message: "Debes poner el código completo para continuar.", cancel: "OK")
                    self.verifyOne.text = ""
                    self.verifyTwo.text = ""
                    self.verifyThree.text = ""
                    self.verifyFour.text = ""
                    self.verifyFive.text = ""
                    self.verifySix.text = ""
                    
                    return;
                }else{
                    
                    
                    //metodo que verifica el codigo
                self.verifyCode()
                    
                }
                
                
            }
            break
            
            
            
        default:
            break
            
            
            
      
        }
        
        
    }
    
    //metodo delegado cuando empieza a editar el textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField.tag {
            
        case 1,2,3,4,5,6:
            textField.text = ""
            break
            
        default:
            break
            
        }
    }
    
    
    //metodo que se ejecuta al presionar el boton de back en el keyboard
    func textFieldDidEnterBackspace(_ textField: MyTextField) {
        
        switch textField.tag {
        
        
        case 1:
            break
            
        case 2:
            self.verifyOne.becomeFirstResponder()
            break
            
        case 3:
            self.verifyTwo.becomeFirstResponder()
            break
            
        case 4:
            self.verifyThree.becomeFirstResponder()
            break
            
        case 5:
            self.verifyFour.becomeFirstResponder()
            break
            
        case 6:
            self.verifyFive.becomeFirstResponder()
            
            break
            
        default:
            break
        }
        
        
    }
    
    
    //metodo delegado del textfield para que no se puedan poner mas caracteres de los permitidos
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
            
            
        case 1:
            
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                return false
            }
            
            break
            
        case 2:
            
            
            
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 3:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 4:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 5:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 6:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                
                return false
            }
            break
            
        default:
            break
        }
        
         return true
    }
    
    
    //metodo que le pone una underline a un botón
    func underlineButton(button:UIButton){
        
        self.attributedString =  NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:button.titleLabel!.text!, attributes:attrs)
        attributedString.append(buttonTitleStr)
        
        button.setAttributedTitle(attributedString, for: .normal)
        
    }



    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

 

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "verifySmsCompleteSegue"{
            
                     let destinoViewController = segue.destination as! AptoMailViewController
                    destinoViewController.tokenExpire = self.tokenExpire
            
        }
        
    
        if segue.identifier == "allVerificationSegue"{
            
            
            
            // nos llevamos el numero a la siguiente clase
            let destinoViewController = segue.destination as! AptoTermsViewController
            
            destinoViewController.email = self.email
            
            
        }
  
        
    }
    
    
    //metodo que se ejecuta para validar el codigo
    func verifyCode(){
        
        
        
        
        DispatchQueue.main.async {
            
            var config:LoaderView.Config = LoaderView.Config()
            config.size = 100;
            config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
            config.spinnerColor    = UIColor.white;
            config.titleTextColor  = UIColor.white;
            LoaderView.setConfig(config: config);
            
            LoaderView.show(title: "Obteniendo", animated: true)
        }
        
        
        
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)"]
        
        
        
        let params : [String : Any] = ["secret":"\(self.verifyOne.text!)\(self.verifyTwo.text!)\(self.verifyThree.text!)\(self.verifyFour.text!)\(self.verifyFive.text!)\(self.verifySix.text!)"]
        
        
        
        
        
        print("este es el enpoint")
        print("\(ServicesLinks.aptoApi)/v1/verifications/\(self.verification_id)/finish")
        
        Alamofire.request("\(ServicesLinks.aptoApi)/v1/verifications/\(self.verification_id)/finish", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic!)
                    
                    let status = dic!["status"] as! String
                    if status == "passed"{
                        
                        print("codigo correcto")
                       
                        
                        if self.fromView == 1{
                            
                            DataUser.PHONE_APTO = self.numberSend
                           
                            if self.tokenExpire == 1{
                             
                                
                                let secound = dic!["secondary_credential"] as! NSDictionary
                                let emailIdVerification =  secound["verification_id"] as! String
                                DataUser.VERIFICATIONID_MAIL = emailIdVerification
                                self.updateViewTokenExpire()
                                return;
                                
                            }
                            
                            self.performSegue(withIdentifier:"verifySmsCompleteSegue", sender: nil)
                        
                        }else if self.fromView == 2{
                            
                            //aun no expiro el token viene de registro normal de apto
                            if self.tokenExpire == 0{
                            self.performSegue(withIdentifier:"allVerificationSegue", sender: nil)
                            }else{
                                //ya expiro el token llamamos vista de login y incoorporar denuevo al home
                               
                             self.performSegue(withIdentifier: "aptoTokenUpdate", sender: nil)
                                
                                
                                
                            }
                            
                        }
                        
                    }else{
                        self.verifyOne.text = ""
                        self.verifyTwo.text = ""
                        self.verifyThree.text = ""
                        self.verifyFour.text = ""
                        self.verifyFive.text = ""
                        self.verifySix.text = ""
                        self.alert(title: "¡Aviso!", message: "El código introducido es erróneo", cancel: "OK")
                        
                    }
                    
                }else{
                    
                  //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                   // print(dicArray!)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
        }
        
    }
    
  
    
    
    
    //metodo que reenvia el código
    @IBAction func resendCodeAction(_ sender: UIButton) {
        
        
        
        
        DispatchQueue.main.async {
            
            var config:LoaderView.Config = LoaderView.Config()
            config.size = 100;
            config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
            config.spinnerColor    = UIColor.white;
            config.titleTextColor  = UIColor.white;
            LoaderView.setConfig(config: config);
            
            LoaderView.show(title: "Obteniendo", animated: true)
        }
        
        
        
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)"]
        
        
        
        var datapoint : [String : Any] = ["country_code":1,"phone_number":2]
        var params : [String : Any] = ["datapoint_type":"phone"]
        params["datapoint"] = datapoint
       
        if self.fromView == 1{
            
            datapoint  = ["country_code":1,"phone_number":Int(self.numberSend)!]
            params  = ["datapoint_type":"phone"]
            params["datapoint"] = datapoint
            
        }else{
            
            datapoint  = ["email":self.email]
            params = ["datapoint_type":"email"]
            params["datapoint"] = datapoint
            
        }
        
        
        
        
        
        Alamofire.request("\(ServicesLinks.aptoApi)/v1/verifications/start", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    //respondemos el servicio
                  //  print("RESPONSE:")
                 //   print(dic)
                    
                    self.verification_id = dic!["verification_id"] as! String
                    
                    
                    //depende de que vista venimos actualizamos el verificationid
                    if self.fromView == 1{
                        
                        DataUser.VERIFICATIONID_SMS = self.verification_id
                        DataUser.PHONE_APTO = self.numberSend
                    }else if self.fromView == 2{
                        
                        DataUser.VERIFICATIONID_MAIL = self.verification_id
                        
                    }
                    
                    self.alert(title: "¡Aviso!", message: "El código se ha vuelto a enviar correctamente.", cancel: "OK")
                    
                }else{
                    
                  //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                   // print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
        }
        
        
    }
    
    
   //metodo que se ejecuta cuando el token expiro para no mandar la vista a donde se ingresa el correo y pedir automaticamente el codigo
    func updateViewTokenExpire(){
        
        self.tokenExpire = 1
        self.fromView = 2
        self.email = ""
        self.verification_id = DataUser.VERIFICATIONID_MAIL!
        
        self.verifyOne.text = ""
        self.verifyTwo.text = ""
        self.verifyThree.text = ""
        self.verifyFour.text = ""
        self.verifyFive.text = ""
        self.verifySix.text = ""
        
        self.numberLabel.text = ""
        self.textTopLabel.text = "Se envió un código a su correo electrónico."
        self.backButton.tintColor = UIColor.clear
        self.backButton.isEnabled = false
        self.textResendCodeLabel.text = ""
        self.resendButton.isHidden = true
        self.resendButton.isUserInteractionEnabled = false
        
    }
    
}









