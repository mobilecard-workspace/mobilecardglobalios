


import UIKit
import AVFoundation
import Alamofire



class AptoPersonalInfoThreeViewController:UIViewController,ServicesDelegate{
    
    
    var estadosNames : [String] = ["Alabama","Alaska","Arizona","Arkansas","California","Carolina del Norte","Carolina del Sur","Colorado","Connecticut","Dakota del Norte","Dakota del Sur","Delaware","Distrito de Columbia","Florida","Fuerzas Armadas de América","Fuerzas Armadas de Europa","Fuerzas Armadas del Pacífico","Georgia","Hawai","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Luisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","Nueva Hampshire","Nueva Jersey","Nueva York","Nuevo México","Ohio","Oklahoma","Oregón","Pensilvania","Rhode Island","Tennessee","Texas","Utah","Vermont","Virginia","Virginia Occidental","Washington","Wisconsin","Wyoming"]
    var codes : [String] = ["AL","AK","AZ","AR","CA","NC","SC","CO","CT","ND","SD","DE","DC","FL","AA","AE","AP","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NY","NM","OH","OK","OR","PA","RI","TN","TX","UT","VT","VA","WV","WA","WI","WY"]
    
    var codeSelected = ""


    
    //variables que recibimos de la clase pasada
    var firstName = ""
    var lastName = ""
    var email = ""
    var fechaNacimiento = ""
    var numeroDocumento = ""
    
    
    var adress = ""
    
    var user_id = ""
    var user_token = ""
    
    let services = Services()
    
    
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var moreAddressTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
      
self.services.delegate = self

        
        
       

        
        
    }



    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func stateSelecionAction(_ sender: UIButton) {
        
        
        ActionSheetStringPicker.show(withTitle: "State", rows: self.estadosNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
            
            
            let select = selection as! String
            
            self.stateTextField.text = select
            
           
            
            self.codeSelected = self.codes[value]
            
        }, cancel: { (action) in
            return
        }, origin: self.stateTextField)
        
    
        
    }
    

    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "PersonalInfoFinishSegue"{
            
            
      
       
            
            
            
            
        }
  
        
    }
    
    
    
    
    @IBAction func continmueAction(_ sender: UIButton) {
        
        
        if (self.addressTextField.text?.count)! < 6 {
            alert(title: "¡Aviso!", message: "Agrega una dirección válida para continuar.", cancel: "OK");
            return;
        }
        
        self.adress =  "\(self.addressTextField.text!) \(self.moreAddressTextField.text!)"
        
        
        
        
        if (self.stateTextField.text?.count)! < 1 {
            alert(title: "¡Aviso!", message: "Agregar un estado válido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.cityTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una ciudad válida para continuar.", cancel: "OK");
            return;
        }
        
        if (self.zipCodeTextField.text?.count)! < 4 {
            alert(title: "¡Aviso!", message: "Agregue un código postal válido para continuar.", cancel: "OK");
            return;
        }
        
        
        
        
        
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)"]
        
        
        var data :[[String:Any]] = []
        
        let smsDataPoint : [String : Any] = ["data_type":"phone","phone_number":DataUser.PHONE_APTO!,"country_code":"1","verification":["verification_id":DataUser.VERIFICATIONID_SMS!]]
        
        let mailDataPoint : [String : Any] = ["data_type":"email","email":self.email,"verification":["verification_id":DataUser.VERIFICATIONID_MAIL!]]
        
        let nameDataPoint : [String : Any] = ["data_type":"name","first_name":self.firstName,"last_name":self.lastName]
        
        
        //pendiente a revisar con apto
        let addressDataPoint : [String : Any] = ["data_type":"address","street_one":self.adress,"country":"US","locality":self.cityTextField.text!,"region":self.codeSelected,"postal_code":self.zipCodeTextField.text!,"street_two":""]
        
        let birthdateDataPoint : [String : Any] = ["data_type":"birthdate","date":self.fechaNacimiento]
        
       let snndateDataPoint : [String : Any] = ["data_type":"id_document","country":"US","doc_type":"ssn","value":self.numeroDocumento]
        
        
        
        
        data.append(smsDataPoint)
        data.append(mailDataPoint)
        data.append(nameDataPoint)
        data.append(addressDataPoint)
        data.append(birthdateDataPoint)
        data.append(snndateDataPoint)
        
        let datapoint : [String : Any] = ["type":"list","data":data]
        
        let params : [String : Any] = ["data_points":datapoint]
       // params["datapoint"] = datapoint
        
        //codigo ya que mandas al servicio donde actualizar la informacion de la sesion apto
        DataUser.APTOADRESS = "\(self.adress), \(self.cityTextField.text!), \(self.stateTextField.text!) \(self.zipCodeTextField.text!)."
        
        DataUser.APTONAMEPERSON = "\(self.firstName) \(self.lastName)"
        
        //validacion para si ya esta en apto pero aun no en nuestro servicio solo llame a nuestro servicio(caso extremo en el que por alguna razon se conecte el servicio de apto pero no el nuestro)
        if DataUser.VERIFICATIONID_MAIL != nil && DataUser.VERIFICATIONID_SMS != nil && DataUser.APTOTOKEN != nil && DataUser.IDUSERAPTO != nil && DataUser.APTOCONNECT != nil{
            
            let headers  : HTTPHeaders = [:]
            let params : [String : Any] = ["id_user":DataUser.USERIDD!,"id_verification_1":DataUser.VERIFICATIONID_SMS!,"id_verification_2":DataUser.VERIFICATIONID_MAIL!,"token":DataUser.APTOTOKEN!,"id_usuario_apto":DataUser.IDUSERAPTO!]
            
           
            self.services.send(type: .POST, url:"Wallet/1/3/en/saveAptoInfo", params: params, header: headers, message: "Cargando",animate: true)
            return;
        }
        //termina la validacion y continua el flujo normal
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Cargando", animated: true)
        
         print("estos son los parametros \(params)")
        
        Alamofire.request("\(ServicesLinks.aptoApi)/v1/user", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
          
                
                print("este es el response \(response.result.value)")
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                        self.user_id = dic!["user_id"] as! String
                        self.user_token = dic!["user_token"] as! String
                        
                        DataUser.IDUSERAPTO = self.user_id
                        DataUser.APTOTOKEN = self.user_token
                        
                        
                        
                        let headers  : HTTPHeaders = [:]
                        let params : [String : Any] = ["id_user":DataUser.USERIDD!,"id_verification_1":DataUser.VERIFICATIONID_SMS!,"id_verification_2":DataUser.VERIFICATIONID_MAIL!,"token":DataUser.APTOTOKEN!,"id_usuario_apto":DataUser.IDUSERAPTO!]
                        self.services.send(type: .POST, url:"Wallet/1/3/en/saveAptoInfo", params: params, header: headers, message: "Cargando",animate: true)
                        
                    }else{
                        
                        if code ==  3044{
                            
                            self.alert(title: "¡Aviso!", message: "El usuario que estas intentando crear ya existe.", cancel: "OK")
                            print("error con el servicio")
                        }else{
                        
                        self.alert(title: "¡Aviso!", message: "Verifique que todos los datos sean válidos para continuar.", cancel: "OK")
                        print("error con el servicio")
                        }
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
        }
        
      
        
        
        
        
    }
    
    
    
    //metodos servicios de nosotros
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print (response)
        if endpoint == "Wallet/1/3/en/saveAptoInfo"{
            
            let code = response["idError"] as! Int
            
            if code == 0{
                
                DataUser.APTOCONNECT = nil
                   self.performSegue(withIdentifier: "PersonalInfoFinishSegue", sender: nil)
                
            }else{
                
                     self.alert(title: "¡Aviso!", message: "Error desconocido contacte a un administrador.", cancel: "OK")
            }
            
        }
        
        
    }
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        
        if endpoint == "Wallet/1/3/en/saveAptoInfo"{
            DataUser.APTOCONNECT = "1"
        }
        
         self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
    }
    
   
}









