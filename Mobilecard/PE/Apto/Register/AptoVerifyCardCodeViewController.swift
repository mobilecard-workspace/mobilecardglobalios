


import UIKit
import AVFoundation
import Alamofire




class AptoVerifyCardCodeViewController:UIViewController,ServicesDelegate{

  
var pan = ""
    var cvv = ""
    var vigencia = ""
    
    @IBOutlet weak var backButton: UIBarButtonItem!

  
    

    
    let services = Services()
    
     var attributedString = NSMutableAttributedString(string:"")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.services.delegate = self
   
        
      
        
   
      
        

        
        
        
        
    }
    
    
    
    
    
   
    
   
    
  

    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
     
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MainMenu_Ctrl.self) {
                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
        
    }
    
    

 //metodo que se ejeucta al presionar el botón de verificación
    @IBAction func verifyAction(_ sender: UIButton) {
        self.getInfoCard()
    }
    
    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    
  
        
    }
    
    
   
    
    //metodo que obtiene la informacion de la tarjeta
    func getInfoCard(){
        
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
        
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Obteniendo", animated: true)
        
        Alamofire.request("\(ServicesLinks.aptoVaul)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                  let state = dic!["state"] as! String
                        let ordered = dic!["ordered_status"] as! String
                        
                        if state == "created" && ordered == "ordered"{
                            
                            self.alert(title: "¡Aviso!", message: "La cuenta aun no esta activada, al recibir su tarjeta debera llamar para su activación , una vez hecho ese proceso intente nuevamente.", cancel: "OK")
                            
                        }else{
                        
                            
                            //obtenemos la informacion sensible de apto
                        self.obtainCardSensibleInformation()
                        
                      
                        
                        
                        
                        
                        
                    }//termina validacion de si esta en verificacion
                        
                    }else{
                        
                        
                        
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                    
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                
            }
            
        }
    }
  
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        if endpoint == "Wallet/1/addAptoCard"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                self.view.isUserInteractionEnabled = true
                self.performSegue(withIdentifier: "loginSegue", sender: nil)
                
            }else{
                
               
                self.view.isUserInteractionEnabled = true
                let mensajeError = response["mensajeError"] as! String
                
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
                
            }
            
            
        }
        
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "Debes contar con una conexión de internet estable para continuar.", cancel: "OK")
    }
   
    
    
    //metodo que obtiene el pan exp y cvv
    func obtainCardSensibleInformation(){
        
        //header
        let header  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Api-Key":"Bearer \(ServicesLinks.aptoApiKey)","Authorization":"Bearer \(DataUser.APTOTOKEN!)"]
        
        
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: "Obteniendo", animated: true)
        
        Alamofire.request("\(ServicesLinks.aptoVaul)/v1/user/accounts/\(DataUser.APTOACCOUNTID!)/details", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if(response.result.isSuccess){
                
                
                print("este es el response \(response.result.value)")
                
                
                let dic = response.result.value  as? NSDictionary
                
                if dic != nil{
                    
                    if dic!.count < 1{
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        return
                    }
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    print(dic)
                    
                    
                    let code = dic!["code"] as? Int
                    
                    if code == nil{
                        
                            
                            self.pan = dic!["pan"] as! String
                            self.cvv = dic!["cvv"] as! String
                            self.vigencia = dic!["expiration"] as! String
                            DataUser.APTOCARD = self.pan
                            DataUser.APTOCODIGO = self.cvv
                            DataUser.APTOVIGENCIA = self.vigencia
                            
                            let object = pootEngine()
                            let pan = object.encryptJSONString(self.pan, withPassword: nil)
                            let cvv = object.encryptJSONString(self.cvv, withPassword: nil)
                            let vigencia = object.encryptJSONString(self.vigencia, withPassword: nil)
                            
                            
                            let headers  : HTTPHeaders = [:]
                            let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,"pan":pan!,"codigo":cvv!,"vigencia":vigencia!,"account_id":DataUser.APTOACCOUNTID!,"nombre":DataUser.APTONAMEPERSON!]
                            
                            print("estos son los parametros \(params)")
                            self.services.send(type: .POST, url:"Wallet/1/addAptoCard", params: params, header: headers, message: "Cargando",animate: true)
                            
                            
                            
                      
                        
                    }else{
                        
                        
                        
                        self.alert(title: "¡Aviso!", message: "Error de servidor interno.", cancel: "OK")
                        
                        print("error con el servicio")
                        
                    }
                    
                    
                }else{
                    
                    //  let dicArray = response.result.value  as! NSArray
                    
                    
                    //respondemos el servicio
                    print("RESPONSE:")
                    //  print(dic)
                    
                }
                
            }else{
                //no se pudo conectar con el servicio
                
                DispatchQueue.main.async {
                    LoaderView.hide()
                    
                }
                self.alert(title: "¡Aviso!", message: "Verifica tu conexión a internet para continuar.", cancel: "OK")
                print("error")
            }
            
            DispatchQueue.main.async {
                LoaderView.hide()
                
            }
            
        }
        
    }
    
}









