//
//  PEGenerateQrController.h
//  Mobilecard
//
//  Created by Raul Mendez on 7/14/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PEGenerateQrController : UIViewController{
    
    __weak IBOutlet UIImageView *img_qr;
    __weak IBOutlet UILabel *lbl_monto;
    __weak IBOutlet UILabel *lbl_propina;
    __weak IBOutlet UILabel *lbl_comision;
    __weak IBOutlet UILabel *lbl_concepto;
    __weak IBOutlet UILabel *lbl_establecimiento;
    __weak IBOutlet UILabel *lbl_total;
}
    
    @property (weak, nonatomic) NSString *imgQr;
    @property (weak, nonatomic) NSString *establecimiento;
    @property (weak, nonatomic) NSString *comision;
    @property (weak, nonatomic) NSString *amount;
    @property (weak, nonatomic) NSString *concepto;
    @property (weak, nonatomic) NSString *propina;
    @property (weak, nonatomic) NSString *total;
    
    
@end

NS_ASSUME_NONNULL_END
