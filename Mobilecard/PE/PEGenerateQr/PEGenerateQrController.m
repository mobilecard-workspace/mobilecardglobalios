//
//  PEGenerateQrController.m
//  Mobilecard
//
//  Created by Raul Mendez on 7/14/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "PEGenerateQrController.h"

@interface PEGenerateQrController ()
    
    @end

@implementation PEGenerateQrController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
     [img_qr setImage:[self decodeBase64ToImage:_imgQr]];
     [lbl_monto setText:_amount];
     [lbl_propina setText:_propina];
     [lbl_comision setText:_comision];
     [lbl_concepto setText:_concepto];
     [lbl_establecimiento setText:[NSString stringWithFormat:@"%@", _establecimiento]];
     [lbl_total setText:[self returnTotal]];
    
}
- (IBAction)back_Action:(id)sender {
    
    //   [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
    
    - (IBAction)continue_Action:(id)sender {
        
        //   [self.navigationController popViewControllerAnimated:YES];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
    
    -(UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
        return [UIImage imageWithData:data];
    }
-(NSString *) returnTotal{
    
        NSArray *array = [_amount componentsSeparatedByString:@"/"];
    
    NSString *amountCorrect = array[1];
 
    
    
    float total = [amountCorrect doubleValue]+[_comision doubleValue] + [_propina doubleValue];
    return [NSString stringWithFormat:@"%f",total];
}
    @end
