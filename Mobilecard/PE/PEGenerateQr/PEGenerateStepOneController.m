//
//  PEGenerateStepOneController.m
//  Mobilecard
//
//  Created by Raul Mendez on 7/14/19.
//  Copyright © 2019 David Poot. All rights reserved.
//
#define idApp 1
#import "PEGenerateStepOneController.h"

@interface PEGenerateStepOneController (){
    
      pootEngine *GenerateQrManager;
}

@end

@implementation PEGenerateStepOneController
@synthesize amount;
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    amount =[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"Amount"];//Amount
    
}
- (IBAction)back_Action:(id)sender {
    
    //   [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
    
    - (IBAction)continue_Action:(id)sender {
        
        
        if ([_conceptoText.text isEqualToString:@""]) {
            [self Alercontroller:@"Ingresa un concepto"];
            return;
        }
       // if ([_propinaText.text isEqualToString:@""]) {
         //   [self Alercontroller:@"Ingresa propina"];
         //   return;
      //  }
      //
        [self CallwsEndpointGenerateQr];
    }
    -(void)Alercontroller:(NSString * )message {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Aviso"
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];

        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    -(void)CallwsEndpointGenerateQr{
        !GenerateQrManager?GenerateQrManager = [[pootEngine alloc]init]:nil;
        [GenerateQrManager setDelegate:self];
        [GenerateQrManager setShowComments:developing];
        
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
           
            typeIdUser=userInfo[kUserIDKey];
        }else{
            typeIdUser= userData[@"idEstablecimiento"];  /////// FALTA ID COMMERCE
          
        }
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        /////// FECHA DE EXPIRACION PARA GENERAR QR Raul Mendez
        //[params setObject:@"2019-07-31" forKey:@"expirationDate"];

        [params setObject:[self returndate] forKey:@"expirationDate"];
        [params setObject:_conceptoText.text forKey:@"description"];
        [params setObject:[self returnComision]  forKey:@"comision"];
        [params setObject:[amount stringByReplacingOccurrencesOfString:@"S\/"
                                                            withString:@""] forKey:@"amount"];
        
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        
        [GenerateQrManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/VisaQRWalletService/%d/%@/%@/-1/%@/payment/generate/qr", simpleServerURL,idApp,[countryInfo objectForKey:kIDKey], NSLocalizedString(@"lang", nil),typeIdUser] withPost:JSONString];
        
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }
    - (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
    {
        [self unLockView];
        
        if (manager == GenerateQrManager) {
            NSDictionary *response = (NSDictionary*)json;
            if ([response[kIDError] intValue] == 0) {
                
                 qrTag = response[@"qrTag"];
                 qrBase64 = response[@"qrBase64"];
                
                 [self performSegueWithIdentifier:@"detail_qr" sender:nil];
            }
        }
    }
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        if([[segue identifier] isEqualToString:@"detail_qr"]){
            PEGenerateQrController *next = (PEGenerateQrController *)[segue destinationViewController];
            [next setAmount:amount];
            [next setConcepto:_conceptoText.text];
            if ([_propinaText.text  isEqual: @""]){
                [next setPropina:@"0.0"];
            }else{
            [next setPropina:_propinaText.text];
            }
            [next setImgQr:qrBase64];
            [next setEstablecimiento:typeIdUser];
            [next setComision:[self returnComision]];
        }
    }

-(NSString *) returndate{
    NSDate* date = [NSDate date];
    
    NSDateComponents* comps = [[NSDateComponents alloc]init];
    comps.day = 2;
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDate* yesterday = [calendar dateByAddingComponents:comps toDate:date options:nil];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString* dateString = [formatter stringFromDate:yesterday];
    
    
    return dateString;
}

-(NSString *) returnComision{
    float total_comicion;
    if (DataUser.COMISION == nil) {
        total_comicion =  0.018;
    }else{
         total_comicion = ([amount intValue]*[DataUser.COMISION intValue]) + ([amount intValue]* 0.018);
    }
    return [NSString stringWithFormat:@"%f",total_comicion];
}
@end
