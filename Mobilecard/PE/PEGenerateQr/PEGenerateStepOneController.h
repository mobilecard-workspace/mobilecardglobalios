//
//  PEGenerateStepOneController.h
//  Mobilecard
//
//  Created by Raul Mendez on 7/14/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PEGenerateQrController.h"
#import "mT_commonController.h"
#import <Mobilecard-Swift.h>


NS_ASSUME_NONNULL_BEGIN

@interface PEGenerateStepOneController : mT_commonController<pootEngineDelegate>{ 
    
    NSString *qrTag;
    NSString *qrBase64;
    NSString *typeIdUser;
    
}
    
    @property (weak, nonatomic) NSString *amount;
    @property (weak, nonatomic) IBOutlet UITextField *conceptoText;
    @property (weak, nonatomic) IBOutlet UITextField *propinaText;
@end

NS_ASSUME_NONNULL_END
