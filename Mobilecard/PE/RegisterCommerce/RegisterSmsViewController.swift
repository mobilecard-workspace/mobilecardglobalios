


import UIKit
import AVFoundation
import Alamofire




class RegisterSmsViewController:UIViewController,UITextFieldDelegate,MyTextFieldDelegate,ServicesDelegate{

//referencias de pantalla
    @IBOutlet weak var viewSms: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var reSendCodeButton: UIButton!
    @IBOutlet weak var phoneTextField: UITextField!
    
    // si es 0 viene de usuario si es 1 viene de registro de comercio si es 2 viene de registro comercio PERU 3 si viene de registro comercio welcome pack
    @objc var isFrom = 0
    //esta variable guarda el serialQR si viene de welcome pack negocio mexico isFrom 3
    @objc var serialQR = ""
    
    @IBOutlet weak var verifyOne: MyTextField!
    @IBOutlet weak var verifyTwo: MyTextField!
    @IBOutlet weak var verifyThree: MyTextField!
    @IBOutlet weak var verifyFour: MyTextField!
    @IBOutlet weak var verifyFive: MyTextField!
    @IBOutlet weak var verifySix: MyTextField!
    
    //variables que controlan el contador de segundos para poder reenviar el código
    var seconds = 10
    var timer = Timer()
    //variable que guarda el numero de caracteres que tiene el telefono dependiendo el pais
    var numberCharactersPhone = 10
    
    //objeto que gestiona los servicios
    let service = Services()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegamos los servicios
        self.service.delegate = self
        
        
        //targets
        self.phoneTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
         self.verifyOne.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
         self.verifyTwo.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
         self.verifyThree.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
         self.verifyFour.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
         self.verifyFive.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
         self.verifySix.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
      //delegados
        self.phoneTextField.delegate = self
        self.verifyOne.delegate = self
        self.verifyTwo.delegate = self
        self.verifyThree.delegate = self
        self.verifyFour.delegate = self
        self.verifyFive.delegate = self
        self.verifySix.delegate = self
        self.verifyOne.myTextFieldDelegate = self
        self.verifyTwo.myTextFieldDelegate = self
        self.verifyThree.myTextFieldDelegate = self
        self.verifyFour.myTextFieldDelegate = self
        self.verifyFive.myTextFieldDelegate = self
        self.verifySix.myTextFieldDelegate = self
        
        //asigna el numero de caracteres de telefono por pais
        if DataUser.COUNTRY != nil{
            
            switch DataUser.COUNTRY{
                
                
            case "CO":
                self.numberCharactersPhone = 10
                
                break;
                
            case "MX":
                self.numberCharactersPhone = 10
                break
                
                
            case "US":
                self.numberCharactersPhone = 10
                break
                
            case "PE":
                self.numberCharactersPhone = 9
                break
                
            default:
                self.numberCharactersPhone = 10
                break
            }
            
            
        }

        
     
        
      
        
        
       

        
        
    }
    
    //metodo que checa en todo momento el contenido del textfield
    @objc func textFieldDidChange(_ textField: UITextField) {
        
         switch textField.tag {
         case 0:
        if textField.text!.count >= self.numberCharactersPhone{
            
            //realizamos accion
            //codigo para cuando ya mande el sms
            self.sendSmsService()
            
            textField.endEditing(false)
        }
            break
            
            
         case 1:
            
            if textField.text!.count >= 1 {
                self.verifyTwo.becomeFirstResponder()
            }
            
            break
            
         case 2:
            
            
           if textField.text!.count >= 1 {
            
                self.verifyThree.becomeFirstResponder()
            }
           
            break
            
         case 3:
          if textField.text!.count >= 1 {
            
                self.verifyFour.becomeFirstResponder()
            
            }
          
            break
            
         case 4:
             if textField.text!.count >= 1 {
                self.verifyFive.becomeFirstResponder()
                
         
            }
             
            break
            
         case 5:
           if textField.text!.count >= 1 {
                self.verifySix.becomeFirstResponder()
            }
           
            break
            
         case 6:
           if textField.text!.count >= 1 {
            
                print("termino la accion")
                textField.endEditing(true)
            
            if self.verifyOne.text == "" || self.verifyTwo.text == "" || self.verifyThree.text == "" || self.verifyFour.text == "" || self.verifyFive.text == "" || self.verifySix.text == ""{
                self.alert(title: "¡Aviso!", message: "Debes poner el código completo para continuar.", cancel: "OK")
                return;
            }else{
                
               
                
                //todo correcto procedemos a llamar el servicio de verificación
                
            
                let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*"]
                let code = "\(self.verifyOne.text!)\(self.verifyTwo.text!)\(self.verifyThree.text!)\(self.verifyFour.text!)\(self.verifyFive.text!)\(self.verifySix.text!)"
               
                
                if let imei = DataUser.IMEI {
                    let params : [String : Any] = ["code":code,"phoneNumber":self.phoneTextField.text!,"imei":imei]
                
                
                if  let countryID = DataUser.COUNTRYID{
                
                self.service.send(type: .POST, url: "sms/1/\(countryID)/\(NSLocalizedString("lang", comment: ""))/activate/code", params: params, header: headers, message: "Enviando",animate: false)
                    }
                }
                
            }
            
            
            }
            break
            
            
            
         default:
            break
        }
    }
    
    //metodo delegado cuando empieza a editar el textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField.tag {
            
        case 0 :
            textField.text = ""
            //realizamos accion
            //codigo para cuando ya mande el sms
            self.viewSms.isHidden = true
            self.viewSms.isUserInteractionEnabled = false
            timer.invalidate()
            self.timeLabel.text = "10"
            self.seconds = 10
            self.verifyOne.text = ""
            self.verifyTwo.text = ""
            self.verifyThree.text = ""
            self.verifyFour.text = ""
            self.verifyFive.text = ""
            self.verifySix.text = ""
             self.reSendCodeButton.isEnabled = false
            break
            
        case 1,2,3,4,5,6:
            textField.text = ""
            break
            
       
            
      
            
            
            
        default:
            break
        }
        
    }
    
    //metodo que se ejecuta al presionar el boton de back en el keyboard
    func textFieldDidEnterBackspace(_ textField: MyTextField) {
        
        switch textField.tag {
            
        case 0:
            break
            
        case 1:
            break
            
        case 2:
              self.verifyOne.becomeFirstResponder()
            break
            
        case 3:
              self.verifyTwo.becomeFirstResponder()
            break
            
        case 4:
              self.verifyThree.becomeFirstResponder()
            break
            
        case 5:
              self.verifyFour.becomeFirstResponder()
            break
            
        case 6:
              self.verifyFive.becomeFirstResponder()
            
            break
            
        default:
            break
            
            
            
        }
    }
    
    //metodo delegado del textfield para que no se puedan poner mas caracteres de los permitidos
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //caso 0 el del numero telefonico del 1 al 6 son los numeros de verificación
        switch textField.tag {
        case 0:
        if ((textField.text?.count)! + (string.count - range.length)) > self.numberCharactersPhone {
            
            return false
            
        }

        break
            
            
        case 1:
            
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
              
                return false
            }
            
            break
            
        case 2:
            
      
            
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
  
                return false
            }
            break
            
        case 3:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 4:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {

                
                return false
            }
            break
            
        case 5:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                

                return false
            }
            break
            
        case 6:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
             
                return false
            }
            break
            
            
            
        default:
            
            break
            
        }
        
        
        return true
      
    }
    


    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
     
        
       self.navigationController?
        .popViewController(animated: true)
          
    }
    
    
    //metodo que actualiza el tiempo
    @objc func updateTimer(){
        
        seconds = seconds - 1
        
        
        
       self.timeLabel.text = String(seconds)
        
        //esto se ejecuta cuando el tiempo es 0
        if seconds == 0 {
            self.timeLabel.text = ""
          self.reSendCodeButton.isEnabled = true
            timer.invalidate()
        }
    }
    
   
   
   
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
      
        if segue.identifier == "mailSegue"{
            
            let destinoViewController = segue.destination as! RegisterEmailViewController
            destinoViewController.isFrom = self.isFrom
            destinoViewController.telefono = self.phoneTextField.text!
            
            
        }
    
        
        
    }
    
    
    //metodo que se ejecuta al presionar el botón de re enviar código
    @IBAction func reSendCodeAction(_ sender: UIButton) {
        
        
        //se ejecuta cuando se renvia el sms con exito
        self.verifyOne.text = ""
        self.verifyTwo.text = ""
        self.verifyThree.text = ""
        self.verifyFour.text = ""
        self.verifyFive.text = ""
        self.verifySix.text = ""
        self.reSendCodeButton.isEnabled = false
        self.timeLabel.text = "10"
        self.seconds = 10
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        self.sendSmsService()
        //aqui termina
        
    }
    
    //metodo que llama al servicio para mandar el sms
    func sendSmsService(){
        
        
      
        
        if let imei = DataUser.IMEI , let countryID = DataUser.COUNTRYID{
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*"]
            var params : [String : Any] = ["phoneNumber":self.phoneTextField.text!,"imei":imei]
        
            if self.isFrom == 0{
                params["tipo"] = "USUARIO"
            }else{
                params["tipo"] = "COMERCIO"
            }
            
            
            print("")
       self.service.send(type: .POST, url: "sms/1/\(countryID)/\(NSLocalizedString("lang", comment: ""))/activation/code", params: params, header: headers, message: "Enviando",animate: false)
            
            
        }else{
            
            self.alert(title: "¡Aviso!", message: "No es posible obtener el imei de tu dispositivo. Es necesario para continuar , por favor retira cualquier reestricción a la app.", cancel: "OK")
        }
    }
    
    //metodos delegados servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
        if  type == .POST && endpoint == "sms/1/\(DataUser.COUNTRYID!)/\(NSLocalizedString("lang", comment: ""))/activate/code"{
            
            let code = response["code"] as! Int
            
            if code == 1{
                
            DataUser.PHONE_TEMP_REGISTER = self.phoneTextField.text!
                
                //si es negocio mexico
                if self.isFrom == 1{
                    //no mandamos al correo
                    
                    let storyboard = UIStoryboard(name: "register_Commerce", bundle: nil)
                                      let vc = storyboard.instantiateViewController(withIdentifier: "registerCommercee") as! Register_Commerce_Step10
                    vc._telefono = self.phoneTextField.text!
                                     
                                     
                     
                                      self.navigationController?.pushViewController(vc, animated: true)
                    
                    return;
                    
                    
                }else if self.isFrom == 3{
                    // es negocio mexico welcome pack
                    
                    let storyboard = UIStoryboard(name: "register_Commerce", bundle: nil)
                                                     let vc = storyboard.instantiateViewController(withIdentifier: "registerCommercee") as! Register_Commerce_Step10
                                   vc._telefono = self.phoneTextField.text!
                    vc._isWelcomePack = "yes"
                    vc._serialQR = self.serialQR
                                                    
                                    
                                                     self.navigationController?.pushViewController(vc, animated: true)
                                   
                                   return;
                    
                    
                }else if self.isFrom == 0{
                    
                    let storyboard = UIStoryboard(name: "register_look_and_feel", bundle: nil)
                                           let vc = storyboard.instantiateViewController(withIdentifier: "Register_Step2") as! RegisterNew_Step2
                                           vc._telefono = self.phoneTextField.text!
                                          
                 
                    self.navigationController?.pushViewController(vc, animated: true)
                                             return
                    
                    
                }else if self.isFrom == 2{
                    //peru comercio
                    
                    let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                                              
                                              let vc = storyboard.instantiateViewController(withIdentifier: "peRegisterIden") as! PERegisterCommerceViewController
                    vc.telefono = self.phoneTextField.text! as String
                                            
                                              self.navigationController?.pushViewController(vc, animated: true)
                    return;
                    
                }
                
             self.performSegue(withIdentifier: "mailSegue", sender: nil)
                
            }else{
                let mensaje = response["message"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                self.verifyOne.text = ""
                self.verifyTwo.text = ""
                self.verifyThree.text = ""
                self.verifyFour.text = ""
                self.verifyFive.text = ""
                self.verifySix.text = ""
            }
            
        }
        

        print(response)
        if  type == .POST && endpoint == "sms/1/\(DataUser.COUNTRYID!)/\(NSLocalizedString("lang", comment: ""))/activation/code"{
            
            
            let code = response["code"] as! Int
            
            if code == 1{
                
                print("si mando el mensaje")
                
               
                
                //si todo es un exito de mandar el mensaje
                self.viewSms.isHidden = false
                self.viewSms.isUserInteractionEnabled = true
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
                //termina
                
            }else if code == 2{
                //el numero ya fue validado
                
               // if let numberTemp = DataUser.PHONE_TEMP_REGISTER{
                    
                  //  if numberTemp == self.phoneTextField.text!{
                    
                //si es comercio mexico
                if self.isFrom == 1{
                                   //no mandamos al correo
                                   
                                   let storyboard = UIStoryboard(name: "register_Commerce", bundle: nil)
                                                     let vc = storyboard.instantiateViewController(withIdentifier: "registerCommercee") as! Register_Commerce_Step10
                                   vc._telefono = self.phoneTextField.text! 
                                                 
                    
                                                    
                                  
                                                     self.navigationController?.pushViewController(vc, animated: true)
                                   
                                   return;
                                   
                    //termina si es comercio Mexico
                }else if self.isFrom == 3{
                    //comercio welcome pack
                    
                    let storyboard = UIStoryboard(name: "register_Commerce", bundle: nil)
                                                                       let vc = storyboard.instantiateViewController(withIdentifier: "registerCommercee") as! Register_Commerce_Step10
                                                     vc._telefono = self.phoneTextField.text!
                    vc._isWelcomePack = "yes"
                    vc._serialQR = self.serialQR
                                                                   
                                      
                                                                      
                                                    
                                                                       self.navigationController?.pushViewController(vc, animated: true)
                                                     
                                                     return;
                    
                    
                }else if self.isFrom == 0{
                                   
                                   let storyboard = UIStoryboard(name: "register_look_and_feel", bundle: nil)
                                                          let vc = storyboard.instantiateViewController(withIdentifier: "Register_Step2") as! RegisterNew_Step2
                   
                   
                    vc._telefono =  self.phoneTextField.text!
                    
                    
                                    
                    
                      
                      
                                                          self.navigationController?.pushViewController(vc, animated: true)
                                                            return
                                   
                                   
                               }else if self.isFrom == 2{
                                   //peru comercio
                                   
                                   let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                                                             
                                                             let vc = storyboard.instantiateViewController(withIdentifier: "peRegisterIden") as! PERegisterCommerceViewController
                                   vc.telefono = self.phoneTextField.text! as String
                                                           
                                                             self.navigationController?.pushViewController(vc, animated: true)
                                   return;
                                   
                               }
                
                
                
                
                        self.performSegue(withIdentifier: "mailSegue", sender: nil)
                        
                //    }else{
                //        let mensaje = response["message"] as! String
                    //    self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                        
               //     }
                    
                    
              //  }else{
              //      let mensaje = response["message"] as! String
                  //  self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
              //  }
                
                
                
            }else{
                let mensaje = response["message"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
           
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        
    }
    
   
}








