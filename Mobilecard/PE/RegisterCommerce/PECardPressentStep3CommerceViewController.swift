


import UIKit
import AVFoundation
import TouchDraw
import Alamofire



class PECardPressentStep3CommerceViewController:UIViewController,ServicesDelegate{

    //objeto que recibimos de la vista pasada
    var cardPressent : CardPressent!
    
    //componentes para pintar
    @IBOutlet weak var drawView: UIView!
    @IBOutlet weak var imageBack: UIImageView!
    @IBOutlet weak var touchDraw: TouchDrawView!

    @IBOutlet weak var colorBlackButton: UIButton!
    @IBOutlet weak var eraseButton: UIButton!
    
    
    
    //variables globales
    var accountId = ""
    var token = ""
    
    //objeto que gestiona los servicios
    let service = Services()
    
    //parametros globales que vamos a encriptar en el getToken y luego mandar en el otro servicio
    var params: [String : Any]!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegamos los servicios
        self.service.delegate = self
       
        
        //hacemos circulo el color negro
        self.colorBlackButton.layer.masksToBounds = true
        self.colorBlackButton.layer.cornerRadius = self.colorBlackButton.bounds.size.width / 2.0
        
        
        //se pone por defecto en color negro y tamaño 2
        self.touchDraw.setColor(UIColor.black)
        self.touchDraw.setWidth(2.0)
      
        
        
        
        
        
        

     

        
     
        
      
        
        
       

        
        
    }
    
    
    //metodo que se ejecuta al presionar el botón de color negro
    @IBAction func blackColorAction(_ sender: UIButton) {
    
        //pintamos de negro
        self.touchDraw.setColor(UIColor.black)
  
    
    }
    
    
    //metodo que se ejecuta al presionar la goma de borrado
    @IBAction func eraseAction(_ sender: UIButton) {
        
        self.touchDraw.clearDrawing()
        
        
    }
    
//metodo de que se ejecuta al presionar el boton de continuar
    @IBAction func continueAction(_ sender: UIButton) {
   
        
        
       //convertimos el area pintada en imagen
        UIGraphicsBeginImageContextWithOptions(drawView.bounds.size, false, 0);
        self.drawView.drawHierarchy(in: drawView.bounds, afterScreenUpdates: true)
        let imageFirm = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        
        self.cardPressent.firma = imageFirm
        let imageData:NSData = imageFirm!.pngData()! as NSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
       self.cardPressent.firmaBase64 = strBase64
        
       
        
        
        
        
        //objeto encargado de encriptar tarjetas cvv fecha
        let object = pootEngine()
        let cardEncripted = object.encryptJSONString(self.cardPressent.numeroTarjeta!, withPassword: nil)
        
        
        self.cardPressent.numeroTarjeta = cardEncripted
        
        let vigencia = "\(self.cardPressent.mes!)/\(self.cardPressent.anio!)"
        
        let vigenciaEncripted = object.encryptJSONString(vigencia, withPassword: nil)
        let ctEncripted = object.encryptJSONString(self.cardPressent.cvv!, withPassword: nil)
        
        
        self.cardPressent.cvv = ctEncripted!
       
        
        print("ESTE ES EL NUMERO DE TARJETA ENCRIPTADA \(self.cardPressent.numeroTarjeta!)")
        self.params  = ["concept":self.cardPressent.concepto!,
                                       "idUser": "0",
                                       "idCard":0,
                                       "establecimientoId": DataUser.USERIDD!,
                                       "amount":Double(self.cardPressent.cantidad!)!,
                                       "comision":Double(self.cardPressent.comision!)!,
                                       "referenciaNeg":"",
                                       "propina": self.cardPressent.propina!,
                                       "tarjeta":self.cardPressent.numeroTarjeta!,
            "lat":Double(self.cardPressent.lat!)!,
            "lon":Double(self.cardPressent.long!)!,
            "msi":0,
            "vigencia":vigenciaEncripted!,
            "ct":self.cardPressent.cvv!,
            "tipoTarjeta":self.cardPressent.tipoTarjeta!,
            "firma":self.cardPressent.firmaBase64!,
            "idBitacora":0,
            "imei":DataUser.IMEI!,
            "modelo":"",
            "qrBase64":"",
            "software":"28",
            "nombre":self.cardPressent.nombreTarjetaHabitante!,
            "apellido":self.cardPressent.apellidosTarjetaHabitante!,
            "celular":self.cardPressent.phone,
            "email":self.cardPressent.mail]
        
        
      let autorization = object.encryptJSON(withParams: self.params	, withPassword: "SJI74cm2dF")
   
  
        

    
        
       
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":autorization!,"Profile":"asdasdasdasd"]
        let params : [String : Any] = [:]
        
        self.service.send(type: .POST, url: ServicesLinks.PETokenizer, params: params, header: headers, message: "Enviando",animate: false)
        

    
    }
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
    _ = self.navigationController?.popViewController(animated: true)
        
        
    }
    
    
    
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "voucherSegue"{
            
            
            //nos llevamos nuestro objeto a la siguiente clase
            let destinoViewController = segue.destination as! PECardPressentStep4CommerceViewController
            
            destinoViewController.cardPressent = self.cardPressent
            
            
            
        }
        
        
        if segue.identifier ==   "voucherFailedSegue"{
        
        //nos llevamos nuestro objeto a la siguiente clase
        let destinoViewController = segue.destination as! PECardPressentStep4FailedCommerceViewController
        
        destinoViewController.cardPressent = self.cardPressent
        
        
        
    }
    
        
    }
    
    
    
 
 
   
    
    //metodos delegados de servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
        
        
        
        print(response)
  
        
        if endpoint == "\(ServicesLinks.PEPayCardPresent)\(self.accountId)\(ServicesLinks.PEPayCardPresentComplex)"{
       
            
            
           
            
            if response["code"] is NSNull{
              
                 self.performSegue(withIdentifier: "voucherFailedSegue", sender: nil)
                return;
            }
            
            
            
            
            let code = response["code"] as! Int
            
            
            //fue un exito la transaccion
            if code == 0{
                
                
                self.performSegue(withIdentifier: "voucherSegue", sender: nil)
                
                let authNumber = response["authNumber"] as! String
                let dateTime = response["dateTime"] as! CLong
                let idTransaccion = response["idTransaccion"] as! Int
                
                self.cardPressent.authNumber = authNumber
                self.cardPressent.dateTime = dateTime
                self.cardPressent.idTransaccion = idTransaccion
                
                
            }else{
               //la transaccion fallo
            
                 self.performSegue(withIdentifier: "voucherFailedSegue", sender: nil)
               
                
            }
        
        }
        
        
        if endpoint == ServicesLinks.PETokenizer{
            
          
            self.accountId = response["accountId"] as! String
            self.token = response["token"] as! String
            
          
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"\(self.token)","Profile":"asdasdasdasd"]
         
            
            print("esto mando \(self.params!)")
            
            
        //    self.service.send(type: .POST, url: "CuantoTeDebo/1/4/es/NEGOCIO/\(self.accountId)/pagoBP", params: self.params, header: headers, message: "Enviando")
             self.service.send(type: .POST, url: "\(ServicesLinks.PEPayCardPresent)\(self.accountId)\(ServicesLinks.PEPayCardPresentComplex)", params: self.params, header: headers, message: "Enviando",animate: true)
 
        }
        
        
    }
    
    //por si llega un array
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible conectarse con el servidor, favor de revisar su conexión a internet.", cancel: "OK")
    }
    
    
    
}








