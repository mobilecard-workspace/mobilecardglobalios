


import UIKit
import AVFoundation
import Alamofire




class PrecioCOViewController:UIViewController{

  

    var producto : PrepagoProductoCO!
    //referencias de elementos graficos
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fiveButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var eightButton: UIButton!
    @IBOutlet weak var nineButton: UIButton!
    @IBOutlet weak var ceroButton: UIButton!
    @IBOutlet weak var eraseButton: UIButton!
    
    @IBOutlet weak var conceptButton: UIButton!
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    
   
    
    var amountGlobal = "0.0"
    
    
    
    
    
    

    
    // formato para los botones
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24.0),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    var attributedString = NSMutableAttributedString(string:"")
    
    var valueString = "$"
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
       
       
        
       
        
        
        
        
        
        //agregamos los underline de los botones
        self.attrs[NSAttributedString.Key.font] = self.oneButton.titleLabel?.font
        self.underlineButton(button: oneButton)
        self.underlineButton(button: twoButton)
        self.underlineButton(button: threeButton)
        self.underlineButton(button: fourButton)
        self.underlineButton(button: fiveButton)
        self.underlineButton(button: sixButton)
        self.underlineButton(button: sevenButton)
        self.underlineButton(button: eightButton)
        self.underlineButton(button: nineButton)
        self.underlineButton(button: ceroButton)
        
       
        
        

       
        
     
        
      
        
        
       

        
        
    }
    
    //metodo que se ejecuta al presionar el botón de regresar de escaneo qr
    @objc func buttonAction(){
        print("se presiono el botón")
        
    }
    
   
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
     
        
        
    }
    

    //metodo que le pone una underline a un botón
    func underlineButton(button:UIButton){
        
      self.attributedString =  NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:button.titleLabel!.text!, attributes:attrs)
        attributedString.append(buttonTitleStr)
       
        button.setAttributedTitle(attributedString, for: .normal)
        
    }
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
      _ = self.navigationController?.popViewController(animated: true)
          
    }
    
    
    
   
   
    //metodo que se ejecuta al presionar algun numero
    @IBAction func numberAction(_ sender: UIButton) {
        
        
        //opcion de borrado
        if sender.tag == 10{
            
            if self.totalAmountLabel.text != "$"{
                var characters = Array(self.totalAmountLabel.text!)
                 characters.remove(at: characters.count-1)
                if characters.count > 1{
                   // self.totalAmountLabel.text = "S/"
                   // self.valueString = "S/"
                  //  return;
                //}else{
                var stringNew = ""
                var stringNoPunto = ""
                for char in characters{
                    if char != "."{
                        stringNoPunto .append(char)
                    }
                    stringNew.append(char)
                }
              //  self.totalAmountLabel.text = stringNew
                    var amount = ""
                    let arrayCharacters = Array(stringNew)
                    
                    for position in 1...arrayCharacters.count-1{
                        amount.append(arrayCharacters[position])
                        
                    }
                    
                    var amountTwo = ""
                    let arrayCharactersTwo = Array(stringNoPunto)
                    
                    for position in 1...arrayCharactersTwo.count-1{
                        amountTwo.append(arrayCharactersTwo[position])
                        
                    }
                    
                    print("esto vale amounttwo \(amountTwo)")
                    if amountTwo == "00"{
                        self.totalAmountLabel.text = "$"
                        self.valueString = "$"
                        return;
                        
                    }
                    
                    let doubleStr = String(format: "%.2f", Double(amountTwo)! / 100)
                    self.amountGlobal = doubleStr
                    self.totalAmountLabel.text = "$\(self.amountGlobal)"
                    
                    var array = Array(self.valueString)
                    array.remove(at: array.count-1)
                    var stringComplete = ""
                    for a in array {
                        stringComplete.append(a)
                    }
                    self.valueString = stringComplete
                    //  self.valueString = self.totalAmountLabel.text!
                  
                  
                return;
                    
                }
                
            }else{
                  self.valueString = "$"
                self.alert(title: "¡Aviso!", message: "No hay valores que borrar actualmente.", cancel: "OK")
                return;
                
            }
            
            
            
        }
        //termina codigo de opcion de borrado
        
        
        
        //inicia codigo para numeros del 0 al 9
        if self.totalAmountLabel.text == "$"{
            
            if sender.tag == 0{
                
                self.alert(title: "¡Aviso!", message: "Agrega un valor valido.", cancel: "OK")
               return;
            }else{
                
                
                self.valueString.append(String(sender.tag))
               
                
                var amount = ""
                let arrayCharacters = Array(self.valueString)
                
                for position in 1...arrayCharacters.count-1{
                    amount.append(arrayCharacters[position])
                    
                }
                
                let doubleStr = String(format: "%.2f", Double(amount)! / 100)
                self.amountGlobal = doubleStr
                self.totalAmountLabel.text = "$\(self.amountGlobal)"
                
                
               
            }
            
            
            
            
        }else{
            
            self.valueString.append(String(sender.tag))
            var amount = ""
            let arrayCharacters = Array(self.valueString)
            
            for position in 1...arrayCharacters.count-1{
                amount.append(arrayCharacters[position])
                
            }
            
            
            let doubleStr = String(format: "%.2f", Double(amount)! / 100)
            self.amountGlobal = doubleStr
            
            self.totalAmountLabel.text = "$\(self.amountGlobal)"
           // self.totalAmountLabel.text = valueString
            
            
            
        }
        
         //termina codigo para numeros del 0 al 9
        
        
    }

    
    
    //metodo que se ejecuta al presionar el botón de recargar
    @IBAction func reloadAction(_ sender: UIButton) {
        
        
        
                     
        
        //requerimientos para avanzar en la vista
        if self.totalAmountLabel.text == "$" || self.totalAmountLabel.text == "$0.00"{
            self.alert(title: "¡Aviso!", message: "Debes agregar un monto para continuar.", cancel: "OK")
            return;
        }
        
        
        
        
        //obtenemos la cantidad a recargar en String
        var amount = ""
        let arrayCharacters = Array(self.totalAmountLabel.text!)
        
        for position in 1...arrayCharacters.count-1{
            amount.append(arrayCharacters[position])
            
        }
        
        //amount tiene la cantidad
       
        

        let array = amount.components(separatedBy: ".")
        
        
        let amountNew = array[0]
        
        
        print("esta es la cantidad \(amountNew)")
      
        if amountNew == "0"{
             self.alert(title: "¡Aviso!", message: "Debes agregar un monto valido para continuar.", cancel: "OK")
            return;
            
        }
        
      
        
         DispatchQueue.main.async {

         let story = UIStoryboard(name: "wallet", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "wallet_view") as! Wallet_Ctrl
                        
                        vc._COCards = "1"
            
            

            let dictio : NSMutableDictionary =  ["idUsuario":DataUser.USERIDD!,"idPais":"2","idApp":"1","idioma":"es","idProducto":self.producto.montos[0].id!,"amount":amountNew,"producto":"\(self.producto.nombre!)"]
                              
                        DataUser.DATA = dictio
                             
        
        
            
            
            self.navigationController?.pushViewController(vc, animated: true)



        }
        
            
            
       
    }

    
    
    func calculaComision(monto: Double)-> Double {
        
        
        let comision = (monto * Double(DataUser.COMISION!)!)
        let impuesto = (comision * 0.18)
        return   (comision + impuesto)
    }
    
    func calculaComisionFija(monto: Double)-> Double {
        let comision = (monto * 0.00)
        let impuesto = (comision * 0.18)
        return (comision + impuesto)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        
        
        
    }
 
   
    

    
    
    

    
    
   
    
    
   
}




extension UINavigationController {

  public func pushViewController(viewController: UIViewController,
                                 animated: Bool,
                                 completion: (() -> Void)?) {
    CATransaction.begin()
    CATransaction.setCompletionBlock(completion)
    pushViewController(viewController, animated: animated)
    CATransaction.commit()
  }

}



