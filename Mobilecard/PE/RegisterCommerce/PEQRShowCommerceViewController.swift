


import UIKit
import AVFoundation
import Alamofire




class PEQRShowCommerceViewController:UIViewController,ServicesDelegate{



    @IBOutlet weak var nameCompanyLabel: UILabel!
    @IBOutlet weak var imageQrImageView: UIImageView!
    @IBOutlet weak var shareStackView: UIStackView!
    
    let services = Services()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.services.delegate = self
        
        if DataUser.NAME != nil && DataUser.QREMPRESA != nil{
            
            
            self.nameCompanyLabel.text = DataUser.NAME!
            
            
            if DataUser.COUNTRYID == "4"{
            //para peru solo viene de los datos de usuario
            let qrData = Data(base64Encoded: DataUser.QREMPRESA!, options: .ignoreUnknownCharacters)
            self.imageQrImageView.image = UIImage(data: qrData!)
            
            }else{
                // si es mexico u otro pais y no lo tiene en datos de usuario lo generamos
                
                
                
                if DataUser.QREMPRESA == nil || DataUser.QREMPRESA == ""{
                let headers  : HTTPHeaders = [:]
                
                
                let params : [String : Any] = ["idEstablecimiento":DataUser.USERIDD!,"monto":"0.0","idPais":DataUser.COUNTRYID!,"typeQR":"ESTATICO"]
                print("PARAMS:\(params)")
                self.services.send(type: .POST, url: "CuantoTeDebo/1/es/generateQR", params: params, header: headers, message: "Obteniendo",animate: true)
                    
                }else{
                    let qrData = Data(base64Encoded: DataUser.QREMPRESA!, options: .ignoreUnknownCharacters)
                    self.imageQrImageView.image = UIImage(data: qrData!)
                }
                
                
                
            }
            
        }
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(PEQRShowCommerceViewController.shareAction))
        self.shareStackView.isUserInteractionEnabled = true
        self.shareStackView.addGestureRecognizer(tap)
        
        
        
    }
    
    
    

    //metodo que se ejecuta al presionar el botón de compartir
    @objc func shareAction(){
    
    
    DispatchQueue.main.async {
    
    UIGraphicsBeginImageContextWithOptions(self.imageQrImageView.bounds.size, false, 0);
    self.imageQrImageView.drawHierarchy(in: self.imageQrImageView.bounds, afterScreenUpdates: true)
    if let imageScreen = UIGraphicsGetImageFromCurrentImageContext(){
    
    UIGraphicsEndImageContext()
    let activityViewController = UIActivityViewController(activityItems: [imageScreen], applicationActivities: nil)
    
    activityViewController.excludedActivityTypes = [.addToReadingList,
    .airDrop,
    .assignToContact,
    .copyToPasteboard,
    .mail,
    .message,
    .print,
    .saveToCameraRoll,
    .postToWeibo,
    .copyToPasteboard,
    .saveToCameraRoll,
    .postToFlickr,
    .postToVimeo,
    .postToTencentWeibo,
    .postToFacebook,
    .postToTwitter
    ]
    
    self.present(activityViewController, animated: true, completion: {})
    }
    
    }
    
    }
        
        
        
        
    
    
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
    self.navigationController?.popViewController(animated: true)
    
    
    }
    
    
    
    
   
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("REPONSE : \(response)")
        if endpoint == "CuantoTeDebo/1/es/generateQR"{
            
            let code = response["idError"] as! Int
            
            if code == 0{
                //exito
                let qrBase64 = response["qrBase64"] as! String
                DataUser.QREMPRESA = qrBase64
                let qrData = Data(base64Encoded: DataUser.QREMPRESA!, options: .ignoreUnknownCharacters)
                self.imageQrImageView.image = UIImage(data: qrData!)
                
            }else{
                //fracaso
                self.alert(title: "¡Aviso!", message: "El código qr no se pudo obtener.", cancel: "OK")
                
            
            }
            
            
        }
        
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "Debes contar con una conexión de internet estable para ver el código qr.", cancel: "OK")
    }
    
 
   

   
}








