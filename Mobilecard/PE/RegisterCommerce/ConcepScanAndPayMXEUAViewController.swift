


import UIKit
import AVFoundation





class ConcepScanAndPayMXEUAViewController:UIViewController{


    //objeto que recibimos de la vista pasada
    var cardPressent : CardPressent!
    
    //de la vista pasada
    var fromView : Int = 0
    
    var scanQr : ScanQrModel!
  
    @IBOutlet weak var conceptTextField: UITextField!
    @IBOutlet weak var propineTextField: UITextField!
    
    @IBOutlet weak var continuarLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        //tarjeta presente
        if self.fromView == 0{
            self.continuarLabel.text = "CONTINUAR"
        if self.cardPressent != nil{
            
            
            if let concepto = self.cardPressent.concepto{
                
                self.conceptTextField.text = concepto
            }
            
           
            if self.cardPressent.propina! != 0.0{
                
                
                self.propineTextField.text = String(self.cardPressent.propina!)
                
            }
            
        }
        
        
        }else if self.fromView == 2{
            
            //escanear codigo
            
            if self.scanQr != nil{
                
                
                if let concepto = self.scanQr.concept{
                    
                    self.conceptTextField.text = concepto
                }
                
                
                if self.scanQr.propine! != "0.0"{
                    
                    
                    self.propineTextField.text = String(self.scanQr.propine!)
                    
                }
                
            }
            
            
            
        }
        
    }
    
    
    

    
//metodo de que se ejecuta al presionar el boton de continuar
    @IBAction func continueAction(_ sender: UIButton) {
   
        
        
        if self.conceptTextField.text != ""{
            
            if self.fromView == 0{
            
            self.cardPressent.concepto = self.conceptTextField.text!
                
                
                
            }else if self.fromView == 2{
                
                 self.scanQr.concept = self.conceptTextField.text!
            }
          
            
        }else{
            self.alert(title: "¡Aviso!", message: "Debes agregar un concepto para continuar.", cancel: "OK")
            
        }
        
        if self.propineTextField.text != ""{
            if self.fromView == 0{
         self.cardPressent.propina = Double(self.propineTextField.text!)!
                
                
              
                
            }else if self.fromView == 2{
                 self.scanQr.propine = self.propineTextField.text!
                
            }
        }else{
            //si no tiene propina le pone 0
            
            if self.fromView == 0{
                              self.cardPressent.propina = 0.0
                              
            }
            
            
        }
        
        
          self.dismiss(animated: true, completion: nil)
        
        
    
    }
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
    self.dismiss(animated: true, completion: nil)
    }
    
    
    
   
    
    
    
    
 
   

   
}








