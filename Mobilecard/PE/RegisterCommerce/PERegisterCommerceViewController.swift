


import UIKit
import AVFoundation





class PERegisterCommerceViewController:UIViewController, ScannerQrDelegate{

    //variable que guarda el qrEscaneado
    var qr = ""
    
    //llegan de la clase pasada
    var telefono = ""
    var email = ""
  
   // var ZBarReader: ZBarReaderViewController?
    
    //objeto que guarda la información
    let commerce : CompanyRegister! = CompanyRegister()
    
    
  

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.commerce.registerType = "manual"
        self.performSegue(withIdentifier: "registerQrSegue", sender: nil)
        
    }
    

    
     
    
    
    //metodo que se ejecuta al presionar el botón de registro manual
    @IBAction func manualAction(_ sender: UIButton) {
        
        self.commerce.registerType = "manual"
          self.performSegue(withIdentifier: "registerQrSegue", sender: nil)
    }
    
    
    //SE QUITO LA OPCION DE REGITRO POR CODIGO QR
    //metodo que se ejecuta al presionar el botón de registrar con qr
    @IBAction func registerQrAction(_ sender: UIButton) {
        
      /*  if (self.ZBarReader == nil) {
            self.ZBarReader = ZBarReaderViewController()
        }
        self.ZBarReader?.readerDelegate = self
        self.ZBarReader?.scanner.setSymbology(ZBAR_UPCA, config: ZBAR_CFG_ENABLE, to: 1)
        self.ZBarReader?.readerView.zoom = 1.0
        self.ZBarReader?.isModalInPopover = false
        self.ZBarReader?.showsZBarControls = false
        navigationController?.pushViewController(self.ZBarReader!, animated:true)
        */
        
        let vc = ScannerQr()
        
        vc.delegate = self
        
        
        
        self.present(vc, animated: true, completion: nil)
        
    }
    
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
       self.navigationController?.dismiss(animated: true, completion: nil)
          
    }
    
    
    
    
    //metodo delegado
    func successQr(qr: String) {
        
  
    
        
        
      let result = qr
        
            
 // ponemos si hay algun espacio como +
         let encodeString = result.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
           
            self.dismiss(animated: false, completion: nil)
            //desencriptamos
            let decryptString = mT_commonController.decryptHard(encodeString)
  
       
        if decryptString == nil{
        self.alert(title: "¡Aviso!", message: "Código QR no valido", cancel: "OK")
        return;
        }
            
            
            //si tenemos datos en el string desencriptado
            if decryptString != nil{
            
        //lo convertimos y pasamos a json nuestro string desencriptado
            let jsonData = decryptString!.data(using: .utf8)
           
            var error = ""
            if (jsonData!.count > 0){
                do {
                   
                  
                    if let jsonData = jsonData {
                       let responseID = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                          print("aaa \(responseID)")
                     error = ""
                    
                       let dictio = responseID as! NSDictionary
                        print("este es el diccionario final \(dictio)")
                        
                        let idEstablecimiento = dictio["idEstablecimiento"] as! Int
                        self.qr = String(idEstablecimiento)
                        
                        self.commerce.id = self.qr
                        self.commerce.registerType = "qr"
                        self.performSegue(withIdentifier: "registerQrSegue", sender: nil)
                        
                    }
                } catch _ {
                    error = "1"
                }
                
                if error == "1" {
                    self.alert(title: "¡Aviso!", message: "Código QR no válido", cancel: "OK")
                 
                    return
                }
                
                
                
                
            }else{
                self.alert(title: "¡Aviso!", message: "Codigo qr no compatible , intente nuevamente.", cancel: "OK")
                }

            }
            
            
        
        

    }
    
    func backActionQr() {
        print("se presiono el back")
        self.dismiss(animated: true, completion: nil)
    }


    
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "registerQrSegue"{
            
            
            //nos llevamos nuestro objeto a la siguiente clase
            let destinoViewController = segue.destination as! PERegisterCommercerTypePersonViewController
            
            destinoViewController.telefono = self.telefono
            destinoViewController.email = self.email
            
            destinoViewController.commerce = self.commerce
            
            
        }
        
        
        
        
        
        
        
    }
    
    
    
   
    
    
  
   
}








extension ZBarSymbolSet: Sequence {
    public func makeIterator() -> NSFastEnumerationIterator {
        return NSFastEnumerationIterator(self)
    }
}


