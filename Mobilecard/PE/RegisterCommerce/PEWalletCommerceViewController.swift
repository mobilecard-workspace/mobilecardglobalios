


import UIKit
import AVFoundation
import Alamofire
//import Wallet_Ctrl.h




class PEWalletCommerceViewController:UIViewController,ServicesDelegate{
    
    var Selection = 0
    
    @IBOutlet weak var activateCardUIImage: UIImageView!
    @IBOutlet weak var agregarCuentaButton: UIButton!
    @IBOutlet weak var addCardView: UIView!
    @IBOutlet weak var showCardCciView: UIView!
    @IBOutlet weak var centerConstraintViewBotTwo: NSLayoutConstraint!
    @IBOutlet weak var indicationLabel: UILabel!
    @IBOutlet weak var topContraintView: NSLayoutConstraint!
    @IBOutlet weak var botConstraintView: NSLayoutConstraint!
    @IBOutlet weak var nameCardCciLabel: UILabel!
    @IBOutlet weak var numberCardCciLabel: UILabel!
    @IBOutlet weak var favoriteStarCCIImageView: UIImageView!
    @IBOutlet weak var favoriteStarTepcaImageView: UIImageView!
    @IBOutlet weak var solicitarView: UIView!
    @IBOutlet weak var tepcaActiveView: UIView!
    @IBOutlet weak var blockButton: UIButton!
    @IBOutlet weak var reposicionButton: UIButton!
    @IBOutlet weak var saldoActualLabel: UILabel!
    @IBOutlet weak var saldoActualAmountLabel: UILabel!
    @IBOutlet weak var menuButtonImageTepca: UIImageView!
    @IBOutlet weak var solicitarLabel: UILabel!
    @IBOutlet weak var bloquearLabel: UILabel!
    @IBOutlet weak var cciTextField: UITextField!
    @IBOutlet weak var informacionBancariaButton: UIButton!
    
    
    //referencias graficas de la tarjeta
    @IBOutlet weak var numberCardLabel: UILabel!
    @IBOutlet weak var expirationLabel: UILabel!
    @IBOutlet weak var showButton: UIButton!
    @IBOutlet weak var tarjetaReversoImageView: UIImageView!
    @IBOutlet weak var numberCvvLabel: UILabel!
    @IBOutlet weak var hideButton: UIButton!
    
    @IBOutlet weak var viewCard: UIView!
    
    //obejeto que maneja los servicios
    let services = Services()
    
    //parametros que recibimos de la clase pasada validacion  para saber que tiene el usuario
    var tebcaCard : TebcaCommerceCard!
    var CciElement : CciModel!
    
    //variable que guarda el nombre de la institucion seleccionada
    var institutionCode = ""
    
    
    //arreglo que contiene el nombre de las instituciones
    
    var institutionNames : [String] = []
    var institutionCodes : [String] = []
    
    @IBOutlet weak var lineGrayImageSeparator: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        
        //inicializamos el objeto de servicios
        self.services.delegate = self
        
        
        //metodo que actualzia visualmente la vista
     self.updateViews()
      
        //seteamos la accion a la imagen activar tarjeta
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(activeCardAction(tapGestureRecognizer:)))
        self.activateCardUIImage.isUserInteractionEnabled = true
        self.activateCardUIImage.addGestureRecognizer(tapGestureRecognizer)

        
        //seteamos la accion a la view de cci
        let tapGestureRecognizerCCIView = UITapGestureRecognizer(target: self, action: #selector(optionsCCIAction(tapGestureRecognizer:)))
        
        self.showCardCciView.addGestureRecognizer(tapGestureRecognizerCCIView)

        
        //seteamos la accion a la view de tepca
        let tapGestureRecognizerTEPCAView = UITapGestureRecognizer(target: self, action: #selector(optionsTEPCAAction(tapGestureRecognizer:)))
        
        self.tepcaActiveView.addGestureRecognizer(tapGestureRecognizerTEPCAView)
        
        //es juridica no mostrar lo de arriba y centrar la vista de abajo
        if DataUser.TIPO_PERSONA! == "juridica"{
            
            self.solicitarLabel.isHidden = true
            self.topContraintView.priority = UILayoutPriority(900)
             self.botConstraintView.priority = UILayoutPriority(900)
            self.centerConstraintViewBotTwo.priority = UILayoutPriority(1000)
       self.lineGrayImageSeparator.image = nil
            self.indicationLabel.isHidden = true
            self.activateCardUIImage.isUserInteractionEnabled = false
            self.activateCardUIImage.isHidden = true
            self.tepcaActiveView.isHidden = true
            self.tepcaActiveView.isUserInteractionEnabled = false
        }
   
      
    }

    
    override func viewDidAppear(_ animated: Bool) {
        
        //metodo que actualzia visualmente la vista
        self.updateViews()
        
  
        
    }
    
    
    
    //metodo que se ejecuta al presionar activar tarjeta
    @objc func activeCardAction(tapGestureRecognizer: UITapGestureRecognizer)
    {
      
        print("se llama el metodo de activar tarjeta")
        
        self.performSegue(withIdentifier: "activateCardAction", sender: nil)
        
        
       
    }
    
    
//metodo que se ejecuta al presionar agregar cuenta
    @IBAction func agregarCuentaAction(_ sender: UIButton) {
        
        self.addCardView.isHidden = false
    }
    
   
    
    @IBAction func walletAction(_ sender: UIButton) {
        
        switch (sender as AnyObject).tag{
            
        case 0 :
            
            self.navigationController?.popViewController(animated: true)
            
            break
            
        //wallet
        case 1:
        
            
            break
            
        default:
            
            self.alertPop(title: "¡Aviso!", message: "Próximamente", cancel: "Aceptar")
            
            break
            
            
        }
        
        
    }
    
  
    
   /* //metodo que se ejecuta al presioanr el botón de escanear qr
    @IBAction func scannerQrAction(_ sender: UIButton) {
        Selection = 2
        self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)
    }
    */
    @IBAction func favoritesAction(_ sender: Any) {
        
        
        self.alertPop(title: "¡Aviso!", message: "Próximamente", cancel: "Aceptar")
        return;
        
        
        
        if verifyStatus() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewcontroller = storyboard.instantiateViewController(withIdentifier: "Favorites")
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    @IBAction func statementsAction(_ sender: Any) {
        if verifyStatus() {
            let storyboard = UIStoryboard(name: "statement", bundle: nil)
            let viewcontroller = storyboard.instantiateViewController(withIdentifier: "statement")
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    
    //metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    /*
    @IBAction func ActionButtonTarjet(_ sender: Any) {
        Selection = 0
        self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)

    }
  
    @IBAction func ActionButtonGenerateQr(_ sender: Any) {
        Selection = 1
        self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)

    }*/
    
    
    //metodo que se ejecuta al presionar el boton del menu
    @IBAction func menuShowAction(_ sender: UIBarButtonItem) {
   
        self.slideMenuController()?.openRightWithVelocity(1200)
        
        
    }
    
    
    @IBAction func historialAction(_ sender: UIButton) {
        self.alertPop(title: "¡Aviso!", message: "Próximamente", cancel: "Aceptar")
        return;
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "activateCardAction"{
            
            let destinoViewController = segue.destination as! PEWalletActiveCardViewController
            destinoViewController.cci = self.CciElement
            destinoViewController.tebcaCard = self.tebcaCard
            
            
        }
        
        //acción antes del segue
        if segue.identifier == "stepSegueObjective"{
            
            let destinoViewController = segue.destination as! PECardPressentStep1CommerceViewController
            
            destinoViewController.fromView = Selection
            
            
            
        }
        
        if segue.identifier == "stepGenerateSegueObjective"{
            
            let destinoViewController = segue.destination as! PECardPressentStep1CommerceViewController
            
            destinoViewController.fromView = Selection
            
        }
        
        
    }
 
    //metodos de verificacion de idStatus
    func  verifyStatus()-> Bool {
        ///// kUserDetailsKey  Raul mendez Validando Jumio en verifyStatus
        var response = UserDefaults.standard.dictionary(forKey:"userDetails")
        let keyJumio=response?["usr_jumio"] as? Int
        let keyStatus=response?["idUsrStatus"] as? Int
        switch keyJumio {
        case 0:
            ///////////////////// AQUI LANZAR ONBOARDING JUMIO /////////////////
             print("")
            
            return false
        case 1:
            
            switch keyStatus {
            case 99:
                ///////////////// AQUI ACTUALIZAR END POINT SESSION  /user/login /////////////////
                //////////////// CREAR UN ALERT CON LOS SIGUIENTES DATOS Y ENVIAR END POINT @" /activate  /////////////////
                
                //                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
                //                [alertMsg setTag:99];
                //                [alertMsg show];
                //                return NO;
                print("")
                
                return false
            case 100:
                //////////////// CREAR UN ALERT CON LOS SIGUIENTES DATOS  EN BOTON OK  ENVIAR A LA SIGUIENTE VISTA ESTOS DATOS
                
                //Register_Step30 *nextView = [[nav viewControllers] firstObject];
                //[nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                //                    [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                //[self.navigationController presentViewController:nav animated:YES completion:nil];
                
                /////////////////

                //  UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                print("")
                
            default:
                print("")
            }
  
        case 3:
            
            ////////// MOSTRAR VISTA DE ERROR  EJEMPLO
          //    [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
            print("")
            
            
        default:
            print("")
        }
 
     return true;
}

    
    //metodos de la gestion de servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE \(response)")
        
        if endpoint == ServicesLinks.PEBanksCodes{
            
            let code = response["idError"] as! Int
            
            
            if code != 0{
                let mensaje = response["mesajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                return;
            }
            
            let banks = response["banks"] as! NSArray
            
            
            self.institutionNames = []
            for element in banks{
                
                let dictio = element as! NSDictionary
                
                let nombre = dictio["nombre_corto"] as! String
                let code = String(dictio["id"] as! Int)
                self.institutionNames.append(nombre)
                self.institutionCodes.append(code)
                
            }
            
            ActionSheetStringPicker.show(withTitle: "Institución bancaria", rows: self.institutionNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                
                
                let select = selection as! String
                
                self.informacionBancariaButton.setTitle(select, for: .normal)
              self.informacionBancariaButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                
                
                self.institutionCode = self.institutionCodes[value]
                
            }, cancel: { (action) in
                return
            }, origin: self.informacionBancariaButton)
            
            
            
            
            
            
            
            
        }
        
        if endpoint == "TradeAccount/1/4/es/account/update"{
            UIView.transition(with: self.showCardCciView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.addCardView.isHidden = true
                self.showCardCciView.isHidden = false
                self.showCardCciView.isUserInteractionEnabled = true
                self.informacionBancariaButton.setTitle("Información bancaria", for: .normal)
                self.informacionBancariaButton.setTitleColor(#colorLiteral(red: 0.7568627451, green: 0.7568627451, blue: 0.7568627451, alpha: 1), for: .normal)
                self.institutionCode = ""
                
            })
        }
        
        
        //todos los endpoints reciben la misma respuesta
        if endpoint == "TradeAccount/1/4/es/Applyforcard" || endpoint ==  "TradeAccount/1/4/es/chgblockstatus/1?id=\(DataUser.USERIDD!)" || endpoint == "TradeAccount/1/4/es/chgblockstatus/0?id=\(DataUser.USERIDD!)" || endpoint == "TradeAccount/1/4/es/account/favorite?id=\(DataUser.USERIDD!)&type=2" || endpoint == "TradeAccount/1/4/es/account/favorite?id=\(DataUser.USERIDD!)&type=1" || endpoint == "TradeAccount/1/4/es/replacement?id=\(DataUser.USERIDD!)" || endpoint == "TradeAccount/1/4/es/account/update"{
            
            
        
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let tebcaCard = response["tebca"] as? NSDictionary
                
                if tebcaCard != nil{
                    //tenemos tarjeta tebca
                    self.tebcaCard.hasTebca = true
                    
                    let estatus = tebcaCard!["estatus"] as! Bool
                    let favorito = tebcaCard!["favorito"] as! Bool
                    
                    let codigo = tebcaCard!["codigo"] as! String
                    let pan = tebcaCard!["pan"] as! String
                    let vigencia = tebcaCard!["vigencia"] as! String
                    let balance = tebcaCard!["balance"] as! Double
                    
                    let object = pootEngine()
                    
                    
                    let codigoEncripted = object.decryptedString(of: codigo, withSensitive: false)
                    let panEncripted = object.decryptedString(of: pan, withSensitive: false)
                    let vigenciaEncripted = object.decryptedString(of: vigencia, withSensitive: false)
                    
                    self.tebcaCard.balance = String(balance)
                    self.tebcaCard.codigo = codigoEncripted as? String
                    self.tebcaCard.tarjeta = panEncripted as? String
                    self.tebcaCard.vigencia = vigenciaEncripted as? String
                    self.tebcaCard.estatus = estatus
                    self.tebcaCard.favorito = favorito
                    
                    
                    
                }else{
                    //no tenemos tarjeta tebca
                    self.tebcaCard.hasTebca = false
                }
                
                let cci = response["cci"] as? NSDictionary
                
                if cci != nil {
                    //tenemos cci
                    
                    self.CciElement.hasCCI = true
                    let cuentaCCI = cci!["cuenta"] as! String
                    let nombreCCI = cci!["nombre"] as! String
                    let favorito = cci!["favorito"] as! Bool
                    let idBanco = String(cci!["idBanco"] as! Int)
                    let nombreBanco = cci!["nombreBanco"] as! String
                    
                    self.CciElement.idBanco = idBanco
                    self.CciElement.nombreBanco = nombreBanco
                    
                    
                    self.CciElement.cuenta = cuentaCCI
                    self.CciElement.nombre = nombreCCI
                    self.CciElement.favorito = favorito
                    
                }else{
                    //no tenemos cci
                    self.CciElement.hasCCI = false
                }
                
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
                
                //actualizamos los elementos visuales
                self.updateViews()
                
                
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
                
            }
            
            
            
        }
        
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor, intente nuevamente.", cancel: "OK")
    }
    
    
    //metodo que se ejecuta al presionar el boton de solicitar
    @IBAction func solicitarAction(_ sender: UIButton) {
        
            let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        
            print("Este es el id del comercio \(DataUser.USERIDD)")
         let params : [String : Any] = ["id":DataUser.USERIDD!]
        
          self.services.send(type: .POSTURLENCODED, url: "TradeAccount/1/4/es/Applyforcard", params: params, header: headers, message: "Obteniendo",animate: true)
        
    }
    
    
    
    //metodo que se ejecuta al presionar el botón de ver la tarjeta
    @IBAction func showCvvButtonAction(_ sender: UIButton) {
        
        UIView.transition(with: self.viewCard, duration: 0.4, options: .transitionFlipFromRight, animations: {
            self.menuButtonImageTepca.isHidden = true
            self.tarjetaReversoImageView.isHidden = false
            self.numberCvvLabel.isHidden = false
            self.hideButton.isHidden = false
            self.hideButton.isUserInteractionEnabled = true
            self.showButton.isUserInteractionEnabled = false
        })
    }
    
    //metodo que se ejecuta al presionar el botón de esconder tarjeta
    @IBAction func hideButtonAction(_ sender: UIButton) {
        
        UIView.transition(with: self.viewCard, duration: 0.4, options: .transitionFlipFromLeft, animations: {
            self.menuButtonImageTepca.isHidden = false
            self.tarjetaReversoImageView.isHidden = true
            self.numberCvvLabel.isHidden = true
            self.hideButton.isHidden = true
            self.hideButton.isUserInteractionEnabled = false
            self.showButton.isUserInteractionEnabled = true
        })
        
        
        
        
    }
    
    
    
    //metodo que actualiza los elementos visuales de la vista
    func updateViews(){
        
        //si tienes cci
        if self.CciElement.hasCCI == true{
            self.showCardCciView.isHidden = false
            self.showCardCciView.isUserInteractionEnabled = true
            self.nameCardCciLabel.text = self.CciElement.nombre!
            self.numberCardCciLabel.text = "CCI: \(self.CciElement.cuenta!)"
            self.addCardView.isHidden = true
            self.agregarCuentaButton.isUserInteractionEnabled = false
            self.agregarCuentaButton.isHidden = true
        }else{
            //no tienes cci
            self.showCardCciView.isUserInteractionEnabled = false
        }
        
        //si tienes tebca
        if self.tebcaCard.hasTebca == true{
            
            if tebcaCard.estatus == true{
                self.bloquearLabel.text = "BLOQUEAR"
            }else{
                self.bloquearLabel.text = "DESBLOQUEAR"
            }
            
            self.solicitarView.isHidden = true
            self.solicitarView.isUserInteractionEnabled = false
            self.tepcaActiveView.isHidden = false
            self.tepcaActiveView.isUserInteractionEnabled = true
            
            self.viewCard.isHidden = false
            self.viewCard.isUserInteractionEnabled = true
            if self.tebcaCard.tarjeta != ""{
                let characters = Array(tebcaCard.tarjeta!)
                self.numberCardLabel.text = "\(characters[0])\(characters[1])\(characters[2])\(characters[3]) \(characters[4])\(characters[5])\(characters[6])\(characters[7]) \(characters[8])\(characters[9])\(characters[10])\(characters[11]) \(characters[12])\(characters[13])\(characters[14])\(characters[15])"
            }
            
            self.expirationLabel.text = self.tebcaCard.vigencia!
            self.numberCvvLabel.text = self.tebcaCard.codigo!
            self.blockButton.isHidden = false
            self.blockButton.isUserInteractionEnabled = true
            self.reposicionButton.isHidden = false
            self.reposicionButton.isUserInteractionEnabled = true
            self.saldoActualLabel.isHidden = false
            self.saldoActualAmountLabel.isHidden = false
            self.saldoActualAmountLabel.text = "S/ \(self.tebcaCard.balance!)"
             self.menuButtonImageTepca.isHidden = false
            
        }else{
            //no tienes tarjeta tepca
            self.tepcaActiveView.isHidden = true
            self.tepcaActiveView.isUserInteractionEnabled = false
        }
        
        //si tienes favorito cci
        if self.CciElement.favorito == true{
            self.favoriteStarCCIImageView.isHidden = false
            
        }else{
            self.favoriteStarCCIImageView.isHidden = true
        }
        
        //si tienes favorito tepca
        if self.tebcaCard.favorito == true{
            self.favoriteStarTepcaImageView.isHidden = false
            
        }else{
            self.favoriteStarTepcaImageView.isHidden = true
        }
        
    }
    
    
    //metodo que se ejecuta al presionar tu tarjeta cci para ver las opciones
    @objc func optionsCCIAction(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        print("ves las opciones de cci")
        
        
        
        let alert:UIAlertController = UIAlertController(title: "¿Cual acción deseas realizar?", message: nil, preferredStyle: UIAlertController.Style.actionSheet);
        let cancel:UIAlertAction    = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil);
        
        let favorito:UIAlertAction     = UIAlertAction(title: "Favorito", style: UIAlertAction.Style.default) { (UIAlertAction) -> Void in
            self.actionCCI(type:0);
        }
        let editar:UIAlertAction   = UIAlertAction(title: "Editar", style: UIAlertAction.Style.default) { (UIAlertAction) -> Void in
            self.actionCCI(type:1);
        }
        
        //si eres favorito no sale la opcion de favorito porque solo hay un elemento
          if DataUser.TIPO_PERSONA! != "juridica"{
        alert.addAction(favorito);
        }
        alert.addAction(editar);
        alert.addAction(cancel);
        present(alert, animated: true, completion: nil);
     
        
        
        
    }
    
    //metodo que se ejecuta al presionar tu tarjeta cci para ver las opciones
    @objc func optionsTEPCAAction(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        print("ves las opciones de TEPCA")
        
        
        
        let alert:UIAlertController = UIAlertController(title: "¿Cual acción deseas realizar?", message: nil, preferredStyle: UIAlertController.Style.actionSheet);
        let cancel:UIAlertAction    = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil);
        
        let favorito:UIAlertAction     = UIAlertAction(title: "Favorito", style: UIAlertAction.Style.default) { (UIAlertAction) -> Void in
            self.actionCCI(type:2);
        }
        
        alert.addAction(favorito);
        alert.addAction(cancel);
        present(alert, animated: true, completion: nil);
        
        
        
        
    }
    
    
    //metodo que decide si editar o favorito type 0:Favorito type1:Editar type2:favorito tepca
    func actionCCI(type : Int) {
        
        if type == 0{
            //esta accion setea favorito
            
            if self.CciElement.favorito == true{
                
                self.alert(title: "¡Aviso!", message: "Esta cuenta ya se encuentra como favorita.", cancel: "OK")
                return;
            }
            
            print("accion de favorito cci")
            
            let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
            
            
            
            self.services.send(type: .POST, url: "TradeAccount/1/4/es/account/favorite?id=\(DataUser.USERIDD!)&type=1", params: [:], header: headers, message: "Cargando",animate: true)
         
            
        }else if type == 1 {
            //esta accion empieza a editar
            print("accion de editar cci")
            self.cciTextField.text = self.CciElement.cuenta!
            UIView.transition(with: self.addCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.addCardView.isHidden = false
                self.showCardCciView.isHidden = true
                self.showCardCciView.isUserInteractionEnabled = false
                self.institutionCode = self.CciElement.idBanco!
                self.informacionBancariaButton.setTitle(self.CciElement.nombreBanco!, for: .normal)
                self.informacionBancariaButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                
                
            })
            
           
            
        }else if type == 2{
            
            print("accion favorito tepca")
            if self.tebcaCard.favorito == true{
                
                self.alert(title: "¡Aviso!", message: "Esta cuenta ya se encuentra como favorita.", cancel: "OK")
                return;
            }
            
            let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
            
            
            self.services.send(type: .POST, url: "TradeAccount/1/4/es/account/favorite?id=\(DataUser.USERIDD!)&type=2", params: [:], header: headers, message: "Cargando",animate: true)
            
            
            
            
        }
        
        
    }
    
    
    //metodo que se ejecuta al presionar el boton de reposicion de la tarjeta tepca
    @IBAction func repositionAction(_ sender: UIButton) {
        
        
        if tebcaCard.estatus == true{
            self.alert(title: "¡Aviso!", message: "Debes bloquear tu cuenta antes de solicitar una reposición.", cancel: "OK")
            return;
        }
        
        
        let alert = UIAlertController(title: "¡Aviso!", message: "¿Confirma que desea solicitar su reposición?", preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil));
        alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action:UIAlertAction) in
            
            
            let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
            
            
            
            
            self.services.send(type: .POST, url: "TradeAccount/1/4/es/replacement?id=\(DataUser.USERIDD!)", params: [:], header: headers, message: "Cargando",animate: true)
            
            
            
            
        }));
        
        present(alert, animated: true, completion: nil);
        

        
        
    }
    
    //metodo que se ejecuta al presionar el boton de bloquear y desbloquear tarjeta tepca
    @IBAction func blockButtonAction(_ sender: UIButton) {
        
        
        
        let alert = UIAlertController(title: "¡Aviso!", message: "¿Deseas \(self.bloquearLabel.text!) tu tarjeta?", preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil));
        alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            if self.tebcaCard.estatus == true{
                //vamos a bloquear mandando 0
                
                let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
                
                
                self.services.send(type: .POST, url: "TradeAccount/1/4/es/chgblockstatus/0?id=\(DataUser.USERIDD!)", params: [:], header: headers, message: "Bloqueando",animate: true)
                

            }else{
                //vamos a desbloquear mandando 1
                
                let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
                self.services.send(type: .POST, url: "TradeAccount/1/4/es/chgblockstatus/1?id=\(DataUser.USERIDD!)", params: [:], header: headers, message: "Desbloqueando",animate: true)
                
            }
            
            
            
            
        }));
        
        present(alert, animated: true, completion: nil);
        
        
        
    }
    
    
    //metodo que se ejecuta al presionar el boton de cerrar X en editar cci
    @IBAction func closeEditCCISectionAction(_ sender: UIButton) {
        
        UIView.transition(with: self.showCardCciView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.addCardView.isHidden = true
            self.showCardCciView.isHidden = false
            self.showCardCciView.isUserInteractionEnabled = true
            self.informacionBancariaButton.setTitle("Información bancaria", for: .normal)
            self.informacionBancariaButton.setTitleColor(#colorLiteral(red: 0.7568627451, green: 0.7568627451, blue: 0.7568627451, alpha: 1), for: .normal)
            self.institutionCode = ""
            
        })
        
     
    }
    
    
    //metodo que se ejecuta en el boton para mostrar la lista de bancos
    @IBAction func getBanksAction(_ sender: UIButton) {
        
        let headers  : HTTPHeaders = [:]
        self.services.send(type: .GET, url: ServicesLinks.PEBanksCodes, params: [:], header: headers, message: "Obteniendo",animate:true)
        
    }
    
    
    
    //metodo que se ejecuta al presionar continuar  y querer actualizar tus datos de cci este metodo es el que llama al servicio y hace las validaciones
    @IBAction func updateCCIButtonAction(_ sender: UIButton) {
        
        
        if (self.cciTextField.text?.count)! < 20 ||  (self.cciTextField.text?.count)! > 20{
            alert(title: "¡Aviso!", message: "Agrega un cci valido para continuar.", cancel: "OK");
            return;
        }
        
        if self.institutionCode == ""{
            alert(title: "¡Aviso!", message: "Debes seleccionar una institución bancaria para continuar", cancel: "Cerrar");
            return;
            
        }
        
        //llamamos el servicio
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json"]
        
       
        print("este es el id \(DataUser.USERIDD)")
        let params : [String : Any] = ["id":DataUser.USERIDD!,
                                       "cuenta":self.cciTextField.text!,
                                       "idBanco":self.institutionCode]
        
        self.services.send(type: .POST, url: "TradeAccount/1/4/es/account/update", params: params, header: headers, message: "Guardando",animate: true)
        
        
    }
    
    
    
}






