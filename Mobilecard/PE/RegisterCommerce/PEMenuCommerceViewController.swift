


import UIKit
import AVFoundation
import Alamofire
//import Wallet_Ctrl.h




class PEMenuCommerceViewController:UIViewController,ServicesDelegate{
    
    var Selection = 0
    let services = Services()
    
    
    
    //parametros que se envian al wallet indicando que es lo que tiene el usuario
    var tebcaCard = TebcaCommerceCard()
    var cci = CciModel()
    @IBOutlet weak var nameCommerceLabel: UILabel!
    
    @IBOutlet weak var miQrImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //si tenemos nombre de comercio lo ponemos en pantalla en el home
        if let name = DataUser.NAME{
            self.nameCommerceLabel.text = name
        }
        
        //delegamos los servicios
       self.services.delegate = self
        //valida si ya tienen una sesion iniciada y no tienen este parametro (personas que ya tenian su cuenta antes de este atributo)
        if DataUser.TIPO_PERSONA == nil || DataUser.TIPO_PERSONA == ""{
            DataUser.TIPO_PERSONA = "natural"
        }
        
        
        
        
    
        //esperamos respuesta del menu
        NotificationCenter.default.addObserver(self, selector: #selector(self.menuAction(_:)), name: NSNotification.Name(rawValue: "menuAction"), object: nil)

        
      let tap = UITapGestureRecognizer(target: self, action: #selector(PEMenuCommerceViewController.miQrAction))
        self.miQrImageView.isUserInteractionEnabled = true
        self.miQrImageView.addGestureRecognizer(tap)
      
    }

    
    override func viewDidAppear(_ animated: Bool) {
        
        
        let defaults:UserDefaults = UserDefaults.standard;
        let userData : NSDictionary = defaults.dictionary(forKey: "userDetails")! as NSDictionary;

        //userDetails
        //registramos el token device de firestore para las push
        let userId : String  = String(userData["idEstablecimiento"] as! Int)
        
        
        let typeUser = "NEGOCIO"
        SendTokenFirestore.send(idUser: userId, typeUser: typeUser, idPais: String(userData["idPais"] as!Int))
    
        
    }
    
    
    @objc func miQrAction(){
    
 
    
        if DataUser.QREMPRESA != nil{

self.performSegue(withIdentifier: "qrShowCommerce", sender: nil)
       
        
        }else{
            self.alert(title: "¡Aviso!", message: "Qr no detectado , favor de cerrar sesión e iniciar nuevamente.", cancel: "OK")
        }
    
    }
    
    //metodo que se ejecuta al presionar una opcion del menu
    @objc func menuAction(_ notification: NSNotification){
        
        //si se puede crear el tag
        if let tag =  notification.object as? Int{
            
            switch tag{
            case 1:
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: PEMenuCommerceViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
                break
                
            //tarjeta presente
            case 2:
                Selection = 0
                self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)
                break
                
            //generar codigo qr
            case 3:
                Selection = 1
                self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)
                break
                
            //escanear codigo qr
            case 4:
              //  Selection = 2
               // self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)
                break
                
            case 6:
                self.performSegue(withIdentifier: "contactPESegue", sender: nil)
                
   
                break
                
                
            default:
                break
            }//termina swtich
            
            
        }//termina if del tag
       
    }
    
    @IBAction func walletAction(_ sender: UIButton) {
        
        
      
        
        switch (sender as AnyObject).tag{
            
        case 0 :
            break
            
        //wallet
        case 1:
            
         
            let headers  : HTTPHeaders = [:]
            
           
            
            
            self.services.send(type: .POST, url: "TradeAccount/1/4/es/CommerceCard?id=\(DataUser.USERIDD!)", params: [:], header: headers, message: "Obteniendo",animate: true)
            
            
            
            
            break
            
        default:
            
            self.alertPop(title: "¡Aviso!", message: "Próximamente", cancel: "Aceptar")
            
            break
            
            
        }
        
        
    }
    
   
    
    //metodo que se ejecuta al presioanr el botón de escanear qr
    @IBAction func scannerQrAction(_ sender: UIButton) {
        Selection = 2
        self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)
    }
    
    @IBAction func favoritesAction(_ sender: Any) {
        
        
        self.alertPop(title: "¡Aviso!", message: "Próximamente", cancel: "Aceptar")
        return;
        
        
        
        if verifyStatus() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewcontroller = storyboard.instantiateViewController(withIdentifier: "Favorites")
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    @IBAction func statementsAction(_ sender: Any) {
        if verifyStatus() {
            let storyboard = UIStoryboard(name: "statement", bundle: nil)
            let viewcontroller = storyboard.instantiateViewController(withIdentifier: "statement")
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    
    //metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        self.navigationController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func ActionButtonTarjet(_ sender: Any) {
        Selection = 0
        self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)

    }
  
    @IBAction func ActionButtonGenerateQr(_ sender: Any) {
        Selection = 1
        self.performSegue(withIdentifier: "stepSegueObjective", sender: nil)

    }
    
    
    //metodo que se ejecuta al presionar el boton del menu
    @IBAction func menuShowAction(_ sender: UIBarButtonItem) {
   
        self.slideMenuController()?.openRightWithVelocity(1200)
        
        
    }
    
    
    @IBAction func historialAction(_ sender: UIButton) {
       
        
            let storyboard : UIStoryboard = UIStoryboard(name: "statement_Commerce", bundle: nil)
        self.navigationController?.pushViewController(storyboard.instantiateInitialViewController()!, animated: true)
        
      
        
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "contactPESegue"{
            
            let destinoViewController = segue.destination as! contact_Ctrl
            destinoViewController._fromPE = "1"
            
        }
        
        if segue.identifier == "walletPEShow"{
            let destinoViewController = segue.destination as! PEWalletCommerceViewController
            destinoViewController.tebcaCard = self.tebcaCard
            destinoViewController.CciElement = self.cci
          
            
            
        }
        
        //acción antes del segue
        if segue.identifier == "stepSegueObjective"{
            
            let destinoViewController = segue.destination as! PECardPressentStep1CommerceViewController
            
            destinoViewController.fromView = Selection
            
            
            
        }
        
        if segue.identifier == "stepGenerateSegueObjective"{
            
            let destinoViewController = segue.destination as! PECardPressentStep1CommerceViewController
            
            destinoViewController.fromView = Selection
            
        }
        
        
    }
     func  showWallet() {
        let storyboard = UIStoryboard(name: "wallet", bundle: nil)
        let viewcontroller = storyboard.instantiateViewController(withIdentifier: "wallet_view")
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    //metodos de verificacion de idStatus
    func  verifyStatus()-> Bool {
        ///// kUserDetailsKey  Raul mendez Validando Jumio en verifyStatus
        var response = UserDefaults.standard.dictionary(forKey:"userDetails")
        let keyJumio=response?["usr_jumio"] as? Int
        let keyStatus=response?["idUsrStatus"] as? Int
        switch keyJumio {
        case 0:
            ///////////////////// AQUI LANZAR ONBOARDING JUMIO /////////////////
             print("")
            
            return false
        case 1:
            
            switch keyStatus {
            case 99:
                ///////////////// AQUI ACTUALIZAR END POINT SESSION  /user/login /////////////////
                //////////////// CREAR UN ALERT CON LOS SIGUIENTES DATOS Y ENVIAR END POINT @" /activate  /////////////////
                
                //                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
                //                [alertMsg setTag:99];
                //                [alertMsg show];
                //                return NO;
                print("")
                
                return false
            case 100:
                //////////////// CREAR UN ALERT CON LOS SIGUIENTES DATOS  EN BOTON OK  ENVIAR A LA SIGUIENTE VISTA ESTOS DATOS
                
                //Register_Step30 *nextView = [[nav viewControllers] firstObject];
                //[nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                //                    [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                //[self.navigationController presentViewController:nav animated:YES completion:nil];
                
                /////////////////

                //  UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                print("")
                
            default:
                print("")
            }
  
        case 3:
            
            ////////// MOSTRAR VISTA DE ERROR  EJEMPLO
          //    [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
            print("")
            
            
        default:
            print("")
        }
 
     return true;
}

    
    
    
    //metodos del delegado de los servicios
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        if endpoint == "TradeAccount/1/4/es/CommerceCard?id=\(DataUser.USERIDD!)"{
            
            print("RESPONSE: \(response)")
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let tebcaCard = response["tebca"] as? NSDictionary
                
                if tebcaCard != nil{
                    //tenemos tarjeta tebca
                    self.tebcaCard.hasTebca = true
                    
                    let estatus = tebcaCard!["estatus"] as! Bool
                    let favorito = tebcaCard!["favorito"] as! Bool
                    
                    let codigo = tebcaCard!["codigo"] as! String
                    let pan = tebcaCard!["pan"] as! String
                    let vigencia = tebcaCard!["vigencia"] as! String
                    let balance = tebcaCard!["balance"] as! Double
                    
                    let object = pootEngine()
                   
                    
                    let codigoEncripted = object.decryptedString(of: codigo, withSensitive: false)
                    let panEncripted = object.decryptedString(of: pan, withSensitive: false)
                    let vigenciaEncripted = object.decryptedString(of: vigencia, withSensitive: false)
                    
                    self.tebcaCard.balance = String(balance)
                    self.tebcaCard.codigo = codigoEncripted as? String
                    self.tebcaCard.tarjeta = panEncripted as? String
                    self.tebcaCard.vigencia = vigenciaEncripted as? String
                    self.tebcaCard.estatus = estatus
                    self.tebcaCard.favorito = favorito
                    
                    
                    
                }else{
                    //no tenemos tarjeta tebca
                    self.tebcaCard.hasTebca = false
                }
                
                let cci = response["cci"] as? NSDictionary
                
                if cci != nil {
                    //tenemos cci
                    
                    self.cci.hasCCI = true
                    let cuentaCCI = cci!["cuenta"] as! String
                    let nombreCCI = cci!["nombre"] as! String
                    let favorito = cci!["favorito"] as! Bool
                    let idBanco = String(cci!["idBanco"] as! Int)
                    let nombreBanco = cci!["nombreBanco"] as! String
                    
                    self.cci.idBanco = idBanco
                    self.cci.nombreBanco = nombreBanco
                    
                    self.cci.cuenta = cuentaCCI
                    self.cci.nombre = nombreCCI
                    self.cci.favorito = favorito
                    
                }else{
                    //no tenemos cci
                    self.cci.hasCCI = false
                }
                
                
                //mandamos a la otra vista
                self.performSegue(withIdentifier: "walletPEShow", sender: nil)
               
                
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
                
            }
            
        
        
            
        }
        
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor, intente nuevamente.", cancel: "OK")
    }
    
    
}






