


import UIKit
import AVFoundation
import TouchDraw




class PECardPressentStep4CommerceViewController:UIViewController{


    
    //objeto que recibimos de la vista pasada
    var cardPressent : CardPressent!
    
    
    @IBOutlet weak var dateOneLabel: UILabel!
    @IBOutlet weak var dateTwoLabel: UILabel!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var autoNumberLabel: UILabel!
    @IBOutlet weak var folioLabel: UILabel!
    @IBOutlet weak var importeLabel: UILabel!
    @IBOutlet weak var comisionLabel: UILabel!
    
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      //obtenemos la fecha
        
        let date = Date(timeIntervalSince1970: (Double(self.cardPressent.dateTime!) / 1000.0))
        let formatterOne = DateFormatter()
        formatterOne.dateFormat = "dd-MM-yyyy"
        formatterOne.locale = NSLocale(localeIdentifier: "es") as Locale?
       
        let firstDateString = formatterOne.string(from: date as Date)
        
        let formatterTwo = DateFormatter()
        formatterTwo.dateFormat = "HH:mm:ss"
        formatterTwo.locale = NSLocale(localeIdentifier: "es") as Locale?
        
        let secoundDateString = formatterTwo.string(from: date as Date)
        
        
        self.dateOneLabel.text = firstDateString
        self.dateTwoLabel.text = secoundDateString
        self.referenceLabel.text = String(self.cardPressent.idTransaccion!)
        self.autoNumberLabel.text = self.cardPressent.authNumber!
        self.folioLabel.text = String(self.cardPressent.idTransaccion!)
        self.importeLabel.text = "S/\(self.cardPressent.cantidad!)"
        
     
        
        self.alert(title: "¡Aviso!", message: "Se ha enviado el comprobante de su pago por correo electrónico.", cancel: "OK")
        
       
        let comisionDouble = Double(self.cardPressent.comision!)!
        let  comisionFormatt = NSString(format: "%.2f", comisionDouble)
        self.comisionLabel.text = "S/\(String(comisionFormatt))"
        
        
     

        
     
        
      
        
        
       

        
        
    }
    
   
    
    
  
    
    //metodo que se ejecuta al presionar el botón de compartir
    @IBAction func shareAction(_ sender: UIButton) {
        
        
        DispatchQueue.main.async {
        
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, 0);
        self.view.drawHierarchy(in: self.view.bounds, afterScreenUpdates: true)
        if let imageScreen = UIGraphicsGetImageFromCurrentImageContext(){
            
            UIGraphicsEndImageContext()
            let activityViewController = UIActivityViewController(activityItems: [imageScreen], applicationActivities: nil)
            
            activityViewController.excludedActivityTypes = [.addToReadingList,
                                                            .airDrop,
                                                            .assignToContact,
                                                            .copyToPasteboard,
                                                            .mail,
                                                            .message,
                                                            .print,
                                                            .saveToCameraRoll,
                                                            .postToWeibo,
                                                            .copyToPasteboard,
                                                            .saveToCameraRoll,
                                                            .postToFlickr,
                                                            .postToVimeo,
                                                            .postToTencentWeibo,
                                                            .postToFacebook,
                                                            .postToTwitter
            ]
            
            self.present(activityViewController, animated: true, completion: {})
        }
        
    }
        
        
    }

    
    
   
    
    
    //metodo que se ejecuta al presionar el botón de realizar otra transferencia
    @IBAction func otherTransferAction(_ sender: UIButton) {
        
           self.cardPressent.cleanData()
        self.cardPressent.newTransaction = 1
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: PECardPressentStep1CommerceViewController.self) {
             
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
        
    }
    
 
   //metodo que se ejecuta al presionar el botón de home
    @IBAction func homeAction(_ sender: UIButton) {
        
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: PEMenuCommerceViewController.self) {
               
  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    
   
}








