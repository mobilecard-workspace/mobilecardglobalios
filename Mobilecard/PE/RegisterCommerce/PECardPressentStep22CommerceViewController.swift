


import UIKit
import AVFoundation
import CreditCardValidator
import CoreLocation




class PECardPressentStep22CommerceViewController:UIViewController{

    
    //objeto que recibimos de la vista pasada
    var cardPressent : CardPressent!


    @IBOutlet weak var mailTextField: UITextField_Validations!
    @IBOutlet weak var phoneTextField: UITextField_Validations!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
   
   
      
        
        
        
        
        
        
        

     

        
     
        
      
        
        
       

        
        
    }
    
    
    
  
    

//metodo que se ejecuta al presionar el botón de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
        
        
    }
    
 

    
    //metodo que se ejecuta al presionar el botón de guardar
    @IBAction func saveAction(_ sender: UIButton) {
        
        
        if (self.mailTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un correo valido para continuar.", cancel: "Cerrar");
            return;
        }
        
      
        self.cardPressent.mail = self.mailTextField.text!
        
        if(self.phoneTextField.text?.count)! > 0{
            self.cardPressent.phone = self.phoneTextField.text!
        }
        
        
     
        self.performSegue(withIdentifier: "firmSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        if segue.identifier == "firmSegue"{
            
            
  
            
            
            
            // nos llevamos el objeto a la siguiente clase
            let destinoViewController = segue.destination as! PECardPressentStep3CommerceViewController
            
            destinoViewController.cardPressent = self.cardPressent
            
            
       
            
        }
    }
    
    
  
    
    
}




