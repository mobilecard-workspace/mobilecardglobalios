


import UIKit
import AVFoundation
import CreditCardValidator
import CoreLocation




class PECardPressentStep2CommerceViewController:UIViewController, CardIOPaymentViewControllerDelegate{

    
    //objeto que recibimos de la vista pasada
    var cardPressent : CardPressent!

//referencias de elementos graficos
    @IBOutlet weak var scanImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UITextField_Validations!
    @IBOutlet weak var lastName: UITextField_Validations!
    @IBOutlet weak var numberTextFieldOne: UITextField_Validations!
     @IBOutlet weak var numberTextFieldTwo: UITextField_Validations!
     @IBOutlet weak var numberTextFieldThree: UITextField_Validations!
    @IBOutlet weak var numberTextFieldFour: UITextField_Validations!
    @IBOutlet weak var monthTextField: UITextField_Validations!
    @IBOutlet weak var yearTextField: UITextField_Validations!
    @IBOutlet weak var cvvTextField: UITextField_Validations!
    @IBOutlet weak var cardTypeTextField: UITextField_Validations!
    @IBOutlet weak var cardTypeButton: UIButton!
    @IBOutlet weak var cardFranchiseImage: UIImageView!
    
    @IBOutlet weak var numberCardStackView: UIStackView!
    
    
    let locationManager = CLLocationManager()
    var locationLatitude : Double!
    var locationLongitude : Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.locationManager.delegate = self
        
       
        CardIOUtilities.preload()
        
        
   
        
        //gesture para tomar acción al presionar la imagen
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageCardTapped(tapGestureRecognizer:)))
        self.scanImageView.addGestureRecognizer(tapGestureRecognizer)
        
        
        //gesture para tomar la accion al presionar el stackview de los textfield de los numeros de tarjeta
        
        let tapGestureRecognizerCard = UITapGestureRecognizer(target: self, action: #selector(imageCardTapped(tapGestureRecognizer:)))
        self.numberCardStackView.addGestureRecognizer(tapGestureRecognizerCard)
        
        
        
        
        
        
        

     

        
     
        
      
        
        
       

        
        
    }
    
    
    
    //metodo que se ejecuta al presionar la imagen de escanear tarjeta
    @objc func imageCardTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {

        
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC!.modalPresentationStyle = .formSheet
        cardIOVC?.useCardIOLogo = false
        cardIOVC?.hideCardIOLogo = true
        cardIOVC?.languageOrLocale = "es"
        cardIOVC?.scanInstructions = "Coloque su tarjeta embosada (con relieve) aquí.\nSe escaneará automáticamente."
        self.present(cardIOVC!, animated: true, completion: nil)
        
      
    }
    

//metodo que se ejecuta al presionar el botón de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
        
        
    }
    
    
    
    //Metodos del delegado de CardIOPayment
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
       
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
         
          
            if info.cardImage != nil{
            self.scanImageView.image = info.cardImage
                self.cardPressent.imagenTarjeta = info.cardImage
            }
            
            
           //se setea la información en pantalla
            self.cvvTextField.text = info.cvv
            self.monthTextField.text = String(info.expiryMonth)
            self.yearTextField.text = String(info.expiryYear)
            
            //agregamos valores a nuestro objeto controlador
            self.cardPressent.cvv = info.cvv
            self.cardPressent.mes = String(info.expiryMonth)
            self.cardPressent.anio = String(info.expiryYear)
         self.cardPressent.numeroTarjeta = info.cardNumber
           
            
            
            //se pone la información de la tarjeta con la informacion del parametro redactedCardNumber
            let characters = Array(info.redactedCardNumber)
            
            self.numberTextFieldOne.text = "\(characters[0])\(characters[1])\(characters[2])\(characters[3])"
            self.numberTextFieldTwo.text = "\(characters[4])\(characters[5])\(characters[6])\(characters[7])"
            self.numberTextFieldThree.text = "\(characters[8])\(characters[9])\(characters[10])\(characters[11])"
            
            for i  in 12...characters.count-1{
                self.numberTextFieldFour.text?.append(characters[i])
            }
            
     
           //conseguimos el tipo de tarjeta (franquicia)
            let v = CreditCardValidator()
        
            if let type = v.type(from: info.cardNumber) {
                
                self.cardPressent.franquicia = type.name
                
                switch self.cardPressent.franquicia{
                case "Visa":
                    self.cardFranchiseImage.image = UIImage(named: "card_visa")
                    break
                    
                case "MasterCard":
                    self.cardFranchiseImage.image = UIImage(named: "card_mastercard")
                    break
                    
                default:
                    break
                    
                }
               
            } else {
               self.alert(title: "¡Aviso!", message: "No se detecto la tarjeta , intente nuevamente.", cancel: "OK")
            }
            
            
        }
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
   

   //metodo que se ejecuta al presionar el botón para seleccionar entre Credito y Debito
    @IBAction func selectedTypeCardAction(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Tipo tarjeta", rows: ["Débito","Crédito"], initialSelection: 0, doneBlock: { (picker, value, selection) in
            
            
            let select = selection as! String
            
            
         
            self.cardTypeTextField.text = select
            
            self.cardPressent.tipoTarjeta = select
            
            
            
        }, cancel: { (action) in
            return
        }, origin: self.cardTypeButton)
        
    }
    
    //metodo que se ejecuta al presionar el botón de guardar
    @IBAction func saveAction(_ sender: UIButton) {
        
        
        if (self.nameLabel.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un nombre para continuar.", cancel: "Cerrar");
            return;
        }
        
        if (self.lastName.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un apellido valido para continuar.", cancel: "Cerrar");
            return;
        }
        
        
    
        if cardPressent.tipoTarjeta == nil{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un tipo de tarjeta para continuar.", cancel: "OK")
            return;
        }
        
        if self.cardPressent.numeroTarjeta == nil || self.cardPressent.anio == nil || self.cardPressent.mes == nil || self.cardPressent.cvv == nil {
            self.alert(title: "¡Aviso!", message: "Debes agregar una tarjeta para continuar.", cancel: "OK")
            return;
        }
        
        if self.cardPressent.franquicia != "Visa" && self.cardPressent.franquicia != "MasterCard"{
            
            self.alert(title: "¡Aviso!", message: "La tarjeta agregada no es válida, intente con una nueva poder continuar.", cancel: "OK")
            return;
        }
        
        
        if self.cardPressent.tipoTarjeta == "Débito"{
            let comision = self.calculaComisionDebito(monto: Double(self.cardPressent.cantidad!)!)
            self.cardPressent.comision = String(comision)
            
        }else if self.cardPressent.tipoTarjeta == "Crédito"{
            let comision = self.calculaComisionCredito(monto: Double(self.cardPressent.cantidad!)!)
            self.cardPressent.comision = String(comision)
            
        }
        
        self.cardPressent.nombreTarjetaHabitante = self.nameLabel.text!
        self.cardPressent.apellidosTarjetaHabitante = self.lastName.text!
        
        
        
        
        self.cardPressent.lat = String(self.locationLatitude)
        self.cardPressent.long = String(self.locationLongitude)
        
       
        
        self.performSegue(withIdentifier: "moreDatesSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        if segue.identifier == "moreDatesSegue"{
            
            
  
            
            
            
            // nos llevamos el objeto a la siguiente clase
            let destinoViewController = segue.destination as! PECardPressentStep22CommerceViewController
            
            destinoViewController.cardPressent = self.cardPressent
            
            
       
            
        }
    }
    
    
    func calculaComisionDebito(monto: Double)-> Double {
        
        
        let comision = (monto * 0.039)
        let impuesto = (comision * 0.18)
        return   (comision + impuesto)
    }
    
    func calculaComisionCredito(monto: Double)-> Double {
        
        
        let comision = (monto * 0.049)
        let impuesto = (comision * 0.18)
        return   (comision + impuesto)
    }
    
}






//delegato de locationManager
extension PECardPressentStep2CommerceViewController :CLLocationManagerDelegate{
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
           
            //pedimos el permiso al usuario
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            print("permiso activo")
            //actualizamos ubicación y llamamos al metodo que actualiza la longitud , latitud actualiza la direccion y el mapa
            locationManager.startUpdatingLocation()
            permissionActive()
            
            
            break
        case .authorizedAlways:
            
            
            print("permiso activo")
            

            locationManager.startUpdatingLocation()
            permissionActive()
            
            break
        case .restricted:
            print("restringido por el momento")
           
            let alertController = UIAlertController(title: "Permiso Restringido", message: "Para utilizar esta sección es necesario tener el permiso activado de localización,favor de ir a ajustes.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (alerta) in
                print("Cerramos ventana")
                //aqui tenemos que hacer alguna accion si no tiene el permiso
                _ = self.navigationController?.popViewController(animated: true)
            })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: {
            })
            
            break
        case .denied:
            print("ha sido negado el permiso")
   
            //Configuramos el mensaje del usuario si no tiene habilitado el permiso
            let alertController = UIAlertController(title: "Permiso Negado", message: "Para utilizar esta sección es necesario tener el permiso activado de localización,favor de ir a ajustes.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (alerta) in
                print("Cerramos ventana")
                //aqui tenemos que hacer alguna accion si no tiene el permiso
                _ = self.navigationController?.popViewController(animated: true)
            })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: {
            })
            
            
            
            
            
            break
            
            
        @unknown default:
            break
        }
        
    }
    
    
    func permissionActive(){
        
        if let location : CLLocation = locationManager.location {
            let coordinate : CLLocationCoordinate2D = location.coordinate
            
            self.locationLatitude = coordinate.latitude
            self.locationLongitude = coordinate.longitude
            print(" esta es mi latitud \(self.locationLatitude!) y esta mi longitud   \(self.locationLongitude!)")
            
            
            
        }else{
            
            //Configuramos el mensaje del usuario si no se pudo acceder a su ubicación por algun motivo
            let alertController = UIAlertController(title: "Ubicación no obtenida", message: "Favor de revisar su conexión a internet e intentar nuevamente.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (alerta) in
                
                //realizamos accion si no puede recuperar la ubicacion por falla de conexion
                _ = self.navigationController?.popViewController(animated: true)
            })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: {
            })
            
            
        }
        
}

}

