

import UIKit
import AlamofireImage
import Alamofire

class MenuPEViewController: UIViewController ,ServicesDelegate{

  
    //refrencia al nombre del usuario
    @IBOutlet weak var nameLabel: UILabel!
   
    
    
    var services :Services!
    
   static var currentTag = 1
    
   
    
    override func viewDidAppear(_ animated: Bool) {
        
     /*   if DataUser.NAME  != nil && DataUser.LAST_NAME != nil{
            self.nameLabel.text = "\(DataUser.NAME!) \(DataUser.LAST_NAME!)"
            
        }
        
        if DataUser.IMAGEURL != nil{
            let profileURL = URL(string: (DataUser.IMAGEURL))
            self.profileImageView.af_setImage(withURL: profileURL!)
        }
        */
       
    }
    
    override func viewDidLoad() {
      
        
        self.services = Services()
        self.services.delegate = self
        /*
        if DataUser.NAME  != nil && DataUser.LAST_NAME != nil{
            self.nameLabel.text = "\(DataUser.NAME) \(DataUser.LAST_NAME)"
            
        }
        
        if DataUser.IMAGEURL != nil{
            let profileURL = URL(string: (DataUser.IMAGEURL))
            self.profileImageView.af_setImage(withURL: profileURL!)
        }
        */
        
        super.viewDidLoad()
        
     
        
    
        
       

        // Do any additional setup after loading the view.
    }
    
    
    
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func selectViewController(_ sender: UIButton){
        
       /* if sender.tag == MenuPEViewController.currentTag{
            self.slideMenuController()?.closeRight()
            return
        }*/
        
        
        if sender.tag == 4{
            self.slideMenuController()?.closeRight()
            return
            
        }
       // MenuPEViewController.currentTag = sender.tag
        
         self.slideMenuController()?.closeRight()
        //se notifica al controlador para que haga la transición
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuAction"), object: sender.tag)
        
 
        
    }
    
    
    @IBAction func termsButtonAction(_ sender: UIButton) {
        
        let vc = PEMenuCommerceViewController()
        
        
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func closeSesionAction(_ sender: UIButton) {
    
        let alert = UIAlertController(title: "¡Aviso", message: "¿Quieres cerrar tu sesión?", preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil));
        alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            //borramos el token de firebase por este servicio
            let header : HTTPHeaders = ["Content-Type":"application/json","authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
            
            let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,"token":DataUser.TOKENFIREBASE!,"tipoUsuario":"ESTABLECIMIENTO"]
            print("PARAMS: \(params)")
            
            self.services.send(type: .DELETE, url: "PushNotifications/removeToken", params: params, header: header, message: "", animate: false)
            
            //borramos los datos del usuario actuales
            DataUser.clean()
         
            
           
            self.dismiss(animated: true, completion: nil)
           /*
            let story = UIStoryboard(name: "Startup", bundle: nil)
            let nav : UIViewController = story.instantiateViewController(withIdentifier: "initialID") as! UINavigationController
            
            */
     
            
         //   self.present(nav, animated: false, completion: nil)
            
            
            
            
            
        }));
        
        present(alert, animated: true, completion: nil);
        
        
    
    }
    
    func cleanData(){
    
        //aqui se elimina la data por que se cierra la sesion
    /*    do {
            
            try Auth.auth().signOut()
            DataUser.clean()
            
        } catch let signOutError as NSError {
            
            print ("Error signing out: %@", signOutError)
            
        }*/
        
    }
    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
    
        print("RESPONSE: \(response)")
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
             print("RESPONSE: \(response)")
    }
    
    
    func errorService(endpoint: String) {
        
    }
    
}
