


import UIKit
import AVFoundation
import Alamofire



class PERegisterStep2ViewController:UIViewController,ServicesDelegate,UITextFieldDelegate{

  
    //objeto que se recibe de la vista pasada
    var commerce : CompanyRegister!
    
    //objeto maneja los servicios
    var services : Services!
    
    //llegan de la clase pasada
    var telefono = ""
    var email = ""
    
    
    //referencia de elementos de la vista
    @IBOutlet weak var ageTextField: UITextField_Validations!
    @IBOutlet weak var nacionalityTextField: UITextField_Validations!
    @IBOutlet weak var addressTextField: UITextField_Validations!
    @IBOutlet weak var distritTextField: UITextField_Validations!
    @IBOutlet weak var provinceTextField: UITextField_Validations!
    @IBOutlet weak var departamentTextField: UITextField_Validations!
    @IBOutlet weak var phoneNumberTextField: UITextField_Validations!
    
    
    @IBOutlet weak var confirmEmailTextField: UITextField_Validations!
    @IBOutlet weak var emailTextField: UITextField_Validations!
    @IBOutlet weak var passTextField: UITextField_Validations!
    @IBOutlet weak var confirmPassTextField: UITextField_Validations!
    
    
    @IBOutlet weak var departamentButton: UIButton!
    @IBOutlet weak var provinceButton: UIButton!
    @IBOutlet weak var distritButton: UIButton!
    
    
    //para la fecha
    var birthDatePicker: UIDatePicker!
    
    //variable que guarda la fecha seleccionada
    var currentBirthDate:String = ""
    
    
    //variables que guardan el codigo seleccionado
    var departamentCode = ""
    var provinceCode = ""
    var distritCode = ""
    
    //arreglos que contienen la informacion de provinsias departamentos distritos
    
    var departamentNames : [String] = []
    var departamentCodes : [String] = []
    var provinceNames : [String] = []
    var provinceCodes : [String] = []
    var distritNames : [String] = []
    var distritCodes : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.phoneNumberTextField.text = self.telefono
      //  self.emailTextField.text = self.email
        
        
   //inicializamos objeto de servicios
     self.services = Services()
     self.services.delegate = self
 
      
        // Date picker
        let toolDateBar = UIToolbar()
        toolDateBar.sizeToFit()
        let doneDateBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(setDate(_:)))
        toolDateBar.setItems([doneDateBtn], animated: false)
        
        self.birthDatePicker = UIDatePicker()
        let loc = Locale(identifier: "es")
        self.birthDatePicker.locale = loc
        self.birthDatePicker.datePickerMode = .date
        self.ageTextField.inputAccessoryView = toolDateBar
        self.ageTextField.inputView = self.birthDatePicker
   
        
        
       

        
        
    }
    
    
    //metodo que le da el formato a la fecha
    
    
    @objc func setDate(_ sender: UIButton){
        let newDateFormat = DateFormatter()
        newDateFormat.dateStyle = .medium
        newDateFormat.timeStyle = .none
        newDateFormat.dateFormat = "dd/MM/yyyy"
        
        self.ageTextField.text = newDateFormat.string(from: birthDatePicker.date)
        
        let newDate = newDateFormat.date(from: ageTextField.text!)
        newDateFormat.dateFormat = "yyyy-MM-dd"
        currentBirthDate = newDateFormat.string(from: newDate!)
        
        
        self.view.endEditing(true)
        
    }
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
       self.navigationController?.popViewController(animated: true)
          
    }
    
    
    
    
    
   
   
    
    
    //metodo que se ejecuta al presionar el botón de continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        //validaciones
        
        if (self.currentBirthDate == "") {
            alert(title: "¡Aviso!", message: "Agrega una fecha de nacimiento para continuar.", cancel: "OK");
            return;
        }
      
        
        if (self.nacionalityTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una nacionalidad para continuar.", cancel: "OK");
            return;
        }
       
        
        if (self.addressTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una dirección para continuar.", cancel: "OK");
            return;
        }
        
     
        if (self.departamentTextField.text?.count)! < 1 {
            alert(title: "¡Aviso!", message: "Agrega un departamento para continuar.", cancel: "OK");
            return;
        }
        
        
        
        if (self.provinceTextField.text?.count)! < 1 {
            alert(title: "¡Aviso!", message: "Agrega una provincia para continuar.", cancel: "OK");
            return;
        }
        
       
        if (self.distritTextField.text?.count)! < 1 {
            alert(title: "¡Aviso!", message: "Agrega un distrito para continuar.", cancel: "OK");
            return;
        }
        
      
       
        
        if (self.phoneNumberTextField.text?.count)! < 9 || (self.phoneNumberTextField.text?.count)! > 12{
            alert(title: "¡Aviso!", message: "Agrega un número celular valido para continuar.", cancel: "OK");
            return;
        }
        
   
        
        
        if (self.emailTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un email para continuar.", cancel: "OK");
            return;
        }
        
        
   
        
        if (self.passTextField.text?.count)! < 8 ||  (self.passTextField.text?.count)! > 12{
            alert(title: "¡Aviso!", message: "La contraseña debe tener entre 8 y 12 caracteres de longitud.", cancel: "OK");
            return;
        }
        
        if (self.passTextField.text! != self.confirmPassTextField.text!) {
            alert(title: "¡Aviso!", message: "Las contraseñas deben coincidir para continuar.", cancel: "OK");
            return;
        }
        
        if (self.emailTextField.text! != self.confirmEmailTextField.text!) {
                alert(title: "¡Aviso!", message: "Los emails deben coincidir para continuar.", cancel: "OK");
                return;
            }
            
        
        self.commerce.nationality = self.nacionalityTextField.text!
        self.commerce.birthdayDate = self.currentBirthDate
        self.commerce.address = self.addressTextField.text!
        self.commerce.district = self.distritCode
        self.commerce.province = self.provinceCode
        self.commerce.departament = self.departamentCode
        self.commerce.phone = self.phoneNumberTextField.text!
        self.commerce.email = self.emailTextField.text!
        self.commerce.password = self.passTextField.text!
        
        
        //llamamos a la vista siguiente
        self.performSegue(withIdentifier: "registerSecondSegue", sender: nil)
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 1 || textField.tag == 2{
            
             guard let textFieldText = textField.text,
                    let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                        return false
                }
                let substringToReplace = textFieldText[rangeOfTextToReplace]
                let count = textFieldText.count - substringToReplace.count + string.count
                return count <= 12
            }
            
        
        
        return true
    }
    
    
    
    //metodo que se ejecuta al presionar departamentos
    @IBAction func departamentAction(_ sender: UIButton) {
        
        
        let headers  : HTTPHeaders = [:]
        self.services.send(type: .GET, url: ServicesLinks.PEGetDeparamentos, params: [:], header: headers, message: "Obteniendo",animate: true)
        
    }
    
    
    
    
   //metodo que se ejecuta al presioanr provincia
    @IBAction func provinceAction(_ sender: UIButton) {
        if self.departamentCode == ""{
            self.alert(title: "¡Aviso!", message: "Debes seleccionar un departamento para continuar.", cancel: "OK")
            return;
        }
        
        
        
        let headers  : HTTPHeaders = [:]
        self.services.send(type: .GET, url: "\(ServicesLinks.PEGetProvincias)\(self.departamentCode)", params: [:], header: headers, message: "Obteniendo",animate: true)
        
        
    }
    
    //metodo que se ejecuta al presionar distrito
    @IBAction func distritAction(_ sender: UIButton) {
        if self.provinceCode == ""{
            self.alert(title: "¡Aviso!", message: "Debes seleccionar una provincia para continuar.", cancel: "OK")
            return;
    }
        
        
        
        let headers  : HTTPHeaders = [:]
        self.services.send(type: .GET, url: "\(ServicesLinks.PEGetDistritos)\(self.provinceCode)", params: [:], header: headers, message: "Obteniendo",animate: true)
        
       
        
        
    }
    
    //antes de llamar a la siguiente vista
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        
        if segue.identifier == "registerSecondSegue"{
            
            
            //nos llevamos nuestro objeto a la siguiente clase
            let destinoViewController = segue.destination as! PERegisterStep3ViewController
            destinoViewController.commerce = self.commerce
            
            
        }
        
        
    }
    
    
    //metodos delegados de servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
    
          
        
    }
    
    
    
    //por si llega un array en la respuesta del servicio
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
        //departamentos
        if endpoint == ServicesLinks.PEGetDeparamentos{
            self.departamentCodes = []
            self.departamentNames = []
    
            for element in response{
                
                let dictio = element as! NSDictionary
                
                let departament = dictio["departamento"] as! String
                let codigo = dictio["codigo"] as! String
                
                self.departamentNames.append(departament)
                self.departamentCodes.append(codigo)
            }
            
            //llamamos el picker
            
            
            ActionSheetStringPicker.show(withTitle: "Departamentos", rows: self.departamentNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                
                
                let select = selection as! String

                self.departamentTextField.text = select
                
                //si seleccionamos otro departamento borramos todas las provincias y distritos seleccionados
                if self.departamentCode != self.departamentCodes[value]{
                    self.provinceCode = ""
                    self.distritCode  = ""
                    self.provinceTextField.text = ""
                    self.distritTextField.text = ""
                }
                
                self.departamentCode = self.departamentCodes[value]

            }, cancel: { (action) in
                return
            }, origin: self.departamentButton)
            
            
        }
        
        
        
        //provincias
        if endpoint == "\(ServicesLinks.PEGetProvincias)\(self.departamentCode)"{
            self.provinceCodes = []
            self.provinceNames = []
            
            for element in response{
                
                let dictio = element as! NSDictionary
                
                let province = dictio["provincia"] as! String
                let codigo = dictio["codigo"] as! String
                
                self.provinceNames.append(province)
                self.provinceCodes.append(codigo)
            }
            
            //llamamos el picker
            
            
            ActionSheetStringPicker.show(withTitle: "Provincia", rows: self.provinceNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                
                
                let select = selection as! String
                
                self.provinceTextField.text = select
                
                //si seleccionamos otra provincia borramos los distritos seleccionados
                if self.provinceCode != self.provinceCodes[value]{
                    self.distritCode  = ""
                    self.distritTextField.text = ""
                }
                
                self.provinceCode = self.provinceCodes[value]
                
            }, cancel: { (action) in
                return
            }, origin: self.provinceButton)
            
            
        }
        
        
        
        //distritos
        if endpoint == "\(ServicesLinks.PEGetDistritos)\(self.provinceCode)"{
            self.distritCodes = []
            self.distritNames = []
            
            for element in response{
                
                let dictio = element as! NSDictionary
                
                let distrit = dictio["distrito"] as! String
                let codigo = dictio["codigo"] as! String
                
                self.distritNames.append(distrit)
                self.distritCodes.append(codigo)
            }
            
            //llamamos el picker
            
            
            ActionSheetStringPicker.show(withTitle: "Distrito", rows: self.distritNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                
                
                let select = selection as! String
                
                self.distritTextField.text = select
                
             
                
                self.distritCode = self.distritCodes[value]
                
            }, cancel: { (action) in
                return
            }, origin: self.distritButton)
            
            
        }
        
        
        
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible conectarse con el servidor, favor de revisar su conexión a internet.", cancel: "OK")
    }
 
   

   
}








