


import UIKit
import AVFoundation





class PERegisterStep1ViewController:UIViewController{

  
    //objeto que se recibe de la vista pasada
    var commerce : CompanyRegister!
    
    //llegan de la clase pasada
    var telefono = ""
    var email = ""
    
 
    
    //referencia de elementos de las vistas
    @IBOutlet weak var idTextField: UITextField_Validations!
    
    @IBOutlet weak var rucTextField: UITextField_Validations!
    @IBOutlet weak var socialRazonTextField: UITextField_Validations!
    @IBOutlet weak var nameCommerceTextField: UITextField_Validations!
    @IBOutlet weak var addressComerceTextField: UITextField_Validations!
    @IBOutlet weak var numberDniTextField: UITextField_Validations!
    @IBOutlet weak var verificationDniTextField: UITextField_Validations!
    @IBOutlet weak var nameTextField: UITextField_Validations!
    @IBOutlet weak var fatherLastNameTextField: UITextField_Validations!
    @IBOutlet weak var motherLastNameTextField: UITextField_Validations!
    
    //contraints de altura para desaparecer elementos en caso de no necesitarlos (conforme a las selecciones de los menus anteriores)
    
    @IBOutlet weak var idHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var distanceTopContraintID: NSLayoutConstraint!
    @IBOutlet weak var rucHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var razonSocialTopContraint: NSLayoutConstraint!
    @IBOutlet weak var razonSocialHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var nombreComercioTopContraint: NSLayoutConstraint!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // verificamos si es natural juridica y si escanearon el qr o es manual
        
        //tipo de registro //EL DE QR SE ELIMINO NUNCA ENTRARA
        if self.commerce.registerType == "qr"{
            self.idTextField.text = self.commerce.id!
             self.idHeightConstraint.constant = 40
            self.distanceTopContraintID.constant = 30
            self.idTextField.isUserInteractionEnabled = false
        }else if self.commerce.registerType == "manual"{
              self.idTextField.isUserInteractionEnabled = false
            self.idHeightConstraint.constant = 0
            self.distanceTopContraintID.constant = 0
        }
        
        
        //tipo de persona
        
        if self.commerce.personType == "juridica"{
       
            self.rucTextField.isUserInteractionEnabled = true
            self.rucHeightConstraint.constant = 40
            self.socialRazonTextField.isUserInteractionEnabled = true
            self.razonSocialHeightContraint.constant = 40
            self.razonSocialTopContraint.constant = 20
            self.nombreComercioTopContraint.constant = 20
        
        }else if self.commerce.personType == "natural"{
        
            self.rucTextField.isUserInteractionEnabled = false
            self.rucHeightConstraint.constant = 0
            self.razonSocialTopContraint.constant = 0
            self.socialRazonTextField.isUserInteractionEnabled = false
            self.razonSocialHeightContraint.constant = 0
            self.nombreComercioTopContraint.constant = 0
        }
     

        
     
        
      
        
        
       

        
        
    }
    
    
    @IBAction func informationButtonAction(_ sender: UIButton) {
        self.alertPop(title: "¡Aviso!", message: "Debes llenar los datos para registrarte correctamente.", cancel: "Aceptar")
        
    }
    
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
       self.navigationController?.popViewController(animated: true)
          
    }
    
    
    
    //metodo que se ejecuta al presionar el botón para continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        // si vienes con codigo qr
        if self.commerce.registerType == "qr"{
          if  self.idTextField.text == ""{
                self.alert(title: "¡Aviso!", message: "Debes tener un id valido para continuar. intenta escanear otro código.", cancel: "OK")
                
            }
            
        }
        
        
        // si vienes por la via juridica
        if self.commerce.personType == "juridica"{
            
            if (self.rucTextField.text?.count)! < 4 {
                alert(title: "¡Aviso!", message: "Agrega un ruc valido para continuar.", cancel: "Cerrar");
                return;
            }
            
            self.commerce.ruc = self.rucTextField.text

        }
        
         if self.commerce.personType == "juridica"{
        
        if (self.socialRazonTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una razón social valida para continuar.", cancel: "OK");
            return;
        }
            
             self.commerce.razonSocial = self.socialRazonTextField.text!
            
        }
        
        
        if (self.addressComerceTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una dirección de comercio valida para continuar.", cancel: "OK");
            return;
        }
        
        if (self.nameCommerceTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un nombre de comercio valido para continuar.", cancel: "OK");
            return;
        }
        
          if (self.numberDniTextField.text!.count < 7 || self.numberDniTextField.text!.count > 8){
            alert(title: "¡Aviso!", message: "Agrega un número de dni valido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.verificationDniTextField.text?.count)! < 1 || (self.verificationDniTextField.text?.count)! > 1 {
            alert(title: "¡Aviso!", message: "Agrega un digito verificador de dni valido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.nameTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un nombre para continuar.", cancel: "OK");
            return;
        }
        
        if (self.fatherLastNameTextField.text?.count)! < 2 {
            alert(title: "¡Aviso!", message: "Agrega un apellido paterno para continuar.", cancel: "OK");
            return;
        }
        
        
        if (self.motherLastNameTextField.text?.count)! < 2 {
            alert(title: "¡Aviso!", message: "Agrega un apellido materno para continuar.", cancel: "OK");
            return;
        }
        
        
      
       self.commerce.comerceAddress = self.addressComerceTextField.text!
        self.commerce.nombreComercio = self.nameCommerceTextField.text!
        self.commerce.numeroDni = self.numberDniTextField.text!
        self.commerce.digitoVerificador = self.verificationDniTextField.text!
        self.commerce.nombrePerson = self.nameTextField.text!
        self.commerce.fatherLastName = self.fatherLastNameTextField.text!
        self.commerce.motherLastName = self.motherLastNameTextField.text!
        
        
        self.performSegue(withIdentifier: "registerFirstSegue", sender: nil)
    
        
        
    }
    
   
   
    
    //antes de mandar a la vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "registerFirstSegue"{
            
            
            //nos llevamos nuestro objeto a la siguiente clase
            let destinoViewController = segue.destination as! PERegisterStep2ViewController
            destinoViewController.commerce = self.commerce
            destinoViewController.telefono = self.telefono
            destinoViewController.email = self.email
            
            
        }
        
    }
    
    
    
    
 
   

   
}








