


import UIKit
import AVFoundation
import M13Checkbox
import Alamofire
import JumioCore
import Netverify



class PERegisterStep3ViewController:UIViewController,ServicesDelegate,NetverifyViewControllerDelegate,CustomAlertPrivacyDelegate,CustomAlertJumioDelegate{

    var idEmpresa = ""
    //objeto que se recibe de la vista pasada
    var commerce : CompanyRegister!
  
    // formato para las labels
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24.0),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    var attributedString = NSMutableAttributedString(string:"")
    
    var scanReferenceAux = ""
    
    //referencias de la vista
    @IBOutlet weak var checkPublicView: UIView!
     @IBOutlet weak var checkContractView: UIView!
     @IBOutlet weak var checkTermsView: UIView!
    
    @IBOutlet weak var contratLabel: UILabel!
    @IBOutlet weak var aceptContratLabel: UILabel!
    @IBOutlet weak var termsLabel: UILabel!
    
    
    @IBOutlet weak var laboralCenterTextField: UITextField_Validations!
    @IBOutlet weak var laboralCenterHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ocupationTextField: UITextField_Validations!
    @IBOutlet weak var ocupationTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var ocupationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionPositionTextField: UITextField_Validations!
    @IBOutlet weak var descriptionPositionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionPositionTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var serverPublicLabel: UILabel!
    @IBOutlet weak var serverPublicLabelHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var serverPublicLabelTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var informationBankTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cciTextField: UITextField_Validations!
    
    @IBOutlet weak var institucionTextField: UITextField_Validations!
    @IBOutlet weak var institucionButton: UIButton!
    
    
    @IBOutlet weak var checkBoxHeightConstraint: NSLayoutConstraint!
    
    
    
    
    //checkboxs
    let checkboxPublic = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
    let checkboxContract = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
    let checkboxTerms = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
    
    //objeto que gestiona los servicios
    let service = Services()
    
    
    //variable que guarda el nombre de la institucion seleccionada
    var institutionCode = ""
 
    
    //arreglo que contiene el nombre de las instituciones
    
    var institutionNames : [String] = []
    var institutionCodes : [String] = []
    
    
    
    //jumio controller
    var netverifyViewController:NetverifyViewController?
    var customUIController:NetverifyUIController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
     
        //delegamos los servicios
        self.service.delegate = self
        
        
        if self.commerce.personType == "juridica"{
            self.laboralCenterTextField.isUserInteractionEnabled = false
            self.laboralCenterHeightConstraint.constant = 0
            self.ocupationTextField.isUserInteractionEnabled = false
            self.ocupationHeightConstraint.constant = 0
            self.ocupationTopConstraint.constant = 0
            self.serverPublicLabel.isHidden = true
            self.serverPublicLabelHeightConstaint.constant = 0
            self.serverPublicLabelTopConstraint.constant = 0
            self.descriptionPositionHeightConstraint.constant = 0
            self.descriptionPositionTextField.isUserInteractionEnabled = false
            self.descriptionPositionTopConstraint.constant = 0
            self.informationBankTopConstraint.constant = 0
            self.checkPublicView.isHidden = true
            self.checkPublicView.isUserInteractionEnabled = false
            self.checkBoxHeightConstraint.constant = 0
            
        }
        
        //checkbox en aspecto cuadrado
        self.checkboxPublic.boxType = .square
         self.checkboxContract.boxType = .square
         self.checkboxTerms.boxType = .square
        
        //agregamos a la view y configuramos los button check
        self.checkboxPublic.tintColor = #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1)
        self.checkPublicView.addSubview(checkboxPublic)
        self.checkboxContract.tintColor = #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1)
        self.checkContractView.addSubview(checkboxContract)
        self.checkboxTerms.tintColor = #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1)
        self.checkTermsView.addSubview(checkboxTerms)
     
        self.checkboxPublic.addTarget(self, action: #selector(checkBoxState), for: .valueChanged)
        
     
        //agregamos los underline de los botones
        self.attrs[NSAttributedString.Key.font] = self.contratLabel.font
        self.underlineLabel(label: self.contratLabel)
      self.underlineLabel(label: self.aceptContratLabel)
        self.underlineLabel(label: self.termsLabel)
 
      
        
        //se le agrega la posibilidad de clickear las labels de privacidad y terminos
        let tap = UITapGestureRecognizer(target: self, action: #selector(PERegisterStep3ViewController.privacyAction(sender:)))
        
        self.aceptContratLabel.addGestureRecognizer(tap)
        
       
        let tapTwo = UITapGestureRecognizer(target: self, action: #selector(PERegisterStep3ViewController.termsAction(sender:)))
        
        self.termsLabel.addGestureRecognizer(tapTwo)
        
        
      
        
        
        
        
    }
    
    //metodo que se ejecuta al presionar le boton de check de servidor publico (sirve para habilitar y deshabilitar el textField de descripción
    @objc func checkBoxState() {
        
     
        
        if self.checkboxPublic.checkState.rawValue == "Unchecked"{
            
            self.descriptionPositionTextField.text = ""
            self.commerce.descriptionPublicServer = ""
            self.descriptionPositionTextField.isUserInteractionEnabled = false
            
        }else{
            
            self.descriptionPositionTextField.isUserInteractionEnabled = true
            
        }
        
    }
    
    
    //metodo que se ejecuta al presionar acepto contrato
    @objc func privacyAction(sender:UITapGestureRecognizer) {
        
        print("se presiono privacidad")
        
        
        let headers  : HTTPHeaders = ["client":"f0b5f378b80597da0572d38cd881c9cda584a0ae4a363b33e157ac32059f0ae5"]
        let params : [String : Any] = [:]
        self.service.send(type: .GET, url: ServicesLinks.PEPrivacyTerms, params: params, header: headers, message: "Obteniendo",animate: true)
        

        
    }
    
    
    //metodo que se ejecuta al presionar terminos y condiciones
    @objc func termsAction(sender:UITapGestureRecognizer) {
        
        print("se presiono terminos y condiciones")
        
        
  let headers  : HTTPHeaders = ["client":"f0b5f378b80597da0572d38cd881c9cda584a0ae4a363b33e157ac32059f0ae5"]
        let params : [String : Any] = [:]
        self.service.send(type: .GET, url: ServicesLinks.PETermsCon, params: params, header: headers, message: "Obteniendo",animate:true)
        
        
    }
    //metodo que le pone una underline a un label
    func underlineLabel(label:UILabel){
        
        self.attributedString =  NSMutableAttributedString(string:"")
        
        let labelTitleStr = NSMutableAttributedString(string:label.text!, attributes:attrs)
        attributedString.append(labelTitleStr)
        
        label.attributedText = self.attributedString
      
        
    }
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
       self.navigationController?.popViewController(animated: true)
          
    }
    
    //metodo que se ejecuta al presionar el botón de institucion bancaria
    @IBAction func institucionAction(_ sender: UIButton) {
        
        
        let headers  : HTTPHeaders = [:]
        self.service.send(type: .GET, url: ServicesLinks.PEBanksCodes, params: [:], header: headers, message: "Obteniendo",animate:true)
        
        
    }
    
    
    
   
    //metodos delegados de servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        

        print(response)
        
        
        if endpoint == "\(ServicesLinks.PEUpdateReference)\(self.commerce.email!)\(ServicesLinks.PEUpdateReferenceComplex)\(scanReferenceAux)"{
            
            
            
            let error = response["idError"] as! Int
            
            if error == 0{
                
                 let mensaje = response["mensajeError"] as! String
                
                
                let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertJumioID") as! CustomAlertJumioViewMessage
                customAlert.providesPresentationContextTransitionStyle = true
                customAlert.definesPresentationContext = true
                customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                customAlert.mensaje = mensaje
                customAlert.delegate = self
                
                self.present(customAlert, animated: true, completion: nil)
                
            }else{
                
                let mensaje = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
            }
            
            
        }
        
        
        if endpoint == "Usuarios/1/es/user/insertv4"{
            
            print("respondio el servicio")
            
             let error = response["idError"] as! Int
            
            //llamamos al metodo que genera el netverify de jumio
            if error == 0{
                
                
                let idEstablecimiento = String(response["idEstablecimiento"] as! Int)
                let comision = String(response["comisionPorcentaje"] as! Double)
                let tipoPersona = response["tipo_persona"] as! String
                let qrEmpresa = response["qrBase64"] as! String
                let nombre = response["nombreEstablecimiento"] as! String
                let email = response["email"] as! String
                
                    DataUser.COUNTRY = "PE"
                DataUser.USERINFO = response
     
                DataUser.USERIDD = idEstablecimiento
                DataUser.COMISION = comision
                DataUser.TIPO_PERSONA = tipoPersona
                DataUser.QREMPRESA = qrEmpresa
                DataUser.NAME = nombre
                DataUser.EMAIL = email
                DataUser.PASS = "success"
                DataUser.IMEI = UIDevice.current.identifierForVendor?.uuidString
                DataUser.APPID = "iOS"
                DataUser.USERTYPE = "NEGOCIO"
                DataUser.OS = UIDevice.current.systemVersion
                DataUser.COUNTRYID = "4"
                DataUser.MANOFACTURER = "Apple"
                
                
                /*
                let id = String(response["id"] as! Int)
                self.idEmpresa = id
                DataUser.MAIL_TEMP_REGISTER = nil
                DataUser.PHONE_TEMP_REGISTER = nil
                
                
               //  self.generateNetverify()
                
                
                
                let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                             let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertJumioID") as! CustomAlertJumioViewMessage
                             customAlert.providesPresentationContextTransitionStyle = true
                             customAlert.definesPresentationContext = true
                             customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                             customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                             customAlert.mensaje = "mensaje de prueba"
                             customAlert.delegate = self
                             
                             self.present(customAlert, animated: true, completion: nil)
                
                */
                DataUser.STATELOGIN = "0"
                  
                 // self.netverifyViewController?.destroy()
                  //self.netverifyViewController = nil
                  
                  
                //aqui debenoms guardar los datos de sesión
                
                      for controller in self.navigationController!.viewControllers as Array {
                          if controller.isKind(of: RegisterNew_Step1.self) {
                              LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                              break
                          }
                      }
                
                
            }else{
                
                let mensaje = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
        }
        
        
         if endpoint == ServicesLinks.PEBanksCodes{
        
        let code = response["idError"] as! Int
            
            
            if code != 0{
               let mensaje = response["mesajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                return;
            }
            
            let banks = response["banks"] as! NSArray
            
            
            self.institutionNames = []
            for element in banks{
                
                let dictio = element as! NSDictionary
                
                let nombre = dictio["nombre_corto"] as! String
                let code = String(dictio["id"] as! Int)
                self.institutionNames.append(nombre)
                self.institutionCodes.append(code)
                
            }
            
            ActionSheetStringPicker.show(withTitle: "Institución bancaria", rows: self.institutionNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                
                
                let select = selection as! String
                
                self.institucionTextField.text = select
                
                
                
                self.institutionCode = self.institutionCodes[value]
                
            }, cancel: { (action) in
                return
            }, origin: self.institucionButton)
            
            
            
            
            
            
        
        
        }
        
        
        
        
        
        
        
        if endpoint == ServicesLinks.PETermsCon{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let termino = response["termino"] as! String
                self.alertPrivacy(title: "Términos y condiciones", message: termino, cancel: "Aceptar")
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
            }
            
        }
     
        
        if endpoint == ServicesLinks.PEPrivacyTerms{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let termino = response["termino"] as! String
                self.alertPrivacy(title: "Aviso de privacidad", message: termino, cancel: "Aceptar")
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
            }
            
        }
        
        
        
    }
    
    //metodo delegado al aceptar la vista que sale de estamos verificando tus datos
    func didAcceptButtonJumioComplete(){
    DataUser.STATELOGIN = "0"
    
    self.netverifyViewController?.destroy()
    self.netverifyViewController = nil
    
    
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RegisterNew_Step1.self) {
                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
        
    }
    
    //por si llega un array
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        
        
        if endpoint == "\(ServicesLinks.PEUpdateReference)\(self.commerce.email!)\(ServicesLinks.PEUpdateReferenceComplex)\(scanReferenceAux)"{
            
            
            
         
            let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
            let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertPrivacyID") as! CustomAlertPrivacyViewMessage
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.titulo = "¡Aviso!"
            customAlert.mensaje = "Inicia sesión en MobileCard para completar tu proceso de validación de documentos."
            customAlert.delegate = self
            
            self.present(customAlert, animated: true, completion: nil)
            
            
        }else{
            self.alert(title: "¡Aviso!", message: "No fue posible conectarse con el servidor, favor de revisar su conexión a internet.", cancel: "OK")
        }
    }
    
    
    
    
    
    
    //metodo que se ejecuta al presionar el botón de guardar
    @IBAction func saveAction(_ sender: UIButton) {
        
        if self.commerce.personType != "juridica"{
        
        if (self.laboralCenterTextField.text?.count)! < 4 {
            alert(title: "¡Aviso!", message: "Agrega un centro laboral valido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.ocupationTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una ocupación valida para continuar.", cancel: "OK");
            return;
        }
            
        }
        
    
        if self.checkboxContract.checkState.rawValue == "Unchecked"{
            alert(title: "¡Aviso!", message: "Debes aceptar contrato para continuar", cancel: "Cerrar");
            return;
        }
        
        if self.checkboxTerms.checkState.rawValue == "Unchecked"{
            alert(title: "¡Aviso!", message: "Debes aceptar los términos y condiciones para continuar", cancel: "Cerrar");
            return;
        }
        
        
        
        
        
        self.commerce.laboralCenter = self.laboralCenterTextField.text!
        self.commerce.ocupation = self.ocupationTextField.text!
     
        
        //en caso de llenar el campo
        if self.descriptionPositionTextField.text != ""{
        self.commerce.descriptionPublicServer = self.descriptionPositionTextField.text!
        }
        
        //en caso de ser servidor publico
        
        if self.checkboxPublic.checkState.rawValue == "Unchecked"{
          
            self.commerce.publicServer = false
        }else{
               self.commerce.publicServer = true
            
        }
        
        if self.institutionCode == ""{
            alert(title: "¡Aviso!", message: "Debes seleccionar una institución bancaria para continuar", cancel: "Cerrar");
            return;
            
        }
        
        
        
        if (self.cciTextField.text?.count)! < 20 ||  (self.cciTextField.text?.count)! > 20{
            alert(title: "¡Aviso!", message: "Agrega un cci valido para continuar.", cancel: "OK");
            return;
        }
        
        self.commerce.CCI = self.cciTextField.text!
        
        
       self.commerce.institucionBancaria = self.institutionCode
        print("esta es la institucion bancaria \(self.institutionCode)")
        
        
        
        
        print("listo para pedir confirmacion y llamar al servicio")
        
        
        //encriptamos el password
        let passwordEncripted = DataUser.encryptString(string: self.commerce.password!)
        self.commerce.password = passwordEncripted
        
        
        //para mandar el parametro como fue solicitado
        var idInt = 0
        if self.commerce.id == ""{
            idInt = 0
        }else{
            idInt = Int(self.commerce.id!)!
        }
        
        let headers  : HTTPHeaders = [:]
        var params : [String:Any] = ["usuario":self.commerce.email!,
                                     "pass":self.commerce.password!,
                                     "nombre_establecimiento":self.commerce.nombreComercio!,
                                     "rfc":self.commerce.ruc!,
                                     "razon_social":self.commerce.razonSocial!,
                                     "representante_legal": "\(self.commerce.nombrePerson!) \(self.commerce.fatherLastName!) \(self.commerce.motherLastName!)",
                                    "direccion":
                                        self.commerce.address!,
                                    "direccion_establecimiento": self.commerce.comerceAddress!,
                                    "email_contacto": self.commerce.email!,
                                    "telefono_contacto": self.commerce.phone!,
                                    "id_banco":self.commerce.institucionBancaria!,
                                    "cuenta_clabe":"",
                                    "nombre_prop_cuenta":"\(self.commerce.nombrePerson!) \(self.commerce.fatherLastName!) \(self.commerce.motherLastName!)",
                                    "id_account":"0",
                                    "estatus":"0",
                                    "comision_fija":"0.0",
                                    "comision_porcentaje":"0.0",
                                    "urlLogo":"",
                                    "cuenta_tipo":"",
                                    "codigo_banco":"",
                                    "id_aplicacion":"1",
                                    "negocio":"true",
                                    "img_ine":"",
                                    "img_domicilio":"",
                                    "t_previvale":"",
                                    "nombre":self.commerce.nombrePerson!,
                                    "paterno":self.commerce.fatherLastName!,
                                    "materno":self.commerce.motherLastName!,
                                    "colonia":"",
                                    "ciudad":"",
                                    "cp":"",
                                    "id_estado":"0",
                                    "curp":"",
                                    "smsCode":"",
                                    "idPais":"4",
                                    "tipoDocumento":"1",
                                    "numeroDocumento":self.commerce.numeroDni!,
                                    "digito_verificador":self.commerce.digitoVerificador!,
                                    "fechaNacimiento":self.commerce.birthdayDate!,
                                    "nacionalidad":self.commerce.nationality!,
                                    "distrito":self.commerce.district!,
                                    "provincia":self.commerce.province!,
                                    "departamento":self.commerce.departament!,
                                    "centroLaboral":self.commerce.laboralCenter!,
                                    "ocupacion":self.commerce.ocupation!,
                                    "cargo":self.commerce.descriptionPublicServer!,
                                    "cci":self.commerce.CCI!,
                                    "id":idInt]
        
        if (self.commerce.publicServer){
            
            params["institucion"] = "Gobierno"
        }else{
            
              params["institucion"] = ""
        }
        
        
        
        
        
        
        self.service.send(type: .POST, url: "Usuarios/1/es/user/insertv4", params: params, header: headers, message: "Guardando",animate:true)
        
    
        
        
        
    }
    
   
    //metodo que configura y lanza el netverify de jumio
    func generateNetverify(){
        
        if JumioDeviceInfo.isJailbrokenDevice() {
         
            self.alert(title: "¡Aviso!", message: "No es posible acceder a esta sección con el dispositivo modificado, contacte a soporte.", cancel: "OK")
            return
        }
        
        let config:NetverifyConfiguration = NetverifyConfiguration()
        //Provide your API token
        config.apiToken = "6623acf1-641c-43af-b62a-44ab4eb878ad"
        //Provide your API secret
        config.apiSecret = "gYjTrv8IMH2UaUIw5b0mSXFKubLSkvXj"
        config.delegate = self
        
       
        
        
        config.customerInternalReference = "N-\(self.idEmpresa)"
        config.userReference = "N-\(self.idEmpresa)"
        
        if (ServicesLinks.cuaCallBackJumio == true){
        config.callbackUrl = "https://www.mobilecard.mx/JumioQA/callback"
        }
            
        if DataUser.COUNTRYID != nil{
            
            switch DataUser.COUNTRYID {
                
            case "1":
                config.preselectedCountry = "MEX";
                break
            case "2":
                config.preselectedCountry = "COL";
                break
            case "3":
                config.preselectedCountry = "USA";
                break
            case "4":
                config.preselectedCountry = "PER";
                break
                
            default:
                break
            }
        }
        
        
        NetverifyDocumentSelectionButton.jumioAppearance().setBackgroundColor(#colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1), for: UIControl.State.normal)
        NetverifyDocumentSelectionButton.jumioAppearance().setIconColor(UIColor.white, for: UIControl.State.normal)
        NetverifyDocumentSelectionButton.jumioAppearance().setTitleColor(UIColor.white, for: UIControl.State.normal)
        
        UINavigationBar.jumioAppearance().tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        UINavigationBar.jumioAppearance().barTintColor = #colorLiteral(red: 0.9254901961, green: 0.4196078431, blue: 0, alpha: 1)
        UINavigationBar.jumioAppearance().titleTextAttributes = [kCTForegroundColorAttributeName: UIColor.white] as [NSAttributedString.Key : Any]
        
        self.netverifyViewController = NetverifyViewController(configuration: config)
        
        
        if let netverifyVC = self.netverifyViewController {
            self.present(netverifyVC, animated: true, completion: nil)
        } else {
            self.alert(title: "Netverify Mobile SDK", message: "NetverifyViewController is nil", cancel: "OK")
            
        }

        
    }
    
    
 
    
   
    
    //metodo delegado jumio se ejecuta cuando inicia el proceso puede contener un error
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didFinishInitializingWithError error: NetverifyError?) {
        print("NetverifyViewController did finish initializing")
    }
    
    
    //metodo delegado jumio se ejecuta cuando finaliza el escaneo viene con los resultados
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didFinishWith documentData: NetverifyDocumentData, scanReference: String) {
        
        
        //ponemos la referencia global
        self.scanReferenceAux = scanReference
        
        print("NetverifyViewController finished successfully with scan reference: %@", scanReference)
        // Share the scan reference for the Authentication SDK
        UserDefaults.standard.set(scanReference, forKey: "enrollmentTransactionReference")
        
        let selectedCountry:String = documentData.selectedCountry
        let selectedDocumentType:NetverifyDocumentType = documentData.selectedDocumentType
        var documentTypeStr:String
        switch (selectedDocumentType) {
        case .driverLicense:
            documentTypeStr = "DL"
            break;
        case .identityCard:
            documentTypeStr = "ID"
            break;
        case .passport:
            documentTypeStr = "PP"
            break;
        case .visa:
            documentTypeStr = "Visa"
            break;
        default:
            documentTypeStr = ""
            break;
        }
        
        //id
        let idNumber:String? = documentData.idNumber
        let personalNumber:String? = documentData.personalNumber
        let issuingDate:Date? = documentData.issuingDate
        let expiryDate:Date? = documentData.expiryDate
        let issuingCountry:String? = documentData.issuingCountry
        let optionalData1:String? = documentData.optionalData1
        let optionalData2:String? = documentData.optionalData2
        
        //person
        let lastName:String? = documentData.lastName
        let firstName:String? = documentData.firstName
        let dateOfBirth:Date? = documentData.dob
        let gender:NetverifyGender = documentData.gender
        var genderStr:String;
        switch (gender) {
        case .unknown:
            genderStr = "Unknown"
            
        case .F:
            genderStr = "female"
            
        case .M:
            genderStr = "male"
            
        case .X:
            genderStr = "Unspecified"
            
        default:
            genderStr = "Unknown"
        }
        
        let originatingCountry:String? = documentData.originatingCountry
        
        //address
        let street:String? = documentData.addressLine
        let city:String? = documentData.city
        let state:String? = documentData.subdivision
        let postalCode:String? = documentData.postCode
        
        // Raw MRZ data
        let mrzData:NetverifyMrzData? = documentData.mrzData
        
        let message:NSMutableString = NSMutableString.init()
        message.appendFormat("Selected Country: %@", selectedCountry)
        message.appendFormat("\nDocument Type: %@", documentTypeStr)
        if (idNumber != nil) { message.appendFormat("\nID Number: %@", idNumber!) }
        if (personalNumber != nil) { message.appendFormat("\nPersonal Number: %@", personalNumber!) }
        if (issuingDate != nil) { message.appendFormat("\nIssuing Date: %@", issuingDate! as CVarArg) }
        if (expiryDate != nil) { message.appendFormat("\nExpiry Date: %@", expiryDate! as CVarArg) }
        if (issuingCountry != nil) { message.appendFormat("\nIssuing Country: %@", issuingCountry!) }
        if (optionalData1 != nil) { message.appendFormat("\nOptional Data 1: %@", optionalData1!) }
        if (optionalData2 != nil) { message.appendFormat("\nOptional Data 2: %@", optionalData2!) }
        if (lastName != nil) { message.appendFormat("\nLast Name: %@", lastName!) }
        if (firstName != nil) { message.appendFormat("\nFirst Name: %@", firstName!) }
        if (dateOfBirth != nil) { message.appendFormat("\ndob: %@", dateOfBirth! as CVarArg) }
        message.appendFormat("\nGender: %@", genderStr)
        if (originatingCountry != nil) { message.appendFormat("\nOriginating Country: %@", originatingCountry!) }
        if (street != nil) { message.appendFormat("\nStreet: %@", street!) }
        if (city != nil) { message.appendFormat("\nCity: %@", city!) }
        if (state != nil) { message.appendFormat("\nState: %@", state!) }
        if (postalCode != nil) { message.appendFormat("\nPostal Code: %@", postalCode!) }
        if (mrzData != nil) {
            if (mrzData?.line1 != nil) {
                message.appendFormat("\nMRZ Data: %@\n", (mrzData?.line1)!)
            }
            if (mrzData?.line2 != nil) {
                message.appendFormat("%@\n", (mrzData?.line2)!)
            }
            if (mrzData?.line3 != nil) {
                message.appendFormat("%@\n", (mrzData?.line3)!)
            }
        }
        
        
        
        
        
        
        //Dismiss the SDK
        self.dismiss(animated: true, completion: {
            print(message)
            
            
          //  self.alert(title: "¡Aviso!", message: message as String, cancel: "OK")
        //    self.showAlert(withTitle: "Netverify Mobile SDK", message: message as String)
            
            //llamamos al updateReference
            
            
            let headers  : HTTPHeaders = [:]
            let params : [String : Any] = [:]
            self.service.send(type: .POST, url: "\(ServicesLinks.PEUpdateReference)\(self.commerce.email!)\(ServicesLinks.PEUpdateReferenceComplex)\(scanReference)", params: params, header: headers, message: "Registrando",animate: true)
            
            
            
           
        })
    }
    
 
    //metodo delegado de jumio se ejecuta cuando se cancela el escaneo
    func netverifyViewController(_ netverifyViewController: NetverifyViewController, didCancelWithError error: NetverifyError?, scanReference: String?) {
        print("NetverifyViewController cancelled with error: \(error?.message ?? "") scanReference: \(scanReference ?? "")")
        
        //Dismiss the SDK
        self.dismiss(animated: true) {
            self.netverifyViewController?.destroy()
            self.netverifyViewController = nil
        }
    }
    
    
    
    //metodo delegado se ejecuta al presionar aceptar en la vista de error CustomAlertPrivacyDelegate
    func didAcceptButton() {
        DataUser.STATELOGIN = "0"
        self.netverifyViewController?.destroy()
        self.netverifyViewController = nil
        
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RegisterNew_Step1.self) {
                LoaderView.hide(); self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

    
}








