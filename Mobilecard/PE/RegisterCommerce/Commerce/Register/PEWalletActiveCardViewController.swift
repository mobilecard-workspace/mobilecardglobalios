


import UIKit
import AVFoundation
import Alamofire




class PEWalletActiveCardViewController:UIViewController,ServicesDelegate{

//recibimos de la clase pasada
    
    var cci : CciModel!
    var tebcaCard : TebcaCommerceCard!
 
    //referencia a componentes visuales
    @IBOutlet weak var numberCardTextField: UITextField_Validations!
    @IBOutlet weak var cvvTextField: UITextField_Validations!
    @IBOutlet weak var expirationTextField: UITextField_Validations!
    
    
    //para la fecha
    var dayDatePicker = MonthYearPickerView()
    //variable que guarda la fecha seleccionada
    var currentDate:String = ""
    
    let services = Services()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.services.delegate = self
 
        
        // Date picker
        let toolDateBar = UIToolbar()
        toolDateBar.sizeToFit()
        let doneDateBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(setDate(_:)))
        toolDateBar.setItems([doneDateBtn], animated: false)
        
      
       
        self.expirationTextField.inputAccessoryView = toolDateBar
        self.expirationTextField.inputView = self.dayDatePicker
     

        
       
        
      
        
        
       

        
        
    }
    
    //metodo que le da el formato a la fecha
    @objc func setDate(_ sender: UIButton){
       
        
        let yearString = String(self.dayDatePicker.year)
        let arrayYear = Array(yearString)
        
        let yearSend = "\(arrayYear[2])\(arrayYear[3])"
        var monthSend : String = "1"
        monthSend = String(self.dayDatePicker.month)
        switch self.dayDatePicker.month{
            
        case 1:
            monthSend = "01"
            break
        case 2:
            monthSend = "02"
            break
        case 3:
            monthSend = "03"
            break
        case 4:
            monthSend = "04"
            break
        case 5:
            monthSend = "05"
            break
        case 6:
            monthSend = "06"
            break
        case 7:
            monthSend = "07"
            break
        case 8:
            monthSend = "08"
            break
        case 9:
            monthSend = "09"
            break
            
            
        default:
            break
        }
     
        self.currentDate = "\(monthSend)/\(yearSend)"
    
        self.expirationTextField.text = "\(monthSend)/\(yearSend)"
 
        self.view.endEditing(true)
        
        
        
    }
    
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
       self.navigationController?.popViewController(animated: true)
          
    }
    
    
    
    //metodo que se ejecuta al presionar el botón para continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        
  
        
        
        
        
        
        if (self.numberCardTextField.text?.count)! < 16 || (self.numberCardTextField.text?.count)! > 16 {
            alert(title: "¡Aviso!", message: "Agrega un número de tarjeta valido para continuar.", cancel: "OK");
            return;
        }
        
        
        if (self.cvvTextField.text?.count)! < 3 || (self.cvvTextField.text?.count)! > 3 {
            alert(title: "¡Aviso!", message: "Agrega cvv valido para continuar.", cancel: "OK");
            return;
        }
        
        
        if self.currentDate == ""{
            alert(title: "¡Aviso!", message: "Agrega una fecha de expiración para continuar.", cancel: "OK");
            return;
        }
        
        
  //llamamos al servicio para activar la mobilecard
    
       let headers  : HTTPHeaders = ["Content-Type":"application/json"]
        
        let object = pootEngine()
        let cardEncripted = object.encryptJSONString(self.numberCardTextField.text!, withPassword: nil)
        let expirationEncripted = object.encryptJSONString(self.expirationTextField.text!, withPassword: nil)
        let cvvEncripted = object.encryptJSONString(self.cvvTextField.text!, withPassword: nil)
       
        let params : [String : Any] = ["id":DataUser.USERIDD!,
                                       "pan":cardEncripted!,
                                       "vigencia":expirationEncripted!,
                                       "codigo":cvvEncripted!]
        
        self.services.send(type: .POST, url: "TradeAccount/1/\(DataUser.COUNTRYID!)/es/Activate", params: params, header: headers, message: "Guardando",animate: true)
        
    }
    
   
   
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        if endpoint == "TradeAccount/1/\(DataUser.COUNTRYID!)/es/Activate"{
            
            
            print("RESPONSE: \(response)")
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let tebcaCard = response["tebca"] as? NSDictionary
                
                if tebcaCard != nil{
                    //tenemos tarjeta tebca
                    self.tebcaCard.hasTebca = true
                    
                    let estatus = tebcaCard!["estatus"] as! Bool
                    let favorito = tebcaCard!["favorito"] as! Bool
                    
                    let codigo = tebcaCard!["codigo"] as! String
                    let pan = tebcaCard!["pan"] as! String
                    let vigencia = tebcaCard!["vigencia"] as! String
                    let balance = tebcaCard!["balance"] as! Double
                    
                    let object = pootEngine()
                    
                    
                    let codigoEncripted = object.decryptedString(of: codigo, withSensitive: false)
                    let panEncripted = object.decryptedString(of: pan, withSensitive: false)
                    let vigenciaEncripted = object.decryptedString(of: vigencia, withSensitive: false)
                    
                    self.tebcaCard.balance = String(balance)
                    self.tebcaCard.codigo = codigoEncripted as? String
                    self.tebcaCard.tarjeta = panEncripted as? String
                    self.tebcaCard.vigencia = vigenciaEncripted as? String
                    self.tebcaCard.estatus = estatus
                    self.tebcaCard.favorito = favorito
                    
                    
                    
                }else{
                    //no tenemos tarjeta tebca
                    self.tebcaCard.hasTebca = false
                }
                
                let cci = response["cci"] as? NSDictionary
                
                if cci != nil {
                    //tenemos cci
                    
                    self.cci.hasCCI = true
                    let cuentaCCI = cci!["cuenta"] as! String
                    let nombreCCI = cci!["nombre"] as! String
                    let favorito = cci!["favorito"] as! Bool
                    
                    
                    
                    self.cci.cuenta = cuentaCCI
                    self.cci.nombre = nombreCCI
                    self.cci.favorito = favorito
                    
                }else{
                    //no tenemos cci
                    self.cci.hasCCI = false
                }
                
                
                self.navigationController?.popViewController(animated: true)
                
                
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
                
            }
            
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor, intente nuevamente.", cancel: "OK")
    }

    
    
    
 
   

   
}








