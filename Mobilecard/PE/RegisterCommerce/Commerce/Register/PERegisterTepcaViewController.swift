


import UIKit
import AVFoundation
import Alamofire
import M13Checkbox


class PERegisterTepcaViewController:UIViewController,ServicesDelegate{

  
  
    
    //objeto maneja los servicios
    var services : Services!
    
    //llegan de la clase pasada
    @objc var numeroTarjeta = ""
    @objc var vencimiento = ""
    @objc var cvv = ""
    @objc var tipoDeTarjeta = ""
    @objc var direccion = ""
    @objc var codigoPostal = ""
    
    var publicServer = false
    
    //referencia de elementos de la vista
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var ageTextField: UITextField_Validations!
    @IBOutlet weak var nacionalityTextField: UITextField_Validations!
    @IBOutlet weak var addressTextField: UITextField_Validations!
    @IBOutlet weak var distritTextField: UITextField_Validations!
    @IBOutlet weak var provinceTextField: UITextField_Validations!
    @IBOutlet weak var departamentTextField: UITextField_Validations!
 
    
    
    @IBOutlet weak var departamentButton: UIButton!
    @IBOutlet weak var provinceButton: UIButton!
    @IBOutlet weak var distritButton: UIButton!
    
    
    @IBOutlet weak var numberDniTextField: UITextField_Validations!
    @IBOutlet weak var verificationDniTextField: UITextField_Validations!
    @IBOutlet weak var nameTextField: UITextField_Validations!
    @IBOutlet weak var fatherLastNameTextField: UITextField_Validations!
    @IBOutlet weak var motherLastNameTextField: UITextField_Validations!
    
      @IBOutlet weak var checkPublicView: UIView!
    
       let checkboxPublic = M13Checkbox(frame: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
    
    
    @IBOutlet weak var laboralCenterTextField: UITextField_Validations!
    @IBOutlet weak var ocupationTextField: UITextField_Validations!
    @IBOutlet weak var descriptionPositionTextField: UITextField_Validations!
    
    
    //para la fecha
    var birthDatePicker: UIDatePicker!
    
    //variable que guarda la fecha seleccionada
    var currentBirthDate:String = ""
    
    
    //variables que guardan el codigo seleccionado
    var departamentCode = ""
    var provinceCode = ""
    var distritCode = ""
    
    //arreglos que contienen la informacion de provinsias departamentos distritos
    
    var departamentNames : [String] = []
    var departamentCodes : [String] = []
    var provinceNames : [String] = []
    var provinceCodes : [String] = []
    var distritNames : [String] = []
    var distritCodes : [String] = []
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cardNumberLabel.text = self.numeroTarjeta
        self.addressTextField.text = self.direccion
        if DataUser.NAME != nil{
            self.nameTextField.text = DataUser.NAME!
        }
        if DataUser.LASTNAME != nil{
            self.fatherLastNameTextField.text = DataUser.LASTNAME!
        }
        if DataUser.LASTNAMEMOTHER != nil{
            self.motherLastNameTextField.text = DataUser.LASTNAMEMOTHER!
        }
      
        
        //configuaramos el checkBox
         self.checkboxPublic.boxType = .square
        self.checkboxPublic.tintColor = #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1)
        
       
        self.checkPublicView.addSubview(checkboxPublic)
        
        //target del checkbox
          self.checkboxPublic.addTarget(self, action: #selector(checkBoxState), for: .valueChanged)
        
   //inicializamos objeto de servicios
     self.services = Services()
     self.services.delegate = self
 
      
        // Date picker
        let toolDateBar = UIToolbar()
        toolDateBar.sizeToFit()
        let doneDateBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(setDate(_:)))
        toolDateBar.setItems([doneDateBtn], animated: false)
        
        self.birthDatePicker = UIDatePicker()
        let loc = Locale(identifier: "es")
        self.birthDatePicker.locale = loc
        self.birthDatePicker.datePickerMode = .date
        self.ageTextField.inputAccessoryView = toolDateBar
        self.ageTextField.inputView = self.birthDatePicker
   
        
        
       

        
        
    }
    
    
    //metodo que le da el formato a la fecha
    
    
    @objc func setDate(_ sender: UIButton){
        let newDateFormat = DateFormatter()
        newDateFormat.dateStyle = .medium
        newDateFormat.timeStyle = .none
        newDateFormat.dateFormat = "dd/MM/yyyy"
        
        self.ageTextField.text = newDateFormat.string(from: birthDatePicker.date)
        
        let newDate = newDateFormat.date(from: ageTextField.text!)
        newDateFormat.dateFormat = "yyyy-MM-dd"
        currentBirthDate = newDateFormat.string(from: newDate!)
        
        
        self.view.endEditing(true)
        
    }
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
       self.navigationController?.popViewController(animated: true)
          
    }
    
    
    
    
    
   
   
    
    
    //metodo que se ejecuta al presionar el botón de continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        //validaciones
        
        if (self.currentBirthDate == "") {
            alert(title: "¡Aviso!", message: "Agrega una fecha de nacimiento para continuar.", cancel: "OK");
            return;
        }
      
        
        if (self.nacionalityTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una nacionalidad para continuar.", cancel: "OK");
            return;
        }
       
        
        if (self.addressTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una dirección para continuar.", cancel: "OK");
            return;
        }
        
     
        if (self.departamentTextField.text?.count)! < 1 {
            alert(title: "¡Aviso!", message: "Agrega un departamento para continuar.", cancel: "OK");
            return;
        }
        
        
        
        if (self.provinceTextField.text?.count)! < 1 {
            alert(title: "¡Aviso!", message: "Agrega una provincia para continuar.", cancel: "OK");
            return;
        }
        
       
        if (self.distritTextField.text?.count)! < 1 {
            alert(title: "¡Aviso!", message: "Agrega un distrito para continuar.", cancel: "OK");
            return;
        }
        
      
       
        

        
        
        if (self.numberDniTextField.text!.count < 7 || self.numberDniTextField.text!.count > 8){
            alert(title: "¡Aviso!", message: "Agrega un número de dni valido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.verificationDniTextField.text?.count)! < 1 || (self.verificationDniTextField.text?.count)! > 1 {
            alert(title: "¡Aviso!", message: "Agrega un digito verificador de dni valido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.nameTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega un nombre para continuar.", cancel: "OK");
            return;
        }
        
        if (self.fatherLastNameTextField.text?.count)! < 2 {
            alert(title: "¡Aviso!", message: "Agrega un apellido paterno para continuar.", cancel: "OK");
            return;
        }
        
        
        if (self.motherLastNameTextField.text?.count)! < 2 {
            alert(title: "¡Aviso!", message: "Agrega un apellido materno para continuar.", cancel: "OK");
            return;
        }
        
        if (self.laboralCenterTextField.text?.count)! < 4 {
            alert(title: "¡Aviso!", message: "Agrega un centro laboral valido para continuar.", cancel: "OK");
            return;
        }
        
        if (self.ocupationTextField.text?.count)! < 3 {
            alert(title: "¡Aviso!", message: "Agrega una ocupación valida para continuar.", cancel: "OK");
            return;
        }
   
        if self.checkboxPublic.checkState.rawValue == "Unchecked"{
            
            self.publicServer = false
        }else{
            self.publicServer = true
            
        }

        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*"]
        var params : [String : Any] = ["id_usuario":DataUser.USERIDD!,
                                    "usr_direccion":self.addressTextField.text!,
                                       "usr_fecha_nac":self.currentBirthDate,
                                    "digito_verificador":self.verificationDniTextField.text!,
                                       "cedula":self.numberDniTextField.text!,
                                       "tipocedula":"1",
                                       "eMail":DataUser.EMAIL!,
                                       "usr_nombre":self.nameTextField.text!,
                                       "usr_sexo":"M",
                                       "usr_apellido":"\(self.fatherLastNameTextField.text!) \(self.motherLastNameTextField.text!)",
                                "usr_provincia":self.provinceCode,
                                "usr_nacionalidad":"PE",
                                "usr_ocupacion":self.ocupationTextField.text!,
                                "usr_telefono":"",
                                "usr_distrito":self.distritCode,
                                "usr_departamento":self.departamentCode,
                                "usr_centro_laboral":self.laboralCenterTextField.text!]
        
        
        if (self.publicServer){
            params["usr_cargo_publico"] = "1"
            
            params["usr_institucion"] = "Gobierno"
        }else{
            params["usr_cargo_publico"] = "0"
            params["usr_institucion"] = ""
        }
        
        let object = pootEngine()
        let panEncripted = object.encryptJSONString(self.numeroTarjeta, withPassword: nil)
           params["pan"] = panEncripted!
        let vigEncripted = object.encryptJSONString(self.vencimiento, withPassword: nil)
        params["vigencia"] = vigEncripted!
        let codEncripted = object.encryptJSONString(self.cvv, withPassword: nil)
        params["codigo"] = codEncripted!
        
        print("PARAMS: \(params)")
        self.services.send(type: .POST, url:"Wallet/1/4/es/saveTebcaCard", params: params, header: headers, message: "Guardando",animate: true)
        
      
        
        
    }
    
    
    
    //metodo que se ejecuta al presionar departamentos
    @IBAction func departamentAction(_ sender: UIButton) {
        
        
        let headers  : HTTPHeaders = [:]
        self.services.send(type: .GET, url: ServicesLinks.PEGetDeparamentos, params: [:], header: headers, message: "Obteniendo",animate: true)
        
    }
    
    
    
    
   //metodo que se ejecuta al presioanr provincia
    @IBAction func provinceAction(_ sender: UIButton) {
        if self.departamentCode == ""{
            self.alert(title: "¡Aviso!", message: "Debes seleccionar un departamento para continuar.", cancel: "OK")
            return;
        }
        
        
        
        let headers  : HTTPHeaders = [:]
        self.services.send(type: .GET, url: "\(ServicesLinks.PEGetProvincias)\(self.departamentCode)", params: [:], header: headers, message: "Obteniendo",animate: true)
        
        
    }
    
    //metodo que se ejecuta al presionar distrito
    @IBAction func distritAction(_ sender: UIButton) {
        if self.provinceCode == ""{
            self.alert(title: "¡Aviso!", message: "Debes seleccionar una provincia para continuar.", cancel: "OK")
            return;
    }
        
        
        
        let headers  : HTTPHeaders = [:]
        self.services.send(type: .GET, url: "\(ServicesLinks.PEGetDistritos)\(self.provinceCode)", params: [:], header: headers, message: "Obteniendo",animate: true)
        
       
        
        
    }
    
    //antes de llamar a la siguiente vista
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        
        if segue.identifier == "registerSecondSegue"{
            
            
            //nos llevamos nuestro objeto a la siguiente clase
            let destinoViewController = segue.destination as! PERegisterStep3ViewController
           
            
            
        }
        
        
    }
    
    
    //metodos delegados de servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
    
        if endpoint == "Wallet/1/4/es/saveTebcaCard"{
            print("ESTE ES EL RESPONSE \(response)")
            let idError = response["idError"] as! Int
            
            if idError == 0{
                //exito
                
               
               /* let defaults = UserDefaults.standard
                defaults.set("1", forKey: "NewTarjet");
                UserDefaults.standard.set(response["tarjetas"], forKey: "tarjetas")
                UserDefaults.standard.synchronize()
             //   defaults.set(response["tarjetas"] as! NSMutableArray ,forKey: "tarjetas")
                defaults.synchronize();*/
                
                NotificationCenter.default.post(name: Notification.Name("WalletUpdate"), object: nil)
                 self.dismiss(animated: true, completion: nil)
                
            }else{
                //fracaso
                self.alert(title: "¡Aviso!", message: "No es posible añadir esta tarjeta.", cancel: "OK")
                
            }
            
        }
        
    }
    
    
    
    //por si llega un array en la respuesta del servicio
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
        //departamentos
        if endpoint == ServicesLinks.PEGetDeparamentos{
            self.departamentCodes = []
            self.departamentNames = []
    
            for element in response{
                
                let dictio = element as! NSDictionary
                
                let departament = dictio["departamento"] as! String
                let codigo = dictio["codigo"] as! String
                
                self.departamentNames.append(departament)
                self.departamentCodes.append(codigo)
            }
            
            //llamamos el picker
            
            
            ActionSheetStringPicker.show(withTitle: "Departamentos", rows: self.departamentNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                
                
                let select = selection as! String

                self.departamentTextField.text = select
                
                //si seleccionamos otro departamento borramos todas las provincias y distritos seleccionados
                if self.departamentCode != self.departamentCodes[value]{
                    self.provinceCode = ""
                    self.distritCode  = ""
                    self.provinceTextField.text = ""
                    self.distritTextField.text = ""
                }
                
                self.departamentCode = self.departamentCodes[value]

            }, cancel: { (action) in
                return
            }, origin: self.departamentButton)
            
            
        }
        
        
        
        //provincias
        if endpoint == "\(ServicesLinks.PEGetProvincias)\(self.departamentCode)"{
            self.provinceCodes = []
            self.provinceNames = []
            
            for element in response{
                
                let dictio = element as! NSDictionary
                
                let province = dictio["provincia"] as! String
                let codigo = dictio["codigo"] as! String
                
                self.provinceNames.append(province)
                self.provinceCodes.append(codigo)
            }
            
            //llamamos el picker
            
            
            ActionSheetStringPicker.show(withTitle: "Provincia", rows: self.provinceNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                
                
                let select = selection as! String
                
                self.provinceTextField.text = select
                
                //si seleccionamos otra provincia borramos los distritos seleccionados
                if self.provinceCode != self.provinceCodes[value]{
                    self.distritCode  = ""
                    self.distritTextField.text = ""
                }
                
                self.provinceCode = self.provinceCodes[value]
                
            }, cancel: { (action) in
                return
            }, origin: self.provinceButton)
            
            
        }
        
        
        
        //distritos
        if endpoint == "\(ServicesLinks.PEGetDistritos)\(self.provinceCode)"{
            self.distritCodes = []
            self.distritNames = []
            
            for element in response{
                
                let dictio = element as! NSDictionary
                
                let distrit = dictio["distrito"] as! String
                let codigo = dictio["codigo"] as! String
                
                self.distritNames.append(distrit)
                self.distritCodes.append(codigo)
            }
            
            //llamamos el picker
            
            
            ActionSheetStringPicker.show(withTitle: "Distrito", rows: self.distritNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                
                
                let select = selection as! String
                
                self.distritTextField.text = select
                
             
                
                self.distritCode = self.distritCodes[value]
                
            }, cancel: { (action) in
                return
            }, origin: self.distritButton)
            
            
        }
        
        
        
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible conectarse con el servidor, favor de revisar su conexión a internet.", cancel: "OK")
    }
 
    
    //metodo que se ejecuta al presionar le boton de check de servidor publico (sirve para habilitar y deshabilitar el textField de descripción
    @objc func checkBoxState() {
        
        
        
        if self.checkboxPublic.checkState.rawValue == "Unchecked"{
            
            self.descriptionPositionTextField.text = ""
 
            self.descriptionPositionTextField.isUserInteractionEnabled = false
            
        }else{
            
            self.descriptionPositionTextField.isUserInteractionEnabled = true
            
        }
        
    }
   

   
}








