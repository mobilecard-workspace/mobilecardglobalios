


import UIKit
import AVFoundation
import Alamofire




class MXWalletActiveCardViewController:UIViewController,ServicesDelegate{

//recibimos de la clase pasada
    
    var cci : CciModel!
    var tebcaCard : TebcaCommerceCard!
 
    //referencia a componentes visuales
    @IBOutlet weak var numberCardTextField: UITextField_Validations!

    
    @IBOutlet weak var ciudadTextField: UITextField_Validations!
    @IBOutlet weak var estadoButton: UIButton!
    @IBOutlet weak var addressTextField: UITextField_Validations!
    @IBOutlet weak var coloniaTextField: UITextField_Validations!
    @IBOutlet weak var curpTextField: UITextField_Validations!
    
    @IBOutlet weak var codigoPostalTextField: UITextField_Validations!

    
    let services = Services()
    
    
       var estadoCode = ""
    
    var estadoNames : [String] = []
       var estadoCodes : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        self.services.delegate = self
 
      
      
       
     
     

        
       
        
      

        
       

        
        
    }
   
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
       self.navigationController?.popViewController(animated: true)
          
    }
    
    
    
    //metodo que se ejecuta al presionar el botón para continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        
  
        
        
        
        
        
        if (self.numberCardTextField.text?.count)! < 16 || (self.numberCardTextField.text?.count)! > 16 {
            alert(title: "¡Aviso!", message: "Agrega un número de tarjeta valido para continuar.", cancel: "OK");
            return;
        }
        
        
        
      
        
        
        if (self.addressTextField.text?.count)! < 10 ||  (self.addressTextField.text?.count)! > 99{
                   alert(title: "¡Aviso!", message: "Agrega una dirección valida para continuar.", cancel: "OK");
                   return;
               }
        
        if (self.coloniaTextField.text?.count)! < 3{
                         alert(title: "¡Aviso!", message: "Agrega una colonia valida para continuar.", cancel: "OK");
                         return;
                     }
        
        
        if self.estadoCode == ""{
            
            self.alert(title: "¡Aviso!", message: "Selecciona un Estado para continuar.", cancel: "OK")
        }
        
        
        if (self.ciudadTextField.text?.count)! < 3 {
                              alert(title: "¡Aviso!", message: "Agrega una ciudad valida para continuar.", cancel: "OK");
                              return;
                          }
        
        
             if (self.codigoPostalTextField.text?.count)! < 5 || (self.codigoPostalTextField.text?.count)! > 5 {
                                   alert(title: "¡Aviso!", message: "Agrega un código postal valido para continuar.", cancel: "OK");
                                   return;
                               }
        
        
        
        if (self.curpTextField.text?.count)! < 18 || (self.curpTextField.text?.count)! > 18 {
                                    alert(title: "¡Aviso!", message: "Agrega un curp valido para continuar.", cancel: "OK");
                                    return;
                                }
        
        
        
  //llamamos al servicio para activar la mobilecard
    
       let headers  : HTTPHeaders = ["Content-Type":"application/json"]
        
        let object = pootEngine()
        let cardEncripted = object.encryptJSONString(self.numberCardTextField.text!, withPassword: nil)
   
       
        let params : [String : Any] = ["id_usuario":DataUser.USERIDD!,
                                       "pan":cardEncripted!,
                                       "usr_nombre":"",
                                       "usr_direccion":self.addressTextField.text!,
                                       "usr_ciudad":self.ciudadTextField.text!,
                                       "usr_id_estado":self.estadoCode,
                                       "curp":self.curpTextField.text!,
                                       "usr_colonia":self.coloniaTextField.text!,
                                       "usr_cp":self.codigoPostalTextField.text!,
                                       "rfc":"",
                                       "id_aplicacion":"1"]
        
        
        print("PARAMS:\(params)")
        
        self.services.send(type: .POST, url: "Wallet/1/1/es/NEGOCIO/\(DataUser.USERIDD!)/registrartarjetaprevivale", params: params, header: headers, message: "Guardando",animate: true)
        
    }
    
   
   
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        //departamentos
            if endpoint == "Catalogos/1/1/es/estados"{
                self.estadoCodes = []
                self.estadoNames = []
        
                
                let array = response["estados"] as! NSArray
                for element in array{
                    
                    let dictio = element as! NSDictionary
                    
                    let departament = dictio["nombre"] as! String
                    let codigo = String(dictio["id"] as! Int)
                    
                    self.estadoNames.append(departament)
                    self.estadoCodes.append(codigo)
                }
                
                //llamamos el picker
                
                
                ActionSheetStringPicker.show(withTitle: "Estados", rows: self.estadoNames, initialSelection: 0, doneBlock: { (picker, value, selection) in
                    
                    
                    let select = selection as! String

                    self.estadoButton.setTitle(select, for: .normal)
                    self.estadoButton.setTitleColor(UIColor.black, for: .normal)
                    
                   
                    
                    self.estadoCode = self.estadoCodes[value]

                }, cancel: { (action) in
                    return
                }, origin: self.estadoButton)
                
                
            }
        
        
        if endpoint == "Wallet/1/1/es/NEGOCIO/\(DataUser.USERIDD!)/registrartarjetaprevivale"{
            
            
            print("RESPONSE: \(response)")
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                //let tebcaCard = response["tebca"] as? NSDictionary
               /*
                if tebcaCard != nil{
                    //tenemos tarjeta tebca
                    self.tebcaCard.hasTebca = true
                    
                    let estatus = tebcaCard!["estatus"] as! Bool
                    let favorito = tebcaCard!["favorito"] as! Bool
                    
                    let codigo = tebcaCard!["codigo"] as! String
                    let pan = tebcaCard!["pan"] as! String
                    let vigencia = tebcaCard!["vigencia"] as! String
                    let balance = tebcaCard!["balance"] as! Double
                    
                    let object = pootEngine()
                    
                    
                    let codigoEncripted = object.decryptedString(of: codigo, withSensitive: false)
                    let panEncripted = object.decryptedString(of: pan, withSensitive: false)
                    let vigenciaEncripted = object.decryptedString(of: vigencia, withSensitive: false)
                    
                    self.tebcaCard.balance = String(balance)
                    self.tebcaCard.codigo = codigoEncripted as? String
                    self.tebcaCard.tarjeta = panEncripted as? String
                    self.tebcaCard.vigencia = vigenciaEncripted as? String
                    self.tebcaCard.estatus = estatus
                    self.tebcaCard.favorito = favorito
                    
                    
                    
                }else{
                    //no tenemos tarjeta tebca
                    self.tebcaCard.hasTebca = false
                }*/
                
              //  let cci = response["cci"] as? NSDictionary
                
             /*   if cci != nil {
                    //tenemos cci
                    
                    self.cci.hasCCI = true
                    let cuentaCCI = cci!["cuenta"] as! String
                    let nombreCCI = cci!["nombre"] as! String
                    let favorito = cci!["favorito"] as! Bool
                    
                    
                    
                    self.cci.cuenta = cuentaCCI
                    self.cci.nombre = nombreCCI
                    self.cci.favorito = favorito
                    
                }else{
                    //no tenemos cci
                    self.cci.hasCCI = false
                }*/
                
                
                self.navigationController?.popViewController(animated: true)
                
                
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
                
            }
            
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor, intente nuevamente.", cancel: "OK")
    }

    
    //metodo que se ejecuta al presionar el boton de estados
    @IBAction func estadosAction(_ sender: UIButton) {
        
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json"]
               
            
              
        let params : [String : Any] = [:]
               
        self.services.send(type: .GET, url: "Catalogos/1/1/es/estados", params: params, header: headers, message: "Obteniendo",animate: true)
        
        
        
    }
    
    
 
   
    //metodo que se ejecuta al presionar el boton de obtener curp
    @IBAction func obtenerCurpAction(_ sender: UIButton) {
        
        
        guard let url = URL(string: "https://www.gob.mx/curp/") else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    

   
}








