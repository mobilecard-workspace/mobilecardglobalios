


import UIKit
import AVFoundation





class PERegisterCommercerTypePersonViewController:UIViewController{

  //objeto que se recibe de la vista pasada
    var commerce : CompanyRegister!
    
    //llegan de la clase pasada
    var telefono = ""
    var email = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      
        
        
        

     

        
     
        
      
        
        
       

        
        
    }
    

    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
         self.commerce.id = ""
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: PERegisterCommerceViewController.self) {
               self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
       self.navigationController?
        .popViewController(animated: true)
          
    }
    
    
    
   
   
    //metodo que se ejecuta al presionar el botón de persona juridica
    @IBAction func personJuridicAction(_ sender: UIButton) {
   
          self.commerce.personType = "juridica"
        self.performSegue(withIdentifier: "registerTypePersonSegue", sender: nil)
    }
    
    
    
    //metodo que se ejecuta al presionar el botón persona natural
    @IBAction func personNaturalAction(_ sender: UIButton) {
    
    
        self.commerce.personType = "natural"
        
        self.performSegue(withIdentifier: "registerTypePersonSegue", sender: nil)
    
    }
    
   
    //al mandar a la otra vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "registerTypePersonSegue"{
            
            
            //nos llevamos nuestro objeto a la siguiente clase
            let destinoViewController = segue.destination as! PERegisterStep1ViewController
            destinoViewController.commerce = self.commerce
            destinoViewController.telefono = self.telefono
            destinoViewController.email = self.email
            
            
        }
        
    
        
        
    }
    

   
}








