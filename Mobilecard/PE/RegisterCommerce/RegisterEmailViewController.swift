


import UIKit
import AVFoundation
import Alamofire




class RegisterEmailViewController:UIViewController,UITextFieldDelegate,ServicesDelegate{

//referencias de pantalla
    @IBOutlet weak var viewEmail: UIView!
    
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var openMailButton: UIButton!
    @IBOutlet weak var sendMailButton: UIButton!
    
    //la recibimos de la vista pasada
     // si es 0 viene de usuario si es 1 viene de registro de comercio si es 2 viene de registro comercio PERU
    var isFrom = 0
    
    //variable que recibimos de la clase pasada
    var telefono = ""
    var email = ""
    var idSelection : Int = 0
    
    //objeto que gestiona los servicios
    let service = Services()
    
    // formato para el botón
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24.0),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
     var attributedString = NSMutableAttributedString(string:"")
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //delegamos los servicios
        self.service.delegate = self

        //agregamos la underline al botón de abrir correo
  self.attrs[NSAttributedString.Key.font] = self.openMailButton.titleLabel?.font
  self.underlineButton(button: self.openMailButton)
        
     
        //targets
        self.mailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
      
        //delegado
        self.mailTextField.delegate = self
        
       

        
        
    }
    
    
    
    //metodo que checa en todo momento el contenido del textfield
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        
        if textField.text!.count >= 3 {
            self.sendMailButton.isEnabled = true
        }
        
    }
  
    //metodo que le pone una underline a un botón
    func underlineButton(button:UIButton){
        
        self.attributedString =  NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:button.titleLabel!.text!, attributes:attrs)
        attributedString.append(buttonTitleStr)
        
        button.setAttributedTitle(attributedString, for: .normal)
        
    }
   
    
    
    //metodo delegado cuando empieza a editar el textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.text = ""
        self.viewEmail.isHidden = true
        self.viewEmail.isUserInteractionEnabled = false
        self.sendMailButton.isEnabled = false
    }
 
 
//metodo que se ejecuta al presionar el botón de enviar correo
    @IBAction func sendMailAction(_ sender: UIButton) {
        
        self.mailTextField.endEditing(true)
        if let countryID = DataUser.COUNTRYID ,let imei = DataUser.IMEI{
        
        let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let params : [String : Any] = ["email":self.mailTextField.text!,"telefono":telefono,"imei":imei]
        
            print("PARAMS:")
        print(params)
     
            
            if isFrom == 1 || isFrom == 2 || isFrom == 3{
                
                  self.service.send(type: .POSTURLENCODED, url: "Usuarios/1/\(countryID)/\(NSLocalizedString("lang", comment: ""))/COMERCIO/emailVerification", params: params, header: headers, message: "Enviando",animate: false)
                
            }else{
            
        self.service.send(type: .POSTURLENCODED, url: "Usuarios/1/\(countryID)/\(NSLocalizedString("lang", comment: ""))/USUARIO/emailVerification", params: params, header: headers, message: "Enviando",animate: false)
                
            }
        
            
        }else{
            
            self.alert(title: "¡Aviso!", message: "Error de memoria , favor de cerrar y abrir la app nuevamente.", cancel: "OK")
        }
        
        
        
    }
    
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
     
        
       self.navigationController?
        .popViewController(animated: true)
          
    }
    
   
    
   
    //metodo que abre la app de mail
    @IBAction func openMailAction(_ sender: UIButton) {
        
        
        if let mailURL = URL(string: "message://"){
        if UIApplication.shared.canOpenURL(mailURL) {
            UIApplication.shared.open(mailURL, options: [:], completionHandler: nil)
        }
        }else{
            self.alert(title: "¡Aviso!", message: "No es posible abrir la app de correos, favor de entrar en su navegador.", cancel: "OK")
        }
        
    }
    
    
    
    //metodo que se ejecuta al presionar el botón de continuar
    @IBAction func continueAction(_ sender: UIButton) {
        //aqui validamos si ya verifico el correo
        
        
        if let countryID = DataUser.COUNTRYID{
            
            let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
            let params : [String : Any] = ["id":self.idSelection]
            
 
            if isFrom == 1 || isFrom == 2 || isFrom == 3{
                
                 self.service.send(type: .POSTURLENCODED, url: "Usuarios/1/\(countryID)/\(NSLocalizedString("lang", comment: ""))/COMERCIO/verify", params: params, header: headers, message: "Enviando",animate: false)
            }else{
            self.service.send(type: .POSTURLENCODED, url: "Usuarios/1/\(countryID)/\(NSLocalizedString("lang", comment: ""))/USUARIO/verify", params: params, header: headers, message: "Enviando",animate: false)
            }
            
            
        }else{
            
            self.alert(title: "¡Aviso!", message: "Error de memoria , favor de cerrar y abrir la app nuevamente.", cancel: "OK")
        }
        
        
        
        
        
        
    }
    
   
   
    //servicios delegate
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        var auxTemp = "USUARIO"
        
        if isFrom == 1 || isFrom == 2 || isFrom ==  3{
            
            auxTemp = "COMERCIO"
        }
        
        print(response)
        
        if endpoint == "Usuarios/1/\(DataUser.COUNTRYID!)/\(NSLocalizedString("lang", comment: ""))/\(auxTemp)/verify"{
            
            let code = response["idError"] as! Int
            
            if code == 0{
                
                
                switch self.isFrom{
                    
                    //usuario
                case 0:
               
                self.email = self.mailTextField.text!
                
                let storyboard = UIStoryboard(name: "register_look_and_feel", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Register_Step2") as! RegisterNew_Step2
                vc._telefono = self.telefono
                vc._email = self.email 
                self.navigationController?.pushViewController(vc, animated: true)
                break
                    
                    //empresa todas menos Peru
                case 1:
                    DataUser.MAIL_TEMP_REGISTER = self.mailTextField.text!
                    self.email = self.mailTextField.text!
                    let storyboard = UIStoryboard(name: "register_Commerce", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "registerCommercee") as! Register_Commerce_Step10
                    vc._telefono = self.telefono
                   
                    vc._email = self.email
                    self.navigationController?.pushViewController(vc, animated: true)
                    break
                    
                
                    
                    //empresa Peru
                case 2:
                    
                    DataUser.MAIL_TEMP_REGISTER = self.mailTextField.text!
                    self.email = self.mailTextField.text!
                    let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                    
                    let vc = storyboard.instantiateViewController(withIdentifier: "peRegisterIden") as! PERegisterCommerceViewController
                    vc.telefono = self.telefono as String
                    vc.email = self.email as String
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    break
                    
                default:
                    break
                    
                }
            
                
            }else{
                let mensaje = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
            
        }
        
        
        if endpoint == "Usuarios/1/\(DataUser.COUNTRYID!)/\(NSLocalizedString("lang", comment: ""))/\(auxTemp)/emailVerification"{
            
            let code = response["idError"] as! Int
            
            if code == 0{
                
                 DataUser.MAIL_TEMP_REGISTER = self.mailTextField.text!
              let id = response["id"] as! Int
                self.idSelection = id
                self.mailTextField.endEditing(true)
                self.viewEmail.isUserInteractionEnabled = true
                self.viewEmail.isHidden = false
               
                
            }else if code == 2{
                //si da el codigo de que ya esta verificado
               
                //si existe datos guardados
              //  if let mailTemp = DataUser.MAIL_TEMP_REGISTER{
                    
                    //si es el mismo
                   // if mailTemp == self.mailTextField.text!{
                        
                        self.email = self.mailTextField.text!
                        
                        switch self.isFrom{
                            
                            
                            //usuarios
                        case 0:
                        
                        let storyboard = UIStoryboard(name: "register_look_and_feel", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "Register_Step2") as! RegisterNew_Step2
                        vc._telefono = self.telefono
                        vc._email = self.email
                        self.navigationController?.pushViewController(vc, animated: true)
                            break
                            
                            //empresa todas menos peru
                        case 1:
                            
                            let storyboard = UIStoryboard(name: "register_Commerce", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "registerCommercee") as! Register_Commerce_Step10
                            vc._telefono = self.telefono 
                            vc._email = self.email
                            self.navigationController?.pushViewController(vc, animated: true)
                            break
                            
                            
                            //empresa peru
                        case 2:
                            
                            
                             let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
                             
                             let vc = storyboard.instantiateViewController(withIdentifier: "peRegisterIden") as! PERegisterCommerceViewController
                             vc.telefono = self.telefono as String
                             vc.email = self.email as String
                             self.navigationController?.pushViewController(vc, animated: true)
                            
                            
                            break
                            
                        default:
                            break
                            
                        }
                        
                   // }else{
                     //   let mensaje = response["mensajeError"] as! String
                   //     self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                        
                  //  }
                    
                    
              //  }else{
              //      let mensaje = response["mensajeError"] as! String
             //       self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                    
             //   }
                
            }else{
                let mensaje = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
            
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }

    
    func errorService(endpoint: String) {
        
    }
   
}








