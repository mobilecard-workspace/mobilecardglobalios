//
//  previvale_Step10.h
//  Mobilecard
//
//  Created by David Poot on 2/5/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol previvaleRegisterProtocol <NSObject>

@required
- (void) previvaleRegisterResponse:(NSDictionary*)response;

@end

@interface previvale_Step10 : mT_commonTableViewController <UITextFieldDelegate>

@property (assign, nonatomic) id <NSObject, previvaleRegisterProtocol> delegate;

@property (strong, nonatomic) NSDictionary *userData;



@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastnameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *motherlastnameText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *colonyText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *rfcText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *curpText;

@property (weak, nonatomic) IBOutlet UIButton *activate_button;
@end

NS_ASSUME_NONNULL_END
