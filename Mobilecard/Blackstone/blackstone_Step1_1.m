//
//  blackstone_Step1_1.m
//  Mobilecard
//
//  Created by David Poot on 7/15/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "blackstone_Step1_1.h"
#import "blackstone_Step2.h"

@interface blackstone_Step1_1 ()
{
    pootEngine *productManager;
    
    NSMutableArray *productArray;
    
    int selectedId;
}
@end

@implementation blackstone_Step1_1

- (void)viewDidLoad {
    [super viewDidLoad];
    
    productArray = [[NSMutableArray alloc] init];
    
    productManager = [[pootEngine alloc] init];
    [productManager setDelegate:self];
    [productManager setShowComments:developing];
    
    NSString *url = [NSString stringWithFormat:@"%@?carrierName=%@", WSGetBlackstoneProducts, [_dataSource[@"carrierName"] stringByAddingPercentEncodingWithAllowedCharacters:[[NSCharacterSet characterSetWithCharactersInString:@"& "] invertedSet]]];
    
    NSString *encodedURL = url;
    
    [productManager startWithoutPostRequestWithURL:encodedURL];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [productArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [productArray[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:productArray[indexPath.row][kIdentifierKey]];
    
    if ([productArray[indexPath.row][kIdentifierKey] isEqualToString:@"generic"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
                
                [label setText:productArray[indexPath.row][kDescriptionKey][@"name"]];
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedId = (int)indexPath.row;
    [self performSegueWithIdentifier:@"step2" sender:nil];
}

#pragma Mark pootEngine Handler
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == productManager) {
        NSArray *response = (NSArray*)json;
        
        [productArray removeAllObjects];
        
        [productArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"header", kIdentifierKey, @"70", kHeightKey, nil]];
        for (NSDictionary *element in response) {
            [productArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"generic", kIdentifierKey, @"70", kHeightKey, element, kDescriptionKey, nil]];
            [productArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
        }
        
        [[self tableView] reloadData];
    }
}

#pragma Mark segue handler

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"step2"]) {
        blackstone_Step2 *nextView = [segue destinationViewController];
        
        [nextView setDataSource:productArray[selectedId][kDescriptionKey]];
    }
}

@end
