//
//  blackstone_Step1.m
//  Mobilecard
//
//  Created by David Poot on 7/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "blackstone_Step1.h"
#import "blackstone_Step1_1.h"

@interface blackstone_Step1 ()
{
    pootEngine *countryManager;
    
    NSMutableArray *countryData;
    
    int selectedId;
}
@end

@implementation blackstone_Step1

- (void)viewDidLoad {
    [super viewDidLoad];
    
    countryManager = [[pootEngine alloc] init];
    [countryManager setDelegate:self];
    [countryManager setShowComments:developing];
    
    [countryManager startWithoutPostRequestWithURL:WSGetBlackstoneCarriers];
    
    [self lockViewWithMessage:nil];
    
    countryData = [[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [countryData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [countryData[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:countryData[indexPath.row][kIdentifierKey]];
    
    if ([countryData[indexPath.row][kIdentifierKey] isEqualToString:@"generic"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
                [label setText:countryData[indexPath.row][kDescriptionKey][@"carrierName"]];
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedId = (int)indexPath.row;
    [self performSegueWithIdentifier:@"step1_1" sender:nil];
}


- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == countryManager) {
        NSArray *response = (NSArray*)json;
        
        [countryData removeAllObjects];
        
        [countryData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"header", kIdentifierKey, @"70", kHeightKey, nil]];
        
        for (NSDictionary *element in response) {
            [countryData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"generic", kIdentifierKey, @"70", kHeightKey, element, kDescriptionKey, nil]];
            [countryData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
        }
        
        [self.tableView reloadData];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step1_1"]) {
        blackstone_Step1_1 *nextView = [segue destinationViewController];
        
        [nextView setDataSource:countryData[selectedId][kDescriptionKey]];
    }
}


@end
