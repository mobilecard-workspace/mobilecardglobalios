//
//  blackstone_result.m
//  Mobilecard
//
//  Created by David Poot on 7/17/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "blackstone_result.h"

@interface blackstone_result ()
{
    NSNumberFormatter *numFormatter;
}

@end

@implementation blackstone_result

- (void)viewDidLoad {
    [super viewDidLoad];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [_resultDetailsText setText:@""];
    
    switch ([_resultData[@"errorCode"] intValue]) {
        case 0:
            {
                [_resultIcon setImage:[UIImage imageNamed:@"icon_ok"]];
                [_resultTitleText setText:NSLocalizedString(@"¡Tu transacción se ha completado!", nil)];
                [_resultDetailsText setText:[NSString stringWithFormat:NSLocalizedString(@"Servicio: %@\nFecha: %@\nDestino: %@\nFolio: %@\nForma de pago: %@\nAutorización bancaria: %@\nMonto recarga: %@\nComisión: %@\nTotal cobrado(comisión incluida): %@\nMoneda: %@", nil), _resultData[@"carrierName"], _resultData[@"fecha"], _resultData[@"toppedUpNumber"], _resultData[@"transactionID"], _resultData[@"authorizationCode"], _resultData[@"maskCard"], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_resultData[@"monto"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_resultData[@"comision"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_resultData[@"total"] floatValue]]], @"USD"]];
                [_shareButton setHidden:NO];
            }
            break;
            
        default:
        {
            [_resultIcon setImage:[UIImage imageNamed:@"icon_nook"]];
            [_resultTitleText setText:_resultData[@"errorMessage"]];
            [_shareButton setHidden:YES];
        }
            break;
    }
    
    [_resultDetailsText setEditable:NO];
    [_resultDetailsText setSelectable:NO];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)share_Action:(id)sender {
    CGRect rect = CGRectMake(0, 0, _result.frame.size.width, _result.frame.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [_result drawViewHierarchyInRect:rect afterScreenUpdates:YES];
    UIImage *snapShotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSMutableArray *activityItems = [NSMutableArray arrayWithObjects:snapShotImage, nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypePrint,                                                         UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,                                                         UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList,                                                         UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,
        UIActivityTypePostToTencentWeibo,UIActivityTypeAirDrop];
    
    [self presentActivityController:activityViewController];
}

- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    //controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            
            // user shared an item
            //NSLog(@"We used activity type%@", activityType);
            
        } else {
            
            // user cancelled
            //NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            //NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

@end
