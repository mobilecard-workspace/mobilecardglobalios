//
//  blackstone_Summary.h
//  Mobilecard
//
//  Created by David Poot on 7/15/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol blackstoneSummaryDelegate <NSObject>

@required 
- (void) summaryResult:(NSDictionary*)result;

@end

@interface blackstone_Summary : mT_commonTableViewController

@property (nonatomic, assign) id <NSObject, blackstoneSummaryDelegate> delegate;


@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneNumberText;
@property (strong, nonatomic) NSString *phoneNumberString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *amountText;
@property (strong, nonatomic) NSString *amountString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *feeText;
@property (strong, nonatomic) NSString *feeString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *totalText;
@property (strong, nonatomic) NSString *totalString;

@property (weak, nonatomic) IBOutlet UIButton *summaryButton;

@end
