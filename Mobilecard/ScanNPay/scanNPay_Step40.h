//
//  scanNPay_Step40.h
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface scanNPay_Step40 : mT_commonTableViewController

@property (weak, nonatomic) IBOutlet UITextView *resultTextView;
@property (strong, nonatomic) NSDictionary *resultData;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@property (weak, nonatomic) IBOutlet UIImageView *QRImageView;
@end
