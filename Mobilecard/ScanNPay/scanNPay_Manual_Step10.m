//
//  scanNPay_Manual_Step10.m
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "scanNPay_Manual_Step10.h"
#import "scanNPay_Step30.h"

@interface scanNPay_Manual_Step10 ()
{
    pootEngine *commerceManager;
}
@end

@implementation scanNPay_Manual_Step10

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:_continue_Button];
    
    [self initializePickersForView:self.navigationController.view];
    
    [_commerceText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_amountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_amountText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]];
    [_amountText setKeyboardType:UIKeyboardTypeDecimalPad];
    [_tipText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_tipText setKeyboardType:UIKeyboardTypeDecimalPad];
    [_tipText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]];
    [_conceptText setUpTextFieldAs:textFieldTypeAddress];
    
    [self addValidationTextField:_commerceText];
    [self addValidationTextField:_amountText];
    [self addValidationTextField:_tipText];
    [self addValidationTextField:_conceptText];
    
    [self addValidationTextFieldsToDelegate];
    
    commerceManager = [[pootEngine alloc] init];
    [commerceManager setDelegate:self];
    [commerceManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[NSNumber numberWithInt:0] forKey:@"lat"];
    [params setObject:[NSNumber numberWithInt:0] forKey:@"lon"];
    [params setObject:@"" forKey:@"ubicacion_actual"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [commerceManager startJSONRequestWithURL:[[NSString stringWithFormat:@"%@/LCPFServices/%@/getEstablecimientos", simpleServerURL, NSLocalizedString(@"lang", nil)] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] withPost:JSONString];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    [self performSegueWithIdentifier:@"step20" sender:nil];
}

#pragma mark pootEngine handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == commerceManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            [_commerceText setInfoArray:response[@"accounts"]];
            [_commerceText setIdLabel:@"id"];
            [_commerceText setDescriptionLabel:@"alias"];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step20"]) {
        
        NSDictionary *commerceData = [_commerceText infoArray][[_commerceText selectedID]];
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        float comisionfija = [commerceData[@"comision_fija"] floatValue];
        float comisionPorcentaje = [commerceData[@"comision_porcentaje"] floatValue];
        float monto = [_amountText.text floatValue];
        
        float comision = comisionfija + (monto + comisionfija)*comisionPorcentaje;
        [data setObject:[NSNumber numberWithFloat:comision] forKey:@"comision"];
        [data setObject:_amountText.text forKey:@"amount"];
        [data setObject:[_tipText.text length]==0?@"0":_tipText.text forKey:@"propina"];
        [data setObject:_conceptText.text forKey:@"concept"];
        [data setObject:[_commerceText infoArray][[_commerceText selectedID]][[_commerceText descriptionLabel]] forKey:@"referenciaNeg"];
        [data setObject:[_commerceText infoArray][[_commerceText selectedID]][[_commerceText idLabel]] forKey:@"establecimientoId"];
        
        scanNPay_Step30 *nextView = [segue destinationViewController];
        
        [nextView setData:data];
        [nextView setShowCardOptionsFirst:YES];
    }
}


@end
