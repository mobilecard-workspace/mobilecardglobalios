//
//  scanNPay_Step30.m
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "scanNPay_Step30.h"
#import "commerce_QRPayment_Step30.h"
#import <Mobilecard-Swift.h>

@interface scanNPay_Step30 ()
{
    NSNumberFormatter *numFormatter;
    
    NSDictionary *cardSelection;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    
    pootEngine *cardManager;
    pootEngine *paymentManager;
    
    
   // THMTrustDefender *profile;
    
    NSMutableDictionary *threatMetrixInfo;
    NSDictionary *selectedCardInfo;
}

@end

@implementation scanNPay_Step30

- (void)viewDidLoad {
    [super viewDidLoad];
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
  //  [self addShadowToView:_continue_Button];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setCurrencySymbol:@"$"];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
     [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [self addValidationTextField:_emailText];
    
    if (_msiText) {
        [self initializePickersForView:self.navigationController.view];
        
        [_msiText setUpTextFieldAs:textFieldTypeRequiredCombo];
        [self addValidationTextField:_msiText];
        [self addValidationTextFieldsToDelegate];
        
        [_msiText setInfoArray:[NSArray arrayWithObjects:
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago en una sola exhibición", kDescriptionKey, @"0", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 3 meses sin intereses", kDescriptionKey, @"3", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 6 meses sin intereses", kDescriptionKey, @"6", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 9 meses sin intereses", kDescriptionKey, @"9", kIDKey, nil],
                                nil]];
        
        [_msiText setDescriptionLabel:(NSString*)kDescriptionKey];
        [_msiText setIdLabel:(NSString*)kIDKey];
        
        [_msiText setSelectedID:0];
        [_msiText setText:[_msiText infoArray][[_msiText selectedID]][[_msiText descriptionLabel]]];
    }
    
    
    if (_showCardOptionsFirst) {
        [self showCardOptions];
    }
    
    [_resultTextView setText:@""];
    
    [self refreshView];
}

- (void) refreshView
{
    [_resultTextView setSelectable:YES];
    [_resultTextView setEditable:YES];
    
    if (_data) {
        
        if ([DataUser.COUNTRYID  isEqual: @"3"]){
            //USA
            if ([_data[@"comision"] floatValue]==0) {
                if (cardSelection) {
                    if ([cardSelection[@"tipoTarjeta"] isEqualToString:@"CREDITO"] && ([cardSelection[@"tipo"] isEqualToString:@"MasterCard"]||[cardSelection[@"tipo"] isEqualToString:@"VISA"])) {
                        [_msiText setEnabled:YES];
                    } else {
                        [_msiText setEnabled:NO];
                    }
                    [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@\n\nTARJETA PAGO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount_usd"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"], [self maskCardStringWithString:[cardManager MCDecryptString:cardSelection[@"pan"] withSensitive:NO]]]];
                } else {
                    [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount_usd"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"]]];
                }
            } else {
                if (cardSelection) {
                    if ([cardSelection[@"tipoTarjeta"] isEqualToString:@"CREDITO"] && ([cardSelection[@"tipo"] isEqualToString:@"MasterCard"]||[cardSelection[@"tipo"] isEqualToString:@"VISA"])) {
                        [_msiText setEnabled:YES];
                    } else {
                        [_msiText setEnabled:NO];
                    }
                    [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nCOMISIÓN: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@\n\nTARJETA PAGO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount_usd"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"comision"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"], [self maskCardStringWithString:[cardManager MCDecryptString:cardSelection[@"pan"] withSensitive:NO]]]];
                } else {
                    [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nCOMISIÓN: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount_usd"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"comision"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"]]];
                }
            }
        }
        else if ([DataUser.COUNTRYID  isEqual: @"1"]) {
            //MEXICO
            NSLog(@"SI ENTRO AQUI Y EL TOTAL ES");
            NSLog(@"%@",_data[@"amount_mxn"]);
            if ([_data[@"comision"] floatValue]==0) {
                if (cardSelection) {
                    if ([cardSelection[@"tipoTarjeta"] isEqualToString:@"CREDITO"] && ([cardSelection[@"tipo"] isEqualToString:@"MasterCard"]||[cardSelection[@"tipo"] isEqualToString:@"VISA"])) {
                        [_msiText setEnabled:YES];
                    } else {
                        [_msiText setEnabled:NO];
                    }
                    [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@\n\nTARJETA PAGO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount_mxn"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"], [self maskCardStringWithString:[cardManager MCDecryptString:cardSelection[@"pan"] withSensitive:NO]]]];
                } else {
                    [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount_mxn"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"]]];
                }
            } else {
                if (cardSelection) {
                    if ([cardSelection[@"tipoTarjeta"] isEqualToString:@"CREDITO"] && ([cardSelection[@"tipo"] isEqualToString:@"MasterCard"]||[cardSelection[@"tipo"] isEqualToString:@"VISA"])) {
                        [_msiText setEnabled:YES];
                    } else {
                        [_msiText setEnabled:NO];
                    }
                    [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nCOMISIÓN: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@\n\nTARJETA PAGO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount_mxn"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"comision"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"], [self maskCardStringWithString:[cardManager MCDecryptString:cardSelection[@"pan"] withSensitive:NO]]]];
                } else {
                    [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nCOMISIÓN: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount_mxn"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"comision"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"]]];
                }
            }
            
            
        }else{
        //CUALQUIER OTRO PAIS
        if ([_data[@"comision"] floatValue]==0) {
            if (cardSelection) {
                if ([cardSelection[@"tipoTarjeta"] isEqualToString:@"CREDITO"] && ([cardSelection[@"tipo"] isEqualToString:@"MasterCard"]||[cardSelection[@"tipo"] isEqualToString:@"VISA"])) {
                    [_msiText setEnabled:YES];
                } else {
                    [_msiText setEnabled:NO];
                }
                [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@\n\nTARJETA PAGO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"], [self maskCardStringWithString:[cardManager MCDecryptString:cardSelection[@"pan"] withSensitive:NO]]]];
            } else {
                [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"]]];
            }
        } else {
            if (cardSelection) {
                if ([cardSelection[@"tipoTarjeta"] isEqualToString:@"CREDITO"] && ([cardSelection[@"tipo"] isEqualToString:@"MasterCard"]||[cardSelection[@"tipo"] isEqualToString:@"VISA"])) {
                    [_msiText setEnabled:YES];
                } else {
                    [_msiText setEnabled:NO];
                }
                [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nCOMISIÓN: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@\n\nTARJETA PAGO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"comision"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"], [self maskCardStringWithString:[cardManager MCDecryptString:cardSelection[@"pan"] withSensitive:NO]]]];
            } else {
                [_resultTextView setText:[NSString stringWithFormat:@"MONTO: %@\nPROPINA: %@\nCOMISIÓN: %@\nDESCRIPCIÓN DEL SERVICIO:\n%@\n\nESTABLECIMIENTO:\n%@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"amount"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"propina"] floatValue]]], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_data[@"comision"] floatValue]]], _data[@"concept"], _data[@"referenciaNeg"]]];
            }
        }
    }//TERMINA IF VALIDACION PAIS
    }
    
    [_resultTextView setSelectable:NO];
    [_resultTextView setEditable:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    if (_showCardOptionsFirst) {
        [self performPaymentWithCard:selectedCardInfo];

       // [self performProfileCheck];
    } else {
        [self showCardOptions];
    }
}


#pragma Mark Handling Wallet

- (void) showCardOptions
{
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
     //   UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
       // [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [DataUser clean];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view set_whiteList:__whiteList];
    [view setType:viewType];
    [view set_isBackDissmis:__isBackDissmis];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    if (_showCardOptionsFirst) {
        cardSelection = [NSDictionary dictionaryWithDictionary:selectedCard];
        selectedCardInfo = selectedCard;
        [self refreshView];
    } else {
        selectedCardInfo = selectedCard;
      //  [self performProfileCheck];
        [self performPaymentWithCard:selectedCardInfo];

    }
}

- (void)cardSelectionAborted
{
    if (_showCardOptionsFirst) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) performPaymentWithCard:(NSDictionary*)card
{
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:_data];
    
    
    //si es mexico o estados unidos sobreescribe el amount por el correcto en su moneda
    if ([DataUser.COUNTRYID  isEqual: @"1"]){
        
        [params setObject:_data[@"amount_mxn"] forKey:@"amount"];
    }else if ([DataUser.COUNTRYID  isEqual: @"3"]){
        
         [params setObject:_data[@"amount_usd"] forKey:@"amount"];
    }
  
    //[params setObject:@"" forKey:@"amount"]; PASSED FROM PREVIOUS WINDOW
    //[params setObject:@"" forKey:@"comision"];
    //[params setObject:@"" forKey:@"concept"];
    [params setObject:@"" forKey:@"ct"];
    //[params setObject:@"" forKey:@"establecimientoId"];
    [params setObject:@"" forKey:@"firma"];
    [params setObject:[NSNumber numberWithInt:0] forKey:@"idBitacora"];
    [params setObject:card[@"idTarjeta"] forKey:@"idCard"];
    [params setObject:userData[kUserIDKey] forKey:@"idUser"];
    [params setObject:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] doubleValue]] forKey:@"lat"];
    [params setObject:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] doubleValue]] forKey:@"lon"];
    [params setObject:@"0" forKey:@"msi"];
    //[params setObject:@"" forKey:@"propina"];
    //[params setObject:@"" forKey:@"referenciaNeg"];
    
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"modelo"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
    
    [params setObject:@"" forKey:@"tarjeta"];
    [params setObject:@"" forKey:@"vigencia"];
    [params setObject:@"" forKey:@"tipoTarjeta"];
    
        [params setObject:_emailText.text forKey:@"email"];
    NSLog(@"Estos son los parametros %@",params);
    
    
    
    if (_msiText) {
        [params setObject:[_msiText infoArray][[_msiText selectedID]][[_msiText idLabel]] forKey:@"msi"];
    }
    
    paymentManager = [[pootEngine alloc] init];
    [paymentManager setDelegate:self];
    [paymentManager setShowComments:developing];
    
    Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
    [nextView setPurchaseInfo:[NSMutableDictionary dictionaryWithDictionary:params]];
    [nextView setSecure3DURL:[NSString stringWithFormat:@"%@/CuantoTeDebo/%d/%d/%@/usuario/%@/pagoBP", simpleServerURL, idApp, [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"] intValue], NSLocalizedString(@"lang", nil), [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]]];
    [nextView setType:serviceTypeCuantoTeDebo];
    [nextView setThreatMetrixInfo:threatMetrixInfo];
     [nextView setSelectedCardInfo:[NSMutableDictionary dictionaryWithDictionary:card]];
    [nextView setDelegate:self];
    
    [self.navigationController pushViewController:nextView animated:YES];
}

#pragma Mark pootEngine handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
    }
}

- (void)Secure3DResponse:(NSDictionary *)response
{
    developing?NSLog(@"Result -> %@", response):nil;
    [self performSegueWithIdentifier:@"result" sender:response];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"result"]) {
        commerce_QRPayment_Step30 *nextView = [segue destinationViewController];
        
        NSDictionary *response = (NSDictionary*)sender;
        
        [nextView setResultData:response];
    }
}



#pragma Mark TrustDefender Handling
/*
- (void) performProfileCheck
{
    [self lockViewWithMessage:nil];
    
    
    profile = [THMTrustDefender sharedInstance];
    
    [profile configure:@{
                         THMOrgID:@"c2ggoqh1",
                         THMFingerprintServer:@"h-sdk.online-metrix.net",
                         }];
    
    [profile doProfileRequestWithCallback:^(NSDictionary *result)
     {
         [self unLockView];
         developing?NSLog(@"PROFILE RESULT -> %@", result):nil;
         self->threatMetrixInfo = [NSMutableDictionary dictionaryWithDictionary:result];
         
         THMStatusCode statusCode = [[result valueForKey:THMProfileStatus] integerValue];
         
         if (statusCode == THMStatusCodeOk) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self performPaymentWithCard:selectedCardInfo];
             });
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:@"%s", statusCode == THMStatusCodeNetworkTimeoutError ? "Timed out"                                                                                                                                                                : statusCode == THMStatusCodeConnectionError     ? "Connection Error"                                                                                                                                                           : statusCode == THMStatusCodeHostNotFoundError   ? "Host not found error"                                                                                                                                                : statusCode == THMStatusCodeInternalError       ? "Internal Error"                                                                                                                                                        : statusCode == THMStatusCodeInterruptedError    ? "Interrupted"                                                                                                                                                           : "other"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [alertMsg dismissViewControllerAnimated:YES completion:nil];
                 }];
                 
                 [alertMsg addAction:ok];
                 
                 [self presentViewController:alertMsg animated:YES completion:nil];
             });
         }
         
         
     }];
}*/

- (void)PaymentWithThreatMetrixResponse:(NSDictionary *)response
{
    if (response) {
        [self performSegueWithIdentifier:@"result" sender:response];
        
        if ([response[@"code"] intValue] == 0) {
            
        };
    }
}







#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}
@end
