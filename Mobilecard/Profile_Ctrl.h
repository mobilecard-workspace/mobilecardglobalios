//
//  Profile_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 10/30/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface Profile_Ctrl : mT_commonTableViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastnameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passwordText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *countryText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UISwitch *editSwitch;
@property (weak, nonatomic) IBOutlet UITextField_Validations *validationText;
@end
