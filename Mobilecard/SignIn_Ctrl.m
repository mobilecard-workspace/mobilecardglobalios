//
//  SignIn_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 10/25/17.
//  Copyright © 2017 David Poot. All rights reserved.
//


#import "SignIn_Ctrl.h"
#import "MainMenu_Ctrl.h"
#import "NSDictionary (keychain).h"
#import "RegisterNew_Step1.h"
#import <Mobilecard-Swift.h>

@interface SignIn_Ctrl ()
    {
        pootEngine *loginManager;
        pootEngine *changeManager;
         ServicesObjc *services;
        
        NSDictionary *preliminaryUserData;
    }
    
    @end

@implementation SignIn_Ctrl
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self autologin];
    services = [[ServicesObjc alloc] init];
    services.delegate = self;
    [self.tableView setContentInset:UIEdgeInsetsMake(40, 0, 0, 0)];
    [_usernameText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_usernameText setMaxLength:50];
    [_usernameText setMinLength:4];
    [_passText setUpTextFieldAs:textFieldTypePassword];
    
    [self addValidationTextField:_usernameText];
    [self addValidationTextField:_passText];
    
    [self addValidationTextFieldsToDelegate];
  
    
    /*if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey] && [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword]) {
        
        
        
        
        developing?NSLog(@"Keychain -> %@", kKeychainGroup):nil;
        
        [self performSegueWithIdentifier:@"main_menu" sender:nil];
    }*/
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated{
    
     DataUser.STATELOGIN = nil;
    
    if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"SessionSMS_To_Menu"] isEqualToString:@"1"]){
        [self startSession_Action:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"SessionSMS_To_Menu"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}



- (IBAction)back_Action:(id)sender {
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    if ([userData[kUserIdCountry] intValue]==1) {
        NSArray *viewControllers = [[self navigationController] viewControllers];
        for( int i=0;i<[viewControllers count];i++){
            id obj=[viewControllers objectAtIndex:i];
            if([obj isKindOfClass:[RegisterNew_Step1 class]]){
                [[self navigationController] popToViewController:obj animated:YES];
                return;
            }
        }
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)register_Action:(id)sender {
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"register_look_and_feel"bundle:nil];
    UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Register_Step2"];
    [self.navigationController pushViewController:scrollview animated:YES];*/
  
    //ahora se pude el sms primero en el registro de usuario
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
    
    RegisterSmsViewController *nextvc = [nextStory instantiateViewControllerWithIdentifier:@"smsRegister"];
    
    [self.navigationController pushViewController:(nextvc) animated:YES];
    
    
}




-(void)autologin{
    
     //autologin Alejandro
        if(DataUser.PASS != nil){
            //pais
        if( [DataUser.COUNTRY isEqualToString:@"PERU"] || [DataUser.COUNTRY isEqualToString:@"PE"]){
            DataUser.COUNTRY = @"PE";
            
            //negocio
            if([DataUser.USERTYPE isEqualToString:@"NEGOCIO"]){
            
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                UIViewController *viewcontroller =(ContainerSliderPEViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Container"];
                
                UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                
                [navs setNavigationBarHidden:YES animated:NO];
                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:navs animated:YES completion:nil];
            
                
            }else{
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                
                UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                
                [navs setNavigationBarHidden:YES animated:NO];
                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:navs animated:YES completion:nil];
             
                
                
                //[self performSegueWithIdentifier:@"startMain" sender:nil];
            }
        
        }else{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]) {
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
                    //[self performSegueWithIdentifier:@"startMain" sender:nil];
                    ////// reemplazo de codigo para ingresar al view de log-in Raul mendez ///////////
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                    
                    //[self performSegueWithIdentifier:@"startMain" sender:nil];
                } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
                   
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainCommerce"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuCommerce"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                    
                    //   [self performSegueWithIdentifier:@"startMain_Commerce" sender:nil];
                } else {
                 
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                   
                    
                    // [self performSegueWithIdentifier:@"startMain" sender:nil];
                }
            }
        }
            
            
        }else{
    
    //termina login
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
            //[self performSegueWithIdentifier:@"startMain" sender:nil];
            ////// reemplazo de codigo para ingresar al view de log-in Raul mendez ///////////
             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                              UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                              
                              UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                              
                              [navs setNavigationBarHidden:YES animated:NO];
                              navs.modalPresentationStyle = UIModalPresentationFullScreen;
                              navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                              
                              [self presentViewController:navs animated:YES completion:nil];
            
        } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
          
          UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainCommerce"bundle:nil];
                             UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuCommerce"];
                             
                             UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                             
                             [navs setNavigationBarHidden:YES animated:NO];
                             navs.modalPresentationStyle = UIModalPresentationFullScreen;
                             navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                             
                             [self presentViewController:navs animated:YES completion:nil];
           
            //  [self performSegueWithIdentifier:@"startMain_Commerce" sender:nil];
        } else {
           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
            UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
            
            UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
            
            [navs setNavigationBarHidden:YES animated:NO];
            navs.modalPresentationStyle = UIModalPresentationFullScreen;
            navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            [self presentViewController:navs animated:YES completion:nil];
        }
    }
            
        }//termina el if del pass != nil que lo otro era el autologin pasado
    
    
}

- (IBAction)startSession_Action:(id)sender {
    [self dismissAllTextFields];
    
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    loginManager = [[pootEngine alloc] init];
    [loginManager setDelegate:self];
    [loginManager setShowComments:developing];
    
   // NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [[NSUserDefaults standardUserDefaults] setObject:_usernameText.text forKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] synchronize];
 /*   [params setObject:_usernameText.text forKey:@"usrLoginOrEmail"];
    [params setObject:[loginManager encryptJSONString:_passText.text withPassword:nil] forKey:kUserPassword];
    //MUST HAVE PARAMETERS
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"manufacturer"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
    [params setObject:@"iOS" forKey:@"platform"];
    [params setObject:@"USUARIO" forKey:@"tipoUsuario"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];*/
    

    
    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
    NSString *idCountry = [[NSString alloc] init];
  
    if ([[countryInfo objectForKey:kIDKey] intValue]) {
        idCountry = [countryInfo objectForKey:kIDKey];
    } else {
        idCountry = @"1";
    }
    
    NSDictionary *header = @{};
    NSDictionary *params = @{@"usrLoginOrEmail":_usernameText.text,kUserPassword:[loginManager encryptJSONString:_passText.text withPassword:nil],@"imei":[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"manufacturer":@"Apple",@"os":[[UIDevice currentDevice] systemVersion],@"platform":@"iOS",@"tipoUsuario":@"USUARIO"};
    
    [services sendWithType:@"POST" url:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, idCountry, (NSLocalizedString(@"lang", nil)), @"/user/login"] params:params header:header message:@"Ingresando" animate:YES];
 
/*
    [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, idCountry, (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
    
    [self lockViewWithMessage:NSLocalizedString(@"Iniciando sesión...", nil)];*/
}
    
- (IBAction)resetPassword_Action:(id)sender {
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Recuperar contraseña", nil) message:NSLocalizedString(@"Escribe tu nombre de usuario o correo electrónico en el campo para recuperar tu contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Recuperar contraseña", nil), nil];
    [alertMsg setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alertMsg setTag:199];
    [alertMsg show];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        if ([segue.identifier isEqualToString:@"main_menu"]) {
            UINavigationController *navController = segue.destinationViewController;
            MainMenu_Ctrl *main_controller = [navController childViewControllers].firstObject;
        }
    }
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
    {
        [self unLockView];
    /*    if (manager == loginManager) {
            NSDictionary *response = (NSDictionary*)json;
            response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
            
            if ([response[kIDError] intValue] == 0) {
                NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                
                
                
                DataUser.COUNTRY = [countryInfo objectForKey:kDetailsKey];  ////// Raul mendez
                DataUser.EMAIL =  _usernameText.text;
                DataUser.PASS =  [loginManager encryptJSONString:_passText.text withPassword:nil];
                DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                DataUser.APPID = @"iOS";
                DataUser.USERTYPE = @"USUARIO";
                DataUser.OS = [[UIDevice currentDevice] systemVersion];
                DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                DataUser.MANOFACTURER =@"Apple";
                
               
                
                [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
               // [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
             
                
                [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                
               
                
                
                switch ([response[kIDJumio] intValue]) {
                /*    case 0:
                    {
                        
                        if (response.count > 1){
                           [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                       [self performSegueWithIdentifier:@"init_boarding_jumio" sender:nil];
                            return;
                        }else{
                           UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                           
                          
                            [alertMsg show];
                            return;
                            
                        }
                    }
                    break;*/
                 //   case 1:
                 //   case 0:
                   // case 2:
                   // case 3:
                        
                 //   {
                   //      switch ([response[kIDStatus] intValue]) {
                        //  switch (100) {
                     /*       case 98:
                            {
                                preliminaryUserData = response;
                                
                                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                                [alertMsg setTag:3031];
                                [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                                [alertMsg show];
                                return;
                            }
                            break;
                            
                            default:
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                                [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
          
                                
                                [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                                [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                                [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                
                                [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                                [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                                
                                [self performSegueWithIdentifier:@"main_menu" sender:nil];
                            }
                            break;
                         
                        }
                        
                    }
                    break;
                /*    case 2:
                    {
                        //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                        [alertMsg show];
                        
                        return;
                    }
                    break;*/
                   /* case 3:
                    {
                   
                           [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                        [[NSUserDefaults standardUserDefaults] setObject:response[@"ideUsuario"] forKey:(NSString*)@"ideUsuario"];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
                        return;
                    }
                    break;*/
         /*       }
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }*/
        if (changeManager == manager) {
            NSDictionary *response = (NSDictionary*)json;
            switch (changeManager.tag) {
                case 3031:
                {
                    if ([response[kIDError] intValue] == 0) {
                        
                        [preliminaryUserData setValue:@"1" forKey:@"idUsrStatus"];
                        [[NSUserDefaults standardUserDefaults] setObject:_validationText.text forKey:(NSString*)kUserPassword];
                        [[NSUserDefaults standardUserDefaults] setObject:preliminaryUserData forKey:(NSString*)kUserDetailsKey];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alertMsg show];
                        
                        [self performSegueWithIdentifier:@"main_menu" sender:nil];
                    } else {
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alertMsg show];
                    }
                }
                break;
                case 199:
                {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
                break;
                default:
                break;
            }
        }
    }
#pragma mark register Delegate
- (void)registryShallLaunchMyMCWithUserData:(NSDictionary *)userdata
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
        UINavigationController *nav = [storyboard instantiateInitialViewController];
        MCCard_Ctrl *nextView = [nav childViewControllers].firstObject;
        [nextView setUserData:userdata];
        
        [super setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [super presentViewController:nav animated:YES completion:nil];
    }
#pragma Mark UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
    {
        if (!(buttonIndex == 0)) {
            switch (alertView.tag) {
                case 3031: //new password request
                {
                    [_validationText setUpTextFieldAs:textFieldTypePassword];
                    [_validationText setText:[alertView textFieldAtIndex:0].text];
                    
                    if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                        changeManager = [[pootEngine alloc] init];
                        [changeManager setDelegate:self];
                        [changeManager setShowComments:developing];
                        [changeManager setTag:alertView.tag];
                        
                        NSString *post = nil;
                        post = [NSString stringWithFormat:@"old=%@&new=%@", [self convertToURLCompatible:[changeManager encryptJSONString:_passText.text withPassword:nil]], [self convertToURLCompatible:[changeManager encryptJSONString:_validationText.text withPassword:nil]]];
                        
                        [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@", userManagementURL,NSLocalizedString(@"lang", nil),@"/",[preliminaryUserData objectForKey:kUserIDKey],@"/password/update"] withPost:post];
                        
                        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                    }
                    
                }
                break;
                
                case 199: //reset password
                {
                    [_validationText setUpTextFieldAs:textFieldTypeUserName];
                    [_validationText setText:[alertView textFieldAtIndex:0].text];
                    
                    if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                        changeManager = [[pootEngine alloc] init];
                        [changeManager setDelegate:self];
                        [changeManager setShowComments:developing];
                        [changeManager setTag:alertView.tag];
                        
                        
                        NSString *post = nil;
                        post = [NSString stringWithFormat:@"userOrEmail=%@", _validationText.text];
                        
                        [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/password/reset"] withPost:post];
                        
                        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                    }
                }
                default:
                break;
            }
        }
    }
- (NSString*) convertToURLCompatible: (NSString*)stringy
    {
        NSString * escapedUrlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)stringy,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]ÁáÉéÍíÓóÚúñ. %",kCFStringEncodingUTF8));
        return escapedUrlString;
    }


//delegados de los servicios
- (void)responseServiceWithType:(NSString *)type endpoint:(NSString *)endpoint response:(NSDictionary *)response{
    
    NSLog(@"RESPONSE:%@",response);
    
    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
       NSString *idCountry = [[NSString alloc] init];
     
       if ([[countryInfo objectForKey:kIDKey] intValue]) {
           idCountry = [countryInfo objectForKey:kIDKey];
       } else {
           idCountry = @"1";
       }
    
     if ([endpoint isEqualToString:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, idCountry, (NSLocalizedString(@"lang", nil)), @"/user/login"]]) {
          
          response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
          
          if ([response[kIDError] intValue] == 0) {
              NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
              
              
              
              DataUser.COUNTRY = [countryInfo objectForKey:kDetailsKey];  ////// Raul mendez
              DataUser.EMAIL =  _usernameText.text;
              DataUser.PASS =  [loginManager encryptJSONString:_passText.text withPassword:nil];
              DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
              DataUser.APPID = @"iOS";
              DataUser.USERTYPE = @"USUARIO";
              DataUser.OS = [[UIDevice currentDevice] systemVersion];
              DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
              DataUser.MANOFACTURER =@"Apple";
              
             
              
              [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
             // [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
           
              
              [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
              [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
              [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
              [[NSUserDefaults standardUserDefaults] synchronize];
              
              [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
              [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
              
             
              
              
              switch ([response[kIDJumio] intValue]) {
              /*    case 0:
                  {
                      
                      if (response.count > 1){
                         [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                     [self performSegueWithIdentifier:@"init_boarding_jumio" sender:nil];
                          return;
                      }else{
                         UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                         
                        
                          [alertMsg show];
                          return;
                          
                      }
                  }
                  break;*/
                  case 1:
                  case 0:
                  case 2:
                  case 3:
                      
                  {
                       switch ([response[kIDStatus] intValue]) {
                      //  switch (100) {
                          case 98:
                          {
                              preliminaryUserData = response;
                              
                              UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                              [alertMsg setTag:3031];
                              [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                              [alertMsg show];
                              return;
                          }
                          break;
                          
                          default:
                          {
                              [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                              [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
        
                              
                              [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                              [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                              [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
                              [[NSUserDefaults standardUserDefaults] synchronize];
                              
                              [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                              [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                              
                              [self performSegueWithIdentifier:@"main_menu" sender:nil];
                          }
                          break;
                       
                      }
                      
                  }
                  break;
              /*    case 2:
                  {
                      //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
                      UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                      [alertMsg show];
                      
                      return;
                  }
                  break;*/
                 /* case 3:
                  {
                 
                         [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                      [[NSUserDefaults standardUserDefaults] setObject:response[@"ideUsuario"] forKey:(NSString*)@"ideUsuario"];
                      
                      [[NSUserDefaults standardUserDefaults] synchronize];
                      [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
                      return;
                  }
                  break;*/
              }
          } else {
              UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
              [alertMsg show];
          }
      }
    
}

- (void)responseArrayServiceWithType:(NSString *)type endpoint:(NSString *)endpoint response:(NSArray *)response{
    
    
   
}



- (void)errorServiceWithEndpoint:(NSString *)endpoint{
    
    

    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"No fue posible establecer conexión con el servidor.", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
      
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
    
}

    @end
