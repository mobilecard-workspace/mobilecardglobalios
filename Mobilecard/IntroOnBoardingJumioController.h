//
//  IntroOnBoardingJumioController.h
//  Mobilecard
//
//  Created by Raul Mendez on 7/17/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetverifyStartViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntroOnBoardingJumioController : UIViewController
@property (nonatomic, assign) int typeUser;
@end

NS_ASSUME_NONNULL_END
