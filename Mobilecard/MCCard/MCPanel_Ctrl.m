//
//  MCPanel_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 11/15/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "MCPanel_Ctrl.h"
#import "MC_Colors.h"
#import <Mobilecard-Swift.h>


@interface MCPanel_Ctrl ()
{
    NSMutableArray *menuItems;
   
    IBOutlet UITableView *tableView;
    int aux;
    pootEngine *cardManager;
  
  
    NSNumberFormatter *numFormatter;
   
    
    int standardItems;
}
@end

@implementation MCPanel_Ctrl

    int aux = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    cardManager = [[pootEngine alloc] init];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    menuItems = [[NSMutableArray alloc] init];
    
    if (_mcCardInfo) {
        //INFO AVAILABLE NO NEED TO SEARCH FOR CARD.
        developing?NSLog(@"CARD INFO PASSED -> %@", _mcCardInfo):nil;
    } else {
        for (NSDictionary *cardElement in [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserCardArray]) {
            if ([cardElement[@"mobilecard"] boolValue]) {
                _mcCardInfo = [[NSDictionary alloc] initWithDictionary:cardElement];
            }
        }
    }
  
    
    
    [self updateMenuItems];
}

- (void)userTappedClabe:(UIGestureRecognizer*)gestureRecognizer{
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _mcCardInfo[@"clabe"];
    
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:@"Clabe copiada en portapapeles con éxito." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertMsg show];
}

- (void) updateMenuItems
{
    [menuItems removeAllObjects];
    
    //Adding standard items
    [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"header", kIdentifierKey, @"40", kHeightKey, nil]];
    [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"cardView", kIdentifierKey, @"180", kHeightKey, nil]];
    //[menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"activate", kIdentifierKey, @"100", kHeightKey, nil]];
    [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"balance", kIdentifierKey, @"105", kHeightKey, nil]];
    
    [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"movimientos", kIdentifierKey, @"100", kHeightKey, nil]];
    
    [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"100", kHeightKey, nil]];
    [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"transaction_Header", kIdentifierKey, @"70", kHeightKey, nil]];
    
    standardItems = (int)[menuItems count];
    //add transactions
    /*if (![_mcCardInfo[kUserCardTransactions] isKindOfClass:[NSString class]]) {
        for (int i = 0; i<[_mcCardInfo[kUserCardTransactions] count]; i++) {
            
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"transactionCell", kIdentifierKey, @"55", kHeightKey, nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"10", kHeightKey, nil]];
        }
        
      
      
    }*/
}


- (IBAction)infoAlertAction:(UIButton *)sender {
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:@"Con la tarjeta MobileCard puedes realizar pagos, recargas y transferencias." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertMsg show];
    
}

- (IBAction)pideTuTarjetaFisicaAction:(UIButton *)sender {
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:@"Servicio no disponible." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertMsg show];
}

- (IBAction)recargaTuTarjetaAction:(UIButton *)sender {
    
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:@"Recarga tú Tarjeta MobileCard desde tu banca movil, con tú cuenta clabe bancaría." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertMsg show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuItems[indexPath.row][kIdentifierKey]];
    
    if ([[cell reuseIdentifier] isEqualToString:@"cardView"]) {
        for (id element in [cell.contentView subviews]) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
              //  [self addShadowToView:label];
                switch (label.tag) {
                    case 1:
                    {
                        ////////////////////  Retiro de mascara en tarjeta mobilecard  Raul Mendez ///////////////////////////
                        NSString *cardNumber = (NSString*)[cardManager decryptedStringOfString:_mcCardInfo[@"pan"] withSensitive:NO];
//                        [label setText:[NSString stringWithFormat:@"%@ XXXX XXXX %@", [cardNumber substringWithRange:NSMakeRange(0, 4)], [cardNumber substringWithRange:NSMakeRange(12, [cardNumber length]==15?3:4)]]];
                        [label setText:[NSString stringWithFormat:@"%@ %@ %@ %@", [cardNumber substringWithRange:NSMakeRange(0, 4)],[cardNumber substringWithRange:NSMakeRange(4, [cardNumber length]-12)], [cardNumber substringWithRange:NSMakeRange(8, [cardNumber length]-12)],[cardNumber substringWithRange:NSMakeRange(12, [cardNumber length]-12)]]];
                      //  [label setText:cardNumber];
                    }
                        break;
                    case 2:
                    {
                        NSString *valid = (NSString*)[cardManager decryptedStringOfString:_mcCardInfo[@"vigencia"] withSensitive:NO];
                        [label setText:[NSString stringWithFormat:@"Vigencia: %@",valid]];
                    }
                        break;
                    case 3:
                    {
                        [label setText:_mcCardInfo[@"nombre"]];
                    }
                         break;
                        
                    case 4:
                    {
                        NSString *valid = (NSString*)[cardManager decryptedStringOfString:_mcCardInfo[@"codigo"] withSensitive:NO];
                        [label setText:[NSString stringWithFormat:@"Cvv: %@",valid]];
                    }
                        break;
                   
                        
                    default:
                        break;
                }
            }
        }
    }
    
    if ([[cell reuseIdentifier] isEqualToString:@"activate"]) {
        UIButton *button = cell.contentView.subviews[0];
        
        [button addTarget:self action:@selector(callActivation:) forControlEvents:UIControlEventTouchUpInside];
        
      //  [self addShadowToView:button];
    }
    
    if ([[cell reuseIdentifier] isEqualToString:@"balance"]) {
        UITextField_Validations *balanceText = cell.contentView.subviews[0];
        [balanceText setFloatingLabelTextColor:[UIColor darkMCOrangeColor]];
        [balanceText setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[_mcCardInfo[@"balance"] floatValue]]]];
        
        UILabel *Saldo = cell.contentView.subviews[2];
        [Saldo setText:[NSString stringWithFormat:@"Saldo : %@ ",[numFormatter stringFromNumber:[NSNumber numberWithFloat:[_mcCardInfo[@"balance"] floatValue]]]]];
        
        UILabel *clabeLabel = cell.contentView.subviews[1];
        [clabeLabel setText:[NSString stringWithFormat:NSLocalizedString(@"Cuenta CLABE: %@", nil), _mcCardInfo[@"clabe"]]];
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedClabe:)];
        [clabeLabel setUserInteractionEnabled:YES];
        [clabeLabel addGestureRecognizer:gesture];
        
        
    }
    
    if ([[cell reuseIdentifier] isEqualToString:@"transactionCell"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = (UILabel*)element;
                switch (label.tag) {
                    case 1:
                        [label setText:_mcCardInfo[kUserCardTransactions][(indexPath.row-standardItems)/2][@"ticket"]];
                        break;
                    case 2:
                        [label setText:_mcCardInfo[kUserCardTransactions][(indexPath.row-standardItems)/2][@"date"]];
                        break;
                    case 3:
                        [label setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[_mcCardInfo[kUserCardTransactions][(indexPath.row-standardItems)%2][@"total"] floatValue]]]];
                        break;
                    default:
                        break;
                }
            }
        }
        
        return cell;
    }
    
    return cell;
}

- (void) callActivation:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:+%@", @"18558847575"]]];
}


//metodo que se ejecuta al presionar el botón de mis movimientos
- (IBAction)MovementsAction:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"historySegue" sender:NULL];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier  isEqual: @"historySegue"]){
        
        HistoryMCViewController *vc = [segue destinationViewController];
        vc.mcCardInfo = _mcCardInfo;
        
    }
    
    
}



@end
