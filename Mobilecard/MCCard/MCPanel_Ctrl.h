//
//  MCPanel_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 11/15/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface MCPanel_Ctrl : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *mcCardInfo;



@end
