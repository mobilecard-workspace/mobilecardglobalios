

import UIKit
import Alamofire

class HistoryMCViewController: UIViewController,ServicesDelegate ,UITableViewDataSource,UITableViewDelegate{

    
    
    @objc var mcCardInfo : NSDictionary!
    var pan : String!
    let services : Services = Services()
    
    @IBOutlet weak var tableView: UITableView!
    
    //arreglo que  controla la tableView
    var history : [HistoryMcModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .none
        self.services.delegate = self
        
        print("DICTIO: \(self.mcCardInfo!)")
        
        let dateActual = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
       
        let dateFin: String = dateFormatter.string(from: dateActual)
        
     
        //obtenemos un mes antes
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.year, .month], from: dateActual)
        let startOfMonth = calendar.date(from: components)
        
        
        
          let dateInicio: String = dateFormatter.string(from: startOfMonth!)
        
        
        print("Date Actual : \(dateFin)")
        print("Date Mes Actual primer dia : \(dateInicio)")
        
        
        self.pan = (mcCardInfo["pan"] as! String)
     //   self.pan = "mtO3zKz++B29B+exrcYXHw=="
        
        let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,"tarjeta":self.pan!,"fechaFin":dateFin,"fechaIni":dateInicio]
        let header : HTTPHeaders = ["Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx","Content-Type":"application/json","Accept":"*/*"]
        
        print("PARAMS: \(params)")
        
        self.services.send(type: .POST, url: "PreviVale/api/3/1/es/movimientosTarjeta", params: params, header: header, message: "", animate: false)
        
    }
    
    
    //metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
        if endpoint == "PreviVale/api/3/1/es/movimientosTarjeta"{
            print("RESPONSE: \(response)")
            
            
            let code =  response["code"] as! Int
            
            if code == 1000{
                //exito
                
                let data = response["data"] as! NSArray
                
                
                self.history = []
                
                for element in data{
                    
                    let dictio = element as! NSDictionary
                    let cantidad = dictio["cantidad"] as! String
                    let establecimiento = dictio["establecimiento"] as! String
                    let fecha = dictio["fecha"] as! String
                    let status = dictio["status"] as! String
                    
              // solo si tiene la fecha formatea y agrega el objeto(por error que en algunos objetos venia la fecha con String vacio)
                    if fecha != ""{
                        
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    let date = dateFormatter.date(from:fecha)!
                    
                    let objectTemp = HistoryMcModel(cantidad: cantidad, establecimiento: establecimiento, fecha: fecha, status: status,date:date)
                    
                
                   
                    self.history!.append(objectTemp)
                    }//termina if de fecha
                    
                }
                
               //ordenamos de mas reciente a mas vieja
                history!.sort(by: { $0.date.compare($1.date) == .orderedDescending })
                
                // se recarga la tableView
                self.tableView.reloadData()
                
            }else{
                //fracaso
                 self.history = []
                self.tableView.reloadData()
                self.alert(title: "¡Aviso!", message: "No fue posible obtener el historial actual.", cancel: "OK")
                
            }
            
            
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    
    func errorService(endpoint: String) {
        
        
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor.", cancel: "OK")
    }
    
    

    
    
    //metodos dataSource de la tableView
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if let count = self.history?.count{
            
            if count == 0{
                
                return 1
            }else{
                return count
                
            }
            
        }else{
            
            return 9
        }
        
     //   return self.history?.count ?? 10
         
      
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //muestra mensaje si no hay elementos
        if (self.history?.count == 0) {
                 
                  let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "No");
                  cell.textLabel?.text = "Sin historicos disponibles";
                  cell.selectionStyle = .none
                
                  return cell;
              }
        
        
        
        
           let celdaID = "historialMcIdentifier"

        let cell = tableView.dequeueReusableCell(withIdentifier: celdaID,for: indexPath) as! HistoryMCNewTableViewCell
        
        
        //si existe el arreglo se esconde la animacion y se pone los datos
        if self.history != nil{
            
             cell.hideAnimations()
            
         
        
       
        
        let element = self.history![indexPath.row]
        
        cell.depositLabel.text = "\(element.status!) \(element.establecimiento!)"
        cell.amountLabel.text = element.cantidad!
        cell.dateLabel.text = element.fecha!
        
        

            cell.selectionStyle = .none
        
        if element.status == "Rechazada"{
            
            cell.iconImageView.image = UIImage(named: "icon_rechazado")
        }else{
              cell.iconImageView.image = UIImage(named: "icon_exitoso")
        }
        
        if element.status == "Cargo"{
                 cell.amountLabel.textColor = UIColor.red
            
        }else{
            cell.amountLabel.textColor = UIColor.black
        }
        
        }
        
        //si no existe se pone la celda normal con animacion
        
        return cell
        
        
    }
    
    
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           
           return 80
       }

}
