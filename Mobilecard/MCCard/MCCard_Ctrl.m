//
//  MCCard_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 2/20/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "MCCard_Ctrl.h"

@interface MCCard_Ctrl ()
{
    pootEngine *statesManager;
    pootEngine *addManager;
    
    NSString *phoneNumber;
    
    NSArray *stateArray;
}

@end

@implementation MCCard_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:_finalize_Button];
    
    phoneNumber = [[NSString alloc] init];
    
    stateArray = [[NSArray alloc] init];
    
    [_SSNText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_SSNText setMaxLength:20];
    [_BirthdayText setUpTextFieldAs:textFieldTypeBirthdate];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_stateText setInfoArray:stateArray];
    [_stateText setIdLabel:@"id"];
    [_stateText setDescriptionLabel:@"nombre"];
    
    [_cityText setUpTextFieldAs:textFieldTypeCity];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_govIDText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_issueDate setUpTextFieldAs:textFieldTypeDate];
    [_expiryDate setUpTextFieldAs:textFieldTypeDate];
    [_issueState setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_issueState setInfoArray:stateArray];
    [_issueState setIdLabel:@"id"];
    [_issueState setDescriptionLabel:@"nombre"];
    
    
    [self addValidationTextField:_SSNText];
    [self addValidationTextField:_BirthdayText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_zipText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_govIDText];
    [self addValidationTextField:_issueDate];
    [self addValidationTextField:_expiryDate];
    [self addValidationTextField:_issueState];
    
    [self addValidationTextFieldsToDelegate];
    
    [self initializePickersForView:self.navigationController.view];
    
    //PERFORM DIFFERENCIATION FOR DOCUMENT TYPE!
    //LICENSE
    [_SSNText setText:_formData[@"ssn"]];
    [_govIDText setText:_formData[@"license"]];
    
    
    statesManager = [[pootEngine alloc] init];
    [statesManager setDelegate:self];
    [statesManager setShowComments:developing];
    
    [statesManager startRequestWithURL:[NSString stringWithFormat:@"%@/%@/estados", walletManagementURL, _userData[kUserIdCountry]]];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButton_Action:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([_delegate conformsToProtocol:@protocol(mcCardDelegate)]&&[_delegate respondsToSelector:@selector(mcCardResult:)]) {
            [_delegate mcCardResult:NO];
        };
    }];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_MC",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (IBAction)addButton_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        
        addManager = [[pootEngine alloc] init];
        [addManager setDelegate:self];
        [addManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[_SSNText text] forKey:@"ssn"];
        [params setObject:[[_BirthdayText text] stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"fechaNac"];
        [params setObject:[[_stateText infoArray] objectAtIndex:[_stateText selectedID]][@"abreviatura"] forKey:@"estado"];
        [params setObject:[_cityText text] forKey:@"ciudad"];
        [params setObject:[_zipText text] forKey:@"cp"];
        [params setObject:[_addressText text] forKey:@"direccion"];
        [params setObject:[_govIDText text] forKey:@"govtId"];
        [params setObject:[[_issueDate text] stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"govtIdIssueDate"];
        [params setObject:[[_expiryDate text] stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"govtIdExpirationDate"];
        [params setObject:[[_issueState infoArray] objectAtIndex:[_issueState selectedID]][@"abreviatura"] forKey:@"govtIdIssueState"];
        [params setObject:_userData[kUserIDKey] forKey:@"idUsuario"];
        
        [params setObject:(NSLocalizedString(@"lang", nil)) forKey:kLanguage];
        
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        [addManager startJSONRequestWithURL:WSWalletAddMCCard withPost:JSONString];
        
        [self lockViewWithMessage:NSLocalizedString(@"Realizando registro...", nil)];
        
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if ( manager == statesManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0) {
            stateArray = response[@"estados"];
            [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
            [_stateText setInfoArray:stateArray];
            [_issueState setUpTextFieldAs:textFieldTypeRequiredCombo];
            [_issueState setInfoArray:stateArray];
        }
    }
    
    if (manager == addManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0) {
            
            [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
            [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([_delegate conformsToProtocol:@protocol(mcCardDelegate)]&&[_delegate respondsToSelector:@selector(mcCardResult:)]) {
                [_delegate mcCardResult:YES];
            }
            
            NSString *myString = response[kErrorMessage];
            
            NSString *myRegex = @"\\d{1} \\d{3} \\d{3} \\d{4}";
            
            NSRange range = [myString rangeOfString:myRegex options:NSRegularExpressionSearch];
            
            phoneNumber = @"";
            if (range.location != NSNotFound) {
                phoneNumber = [myString substringWithRange:range];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
            } else {
                //NSLog(@"No phone number found");
            }
            
            NSString *finalMsg = response[kErrorMessage];
            finalMsg = [finalMsg stringByReplacingOccurrencesOfString:@"<<" withString:@""];
            finalMsg = [finalMsg stringByReplacingOccurrencesOfString:@">>" withString:@""];
            
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¡Ya casi has terminado!", nil) message:finalMsg delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Activar ahora", nil), nil];
            [alertMsg setDelegate:self];
            [alertMsg setTag:92];
            [alertMsg show];

        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}

/*
-(BOOL)textFieldShouldBeginEditing:(UITextField_Validations *)textField {
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: _dataTable];
        NSIndexPath *indexPath = [_dataTable indexPathForRowAtPoint:buttonPosition];
        
        [_dataTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:_dataTable];
    CGPoint contentOffset = _dataTable.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    [_dataTable setContentOffset:contentOffset animated:YES];
    return [self subTextFieldShouldBeginEditing:textField];
}


-(BOOL)textFieldShouldEndEditing:(UITextField_Validations *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: _dataTable];
        NSIndexPath *indexPath = [_dataTable indexPathForRowAtPoint:buttonPosition];
        
        [_dataTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}*/



//metodo que se ejecuta al presionar el boton de obten tu curp
- (IBAction)curpAction:(UIButton *)sender {
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"https://www.gob.mx/curp/"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
    }];
    
    
    
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 92) {
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:+%@", phoneNumber]]];
            }
                break;
                
            default:
                break;
        }
    }
}


@end
