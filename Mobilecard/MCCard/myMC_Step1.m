//
//  myMC_Step1.m
//  Mobilecard
//
//  Created by David Poot on 11/14/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "myMC_Step1.h"

@interface myMC_Step1 ()

@end

@implementation myMC_Step1 

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_infoCollection setFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height*0.4)];
    [_pageControl setFrame:CGRectMake(0, 40+self.view.frame.size.height*0.4, self.view.frame.size.width, 37)];
    
    //[self addShadowToView:_continue_Button];
    
    [_continue_Button setAlpha:0.0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NSString stringWithFormat:@"page%ld", indexPath.row+1] forIndexPath:indexPath];
    
   // [self addShadowToView:cell];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(collectionView.frame)-40, (CGRectGetHeight(collectionView.frame))-25);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = _infoCollection.frame.size.width;
    _pageControl.currentPage = _infoCollection.contentOffset.x / pageWidth;
    
    [self refreshButton];
}

- (IBAction)pageControl_Action:(id)sender {
    [_infoCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_pageControl.currentPage inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];

    
    [self refreshButton];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)refreshButton
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    
    [_continue_Button setAlpha:_pageControl.currentPage==2?1.0:0.0];
    
    [UIView commitAnimations];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier]isEqualToString:@"step2"]) {
        myMC_Step2 *nextView = [segue destinationViewController];
        [nextView setUserData:_userData];
    }
}

@end
