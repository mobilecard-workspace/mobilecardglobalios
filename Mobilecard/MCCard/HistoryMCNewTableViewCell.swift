//
//  HistoryMCNewTableViewCell.swift
//  Mobilecard
//
//  Created by Luis Flores on 28/01/20.
//  Copyright © 2020 David Poot. All rights reserved.
//

import UIKit
import SkeletonView

class HistoryMCNewTableViewCell: UITableViewCell {

    
    @IBOutlet weak var depositLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var separationImage: UIImageView!
    
    override func awakeFromNib() {
        
        self.iconImageView.layer.cornerRadius = self.iconImageView.frame.width/2
                   self.iconImageView.layer.masksToBounds = true
              
        [depositLabel,dateLabel,amountLabel,iconImageView,separationImage].forEach(){
         //   $0?.showAnimatedSkeleton()
            $0?.showAnimatedGradientSkeleton()
        }
        
       
  

      
     
    }
    
    
    func hideAnimations(){
       
        
        [depositLabel,dateLabel,amountLabel,iconImageView,separationImage].forEach(){
                  $0?.hideSkeleton()
              }
              
        
 
        
    }
    
   
 
}
