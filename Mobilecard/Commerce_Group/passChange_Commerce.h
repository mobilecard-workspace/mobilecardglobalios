//
//  passChange_Commerce.h
//  Mobilecard
//
//  Created by David Poot on 9/18/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface passChange_Commerce : mT_commonTableViewController

@property (weak, nonatomic) IBOutlet UITextField_Validations *passText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *confirmPassText;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
