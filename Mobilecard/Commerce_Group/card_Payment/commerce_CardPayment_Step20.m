//
//  commerce_CardPayment_Step20.m
//  Mobilecard
//
//  Created by David Poot on 9/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "commerce_CardPayment_Step20.h"
#import "commerce_CardPayment_Step30.h"

@interface commerce_CardPayment_Step20 ()
{
    CardIOCreditCardInfo *cardInfo;
    pootEngine *crypter;
}
@end

@implementation commerce_CardPayment_Step20

- (void)viewDidLoad {
    [super viewDidLoad];
    
    crypter = [[pootEngine alloc] init];
    
  //  [self addShadowToView:_continue_Button];
    
    [self initializePickersForView:self.navigationController.view];
    
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_cardText setUpTextFieldAs:textFieldTypeCard];
    [_cvvText setUpTextFieldAs:textFieldTypeCvv2];
    [_validText setUpTextFieldAs:textFieldTypeValidCard];
    [_mailText setUpTextFieldAs:textFieldTypeEmail];
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_cardText];
    [self addValidationTextField:_cvvText];
    [self addValidationTextField:_validText];
    [self addValidationTextField:_mailText];
    
    [self addValidationTextFieldsToDelegate];
    
    if (_msiText) {
        [self initializePickersForView:self.navigationController.view];
        
        [_msiText setUpTextFieldAs:textFieldTypeRequiredCombo];
        [self addValidationTextField:_msiText];
        [self addValidationTextFieldsToDelegate];
        
        [_msiText setInfoArray:[NSArray arrayWithObjects:
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago en una sola exhibición", kDescriptionKey, @"0", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 3 meses sin intereses", kDescriptionKey, @"3", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 6 meses sin intereses", kDescriptionKey, @"6", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 9 meses sin intereses", kDescriptionKey, @"9", kIDKey, nil],
                                nil]];
        
        [_msiText setDescriptionLabel:(NSString*)kDescriptionKey];
        [_msiText setIdLabel:(NSString*)kIDKey];
        
        [_msiText setSelectedID:0];
        [_msiText setText:[_msiText infoArray][[_msiText selectedID]][[_msiText descriptionLabel]]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)textFieldDidEndEditing:(UITextField_Validations *)textField
{
    if (textField == _cardText) {
        textField.hiddenString = textField.text;
        NSMutableString *card = [[NSMutableString alloc]init];
        
        if ([_cardText.text length] > 1){
        if ([_cardText.text length]!=0 && [_cardText.text length]>4){
            for (int i=0; i<([textField.text length]-4); i++) {
                [card appendString:@"X"];
            }
            for (int i=((int)[_cardText.text length]-4); i<[_cardText.text length]; i++) {
                [card appendString:[NSString stringWithFormat:@"%c",[_cardText.text characterAtIndex:i]]];
            }
            _cardText.text = card;
        }
        
        if ([[textField.hiddenString substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"34"]||[[textField.hiddenString substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"37"]) {
            [_msiText setSelectedID:0];
            [_msiText setText:[_msiText infoArray][[_msiText selectedID]][[_msiText descriptionLabel]]];
            [_msiText setEnabled:NO];
        } else {
            [_msiText setEnabled:YES];
        }
        }
    }
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)scanCard:(id)sender {
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    scanViewController.hideCardIOLogo=YES;
      scanViewController.languageOrLocale = @"es";
    scanViewController.scanInstructions = @"Coloque su tarjeta embosada (con relieve) aquí.\nSe escaneará automáticamente.";
    [self.navigationController presentViewController:scanViewController animated:YES completion:nil];
}

- (IBAction)continue_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    [self performSegueWithIdentifier:@"step30" sender:nil];
}

#pragma mark - CardIOPaymentViewControllerDelegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    cardInfo = info;
    
    [_cardText setText:cardInfo.cardNumber];
    [self textFieldDidEndEditing:_cardText];
    
    [_cvvText setText:cardInfo.cvv];
    NSString *expYear = [NSString stringWithFormat:@"%d", (int)cardInfo.expiryYear];
    [_validText setText:[NSString stringWithFormat:@"%02d/%@", (int)cardInfo.expiryMonth, [expYear substringWithRange:NSMakeRange(2, 2)]]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"step30"]) {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:_passedData];
        
        [data setObject:[crypter encryptJSONString:_cardText.hiddenString withPassword:nil] forKey:@"tarjeta"];
        [data setObject:[crypter encryptJSONString:_validText.text withPassword:nil] forKey:@"vigencia"];
        [data setObject:[crypter encryptJSONString:_cvvText.text withPassword:nil] forKey:@"ct"];
        [data setObject:@"0" forKey:@"tipoTarjeta"]; //DUDA!!
        [data setObject:[_msiText infoArray][[_msiText selectedID]][[_msiText idLabel]] forKey:@"msi"];
        [data setObject:[_mailText text] forKey:@"email"];
        [data setObject:[_phoneText text] forKey:@"celular"];
         [data setObject:[_nameText text] forKey:@"nombre"];
        commerce_CardPayment_Step30 *nextView = [segue destinationViewController];
        [nextView setPassedData:data];
    }
}
@end
