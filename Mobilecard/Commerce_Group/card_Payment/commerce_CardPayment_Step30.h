//
//  commerce_CardPayment_Step30.h
//  Mobilecard
//
//  Created by David Poot on 9/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Secure3D_Ctrl.h"
#import "SignatureView.h"
//#import <TrustDefender/TrustDefender.h>
#import <CoreLocation/CoreLocation.h>

@interface commerce_CardPayment_Step30 : mT_commonTableViewController <Secure3DDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) NSDictionary *passedData;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@property (weak, nonatomic) IBOutlet UIButton *clear_Button;
@property (weak, nonatomic) IBOutlet SignatureView *signView;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@end
