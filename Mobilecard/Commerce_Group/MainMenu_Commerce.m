//
//  MainMenu_Commerce.m
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "MainMenu_Commerce.h"
#import "AppDelegate.h"
#import "MCPanel_Ctrl.h"
#import <Mobilecard-Swift.h>


@interface MainMenu_Commerce ()
{
    NSMutableArray *menuItems;
    pootEngine *verifyManager;
     pootEngine *loginManager;
    
    NSDictionary *verifyInfo;
    
    BOOL myMobileCard_Show;
}
@end

@implementation MainMenu_Commerce

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //mensaje de bienvenida mobilecard
    if ((DataUser.MESSAGEMC  != NULL) &&(![DataUser.MESSAGEMC  isEqual: @""])){
     
    
     [SendTokenFirestore AlertMcMexWithView:self];
        
        
    
    }
    
    //parche para evitar que se combinen las sesiones de usuario y comercio
    if (![DataUser.USERTYPE  isEqual: @"NEGOCIO"]){
        
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        
        [DataUser clean];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
        
    }//termina parche
    
    
   // [self addShadowToView:self.navigationController.navigationBar];
    
    //se le pone la accion a la imagen de codigo qr estatico
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageQrAction)];
    singleTap.numberOfTapsRequired = 1;
    [_imageQrImage addGestureRecognizer:singleTap];
    
  
    
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    _nameComerceLabel.text = [userData valueForKey:@"nombreEstablecimiento"];
   
    
        NSString *qrEstatico = userData[@"qrBase64"];
        NSString *nombreEstablecimiento = userData[@"nombreEstablecimiento"];
     NSString *userId = [userData[@"idEstablecimiento"] stringValue];
        DataUser.QREMPRESA = qrEstatico;
    DataUser.NAME = nombreEstablecimiento;
    
    DataUser.COUNTRYID = [NSString stringWithFormat:@"%d",[[userData objectForKey:kUserIdCountry] intValue]];
    DataUser.USERIDD = userId;
    
    
    menuItems = [[NSMutableArray alloc] init];
    
    [self updateMenu];
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [self previvaleVerifyAndShowMenu:NO];
    
    myMobileCard_Show = NO;
}


// metodo que manda a la vista del codigo qr estatico
-(void)imageQrAction{

    UIStoryboard * nexStory = [UIStoryboard storyboardWithName:@"PEmainCommerce" bundle:nil];
    PEQRShowCommerceViewController *vc = [nexStory instantiateViewControllerWithIdentifier:@"codigoqrShowIdentifier"];
    [self.navigationController pushViewController:vc animated:TRUE];
    
}

- (void) previvaleVerifyAndShowMenu:(BOOL)showMenu
{
    NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    verifyManager = [[pootEngine alloc] init];
    [verifyManager setDelegate:self];
    [verifyManager setShowComments:developing];
    
    verifyManager.tag = showMenu;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:userDetails[@"idEstablecimiento"] forKey:@"id"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    //[verifyManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@?id=%@", WSPrevivaleVerify, userDetails[@"idEstablecimiento"]] withPost:JSONString];
    
    //[self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewDidAppear:(BOOL)animated{
    
    
       NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    //registramos el token device de firestore para las push
    NSString *userId = [userData[@"idEstablecimiento"] stringValue];
    
    NSString *typeUser = @"NEGOCIO";
    
    
       [SendTokenFirestore sendWithIdUser:userId typeUser:typeUser idPais:[NSString stringWithFormat:@"%@", [userData objectForKey:kUserIdCountry]]];
 
    
    
    
    
}


- (void)updateMenu
{
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
    [menuItems removeAllObjects];
    switch ([userData[@"id"] intValue]) {
        case 1: //MEXICO
        case 3: //USA
        {
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"10", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_card", kIdentifierKey, @"75", kHeightKey,nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_qrcode", kIdentifierKey, @"75", kHeightKey,nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_scan", kIdentifierKey, @"75", kHeightKey,nil]];
            if (myMobileCard_Show) {
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"75", kHeightKey,nil]];
              //  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_mymc", kIdentifierKey, @"54", kHeightKey,nil]];
            }
        }
            break;
        default:
            break;
    }
    
    [_menuTableView reloadData];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateMenu];
    
    [super viewWillAppear:YES];
}


#pragma Mark UITableView Implementation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItems count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

- (InsetCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:menuItems[indexPath.row][kIdentifierKey]];
    
   // [self addShadowToView:cell];
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"Header"]) {
        UILabel *label = cell.contentView.subviews[0];
        //NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];;
        [label setText:[NSString stringWithFormat:NSLocalizedString(@"SELECCIONA COMO DESEAS COBRAR", nil)]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView  deselectRowAtIndexPath:indexPath animated:YES];
    
    [self callEndpointSession];
    selectedItemCell = (int)indexPath.row;
    
    
    
}


-(void)callEndpointSession{
    mT_commonTableViewController *helper = [[mT_commonTableViewController alloc] init];
 
    loginManager = [[pootEngine alloc] init];
    [loginManager setDelegate:self];
    [loginManager setShowComments:developing];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    [params setObject:userDetails[@"email"] forKey:@"usrLoginOrEmail"];
    [params setObject:[helper encryptString:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword]] forKey:kUserPassword];
    //MUST HAVE PARAMETERS
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"manufacturer"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
    [params setObject:@"iOS" forKey:@"platform"];
    [params setObject:@"NEGOCIO" forKey:@"tipoUsuario"];
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
   
    
   //  [self performSegueWithIdentifier:menuItems[selectedItemCell][kIdentifierKey] sender:nil];
}

- (void)startPrevivale
{
    ////////////////////  Validación si no tiene Card que lo mande acrear una nueva  Raul Mendez ///////////////////////////
    if (verifyInfo[@"card"] == (id)[NSNull null]) {
        if (!verifyInfo[@"card"]||[verifyInfo[@"card"][@"pan"] length]==0) {
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"previvale" bundle:nil];
            UINavigationController *nav;
            
            nav = [nextStory instantiateInitialViewController];
            
            previvale_Step10 *nextView = [[nav childViewControllers] firstObject];
            [nextView setDelegate:self];
            [[self navigationController] presentViewController:nav animated:YES completion:nil];
        } else {
            [self previvaleRegisterResponse:verifyInfo];
        }
    } else{
        /// creacion de tarjeta
       // if (!verifyInfo[@"card"]||[verifyInfo[@"card"][@"pan"] length]==0) {
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"previvale" bundle:nil];
            UINavigationController *nav;
            
            nav = [nextStory instantiateInitialViewController];
            
            previvale_Step10 *nextView = [[nav childViewControllers] firstObject];
            [nextView setDelegate:self];
            [[self navigationController] presentViewController:nav animated:YES completion:nil];
      //  } else {
       //     [self previvaleRegisterResponse:verifyInfo];
       // }
    }
}




- (BOOL)verifyStatus
{
    ///// kUserDetailsKey  Raul mendez Validando Jumio en verifyStatus
    NSDictionary *response = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    switch ([response[kIDJumio] intValue]) {
        case 0:
        {
            [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:response[@"email"] forKey:@"user_email"];
           // [self performSegueWithIdentifier:@"init_boarding_jumio" sender:nil];
            return YES;;
        }
            break;
        case 1:
        {
            if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 100)) {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
                    UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
                    Register_Step30 *nextView = [[nav viewControllers] firstObject];
                    [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                    [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idEstablecimiento"]];
                    [self.navigationController presentViewController:nav animated:YES completion:nil];
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                }];
                [alertMsg addAction:ok];
                [alertMsg addAction:cancel];
                [self presentViewController:alertMsg animated:YES completion:nil];
                return NO;
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
                [alertMsg setTag:99];
                [alertMsg show];
                return NO;
            }
        }
            break;
        case 2:
        {
            //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
            return NO;
        }
            break;
        case 3:
        {
            [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
            return NO;
        }
            break;
    }
    return YES;
}

#pragma Mark segue handler
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    
    
    
    
    if([[segue identifier] isEqualToString:@"init_boarding_jumio"]){
        IntroOnBoardingJumioController *next = (IntroOnBoardingJumioController *)[segue destinationViewController];
        [next setTypeUser:1];
    }
    
    
    if([[segue identifier] isEqualToString:@"side_card"]){
        MXCardPressentStepCommerceViewController *next = (MXCardPressentStepCommerceViewController *)[segue destinationViewController];
        [next setFromView:0];
    }
    
    if([[segue identifier] isEqualToString:@"side_qrcode"]){
        MXCardPressentStepCommerceViewController *next = (MXCardPressentStepCommerceViewController *)[segue destinationViewController];
        [next setFromView:1];
    }
    
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    
    
    if (manager == loginManager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        response  = [self cleanDictionary:response];
        if ([response[@"errorCode"] intValue] == 0) {
            [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
          //  [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
            if ([self verifyStatus]) {
                if ([menuItems[selectedItemCell][kIdentifierKey] isEqualToString:@"side_mymc"]) {
                    [self previvaleVerifyAndShowMenu:YES];
                } else {
                    [self performSegueWithIdentifier:menuItems[selectedItemCell][kIdentifierKey] sender:nil];
                }
            }
            [self.menuTableView reloadData];
        }else{
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
            [DataUser clean];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:FALSE forKey:@"enable_tutorial"];
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"SessionCommercio"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             NSDictionary *logout = [[NSDictionary alloc] init];
             [logout deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
             [self dismissViewControllerAnimated:YES completion:nil];
             }
             }
             
             
             
             
         

    
    
    
    
    
    
    if (manager == verifyManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        verifyInfo = [NSDictionary dictionaryWithDictionary:response];
        
        if ([verifyInfo[kIDError] intValue]==0) {
            if ([verifyInfo[@"accountId"] length]==0) {
                myMobileCard_Show = YES;
                if (verifyManager.tag == 1) {
                    [self startPrevivale];
                }
            } else {
                myMobileCard_Show = NO;
            }
            [self updateMenu];
        } else if ([verifyInfo[kIDError] intValue]==200) {
            myMobileCard_Show = YES;
            if (verifyManager.tag == 1) {
                [self startPrevivale];
            }
            [self updateMenu];
        } else {
            myMobileCard_Show = NO;
            [self updateMenu];
            
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
             
        }
    }
}


- (IBAction)historyCommerceAction:(UIButton *)sender {
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"statement_Commerce" bundle:nil];
                               
                               [self.navigationController pushViewController:[nextStory instantiateInitialViewController] animated:YES];
}


#pragma mark Previvale Handler

- (void)previvaleRegisterResponse:(NSDictionary *)response
{
    if (response) {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
        MCPanel_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"mcpanel_view"];
        [nextView setMcCardInfo:response[@"card"]];
        
        [[self navigationController] pushViewController:nextView animated:YES];
    }
}


//metodo que abre el wallet
- (IBAction)showWalletActionn:(UIButton *)sender {
    
                  [self performSegueWithIdentifier:@"showWalletIdentifier" sender:nil];

    
    
    
    
  
}

//metodo delegado pop
- (void)successComplete{
    
    
    
}




@end
