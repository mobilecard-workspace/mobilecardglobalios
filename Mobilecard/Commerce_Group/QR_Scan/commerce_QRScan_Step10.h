//
//  commerce_QRScan_Step10.h
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "ZBarSDK.h"
#import "MC_SAPOverlayView.h"

@interface commerce_QRScan_Step10 : mT_commonTableViewController <ZBarReaderDelegate>
{
    MC_SAPOverlayView *overlayView;
}

@property (weak, nonatomic) IBOutlet UITextField_Validations *idNumberText;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end
