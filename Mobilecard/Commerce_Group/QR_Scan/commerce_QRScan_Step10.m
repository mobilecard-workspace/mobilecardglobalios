//
//  commerce_QRScan_Step10.m
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "commerce_QRScan_Step10.h"
#import "commerce_QRPayment_Step30.h"

@interface commerce_QRScan_Step10 ()
{
    pootEngine *confirmManager;
    
    NSString *scannerReference;
}
@end



@implementation commerce_QRScan_Step10

- (void)viewDidLoad {
    [super viewDidLoad];

   // [self addShadowToView:_continue_Button];
    
    [_idNumberText setUpTextFieldAs:textFieldTypeGeneralNumericNoRequired];
    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
    if ([[countryInfo objectForKey:kIDKey] intValue] == 4) {
        [self scanQR:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)scanQR:(id)sender {
    @try {
        ZBarReaderViewController *reader = [[ZBarReaderViewController alloc]init];
        
        reader.readerDelegate = self;
        //[reader.scanner setSymbology:0 config:ZBAR_CFG_ENABLE to:0];
        [reader.scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_ENABLE to:1];  //QR READING
        //[reader.scanner setSymbology:ZBAR_CODE128 config:ZBAR_CFG_ENABLE to:1]; //BAR READING
        
        [reader setTitle:@"Enfoque el código"];
        
        reader.showsZBarControls = NO;
        
        
        overlayView = [[MC_SAPOverlayView alloc]initWithNibName:@"MC_SAPOverlayView" bundle:nil];
        [overlayView.view setFrame:CGRectMake(0, 0, 320, 480)];
        
        [reader setCameraOverlayView:[overlayView view]];
        //[reader setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        
        [self presentViewController:reader animated:YES completion:^{
            
            CGRect backFrame = CGRectMake(0, reader.view.frame.size.height-60, reader.view.frame.size.width, 60);
            
            UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [backButton setFrame:backFrame];
            [backButton setBackgroundColor:[UIColor clearColor]];
            [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [backButton setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
            
            [overlayView.view addSubview:backButton];
        }];
        
        
    }
    @catch (NSException *exception) {
        NSLog(@"No se activo escaner: %@",[exception description]);
    }
}

- (void)backAction:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)continue_Action:(id)sender {
    if ([[_idNumberText text] length]>0) {
        confirmManager = [[pootEngine alloc] init];
        [confirmManager setDelegate:self];
        [confirmManager setShowComments:developing];
        
        [confirmManager startRequestWithURL:[NSString stringWithFormat:@"%@/CuantoTeDebo/%d/%d/%@/resultByID?idBitacora=%@", simpleServerURL, idApp, [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"] intValue], NSLocalizedString(@"lang", nil), _idNumberText.text]];
        
        [self lockViewWithMessage:nil];
    }
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];

    if (confirmManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        [self performSegueWithIdentifier:@"result" sender:response];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"result"]) {
        commerce_QRPayment_Step30 *nextView = [segue destinationViewController];
        
        NSDictionary *response = (NSDictionary*)sender;
        
        [nextView setResultData:response];
    }
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    @try {
        
        ZBarSymbolSet *symbolset=[info objectForKey:ZBarReaderControllerResults];
        ZBarSymbol  *symbol=nil;
        
        NSString *qrCode=[[NSString alloc]init];
        
        for(symbol in symbolset )
        {
            qrCode=[NSString stringWithString:symbol.data];
        }
        
        scannerReference = qrCode;
        
        [self backAction:nil];
        
        id responseID;
        NSError *jsonError = nil;
        NSData *jsonData = [scannerReference dataUsingEncoding:NSUTF8StringEncoding];
        
        if ([jsonData length]>0) {
            responseID = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&jsonError];
            
            if (jsonError) {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Código QR no válido", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
                return;
            }
        }
        
        NSDictionary *response = (NSDictionary*)responseID;
        
        [self performSegueWithIdentifier:@"result" sender:response];
    }
    @catch (NSException *exception) {
        NSLog(@"Error:%@",[exception description]);
        UIAlertView *error=[[UIAlertView  alloc]initWithTitle:@"Error." message:@"Codigo QR incorrecto." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [error show];
    }
}

@end
