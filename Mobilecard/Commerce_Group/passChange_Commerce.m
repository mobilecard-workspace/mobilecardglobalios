//
//  passChange_Commerce.m
//  Mobilecard
//
//  Created by David Poot on 9/18/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "passChange_Commerce.h"

@interface passChange_Commerce ()
{
    pootEngine *changeManager;
}
@end

@implementation passChange_Commerce

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self addShadowToView:_continue_Button];
    
    [_passText setUpTextFieldAs:textFieldTypePassword];
    [_confirmPassText setUpTextFieldAs:textFieldTypePassword];
    
    [self addValidationTextField:_passText];
    [self addValidationTextField:_confirmPassText];
    
    [self addValidationTextFieldsToDelegate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        if ([_passText.text isEqualToString:[_confirmPassText text]]) {
            changeManager = [[pootEngine alloc] init];
            [changeManager setDelegate:self];
            [changeManager setShowComments:developing];
            
            [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@/commerce/passUpdate?userAdmin=%d&newPass=%@", userManagementURL, NSLocalizedString(@"lang", nil), [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idEstablecimiento"] intValue], [self encryptString:[_passText text]]]];
            
            [self lockViewWithMessage:nil];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"La contraseña debe de coincidir" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (changeManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
            
            if ([response[kIDError] intValue] == 0) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
    }
}


@end
