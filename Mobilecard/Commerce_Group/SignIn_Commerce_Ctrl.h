//
//  SignIn_Commerce_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Register_Step30.h"
#import "NSDictionary (keychain).h"

@interface SignIn_Commerce_Ctrl : mT_commonTableViewController

@property (weak, nonatomic) IBOutlet UITextField_Validations *commerceText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *validationText;
@property (weak, nonatomic) IBOutlet UIImageView *business_imageView;

@property (weak, nonatomic) IBOutlet UIButton *signIn_Button;
@end
