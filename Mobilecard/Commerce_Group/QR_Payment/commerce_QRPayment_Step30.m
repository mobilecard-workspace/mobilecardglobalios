//
//  commerce_QRPayment_Step30.m
//  Mobilecard
//
//  Created by David Poot on 9/13/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "commerce_QRPayment_Step30.h"
#import "scanNPay_Step40.h"

@interface commerce_QRPayment_Step30 ()
{
    NSNumberFormatter *numFormatter;
}
@end

@implementation commerce_QRPayment_Step30

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // DELETE
    /*
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    [result setObject:@"271762" forKey:@"authProcom"];
    [result setObject:@"0.7" forKey:@"comision"];
    [result setObject:@"09/14/2018 11:12:29 AM" forKey:@"fecha"];
    [result setObject:@"158985" forKey:@"idBitacora"];
    [result setObject:@"0" forKey:@"idError"];
    [result setObject:@"Pago Exitoso" forKey:@"mensajeError"];
    [result setObject:@"11.7" forKey:@"montoTransfer"];
    [result setObject:@"436715624739" forKey:@"refProcom"];
    [result setObject:@"0" forKey:@"referenceBanorte"];
    [result setObject:@"Test" forKey:@"referenciaNeg"];
    [result setObject:@"4152- XXXX - XXXX - 9591" forKey:@"tarjeta"];
    
    _resultData = result;
     */
    // DELETE
    
  //  [self addShadowToView:_continue_Button];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [_resultDetailsText setText:@""];

    developing?NSLog(@"Result to display -> %@", _resultData):nil;
    
    NSString *messageNew = @"¡Operación exitosa!";
    if([_resultData[@"message"] isEqual:[NSNull null]]){
    }else{
        NSLog(@"Si tenemos message");
        messageNew = _resultData[@"message"];
    }
    
    switch ([_resultData[@"code"] intValue]) {
        case 0:
        {
            
            
            
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"Se ha enviado el comprobante de su pago por correo electrónico." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
            [alertMsg show];
            
            NSTimeInterval seconds = [_resultData[@"dateTime"]longValue] / 1000;
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
            NSLog(@"ans : %@",date);
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
            NSLog(@"result: %@", [dateFormatter stringFromDate:date]);
            
          
            
            NSString *opIDNew = @"0";
            if([_resultData[@"opId"] isEqual:[NSNull null]]){
            }else{
             
                opIDNew = _resultData[@"opId"];
            }
            
            [_resultIcon setImage:[UIImage imageNamed:@"icon_exitoso"]];
            [_resultTitleText setText:NSLocalizedString(@"¡Tu transacción se ha completado!", nil)];
            [_resultDetailsText setText:[NSString stringWithFormat:NSLocalizedString(@"Referencia: %@\nID transacción: %@\nAutorización bancaria: %@\nNúmero de tarjeta: %@\nFecha: %@\nTotal: %@", nil), opIDNew, _resultData[@"idTransaccion"], _resultData[@"authNumber"], _resultData[@"maskedPAN"], date, [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_resultData[@"amount"] floatValue]]]]];
            [_shareButton setHidden:NO];
        }
            break;
            
        default:
        {
            [_resultIcon setImage:[UIImage imageNamed:@"icon_rechazado"]];
            [_resultTitleText setText:_resultData[@"message"]];
            [_shareButton setHidden:YES];
        }
            break;
    }
    
    [_resultDetailsText setEditable:NO];
    [_resultDetailsText setSelectable:NO];
    
    if (_showQR_Button) {
      //  [self addShadowToView:_showQR_Button];
        [_resultTitleText setText:messageNew];
        switch ([_resultData[@"code"] intValue]) {
            case 0:
            {
                [_showQR_Button setTitle:@"MOSTRAR CÓDIGO QR" forState:UIControlStateNormal];
            }
                break;
                
            default:
            {
                [_showQR_Button setTitle:@"OK" forState:UIControlStateNormal];
            }
                break;
        }
    }
}

- (IBAction)back_Action:(id)sender {
    switch ([_resultData[@"code"] intValue]) {
        case 0:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        default:
            [self.navigationController popViewControllerAnimated:YES];
            break;
    }
}

- (IBAction)share_Action:(id)sender {
    CGRect rect = CGRectMake(0, 0, _result.frame.size.width, _result.frame.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [_result drawViewHierarchyInRect:rect afterScreenUpdates:YES];
    UIImage *snapShotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSMutableArray *activityItems = [NSMutableArray arrayWithObjects:snapShotImage, nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypePrint,                                                         UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,                                                         UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList,                                                         UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,
                                                     UIActivityTypePostToTencentWeibo,UIActivityTypeAirDrop];
    
    [self presentActivityController:activityViewController];
}

- (IBAction)showQRCode:(id)sender {
    switch ([_resultData[@"code"] intValue]) {
        case 0:
        {
            [self performSegueWithIdentifier:@"showqr" sender:nil];
        }
            break;
        default:
            [self back_Action:nil];
            break;
    }
}

- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    //controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            
            // user shared an item
            //NSLog(@"We used activity type%@", activityType);
            
        } else {
            
            // user cancelled
            //NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            //NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showqr"]) {
        scanNPay_Step40 *nextView = [segue destinationViewController];
        [nextView setResultData:_resultData];
    }
}

@end
