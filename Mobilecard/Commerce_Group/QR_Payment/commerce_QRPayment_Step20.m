//
//  commerce_QRPayment_Step20.m
//  Mobilecard
//
//  Created by David Poot on 9/13/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "commerce_QRPayment_Step20.h"
#import "commerce_QRPayment_Step30.h"
#import <CoreImage/CoreImage.h>

@interface commerce_QRPayment_Step20 ()
{
    pootEngine *idManager;
    pootEngine *confirmManager;
    pootEngine *feeManager;
     pootEngine *encrypter;
    
    NSString *idBitacora;
}
@end

@implementation commerce_QRPayment_Step20

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    encrypter = [[pootEngine alloc] init];
 //   [self addShadowToView:_continue_Button];
    
   
    [self generateQRWithObject:_passedData];
    
    idManager = [[pootEngine alloc] init];
    [idManager setDelegate:self];
    [idManager setShowComments:developing];
    
    [idManager startRequestWithURL:[[NSString stringWithFormat:@"%@/CuantoTeDebo/%d/%d/%@/NEGOCIO/generatePaymentIdBP?concept=%@&establecimientoId=%@&amount=%@&comision=%@&referenciaNeg=%@&propina=%@", simpleServerURL, idApp, [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"] intValue], NSLocalizedString(@"lang", nil), _passedData[@"concept"], _passedData[@"establecimientoId"], _passedData[@"amount"], _passedData[@"comision"], _passedData[@"referenciaNeg"], _passedData[@"propina"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    
    confirmManager = [[pootEngine alloc] init];
    [confirmManager setDelegate:self];
    [confirmManager setShowComments:developing];
    
    [confirmManager startRequestWithURL:[NSString stringWithFormat:@"%@/CuantoTeDebo/%d/%d/%@/resultByID?idBitacora=%@", simpleServerURL, idApp, [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"] intValue], NSLocalizedString(@"lang", nil), idBitacora]];
    
    [self lockViewWithMessage:nil];
}


- (void) generateQRWithObject:(NSDictionary*)object2Send
{
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    
    [filter setDefaults];
    
  //  NSError *JSONError;
   // NSData *json2Send = [NSJSONSerialization dataWithJSONObject:object2Send options:0 error:&JSONError];
   // NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    //pruebas
    NSString *dataString = [encrypter encryptJSONWithParams:object2Send withPassword:nil];
    //
    
   // NSString *QRData = JSONString;
    NSString *QRData = dataString;
    
    NSData *data = [QRData dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    
    CIImage *outputImage = [filter outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:outputImage
                                       fromRect:[outputImage extent]];
    
    UIImage *image = [UIImage imageWithCGImage:cgImage
                                         scale:1.
                                   orientation:UIImageOrientationUp];
    
    // Resize without interpolating
    UIImage *resized = [self resizeImage:image
                             withQuality:kCGInterpolationNone
                                    rate:5.0];
    
    [_QRImageView setImage:resized];
    
    CGImageRelease(cgImage);
}

#pragma mark - Private

- (UIImage *)resizeImage:(UIImage *)image
             withQuality:(CGInterpolationQuality)quality
                    rate:(CGFloat)rate
{
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (idManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue] == 0) {
            idBitacora = response[@"idBitacora"];
            [_idNumberLabel setText:[NSString stringWithFormat:NSLocalizedString(@"NÚMERO DE ID: %@", nil), idBitacora]];
        }
    }
    
    if (confirmManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        [self performSegueWithIdentifier:@"result" sender:response];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"result"]) {
        commerce_QRPayment_Step30 *nextView = [segue destinationViewController];
        
        NSDictionary *response = (NSDictionary*)sender;
        
        [nextView setResultData:response];
    }
}

@end
