//
//  commerce_QRPayment_Step10.h
//  Mobilecard
//
//  Created by David Poot on 9/13/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface commerce_QRPayment_Step10 : mT_commonTableViewController

@property (weak, nonatomic) IBOutlet UITextField_Validations *amountText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *tipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *conceptText;
@property (weak, nonatomic) NSString *_importe;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end
