//
//  MainMenu_Commerce.h
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonController.h"
#import "Register_Step30.h"
#import "previvale_Step10.h"
#import "InsetCell.h"
#import "NSDictionary (keychain).h"
#import "IntroOnBoardingJumioController.h"



@interface MainMenu_Commerce : mT_commonController <UITableViewDelegate, previvaleRegisterProtocol>
{
    int selectedItemCell;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UILabel *nameComerceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageQrImage;

@property (strong, nonatomic) NSString *_createdMC;

@end
