//
//  register_Commerce_Step40.m
//  Mobilecard
//
//  Created by David Poot on 11/23/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "register_Commerce_Step40.h"
#import <Mobilecard-Swift.h>

@interface register_Commerce_Step40 ()
{
    pootEngine *encrypter;
    pootEngine *registerManager;
    pootEngine *statesManager;
}

@end

@implementation register_Commerce_Step40

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encrypter = [[pootEngine alloc] init];
    [encrypter setDelegate:self];
    [encrypter setShowComments:developing];
    
   // [self addShadowToView:_saveButton];
    
    [self initializePickersForView:self.navigationController.view];
    
   // [_cardNumberText setUpTextFieldAs:textFieldTypeCard];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_townText setUpTextFieldAs:textFieldTypeTown];
    [_cityText setUpTextFieldAs:textFieldTypeCity];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    [_rfcText setUpTextFieldAs:textFieldTypeRFC];
    [_curpText setUpTextFieldAs:textFieldTypeName];
    
  //  [self addValidationTextField:_cardNumberText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_townText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_zipText];
    [self addValidationTextField:_rfcText];
    [self addValidationTextField:_curpText];
    
    [_rfcText setRequired:NO]; //RFC OPTIONAL
    
    [self addValidationTextFieldsToDelegate];
    
    statesManager = [[pootEngine alloc] init];
    [statesManager setDelegate:self];
    [statesManager setShowComments:developing];
    
    [statesManager startRequestWithURL:[NSString stringWithFormat:@"%@/1/estados", walletManagementURL]];
    
    [self lockViewWithMessage:nil];
}

- (IBAction)back_Action:(id)sender {
    
   [self.navigationController popViewControllerAnimated:YES];
   // [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButton_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
  //  [[self navigationController] dismissViewControllerAnimated:YES completion:^{
     //   if ([_delegate conformsToProtocol:@protocol(activateAccountProtocol)]&&[_delegate respondsToSelector:@selector(activateAccount_Result:)]) {
           // NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
           // [params setObject:[encrypter MCEncryptString:_cardNumberText.hiddenString withPassword:nil] forKey:@"t_previvale"];
    
    
    registerManager = [[pootEngine alloc] init];
          [registerManager setDelegate:self];
          [registerManager setShowComments:developing];
            
            [__data setObject:_addressText.text forKey:@"direccion"];
            [__data setObject:_townText.text forKey:@"colonia"];
            [__data setObject:_cityText.text forKey:@"ciudad"];
            [__data setObject:[_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]] forKey:@"idEstado"];
            [__data setObject:_zipText.text forKey:@"cp"];
            [__data setObject:_rfcText.text forKey:@"rfc"];
            [__data setObject:_curpText.text forKey:@"curp"];
    
    //si tenemos serial qr es que venimos de welcome pack se agrega el parametro
    if (__serialQR.length > 0){
          [__data setObject:__serialQR forKey:@"serialQR"];
    }
    
    
     developing?NSLog(@"params to be sent -> %@", __data):nil;
    
    
    @try {
             NSError *JSONError;
             NSData *json2Send = [NSJSONSerialization dataWithJSONObject:__data options:0 error:&JSONError];
             NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
             
             [registerManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@", userManagementURL, @"1/es/comercio/alta"] withPost:JSONString];
             
             [self lockViewWithMessage:NSLocalizedString(@"Realizando registro...", nil)];
         } @catch (NSException *exception) {
             NSLog(@"EXCEPTION -> %@", exception.description);
         } @finally {
             nil;
         }
            
          //  [_delegate activateAccount_Result:params];
      //  };
   // }];
}

- (void)textFieldDidEndEditing:(UITextField_Validations *)textField
{
    if (textField == _cardNumberText) {
        textField.hiddenString = textField.text;
        NSMutableString *card = [[NSMutableString alloc]init];
        if ([_cardNumberText.text length]!=0 && [_cardNumberText.text length]>4){
            for (int i=0; i<([textField.text length]-4); i++) {
                [card appendString:@"X"];
            }
            for (int i=((int)[_cardNumberText.text length]-4); i<[_cardNumberText.text length]; i++) {
                [card appendString:[NSString stringWithFormat:@"%c",[_cardNumberText.text characterAtIndex:i]]];
            }
            _cardNumberText.text = card;
        }
        
        [self.tableView reloadData];
    }
}




//metodo que se ejecuta al presionar el boton de obten tu curp
- (IBAction)curpAction:(UIButton *)sender {
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"https://www.gob.mx/curp/"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
    }];
    
    
    
}



#pragma Mark pootEngine Handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == statesManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue]==0) {
            [_stateText setInfoArray:response[@"estados"]];
            [_stateText setDescriptionLabel:@"nombre"];
            [_stateText setIdLabel:@"id"];
            
            [_stateText setSelectedID:0];
            [_stateText setText:[_stateText infoArray][[_stateText selectedID]][[_stateText descriptionLabel]]];
        }
    }
    
    if (manager == registerManager){
        
        
       NSDictionary *response = (NSDictionary*)json;
        response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
        
        if ([response[kIDError] intValue] == 0) {
            switch ([response[kIDJumioCommerce] intValue]) {
            /*        case 0:
                {
                    
                    
                    if (response.count > 1){
                    
                    //variable para no tener el data del autologin antes del jumio
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    
                    DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                    
                    DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                    
                    DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                    DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                    DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                    
                    
                   [self performSegueWithIdentifier:@"init_boarding_jumio" sender:nil];
                        return;
                    }else{
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                        
                        
                        [alertMsg show];
                        return;
                        
                    }
                        
                }
                    break;*/
                case 1:
                case 2:
                case 3:
                case 0:
                {
                    
                    if (response.count < 1){
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                                               
                                               
                                               [alertMsg show];
                                               return;
                    }
                    
                    switch ([response[kIDStatus] intValue]) {
                       //       switch (100) {
                            case 98:
                        {
                        //    preliminaryUserData = response;
        
                            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                            [alertMsg setTag:3031];
                            [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                            [alertMsg show];
                            return;
                        }
                            break;
                            
                        default:
                        {
                                [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                           
                            [[NSUserDefaults standardUserDefaults] setObject:__password forKey:(NSString*)kUserPassword];
                             
                                [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                                [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                                [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
            
                            
                                NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                            
                            
                            DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                            
                            DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                            
                            DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                            DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                            DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                            
                           
                            DataUser.MESSAGEMC = [userData valueForKey:@"mensajeError"];
                            
                            BOOL *bol = [response[@"cardPrevivale"] boolValue];
                            
                            NSString *booleanString = (bol) ? @"True" : @"False";
                            DataUser.STATUSMC = booleanString;
                            
                                //[response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                                //[response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            
                            // si es peru empresa
                            if([[[userData objectForKey:kUserIdCountry] stringValue]  isEqual: @"4"]){
                                    
                                    
                                    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                                
                                DataUser.COUNTRY = @"PE";
                                //DataUser.COUNTRY = [countryInfo objectForKey:kDetailsKey];  ////// Raul mendez(TE ODIO)
                                DataUser.EMAIL =  __email;
                                DataUser.PASS = __password;
                                 //   DataUser.PASS =  [loginManager encryptJSONString:_passText.text withPassword:nil];
                                    DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                                    DataUser.APPID = @"iOS";
                                    DataUser.USERTYPE = @"NEGOCIO";
                                    DataUser.OS = [[UIDevice currentDevice] systemVersion];
                                    DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                                    DataUser.MANOFACTURER =@"Apple";
                                    

                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                                    UIViewController *viewcontroller =(ContainerSliderPEViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Container"];
            
                                     UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
            
                                    [navs setNavigationBarHidden:YES animated:NO];
                                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                    [self presentViewController:navs animated:YES completion:nil];
            
                                }else{
                                    // si es empresa cualquier otro pais
                                  
                                     DataUser.JUMIOSTATUS = userData[@"jumioStatus"];
                                    DataUser.USERTYPE = @"NEGOCIO";
                                    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                                    DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                                    
                                    
                                    
                                //[self performSegueWithIdentifier:@"main_menu" sender:nil];
                                    


                                    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainCommerce" bundle:nil];
                                                                                                            UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"SignInCommerce"];
                                                                                               
                                                                                               UIStoryboard* storyboardTwo = [UIStoryboard storyboardWithName:@"Startup" bundle:nil];
                                                                                                                    UIViewController* controllerTwo = [storyboardTwo instantiateViewControllerWithIdentifier:@"initialView"];
                                                                            
                                                              
                                                                            
                                                                
                                                                                               
                                                                                               NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
                                                                                                 
                                                                                                 [navigationArray removeAllObjects];
                                                                                                 [navigationArray addObject:controllerTwo];
                                                                                                 [navigationArray addObject:controller];
                                                                                                 self.navigationController.viewControllers = navigationArray;
                                                                            
                                                                            

                                    
                                    
                                    
                                }
                            }
                            break;
                            ////////////////////  Validación de idUsrStatus   Raul Mendez ///////////////////////////
                            case 100:
                            
                        {/*
                            [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                            [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                            [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                            [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                            [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                            [[NSUserDefaults standardUserDefaults] setObject:response[kIdNegocio] forKey:(NSString*)@"IdNegocio"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
                                UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
                                Register_Step30 *nextView = [[nav viewControllers] firstObject];
                                
                                [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                                [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                                
                                [self.navigationController presentViewController:nav animated:YES completion:nil];
                                
                                [alertMsg dismissViewControllerAnimated:YES completion:nil];
                            }];
                            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                            }];
                            [alertMsg addAction:ok];
                            [alertMsg addAction:cancel];
                            
                            [self presentViewController:alertMsg animated:YES completion:nil];
                            
                            return;
                        */
                            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:
                                                     @"Error 100, contactar a soporte." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alertMsg show];
                            return;
                        }
                            break;
                    }
                    
                }
                    break;
               /*     case 2:
                
                {
                    //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
                    
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                    [alertMsg show];
                    
                    return;
                }
                    break;
                */
              /*      case 3:
                {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    
                    DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                    
                    DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                    
                    DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                    DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                    DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                    
                    [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
                    return;
                }
                    break;
               */
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
        
          
        
        
        
    }
    
    
}

@end
