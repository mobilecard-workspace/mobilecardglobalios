//
//  Register_Commerce_Step20.h
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "MC_TermsAndConditionsCtrller.h"
#import "register_Commerce_Step30.h"
#import "register_Commerce_Step40.h"
#import "Register_Step30.h"

@interface Register_Commerce_Step20 : mT_commonTableViewController <termsDelegate, addAccountProtocol, activateAccountProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate, registerSMSProtocol>

@property (strong, nonatomic) NSDictionary *data;

@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastNameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *motherLastNameText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *businessNameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;

@property (weak, nonatomic) IBOutlet UIImageView *imageView_ID;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_Address;

@property (weak, nonatomic) IBOutlet UISwitch *termsSwitch;

@property (weak, nonatomic) IBOutlet UIButton *addPayment_Button;

@property (weak, nonatomic) IBOutlet UIButton *getAccount_Button;
@property (weak, nonatomic) IBOutlet UIButton *activateAccount_Button;

@property (weak, nonatomic) NSString *_email;
@property (weak, nonatomic) IBOutlet UIButton *save_Button;
@end
