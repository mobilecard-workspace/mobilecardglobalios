//
//  Register_Commerce_Step10.m
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "Register_Commerce_Step10.h"
#import "Register_Commerce_Step20.h"
#import <Mobilecard-Swift.h>

@interface Register_Commerce_Step10 ()
{
    pootEngine *encrypter;
    MC_TermsAndConditionsCtrller *termsView;
    
     pootEngine *registerManager;
    
     pootEngine *bankManager;
    
    NSMutableDictionary *paramsToSend;
    
}
@end

@implementation Register_Commerce_Step10


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (__isWelcomePack.length > 0){
        NSLog(@"Si viene welcome pack y este es el serialQR %@",__serialQR);
             [_mobileCardSwitch setOn:YES animated:NO];
           [_bankSwitch setOn:NO animated:NO];
        [_mobileCardSwitch setUserInteractionEnabled:NO];
           [_bankSwitch setUserInteractionEnabled:NO];
    }else{
          NSLog(@"Registro normal");
    }
    
      [self initializePickersForView:self.navigationController.view];
    
    termsView = [[MC_TermsAndConditionsCtrller alloc] initWithNibName:@"MC_TermsAndConditionsCtrller" bundle:nil];
       [termsView setDelegate:self];
   
    
   // [self addShadowToView:self.navigationController.navigationBar];
   // [self addShadowToView:_continue_Button];
    
    encrypter = [[pootEngine alloc] init];
    [encrypter setShowComments:developing];
    
    [_commerceText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_confirmEmailText setUpTextFieldAs:textFieldTypeEmail];
    [_phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [_passwordText setUpTextFieldAs:textFieldTypePassword];
    [_passwordConfirmText setUpTextFieldAs:textFieldTypePassword];
    
    [_accountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_bankText setUpTextFieldAs:textFieldTypeRequiredCombo];
    
    
    
    [self addValidationTextField:_commerceText];
    [self addValidationTextField:_emailText];
      [self addValidationTextField:_confirmEmailText];
    [self addValidationTextField:_phoneText];
    [self addValidationTextField:_passwordText];
    [self addValidationTextField:_passwordConfirmText];
    
    

    [self addValidationTextField:_accountText];
      [self addValidationTextField:_bankText];
    
    [self addValidationTextFieldsToDelegate];
    
    
    //si es negocio Mexico
  //  if ([DataUser.COUNTRYID  isEqual: @"1"] && [DataUser.USERTYPE  isEqual: @"NEGOCIO"]){
        
      
        [self->_phoneText setText:__telefono];
        _emailText.userInteractionEnabled = YES;
        _phoneText.userInteractionEnabled = NO;
    
  //  }else{
        
   // [self->_emailText setText:__email];
  //  [self->_phoneText setText:__telefono];
  //  _emailText.userInteractionEnabled = NO;
  //  _phoneText.userInteractionEnabled = NO;
        
        
 //   }
    
         bankManager = [[pootEngine alloc] init];
           [bankManager setDelegate:self];
           [bankManager setShowComments:developing];
           
           [bankManager startWithoutPostRequestWithURL:WSCatalogGetBanks];

        [self lockViewWithMessage:nil];

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continue_Action:(id)sender {
 //   if (![self validateFieldsInArray:[self getValidationTextFields]]) {
  //      return;
  //  }
    
    
    if ([[_commerceText text] length] < 3){
              
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                           message:@"Debes agregar un nombre de comercio valido para continuar."
                                           preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {}];

                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
        return;
        
    }
    
    
    if ([[_emailText text] length] < 3){
                
          UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                             message:@"Debes agregar un correo electrónico valido para continuar."
                                             preferredStyle:UIAlertControllerStyleAlert];

                  UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action) {}];

                  [alert addAction:defaultAction];
                  [self presentViewController:alert animated:YES completion:nil];
        return;
          
      }
    
    
    if ([[_nameText text] length] < 3){
                   
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                                message:@"Debes agregar un nombre valido para continuar."
                                                preferredStyle:UIAlertControllerStyleAlert];

                     UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {}];

                     [alert addAction:defaultAction];
                     [self presentViewController:alert animated:YES completion:nil];
        return;
         }
    
    if ([[_fatherLastNameText text] length] < 3){
              
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                           message:@"Debes agregar un apellido paterno valido para continuar."
                                           preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {}];

                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if ([[_motherLastNameText text] length] < 3){
              
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                           message:@"Debes agregar un apellido materno valido para continuar."
                                           preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {}];

                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    
    if (([[_passwordText text] length] < 8)){
              
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                           message:@"Debes agregar una contraseña valida para continuar."
                                           preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {}];

                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
        
        return;
        
    }

    
    
    if (![[_passwordText text] isEqualToString:[_passwordConfirmText text]]) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"La contraseña y la confirmación deben coincidir, intente de nuevo", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    
    if (![[_emailText text] isEqualToString:[_confirmEmailText text]]) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"El correo electrónico y la confirmación deben coincidir, intente de nuevo", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if ((_bankSwitch.on == NO) && (_mobileCardSwitch.on == NO)) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"!Aviso¡", nil) message:NSLocalizedString(@"Debes agregar una cuenta bancaria o generar una cuenta MobileCard para continuar.", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
               [alertMsg show];
        return;
    }
    
    if (!_termsSwitch.on) {
           UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Para continuar, es necesario aceptar los términos y condiciones", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
           [alertMsg show];
           return;
       }
    
    
    
    registerManager = [[pootEngine alloc] init];
       [registerManager setDelegate:self];
       [registerManager setShowComments:developing];
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
  
    //validacion de si esta el switch  mandar esos parametros de clave y entidad
    
      [params setObject:@"" forKey:@"clabe"];
      [params setObject:@"0" forKey:@"idBanco"];
    
    //Validating all inputs
      if (_bankSwitch.on) {
          
          if ((_accountText.text.length < 18)|| (_accountText.text.length >18)){
              
              UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Debes agregar una clabe valida para continuar.", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
              
              return;
          }
          
          NSLog(@"Este es el texto %@",_bankText.text);
          if ([_bankText.text  isEqual: @""]){
              UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"Debes seleccionar un banco para continuar.", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
              [alertMsg show];
              return;
          }
          
      
          [params setObject:_accountText.text forKey:@"clabe"];
             [params setObject:[_bankText infoArray][[_bankText selectedID]][[_bankText idLabel]] forKey:@"idBanco"];
          //agregamos los parametros de envio
          
      }
    
    
    [params setObject:_phoneText.text forKey:@"celular"];
      [params setObject:@"" forKey:@"ciudad"];
     [params setObject:@"" forKey:@"colonia"];
     [params setObject:@"" forKey:@"cp"];
         [params setObject:@"" forKey:@"direccion"];
        [params setObject:_emailText.text forKey:@"email"];
     [params setObject:@"0" forKey:@"idEstado"];
     [params setObject:_motherLastNameText.text forKey:@"materno"];
     [params setObject:_nameText.text forKey:@"nombre"];
     [params setObject:_fatherLastNameText.text forKey:@"paterno"];
     [params setObject:_commerceText.text forKey:@"nombreEstablecimiento"];
    
      [params setObject:[self encryptString:_passwordText.text] forKey:@"password"];
    
    //completamos los parametros de envio
    

    developing?NSLog(@"params to be sent -> %@", params):nil;
    
    
    
    if  (_mobileCardSwitch.on == YES){
        paramsToSend = params;
                        [self performSegueWithIdentifier:@"registerCard" sender:nil];
        
        return;
    }
      
      @try {
          NSError *JSONError;
          NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
          NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
          
          [registerManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@", userManagementURL, @"1/es/comercio/alta"] withPost:JSONString];
          
          [self lockViewWithMessage:NSLocalizedString(@"Realizando registro...", nil)];
      } @catch (NSException *exception) {
          NSLog(@"EXCEPTION -> %@", exception.description);
      } @finally {
          nil;
      }
   // [self performSegueWithIdentifier:@"step2" sender:self];
    
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Reg1",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}


- (IBAction)switchObtainMobilecardAction:(UISwitch *)sender {
    
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        
        [_bankSwitch setOn:NO animated:YES];
        
                 [_accountText setText:@""];
                
                  [_bankText setText:@""];
                 _heightNormalConstraint.constant = 0;
                 
                 _accountText.hidden = YES;
                          _bankText.hidden = YES;
        
    }else{
        //si se desactiva validamos que no tenemos datos de mobilecard ya
        
        
    }
    
}


//metodo que se ejecuta al presionar el switch
- (IBAction)switchAction:(UISwitch *)sender {
    
     UISwitch *mySwitch = (UISwitch *)sender;
      if ([mySwitch isOn]) {
          
          [_mobileCardSwitch setOn:NO animated:YES]; _heightNormalConstraint.constant = 30;
          _accountText.hidden = NO;
          _bankText.hidden = NO;
      } else {
          
        
         
          
          [_accountText setText:@""];
         
           [_bankText setText:@""];
          _heightNormalConstraint.constant = 0;
          
          _accountText.hidden = YES;
                   _bankText.hidden = YES;
          
      }
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"registerCard"]) {
        
        register_Commerce_Step40 *nextView = [segue destinationViewController];
             
        nextView._data = paramsToSend;
        nextView._email = _emailText.text;
        nextView._password = _passwordText.text;
        nextView._serialQR = __serialQR;
        
        
    }
    
    if ([[segue identifier] isEqualToString:@"step2"]) {
        Register_Commerce_Step20 *nextView = [segue destinationViewController];
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setObject:_commerceText.text forKey:@"nombre_establecimiento"];
        [data setObject:_emailText.text forKey:@"usuario"];
        [data setObject:[self encryptString:_passwordText.text] forKey:@"pass"];
        [data setObject:_emailText.text forKey:@"email_contacto"];
        [data setObject:_phoneText.text forKey:@"telefono_contacto"];
        [nextView set_email:__email];
        [nextView setData:data];
    }
}

- (IBAction)privacyAction:(UIButton *)sender {
    
    //puente para mostrar el aviso de privacidad
    
    [SendTokenFirestore privacyMexTwoWithView:self];
    
    
    
    
}


#pragma mark terms handler
- (IBAction)termsSwitch_Action:(id)sender {
    if (_termsSwitch.on) {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:termsView];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

- (void)termsAcceptance:(BOOL)result
{
    _termsSwitch.on = result;
}




- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == bankManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            
            if (response.count < 1){
                                   UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                                                          
                                                          
                                                          [alertMsg show];
                                                          return;
                               }
            
            
            
            [_bankText setInfoArray:response[@"banks"]];
            [_bankText setDescriptionLabel:@"nombre_corto"];
            [_bankText setIdLabel:@"id"];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    
    if (manager == registerManager){
        
        
       NSDictionary *response = (NSDictionary*)json;
        response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
        
        if ([response[kIDError] intValue] == 0) {
            switch ([response[kIDJumioCommerce] intValue]) {
            /*        case 0:
                {
                    
                    
                    if (response.count > 1){
                    
                    //variable para no tener el data del autologin antes del jumio
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    
                    DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                    
                    DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                    
                    DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                    DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                    DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                    
                    
                   [self performSegueWithIdentifier:@"init_boarding_jumio" sender:nil];
                        return;
                    }else{
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                        
                        
                        [alertMsg show];
                        return;
                        
                    }
                        
                }
                    break;*/
                case 1:
                case 2:
                case 3:
                case 0:
                {
                    
                    if (response.count < 1){
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                                               
                                               
                                               [alertMsg show];
                                               return;
                    }
                    
                    switch ([response[kIDStatus] intValue]) {
                       //       switch (100) {
                            case 98:
                        {
                        //    preliminaryUserData = response;
        
                            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                            [alertMsg setTag:3031];
                            [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                            [alertMsg show];
                            return;
                        }
                            break;
                            
                        default:
                        {
                                [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                                [[NSUserDefaults standardUserDefaults] setObject:_passwordText.text forKey:(NSString*)kUserPassword];
                                [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                                [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                                [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
            
                            
                                NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                            
                            
                            DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                            
                            DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                            
                            DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                            DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                            DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                            
                            
                       
                           
                            
                                //[response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                                //[response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            
                            // si es peru empresa
                            if([[[userData objectForKey:kUserIdCountry] stringValue]  isEqual: @"4"]){
                                    
                                    
                                    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                                
                                DataUser.COUNTRY = @"PE";
                                //DataUser.COUNTRY = [countryInfo objectForKey:kDetailsKey];  ////// Raul mendez(TE ODIO)
                                    DataUser.EMAIL =  _emailText.text;
                                DataUser.PASS = _passwordText.text;
                                 //   DataUser.PASS =  [loginManager encryptJSONString:_passText.text withPassword:nil];
                                    DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                                    DataUser.APPID = @"iOS";
                                    DataUser.USERTYPE = @"NEGOCIO";
                                    DataUser.OS = [[UIDevice currentDevice] systemVersion];
                                    DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                                    DataUser.MANOFACTURER =@"Apple";
                                    

                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                                    UIViewController *viewcontroller =(ContainerSliderPEViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Container"];
            
                                     UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
            
                                    [navs setNavigationBarHidden:YES animated:NO];
                                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                    [self presentViewController:navs animated:YES completion:nil];
            
                                }else{
                                    // si es empresa cualquier otro pais
                                  
                                     DataUser.JUMIOSTATUS = userData[@"jumioStatus"];
                                    DataUser.USERTYPE = @"NEGOCIO";
                                    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                                    DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                                    
                                    
                                    
                                //[self performSegueWithIdentifier:@"main_menu" sender:nil];
                                    


                                    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainCommerce" bundle:nil];
                                                                                                            UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"SignInCommerce"];
                                                                                               
                                                                                               UIStoryboard* storyboardTwo = [UIStoryboard storyboardWithName:@"Startup" bundle:nil];
                                                                                                                    UIViewController* controllerTwo = [storyboardTwo instantiateViewControllerWithIdentifier:@"initialView"];
                                                                            
                                                              
                                                                            
                                                                
                                                                                               
                                                                                               NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
                                                                                                 
                                                                                                 [navigationArray removeAllObjects];
                                                                                                 [navigationArray addObject:controllerTwo];
                                                                                                 [navigationArray addObject:controller];
                                                                                                 self.navigationController.viewControllers = navigationArray;
                                                                            
                                                                            

                                    
                                    
                                    
                                }
                            }
                            break;
                            ////////////////////  Validación de idUsrStatus   Raul Mendez ///////////////////////////
                            case 100:
                            
                        {/*
                            [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                            [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                            [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                            [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                            [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                            [[NSUserDefaults standardUserDefaults] setObject:response[kIdNegocio] forKey:(NSString*)@"IdNegocio"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
                                UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
                                Register_Step30 *nextView = [[nav viewControllers] firstObject];
                                
                                [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                                [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                                
                                [self.navigationController presentViewController:nav animated:YES completion:nil];
                                
                                [alertMsg dismissViewControllerAnimated:YES completion:nil];
                            }];
                            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                            }];
                            [alertMsg addAction:ok];
                            [alertMsg addAction:cancel];
                            
                            [self presentViewController:alertMsg animated:YES completion:nil];
                            
                            return;
                        */
                            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:
                                                     @"Error 100, contactar a soporte." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alertMsg show];
                            return;
                        }
                            break;
                    }
                    
                }
                    break;
               /*     case 2:
                
                {
                    //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
                    
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                    [alertMsg show];
                    
                    return;
                }
                    break;
                */
              /*      case 3:
                {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    
                    DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                    
                    DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                    
                    DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                    DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                    DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                    
                    [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
                    return;
                }
                    break;
               */
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
        
          
        
        
        
    }

    
}


-(void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self subPickerView:pickerView didSelectRow:row inComponent:component];
    
    if ([[_bankText infoArray][[_bankText selectedID]][[_bankText idLabel]] intValue] == 11 || [[_bankText infoArray][[_bankText selectedID]][[_bankText idLabel]] intValue] == 20) { //BANORTE E IXE
        [_accountText setPlaceholder:@"INGRESA CLABE INTERBANCARIA"];
        [_accountText setMinLength:10];
        [_accountText setMaxLength:10];
    } else {
        [_accountText setPlaceholder:@"CUENTA CLABE"];
        [_accountText setMinLength:18];
        [_accountText setMaxLength:18];
    }
    [_accountText setText:@""];
}

-(void) successComplete
{}

-(void) failComplete
{}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
switch (indexPath.row){
case 0:
    {
        return 70;
    }
    break;
        
case 1:
        {
            return 255;
        }
        break;
case 2:
           {
               return 20;
           }
           break;
case 3:
        {
            return 195;
        }
        break;
case 4:
        {
            return 20;
        }
        break;
        
case 5:
        {
            return 135;
        }
        break;
case 6:
        {
            return 20;
        }
        break;
        
case 7:
        {
        if (__isWelcomePack.length > 0){
                                      return 0;
                                  }else{
                                
                                 return 163;
                                  }
        }
        break;
        
case 8:
        {
        
             if (__isWelcomePack.length > 0){
                           return 0;
                       }else{
                     
                      return 239;
                       }
        }
        break;
        
    case 9:
        {
            
         
            return 100;
             
        }
        break;
case 10:
               {
                   
                    return 130;
                 
               }
               break;
    
default:{
    return 0;
}
    break;
    
}
    
    return 0 ;
}


@end
