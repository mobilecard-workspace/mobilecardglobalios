//
//  Register_Commerce_Step10.h
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.


//
#import "MC_TermsAndConditionsCtrller.h"

#import "mT_commonTableViewController.h"




@protocol registerCommerceProtocol <NSObject>

@required
- (void) registryCommerceShallLaunchMyMCWithUserData:(NSDictionary*)userdata;
@end

@interface Register_Commerce_Step10 : mT_commonTableViewController<termsDelegate>

@property (nonatomic, assign) id <registerCommerceProtocol, NSObject> delegate;
@property (weak, nonatomic) IBOutlet UITextField_Validations *confirmEmailText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *commerceText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passwordText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passwordConfirmText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *fatherLastNameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *motherLastNameText;
@property (weak, nonatomic) IBOutlet UITextField *institutionText;
@property (weak, nonatomic) IBOutlet UITextField *clabeText;

@property (weak, nonatomic) IBOutlet UISwitch *termsSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightNormalConstraint;
@property (weak, nonatomic) IBOutlet UISwitch *bankSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *mobileCardSwitch;



@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@property (weak, nonatomic) IBOutlet UITextField_Validations *accountText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *bankText;





@property (strong, nonatomic) NSString *_isWelcomePack;
@property (strong, nonatomic) NSString *_serialQR;
@property (strong, nonatomic) NSString *_telefono;
@property (strong, nonatomic) NSString *_email;
@end
