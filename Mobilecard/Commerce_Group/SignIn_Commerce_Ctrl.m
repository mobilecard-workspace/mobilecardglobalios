//
//  SignIn_Commerce_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "SignIn_Commerce_Ctrl.h"
#import "Register_Commerce_Step10.h"
#import <Mobilecard-Swift.h>

@interface SignIn_Commerce_Ctrl ()
{
    pootEngine *loginManager;
    pootEngine *changeManager;
     ServicesObjc *services;
    NSDictionary *preliminaryUserData;
}
@end

@implementation SignIn_Commerce_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];

    [self autologin];
    services = [[ServicesObjc alloc] init];
        services.delegate = self;
    //RAUL EN ESTAS VARIABLES PUEDES GUARDAD LOS DATOS DEL LOGIN con un ejemplo del tipo de dato que es el que debes igualar
    
  /*  DataUser.COUNTRY =  String  "PE"
    DataUser.EMAIL =   String  "correo@hotmail.com"
    DataUser.PASS =   String "123456"
    DataUser.IMEI = String "a920i392u203023"
    DataUser.APPID = String "12"
    DataUser.USERTYPE = String "loqueseaqueguardes"
    DataUser.OS = String "loqueseaqueguardes"
    DataUser.COUNTRYID = String "4"
    DataUser.MANOFACTURER = String "loqueseaqueguardes"
*/
    
  //  [self addShadowToView:self.navigationController.navigationBar];
  //  [self addShadowToView:_signIn_Button];
    
    [_commerceText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_passText setUpTextFieldAs:textFieldTypePassword];
    
    //[self addValidationTextField:_commerceText];
    [self addValidationTextField:_emailText];
    [self addValidationTextField:_passText];
    
    [self addValidationTextFieldsToDelegate];
    
    
    
    //autologin Alejandro
  /*  if(DataUser.PASS != nil){
        
        
        //pais
        if( [DataUser.COUNTRY isEqualToString:@"PERU"] || [DataUser.COUNTRY isEqualToString:@"PE"]){
            DataUser.COUNTRY = @"PE";
            
            //negocio
            if([DataUser.USERTYPE isEqualToString:@"NEGOCIO"]){
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                UIViewController *viewcontroller =(ContainerSliderPEViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Container"];
                
                UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                
                [navs setNavigationBarHidden:YES animated:NO];
                
                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:navs animated:YES completion:nil];
                
            }
            
        }
    }else{
        
    //esto ya estaba
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey] && [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword]) {
        [self performSegueWithIdentifier:@"main_menu" sender:nil];
    }
        
    }*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewDidAppear:(BOOL)animated{
    
    
    DataUser.STATELOGIN = nil;
    
    if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"SessionSMS_To_Menu"] isEqualToString:@"1"]){
        [self startSession_Action:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"SessionSMS_To_Menu"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)register_Action:(id)sender {
    
   
    // si es peru
    if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"codigo"] isEqualToString:@"PE"]){
        
        //registroComercioPeru
        
        DataUser.COUNTRY = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"codigo"];
        
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
        
        RegisterSmsViewController *nextvc = [nextStory instantiateViewControllerWithIdentifier:@"smsRegister"];
        //se va con comercio  peru
        nextvc.isFrom = 2;
        
        [self.navigationController pushViewController:(nextvc) animated:YES];
        
        
    
        
        
    }else{
    
   //registro de comercio todos menos peru
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
        
        RegisterSmsViewController *nextvc = [nextStory instantiateViewControllerWithIdentifier:@"smsRegister"];
        //se va con comercio todos menos peru
        nextvc.isFrom = 1;
        
        [self.navigationController pushViewController:(nextvc) animated:YES];
        
    }
}


-(void)autologin{
    
     //autologin Alejandro
        if(DataUser.PASS != nil){
            //pais
        if( [DataUser.COUNTRY isEqualToString:@"PERU"] || [DataUser.COUNTRY isEqualToString:@"PE"]){
            DataUser.COUNTRY = @"PE";
            
            //negocio
            if([DataUser.USERTYPE isEqualToString:@"NEGOCIO"]){
            
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                UIViewController *viewcontroller =(ContainerSliderPEViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Container"];
                
                UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                
                [navs setNavigationBarHidden:YES animated:NO];
                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:navs animated:YES completion:nil];
            
                
            }else{
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                
                UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                
                [navs setNavigationBarHidden:YES animated:NO];
                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                
                [self presentViewController:navs animated:YES completion:nil];
             
                
                
                //[self performSegueWithIdentifier:@"startMain" sender:nil];
            }
        
        }else{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]) {
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
                    //[self performSegueWithIdentifier:@"startMain" sender:nil];
                    ////// reemplazo de codigo para ingresar al view de log-in Raul mendez ///////////
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                    
                    //[self performSegueWithIdentifier:@"startMain" sender:nil];
                } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
                   
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainCommerce"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuCommerce"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    
                    navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                    
                    //   [self performSegueWithIdentifier:@"startMain_Commerce" sender:nil];
                } else {
                 
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
                    UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuUser"];
                    
                    UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
                    
                    [navs setNavigationBarHidden:YES animated:NO];
                    
                  navs.modalPresentationStyle = UIModalPresentationFullScreen;
                    
                    [self presentViewController:navs animated:YES completion:nil];
                   
                    
                    // [self performSegueWithIdentifier:@"startMain" sender:nil];
                }
            }
        }
            
            
        }else{
    
    //termina login
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
            //[self performSegueWithIdentifier:@"startMain" sender:nil];
            ////// reemplazo de codigo para ingresar al view de log-in Raul mendez ///////////
            [self performSegueWithIdentifier:@"startMain" sender:nil];
           
        } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
          
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainCommerce"bundle:nil];
            UIViewController *viewcontroller =(LGSideMenuController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuCommerce"];
            
            
            
            UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
            
             navs.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [navs setNavigationBarHidden:YES animated:NO];
            
            navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            [self presentViewController:navs animated:YES completion:nil];
           
            //  [self performSegueWithIdentifier:@"startMain_Commerce" sender:nil];
        } else {
            [self performSegueWithIdentifier:@"startMain" sender:nil];
            
        }
    }
            
        }//termina el if del pass != nil que lo otro era el autologin pasado
    
    
}

- (IBAction)startSession_Action:(id)sender {
    [self dismissAllTextFields];
    
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    //NEED TO UPDATE LOGIN
    loginManager = [[pootEngine alloc] init];
    [loginManager setDelegate:self];
    [loginManager setShowComments:developing];
    
  
    //NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [[NSUserDefaults standardUserDefaults] setObject:_commerceText.text forKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] setObject:_emailText.text forKey:@"user_email"];
 /*   [params setObject:_emailText.text forKey:@"usrLoginOrEmail"];
    [params setObject:[self encryptString:_passText.text] forKey:kUserPassword];
    //MUST HAVE PARAMETERS
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"manufacturer"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
    [params setObject:@"iOS" forKey:@"platform"];
    [params setObject:@"NEGOCIO" forKey:@"tipoUsuario"];
    
   
    */
    
   // NSError *JSONError;
  //  NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
 //   NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *header = @{};
    NSDictionary *params = @{@"usrLoginOrEmail":_emailText.text,kUserPassword:[self encryptString:_passText.text],@"imei":[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"manufacturer":@"Apple",@"os":[[UIDevice currentDevice] systemVersion],@"platform":@"iOS",@"tipoUsuario":@"NEGOCIO"};
       [services sendWithType:@"POST" url:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] params:params header:header message:@"Ingresando" animate:YES];
   
    //[loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
    
   // [self lockViewWithMessage:NSLocalizedString(@"Iniciando sesión...", nil)];
}

- (IBAction)resetPassword_Action:(id)sender {
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Recuperar contraseña", nil) message:NSLocalizedString(@"Escribe tu nombre de usuario o correo electrónico en el campo para recuperar tu contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Recuperar contraseña", nil), nil];
    [alertMsg setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alertMsg setTag:100];
    [alertMsg show];
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
  /*  if (manager == loginManager) {
        NSDictionary *response = (NSDictionary*)json;
        response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
        
        if ([response[kIDError] intValue] == 0) {
            switch ([response[kIDJumioCommerce] intValue]) {
            /*        case 0:
                {
                    
                    
                    if (response.count > 1){
                    
                    //variable para no tener el data del autologin antes del jumio
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    
                    DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                    
                    DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                    
                    DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                    DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                    DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                    
                    
                   [self performSegueWithIdentifier:@"init_boarding_jumio" sender:nil];
                        return;
                    }else{
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                        
                        
                        [alertMsg show];
                        return;
                        
                    }
                        
                }
                    break;*/
         //       case 1:
           //     case 2:
             //   case 3:
              /*  case 0:
                {
                    
                    if (response.count < 1){
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                                               
                                               
                                               [alertMsg show];
                                               return;
                    }
                    
                    switch ([response[kIDStatus] intValue]) {
                       //       switch (100) {
                            case 98:
                        {
                            preliminaryUserData = response;
        
                            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                            [alertMsg setTag:3031];
                            [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                            [alertMsg show];
                            return;
                        }
                            break;
                            
                        default:
                        {
                                [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                                [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                                [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                                [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                                [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
            
                            
                                NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                            
                            
                            DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                            
                            DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                            
                            DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                            DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                            DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                            
                            
                       
                           
                            
                                //[response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                                //[response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            
                            // si es peru empresa
                            if([[[userData objectForKey:kUserIdCountry] stringValue]  isEqual: @"4"]){
                                    
                                    
                                    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                                
                                DataUser.COUNTRY = @"PE";
                                //DataUser.COUNTRY = [countryInfo objectForKey:kDetailsKey];  ////// Raul mendez(TE ODIO)
                                    DataUser.EMAIL =  _emailText.text;
                                    DataUser.PASS =  [loginManager encryptJSONString:_passText.text withPassword:nil];
                                    DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                                    DataUser.APPID = @"iOS";
                                    DataUser.USERTYPE = @"NEGOCIO";
                                    DataUser.OS = [[UIDevice currentDevice] systemVersion];
                                    DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                                    DataUser.MANOFACTURER =@"Apple";
                                    

                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                                    UIViewController *viewcontroller =(ContainerSliderPEViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Container"];
            
                                     UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
            
                                    [navs setNavigationBarHidden:YES animated:NO];
                                navs.modalPresentationStyle = UIModalPresentationFullScreen;
                                navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                    [self presentViewController:navs animated:YES completion:nil];
            
                                }else{
                                    // si es empresa cualquier otro pais
                                  
                                     DataUser.JUMIOSTATUS = userData[@"jumioStatus"];
                                    DataUser.USERTYPE = @"NEGOCIO";
                                    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                                    DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                                [self performSegueWithIdentifier:@"main_menu" sender:nil];
                                }
                            }
                            break;
                            ////////////////////  Validación de idUsrStatus   Raul Mendez ///////////////////////////
                            case 100:
                            
                        {/*
                            [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                            [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                            [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                            [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                            [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                            [[NSUserDefaults standardUserDefaults] setObject:response[kIdNegocio] forKey:(NSString*)@"IdNegocio"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
                                UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
                                Register_Step30 *nextView = [[nav viewControllers] firstObject]; 
                                
                                [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                                [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                                
                                [self.navigationController presentViewController:nav animated:YES completion:nil];
                                
                                [alertMsg dismissViewControllerAnimated:YES completion:nil];
                            }];
                            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                            }];
                            [alertMsg addAction:ok];
                            [alertMsg addAction:cancel];
                            
                            [self presentViewController:alertMsg animated:YES completion:nil];
                            
                            return;
                        */
       /*                     UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:
                                                     @"Error 100, contactar a soporte." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alertMsg show];
                            return;
                        }
                            break;
                    }
                    
                }
                    break;
               /*     case 2:
                
                {
                    //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
                    
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                    [alertMsg show];
                    
                    return;
                }
                    break;
                */
              /*      case 3:
                {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    
                    DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                    
                    DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                    
                    DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                    
                    DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                    DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                    
                    [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
                    return;
                }
                    break;
               */
         /*   }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }*/
    
    if (changeManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        switch (changeManager.tag) {
            case 3031:
            {
                if ([response[kIDError] intValue] == 0) {
                    
                    [preliminaryUserData setValue:@"1" forKey:@"idUsrStatus"];
                    [[NSUserDefaults standardUserDefaults] setObject:_validationText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:preliminaryUserData forKey:(NSString*)kUserDetailsKey];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                    
                    [self performSegueWithIdentifier:@"main_menu" sender:nil];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
            }
                break;
            case 100:
            {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark register Delegate
/*
- (void)registryShallLaunchMyMCWithUserData:(NSDictionary *)userdata
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
    UINavigationController *nav = [storyboard instantiateInitialViewController];
    MCCard_Ctrl *nextView = [nav childViewControllers].firstObject;
    [nextView setUserData:userdata];
    
    [super setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [super presentViewController:nav animated:YES completion:nil];
}
*/

#pragma Mark UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (!(buttonIndex == 0)) {
        switch (alertView.tag) {
            case 3031: //new password request
            {
                [_validationText setUpTextFieldAs:textFieldTypePassword];
                [_validationText setText:[alertView textFieldAtIndex:0].text];
                
                if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                    changeManager = [[pootEngine alloc] init];
                    [changeManager setDelegate:self];
                    [changeManager setShowComments:developing];
                    [changeManager setTag:alertView.tag];
                    
                    [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@/commerce/passUpdate?userAdmin=%d&newPass=%@", userManagementURL, NSLocalizedString(@"lang", nil), [preliminaryUserData[@"idEstablecimiento"] intValue], [self encryptString:[_passText text]]]];
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                }
                
            }
                break;
                
            case 100: //reset password
            {
                [_validationText setUpTextFieldAs:textFieldTypeUserName];
                [_validationText setText:[alertView textFieldAtIndex:0].text];
                
                if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                    changeManager = [[pootEngine alloc] init];
                    [changeManager setDelegate:self];
                    [changeManager setShowComments:developing];
                    [changeManager setTag:alertView.tag];
                    
                    
                    NSString *post = nil;
                    post = [NSString stringWithFormat:@"userAdmin=%@", _validationText.text];
                    
                    [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/commerce/passReset"] withPost:post];
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                }
            }
            default:
                break;
        }
    }
}


- (NSString*) convertToURLCompatible: (NSString*)stringy
{
    NSString * escapedUrlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)stringy,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]ÁáÉéÍíÓóÚúñ. %",kCFStringEncodingUTF8));
    
    return escapedUrlString;
}


//delegados de los servicios
- (void)responseServiceWithType:(NSString *)type endpoint:(NSString *)endpoint response:(NSDictionary *)response{
    
    
  if ([endpoint isEqualToString:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"]]) {
      
          
           response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
           
           if ([response[kIDError] intValue] == 0) {
               switch ([response[kIDJumioCommerce] intValue]) {
               /*        case 0:
                   {
                       
                       
                       if (response.count > 1){
                       
                       //variable para no tener el data del autologin antes del jumio
                       [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                       [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                       [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                       [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                       [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                       [[NSUserDefaults standardUserDefaults] synchronize];
                       
                       
                       NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                       
                       
                       DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                       
                       DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                       
                       DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                       
                       DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                       DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                       
                       
                      [self performSegueWithIdentifier:@"init_boarding_jumio" sender:nil];
                           return;
                       }else{
                           UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                           
                           
                           [alertMsg show];
                           return;
                           
                       }
                           
                   }
                       break;*/
                   case 1:
                   case 2:
                   case 3:
                   case 0:
                   {
                       
                       if (response.count < 1){
                           UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                                                  
                                                  
                                                  [alertMsg show];
                                                  return;
                       }
                       
                       switch ([response[kIDStatus] intValue]) {
                          //       switch (100) {
                               case 98:
                           {
                               preliminaryUserData = response;
           
                               UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                               [alertMsg setTag:3031];
                               [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                               [alertMsg show];
                               return;
                           }
                               break;
                               
                           default:
                           {
                                   [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                                   [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                                   [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                                   [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                                   [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                                   [[NSUserDefaults standardUserDefaults] synchronize];
               
                               
                                   NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                               
                               
                               DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                               
                               DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                               
                               DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                       
                               DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                               DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                               
                               
                          
                              
                               
                                   //[response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                                   //[response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                               
                               // si es peru empresa
                               if([[[userData objectForKey:kUserIdCountry] stringValue]  isEqual: @"4"]){
                                       
                                       
                                       NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                                   
                                   DataUser.COUNTRY = @"PE";
                                   //DataUser.COUNTRY = [countryInfo objectForKey:kDetailsKey];  ////// Raul mendez(TE ODIO)
                                       DataUser.EMAIL =  _emailText.text;
                                       DataUser.PASS =  [loginManager encryptJSONString:_passText.text withPassword:nil];
                                       DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                                       DataUser.APPID = @"iOS";
                                       DataUser.USERTYPE = @"NEGOCIO";
                                       DataUser.OS = [[UIDevice currentDevice] systemVersion];
                                       DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                                       DataUser.MANOFACTURER =@"Apple";
                                       

                                       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PEmainCommerce"bundle:nil];
                                       UIViewController *viewcontroller =(ContainerSliderPEViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Container"];
               
                                        UINavigationController *navs = [[UINavigationController alloc] initWithRootViewController: viewcontroller];
               
                                       [navs setNavigationBarHidden:YES animated:NO];
                                   navs.modalPresentationStyle = UIModalPresentationFullScreen;
                                   navs.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                       [self presentViewController:navs animated:YES completion:nil];
               
                                   }else{
                                       // si es empresa cualquier otro pais
                                     
                                        DataUser.JUMIOSTATUS = userData[@"jumioStatus"];
                                       DataUser.USERTYPE = @"NEGOCIO";
                                       NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                                       DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                                   [self performSegueWithIdentifier:@"main_menu" sender:nil];
                                   }
                               }
                               break;
                               ////////////////////  Validación de idUsrStatus   Raul Mendez ///////////////////////////
                               case 100:
                               
                           {/*
                               [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                               [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                               [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                               [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                               [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                               [[NSUserDefaults standardUserDefaults] setObject:response[kIdNegocio] forKey:(NSString*)@"IdNegocio"];
                               [[NSUserDefaults standardUserDefaults] synchronize];
                               [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                               [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                               UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                               UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                   UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
                                   UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
                                   Register_Step30 *nextView = [[nav viewControllers] firstObject];
                                   
                                   [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                                   [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                                   
                                   [self.navigationController presentViewController:nav animated:YES completion:nil];
                                   
                                   [alertMsg dismissViewControllerAnimated:YES completion:nil];
                               }];
                               UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                               }];
                               [alertMsg addAction:ok];
                               [alertMsg addAction:cancel];
                               
                               [self presentViewController:alertMsg animated:YES completion:nil];
                               
                               return;
                           */
                               UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:
                                                        @"Error 100, contactar a soporte." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                               [alertMsg show];
                               return;
                           }
                               break;
                       }
                       
                   }
                       break;
                  /*     case 2:
                   
                   {
                       //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
                       
                       
                       UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                       [alertMsg show];
                       
                       return;
                   }
                       break;
                   */
                 /*      case 3:
                   {
                       
                       [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                       [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                       [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                       [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                       [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                       [[NSUserDefaults standardUserDefaults] synchronize];
                       
                       
                       NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                       
                       
                       DataUser.USERIDD = [userData[@"idEstablecimiento"] stringValue];
                       
                       DataUser.COMISION = [userData[@"comisionPorcentaje"] stringValue];
                       
                       DataUser.TIPO_PERSONA = [userData valueForKey:@"tipo_persona"];
                       
                       DataUser.QREMPRESA = [userData valueForKey:@"qrBase64"];
                       DataUser.NAME =  [userData valueForKey:@"nombreEstablecimiento"];
                       
                       [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
                       return;
                   }
                       break;
                  */
               }
           } else {
               UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
               [alertMsg show];
           }
       }
    
}

- (void)responseArrayServiceWithType:(NSString *)type endpoint:(NSString *)endpoint response:(NSArray *)response{
    
    
   
}



- (void)errorServiceWithEndpoint:(NSString *)endpoint{
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"¡Aviso!", nil) message:NSLocalizedString(@"No fue posible establecer conexión con el servidor.", nil) preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
       
     }];
     
     [alertMsg addAction:ok];
     
     [self presentViewController:alertMsg animated:YES completion:nil];
     
    
}

@end
