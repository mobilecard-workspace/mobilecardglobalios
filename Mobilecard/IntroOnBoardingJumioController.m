//
//  IntroOnBoardingJumioController.m
//  Mobilecard
//
//  Created by Raul Mendez on 7/17/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "IntroOnBoardingJumioController.h"
#import <Mobilecard-Swift.h>

@interface IntroOnBoardingJumioController ()

@end

@implementation IntroOnBoardingJumioController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)continue_Action:(id)sender {
    
   
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"register_look_and_feel" bundle:nil];  
    NetverifyStartViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"OnBoarding"];
    [controller setTypeUser:_typeUser];
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (IBAction)continue_ActionRegister:(id)sender {
    
    if ([DataUser.USERTYPE  isEqual: @"NEGOCIO"] && [DataUser.COUNTRYID  isEqual: @"1"]){
      
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[MXWalletCommerceViewController class]]){
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
        
    }else{
        
    }
    
 //   [self performSegueWithIdentifier:@"session" sender:nil];
}


@end
