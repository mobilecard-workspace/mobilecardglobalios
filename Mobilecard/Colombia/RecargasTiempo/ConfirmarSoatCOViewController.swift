

import UIKit
import Alamofire

class ConfirmarSoatCOViewController: UIViewController ,ServicesDelegate{

   
    
    //elementos visuales
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var marcaLabel: UILabel!
    @IBOutlet weak var ccLabel: UILabel!
    @IBOutlet weak var lineaLabel: UILabel!
    @IBOutlet weak var telefonoLabel: UILabel!
    @IBOutlet weak var modeloLabel: UILabel!
    
    @IBOutlet weak var correoLabel: UILabel!
    @IBOutlet weak var placaLabel: UILabel!
    @IBOutlet weak var categoriaLabel: UILabel!
    
    @IBOutlet weak var seguroSoatLabel: UILabel!
    @IBOutlet weak var vigenciaIniLabel: UILabel!
    @IBOutlet weak var vigenciaFinLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var imagenTarjetaImageView: UIImageView!
    @IBOutlet weak var numeroTarjetaLabel: UILabel!
    
    let services = Services()
     var idTarjeta = 0
    
    @IBOutlet weak var continuarButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.services.delegate = self
        
        let  cardToUpdate :NSArray = UserDefaults.standard.object(forKey: "tarjetas") as! NSArray
        
        let tarjeta : NSDictionary = cardToUpdate[Int(DataUser.IDTARJETACO)!] as! NSDictionary
        let img_short = tarjeta["img_short"] as! String
        
        
        if let imageURL = URL(string: img_short){
            self.imagenTarjetaImageView.af_setImage(withURL: imageURL)
        }
        
        self.idTarjeta = tarjeta["idTarjeta"] as! Int
        
        let pan = tarjeta["pan"] as! String
        
        let cardManager : pootEngine = pootEngine()
        
        let panDecoded : String = cardManager.decryptedString(of: pan, withSensitive: false) as! String
        
        
        let lastFour = String(panDecoded.suffix(4))
        let firstFour = String(panDecoded.prefix(4))
        
        let stringCard = "\(firstFour) **** **** \(lastFour)"
        
        self.numeroTarjetaLabel.text = stringCard
        
        
        
        
        
        print("Este es el id de tarjeta \(tarjeta)")
        print("estos son los demas datos \(DataUser.DATA!)")
        
       // DataUser.DATA!["type"] as! String
        
      
        self.nameLabel.text = DataUser.DATA!["nombre"] as? String
        self.marcaLabel.text = DataUser.DATA!["marca"] as? String
        self.ccLabel.text = DataUser.DATA!["cc"] as? String
        self.lineaLabel.text = DataUser.DATA!["linea"] as? String
        self.telefonoLabel.text = DataUser.DATA!["telefono"] as? String
        self.modeloLabel.text = DataUser.DATA!["modelo"] as? String
        self.correoLabel.text = DataUser.DATA!["email"] as? String
        self.placaLabel.text = DataUser.DATA!["placa"] as? String
        self.categoriaLabel.text = DataUser.DATA!["categoria"] as? String
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
       
        if let formattedTipAmount = formatter.string(from: Double(DataUser.DATA!["seguroSoat"] as! String)! as NSNumber) {
            self.seguroSoatLabel.text = "\(String(formattedTipAmount))"
        }
        
     
        
        
        self.vigenciaIniLabel.text = DataUser.DATA!["vigenciaIni"] as? String
        self.vigenciaFinLabel.text = DataUser.DATA!["vigenciaFin"] as? String
        
        
        if let formattedTipAmountTotal = formatter.string(from: Double(DataUser.DATA!["total"] as! String)! as NSNumber) {
            
            self.totalLabel.text = "\(String(formattedTipAmountTotal))"
            
        }

    
        
        
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    //metodo que se ejecuta al presionar el botón de continuar
    @IBAction func continuarAction(_ sender: UIButton) {
        
      
            
          print("continuamos pago y jumio")
            

            let alert = UIAlertController(title: "¡Aviso!", message: "Una vez expedida la póliza, es irrevocable, ¿Desea continuar?", preferredStyle: .alert);
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil));
            alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action:UIAlertAction) in
     
                
                
                //acepto
                      let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
                      
                      let params : [String : Any] = ["nombre":DataUser.DATA!["nombre"] as! String,
                                                     "cc":DataUser.DATA!["cc"] as! String,
                                                     "telefono":DataUser.DATA!["telefono"] as! String,
                                                     "email":DataUser.DATA!["email"] as! String,
                                                     "marca":DataUser.DATA!["marca"] as! String,
                                                     "linea":DataUser.DATA!["linea"] as! String,
                                                     "modelo":DataUser.DATA!["modelo"] as! String,
                                                     "placa":DataUser.DATA!["placa"] as! String,
                                                     "categoria":DataUser.DATA!["categoria"] as! String,
                                                     "total":DataUser.DATA!["total"] as! String,
                                                     "vigenciaIni":DataUser.DATA!["vigenciaIni"] as! String,
                                                     "vigenciaFin":DataUser.DATA!["vigenciaFin"] as! String,
                                                     "seguroSoat":DataUser.DATA!["seguroSoat"] as! String,
                                                     "idUsuario":DataUser.USERIDD!,
                                                     "idPais":"2",
                                                     "idApp":"1",
                                                     "idioma":"es",
                                                     "idTarjeta":String(self.idTarjeta)]
                      
                      print("PARAMS: \(params)")
                      
                      self.services.send(type: .POST, url: "ColombiaMiddleware/api/soat/expedirPoliza", params: params, header: headers, message: "Pagando",animate: true)
                      
                          
                          
                
                
            }));
            
            present(alert, animated: true, completion: nil);
        
        
      
            
            
       
  
      

        
    }
    
 
    
    
    //delegado de los servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
       
        
        
        if endpoint == "ColombiaMiddleware/api/soat/expedirPoliza"{
            
            let codigo = response["codigo"] as! Int
            
            if codigo == 0{
                //exito
                self.performSegue(withIdentifier: "successSegue", sender: nil)
            }else{
                //fracaso
                self.performSegue(withIdentifier: "failSegue", sender: nil)
            }
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor. Intente nuevamente.", cancel: "OK")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "successSegue"{
            
            
            let destinoViewController = segue.destination as! AprobadaRecargaCOViewController
            
            destinoViewController.isFromSoat = true
        }
        
        if segue.identifier == "failSegue"{
            
            
            let destinoViewController = segue.destination as! RechazadaRecargaCOViewController
            
            destinoViewController.isFromSoat = true
        }
        
    }
    
   
}
