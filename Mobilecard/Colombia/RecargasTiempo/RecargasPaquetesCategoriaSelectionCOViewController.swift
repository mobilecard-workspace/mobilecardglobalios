

import UIKit
import Alamofire

class RecargasPaquetesCategoriaSelectionCOViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,ServicesDelegate{

    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imageCommerce: UIImageView!
    let services = Services()
    
    //objeto que recibimos de la clase pasada
    var elementLast : RecargaModel!
    
    //arreglo que controla la tableView
    var data : [RecargaModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        
        if let imageURL = URL(string: self.elementLast.imgcolor!){
            self.imageCommerce.af_setImage(withURL: imageURL)
        }
        

        self.services.delegate = self
        
        
        let headers  : HTTPHeaders = [:]
        
        
        let params : [String : Any] = [:]
        
        self.services.send(type: .GET, url: "Catalogos/1/2/es/puntored/getCategorias/\(self.elementLast.id!)", params: params, header: headers, message: "Obteniendo",animate: true)
        
    
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  
    
    
    
   
    
      //metodos del datasource de la tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.data.count
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celdaID = "categoriaTableIdentifier"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: celdaID,for: indexPath) as! CategoriaTableCell
        
        cell.titleLabel.text = self.data[indexPath.row].nombre
        
         cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
      return 80
    }
    
    
    
    
    
  
     //metodos del delegado de tableView
    //metodo que se ejecuta al presionar una celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selection index \(indexPath.row)")
        
        self.performSegue(withIdentifier: "showPaquetesDetalleIdentifier", sender: indexPath.row)
        
        
    }

    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
        print("RESPONSE \(response)")
        
        if endpoint == "Catalogos/1/2/es/puntored/getCategorias/\(self.elementLast.id!)"{
            
            
            
         let idError = response["idError"] as! Int
            
            if idError == 0{
                //EXITO
                
                
                self.data = []
                
                let operadores = response["categorias"] as! NSArray
                
                for element in operadores {
                    
                    let dictio = element as! NSDictionary
                
                let id = dictio["id"] as! Int
                let codigo = dictio["codigo"] as! String
                let nombre = dictio["descripcion"] as! String
               
                    
                    let objectTemp = RecargaModel(id: id, codigo: codigo, nombre: nombre)
                    
                    self.data.append(objectTemp)
                
                }
                
                self.tableView.reloadData()
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel:"OK")
                
            }
    
            
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPaquetesDetalleIdentifier"{
            
            let indexPath = sender as! Int
            
            
            let destinoViewController = segue.destination as! RecargasPaquetesCostoSelectionCOViewController
            
            
            destinoViewController.idCategoria = self.data[indexPath].id
            destinoViewController.elementLast = self.elementLast
            
            
        }
        
        
    }
    
}
