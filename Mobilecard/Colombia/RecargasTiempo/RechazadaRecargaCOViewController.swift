

import UIKit
import Alamofire

class RechazadaRecargaCOViewController: UIViewController {

    // si viene de telepeaje se pone true
    var isFromTele = false
    //si viene de soat
    var isFromSoat = false
    
    //si viene de tarjetas prepago
    var isFromCard = false
    
    //si viene de pago de servicios
    var isFromServices = false
    
       @IBOutlet weak var textButton: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.isFromSoat == true{
            self.title = "Soat"
            self.textButton.text = "Pagar otra póliza"
        }else if self.isFromTele == true{
            self.title = "Recarga"
            self.textButton.text = "REALIZA OTRA RECARGA"
            
        }else if self.isFromCard == true{
            self.title = "Tarjeta prepago"
                   self.textButton.text = "REALIZA OTRA TRANSFERENCIA"
            
        }else if self.isFromServices == true{
            self.title = "Pago de servicios"
            self.textButton.text = "REALIZAR OTRO PAGO"
        }
        
    }

    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //metodo que se ejecuta al presionar el boton de home
    @IBAction func homeAction(_ sender: UIButton) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MainMenu_Ctrl.self) {
                
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    
    //metodo que se ejecuta al presionar otra recarga
    @IBAction func otherAction(_ sender: UIButton) {
        
        if self.isFromSoat == true{
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: SoatCOViewController.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            
            return;
        }
        
        
        if self.isFromServices == true{
                 
                 for controller in self.navigationController!.viewControllers as Array {
                     if controller.isKind(of: PagoDeServiciosCOViewController.self) {
                         
                         self.navigationController!.popToViewController(controller, animated: true)
                         break
                     }
                 }
                 
                 return;
             }
        
        if self.isFromTele == true{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: RecargasTeleCOViewController.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            return;
        }else if self.isFromCard == true{
            for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: TarjetasPrepagoCOViewController.self) {
                            
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    return;
            
            
        }else{
        
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RecargasMenuCOViewController.self) {
                
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
            return;
        
    }
        
    }
    
    
    
    
    
    
    
}
