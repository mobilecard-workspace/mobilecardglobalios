

import UIKit
import Alamofire

class PagoDeServiciosNormalSelectCOViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    //producto que recibimos de la clase pasada
    var producto : ServiciosProductoCO!
    
    @IBOutlet weak var collectionView: UICollectionView!
      
      let services = Services()
      
    
  

    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.title = "\(self.producto.nombre!)"
            
    
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
           //metodos del datasource de la collectionView
         func numberOfSections(in collectionView: UICollectionView) -> Int {
             return 1
         }
         
         func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             
             
            return self.producto.servicios.count
         }
       
       
         
         
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriaProductCellIdentifier", for: indexPath) as! CategoriaPrepagoCell
            
            print(self.producto.servicios[indexPath.row].nombre!)
           cell.nameLabel.text = self.producto.servicios[indexPath.row].nombre!
             
          
             
         return cell
         }
         
         
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
             return CGSize(width: UIScreen.main.bounds.width / 2.07, height: self.collectionView.bounds.height / 5.1)
         }
         
       
          //metodos del delegado de collectionView
         //metodo que se ejecuta al presionar una celda
         func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
             
         
         
            
            print("se selecciono \(self.producto.servicios[indexPath.row].nombre!)")
           
         
        
           
            var dictio :[String:Any] = [:]
            
            dictio["id"] = self.producto.servicios[indexPath.row].id!
            
            dictio["nombre"] = self.producto.servicios[indexPath.row].nombre!
            
             
            self.performSegue(withIdentifier: "scanSegue", sender: dictio)
             
             
         }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "scanSegue"{
            
            let destination = segue.destination as! PagoDeServiciosScanCOViewController
            
            let dictio = sender as! [String: Any]
            
            destination.productName = self.producto.nombre!
            destination.serviceName = dictio["nombre"] as? String
            destination.serviceId = dictio["id"] as? String
            
        }
        
        
    }
    
    

}
