

import UIKit
import Alamofire

class RecargasTeleCostoSelectionCOViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,ServicesDelegate,UICollectionViewDelegateFlowLayout{

    
   
    @IBOutlet weak var cuentaLabel: UILabel!
    @IBOutlet weak var saldoLabel: UILabel!
    @IBOutlet weak var placaLabel: UILabel!
    
    var saldo = ""
    var placa = ""
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var continueButton: UIButton!
   
    let services = Services()
    
    //objeto que recibimos de la clase pasada
    var tagElement : TagCOModel!
    
    
    var precio : Double! = 0.0
    
    
    
    //arreglo que controla la tableView
    var data : [PrecioModel] = []
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
      

        self.services.delegate = self
        
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
        
        
        let params : [String : Any] = [:]
        
        self.services.send(type: .GET, url: "ColombiaMiddleware/1/2/es/viarapida/api/detalleTag/\(self.tagElement.idTagBd!)/\(DataUser.USERIDD!)", params: params, header: headers, message: "Obteniendo",animate: true)
        
    
   
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
      
        
        
    }

   
    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  
    
    
    
   
    
      //metodos del datasource de la collectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return self.data.count
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let celdaID = "precioIdentifier"
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: celdaID, for: indexPath) as! PrecioCell
        
        
        if self.data[indexPath.row].isSelected == true{
            //cuando esta seleccionada
         
            cell.amountLabel.textColor = UIColor.white
            cell.buttonBackground.setBackgroundImage(UIImage(named: "btn_naranja-3x"), for: .normal)
        }else{
            //cuando no esta seleccionada
            
            cell.amountLabel.textColor = UIColor.lightGray
            cell.buttonBackground.setBackgroundImage(UIImage(named: "unselectedTiempo"), for: .normal)
            
        }
        
       
        cell.amountLabel.text = "$\(self.data[indexPath.row].amount!)"
        
       
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2.3, height: self.collectionView.bounds.height / 6.1)
    }
    
    
    
    
  
     //metodos del delegado de collectionView
    //metodo que se ejecuta al presionar una celda
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("selection index \(indexPath.row)")
        
        //seleccionamos la celda
        
        for element in self.data{
            
            element.isSelected = false
            
        }
        
        self.data[indexPath.row].isSelected = true
        self.precio = self.data[indexPath.row].amount
        self.continueButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
        self.collectionView.reloadData()
        
     
        
    }

    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
        print("RESPONSE \(response)")
        
        if endpoint == "ColombiaMiddleware/1/2/es/viarapida/api/detalleTag/\(self.tagElement.idTagBd!)/\(DataUser.USERIDD!)"{
            
            
            
         let idError = response["codigo"] as! Int
            
            if idError == 0{
                //EXITO
                
                let cuenta = response["cuenta"] as! String
                let placa = response["placa"] as! String
                let saldo = response["saldo"] as! Double
                
                self.cuentaLabel.text = cuenta
                self.placaLabel.text = placa
                self.saldoLabel.text = "$\(saldo) COP"
                
                self.saldo = String(saldo)
                self.placa = placa
                
                self.data = []
                
                let montos = response["montos"] as! NSArray
                
                for element in montos {
                    let cantidad = element as! Double
                 
                
              
                    
                    let objectTemp = PrecioModel(amount: cantidad)
                    
    
                    
                    
                    self.data.append(objectTemp)
                
                }
                
                self.collectionView.reloadData()
                
            }else{
                let mensajeError = response["mensaje"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel:"OK")
                
            }
    
            
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    

    
    
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        
    
        
        
        if self.precio == 0.0{
            self.alert(title: "¡Aviso!", message: "Debes seleccionar un monto para continuar.", cancel: "OK")
            return;
            
        }
        
       
        
       
    
        let story = UIStoryboard(name: "wallet", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "wallet_view") as! Wallet_Ctrl
        
        let dictio : NSMutableDictionary =  ["saldo":self.saldo,"placa":self.placa,"monto":self.precio!,"tagId":self.tagElement.idTagBd!]
        
      
         vc._COTelepe = "activado"
        
         DataUser.DATA = dictio
 
        
        self.navigationController?.pushViewController(vc, animated: true)
    
    
    }
    
    
  
    
    
}
