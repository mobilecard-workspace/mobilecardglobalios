

import UIKit
import Alamofire
import Firebase

class SoatCOViewController: UIViewController,UITextFieldDelegate,MyTextFieldDelegate,ServicesDelegate {

    @IBOutlet weak var verifyOne: MyTextField!
    @IBOutlet weak var verifyTwo: MyTextField!
    @IBOutlet weak var verifyThree: MyTextField!
    @IBOutlet weak var verifyFour: MyTextField!
    @IBOutlet weak var verifyFive: MyTextField!
    @IBOutlet weak var verifySix: MyTextField!
    
    @IBOutlet weak var continuarButton: UIButton!
    
    //variable que guarda toda la info de la placa
    var soatModel : SoatModel!
    //objeto para los servicios
    let services = Services()
    
    //variable que bloquea el acceso
    var access = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
     
        
        //delegamos los servicios
        self.services.delegate = self

        //targets
        self.verifyOne.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifyTwo.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifyThree.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifyFour.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifyFive.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.verifySix.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.verifyOne.myTextFieldDelegate = self
        self.verifyTwo.myTextFieldDelegate = self
        self.verifyThree.myTextFieldDelegate = self
        self.verifyFour.myTextFieldDelegate = self
        self.verifyFive.myTextFieldDelegate = self
        self.verifySix.myTextFieldDelegate = self
        
        
   
        
        self.checkAccess()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.verifyOne.text = ""
        self.verifyTwo.text = ""
        self.verifyThree.text = ""
        self.verifyFour.text = ""
        self.verifyFive.text = ""
        self.verifySix.text = ""
        self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        
    }
    
    //metodo que checa en firebase si tenemos acceso a soat
    func checkAccess(){
        
        var config:LoaderView.Config = LoaderView.Config()
               config.size = 100;
               config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
               config.spinnerColor    = UIColor.white;
               config.titleTextColor  = UIColor.white;
               LoaderView.setConfig(config: config);
                 LoaderView.show(title: "Obteniendo", animated: true)
        
        var ref: DatabaseReference!
                    ref = Database.database().reference()
        
        
        ref.child("bloqueos").observeSingleEvent(of: .value, with: {(snap) in
            
            let quiz = snap.value as! NSDictionary
            
            let enumerator = quiz.keyEnumerator()
            while let key = enumerator.nextObject() {
                
                let val = quiz[key as! String] as! Dictionary<String, Any>
             
                let seccion = key as! String
                
                if seccion == "soat"{
                    
                        let status = val["status"] as! Bool
                    
                    if status == true{
                        //lo mantiene bloqueado nos salimos de la seccion
                         LoaderView.hide()
                        
                        let alert = UIAlertController(title: "¡Aviso!", message: "Sección no diponible actualmente.", preferredStyle: .alert);
                        
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                            
                            self.navigationController?.popViewController(animated: true)
                            
                            
                            
                            
                        }));
                        
                        self.present(alert, animated: true, completion: nil);
                        
                        
                    }else{
                        //desbloqueado
                        LoaderView.hide()
                        self.access = 1
                    }
                    
                }
                
            
              
                    
              //  }
                
            }
       
            
        })
        {(error) in
            
            LoaderView.hide()
            
            let alert = UIAlertController(title: "¡Aviso!", message: "Sección no diponible actualmente.", preferredStyle: .alert);
                                   
                                   alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                                       
                                       self.navigationController?.popViewController(animated: true)
                                       
                                       
                                       
                                       
                                   }));
                                   
                                   self.present(alert, animated: true, completion: nil);
        }
        
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //metodo que checa en todo momento el contenido del textfield
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        switch textField.tag {
            
            
        case 1:
            
            if textField.text!.count >= 1 {
                self.verifyTwo.becomeFirstResponder()
            }
            
            break
            
        case 2:
            
            
            if textField.text!.count >= 1 {
                
                self.verifyThree.becomeFirstResponder()
            }
            
            break
            
        case 3:
            if textField.text!.count >= 1 {
                
                self.verifyFour.becomeFirstResponder()
                
            }
            
            break
            
        case 4:
            if textField.text!.count >= 1 {
                self.verifyFive.becomeFirstResponder()
                
                
            }
            
            break
            
        case 5:
            if textField.text!.count >= 1 {
                self.verifySix.becomeFirstResponder()
            }
            
            break
            
        case 6:
            if textField.text!.count >= 1 {
                
                print("termino la accion")
                textField.endEditing(true)
                
                if self.verifyOne.text == "" || self.verifyTwo.text == "" || self.verifyThree.text == "" || self.verifyFour.text == "" || self.verifyFive.text == "" || self.verifySix.text == ""{
                    self.alert(title: "¡Aviso!", message: "Debes poner el código completo para continuar.", cancel: "OK")
                    self.verifyOne.text = ""
                    self.verifyTwo.text = ""
                    self.verifyThree.text = ""
                    self.verifyFour.text = ""
                    self.verifyFive.text = ""
                    self.verifySix.text = ""
                    self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                    
                    return;
                }else{
                    
                    
                    //fue un exito
                
                    self.continuarButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
                    
                }
                
                
            }
            break
            
            
            
        default:
            break
            
            
            
            
        }
        
        
    }
    
    //metodo delegado cuando empieza a editar el textfield
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField.tag {
            
        case 1,2,3,4,5,6:
            textField.text = ""
              self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            break
            
        default:
            break
            
        }
    }
    
    
    
    //metodo que se ejecuta al presionar el boton de back en el keyboard
    func textFieldDidEnterBackspace(_ textField: MyTextField) {
        
        switch textField.tag {
            
            
        case 1:
              self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            break
            
        case 2:
            self.verifyOne.becomeFirstResponder()
              self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            break
            
        case 3:
            self.verifyTwo.becomeFirstResponder()
              self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            break
            
        case 4:
            self.verifyThree.becomeFirstResponder()
              self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            break
            
        case 5:
            self.verifyFour.becomeFirstResponder()
              self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            break
            
        case 6:
            self.verifyFive.becomeFirstResponder()
              self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            
            break
            
        default:
            break
        }
        
        
    }
    
    
    //metodo delegado del textfield para que no se puedan poner mas caracteres de los permitidos
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
            
            
        case 1:
            
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                return false
            }
            
            break
            
        case 2:
            
            
            
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 3:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 4:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 5:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                return false
            }
            break
            
        case 6:
            if ((textField.text?.count)! + (string.count - range.length)) > 1 {
                
                
                
                return false
            }
            break
            
        default:
            break
        }
        
        return true
    }
    
    
    //metodo que se ejecuta al presionar el botón de continuar
    @IBAction func continuarAction(_ sender: UIButton) {
        
        if self.verifyOne.text == "" || self.verifyTwo.text == "" || self.verifyThree.text == "" || self.verifyFour.text == "" || self.verifyFive.text == "" || self.verifySix.text == ""{
            self.alert(title: "¡Aviso!", message: "Debes agregar todos los datos de la placa para continuar.", cancel: "OK")
            
            return;
        }
        
        
        if self.access == 0{
             
              let alert = UIAlertController(title: "¡Aviso!", message: "Sección no diponible actualmente.", preferredStyle: .alert);
              
              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                  
                  self.navigationController?.popViewController(animated: true)
                  
                  
                  
                  
              }));
              
              self.present(alert, animated: true, completion: nil);
            return;
        }
        
        print("todo listo, continuamos con el llamado del servicio")
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
      
        
        let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,
                                       "idPais":"2",
                                       "idApp":"1",
                                       "idioma":"es",
                                       "placa":"\(self.verifyOne.text!.uppercased())\(self.verifyTwo.text!.uppercased())\(self.verifyThree.text!.uppercased())\(self.verifyFour.text!.uppercased())\(self.verifyFive.text!.uppercased())\(self.verifySix.text!.uppercased())"]
        
        print("PARAMS: \(params)")
        
        self.services.send(type: .POST, url: "ColombiaMiddleware/api/soat/calculaPoliza", params: params, header: headers, message: "Obteniendo",animate: true)

        
    }
    
    
    //metodos delegados de los servicios
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        if endpoint == "ColombiaMiddleware/api/soat/calculaPoliza"{
            
            let codigo = response["codigo"] as! Int
            
            if codigo == 0{
                //exito
                
                
                let nombre = response["nombre"] as! String
                let cc = response["cc"] as! String
                let telefono = response["telefono"] as! String
                let email = response["email"] as! String
                let marca = response["marca"] as! String
                let linea = response["linea"] as! String
                let modelo = response["modelo"] as! String
                let placa = response["placa"] as! String
                let categoria = response["categoria"] as! String
                let total = response["total"] as! Double
                let vigenciaIni = response["vigenciaIni"] as! String
                let vigenciaFin = response["vigenciaFin"] as! String
                let seguroSoat = response["seguroSoat"] as! Double
                
                self.soatModel = SoatModel(nombre: nombre, cc: cc, telefono: telefono, email: email, marca: marca, linea: linea, modelo: modelo, placa: placa, categoria: categoria, total: total, vigenciaIni: vigenciaIni, vigenciaFin: vigenciaFin, seguroSoat: seguroSoat)
                
                self.performSegue(withIdentifier: "continueSegue", sender: nil)
                
                
            }else{
                //fracaso
                if response["mensaje"] is NSNull{
                    
                      self.alert(title: "¡Aviso!", message: "No fue posible obtener la información de la placa , intente mas tarde.", cancel: "OK")
                }else{
                
                let mensaje = response["mensaje"] as! String
                
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                }
                
            }
            
            
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    
    }
    
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor , intente nuevamente.", cancel: "OK")
    }

    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "continueSegue"{
            
       
            let destinoViewController = segue.destination as! SoatDetailCOViewController
            
            destinoViewController.soatModel = self.soatModel
            
            
            
        }
        
        
    }
    
    
}
