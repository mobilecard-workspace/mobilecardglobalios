

import UIKit
import Alamofire

class RecargasPaquetesSelectionCOViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ServicesDelegate{

    @IBOutlet weak var collectionView: UICollectionView!
    
    let services = Services()
    
    //arreglo que controla la colectionView
    
    var data : [RecargaModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.services.delegate = self
        
        
        let headers  : HTTPHeaders = [:]
        
        
        let params : [String : Any] = [:]
        
        self.services.send(type: .GET, url: "Catalogos/1/2/es/puntored/getOperadoresPaquetes", params: params, header: headers, message: "Obteniendo",animate: true)
        
    
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  
    
    
    
   
    
      //metodos del datasource de la collectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return self.data.count
    }
    
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriaCellIdentifier", for: indexPath) as! CategoriaCell
    
        
        if let imageURL = URL(string: self.data[indexPath.row].imgwild){
            cell.imagenNegocio.af_setImage(withURL: imageURL)
        }
        
    return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2.3, height: self.collectionView.bounds.height / 5.1)
    }
    
  
     //metodos del delegado de collectionView
    //metodo que se ejecuta al presionar una celda
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("selected index \(indexPath.row)")
        
        self.performSegue(withIdentifier: "showCategories", sender: indexPath.row)
        
        
        
    }

    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        if endpoint == "Catalogos/1/2/es/puntored/getOperadoresPaquetes"{
            
            
         let idError = response["idError"] as! Int
            
            if idError == 0{
                //EXITO
                
                
                self.data = []
                
                let operadores = response["operadores"] as! NSArray
                
                for element in operadores {
                    
                    let dictio = element as! NSDictionary
                
                let id = dictio["id"] as! Int
                let codigo = dictio["codigo"] as! String
                let nombre = dictio["nombre"] as! String
                let imgcolor = dictio["imgcolor"] as! String
                let imgwild = dictio["imgwild"] as! String
                    
                    let objectTemp = RecargaModel(id: id, codigo: codigo, nombre: nombre, imgcolor: imgcolor, imgwild: imgwild)
                    
                    self.data.append(objectTemp)
                
                }
                
                self.collectionView.reloadData()
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel:"OK")
                
            }
    
            
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    //para preparar el cambio de vista
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showCategories"{
            
            let indexPath = sender as! Int
            
            
            let destinoViewController = segue.destination as! RecargasPaquetesCategoriaSelectionCOViewController
            
            destinoViewController.elementLast = self.data[indexPath]
            
            
        }
        
    }
    
}
