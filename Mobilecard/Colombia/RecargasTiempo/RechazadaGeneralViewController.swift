

import UIKit
import Alamofire

class RechazadaGeneralViewController: UIViewController {

    
       @IBOutlet weak var textButton: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

     self.title = "Rechazada"
        
    }

    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //metodo que se ejecuta al presionar el boton de home
    @IBAction func homeAction(_ sender: UIButton) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MainMenu_Ctrl.self) {
                
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    
    
    
    
    
    
    
}
