


import UIKit
import AVFoundation
import Alamofire




class MXUserScanAndPayKeyCommerceViewController:UIViewController,ScannerQrDelegate,ServicesDelegate{

    
    

    
    //referencias de elementos graficos
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fiveButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var eightButton: UIButton!
    @IBOutlet weak var nineButton: UIButton!
    @IBOutlet weak var ceroButton: UIButton!
    @IBOutlet weak var eraseButton: UIButton!
    
    @IBOutlet weak var conceptButton: UIButton!
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    //esta variable si es 0 viene de tarjeta presente /1 de generar qr /2 escanear qr
    @objc var fromView = 0
    
    var amountGlobal = "0.0"
    
    //objeto que contiene toda la informacion  de escanear qr
    var scanQr : ScanQrModel!
    var qrGlobal = ""
    
    //objeto que contiene toda la informacion  de tarjeta presente
    var cardPressent : CardPressent!
    
    
    //objeto que gestiona los servicios
    let service = Services()
    
    // formato para los botones
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24.0),
        NSAttributedString.Key.foregroundColor : UIColor.black,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    var attributedString = NSMutableAttributedString(string:"")
    
    var valueString = "$"
    
    //diccionario que recibimos de la clase pasada
    @objc var dictioQR : NSDictionary!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        service.delegate = self
       
       
        //iniciamos el objeto que contiene la información
        self.cardPressent = CardPressent()
        
        self.scanQr = ScanQrModel()
        
        
        
        
        
        //agregamos los underline de los botones
        self.attrs[NSAttributedString.Key.font] = self.oneButton.titleLabel?.font
        self.underlineButton(button: oneButton)
        self.underlineButton(button: twoButton)
        self.underlineButton(button: threeButton)
        self.underlineButton(button: fourButton)
        self.underlineButton(button: fiveButton)
        self.underlineButton(button: sixButton)
        self.underlineButton(button: sevenButton)
        self.underlineButton(button: eightButton)
        self.underlineButton(button: nineButton)
        self.underlineButton(button: ceroButton)
        
        
       
        
        

        self.title = "Escanea y paga"
     
        
      
        
        
       

        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print("PROPINA : \(self.cardPressent.propina!)")
    }
    //metodo que se ejecuta al presionar el botón de regresar de escaneo qr
    @objc func buttonAction(){
        print("se presiono el botón")
        
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        //tarjeta presente
        if self.fromView == 0{
     
            if self.cardPressent.concepto != nil{
                
                self.conceptButton.setBackgroundImage(#imageLiteral(resourceName: "conceptoNaranja-3x"), for: .normal)
                
            }else{
                self.conceptButton.setBackgroundImage(#imageLiteral(resourceName: "concepto-3x"), for: .normal)
                
                
            }
        
        //cuando viene a realizar otra transacción nueva
        if self.cardPressent.newTransaction == 1{
            self.valueString = "$"
            self.totalAmountLabel.text = "$"
            self.cardPressent.newTransaction = 0
        }
            
        }
        
        
    }
    

    //metodo que le pone una underline a un botón
    func underlineButton(button:UIButton){
        
      self.attributedString =  NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:button.titleLabel!.text!, attributes:attrs)
        attributedString.append(buttonTitleStr)
       
        button.setAttributedTitle(attributedString, for: .normal)
        
    }
    
  
//metodo que se ejecuta al presionar el boton de back
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
      _ = self.navigationController?.popViewController(animated: true)
          
    }
    
    
    
   
   
    //metodo que se ejecuta al presionar algun numero
    @IBAction func numberAction(_ sender: UIButton) {
        
        
        //opcion de borrado
        if sender.tag == 10{
            
            if self.totalAmountLabel.text != "$"{
                var characters = Array(self.totalAmountLabel.text!)
                 characters.remove(at: characters.count-1)
                if characters.count > 1{
                   // self.totalAmountLabel.text = "S/"
                   // self.valueString = "S/"
                  //  return;
                //}else{
                var stringNew = ""
                var stringNoPunto = ""
                for char in characters{
                    if char != "."{
                        stringNoPunto .append(char)
                    }
                    stringNew.append(char)
                }
              //  self.totalAmountLabel.text = stringNew
                    var amount = ""
                    let arrayCharacters = Array(stringNew)
                    
                    for position in 1...arrayCharacters.count-1{
                        amount.append(arrayCharacters[position])
                        
                    }
                    
                    var amountTwo = ""
                    let arrayCharactersTwo = Array(stringNoPunto)
                    
                    for position in 1...arrayCharactersTwo.count-1{
                        amountTwo.append(arrayCharactersTwo[position])
                        
                    }
                    
                    print("esto vale amounttwo \(amountTwo)")
                    if amountTwo == "00"{
                        self.totalAmountLabel.text = "$"
                        self.valueString = "$"
                        return;
                        
                    }
                    
                    let doubleStr = String(format: "%.2f", Double(amountTwo)! / 100)
                    self.amountGlobal = doubleStr
                    self.totalAmountLabel.text = "$\(self.amountGlobal)"
                    
                    var array = Array(self.valueString)
                    array.remove(at: array.count-1)
                    var stringComplete = ""
                    for a in array {
                        stringComplete.append(a)
                    }
                    self.valueString = stringComplete
                    //  self.valueString = self.totalAmountLabel.text!
                    ///// raul mendez guarde el valor de amount en un userdefault
              //      UserDefaults .standard.set (totalAmountLabel.text, forKey: "Amount")
                  
                return;
                    
                }
                
            }else{
                  self.valueString = "$"
                self.alert(title: "¡Aviso!", message: "No hay valores que borrar actualmente.", cancel: "OK")
                return;
                
            }
            
            
            
        }
        //termina codigo de opcion de borrado
        
        
        
        //inicia codigo para numeros del 0 al 9
        if self.totalAmountLabel.text == "$"{
            
            if sender.tag == 0{
                
                self.alert(title: "¡Aviso!", message: "Agrega un valor valido.", cancel: "OK")
               return;
            }else{
                
                
                self.valueString.append(String(sender.tag))
               
                
                var amount = ""
                let arrayCharacters = Array(self.valueString)
                
                for position in 1...arrayCharacters.count-1{
                    amount.append(arrayCharacters[position])
                    
                }
                
                let doubleStr = String(format: "%.2f", Double(amount)! / 100)
                self.amountGlobal = doubleStr
                self.totalAmountLabel.text = "$\(self.amountGlobal)"
                
                
                ///// raul mendez guarde el valor de amount en un userdefault
               // UserDefaults .standard.set (totalAmountLabel.text, forKey: "Amount")
            }
            
            
            
            
        }else{
            
            self.valueString.append(String(sender.tag))
            var amount = ""
            let arrayCharacters = Array(self.valueString)
            
            for position in 1...arrayCharacters.count-1{
                amount.append(arrayCharacters[position])
                
            }
            
            
            let doubleStr = String(format: "%.2f", Double(amount)! / 100)
            self.amountGlobal = doubleStr
            
            self.totalAmountLabel.text = "$\(self.amountGlobal)"
           // self.totalAmountLabel.text = valueString
            
            ///// raul mendez guarde el valor de amount en un userdefault
          //  UserDefaults .standard.set (totalAmountLabel.text, forKey: "Amount")
            
        }
        
         //termina codigo para numeros del 0 al 9
        
        
    }
    
    
    //metodo que se ejecuta al presionar el botón de recargar
    @IBAction func reloadAction(_ sender: UIButton) {
        
        
        //requerimientos para avanzar en la vista
        if self.totalAmountLabel.text == "$" || self.totalAmountLabel.text == "$0.00"{
            self.alert(title: "¡Aviso!", message: "Debes agregar un monto para continuar.", cancel: "OK")
            return;
        }
        
        if self.cardPressent.concepto == nil{
            self.alert(title: "¡Aviso!", message: "Debes agregar un concepto para continuar.", cancel: "OK")
            return;
        }
        
        
        
        
        //obtenemos la cantidad a recargar en String
        var amount = ""
        let arrayCharacters = Array(self.totalAmountLabel.text!)
        
        for position in 1...arrayCharacters.count-1{
            amount.append(arrayCharacters[position])
            
        }
        
        
        
        if self.fromView == 0{
        
        self.cardPressent.cantidad = amount
            
            
           // let comision = self.calculaComision(monto: Double(amount)!)
            
           // let doubleTwoDecimals = Double(String(format: "%.2f", comision))
            
           // self.cardPressent.comision = String(doubleTwoDecimals!)
            //obtenemos la comision
            
             let mutable : NSMutableDictionary = NSMutableDictionary(dictionary: self.dictioQR)
            
            
            
            let headers  : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded","Accept":"*/*"]
            let params : [String : Any] = ["idCommerce":String(mutable["establecimientoId"] as! Int)]
            print("PARAMS: \(params)")
                  
                  self.service.send(type: .POSTURLENCODED, url: "CuantoTeDebo/1/1/es/getComision", params: params, header: headers, message: "Obteniendo",animate: true)
            
            
            
        }
    }

    
    
    func calculaComision(monto: Double,comisionPorcentaje:Double,comisionFija:Double)-> Double {
        
        
        let comision = (monto * comisionPorcentaje)+comisionFija
        return   (comision)
    }
    
    func calculaComisionFija(monto: Double)-> Double {
        let comision = (monto * 0.00)
        let impuesto = (comision * 0.18)
        return (comision + impuesto)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //acción antes del segue
        if segue.identifier == "cardPressentMxSegue"{
            
            let destinoViewController = segue.destination as! commerce_CardPayment
            
            destinoViewController._importe = self.cardPressent.cantidad! as NSString
            
            
            
        }
        
  
        if segue.identifier == "step30"{
            
            let destinoViewController = segue.destination as! scanNPay_Step30
            
            destinoViewController.data = (sender as! NSDictionary as! [AnyHashable : Any])
            destinoViewController.showCardOptionsFirst = true
            destinoViewController._isBackDissmis = "1"
            
            
            
        }
        
        
        
        if segue.identifier == "conceptSegue"{
            
        
            
            let navController = segue.destination as! UINavigationController
            
            let destinoViewController  = navController.children.first as! ConcepScanAndPayMXEUAViewController
           
            destinoViewController.cardPressent = self.cardPressent
            
            
            
        }
       
        
        
    }
 
   
    

    
    
    
    @IBAction func conceptAction(_ sender: UIButton) {
        
        if  self.fromView == 0 || self.fromView == 2{
        self.performSegue(withIdentifier: "conceptSegue", sender: nil)
       
        }else if self.fromView == 1{
            
            if self.totalAmountLabel.text == "$" || self.totalAmountLabel.text == "$0.00"{
                self.alert(title: "¡Aviso!", message: "Debes agregar un monto para continuar.", cancel: "OK")
                return;
            }
            
        //UserDefaults.standard.set(totalAmountLabel.text, forKey: "Amount")

        //AQUI CAMBIA DE VISTA RAUL
        self.performSegue(withIdentifier: "conceptSegueObjective", sender: nil)
        
        //AQUI AGREGA EN LA PRIMERA LINEA EL NOMBRE DEL STORYBOARD
        //LUEGO EL NOMBRE DEL IDENTIFICADOR DE la vista a la que vas (esta en la interfas grafica) y en donde dice as! pon el nombre de la clase a la que vas
        
//        let storyboard = UIStoryboard(name: "PEregister_Commerce", bundle: nil)
//        let viewcontroller = storyboard.instantiateViewController(withIdentifier: "CustomAlertAceptarID") as! CustomAlertViewMessage
//
//
//            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
        
    }
    
    //metodos delegado ScanReferenceDelegate
    
    func successQr(qr: String) {
        
        print("este es el qr \(qr)")
        self.qrGlobal = qr
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*"]
        let params : [String : Any] = ["qrHexa":qr]
        
        self.service.send(type: .POST, url: ServicesLinks.VisaQRWalletDecode, params: params, header: headers, message: "Enviando",animate: true)
        
        
        
        
        
        
    }
    
    func backActionQr() {
        print("se presiono el back")
         _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //metodos delegado servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        if endpoint == "CuantoTeDebo/1/1/es/getComision"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let comisionPorcentaje :Double = response["comision_porcentaje"] as! Double
                let comisionFija :Double = response["comision_fija"] as! Double
                
                let comision = self.calculaComision(monto: Double(self.cardPressent.cantidad!)!,comisionPorcentaje: comisionPorcentaje,comisionFija:comisionFija)
                        
                        let doubleTwoDecimals = Double(String(format: "%.2f", comision))
                        
                        self.cardPressent.comision = String(doubleTwoDecimals!)
                        //obtenemos la comision
                        

                             print("esta es la comision \(self.cardPressent.comision!)")
                       print("esta es la cantidad \(self.cardPressent.cantidad!)")
                       
                       //aqui
                   //llamamos a la vista

                       
                       let mutable : NSMutableDictionary = NSMutableDictionary(dictionary: self.dictioQR)
                       
                       mutable["amount"] = self.cardPressent.cantidad!
                        mutable["amount_mxn"] = self.cardPressent.cantidad!
                       mutable["comision"] = self.cardPressent.comision
                       mutable["concept"] = self.cardPressent.concepto!
                       mutable["propina"] = self.cardPressent.propina!
                     
                       let dictioSend = NSDictionary(dictionary: mutable)
                       
                
                       
                       self.performSegue(withIdentifier: "step30", sender: dictioSend)
                
                
            }else{
                
                let mensaje = response["mensajeError"] as! String
                
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
            }
            
        }
       
        
        if endpoint == ServicesLinks.VisaQRWalletDecode{
            
        let code = response["code"] as! Int
            
            if code == 0{
                
                let bussinesName = response["businessName"] as! String
                let city = response["city"] as! String
                let currencyCode = response["currencyCode"] as! String
                let amount = String(response["amount"] as! Double)
                
                
                
                self.scanQr.setValues(qr: self.qrGlobal, bussinesName: bussinesName, city: city, currencyCode: currencyCode)
                print("esto vale amount \(amount)")
                if amount != "0.0"{
                 self.scanQr.amount = amount
                    self.scanQr.concept = "Pago"
                    self.scanQr.comision = "0"
                    self.scanQr.propine = "0"
                    let dictio : NSDictionary = NSDictionary(dictionary: ["amount":self.scanQr.amount!,"businessName":self.scanQr.businessName!,"city":self.scanQr.city!,"concept":self.scanQr.concept!,"currencyCode":self.scanQr.currencyCode!,"comision":self.scanQr.comision!,"propina":self.scanQr.propine!,"qrBase64":self.scanQr.qr!])
                    
                    DataUser.DATA = dictio
                    //UserDefaults.standard.set(amount, forKey: "Amount")
                    
                    let story = UIStoryboard(name: "wallet", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "wallet_view")
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
             
                
            }else{
                
                let alert = UIAlertController(title: "¡Aviso!", message: "Código qr no valido.", preferredStyle: .alert);
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                    
                 
                    
                    
                    _ = self.navigationController?.popViewController(animated: true)
            
                }));
                
                present(alert, animated: true, completion: nil);
                
                
            }
            
        }
        
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }

    func errorService(endpoint: String) {
        
        
        if endpoint == "CuantoTeDebo/1/1/es/getComision"{
            _ = self.navigationController?.popViewController(animated: true)
                    
            
        }
        
        if  endpoint == ServicesLinks.VisaQRWalletDecode{
            
            _ = self.navigationController?.popViewController(animated: true)
            
        }
        
    }
    
    
    
   
}








