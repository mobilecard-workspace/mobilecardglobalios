

import UIKit
import Alamofire

class ConfirmarTeleCOViewController: UIViewController,ServicesDelegate {

    @IBOutlet weak var montoLabel: UILabel!
    @IBOutlet weak var saldoLabel: UILabel!
    @IBOutlet weak var placaLabel: UILabel!
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var comisionLabel: UILabel!
    @IBOutlet weak var imagenTarjetaImageView: UIImageView!
    @IBOutlet weak var numeroTarjetaLabel: UILabel!
    
    //variables que recibimos y las hacemos globales
    var idTarjeta = 0
    var monto = 0.0
    var saldo = ""
    var placa = ""
    var tagId = ""
    
    let services = Services()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let  cardToUpdate :NSArray = UserDefaults.standard.object(forKey: "tarjetas") as! NSArray

        let tarjeta : NSDictionary = cardToUpdate[Int(DataUser.IDTARJETACO)!] as! NSDictionary
        let img_short = tarjeta["img_short"] as! String
        
        
        if let imageURL = URL(string: img_short){
            self.imagenTarjetaImageView.af_setImage(withURL: imageURL)
        }
        
        
        print("Este es el id de tarjeta \(tarjeta)")
        print("estos son los demas datos \(DataUser.DATA!)")
       
      
        
       
            
     
            self.monto = DataUser.DATA!["monto"] as! Double
            self.placa = DataUser.DATA!["placa"] as! String
            self.saldo = DataUser.DATA!["saldo"] as! String
        
       
      
        self.placaLabel.text = self.placa
      
        
        
        self.montoLabel.text = "$\(DataUser.DATA!["monto"] as! Double) COP"
        
        self.saldoLabel.text = "\(DataUser.DATA!["saldo"] as! String) COP"
        
         self.totalLabel.text = "$\(DataUser.DATA!["monto"] as! Double) COP"
        self.comisionLabel.text = "$0.0 COP"
        
        self.tagId = DataUser.DATA!["tagId"] as! String
            self.idTarjeta = tarjeta["idTarjeta"] as! Int
        
        let pan = tarjeta["pan"] as! String
        
        let cardManager : pootEngine = pootEngine()
        
        let panDecoded : String = cardManager.decryptedString(of: pan, withSensitive: false) as! String
        
        
        let lastFour = String(panDecoded.suffix(4))
        let firstFour = String(panDecoded.prefix(4))
        
        let stringCard = "\(firstFour) **** **** \(lastFour)"
        
        self.numeroTarjetaLabel.text = stringCard
        
        
        //delegado de los servicios esta clase
        self.services.delegate = self
    }
    

  //metodo que se ejecuta al presionar continuar
    @IBAction func continuarAction(_ sender: UIButton) {
        
       
          
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
        
            let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,
                                           "idPais":"2",
                                           "idApp":"1",
                                           "tagId":self.tagId,
                                           "placa":self.idTarjeta,
                                           "monto":String(self.monto),
                                           "imei":DataUser.IMEI!,]
           
           print("PARAMS: \(params)")
           
            self.services.send(type: .POST, url: "ColombiaMiddleware/viarapida/api/recarga", params: params, header: headers, message: "Generando",animate: true)
            
        
      
            
       
        
        
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //delegado de los servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        if endpoint == "ColombiaMiddleware/viarapida/api/recarga"{
            
            let codigo = response["codigo"] as! Int
            
            if codigo == 0{
                //exito
                self.performSegue(withIdentifier: "successSegue", sender: nil)
            }else{
                //fracaso
                  self.performSegue(withIdentifier: "failSegue", sender: nil)
            }
            
        }
        
        
        if endpoint == "ColombiaMiddleware/puntored/api/paquete"{
            
            let codigo = response["codigo"] as! Int
            
            if codigo == 0{
                //exito
                self.performSegue(withIdentifier: "successSegue", sender: nil)
            }else{
                //fracaso
                self.performSegue(withIdentifier: "failSegue", sender: nil)
            }
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor. Intente nuevamente.", cancel: "OK")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "successSegue"{
            
            
             let destinoViewController = segue.destination as! AprobadaRecargaCOViewController
            
            destinoViewController.isFromTele = true
        }
        
        if segue.identifier == "failSegue"{
            
            
            let destinoViewController = segue.destination as! RechazadaRecargaCOViewController
            
            destinoViewController.isFromTele = true
        }
        
    }
    
}
