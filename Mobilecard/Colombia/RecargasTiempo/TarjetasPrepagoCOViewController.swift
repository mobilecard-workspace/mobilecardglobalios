

import UIKit
import Alamofire

class TarjetasPrepagoCOViewController: UIViewController ,ServicesDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    @IBOutlet weak var collectionView: UICollectionView!
     
     let services = Services()
     var data : [PrepagoProductoCO] = []

    override func viewDidLoad() {
        super.viewDidLoad()
      
         self.services.delegate = self
        
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
              
              
              let params : [String : Any] = [:]
              
              self.services.send(type: .GET, url: "ColombiaMiddleware/1/2/es/multimarket/api/catalogoProductos", params: params, header: headers, message: "Obteniendo",animate: true)

    
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    

      
        //metodos del datasource de la collectionView
      func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
      }
      
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          
          
          return self.data.count
      }
    
    
      
      
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriaProductCellIdentifier", for: indexPath) as! CategoriaPrepagoCell
      
        cell.nameLabel.text = self.data[indexPath.row].nombre!
          
       
          
      return cell
      }
      
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          return CGSize(width: UIScreen.main.bounds.width / 2.07, height: self.collectionView.bounds.height / 5.1)
      }
      
    
       //metodos del delegado de collectionView
      //metodo que se ejecuta al presionar una celda
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
          print("selected index \(indexPath.row)")
          
        
        var sendMounts = true
        for element in  self.data[indexPath.row].montos{
            
            if element.valor == "N/A"{
                sendMounts = false
            }
            
        }
        
        
        if sendMounts == true{
            //mandamos a la vista de los montos
            print("mandamos a los montos")
            self.performSegue(withIdentifier: "montosIdentifier", sender: indexPath.row)
            
            
        }else{
            //mandamos al teclado
            print("mandamos al teclado")
            
               self.performSegue(withIdentifier: "ingresaMontoIdentifier", sender: indexPath.row)
            
        }
        
        
        //self.performSegue(withIdentifier: "showCategories", sender: indexPath.row)
          
          
          
      }

    
    
    //metodos delegados de los servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        

        if endpoint == "ColombiaMiddleware/1/2/es/multimarket/api/catalogoProductos"{
                  
                  
               let idError = response["codigo"] as! Int
                  
                  if idError == 0{
                      //EXITO
                    
                    self.data = []
                    
                let productos = response["productos"] as! NSArray
                    
                    //primer ciclo
                    for element in productos {
                        let dictio = element as! NSDictionary
                        let nombre = dictio["nombre"] as! String
                        let valorArray = dictio["valor"] as! NSArray
                        
                        var tempArrayMounts : [PrepagoMontosCO] = []
                        //segundo ciclo
                        for elementMount in valorArray{
                            let dictioMount = elementMount as! NSDictionary
                            let productId : String = String(dictioMount["productId"] as! Int)
                            let valor = dictioMount["valor"] as! String
                           
                            tempArrayMounts.append(PrepagoMontosCO(id: productId, valor: valor))
                            
                        }
                        
                        let elementTemp = PrepagoProductoCO(nombre: nombre, montos: tempArrayMounts)
                        
                        self.data.append(elementTemp)
                        
                        
                        
                        
                    }
                    
                    self.collectionView.reloadData()
                    
                  }else{
                    let mensaje = response["mensaje"] as! String
                    self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
            }
            
        }
                    
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "montosIdentifier"{
            
            
            let index = sender as! Int
                   // nos llevamos el numero a la siguiente clase
                   let destinoViewController = segue.destination as! TarjetasPrepagoMontosCOViewController
            destinoViewController.producto = self.data[index]
            
            
        }
        
        if segue.identifier == "ingresaMontoIdentifier"{
              
              
              let index = sender as! Int
                     // nos llevamos el numero a la siguiente clase
                     let destinoViewController = segue.destination as! PrecioCOViewController
              destinoViewController.producto = self.data[index]
              
              
          }
        
        
        
        
    }

}
