

import UIKit
import Alamofire

class HistorialTeleCostoSelectionCOViewController: UIViewController ,ServicesDelegate,UITableViewDelegate,UITableViewDataSource{

    
    //arreglo que controla la tableView
    var data : [HistorialTeleModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
   
   
    let services = Services()
    
 
  
    

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
       self.tableView.separatorStyle = .none
      

        self.services.delegate = self
        
        
        let headers  : HTTPHeaders = [:]
        
        
     //   let params : [String : Any] = [:]
        
    //    self.services.send(type: .GET, url: "Catalogos/1/2/es/puntored/getMontos", params: params, header: headers, message: "Obteniendo",animate: true)
        
        
        
        
        //objetos temporales mientras no hay servicio de historial
        
        let object1 = HistorialTeleModel(type: "0", amount: "", day: "", hour: "", city: "", location: "", monthYear: "OCTUBRE 2019")
        let object2 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object3 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object4 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object5 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object6 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object7 = HistorialTeleModel(type: "0", amount: "", day: "", hour: "", city: "", location: "", monthYear: "SEPTIEMBRE 2019")
        let object8 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object9 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object10 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object11 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        let object12 = HistorialTeleModel(type: "1", amount: "-$80.00", day: "Sábado 15", hour: "20:48", city: "México - Acapulco", location: "Tepoztlan", monthYear: "")
        
        self.data.append(object1)
        self.data.append(object2)
        self.data.append(object3)
        self.data.append(object4)
        self.data.append(object5)
        self.data.append(object6)
        self.data.append(object7)
        self.data.append(object8)
        self.data.append(object9)
        self.data.append(object10)
        self.data.append(object11)
        self.data.append(object12)
        self.tableView.reloadData()
    
   
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
      
        
        
    }

   
    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  
    
    
    
   
   
    
    
    
    
  
   

    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        /*
        
        print("RESPONSE \(response)")
        
        if endpoint == "Catalogos/1/2/es/puntored/getMontos"{
            
            
            
         let idError = response["idError"] as! Int
            
            if idError == 0{
                //EXITO
                
                
                self.data = []
                
                let montos = response["montos"] as! NSArray
                
                for element in montos {
                    let cantidad = element as! Double
                 
                
              
                    
                    let objectTemp = PrecioModel(amount: cantidad)
                    
    
                    
                    
                    self.data.append(objectTemp)
                
                }
                
                self.collectionView.reloadData()
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel:"OK")
                
            }
    
            
        }
 
 */
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //metodos del datasource de la tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.data.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celdaIDMes = "historialTeleHeaderIdentifier"
          let celdaID = "historialTeleIdentifier"
        
        
        if self.data[indexPath.row].type == "1"{
            //es transaccion
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: celdaID,for: indexPath) as! HistorialTeleTableCell
            
            cell.amountLabel.text = self.data[indexPath.row].amount
            cell.cityLabel.text = self.data[indexPath.row].city
            cell.dayLabel.text = self.data[indexPath.row].day
            cell.hourLabel.text = self.data[indexPath.row].hour
            cell.locationLabel.text = self.data[indexPath.row].location
            
            cell.selectionStyle = .none
            
            return cell
            
        }else{
            
            // es header de mes año
            
             let cell = tableView.dequeueReusableCell(withIdentifier: celdaIDMes,for: indexPath) as! HistorialTeleTableCell
            
            cell.monthYearLabel.text = self.data[indexPath.row].monthYear
            return cell
        }
 
    }
    
    /*
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }*/
    
    
    
    
    
    
    //metodos del delegado de tableView
    //metodo que se ejecuta al presionar una celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selection index \(indexPath.row)")
        
   
        
        
    }


    
  
    
    
}
