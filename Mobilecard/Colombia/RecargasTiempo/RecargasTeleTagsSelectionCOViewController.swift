

import UIKit
import Alamofire


class RecargasTeleTagsSelectionCOViewController: UIViewController ,ServicesDelegate{

    @IBOutlet weak var selectTagButton: UIButton!
    @IBOutlet weak var numberManualTextField: UITextField!
    @IBOutlet weak var placaTextField: UITextField!
    @IBOutlet weak var aliasTextField: UITextField!
    @IBOutlet weak var constraintHeightPhoneManual: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightPlaca: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightAlias: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightSaveButton: NSLayoutConstraint!
    
    @IBOutlet weak var phoneSwitch: UISwitch!
    
    @IBOutlet weak var eraseTagButton: UIButton!
    
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var imageCommerce: UIImageView!
    let services = Services()
    
   
    var data : [TagCOModel] = []
    var elementSelected : TagCOModel!
    
    //label donde se pinta el tag
    @IBOutlet weak var numberContactLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
 
        
        self.services.delegate = self
      

        
    
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
      
        
        
        
    }

    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  
    
    
    
   
    


    
    

    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
        print("RESPONSE \(response)")
        
        if endpoint == "ColombiaMiddleware/viarapida/api/deleteTag"{
            
            
            let codigo = response["codigo"] as! Int
            
            if codigo == 0{
                
                self.eraseTagButton.isHidden = true
                self.eraseTagButton.isUserInteractionEnabled = false
                self.numberContactLabel.text = "Selecciona un TAG guardado"
                
                for element in self.data{
                    element.isSelected = false
                }
                
                self.alert(title: "¡Aviso!", message: "¡Tag borrado correctamente.!", cancel: "OK")
                
            }else{
                let mensaje = response["mensaje"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
            }
            
        }
        
        if endpoint == "ColombiaMiddleware/1/2/es/viarapida/api/listTags/\(DataUser.USERIDD!)"{
            
            let codigo = response["codigo"] as! Int
            
            
            if codigo == 0{
                //exito
                self.data = []
                self.eraseTagButton.isHidden = true
                self.eraseTagButton.isUserInteractionEnabled = false
                self.numberContactLabel.text = "Selecciona un TAG guardado"
                
               
                
            let array = response["tags"] as! NSArray
                
                for element in array{
                    
                    let dictio = element as! NSDictionary
                    
                    let alias = dictio["alias"] as! String
                    let cuenta = dictio["cuenta"] as! String
                    let idTagBd = dictio["idViaTag"] as! Int
                    let placa = dictio["placa"] as! String
                    let tagId = dictio["tagId"] as! String
                    let objectTemp = TagCOModel(isSelected: false, tagId: tagId, placa: placa, alias: alias, idTagBd: String(idTagBd), cuenta: cuenta)
                    
                    self.data.append(objectTemp)
                    
                }
                
                var nombres : [String] = []
                for element in self.data{
                    nombres.append("\(element.alias!) / \(element.placa!)")
                }
                
                //llamamos el picker
                ActionSheetStringPicker.show(withTitle: "Tags", rows: nombres, initialSelection: 0, doneBlock: { (picker, value, selection) in
                    
                    
                    let select = selection as! String
                    
                    
                    
                    self.numberContactLabel.text = select
                    
                    for element in self.data{
                        element.isSelected = false
                    }
                    
                    
                    self.data[value].isSelected = true
                    self.eraseTagButton.isHidden = false
                    self.eraseTagButton.isUserInteractionEnabled = true
                    
                    
                    
                }, cancel: { (action) in
                    return
                }, origin: self.selectTagButton)
                
                
                
                
            }else{
                //fracaso
                let mensaje = response["mensaje"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
            }
            
        }
        
        
        if endpoint == "ColombiaMiddleware/viarapida/api/saveTag"{
            
            let codigo = response["codigo"] as! Int
            
            if codigo == 0{
                //exito
                
                for element in self.data{
                    
                    element.isSelected = false
                }
                
                let dictio = response["tag"] as! NSDictionary
                let tagId = dictio["tagId"] as! String
                let placa = dictio["placa"] as! String
                let alias = dictio["alias"] as! String
                let idTagbd = dictio["idViaTag"] as! Int
                let cuenta = dictio["cuenta"] as! String
                
                let objectTemp = TagCOModel(isSelected: true, tagId: tagId, placa: placa, alias: alias, idTagBd: String(idTagbd),cuenta:cuenta)
                self.data.append(objectTemp)
                
                self.numberManualTextField.text = ""
                self.constraintHeightPhoneManual.constant = 0
                self.placaTextField.text = ""
                self.constraintHeightPlaca.constant = 0
                self.aliasTextField.text = ""
                self.constraintHeightAlias.constant = 0
                self.constraintHeightSaveButton.constant = 0
                
                self.updateSelectedTag()
                self.alert(title: "¡Aviso!", message: "Tag agregado exitosamente.", cancel: "OK")
                
                
                
            }else{
                //fracaso
                let mensaje = response["mensaje"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
            
        }
        
    
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //metodo que se ejecuta al presionar el botón de elige un número
    @IBAction func selectNumberAction(_ sender: UIButton) {
        
        self.phoneSwitch.setOn(false, animated: false)
       self.numberManualTextField.text = ""
        self.constraintHeightPhoneManual.constant = 0
        self.placaTextField.text = ""
        self.constraintHeightPlaca.constant = 0
         self.aliasTextField.text = ""
        self.constraintHeightAlias.constant = 0
        self.constraintHeightSaveButton.constant = 0
        
       //aqui llamamos al picker de tags existentes despues de llamar el servicio para obtenerlos
        //llamamos al servicio de agregar tag
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
        
        let params : [String : Any] = [:]
        
        print("PARAMS: \(params)")
        
        self.services.send(type: .GET, url: "ColombiaMiddleware/1/2/es/viarapida/api/listTags/\(DataUser.USERIDD!)", params: params, header: headers, message: "Obteniendo",animate: true)
        
     
    }
    
    
    //metodo que se ejecuta al presionar el switch
    @IBAction func switchAction(_ sender: UISwitch) {
        
        if sender.isOn == true{
            //activar
            
      
            self.constraintHeightPhoneManual.constant = 40
            self.constraintHeightPlaca.constant = 40
            self.constraintHeightAlias.constant = 40
            self.constraintHeightSaveButton.constant = 40
            
            
            
        }else{
            //desactivar
            self.constraintHeightPhoneManual.constant = 0
            self.constraintHeightPlaca.constant = 0
            self.constraintHeightAlias.constant = 0
            self.constraintHeightSaveButton.constant = 0
            self.numberManualTextField.text = ""
            self.placaTextField.text = ""
            self.aliasTextField.text = ""
        }
        
    }
    
    
    
 
    
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        
      print("se presiono el boton de salvar")
        
        if self.numberManualTextField.text!.count < 15 || self.numberManualTextField.text!.count > 45{
            self.alert(title: "¡Aviso!", message: "Debes agregar un tag valido para continuar.", cancel: "OK")
            return;
        }
        
        if self.placaTextField.text!.count < 5 || self.placaTextField.text! .count > 10{
            
            self.alert(title: "¡Aviso!", message: "Debes agregar una placa valida para continuar.", cancel: "OK")
            return;
        }
       
        if self.aliasTextField.text!.count < 1{
              self.alert(title: "¡Aviso!", message: "Debes agregar un alias para continuar.", cancel: "OK")
            return;
        }
    
       
        //llamamos al servicio de agregar tag
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
        
        let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,
                                       "idPais":"2",
                                       "idApp":"1",
                                       "idioma":"es",
                                       "tagId":self.numberManualTextField.text!,
                                       "placa":self.placaTextField.text!,
                                       "alias":self.aliasTextField.text!]
        
        print("PARAMS: \(params)")
        
        self.services.send(type: .POST, url: "ColombiaMiddleware/viarapida/api/saveTag", params: params, header: headers, message: "Guardando",animate: true)
    
    }
    
    //metodo que pone el tag seleccionado en pantalla
    func updateSelectedTag(){
        
        for element in self.data{
            
            if element.isSelected == true{
            self.numberContactLabel.text = "\(element.alias!) / \(element.placa!)"
                self.eraseTagButton.isHidden = false
                self.eraseTagButton.isUserInteractionEnabled = true
            }
            
        }
        
        
    }
    
    
    //metodo que se ejecuta cuando se borra un tag
    @IBAction func eraseTagAction(_ sender: UIButton) {
        
        var eraseElement : TagCOModel!
        
        
        for element in self.data{
            if element.isSelected == true{
                eraseElement = element
            }
        }
        
        
        let alert = UIAlertController(title: NSLocalizedString("¡Aviso!", comment:""), message: NSLocalizedString("¿Deseas borrar el tag \(eraseElement.alias!)?", comment:""), preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancelar", comment:""), style: .cancel, handler: nil));
        alert.addAction(UIAlertAction(title: NSLocalizedString("Si", comment:""), style: .default, handler: { (action:UIAlertAction) in
        
            
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
            
            let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,
                                           "idPais":"2",
                                           "idApp":"1",
                                           "idioma":"es",
                                           "tagId":eraseElement.tagId!,
                                           "placa":eraseElement.placa!,
                                           "alias":eraseElement.alias!,
                                           "idTagBd":eraseElement.idTagBd!]
            
            print("PARAMS: \(params)")
            
            self.services.send(type: .DELETE, url: "ColombiaMiddleware/viarapida/api/deleteTag", params: params, header: headers, message: "Eliminando",animate: true)
            
            
            
            
        }));
        
        present(alert, animated: true, completion: nil);
        
        
        
        
        
    }
    
    //metodo que se ejecuta al presionar el boton de recarga
    @IBAction func recargaAction(_ sender: UIButton) {
        
        
        
        for element in self.data{
            
            if element.isSelected == true{
                
                self.elementSelected = element;
                self.performSegue(withIdentifier: "recargaSegue", sender: nil)
                return;
            }
            
        }
        
        self.alert(title: "¡Aviso!", message: "Debes seleccionar un tag para poder recargar.", cancel: "OK")
        
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "recargaSegue"{
            
               let destinoViewController = segue.destination as! RecargasTeleCostoSelectionCOViewController
            
            destinoViewController.tagElement = self.elementSelected
            
            
        }
        
        
    }
    
}
