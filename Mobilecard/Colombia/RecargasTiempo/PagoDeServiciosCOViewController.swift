

import UIKit
import Alamofire

class PagoDeServiciosCOViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout ,ServicesDelegate{
    
    @IBOutlet weak var collectionView: UICollectionView!
      
      let services = Services()
      var data : [ServiciosProductoCO] = []
    
    var nameSelected = ""
    var indexSelected = 0

    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.services.delegate = self
             
             
             let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
                   
                   
                   let params : [String : Any] = [:]
                   
                   self.services.send(type: .GET, url: "ColombiaMiddleware/1/2/es/multimarket/api/catalogoCategorias", params: params, header: headers, message: "Obteniendo",animate: true)
    
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
           //metodos del datasource de la collectionView
         func numberOfSections(in collectionView: UICollectionView) -> Int {
             return 1
         }
         
         func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             
             
             return self.data.count
         }
       
       
         
         
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriaProductCellIdentifier", for: indexPath) as! CategoriaPrepagoCell
            
            print(self.data[indexPath.row].nombre!)
           cell.nameLabel.text = self.data[indexPath.row].nombre!
             
          
             
         return cell
         }
         
         
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
             return CGSize(width: UIScreen.main.bounds.width / 2.07, height: self.collectionView.bounds.height / 5.1)
         }
         
       
          //metodos del delegado de collectionView
         //metodo que se ejecuta al presionar una celda
         func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
             
            
            
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
            
            
            let params : [String : Any] = [:]
            
            self.nameSelected = "\(self.data[indexPath.row].nombreCodificado!)"
            self.indexSelected = indexPath.row
            self.services.send(type: .GET, url: "ColombiaMiddleware/1/2/es/multimarket/api/catalogoServicios?categoria=\(self.data[indexPath.row].nombreCodificado!)", params: params, header: headers, message: "Obteniendo",animate: true)
         
           
           
           
             
             
             
         }
    
    
    
    //metodos delegados de los servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        
        if endpoint == "ColombiaMiddleware/1/2/es/multimarket/api/catalogoServicios?categoria=\(self.nameSelected)"{
            
            
                     let idError = response["codigo"] as! Int
                        
            if idError == 0{
                
                self.data[indexSelected].servicios = []
                
                let servicios = response["servicios"] as! NSArray
                let search = response["searchable"] as! BooleanLiteralType
                
                self.data[self.indexSelected].search = search
                
                for element in servicios{
                    
                    
                    let dictio = element as! NSDictionary
                    
                    let productId = dictio["productId"] as! Int
                    let name = dictio["name"] as! String
                    
                    let objectTemp = ServiciosSeleccionCO(id: String(productId), nombre: name)
                    
                    self.data[self.indexSelected].servicios.append(objectTemp)
                    
                }
                
                //ya tenemos cargadas todas la info
                
                if self.data[self.indexSelected].search == true{
                    print("mandamos a la vista de busqueda")
                    
                    self.performSegue(withIdentifier: "searchSegue", sender: nil)
                }else{
                    print("mandamos a la vista de seleccion normal")
                    
                    self.performSegue(withIdentifier: "normalSegue", sender: nil)
                }
                
                
            }else{
                let mensaje = response["mensaje"] as! String
                              self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
            
        }

        if endpoint == "ColombiaMiddleware/1/2/es/multimarket/api/catalogoCategorias"{
                  
                  
               let idError = response["codigo"] as! Int
                  
                  if idError == 0{
                      //EXITO
                    
                    self.data = []
                    
                let productos = response["categorias"] as! NSArray
                    
                    //primer ciclo
                    for element in productos {
                        let dictio = element as! NSDictionary
                        let nombre = dictio["nombre"] as! String
                     let nombreCodificado =  nombre.replacingOccurrences(of: " ", with: "%20")
                        
                       
                        
                        let elementTemp = ServiciosProductoCO(nombre: nombre)
                        elementTemp.nombreCodificado = nombreCodificado
                        
                        self.data.append(elementTemp)
                        
                        
                        
                        
                    }
                    
                    self.collectionView.reloadData()
                    
                  }else{
                    let mensaje = response["mensaje"] as! String
                    self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
            }
            
        }
                    
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "normalSegue"{
            
            let destinoViewController = segue.destination as!  PagoDeServiciosNormalSelectCOViewController
            
            destinoViewController.producto = self.data[self.indexSelected]
            
        }
        
        
        if segue.identifier == "searchSegue"{
                
                let destinoViewController = segue.destination as!  PagoDeServiciosSearchSelectCOViewController
                
                destinoViewController.producto = self.data[self.indexSelected]
                
            }
        
        
    }

}
