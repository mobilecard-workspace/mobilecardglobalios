

import UIKit
import SwiftyContacts


class SelectContactViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    //referencias
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    //arreglo que controla la tableView
    var contactos : [Contacto] = []
    
    //objeto que recibimos de la clase pasada
    var contacto : Contacto!
    
    let nibTableView: String = "ContactoTableViewCell"
    let cellTableViewIdentifier: String = "ContactoIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.tableView.register(UINib(nibName: self.nibTableView, bundle: nil), forCellReuseIdentifier: self.cellTableViewIdentifier)
      
  
        
        //obtenemos los contactos
        requestAccess { (responce) in
            if responce{
                print("Contacts Acess Granted")
                authorizationStatus { (status) in
                    switch status {
                    case .authorized:
                        print("authorized")
                        
                        
                        fetchContacts(completionHandler: { (result) in
                            switch result{
                            case .Success(response: let contacts):
                                // Do your thing here with [CNContacts] array
                                
                                self.contactos = []
                                for element in contacts{
                                    
                                    var nameBuild : String = ""
                                    var phoneBuild : String = ""
                                    var mailBuild : String = ""
                                    
                                    if element.givenName != ""{
                                       nameBuild.append("\(element.givenName) ")
                                    }
                                    if element.middleName != ""{
                                          nameBuild.append("\(element.middleName) ")
                                    }
                                    
                                    if element.familyName != ""{
                                          nameBuild.append("\(element.familyName)")
                                    }
                                    
                                    
                                    print(nameBuild)
                                    if element.phoneNumbers.count > 0{
                                        phoneBuild = element.phoneNumbers[0].value.stringValue
                                    }
                                    
                                   
                                   
                                     phoneBuild = phoneBuild.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                                    phoneBuild = phoneBuild.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                                    phoneBuild = phoneBuild.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
                                    phoneBuild = phoneBuild.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
                               
                                    print(phoneBuild)
                                    
                                    if element.emailAddresses.count > 0{
                                        mailBuild = String(element.emailAddresses[0].value)
                                 
                                    }
                                    print(mailBuild)
                                    
                                    
                                    if nameBuild == ""{
                                        
                                    }else{
                                        //si tiene nombre es un contacto utilizable
                                        let objectTemp = Contacto(nombre: nameBuild)
                                        
                                        if phoneBuild != ""{
                                            objectTemp.telefono = phoneBuild
                                        }
                                        if mailBuild != ""{
                                            objectTemp.correo = mailBuild
                                        }
                                        
                                        if objectTemp.telefono.count >= 10{
                                            
                                            let lastTen = String(objectTemp.telefono.suffix(10))
                                            print("este es el lastTen \(lastTen)")
                                            
                                            objectTemp.telefono = lastTen
                                            
                                            
                                            
                                            self.contactos.append(objectTemp)
                                        }
                                        
                                        
                                    }
                                    self.tableView.reloadData()
                                    
                                    
                                }
                                
                                break
                            case .Error(error: let error):
                                print(error)
                                break
                            }
                        })
                        
                        
                        break
                    case .denied:
                        print("denied")
                        let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                            
                            self.dismiss(animated: true, completion: nil)
                        }));
                        self.present(alert, animated: true, completion: nil);
                        break
                    default:
                        break
                    }
                }
            } else {
                print("Contacts Acess Denied")
                
                
                
                let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                    
                    self.dismiss(animated: true, completion: nil)
                }));
                self.present(alert, animated: true, completion: nil);
                
                
            }
        }
        
        
        
      
    
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
   
        
    }
    override func viewDidAppear(_ animated: Bool) {
       
        
       
        
        
    }
    
    
    @IBAction func returnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
  
    
    //metodos del datasource y delegate de tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.contactos.count == 0 ? 1 : self.contactos.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (self.contactos.count == 0) {
            let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "No");
            cell.textLabel?.text = "No hay contactos disponibles.";
            cell.selectionStyle = .none
            return cell;
        }
        
        let cell: ContactoTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: self.cellTableViewIdentifier) as! ContactoTableViewCell
        
        
        
        
        cell.nameLabel.text = self.contactos[indexPath.row].nombre!
        
        
        
        cell.selectionStyle = .none
        
        return cell;
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("se presiono un celda")
        
      
        DispatchQueue.main.async {
        self.contacto.nombre = self.contactos[indexPath.row].nombre
             self.contacto.telefono = self.contactos[indexPath.row].telefono
            
          
            
            
        self.dismiss(animated: true, completion: nil)
        }
   
        
    }
   
    
    
   
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
     
     
        
        if textField.text != ""{
            
            // si tiene contenido el textfield hacemos la busqueda con lo que puso
        
        searchContact(SearchString:  textField.text!) { (result) in
            switch result{
            case .Success(response: let contacts):
                
               
                
             
  
                    self.contactos = []
                    for element in contacts{
                        
                        var nameBuild : String = ""
                        var phoneBuild : String = ""
                        var mailBuild : String = ""
                        
                        if element.givenName != ""{
                            nameBuild.append("\(element.givenName) ")
                        }
                        if element.middleName != ""{
                            nameBuild.append("\(element.middleName) ")
                        }
                        
                        if element.familyName != ""{
                            nameBuild.append("\(element.familyName)")
                        }
                        
                        
                        print(nameBuild)
                        if element.phoneNumbers.count > 0{
                            phoneBuild = element.phoneNumbers[0].value.stringValue
                        }
                        
                        
                        
                        phoneBuild = phoneBuild.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                        phoneBuild = phoneBuild.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                        phoneBuild = phoneBuild.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
                        phoneBuild = phoneBuild.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
                        
                        print(phoneBuild)
                        
                        if element.emailAddresses.count > 0{
                            mailBuild = String(element.emailAddresses[0].value)
                            
                        }
                        print(mailBuild)
                        
                        
                        if nameBuild == ""{
                            
                        }else{
                            //si tiene nombre es un contacto utilizable
                            let objectTemp = Contacto(nombre: nameBuild)
                            
                            if phoneBuild != ""{
                                
                                
                                objectTemp.telefono = phoneBuild
                                
                                
                                
                            }
                            if mailBuild != ""{
                                objectTemp.correo = mailBuild
                            }
                            
                            
                            if objectTemp.telefono.count >= 10{
                                
                            let lastTen = String(objectTemp.telefono.suffix(10))
                                print("este es el lastTen \(lastTen)")
                                
                                objectTemp.telefono = lastTen
                                
                                
                                
                            self.contactos.append(objectTemp)
                            }
                            
                        }
                        self.tableView.reloadData()
                        
                        
                    }
                    
                
                
                break
            case .Error(error: let error):
                print(error)
                
                let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                    
                    self.navigationController?.popViewController(animated: true)
                }));
                self.present(alert, animated: true, completion: nil);
                
                
                
                break
            }
        }
        
        
        }else{
            
          
            
            //si no tiene contenido el textfield hacemos la busqueda vacia
            requestAccess { (responce) in
                if responce{
                    print("Contacts Acess Granted")
                    authorizationStatus { (status) in
                        switch status {
                        case .authorized:
                            print("authorized")
                            
                            
                            fetchContacts(completionHandler: { (result) in
                                switch result{
                                case .Success(response: let contacts):
                                    // Do your thing here with [CNContacts] array
                                    
                                    self.contactos = []
                                    for element in contacts{
                                        
                                        var nameBuild : String = ""
                                        var phoneBuild : String = ""
                                        var mailBuild : String = ""
                                        
                                        if element.givenName != ""{
                                            nameBuild.append("\(element.givenName) ")
                                        }
                                        if element.middleName != ""{
                                            nameBuild.append("\(element.middleName) ")
                                        }
                                        
                                        if element.familyName != ""{
                                            nameBuild.append("\(element.familyName)")
                                        }
                                        
                                        
                                        print(nameBuild)
                                        if element.phoneNumbers.count > 0{
                                            phoneBuild = element.phoneNumbers[0].value.stringValue
                                        }
                                        
                                        
                                        
                                        phoneBuild = phoneBuild.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                                        phoneBuild = phoneBuild.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                                        phoneBuild = phoneBuild.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
                                        phoneBuild = phoneBuild.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
                                        
                                        print(phoneBuild)
                                        
                                        if element.emailAddresses.count > 0{
                                            mailBuild = String(element.emailAddresses[0].value)
                                            
                                        }
                                        print(mailBuild)
                                        
                                        
                                        if nameBuild == ""{
                                            
                                        }else{
                                            //si tiene nombre es un contacto utilizable
                                            let objectTemp = Contacto(nombre: nameBuild)
                                            
                                            if phoneBuild != ""{
                                                objectTemp.telefono = phoneBuild
                                            }
                                            if mailBuild != ""{
                                                objectTemp.correo = mailBuild
                                            }
                                            
                                            if objectTemp.telefono.count >= 10{
                                                
                                                let lastTen = String(objectTemp.telefono.suffix(10))
                                                print("este es el lastTen \(lastTen)")
                                                
                                                objectTemp.telefono = lastTen
                                                
                                                
                                                
                                                self.contactos.append(objectTemp)
                                            }
                                            
                                            
                                        }
                                        self.tableView.reloadData()
                                        
                                        
                                    }
                                    
                                    break
                                case .Error(error: let error):
                                    print(error)
                                    break
                                }
                            })
                            
                            
                            break
                        case .denied:
                            print("denied")
                            let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                                
                                self.navigationController?.popViewController(animated: true)
                            }));
                            self.present(alert, animated: true, completion: nil);
                            break
                        default:
                            break
                        }
                    }
                } else {
                    print("Contacts Acess Denied")
                    
                    
                    
                    let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                        
                        self.navigationController?.popViewController(animated: true)
                    }));
                    self.present(alert, animated: true, completion: nil);
                    
                    
                }
            }
            
        }
        
    }
   
}



