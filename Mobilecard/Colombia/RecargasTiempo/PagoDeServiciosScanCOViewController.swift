

import UIKit
import Alamofire

class PagoDeServiciosScanCOViewController: UIViewController,UITextFieldDelegate,ScannerQrDelegate,ServicesDelegate{
    
    
    @IBOutlet weak var numberServiceTextField: UITextField_Validations!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var saldoLabel: UILabel!
    
    @IBOutlet weak var continuarButton: UIButton!
    @IBOutlet weak var comisionLabel: UILabel!
    
    var serviceId : String!
    var productName : String!
    var serviceName: String!
    
    var serviceComplete = false

    //la referencia del servicio
    var idReferencia = ""
    var monto = ""
    var comision = ""
    
      let services = Services()
       
    var dictioToSend : NSMutableDictionary = [:]
    
  

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.services.delegate = self
      
        
        self.nameLabel.text = self.serviceName!
        
        
        print("id seleccionado = \(self.serviceId!)")
      
      //  self.title = "\(self.producto.nombre!)"
            
    
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
      
      //metodo que se ejecuta al presionar el boton de escanear numero de servicio
    @IBAction func scanAction(_ sender: UIButton) {
        
    let scan = ScannerQr()
        scan.isFromServices = true
          scan.delegate = self
       self.present(scan, animated: true, completion: nil)
    }
    
       
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        if textField.text != ""{
            
        print("termino de editar y este es el texto \(textField.text!)")
            
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
                              
                              
            let params : [String : Any] = ["idConvenio":self.serviceId!,"reference":textField.text!,"barcode":"false","idUsuario":DataUser.USERIDD!]
                              
                              self.services.send(type: .POST, url: "ColombiaMiddleware/multimarket/api/escaneaFactura", params: params, header: headers, message: "Obteniendo",animate: true)
            print("PARAMS: \(params)")
            
            
        }
        
        
    }

    //metodo que se ejecuta al presionar el botón de información
    @IBAction func informationAction(_ sender: UIButton) {
        
    
        self.alert(title: "Nota", message: "Por favor verifica que tu número de servicio o referencia de recibo este correcto para asegurar el correcto pago a la compañia correspondiente.", cancel: "Aceptar")
    }
    
    
    
    //metodos delegados de la obtención del código y al cerrar la vista
    func successQr(qr: String) {
        
        let headers  : HTTPHeaders = ["Content-Type":"application/json","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
                          
                        
        self.numberServiceTextField.text = qr
        let params : [String : Any] = ["idConvenio":self.serviceId!,"reference":qr,"barcode":"true","idUsuario":DataUser.USERIDD!]
                          
                          self.services.send(type: .POST, url: "ColombiaMiddleware/multimarket/api/escaneaFactura", params: params, header: headers, message: "Obteniendo",animate: true)
        print("PARAMS: \(params)")
    }
    
    
    func backActionQr() {
        
    }
    
    
    //delegado de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        if endpoint == "ColombiaMiddleware/multimarket/api/escaneaFactura"{
            
            let code = response["codigo"] as! Int
            
            if code == 0{
                
                
                
              
                
                
                let idReferencia = String(response["idReferencia"] as! Int)
                
                let amountEditable = response["amountEditable"] as!Bool
                
                  self.idReferencia = idReferencia
                
                if amountEditable{
                    //es editable mandamos al teclado
                    
                    
                    self.dictioToSend =  ["idReferencia":self.idReferencia,"serviceNumber":self.numberServiceTextField.text!]
                    self.performSegue(withIdentifier: "montoSegue", sender: nil)
                    
                }else{
                //no es editable seguimos en esta vista mostrando la información
                    
                
                let comision = response["comision"] as! Double
                let monto = response["monto"] as! Double
                self.serviceComplete = true
                self.monto = String(monto)
                self.comision = String(comision)
                self.saldoLabel.text = "SALDO (COP): $ \(monto)"
                self.comisionLabel.text = "Comisión (COP): $ \(comision)"
                
                self.continuarButton.backgroundColor = #colorLiteral(red: 0.5678757429, green: 0.7434515953, blue: 0.08672682196, alpha: 1)
                    
                }
                
                
            }else{
                
                self.continuarButton.backgroundColor = #colorLiteral(red: 0.9499617219, green: 0.3273513913, blue: 0.1274631917, alpha: 1)
                self.idReferencia = ""
                self.serviceComplete = false
                let mensaje = response["mensaje"] as! String
                self.alert(title: "¡Aviso!", message: mensaje, cancel: "OK")
                
            }
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    
    func errorService(endpoint: String) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    //metodo que se ejecuta al presionar el boton de continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        if self.serviceComplete == false || self.idReferencia == ""{
            
            self.alert(title: "¡Aviso!", message: "Debes escanear o agregar un número de servicio para continuar.", cancel: "OK")
            return;
        }
        
        
        
        
               let story = UIStoryboard(name: "wallet", bundle: nil)
               let vc = story.instantiateViewController(withIdentifier: "wallet_view") as! Wallet_Ctrl


        let dictio : NSMutableDictionary =  ["idReferencia":self.idReferencia,"monto":self.monto,"comision":self.comision,"serviceNumber":self.numberServiceTextField.text!]
               

        vc._COServices = "1"

         DataUser.DATA = dictio
        
               
               self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "montoSegue"{
            
            let vc = segue.destination as! PrecioServiciosCOViewController
            
            vc.data = self.dictioToSend
            
            
        }
        
        
    }
    
    
}
