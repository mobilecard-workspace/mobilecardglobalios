

import UIKit
import Alamofire
import SwiftyContacts

class RecargasTiempoCostoSelectionCOViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,ServicesDelegate,UITextFieldDelegate,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var numberManualTextField: UITextField!
    @IBOutlet weak var constraintHeightPhoneManual: NSLayoutConstraint!
    
    @IBOutlet weak var phoneSwitch: UISwitch!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var imageCommerce: UIImageView!
    let services = Services()
    
    var contacto : Contacto!
    
    var precio : Double! = 0.0
    
    //objeto que recibimos de la clase pasada
    var elementLast : RecargaModel!
    
    //arreglo que controla la tableView
    var data : [PrecioModel] = []
    
    //label donde se pinta el numero sacado de los contactos
    @IBOutlet weak var numberContactLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        self.contacto = Contacto(nombre: "")
        
        
        
        if let imageURL = URL(string: self.elementLast.imgcolor!){
            self.imageCommerce.af_setImage(withURL: imageURL)
        }
        

        self.services.delegate = self
        
        
        let headers  : HTTPHeaders = [:]
        
        
        let params : [String : Any] = [:]
        
        self.services.send(type: .GET, url: "Catalogos/1/2/es/puntored/getMontos", params: params, header: headers, message: "Obteniendo",animate: true)
        
    
        self.requestPermisseContacts()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
      
        //si tenemos un contacto seleccionado
        if self.contacto.nombre != ""{
            self.numberContactLabel.text = "\(self.contacto.nombre!) \(self.contacto.telefono!)"
            
            
            if self.precio != 0.0{
               
                    self.continueButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
                
            }else{
                self.continueButton.backgroundColor = #colorLiteral(red: 0.9499617219, green: 0.3273513913, blue: 0.1274631917, alpha: 1)
            }
            
        }
        
        
    }

    func requestPermisseContacts(){
        
    
        
        //obtenemos los contactos
        requestAccess { (responce) in
            if responce{
                print("Contacts Acess Granted")
                authorizationStatus { (status) in
                    switch status {
                    case .authorized:
                        print("authorized")
                        
                        
                        fetchContacts(completionHandler: { (result) in
                            switch result{
                            case .Success(response: let contacts):
                             
                                
                       
                              
                                
                                break
                            case .Error(error: let error):
                                print(error)
                                break
                            }
                        })
                        
                        
                        break
                    case .denied:
                        print("denied")
                     /*   let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                            
                            self.navigationController?.popViewController(animated: true)
                        }));
                        self.present(alert, animated: true, completion: nil);*/
                        break
                    default:
                        break
                    }
                }
            } else {
                print("Contacts Acess Denied")
                
                
                /*
                let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                    
                    self.navigationController?.popViewController(animated: true)
                }));
                self.present(alert, animated: true, completion: nil);*/
                
                
            }
        }
        
        
    }
    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  
    
    
    
   
    
      //metodos del datasource de la collectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return self.data.count
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let celdaID = "precioIdentifier"
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: celdaID, for: indexPath) as! PrecioCell
        
        
        if self.data[indexPath.row].isSelected == true{
            //cuando esta seleccionada
         
            cell.amountLabel.textColor = UIColor.white
            cell.buttonBackground.setBackgroundImage(UIImage(named: "btn_naranja-3x"), for: .normal)
        }else{
            //cuando no esta seleccionada
            
            cell.amountLabel.textColor = UIColor.lightGray
            cell.buttonBackground.setBackgroundImage(UIImage(named: "unselectedTiempo"), for: .normal)
            
        }
        
       
        cell.amountLabel.text = "$\(self.data[indexPath.row].amount!)"
        
       
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2.3, height: self.collectionView.bounds.height / 6.1)
    }
    
    
    
    
  
     //metodos del delegado de collectionView
    //metodo que se ejecuta al presionar una celda
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("selection index \(indexPath.row)")
        
        //seleccionamos la celda
        
        for element in self.data{
            
            element.isSelected = false
            
        }
        
        self.data[indexPath.row].isSelected = true
        self.precio = self.data[indexPath.row].amount
        self.collectionView.reloadData()
        
        
        //ya tenemos todos los campos llenos
        if self.contacto.nombre != "" || (self.numberManualTextField.text!.count >= 9 && self.numberManualTextField.text!.count <= 12){
            self.continueButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
            
            
        }else{
              self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        
    }

    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
        print("RESPONSE \(response)")
        
        if endpoint == "Catalogos/1/2/es/puntored/getMontos"{
            
            
            
         let idError = response["idError"] as! Int
            
            if idError == 0{
                //EXITO
                
                
                self.data = []
                
                let montos = response["montos"] as! NSArray
                
                for element in montos {
                    let cantidad = element as! Double
                 
                
              
                    
                    let objectTemp = PrecioModel(amount: cantidad)
                    
    
                    
                    
                    self.data.append(objectTemp)
                
                }
                
                self.collectionView.reloadData()
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel:"OK")
                
            }
    
            
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //metodo que se ejecuta al presionar el botón de elige un número
    @IBAction func selectNumberAction(_ sender: UIButton) {
        
        self.phoneSwitch.setOn(false, animated: false)
       self.numberManualTextField.text = ""
        self.constraintHeightPhoneManual.constant = 0
          self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        let vc = SelectContactViewController()
         vc.modalPresentationStyle = .fullScreen
        vc.contacto = self.contacto
        
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    
    //metodo que se ejecuta al presionar el switch
    @IBAction func switchAction(_ sender: UISwitch) {
        
        if sender.isOn == true{
            //activar
            self.numberContactLabel.text = "Selecciona un número de tus contactos"
            self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.contacto.nombre = ""
            self.contacto.telefono = ""
            self.constraintHeightPhoneManual.constant = 40
            
        }else{
            //desactivar
            self.constraintHeightPhoneManual.constant = 0
            self.numberManualTextField.text = ""
            self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text!.count >= 9 && textField.text!.count <= 12{
            
            if self.precio != 0.0{
                   self.continueButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
            }else{
                self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                
            }
            
            
        }else{
             self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        
    }
    
    
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        
        if self.contacto.nombre == "" && (self.numberManualTextField.text!.count < 9 || self.numberManualTextField.text!.count > 12){
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un teléfono para continuar.", cancel: "OK")
            return;
        }
        
        
        if self.precio == 0.0{
            self.alert(title: "¡Aviso!", message: "Debes seleccionar un monto para continuar.", cancel: "OK")
            return;
            
        }
        
       
        
       
    
        let story = UIStoryboard(name: "wallet", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "wallet_view") as! Wallet_Ctrl
        
        let dictio : NSMutableDictionary =  ["type":"1","idPaquete":"","monto":self.precio!,"proveedor":self.elementLast.codigo!]
        
        if self.numberManualTextField.text != ""{
            dictio["telefono"] = self.numberManualTextField.text!
                 vc._COPhone = self.numberManualTextField.text!
        }else{
            dictio["telefono"] = self.contacto.telefono!
                 vc._COPhone = self.contacto.telefono!
        }
        
         DataUser.DATA = dictio
 
        
        self.navigationController?.pushViewController(vc, animated: true)
    
    
    }
    
    
  
    
    
}
