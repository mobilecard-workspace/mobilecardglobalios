

import UIKit
import Alamofire

class PagoDeServiciosSearchSelectCOViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    
  //producto que recibimos de la clase pasada
    var producto : ServiciosProductoCO!
        var filterActive = 0
    
    var filtroServicios : [ServiciosSeleccionCO] = []
    
    @IBOutlet weak var tableView: UITableView!
    
   
    @IBOutlet weak var searchTextField: UITextField!
    

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "\(self.producto.nombre!)"
        
        //target para el search
               self.searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
       self.tableView.separatorStyle = .none
      

     
     
    
   
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
      
        
        
    }

   //metodo que checa en todo momento el contenido del textfield
   @objc func textFieldDidChange(_ textField: UITextField){
       
    let filtered = self.producto.servicios.filter{ $0.nombre.contains(textField.text!) || $0.nombre.lowercased().contains(textField.text!)}
       
      
       
       self.filtroServicios = filtered
       
       if textField.text!.count > 0{
           
           self.filterActive = 1
       }else{
           self.filterActive = 0
       }
       
       self.tableView.reloadData()
      
       
   }
    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  
    
    
    
   
   
    
    
    
    
  
   

    
    
    
    //metodos del datasource de la tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
         if self.filterActive == 1{
            
             return self.filtroServicios.count
         }else{
            return self.producto.servicios.count
         }
        
        
   
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
  
        var servicioIndex = self.producto.servicios[indexPath.row]
     
     
        
        
        if self.filterActive == 1{
                  servicioIndex = self.filtroServicios[indexPath.row]
                  
              }else{
            servicioIndex = self.producto.servicios[indexPath.row]
              }
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchServicioCOIdentifier",for: indexPath) as! SearchServicioCOTableCell
            
        cell.nameLabel.text = servicioIndex.nombre!
            
            cell.selectionStyle = .none
            
            return cell
            
       
 
    }
    
    /*
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }*/
    
    
    
    
    
    
    //metodos del delegado de tableView
    //metodo que se ejecuta al presionar una celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selection index \(indexPath.row)")
        
        if self.filterActive == 1{
            
            print("se selecciono \(self.filtroServicios[indexPath.row].nombre!)")
            //se va a la otra vista con este objeto
            

            
             var dictio :[String:Any] = [:]
             
             dictio["id"] = self.filtroServicios[indexPath.row].id!
             
             dictio["nombre"] = self.filtroServicios[indexPath.row].nombre!
             
              
             self.performSegue(withIdentifier: "scanSegue", sender: dictio)
              
            
            
        }else{
            print("se selecciono \(self.producto.servicios[indexPath.row].nombre!)")
            //se va a la otra vista con este objeto
            

            
             var dictio :[String:Any] = [:]
             
             dictio["id"] = self.producto.servicios[indexPath.row].id!
             
             dictio["nombre"] = self.producto.servicios[indexPath.row].nombre!
             
              
             self.performSegue(withIdentifier: "scanSegue", sender: dictio)
              
            
        }
        
 
   
        
        
    }


    
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
      if segue.identifier == "scanSegue"{
          
          let destination = segue.destination as! PagoDeServiciosScanCOViewController
          
          let dictio = sender as! [String: Any]
          
          destination.productName = self.producto.nombre!
          destination.serviceName = dictio["nombre"] as? String
          destination.serviceId = dictio["id"] as? String
          
      }
      
      
  }
    
    
}
