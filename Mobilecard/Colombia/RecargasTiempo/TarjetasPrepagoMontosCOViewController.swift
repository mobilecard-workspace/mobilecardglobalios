

import UIKit
import Alamofire

class TarjetasPrepagoMontosCOViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    @IBOutlet weak var collectionView: UICollectionView!
     
     
     var producto : PrepagoProductoCO!

    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        print("si vienen productos\(producto.montos.count)")
        self.title = "\(self.producto.nombre!)"
    
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    

      
        //metodos del datasource de la collectionView
      func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
      }
      
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          
          
        return self.producto.montos.count
      }
    
    
      
      
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
           let celdaID = "precioIdentifier"
                 
                     let cell = collectionView.dequeueReusableCell(withReuseIdentifier: celdaID, for: indexPath) as! PrecioCell
                 
                 
        if self.producto.montos[indexPath.row].isSelected == true{
                     //cuando esta seleccionada
                  
                     cell.amountLabel.textColor = UIColor.white
                     cell.buttonBackground.setBackgroundImage(UIImage(named: "btn_naranja-3x"), for: .normal)
                 }else{
                     //cuando no esta seleccionada
                     
                     cell.amountLabel.textColor = UIColor.lightGray
                     cell.buttonBackground.setBackgroundImage(UIImage(named: "unselectedTiempo"), for: .normal)
                     
                 }
                 
                
        cell.amountLabel.text = "$\(self.producto.montos[indexPath.row].valor!)"
                 
                
                 
                 return cell
      }
      
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          return CGSize(width: UIScreen.main.bounds.width / 2.3, height: self.collectionView.bounds.height / 6.1)
      }
      
    
       //metodos del delegado de collectionView
      //metodo que se ejecuta al presionar una celda
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
          print("selected index \(indexPath.row)")
          
        for element in self.producto.montos{
            
            element.isSelected = false
            
            
            
        }
        
        self.producto.montos[indexPath.row].isSelected = true
          //self.performSegue(withIdentifier: "showCategories", sender: indexPath.row)
        self.collectionView.reloadData()
          
          
      }

    
  //este es el metodo que se ejecuta al presionar el boton de continuar
    @IBAction func continueAction(_ sender: UIButton) {
        
        
        DispatchQueue.main.async {
        
        for element in self.producto.montos{
            
            
            if element.isSelected == true{
                
                
                let story = UIStoryboard(name: "wallet", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "wallet_view") as! Wallet_Ctrl
                
                vc._COCards = "1"

                let dictio : NSMutableDictionary =  ["idUsuario":DataUser.USERIDD!,"idPais":"2","idApp":"1","idioma":"es","idProducto":element.id!,"amount":element.valor!,"producto":"\(self.producto.nombre!)"]
                      
                DataUser.DATA = dictio
                      self.navigationController?.pushViewController(vc, animated: true)



                
                //nos llevamos ese monto
                return;
            }
            
            
        }
        
        //si termina el ciclo no tenemos ninguno seleccionado
        
        self.alert(title: "¡Aviso!", message: "Debes seleccionar un monto para continuar.", cancel: "OK")
        
    }
    
    
    
    }


}
