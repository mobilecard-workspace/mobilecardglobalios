

import UIKit
import Alamofire

class SoatDetailCOViewController: UIViewController,ServicesDelegate {

    //objeto que recibimos de la vista pasada
    var soatModel : SoatModel!
    
    var services : Services = Services()
    
    //elementos visuales
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var marcaLabel: UILabel!
    @IBOutlet weak var ccLabel: UILabel!
    @IBOutlet weak var lineaLabel: UILabel!
    @IBOutlet weak var telefonoLabel: UILabel!
    @IBOutlet weak var modeloLabel: UILabel!
    
    @IBOutlet weak var correoLabel: UILabel!
    @IBOutlet weak var placaLabel: UILabel!
    @IBOutlet weak var categoriaLabel: UILabel!
    
    @IBOutlet weak var seguroSoatLabel: UILabel!
    @IBOutlet weak var vigenciaIniLabel: UILabel!
    @IBOutlet weak var vigenciaFinLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var privacySwitch: UISwitch!
    
    
    
    @IBOutlet weak var continuarButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      self.services.delegate = self
        self.nameLabel.text = self.soatModel.nombre!
        self.marcaLabel.text = self.soatModel.marca!
        self.ccLabel.text = self.soatModel.cc!
        self.lineaLabel.text = self.soatModel.linea!
        self.telefonoLabel.text = self.soatModel.telefono!
        self.modeloLabel.text = self.soatModel.modelo!
        self.correoLabel.text = self.soatModel.email!
        self.placaLabel.text = self.soatModel.placa!
        self.categoriaLabel.text = self.soatModel.categoria!
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: self.soatModel.seguroSoat as NSNumber) {
             self.seguroSoatLabel.text = "\(String(formattedTipAmount))"
        }
        
       
        self.vigenciaIniLabel.text = self.soatModel.vigenciaIni!
        self.vigenciaFinLabel.text = self.soatModel.vigenciaFin!
        
        if let formattedTipAmountTotal = formatter.string(from: self.soatModel.total! as NSNumber) {
         
            self.totalLabel.text = "\(String(formattedTipAmountTotal))"
           
        }
        
     
        
        
        
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    //metodo que se ejecuta al presionar el botón de continuar
    @IBAction func continuarAction(_ sender: UIButton) {
        //hilo principal
      DispatchQueue.main.async {
        
        if self.privacySwitch.isOn == true{
            
          print("continuamos pago y jumio")
            
            
            let story = UIStoryboard(name: "wallet", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "wallet_view") as! Wallet_Ctrl
            
            let dictio : NSMutableDictionary =  ["nombre":self.soatModel.nombre!,"cc":self.soatModel.cc!,"telefono":self.soatModel.telefono!,"email":self.soatModel.email!,"marca":self.soatModel.marca!,"linea":self.soatModel.linea!,"modelo":self.soatModel.modelo!,"placa":self.soatModel.placa!,"categoria":self.soatModel.categoria!,"total":String(self.soatModel.total!),"vigenciaIni":self.soatModel.vigenciaIni!,"vigenciaFin":self.soatModel.vigenciaFin!,"seguroSoat":String(self.soatModel.seguroSoat!),"idUsuario":"\(DataUser.USERIDD!)","idPais":"2","idApp":"1","idioma":"es"]
        
        
                 vc._COSoat = "1"
            
            DataUser.DATA = dictio
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
            
        }else{
            self.alert(title: "¡Aviso!", message: "Debes aceptar los terminos y condiciones para continuar.", cancel: "OK")
        }
  
      
        }
        
    }
    
    
    //metodo que se  ejecuta al presionar el switch
    @IBAction func switchAction(_ sender: UISwitch) {
   
        if sender.isOn == true{
            //se puso
      self.continuarButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
            let headers  : HTTPHeaders = ["client":"f0b5f378b80597da0572d38cd881c9cda584a0ae4a363b33e157ac32059f0ae5"]
            let params : [String : Any] = [:]
            
            self.services.send(type: .GET, url: "Usuarios/1/terminos/2/es", params: params, header: headers, message: "Obteniendo",animate:true)
            
        }else{
            //se quito
            
            self.continuarButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            
        }
        
    
    }
    
    
    
    //metodo que se ejecuta al presionar el botón de terminos y condiciones
    @IBAction func terminosAction(_ sender: UIButton) {
        
        let headers  : HTTPHeaders = ["client":"f0b5f378b80597da0572d38cd881c9cda584a0ae4a363b33e157ac32059f0ae5"]
        let params : [String : Any] = [:]
       
        self.services.send(type: .GET, url: "Usuarios/1/terminos/2/es", params: params, header: headers, message: "Obteniendo",animate:true)
        
    }
    
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        if endpoint == "Usuarios/1/terminos/2/es"{
            
            let idError = response["idError"] as! Int
            
            if idError == 0{
                
                let termino = response["termino"] as! String
                self.alertPrivacy(title: "Términos y condiciones", message: termino, cancel: "Aceptar")
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel: "OK")
            }
            
        }
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    
    func errorService(endpoint: String) {
                self.alert(title: "¡Aviso!", message: "No fue posible conectarse con el servidor, favor de revisar su conexión a internet.", cancel: "OK")
    }
    
   
}
