

import UIKit
import Alamofire

class ConfirmarRecargaCOViewController: UIViewController,ServicesDelegate {

    @IBOutlet weak var montoLabel: UILabel!
    @IBOutlet weak var telefonoLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var comisionLabel: UILabel!
    @IBOutlet weak var imagenTarjetaImageView: UIImageView!
    @IBOutlet weak var numeroTarjetaLabel: UILabel!
    
    //variables que recibimos y las hacemos globales
    var idTarjeta = 0
    var monto = 0.0
    var type = ""
    var proveedor = ""
    var telefono = ""
    var idPaquete = 0
    
    let services = Services()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let  cardToUpdate :NSArray = UserDefaults.standard.object(forKey: "tarjetas") as! NSArray

        let tarjeta : NSDictionary = cardToUpdate[Int(DataUser.IDTARJETACO)!] as! NSDictionary
        let img_short = tarjeta["img_short"] as! String
        
        
        if let imageURL = URL(string: img_short){
            self.imagenTarjetaImageView.af_setImage(withURL: imageURL)
        }
        
        
        print("Este es el id de tarjeta \(tarjeta)")
        print("estos son los demas datos \(DataUser.DATA!)")
       
        self.type = DataUser.DATA!["type"] as! String
        
        //type 1 recarga normal type 2 paquetes
        
        switch type{
            
        case "1":
            self.monto = DataUser.DATA!["monto"] as! Double
            self.telefono = DataUser.DATA!["telefono"] as! String
            self.proveedor = DataUser.DATA!["proveedor"] as! String
            break
        case "2":
        self.idPaquete = DataUser.DATA!["idPaquete"] as! Int
        self.telefono = DataUser.DATA!["telefono"] as! String
            break
        default:
            break
        }
        
      
        
        
        self.montoLabel.text = "$\(DataUser.DATA!["monto"] as! Double)"
        
        self.telefonoLabel.text = "\(DataUser.DATA!["telefono"] as! String)"
        
         self.totalLabel.text = "$\(DataUser.DATA!["monto"] as! Double)"
        
            self.idTarjeta = tarjeta["idTarjeta"] as! Int
        
        let pan = tarjeta["pan"] as! String
        
        let cardManager : pootEngine = pootEngine()
        
        let panDecoded : String = cardManager.decryptedString(of: pan, withSensitive: false) as! String
        
        
        let lastFour = String(panDecoded.suffix(4))
        let firstFour = String(panDecoded.prefix(4))
        
        let stringCard = "\(firstFour) **** **** \(lastFour)"
        
        self.numeroTarjetaLabel.text = stringCard
        
        
        //delegado de los servicios esta clase
        self.services.delegate = self
    }
    

  //metodo que se ejecuta al presionar continuar
    @IBAction func continuarAction(_ sender: UIButton) {
        
        switch type{
            
        case "1":
          
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
            
            let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,
                                           "idPais":"2",
                                           "idApp":"1",
                                           "codigoProveedor":self.proveedor,
                                           "idTarjeta":self.idTarjeta,
                                           "monto":String(self.monto),
                                           "imei":DataUser.IMEI!,
                                           "idioma":"es",
                                           "concepto":"recarga",
                                           "numeroCelular":self.telefono]
           
           print("PARAMS: \(params)")
           
            self.services.send(type: .POST, url: "ColombiaMiddleware/puntored/api/recarga", params: params, header: headers, message: "Generando",animate: true)
            
            
            break
            
        case "2":
            
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*","Authorization":"Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"]
            
            let params : [String : Any] = ["idUsuario":DataUser.USERIDD!,
                                           "idPais":"2",
                                           "idApp":"1",
                                           "idTarjeta":self.idTarjeta,
                                           "imei":DataUser.IMEI!,
                                           "idioma":"es",
                                           "concepto":"paquete",
                                           "idPaquete":String(self.idPaquete),
                                        "numeroCelular":self.telefono]
            
            print("PARAMS: \(params)")
            
            self.services.send(type: .POST, url: "ColombiaMiddleware/puntored/api/paquete", params: params, header: headers, message: "Generando",animate: true)
            
            
            
            break
         
        default:
            break
        }
        
        
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //delegado de los servicios
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        print("RESPONSE: \(response)")
        
        if endpoint == "ColombiaMiddleware/puntored/api/recarga"{
            
            let codigo = response["codigo"] as! Int
            
            if codigo == 0{
                //exito
                self.performSegue(withIdentifier: "successSegue", sender: nil)
            }else{
                //fracaso
                  self.performSegue(withIdentifier: "failSegue", sender: nil)
            }
            
        }
        
        
        if endpoint == "ColombiaMiddleware/puntored/api/paquete"{
            
            let codigo = response["codigo"] as! Int
            
            if codigo == 0{
                //exito
                self.performSegue(withIdentifier: "successSegue", sender: nil)
            }else{
                //fracaso
                self.performSegue(withIdentifier: "failSegue", sender: nil)
            }
            
        }
        
    }
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
        self.alert(title: "¡Aviso!", message: "No fue posible establecer conexión con el servidor. Intente nuevamente.", cancel: "OK")
    }
    
}
