

import UIKit
import Alamofire
import SwiftyContacts

class RecargasPaquetesCostoSelectionCOViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,ServicesDelegate,UITextFieldDelegate{

    @IBOutlet weak var numberManualTextField: UITextField!
    @IBOutlet weak var constraintHeightPhoneManual: NSLayoutConstraint!
    
    @IBOutlet weak var phoneSwitch: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var imageCommerce: UIImageView!
    let services = Services()
    
    var contacto : Contacto!
    
    var idCategoria : Int!
    
    var idPaquete : Int = 0
    var montoEnviar : Double = 0.0
    
    //objeto que recibimos de la clase pasada
    var elementLast : RecargaModel!
    
    //arreglo que controla la tableView
    var data : [RecargaModel] = []
    
    //label donde se pinta el numero sacado de los contactos
    @IBOutlet weak var numberContactLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        self.contacto = Contacto(nombre: "")
        
        self.tableView.separatorStyle = .none
        
        if let imageURL = URL(string: self.elementLast.imgcolor!){
            self.imageCommerce.af_setImage(withURL: imageURL)
        }
        

        self.services.delegate = self
        
        
        let headers  : HTTPHeaders = [:]
        
        
        let params : [String : Any] = [:]
        
        self.services.send(type: .GET, url: "Catalogos/1/2/es/puntored/getPaquetes/\(self.idCategoria!)", params: params, header: headers, message: "Obteniendo",animate: true)
        
    
        self.requestPermisseContacts()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
      
        //si tenemos un contacto seleccionado
        if self.contacto.nombre != ""{
            self.numberContactLabel.text = "\(self.contacto.nombre!) \(self.contacto.telefono!)"
            
            
            if self.idPaquete != 0{
               
                    self.continueButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
                
            }else{
                self.continueButton.backgroundColor = #colorLiteral(red: 0.9499617219, green: 0.3273513913, blue: 0.1274631917, alpha: 1)
            }
            
        }
        
        
    }

    func requestPermisseContacts(){
        
    
        
        //obtenemos los contactos
        requestAccess { (responce) in
            if responce{
                print("Contacts Acess Granted")
                authorizationStatus { (status) in
                    switch status {
                    case .authorized:
                        print("authorized")
                        
                        
                        fetchContacts(completionHandler: { (result) in
                            switch result{
                            case .Success(response: let contacts):
                             
                                
                       
                              
                                
                                break
                            case .Error(error: let error):
                                print(error)
                                break
                            }
                        })
                        
                        
                        break
                    case .denied:
                        print("denied")
                     /*   let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                            
                            self.navigationController?.popViewController(animated: true)
                        }));
                        self.present(alert, animated: true, completion: nil);*/
                        break
                    default:
                        break
                    }
                }
            } else {
                print("Contacts Acess Denied")
                
                
                /*
                let alert = UIAlertController(title: "¡Aviso!", message: "Debes aceptar el permiso de acceso a contactos en ajustes para continuar.", preferredStyle: .alert);
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                    
                    self.navigationController?.popViewController(animated: true)
                }));
                self.present(alert, animated: true, completion: nil);*/
                
                
            }
        }
        
        
    }
    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  
    
    
    
   
    
      //metodos del datasource de la tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.data.count
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celdaID = "paquetesTableIdentifier"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: celdaID,for: indexPath) as! PaquetesCOTableCell
        
        
        if self.data[indexPath.row].isSelected == true{
            //cuando esta seleccionada
            cell.titleLabel.textColor = UIColor.white
            cell.amountLabel.textColor = UIColor.white
            cell.buttonBackground.setBackgroundImage(UIImage(named: "btn_naranja-3x"), for: .normal)
        }else{
            //cuando no esta seleccionada
            cell.titleLabel.textColor = UIColor.lightGray
            cell.amountLabel.textColor = UIColor.lightGray
            cell.buttonBackground.setBackgroundImage(UIImage(named: "unselectedPaquete"), for: .normal)
            
        }
        
        cell.titleLabel.text = self.data[indexPath.row].nombre
        cell.amountLabel.text = "$\(self.data[indexPath.row].amount!)"
        
         cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
      return 60
    }
    
    
    
    
    
  
     //metodos del delegado de tableView
    //metodo que se ejecuta al presionar una celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selection index \(indexPath.row)")
        
        //seleccionamos la celda
        
        for element in self.data{
            
            element.isSelected = false
            
        }
        
        self.data[indexPath.row].isSelected = true
        self.idPaquete = self.data[indexPath.row].id
    self.montoEnviar = Double(self.data[indexPath.row].amount!)!
        self.tableView.reloadData()
        
        
        //ya tenemos todos los campos llenos
        if self.contacto.nombre != "" || (self.numberManualTextField.text!.count >= 9 && self.numberManualTextField.text!.count <= 12){
            self.continueButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
            
            
        }else{
              self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        
    }

    
    
    //metodos delegados de los servicios
    
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {
        
        
        print("RESPONSE \(response)")
        
        if endpoint == "Catalogos/1/2/es/puntored/getPaquetes/\(self.idCategoria!)"{
            
            
            
         let idError = response["idError"] as! Int
            
            if idError == 0{
                //EXITO
                
                
                self.data = []
                
                let operadores = response["paquetes"] as! NSArray
                
                for element in operadores {
                    
                    let dictio = element as! NSDictionary
                
                let id = dictio["id"] as! Int
                let codigo = dictio["codigo"] as! String
                let nombre = dictio["descripcion"] as! String
               
                    
                    let objectTemp = RecargaModel(id: id, codigo: codigo, nombre: nombre)
                    
                    let amount =  dictio["valor"] as! String
                    
                    objectTemp.amount = amount
                    
                    
                    self.data.append(objectTemp)
                
                }
                
                self.tableView.reloadData()
                
            }else{
                let mensajeError = response["mensajeError"] as! String
                self.alert(title: "¡Aviso!", message: mensajeError, cancel:"OK")
                
            }
    
            
        }
        
    }
    
    
    func responseArrayService(type: ServicesTypes, endpoint: String, response: NSArray) {
        
    }
    
    func errorService(endpoint: String) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //metodo que se ejecuta al presionar el botón de elige un número
    @IBAction func selectNumberAction(_ sender: UIButton) {
        
        self.phoneSwitch.setOn(false, animated: false)
       self.numberManualTextField.text = ""
        self.constraintHeightPhoneManual.constant = 0
        
        let vc = SelectContactViewController()
        vc.contacto = self.contacto
        vc.modalPresentationStyle = .fullScreen
        
          self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    
    //metodo que se ejecuta al presionar el switch
    @IBAction func switchAction(_ sender: UISwitch) {
        
        if sender.isOn == true{
            //activar
            self.numberContactLabel.text = "Selecciona un número de tus contactos"
            self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.contacto.nombre = ""
            self.contacto.telefono = ""
            self.constraintHeightPhoneManual.constant = 40
            
        }else{
            //desactivar
            self.constraintHeightPhoneManual.constant = 0
            self.numberManualTextField.text = ""
            self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            
        }
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text!.count >= 9 && textField.text!.count <= 12{
            
            if self.idPaquete != 0{
                   self.continueButton.backgroundColor = #colorLiteral(red: 0.5658991337, green: 0.7449050546, blue: 0.08672774583, alpha: 1)
            }else{
                self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                
            }
            
            
        }else{
             self.continueButton.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        
    }
    
    //metodo que se ejecuta al presionar el boton de continuar
    @IBAction func continuarAction(_ sender: UIButton) {
        
        
        if self.contacto.nombre == "" && (self.numberManualTextField.text!.count < 9 || self.numberManualTextField.text!.count > 12){
            
            self.alert(title: "¡Aviso!", message: "Debes agregar un teléfono para continuar.", cancel: "OK")
            return;
        }
        
        
        if self.idPaquete == 0{
            self.alert(title: "¡Aviso!", message: "Debes seleccionar un paquete para continuar.", cancel: "OK")
            return;
            
        }
        
     
       

      
        
    
        
        
        let story = UIStoryboard(name: "wallet", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "wallet_view") as! Wallet_Ctrl
        
        let dictio : NSMutableDictionary =  ["type":"2","idPaquete":self.idPaquete,"monto":self.montoEnviar]
        
        if self.numberManualTextField.text != ""{
            dictio["telefono"] = self.numberManualTextField.text!
            vc._COPhone = self.numberManualTextField.text!
        }else{
            dictio["telefono"] = self.contacto.telefono!
            vc._COPhone = self.contacto.telefono!
        }
        
        DataUser.DATA = dictio
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}
