//
//  colombia_TopUp_Step10.h
//  Mobilecard
//
//  Created by David Poot on 11/4/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "generalSummary.h"
#import "Wallet_Ctrl.h"

#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface colombia_TopUp_Step10 : mT_commonTableViewController <UICollectionViewDelegate, UICollectionViewDataSource, generalSummaryDelegate, cardSelectionDelegate, updateCardDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) NSDictionary *servicesData;
@property (strong, nonatomic) NSDictionary *serviceData;

@property (weak, nonatomic) IBOutlet UIButton *contactListButton;

@property (weak, nonatomic) IBOutlet UICollectionView *amountCollection;

@end

NS_ASSUME_NONNULL_END
