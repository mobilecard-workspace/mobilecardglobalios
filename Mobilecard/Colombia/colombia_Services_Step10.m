//
//  colombia_Services_Step10.m
//  Mobilecard
//
//  Created by David Poot on 11/5/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "colombia_Services_Step10.h"
#import "colombia_TopUp_result.h"


@interface colombia_Services_Step10 ()
{
    NSMutableArray *menuItems;
    
    pootEngine *queryManager;
    pootEngine *paymentManager;
    pootEngine *cardManager;
    pootEngine *tokenManager;
    
    NSDictionary *queryData;
    
    NSNumberFormatter *numFormatter;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    
    NSDictionary *cardData;
}
@end

@implementation colombia_Services_Step10

- (void)viewDidLoad {
    [super viewDidLoad];
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    menuItems = [[NSMutableArray alloc] init];
    
    for (NSDictionary *element in _serviceData[@"campos"]) {
        [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"field", kIdentifierKey, @"65", kHeightKey, element, kDescriptionKey, nil]];
        [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
    }
    
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"payButton", kIdentifierKey, @"74", kHeightKey, nil]];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) payment_Action:(id)sender
{
    queryManager = [[pootEngine alloc] init];
    [queryManager setDelegate:self];
    [queryManager setShowComments:developing];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSNumber numberWithInt:idApp] forKey:@"idAplicacion"];
    [params setObject:_serviceData[@"idProducto"] forKey:@"idProducto"];
    [params setObject:userData[kUserIDKey] forKey:@"idUsuario"];
    
    NSMutableArray *references = [[NSMutableArray alloc] init];
    for (int i = 0; i<[_serviceData[@"campos"] count]; i++) {
        NSMutableDictionary *element = [NSMutableDictionary dictionaryWithDictionary:_serviceData[@"campos"][i]];
        
        UITextField_Validations *field = [self getValidationTextFields][i];
        [element setObject:[field text] forKey:@"valor"];
        
        [references addObject:element];
    }
    
    [params setObject:references forKey:@"referencias"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:colHeader, @"Authorization", nil];
    
    [queryManager startJSONRequestWithURL:WSColombiaQuery withPost:JSONString andHeader:header];
    
    [self lockViewWithMessage:nil];
}

- (void)performPayment:(NSDictionary*)cardInfo
{
    cardData = cardInfo;
    
    tokenManager = [[pootEngine alloc] init];
    [tokenManager setDelegate:self];
    [tokenManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSNumber numberWithInt:idApp] forKey:@"idAplicacion"];
    [params setObject:@"0" forKey:@"idProducto"];
    [params setObject:@"0" forKey:@"idProveedor"];
    [params setObject:@"0" forKey:@"idRecarga"];
    [params setObject:@"0" forKey:@"idServicio"];
    [params setObject:@"0" forKey:@"idUsuario"];
    [params setObject:@"addcelBancolombia2014" forKey:@"password"];
    [params setObject:@"addcelBancolombia" forKey:@"usuario"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:colHeader, @"Authorization", nil];
    
    [tokenManager startJSONRequestWithURL:WSColombiaToken withPost:JSONString andHeader:header];
    
    [self lockViewWithMessage:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItems count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"%@ - %@", _serviceData[@"descripcion_servicio"], _serviceData[@"descripcion"]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuItems[indexPath.row][kIdentifierKey] forIndexPath:indexPath];
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"field"]) {
        UITextField_Validations *textfield = [[cell contentView] subviews][0];
        
        [textfield setPlaceholder:menuItems[indexPath.row][kDescriptionKey][@"nombre"]];
        
        if ([menuItems[indexPath.row][kDescriptionKey][@"tipo"] isEqualToString:@"number"]) {
            [textfield setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
        } else {
            [textfield setUpTextFieldAs:textFieldTypeName];
        }
        
        [textfield setMaxLength:[menuItems[indexPath.row][kDescriptionKey][@"len"] intValue]];
        
        [self addValidationTextField:textfield];
        [self addValidationTextFieldsToDelegate];
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"payButton"]) {
        UIButton *payButton = [[cell contentView] subviews][0];
        [payButton addTarget:self action:@selector(payment_Action:) forControlEvents:UIControlEventTouchUpInside];
        
      //  [self addShadowToView:payButton];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}






#pragma mark Manager
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == queryManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue] == 0) {
            queryData = [NSDictionary dictionaryWithDictionary:response];
            [self performSegueWithIdentifier:@"summary" sender:self];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
    
    
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
            }
        }
    }
    
    if (manager == tokenManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue]==0) { //CALL PAYMENT!!
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            paymentManager = [[pootEngine alloc] init];
            [paymentManager setDelegate:self];
            [paymentManager setShowComments:developing];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:queryData];
            
            [params setObject:queryData[@"idProducto"] forKey:@"idRecarga"];
            [params setObject:response[@"token"] forKey:@"token"];
            
            [params setObject:@"1" forKey:@"key"];
            [params setObject:cardData[@"idTarjeta"] forKey:@"idTarjeta"];
            [params setObject:userData[kUserIDKey] forKey:@"idUsuario"];
            [params setObject:userData[kUserLogin] forKey:@"login"];
            
            [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
            [params setObject:[[UIDevice currentDevice] model] forKey:@"modelo"];
            
            [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
            
            [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"cx"];
            [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"cy"];
            
            NSError *JSONError;
            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            
            NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:colHeader, @"Authorization", nil];
            
            [paymentManager startJSONRequestWithURL:WSColombiaPurchase withPost:JSONString andHeader:header];
            
            [self lockViewWithMessage:nil];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
    
    if (manager == paymentManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if (response) {
            [self performSegueWithIdentifier:@"result" sender:[NSDictionary dictionaryWithDictionary:response]];
        }
    }
}





#pragma mark segue handler
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"summary"]) {
        UINavigationController *nav = [segue destinationViewController];
        colombia_Services_Summary *nextView = [[nav viewControllers] firstObject];
        [nextView setDelegate:self];
        
        NSMutableString *references = [[NSMutableString alloc] init];
        for (NSDictionary *element in queryData[@"referencias"]) {
            [references appendString:[NSString stringWithFormat:@"%@: %@", element[@"nombre"], element[@"valor"]]];
            element == [queryData[@"referencias"] lastObject]?nil:[references appendString:@"\n"];
        }
        
        [nextView setMainString:references];
        [nextView setMainHeight:[NSString stringWithFormat:@"%lu", [queryData[@"referencias"] count]*26]];
        [nextView setFirstString:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryData[@"monto"] floatValue]]]];
        [nextView setSecondString:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryData[@"comision"] floatValue]]]];
        [nextView setThirdString:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryData[@"monto"] floatValue]]]];
    }
    
    if ([[segue identifier] isEqualToString:@"result"]) {
        colombia_TopUp_result *nextView = [segue destinationViewController];
        
        NSDictionary *data = sender;
        [nextView setResultData:data];
    }
}


#pragma Mark summary Handling

- (void)generalSummaryResponse:(NSDictionary *)response
{
    [self showCardOptions];
}

#pragma mark CARD MODULE

- (void) showCardOptions
{
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
     //   UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
       // [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
       
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view setType:viewType];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    [self performPayment:selectedCard];
}

#pragma mark end of CARD MODULE




























#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}
@end
