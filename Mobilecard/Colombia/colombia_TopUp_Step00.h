//
//  colombia_TopUp_Step00.h
//  Mobilecard
//
//  Created by David Poot on 11/4/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface colombia_TopUp_Step00 : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *serviceData;

@end

NS_ASSUME_NONNULL_END
