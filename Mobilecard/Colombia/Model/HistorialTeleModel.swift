
import Foundation

class HistorialTeleModel : NSObject{
    
    // si es 1 es transaccion si es 2 es header de mes año
    var type : String! = "1"
    var amount : String! = ""
    var day : String! = ""
    var hour : String! = ""
    var city : String! = ""
    var location : String! = ""
    var monthYear : String! = ""
    
    
    
    
    
    init(type : String , amount : String , day : String , hour : String , city: String , location : String , monthYear: String) {
        
        
      self.type = type
        self.amount = amount
        self.day = day
        self.hour = hour
        self.city = city
        self.location = location
        self.monthYear = monthYear
    }
  
    
    
    
    
}
