
import Foundation

class SoatModel : NSObject{
    
    
    let nombre :String!
    let cc : String!
    let telefono : String!
    let email : String!
    let marca : String!
    let linea : String!
    let modelo : String!
    let placa : String!
    let categoria : String!
    let total : Double!
    let vigenciaIni : String!
    let vigenciaFin : String!
    let seguroSoat : Double!
    
    
    
    
    
    init(nombre:String,cc:String,telefono:String,email:String,marca:String,linea:String,modelo:String,placa:String,categoria:String,total:Double,vigenciaIni:String,vigenciaFin:String,seguroSoat:Double) {
        
        
   self.nombre = nombre
        self.cc = cc
        self.telefono = telefono
        self.email = email
        self.marca = marca
        self.linea = linea
        self.modelo = modelo
        self.placa = placa
        self.categoria = categoria
        self.total = total
        self.vigenciaIni = vigenciaIni
        self.vigenciaFin = vigenciaFin
        self.seguroSoat = seguroSoat
    }
    
 
  
    
    
    
}
