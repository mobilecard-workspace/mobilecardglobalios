
import Foundation

class RecargaModel : NSObject{
    
    
    
    let id : Int!
    let codigo : String!
    let nombre : String!
    var imgcolor : String! = ""
    var imgwild : String! = ""
    var isSelected : BooleanLiteralType? = false
    var amount : String?
    
    
    
    
    
    init(id : Int, codigo: String, nombre : String , imgcolor : String , imgwild: String) {
        
        
        self.id = id
        self.codigo = codigo
        self.nombre = nombre
        self.imgcolor = imgcolor
        self.imgwild = imgwild
    }
    
    init(id : Int , codigo : String , nombre: String){
        self.id = id
        self.codigo = codigo
        self.nombre = nombre
        
    }
    
    
    
    
}
