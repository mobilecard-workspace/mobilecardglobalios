//
//  colombia_TopUp_Step10.m
//  Mobilecard
//
//  Created by David Poot on 11/4/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "colombia_TopUp_Step10.h"
#import "generalCollectionCell.h"
#import "colombia_TopUp_result.h"

@interface colombia_TopUp_Step10 ()
{
    NSNumberFormatter *numFormat;
    
    int selectedID;
    
    NSMutableArray *cellArray;
    NSMutableArray *menuItems;
    
    UILabel *feeLabel;
    
    NSArray *amountDataSource;
    
    pootEngine *cardManager;
    pootEngine *tokenManager;
    pootEngine *paymentManager;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    
    NSDictionary *cardData;
}
@end

@implementation colombia_TopUp_Step10

- (void)viewDidLoad {
    [super viewDidLoad];
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    numFormat = [[NSNumberFormatter alloc] init];
    [numFormat setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormat setCurrencySymbol:@"$"];
    
    selectedID = 0;
    
    cellArray = [[NSMutableArray alloc] init];
    menuItems = [[NSMutableArray alloc] init];
    amountDataSource = _serviceData[@"montos"];
    
    menuItems = [[NSMutableArray alloc] init];
    
    for (NSDictionary *element in _servicesData[@"campos"]) {
        [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"field", kIdentifierKey, element, kDescriptionKey, @"65", kHeightKey, nil]];
        [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
    }
    
    
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"amounts", kIdentifierKey, [NSString stringWithFormat:@"%lu", 10*2+(amountDataSource.count/2)*10+50+50*(amountDataSource.count/2)], kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"fee", kIdentifierKey, @"41", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"payButton", kIdentifierKey, @"74", kHeightKey, nil]];
}

- (void)viewDidAppear:(BOOL)animated
{
    UIButton *button = [[UIButton alloc] init];
    [button setTag:selectedID];
    [self cellSelected:button];
    
    [super viewDidAppear:animated];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateAllView
{
    [feeLabel setText:[NSString stringWithFormat:NSLocalizedString(@"Comisión %@", nil), [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountDataSource[selectedID][@"comision"] floatValue]]]]];
}

- (void)proceedWithPayment:(id)sender
{
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    [self performSegueWithIdentifier:@"summary" sender:nil];
}

- (void)performPayment:(NSDictionary*)cardInfo
{
    cardData = cardInfo;
    
    tokenManager = [[pootEngine alloc] init];
    [tokenManager setDelegate:self];
    [tokenManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSNumber numberWithInt:idApp] forKey:@"idAplicacion"];
    [params setObject:@"0" forKey:@"idProducto"];
    [params setObject:@"0" forKey:@"idProveedor"];
    [params setObject:@"0" forKey:@"idRecarga"];
    [params setObject:@"0" forKey:@"idServicio"];
    [params setObject:@"0" forKey:@"idUsuario"];
    [params setObject:@"addcelBancolombia2014" forKey:@"password"];
    [params setObject:@"addcelBancolombia" forKey:@"usuario"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:colHeader, @"Authorization", nil];
    
    [tokenManager startJSONRequestWithURL:WSColombiaToken withPost:JSONString andHeader:header];
    
    [self lockViewWithMessage:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"%@ - %@", _servicesData[@"descripcion_servicio"], _serviceData[@"label"]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuItems[indexPath.row][kIdentifierKey]];
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"field"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UITextField_Validations class]]) {
                UITextField_Validations *textfield = element;
                
                if ([menuItems[indexPath.row][kDescriptionKey][@"tipo"] isEqualToString:@"number"]) {
                    [textfield setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
                } else {
                    [textfield setUpTextFieldAs:textFieldTypeName];
                }
                
                [textfield setPlaceholder:menuItems[indexPath.row][kDescriptionKey][@"nombre"]];
                [textfield setMaxLength:[menuItems[indexPath.row][kDescriptionKey][@"len"] intValue]];
                
                [self addValidationTextField:textfield];
                [self addValidationTextFieldsToDelegate];
            }
        }
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"amounts"]) {
        _amountCollection = [[cell contentView] subviews][0];
        
        [_amountCollection setDelegate:self];
        [_amountCollection setDataSource:self];
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"fee"]) {
        UILabel *label = [[cell contentView] subviews][0];
        
        feeLabel = label;
        
        [self updateAllView];
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"payButton"]) {
        UIButton *payButton = [[cell contentView] subviews][0];
        
        [payButton addTarget:self action:@selector(proceedWithPayment:) forControlEvents:UIControlEventTouchUpInside];
        
       // [self addShadowToView:payButton];
    }
    
    return cell;
}


#pragma mark setting up Collection view

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [amountDataSource count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width-10)/2, 50);
}

- (generalCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"Cell_amount";
    
    generalCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.tag = indexPath.row;
    cell.cellButton.tag = indexPath.row;
    [cell.cellText setText:[NSString stringWithFormat:@"%@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountDataSource[indexPath.row][@"monto"] floatValue]]]]];
    [cell.cellButton setEnabled:YES];
    [cell.cellButton addTarget:self action:@selector(cellSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    for (generalCollectionCell *cellInArray in cellArray) {
        if (cellInArray == cell) {
            return cell;
        }
    }
    
    [cellArray addObject:cell];
    
    return cell;
}

- (void) cellSelected:(id)sender
{
    UIButton *cellButton = sender;
    
    selectedID = (int)cellButton.tag;
    
    for (generalCollectionCell *cell in cellArray) {
        if (cell.tag == cellButton.tag) {
            [cell selectCell];
        } else {
            [cell deSelectCell];
        }
    }
    
    [self updateAllView];
}

#pragma mark segue handing
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"summary"]) {
        UINavigationController *nav = [segue destinationViewController];
        generalSummary *nextView = [[nav viewControllers] firstObject];
        [nextView setDelegate:self];
        
        [nextView setMainPlaceholderString:[[self getValidationTextFields][0] placeholder]];
        [nextView setMainString:[[self getValidationTextFields][0] text]];
        [nextView setFirstString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountDataSource[selectedID][@"monto"] floatValue]]], @""]];
        [nextView setSecondString:nil];
        [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountDataSource[selectedID][@"comision"] floatValue]]], @""]];
        [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountDataSource[selectedID][@"monto"] floatValue]+[amountDataSource[selectedID][@"comision"] floatValue]]], @""]];
        
        [nextView setConvertToUSD:NO];
    }
    
    if ([[segue identifier] isEqualToString:@"result"]) {
        colombia_TopUp_result *nextView = [segue destinationViewController];
        
        NSDictionary *data = sender;
        [nextView setResultData:data];
    }
}

#pragma mark general Summary handler

- (void)generalSummaryResponse:(NSDictionary *)response
{
    [self showCardOptions];
}


#pragma mark CARD MODULE

- (void) showCardOptions
{
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
      //  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
       // [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view setType:viewType];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    [self performPayment:selectedCard];
}

#pragma mark end of CARD MODULE


- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
            }
        }
    }
    
    if (manager == tokenManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue]==0) { //CALL PAYMENT!!
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            paymentManager = [[pootEngine alloc] init];
            [paymentManager setDelegate:self];
            [paymentManager setShowComments:developing];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            
            [params setObject:amountDataSource[selectedID][@"comision"] forKey:@"comision"];
            [params setObject:[NSNumber numberWithInt:idApp] forKey:@"idAplicacion"];
            [params setObject:amountDataSource[selectedID][@"idProducto"] forKey:@"idProducto"];
            [params setObject:amountDataSource[selectedID][@"idRecarga"] forKey:@"idRecarga"];
            [params setObject:amountDataSource[selectedID][@"monto"] forKey:@"monto"];
            [params setObject:response[@"token"] forKey:@"token"];
            
            NSMutableArray *references = [[NSMutableArray alloc] init];
            for (int i = 0; i<[_servicesData[@"campos"] count]; i++) {
                NSMutableDictionary *element = [NSMutableDictionary dictionaryWithDictionary:_servicesData[@"campos"][i]];
                
                UITextField_Validations *field = [self getValidationTextFields][i];
                [element setObject:[field text] forKey:@"valor"];
                
                [references addObject:element];
            }
            
            [params setObject:references forKey:@"referencias"];
            [params setObject:@"1" forKey:@"key"];
            [params setObject:@"0" forKey:@"valorImpuesto"];
            
            
            [params setObject:cardData[@"idTarjeta"] forKey:@"idTarjeta"];
            [params setObject:userData[kUserIDKey] forKey:@"idUsuario"];
            [params setObject:userData[kUserLogin] forKey:@"login"];
            
            [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
            [params setObject:[[UIDevice currentDevice] model] forKey:@"modelo"];
            
            [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
            
            [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"cx"];
            [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"cy"];
            
            NSError *JSONError;
            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            
            NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:colHeader, @"Authorization", nil];
            
            [paymentManager startJSONRequestWithURL:WSColombiaPurchase withPost:JSONString andHeader:header];
            
            [self lockViewWithMessage:nil];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
    
    if (manager == paymentManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if (response) {
            [self performSegueWithIdentifier:@"result" sender:[NSDictionary dictionaryWithDictionary:response]];
        }
    }
}


























#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}
@end
