//
//  colombia_Services_Summary.h
//  Mobilecard
//
//  Created by David Poot on 11/5/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol generalSummaryDelegate

@optional
- (void)generalSummaryResponse:(NSDictionary*)response;

@end

@interface colombia_Services_Summary : mT_commonTableViewController

@property (assign, nonatomic) id <NSObject, generalSummaryDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@property (weak, nonatomic) IBOutlet UITextView *mainText;
@property (strong, nonatomic) NSString *mainString;
@property (strong, nonatomic) NSString *mainHeight;

@property (strong, nonatomic) NSString *mainPlaceholderString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *firstText;
@property (strong, nonatomic) NSString *firstString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *secondText;
@property (strong, nonatomic) NSString *secondString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *thirdText;
@property (strong, nonatomic) NSString *thirdString;

@end

NS_ASSUME_NONNULL_END
