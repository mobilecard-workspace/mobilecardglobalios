//
//  colombia_Services_Step10.h
//  Mobilecard
//
//  Created by David Poot on 11/5/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "colombia_Services_Summary.h"
#import "Wallet_Ctrl.h"

#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface colombia_Services_Step10 : mT_commonTableViewController <generalSummaryDelegate, cardSelectionDelegate, updateCardDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) NSDictionary *serviceData;

@end

NS_ASSUME_NONNULL_END
