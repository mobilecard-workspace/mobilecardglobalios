//
//  colombia_Services_Step00.m
//  Mobilecard
//
//  Created by David Poot on 11/4/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "colombia_Services_Step00.h"
#import "colombia_Services_Step10.h"

@interface colombia_Services_Step00 ()
{
    pootEngine *servicesManager;
    
    NSMutableArray *tableData;
}
@end

@implementation colombia_Services_Step00

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tableData = [[NSMutableArray alloc] init];
    
    [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"70", kHeightKey, nil]];
    for (NSDictionary *element in _serviceData[@"SERVICIOS"]) {
        [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"data", kIdentifierKey, @"90", kHeightKey, element, kDescriptionKey, nil]];
        [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
    }
    
    [self.tableView reloadData];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableData[indexPath.row][kIdentifierKey] forIndexPath:indexPath];
    
    if ([tableData[indexPath.row][kIdentifierKey] isEqualToString:@"data"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *title = element;
                [title setText:tableData[indexPath.row][kDescriptionKey][@"descripcion"]];
            }
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableData[indexPath.row][kHeightKey] floatValue];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"services_step10" sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"services_step10"]) {
        colombia_Services_Step10 *nextView = [segue destinationViewController];
        
        NSIndexPath *indexpath = sender;
        [nextView setServiceData:tableData[indexpath.row][kDescriptionKey]];
    }
}

@end
