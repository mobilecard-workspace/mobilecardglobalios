

import UIKit

class HistorialTeleTableCell: UITableViewCell {

    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var hourLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    
}
