

import UIKit

class PaquetesCOTableCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var buttonBackground: UIButton!
    
}
