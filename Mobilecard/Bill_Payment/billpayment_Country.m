//
//  billpayment_Country.m
//  Mobilecard
//
//  Created by David Poot on 10/29/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "billpayment_Country.h"
#import "billpayment_Selection.h"

@interface billpayment_Country ()

@end

@implementation billpayment_Country

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    switch ([userData[kUserIdCountry] intValue]) {
        case 1: //MEXICO
        {
            return 3;
        }
            break;
        case 3:
        {
            return 4;
        }
            break;
        default:
        {
            return 4;
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    switch (cell.tag) {
        case 1: [self performSegueWithIdentifier:@"mexicoServices" sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:@"usaServices" sender:self];
            break;
        default:
            break;
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"mexicoServices"]) {
        billpayment_Selection *nextView = [segue destinationViewController];
        [nextView setType:viewTypeServicios];
    }
}

@end
