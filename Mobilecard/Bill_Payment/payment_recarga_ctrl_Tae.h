//
//  payment_recarga_ctrl.h
//  Mobilecard
//
//  Created by David Poot on 11/1/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import <ContactsUI/ContactsUI.h>
#import "Wallet_Ctrl.h"
#import "generalSummary.h"
#import <CoreLocation/CoreLocation.h>
//#import <TrustDefender/TrustDefender.h>
#import "Secure3D_Ctrl.h"

@interface payment_recarga_ctrl_Tae : mT_commonTableViewController <UITextFieldDelegate, pootEngineDelegate, CNContactPickerDelegate, UIActionSheetDelegate, cardSelectionDelegate, updateCardDelegate, generalSummaryDelegate, CLLocationManagerDelegate, Secure3DDelegate>

@property (nonatomic, assign) serviceType type;

@property (strong, nonatomic) NSMutableDictionary *dataSource;
@property (strong, nonatomic) NSArray *amountDataSource;
@property (strong, nonatomic) NSDictionary *countryData;

@property (strong, nonatomic) NSDictionary *favoriteDataSource;

@property (weak, nonatomic) IBOutlet UITextField_Validations *numberText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *numberConfirmText;

@property (weak, nonatomic) IBOutlet UILabel *favoriteLabel;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *contactListButton;

@property (weak, nonatomic) IBOutlet UICollectionView *amountCollection;
@property (weak, nonatomic) IBOutlet UILabel *comissionLabel;
@property (weak, nonatomic) IBOutlet UIButton *payButton;

@end


@interface payment_recarga_viewCtrl_Tae : mT_commonController <UICollectionViewDelegate, UICollectionViewDataSource>

@end
