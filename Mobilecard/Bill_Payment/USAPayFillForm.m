//
//  USAPayFillForm.m
//  MobileCard_X
//
//  Created by David Poot on 5/9/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "USAPayFillForm.h"

@interface USAPayFillForm ()
{
    pootEngine *decrypter;
    pootEngine *statesManager;
    pootEngine *paymentManager;
    
    NSDictionary *confirmationResult;
}
@end

@implementation USAPayFillForm

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializePickersForView:self.view];
    
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    //[_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_cityText setUpTextFieldAs:textFieldTypeCity];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    [_cvvText setUpTextFieldAs:textFieldTypeCvv2];
    
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_zipText];
    [self addValidationTextField:_cvvText];
    
    [self addValidationTextFieldsToDelegate];
    
    decrypter = [[pootEngine alloc] init];
    [decrypter setShowComments:developing];
    
    [_infoText setText:[NSString stringWithFormat:NSLocalizedString(@"Servicio: %@\nCuenta: %@\nSucursal: %@\nMonto: %@\nComisión: %@\nForma de pago: %@", nil), _params[@"nombreServicio"], _params[@"referencia"], _params[@"ciudadServicio"], _paymentData[@"amount"], _paymentData[@"fee"], [self maskCard:(NSString*)[decrypter decryptedStringOfString:_selectedCard[@"pan"] withSensitive:NO]]]];
    
    statesManager = [[pootEngine alloc] init];
    [statesManager setDelegate:self];
    [statesManager setShowComments:developing];
    
    [statesManager startRequestWithURL:[NSString stringWithFormat:@"%@/%@/estados", walletManagementURL, @"3"]];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*)maskCard:(NSString*)cardPan
{
    NSMutableString *maskedString = [[NSMutableString alloc] init];
    
    for (int i=0; i<[cardPan length]; i++) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 12:
            case 13:
            case 14:
            case 15:
            {
                [maskedString appendString:[cardPan substringWithRange:NSMakeRange(i, 1)]];
            }
                break;
                
            default:
            {
                [maskedString appendString:@"●"];
            }
                break;
        }
    }
    
    return maskedString;
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)continue_Action:(id)sender
{
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        paymentManager = [[pootEngine alloc] init];
        [paymentManager setDelegate:self];
        [paymentManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:_params];
        
        [params setObject:[_cityText text] forKey:@"ciudadUsuario"];
        [params setObject:[_zipText text] forKey:@"cpUsuario"];
        [params setObject:[_addressText text] forKey:@"direccionUsuario"];
        [params setObject:[_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]] forKey:@"estadoUsuario"];
        [params setObject:[decrypter encryptJSONString:[_cvvText text] withPassword:nil] forKey:@"codigo"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"%@", JSONString];
        
        [paymentManager startJSONRequestWithURL:ACIPaymentNoData withPost:post];
        
        [self lockViewWithMessage:nil];
    }
}


#pragma Mark pootEngine Handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if ( manager == statesManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0) {
            [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
            [_stateText setInfoArray:response[@"estados"]];
            [_stateText setIdLabel:@"id"];
            [_stateText setDescriptionLabel:@"nombre"];
            
            if ([userData[@"usrIdEstado"] intValue]>=33) {
                [_stateText getSelectedIDWithID:[userData[@"usrIdEstado"] intValue]];
                [_stateText setText:[_stateText infoArray][[_stateText selectedID]][[_stateText descriptionLabel]]];
                [_stateText disable];
            }
            
            if ([userData[@"usrCiudad"] length]>0) {
                [_cityText setText:userData[@"usrCiudad"]];
            }
            
            if ([userData[@"usrCp"] intValue]>0) {
                [_zipText setText:userData[@"usrCp"]];
            }
            
            if ([userData[@"usrDireccion"] length]>0) {
                [_addressText setText:userData[@"usrDireccion"]];
            }
            
            if ([[decrypter decryptedStringOfString:_selectedCard[@"codigo"] withSensitive:NO] length]>0) {
                [_cvvText setText:[decrypter decryptedStringOfString:_selectedCard[@"codigo"] withSensitive:NO]];
                [_cvvText disable];
            }
        }
    }
    
    if (manager == paymentManager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        
        response = [self cleanDictionary:response];
        
        confirmationResult = response;
        
        [self performSegueWithIdentifier:@"result" sender:nil];
    }
}

#pragma mark Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"result"]) {
        orderResult_Ctrl *nextView = [segue destinationViewController];
        [nextView setType:paymentTypeACI];
        [nextView setConfirmationResult:confirmationResult];
    }
}

@end
