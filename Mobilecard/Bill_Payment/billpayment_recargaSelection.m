//
//  billpayment_recargaSelection.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/21/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "billpayment_recargaSelection.h"

@interface billpayment_recargaSelection (){
    
    pootEngine *categoriesManager;
    NSMutableArray *categoriesInfo;
    
    NSMutableArray *CategoriesArray;
    
    NSMutableArray *cellsArray;
}

@end

@implementation billpayment_recargaSelection

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CategoriesArray = [[NSMutableArray alloc] init];
    valueIndexpath=0;
    
    categoriesManager = [[pootEngine alloc] init];
    [categoriesManager setDelegate:self];
    [categoriesManager setShowComments:developing];

    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
    [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
    [params setObject:userData[kUserIdCountry] forKey:kUserIdCountry];

    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];

    [categoriesManager startJSONRequestWithURL:WSGetCatalogRecargas withPost:JSONString];

    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
- (IBAction)back_Action:(id)sender {
    
    
    switch (_type) {
        case viewTypeRecargas:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case viewTypeServicios:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
    }
}
- (void)loadItemsOfView
{

    cellsArray = [[NSMutableArray alloc] init];

    
    [cellsArray addObject:[NSDictionary  dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"70", kHeightKey, nil]];
    
    for (NSDictionary *element in categoriesInfo) {
        [cellsArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"generic", kIdentifierKey, element, kDetailsKey, @"100", kHeightKey, nil]];
        [cellsArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"", kDetailsKey, @"20", kHeightKey, nil]];
    }
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cellsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [cellsArray[indexPath.row][kHeightKey] floatValue];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = cellsArray[indexPath.row][kIdentifierKey];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if ([identifier isEqualToString:@"generic"]) {
        RecargasModel * item =[CategoriesArray objectAtIndex:valueIndexpath];
        
        UIImageView * imgRecarga = (UIImageView *)[cell viewWithTag:1];
        imgRecarga.image = [UIImage imageNamed:item.description_recarga];
        
        UILabel *lbl_description = (UILabel *)[cell viewWithTag:2];
        [lbl_description setText:item.description_recarga];
        
        valueIndexpath++;;
    }
     return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==1) {
         SelectItem =0;
    }else if (indexPath.row == 3){
         SelectItem =1;
    }
   
    [self performSegueWithIdentifier:@"side_topup_step" sender:self];
    
}
#pragma Mark PootEngine Handling

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    if (manager == categoriesManager) {
        
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue] == 0) {
            switch (_type) { 
                case viewTypeRecargas:
                {
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    if ([userData[kUserIdCountry] intValue]==1) {
                        categoriesInfo = [NSMutableArray arrayWithArray:response[@"recargas"]];
                        NSArray *Categorias = [json objectForKey:@"recargas"];
                        for (NSDictionary *jsonObjectCategoria in Categorias) {
                            
                            RecargasModel *item =[[RecargasModel alloc] init];
        
                            item.id_recarga=[[jsonObjectCategoria objectForKey:@"id"] integerValue];
                            item.description_recarga =[jsonObjectCategoria objectForKey:@"descripcion"];
                            item.esAntad =[[jsonObjectCategoria objectForKey:@"esAntad"] boolValue];
                            
                
                            [CategoriesArray addObject:item];
                        }
                    } else {
                        categoriesInfo = [NSMutableArray arrayWithArray:response[@"recargas"]];
                        
                        [categoriesInfo removeObjectAtIndex:1];
                        
                        NSArray *Categorias = [json objectForKey:@"recargas"];
                        for (NSDictionary *jsonObjectCategoria in Categorias) {
                            
                            RecargasModel *item =[[RecargasModel alloc] init];
                            
                            item.id_recarga=[[jsonObjectCategoria objectForKey:@"id"] integerValue];
                            item.description_recarga =[jsonObjectCategoria objectForKey:@"descripcion"];
                            item.esAntad =[[jsonObjectCategoria objectForKey:@"esAntad"] boolValue];
                            
                            
                            [CategoriesArray addObject:item];
                        }
                        [CategoriesArray removeObjectAtIndex:1];

                    }
                }
                    break;
                    
                case viewTypeServicios:
                {
                    categoriesInfo = [NSMutableArray arrayWithArray:response[@"categorias"]];
                    
                   
                }
                    break;
            }
            [self loadItemsOfView];
            [self.tableView reloadData];
        }
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"side_topup_step"]) {
        billpayment_Selection *nextView = [segue destinationViewController];
        [nextView setType:viewTypeRecargas];
        [nextView setId_Categoria:SelectItem];
        [nextView setCategories:categoriesInfo];
    }
}

@end
