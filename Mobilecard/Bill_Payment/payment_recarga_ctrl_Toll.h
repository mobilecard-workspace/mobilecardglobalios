//
//  payment_recarga_ctrl.h
//  Mobilecard
//
//  Created by David Poot on 11/1/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import <ContactsUI/ContactsUI.h>
#import "Wallet_Ctrl.h"
#import "generalSummary.h"
#import <CoreLocation/CoreLocation.h>

@interface payment_recarga_ctrl_Toll : mT_commonTableViewController <UITextFieldDelegate, pootEngineDelegate, CNContactPickerDelegate, UIActionSheetDelegate, cardSelectionDelegate, updateCardDelegate, generalSummaryDelegate, CLLocationManagerDelegate>

@property (nonatomic, assign) serviceType type;

@property (strong, nonatomic) NSMutableDictionary *dataSource;
@property (strong, nonatomic) NSArray *amountDataSource;
@property (strong, nonatomic) NSDictionary *countryData;

@property (strong, nonatomic) NSDictionary *favoriteDataSource;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segControl;
@property (weak, nonatomic) IBOutlet UILabel *tagBalanceLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *selectedTagText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *tagNumberText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *digitText;
@property (weak, nonatomic) IBOutlet UILabel *comissionLabel;
@property (weak, nonatomic) IBOutlet UIView *line_1;
@property (weak, nonatomic) IBOutlet UIView *line_2;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton_1;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton_2;
@property (weak, nonatomic) IBOutlet UILabel *favoriteLabel_1;
@property (weak, nonatomic) IBOutlet UILabel *favoriteLabel_2;
@property (weak, nonatomic) IBOutlet UIButton *deleteTagButton;
@property (weak, nonatomic) IBOutlet UIButton *addTagButton;
@property (weak, nonatomic) IBOutlet UICollectionView *amount_collection;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@end


@interface payment_recarga_viewCtrl_Toll : mT_commonController <UICollectionViewDelegate, UICollectionViewDataSource>

@end
