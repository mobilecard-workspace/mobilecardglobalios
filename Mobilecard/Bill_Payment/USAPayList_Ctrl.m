//
//  USAPayList_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 12/19/16.
//  Copyright © 2016 David Poot. All rights reserved.
//

#import "USAPayList_Ctrl.h"
#import "payServices_Ctrl.h"
#import "MC_Colors.h"

@interface USAPayList_Ctrl ()
{
    pootEngine *servicesManager;
    pootEngine *addressManager;
    
    NSMutableArray *servicesInfo;
    
    NSArray *addressesInfo;
    NSDictionary *serviceInfo;
}

@end

@implementation USAPayList_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    servicesInfo = [[NSMutableArray alloc] init];
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;
    _searchController.searchBar.delegate = self;
    
    [self.view addSubview:_searchController.searchBar];
    
    //[_servicesTable setTableHeaderView:_searchController.searchBar];
    
    self.definesPresentationContext = YES;
    
  //  [self addShadowToView:_servicesTableContainer];
  //  [self addShadowToView:_searchController.searchBar];
    
    servicesManager = [[pootEngine alloc] init];
    [servicesManager setDelegate:self];
    [servicesManager setShowComments:developing];
    
    [servicesManager startWithoutPostRequestWithURL:ACIServicesURL];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

- (NSArray *)searchResults {
    
    NSString *searchString = _searchController.searchBar.text;
    if (searchString.length > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[c] %@", searchString];
        return [servicesInfo filteredArrayUsingPredicate:predicate];
    }
    else {
        return servicesInfo;
    }
    
    return @[];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTranslucent:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"payment"]) {
        USAPayCheckOut *nextView = segue.destinationViewController;
        [nextView setAddressesInfo:addressesInfo];
        [nextView setServiceInfo:serviceInfo];
    }
}

#pragma Mark UITableView implementation

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_searchController.isActive && _searchController.searchBar.text.length > 0) {
        return [[self searchResults] count];
    }
    else {
        return [servicesInfo count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    NSString *titletext;
    
    if (_searchController.isActive && (![_searchController.searchBar.text isEqualToString:@""])) {
        titletext = [[self searchResults] objectAtIndex:indexPath.row][@"name"];
    }
    else {
        titletext = [servicesInfo objectAtIndex:indexPath.row][@"name"];
    }
    
    [cell.textLabel setText:titletext];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    addressManager = [[pootEngine alloc] init];
    [addressManager setDelegate:self];
    [addressManager setShowComments:developing];
    
    if (_searchController.isActive && _searchController.searchBar.text.length > 0) {
        [addressManager startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@%@/addresses", ACIAddressURL, [[self searchResults] objectAtIndex:indexPath.row][kIDKey]]];
        serviceInfo = [[self searchResults] objectAtIndex:indexPath.row];
    } else {
        [addressManager startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@%@/addresses", ACIAddressURL, [servicesInfo objectAtIndex:indexPath.row][kIDKey]]];
        serviceInfo = [servicesInfo objectAtIndex:indexPath.row];
    }
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    
    [_servicesTable deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 26;
}

#pragma mark - UISearchControllerDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    [_servicesTable reloadData];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//PootEngine

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (manager == servicesManager) {
        NSArray *response = (NSArray*)json;
        servicesInfo = [[NSMutableArray alloc] initWithArray:response];
        [_servicesTable reloadData];
    }
    
    if (manager == addressManager) {
        NSArray *response = (NSArray*)json;
        
        addressesInfo = response;
        [self performSegueWithIdentifier:@"payment" sender:nil];
    }
}

@end
