//
//  USAPayCheckOut.m
//  MobileCard_X
//
//  Created by David Poot on 5/9/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "USAPayCheckOut.h"


@interface USAPayCheckOut ()
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    
    float finalAmount;
    float finalFee;
    
    NSNumberFormatter *numFormatter;
    
    NSDictionary *currentCard;
    NSDictionary *confirmationResult;
    
    pootEngine *cardManager;
    pootEngine *checkManager;
    pootEngine *directPayManager;
    pootEngine *indirectPayManager;
    
    NSMutableDictionary *params;
}

@end

@implementation USAPayCheckOut

- (void)viewDidLoad {
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    
    [super viewDidLoad];
    
  //  [self addShadowToView:_payButton];
    
    [self initializePickersForView:self.tableView];
    
    [self setTitle:_serviceInfo[@"name"]];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [_accountNumberText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_amountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_amountText setKeyboardType:UIKeyboardTypeDecimalPad];
    [_amountText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]];
    
    if ([_addressesInfo count]>0) {
        [_addressText setUpTextFieldAs:textFieldTypeRequiredCombo];
        
        [self addValidationTextField:_addressText];
        
        [_addressText setInfoArray:_addressesInfo];
        [_addressText setIdLabel:@"street"];
        [_addressText setDescriptionLabel:@"city"];
    }
    
    [self addValidationTextField:_accountNumberText];
    [self addValidationTextField:_amountText];
    
    [self addValidationTextFieldsToDelegate];
}

- (void)viewDidLayoutSubviews
{
    UITableViewCell *cell = [_tableViewData cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    [cell setHidden:!([_addressesInfo count]>0)];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return  70;
            break;
        case 1:
        {
            return [_addressesInfo count]>0?70:0;
        }
            break;
        case 2:
            return [_addressesInfo count]>0?20:0;;
            break;
        case 3:
            return 140;
            break;
        case 4:
            return 130;
            break;
            
        default:
            return 65;
            break;
    }
}

- (BOOL)textField:(UITextField_Validations *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _amountText) {
        finalAmount = [[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue];
        finalFee = [[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue]*[_serviceInfo[@"pctComision"] floatValue];
        
        [_feeText setText:[NSString stringWithFormat:NSLocalizedString(@"Comisión: %@", nil), [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalFee]]]];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pay_Action:(id)sender {
    [self dismissAllTextFields];
    
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        @try {
            cardManager = [[pootEngine alloc] init];
            [cardManager setDelegate:self];
            [cardManager setShowComments:developing];
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
            
         //   UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
          //  [alertMsg show];
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

#pragma Mark pootEngine

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (cardManager == manager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0) {
            if ([response[kUserCardArray] count]>0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == checkManager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        
        response = [self cleanDictionary:response];
        
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        params = [[NSMutableDictionary alloc] init];
        
        [params setObject:[_addressesInfo count]>0?[_addressText infoArray][[_addressText selectedID]][@"city"]:@"" forKey:@"ciudadServicio"];
        [params setObject:[_addressesInfo count]>0?[_addressText infoArray][[_addressText selectedID]][@"street"]:@"" forKey:@"direccionServicio"];
        [params setObject:_serviceInfo[@"id"] forKey:@"idServicio"];
        [params setObject:_serviceInfo[@"name"] forKey:@"nombreServicio"];
        [params setObject:[_accountNumberText text] forKey:@"referencia"];
        [params setObject:[_amountText text] forKey:@"monto"];
        [params setObject:currentCard[@"idTarjeta"] forKey:@"idTarjeta"];
        [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
        
        [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
        [params setObject:[self platformString] forKey:@"modelo"];
        [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
        [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"wkey"];
        
        [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"lat"];
        [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"lon"];
        
        [params setObject:(NSLocalizedString(@"lang", nil)) forKey:kLanguage];
        
        if ([response[kIDError] intValue]==0) {
            directPayManager = [[pootEngine alloc] init];
            [directPayManager setDelegate:self];
            [directPayManager setShowComments:developing];
            
            NSString *post = nil;
            NSError *JSONError;
            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            post = [NSString stringWithFormat:@"%@", JSONString];
            
            [directPayManager startJSONRequestWithURL:ACIPayment withPost:post];
            
            [self lockViewWithMessage:nil];
            
        } else if ([response[kIDError] intValue]==100){
            [self performSegueWithIdentifier:@"fillForm" sender:nil];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == directPayManager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        
        response = [self cleanDictionary:response];
        
        confirmationResult = response;
        
        [self performSegueWithIdentifier:@"result" sender:nil];
    }
}

#pragma Mark storyBoards
- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view setType:viewType];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

#pragma MARK Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"result"]) {
        orderResult_Ctrl *nextView = [segue destinationViewController];
        [nextView setType:paymentTypeACI];
        [nextView setConfirmationResult:confirmationResult];
    }
    
    if ([[segue identifier] isEqualToString:@"fillForm"]) {
        USAPayFillForm *nextView = [segue destinationViewController];
        [nextView setParams:params];
        [nextView setSelectedCard:currentCard];
        NSMutableDictionary *paymentData = [[NSMutableDictionary alloc] init];
        
        [paymentData setObject:[_amountText text] forKey:@"amount"];
        [paymentData setObject:[NSString stringWithFormat:@"%f", finalFee] forKey:@"fee"];
        
        [nextView setPaymentData:paymentData];
    }
}

#pragma Mark CARD SELECTION HANDLER

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];

    checkManager = [[pootEngine alloc] init];
    [checkManager setDelegate:self];
    [checkManager setShowComments:developing];
    
    currentCard = selectedCard;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:selectedCard[@"idTarjeta"] forKey:@"idTarjeta"];
    [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"%@", JSONString];
    
    [checkManager startJSONRequestWithURL:ACICheckUserData withPost:post];
    
    [self lockViewWithMessage:nil];
}



























#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}

@end
