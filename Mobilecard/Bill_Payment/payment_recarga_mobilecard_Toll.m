//
//  payment_recarga_ctrl.m
//  Mobilecard
//
//  Created by David Poot on 11/1/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "payment_recarga_mobilecard_Toll.h"
#import "generalCollectionCell.h"
#import <Mobilecard-Swift.h>
#import "Secure3D_Ctrl.h"

@interface payment_recarga_mobilecard_Toll ()

@end

@implementation payment_recarga_mobilecard_Toll
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    
    NSNumberFormatter *numFormat;
    
    NSMutableArray *cellArray;
    NSMutableArray *menuItems;
    
    float currentBalanace;
    
    int selectedID;
    
    pootEngine *tagsManager;
    pootEngine *cardManager;
    pootEngine *balanceManager;
    
    NSMutableArray *userTagList;
    
    BOOL selectTag;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _usuarioText.text = DataUser.EMAIL;
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    selectTag = NO;
    
    cellArray = [[NSMutableArray alloc] init];
    
    
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
    
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"imagenMobileTag", kIdentifierKey, @"90", kHeightKey, nil]];
    
    
    
     [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
    
     [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"usuarioMobilecard", kIdentifierKey, @"56", kHeightKey, nil]];
    
    [_usuarioText setUpTextFieldAs:textFieldTypeName];
    [self addValidationTextField:_usuarioText];
    [_usuarioText setUpTextFieldAs:textFieldTypeName];
    [self addValidationTextField:_usuarioText];
    
  //  [_segControl setTitle:NSLocalizedString(@"select_save", nil) forSegmentAtIndex:0];
   // [_segControl setTitle:NSLocalizedString(@"new", nil) forSegmentAtIndex:1];
    
    switch (_type) {
        case serviceTypeIAVE:
        case serviceTypePASE:
        {
            menuItems = [[NSMutableArray alloc] init];
          //  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"tagTypeSelection", kIdentifierKey, @"40", kHeightKey, nil]];
         //   [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"selectedTag", kIdentifierKey, @"125", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
         //   [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"tagNumber", kIdentifierKey, @"56", kHeightKey, nil]];
          //  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
          //  [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"digit", kIdentifierKey, @"125", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"amounts", kIdentifierKey, @"200", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"comission", kIdentifierKey, @"41", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"payButton", kIdentifierKey, @"74", kHeightKey, nil]];
        }
            break;
            
        default:
        {
            menuItems = [[NSMutableArray alloc] init];
            
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"phoneNumber", kIdentifierKey, @"56", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
         //   [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"confirmPhoneNumber", kIdentifierKey, @"125", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"contactList", kIdentifierKey, @"40", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"amounts", kIdentifierKey, @"200", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"comission", kIdentifierKey, @"41", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"payButton", kIdentifierKey, @"74", kHeightKey, nil]];
        }
            break;
    }
    
  
    numFormat = [[NSNumberFormatter alloc] init];
    [numFormat setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormat setCurrencySymbol:@"$"];
    
    NSArray *fav = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey];
    for (NSDictionary *element in fav) {
        if ([[element objectForKey:@"dataSource"][kIDKey] intValue] == [_dataSource[kIDKey] intValue]) {
            _favoriteDataSource = element;
        }
    }
    


    
  
    
    // [self addShadowToView:_payButton];
    [_payButton addTarget:self action:@selector(pay_action:) forControlEvents:UIControlEventTouchUpInside];
    [_payButton setTitle:NSLocalizedString(@"pay", nil) forState:UIControlStateNormal];
    
 
    
    [_amount_collection setFrame:CGRectMake(_amount_collection.frame.origin.x, _amount_collection.frame.origin.y, _amount_collection.frame.size.width, 10*2+(_amountDataSource.count/2)*10+50+50*(_amountDataSource.count/2))];
    
    [self addValidationTextFieldsToDelegate];
    
  
    
    [_comissionLabel setText:@""];
    
    UIButton *button = [[UIButton alloc] init];
    button.tag = 0;
    [self cellSelected:button];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0], NSFontAttributeName, [UIColor darkMCGreyColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    [[UISegmentedControl appearance] setBackgroundImage:[self imageFromColor:[UIColor darkMCGreyColor]] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:[self imageFromColor:[UIColor clearColor]] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    UIButton *button = [[UIButton alloc] init];
    [button setTag:selectedID];
    [self cellSelected:button];
    
   
    
    [super viewDidAppear:animated];
}



- (void)saveViewData
{
    NSMutableArray *favoritesArray;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]) {
        favoritesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]];
    } else {
        favoritesArray = [[NSMutableArray alloc] init];
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:_dataSource forKey:@"dataSource"];
    [params setObject:_amountDataSource forKey:@"amountDataSource"];
    [params setObject:[NSString stringWithFormat:@"%d", favoriteTopUpToll] forKey:@"type"]; // Recargas THIS MAY CHANGE DUE TO TOLL DIFFERENCIATION
    
    [favoritesArray addObject:params];
    
    [[NSUserDefaults standardUserDefaults] setObject:favoritesArray forKey:(NSString*)kUserFavoritesKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _favoriteDataSource = params;
}

- (void)favoriteButton_Action:(id)sender {
    if (_favoriteDataSource) {
        NSMutableArray *favoritesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]];
        
        [favoritesArray removeObject:_favoriteDataSource];
        _favoriteDataSource = nil;
        
        [[NSUserDefaults standardUserDefaults] setObject:favoritesArray forKey:(NSString*)kUserFavoritesKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [self saveViewData];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showTagOptionList:(id)sender
{
    for (UITextField_Validations *element in [self getValidationTextFields]) {
        switch (element.tag) {
            case 10:
                [element setText:@""];
                break;
            case 20:
                [element setText:@""];
                break;
            default:
                break;
        }
    }
    
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¿Desea seleccionar o eliminar TAG?", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Seleccionar", nil), NSLocalizedString(@"Eliminar", nil), nil];
    [alertMsg setTag:100];
    [alertMsg show];
}

- (void) showTagList:(id)sender
{
    if (!userTagList) {
        tagsManager = [[pootEngine alloc]init];
        [tagsManager setDelegate:self];
        [tagsManager setShowComments:developing];
        [tagsManager setTag:90];
        
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [tagsManager startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/", userInfo[@"ideUsuario"], @"/tags/", _type==serviceTypeIAVE?@"1":@"4"]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } else {
        if ([userTagList count]==0) {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"No existen TAGs guardados", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Seleccione un TAG:", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles: nil];
            for (NSDictionary *item in userTagList) {
                [alertMsg addButtonWithTitle:[NSString stringWithFormat:@"%@ - %@", item[kTagLabelKey], item[kTagNumberKey]]];
            }
            [alertMsg setTag:200];
            [alertMsg show];
        }
    }
}

- (void) showDeleteTagList:(id)sender
{
    if (!userTagList) {
        tagsManager = [[pootEngine alloc]init];
        [tagsManager setDelegate:self];
        [tagsManager setShowComments:developing];
        [tagsManager setTag:90];
        
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [tagsManager startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/", userInfo[@"ideUsuario"], @"/tags/", _type==serviceTypeIAVE?@"1":@"4"]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } else {
        if ([userTagList count]==0) {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"No existen TAGs guardados", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Seleccione un TAG:", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles: nil];
            for (NSDictionary *item in userTagList) {
                [alertMsg addButtonWithTitle:[NSString stringWithFormat:@"%@ - %@", item[kTagLabelKey], item[kTagNumberKey]]];
            }
            [alertMsg setTag:300];
            [alertMsg show];
        }
    }
}

- (void) addTag:(id)sender
{
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:NSLocalizedString(@"¿Desea guardar el TAG con número %@-%@?\n\nPara guardarlo introduzca una etiqueta:", nil), [(UITextField_Validations*)[self getValidationTextFields][0] text], [(UITextField_Validations*)[self getValidationTextFields][1] text]] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Guardar", nil), nil];
        [alertMsg setTag:201];
        [alertMsg setAlertViewStyle:UIAlertViewStylePlainTextInput];
        
        [alertMsg show];
    }
}

- (void) deleteTag:(id)sender
{
    UITextField_Validations *tagNumber;
    for (UITextField_Validations *element in [self getValidationTextFields]) {
        switch (element.tag) {
            case 10:
                tagNumber = element;
                break;
            default:
                break;
        }
    }
    
    if ([[tagNumber text] length] == 0) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Necesita seleccionar un TAG primero", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
    } else {
    
        
  
    }
}

- (void) showContactList:(id)sender
{
    CNContactPickerViewController *contactPicker = [[CNContactPickerViewController alloc] init];
    contactPicker.delegate = self;
    
    NSArray *arrKeys = @[CNContactPhoneNumbersKey]; //display only phone numbers
    contactPicker.displayedPropertyKeys = arrKeys;
    
    [self presentViewController:contactPicker animated:YES completion:nil];
}

- (void) selectTagTypeChanged:(id)sender
{
    UISegmentedControl *segmented = sender;
    
    selectTag = segmented.selectedSegmentIndex==0?YES:NO;
    

    
    for (UITextField_Validations *element in [self getValidationTextFields]) {
        [element setText:@""];
    }
    
 
    
  
   
    
    [self.tableView reloadData];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField_Validations *)textField
{
    BOOL result = [self subTextFieldShouldBeginEditing:textField];
    
 
    
    return result;
}

- (IBAction)pay_action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        switch (_type) {
            case serviceTypePASE:
            case serviceTypeIAVE:
            {
              
                    [self confirmTollPayment];
              
                
                //[self showCardOptions];
            }
                break;
                
            default:
            {
                //ANY OTHER TYPE
            }
                break;
        }
    }
}

- (void) checkBalance
{
    balanceManager = [[pootEngine alloc] init];
    [balanceManager setDelegate:self];
    [balanceManager setShowComments:developing];
    
    NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[NSNumber numberWithInt:_type == serviceTypeIAVE?1:4] forKey:kTagTypeKey];
    [params setObject:@"" forKey:kTagLabelKey];
    //[params setObject:[_tagNumberText text] forKey:kTagNumberKey];
//    [params setObject:[NSNumber numberWithInt:[[_digitText text] intValue]] forKey:kTagDVKey];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [balanceManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/%@/tags/checkBalance", WSGetPassBalance, userDetails[kUserIDKey]] withPost:JSONString];
    
    [self lockViewWithMessage:nil];
}


- (void) confirmTollPayment
{
    [self performSegueWithIdentifier:@"summaryToll" sender:nil];
}

- (void) showCardOptions
{
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
      //  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
       // [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [DataUser clean];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark Tableview setting up
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            switch (_type) {
                case serviceTypeIAVE:
                case serviceTypePASE:
                {
                    if ([_amountDataSource count]>0) {
                        return //_amountDataSource[0][@"concepto"];
                        @"RECARGA DE MOBILETAG";
                    } else {
                        return _dataSource[kDescriptionKey];
                    }
                }
                    break;
                    
                default:
                    //ANY OTHER TYPE;
                    return @"NO TYPE";
                    break;
            }
        }
            break;
            
        default:
            return @"";
            break;
    }
}

- (IBAction)back_Action:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (_type) {
        case serviceTypeIAVE:
        case serviceTypePASE:
        {
            switch (indexPath.row) {
                case 0://tag list selection
                    return 90;
                    break;
                case 1://tag selected
                    return 15;
                    break;
                case 2://tag selected
                    return 70;
                    break;
                case 3://tag
                    return 15;
                    break;
                case 4:
                    return 10*2+(_amountDataSource.count/2)*10+50+50*(_amountDataSource.count/2);
                    break;
                case 5://digit
                    return  30;
                    break;
                case 6://amounts
                    return 74;
                    break;
                case 7://comission
                    return 30;
                    break;
                case 8://Paybutton
                    return 74;
                    break;
               
                default:
                    return 50;
                    break;
            }
        }
            break;
            
        default:
        {
            switch (indexPath.row) {
                case 0:
                    return 70;
                default:
                    return 10;
                    break;
            }
        }
            break;
    }
}

#pragma mark setting up Collection view

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_amountDataSource count];
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width-10)/2, 50);
  //  return CGSizeMake((collectionView.frame.size.width/2.07), 50);
}

- (generalCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"Cell_amount";
    
    generalCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.tag = indexPath.row;
    cell.cellButton.tag = indexPath.row;
    [cell.cellText setText:[NSString stringWithFormat:@"%@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[_amountDataSource[indexPath.row][@"monto"] floatValue]]]]];
    [cell.cellButton setEnabled:YES];
    [cell.cellButton addTarget:self action:@selector(cellSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    for (generalCollectionCell *cellInArray in cellArray) {
        if (cellInArray == cell) {
            return cell;
        }
    }
    
    [cellArray addObject:cell];
    
    return cell;
}

- (void) cellSelected:(id)sender
{
    UIButton *cellButton = sender;
    
    selectedID = (int)cellButton.tag;
    
    for (generalCollectionCell *cell in cellArray) {
        if (cell.tag == cellButton.tag) {
            [cell selectCell];
        } else {
            [cell deSelectCell];
        }
    }
    
    [_comissionLabel setText:[NSString stringWithFormat:NSLocalizedString(@"Comisión %@", nil), [numFormat stringFromNumber:[NSNumber numberWithFloat:[_amountDataSource[cellButton.tag][@"comision"] floatValue]]]]];
}

#pragma Mark pootEngine handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == tagsManager) {
        switch (tagsManager.tag) {
            case 90:
            {
                NSDictionary *response = (NSDictionary*)json;
                if (!([response[kIDError] intValue] == 0)) {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"No existen TAGs guardados", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                } else {
                    NSDictionary *response = (NSDictionary*)json;
                    userTagList = [[NSMutableArray alloc] init];
                    
                    for (id tag in [response objectForKey:@"tags"]) {
                        if ([[tag objectForKey:kTagLabelKey]isEqualToString:@"Seleccione"]||[[tag objectForKey:kTagLabelKey]isEqualToString:@"Otro"]) {
                            nil;
                        } else {
                            [userTagList addObject:tag];
                        }
                    }
                    
                    [self showTagList:nil];
                }
            }
                break;
            case 91:
            {
                NSDictionary *response = (NSDictionary*)json;
                
                if ([response[kIDError] intValue]==0) {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información",nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                    
                    userTagList = response[@"tags"];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información",nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
                
                userTagList = nil;
            }
                break;
            case 92:
            {
                NSDictionary *response = (NSDictionary*)json;
                
                if ([[response objectForKey:kIDError]intValue]==0) {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información",nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                   
                    userTagList = response[@"tags"];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información",nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
            }
                break;
                
            default:
                break;
        }
    }
    
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
    }
    
    if (manager == balanceManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue]==0) {
            currentBalanace = [response[@"balance"] floatValue];
            
           
                [self confirmTollPayment];
            
            return;
        } else {
         
        }
        
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
    }
}

#pragma Mark storyBoards
- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view setType:viewType];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"summaryToll"]) {
        UINavigationController *nav = [segue destinationViewController];
        
        generalSummary *nextView = [[nav viewControllers] firstObject];
        [nextView setDelegate:self];
        
        NSMutableDictionary *amountData = [[NSMutableDictionary alloc] initWithDictionary:_amountDataSource[selectedID]];
        
     //   [nextView setMainString:[NSString stringWithFormat:@"%@ %@", [(UITextField_Validations*)[self getValidationTextFields][0] text], [(UITextField_Validations*)[self getValidationTextFields][1] text]]];
        [nextView setMainString:[NSString stringWithFormat:@"%@", [(UITextField_Validations*)[self getValidationTextFields][0] text]]];
        [nextView setFirstString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:currentBalanace]], @"MXN"]];
        [nextView setSecondString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]]], @"MXN"]];
        [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"comision"] floatValue]]], @"MXN"]];
        [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]+[amountData[@"comision"] floatValue]]], @"MXN"]];
        [nextView set_FromMobileTag:@"1"];
        
        [nextView setConvertToUSD:NO];
    }
}


#pragma cardSelection Handler

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    switch (_type) {
        case serviceTypeIAVE:
        case serviceTypePASE:
        {
            
              NSMutableDictionary *amountData = [[NSMutableDictionary alloc] initWithDictionary:_amountDataSource[selectedID]];
            
            NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:userInfo[kUserLogin] forKey:@"login"];
            
            
            //[amountData[@"monto"] floatValue]
            
             [params setObject:selectedCard[@"idTarjeta"] forKey:@"idCard"];
         
            [params setObject:[NSNumber numberWithFloat:[_amountDataSource[selectedID][@"monto"] floatValue]] forKey:@"cargo"];
            
              [params setObject:@"" forKey:@"codigoPais"];
            
              [params setObject:[NSNumber numberWithFloat:[_amountDataSource[selectedID][@"comision"] floatValue]] forKey:@"comision"];
            
            [params setObject:@"RECARGA TELEPEAJE PASE URBANO" forKey:@"concepto"];
            
            [params setObject:@"800" forKey:@"emisor"];
                     [params setObject:@"1" forKey:@"idAplicacion"];
                     [params setObject:DataUser.COUNTRYID forKey:@"idPais"];
              [params setObject:@"800" forKey:@"idProveedor"];
                [params setObject:[NSNumber numberWithDouble:[userInfo[kUserIDKey] doubleValue]] forKey:@"idUser"];
            
             [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
            
            [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"lat"];
            [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"lon"];
            
               [params setObject:@"Apple" forKey:@"modelo"];
            
             [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
            
            [params setObject:[NSString stringWithFormat:@"%@", [(UITextField_Validations*)[self getValidationTextFields][0] text]] forKey:@"referencia"];
            
            [params setObject:_amountDataSource[selectedID][@"operacion"] forKey:@"operacion"];
            
           // [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
            
        
            developing?NSLog(@"PARAMS -> %@", params):nil;
            
            Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
            [nextView setPurchaseInfo:params];
            [nextView setSecure3DURL:WSGetANTADIAVEPayment];
            [nextView setType:serviceTypeMobileTag];
            [nextView setSelectedCardInfo:[NSMutableDictionary dictionaryWithDictionary:selectedCard]];
            
            [self.navigationController pushViewController:nextView animated:YES];
        }
            break;
            
        default:
        {
            //NOT USED
        }
            break;
    }
}

#pragma Mark summary Handler

- (void)generalSummaryResponse:(NSDictionary *)response
{
    [self showCardOptions];
}

#pragma Mark UIAlertView Handler

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (!(buttonIndex == 0)) {
        switch (alertView.tag) {
            case 100:
            {
                switch (buttonIndex) {
                    case 1:
                        [self showTagList:nil];
                        break;
                    case 2:
                        [self showDeleteTagList:nil];
                        break;
                    default:
                        break;
                }
                
            }
                break;
            case 200:
            {
                
                
            
                
                [self checkBalance];
            }
                break;
            case 201:
            {
                if ([[[alertView textFieldAtIndex:0] text] length]>0) {
                    tagsManager=[[pootEngine alloc]init];
                    [tagsManager setDelegate:self];
                    [tagsManager setShowComments:developing];
                    [tagsManager setTag:91];
                    
                    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    
                    [params setObject:_type == serviceTypeIAVE?@"1":@"4" forKey:kTagTypeKey];
                    [params setObject:[[alertView textFieldAtIndex:0] text] forKey:kTagLabelKey];
                    [params setObject:[(UITextField_Validations*)[self getValidationTextFields][0] text] forKey:kTagNumberKey];
                    [params setObject:[(UITextField_Validations*)[self getValidationTextFields][1] text] forKey:kTagDVKey];
                    
                    NSError *JSONError;
                    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                    
                    [tagsManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/", userInfo[kUserIDKey], @"/tags/", _type == serviceTypeIAVE?@"1":@"4", @"/add"] withPost:JSONString];
                    
                    /*
                     
                     [tagsManager startRequestWithValues:params forWS:WSSetTag withUncryption:YES];
                     */
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Debe introducir una etiqueta para guardar el TAG", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
            }
                break;
                
            case 300:
            {
                switch (buttonIndex) {
                    case 1:
                    {
                        tagsManager=[[pootEngine alloc]init];
                        [tagsManager setDelegate:self];
                        [tagsManager setShowComments:developing];
                        [tagsManager setTag:92];
                        
                        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                        
                        NSString *tagToBeDeleted = [[NSString alloc] init];
                        
                        for (UITextField_Validations *element in [self getValidationTextFields]) {
                         
                            
                            switch (element.tag) {
                                case 10:
                                    tagToBeDeleted = [element text];
                                    break;
                                default:
                                    break;
                            }
                        }
                        
                        
                        [tagsManager startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/", userInfo[kUserIDKey], @"/tags/", _type == serviceTypeIAVE?@"1":@"4", @"/", tagToBeDeleted, @"/delete"]];
                        
                        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
                break;
            default:
                break;
        }
    }
}

#pragma Mark CNContactPicker Handler (Address book)

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty{
    
    CNPhoneNumber *phoneNumber = contactProperty.value;
    
    for (UITextField_Validations *element in [self getValidationTextFields]) {
        UITextField_Validations *textfield = (UITextField_Validations*)element;
        
        @try {
            NSString *phone = [phoneNumber.stringValue stringByReplacingOccurrencesOfString:@" " withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
            if ([phone length]>=10) {
                [textfield setText:[phone substringWithRange:NSMakeRange([phone length] - 10, 10)]];
            } else {
                [textfield setText:phone];
            }
            
        } @catch (NSException *exception) {
            nil;
        } @finally {
            nil;
        }
        
        
    }
}

























#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}

@end

