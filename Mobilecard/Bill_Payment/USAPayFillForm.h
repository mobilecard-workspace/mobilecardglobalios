//
//  USAPayFillForm.h
//  MobileCard_X
//
//  Created by David Poot on 5/9/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "orderResult_Ctrl.h"

@interface USAPayFillForm : mT_commonTableViewController


@property (strong, nonatomic) NSDictionary *params;
@property (strong, nonatomic) NSDictionary *selectedCard;
@property (strong, nonatomic) NSDictionary *paymentData;

@property (weak, nonatomic) IBOutlet UILabel *infoText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cvvText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;


@end
