//
//  payment_recarga_ctrl.h
//  Mobilecard
//
//  Created by David Poot on 11/1/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import <ContactsUI/ContactsUI.h>
#import "Wallet_Ctrl.h"
#import "generalSummary.h"
#import <CoreLocation/CoreLocation.h>

@interface payment_recarga_mobilecard_Toll : mT_commonTableViewController <UITextFieldDelegate, pootEngineDelegate, CNContactPickerDelegate, UIActionSheetDelegate, cardSelectionDelegate, updateCardDelegate, generalSummaryDelegate, CLLocationManagerDelegate>

@property (nonatomic, assign) serviceType type;

@property (strong, nonatomic) NSMutableDictionary *dataSource;
@property (strong, nonatomic) NSArray *amountDataSource;
@property (strong, nonatomic) NSDictionary *countryData;

@property (strong, nonatomic) NSDictionary *favoriteDataSource;

@property (weak, nonatomic) IBOutlet UITextField_Validations *usuarioText;
@property (weak, nonatomic) IBOutlet UILabel *comissionLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *amount_collection;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@end


@interface payment_recarga_viewCtrl_Toll : mT_commonController <UICollectionViewDelegate, UICollectionViewDataSource>

@end
