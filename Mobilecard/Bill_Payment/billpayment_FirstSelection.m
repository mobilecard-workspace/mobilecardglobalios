//
//  billpayment_FirstSelection.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/20/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "billpayment_FirstSelection.h"

@interface billpayment_FirstSelection (){
    
    pootEngine *categoriesManager;
    
    NSMutableArray *categoriesInfo;
    NSMutableArray *categoriesSubmenuInfo;
    NSMutableArray *masterCategoriesSubmenuInfo;
}

@end

@implementation billpayment_FirstSelection

- (void)viewDidLoad {
    [super viewDidLoad];
    
    recipeServices =[[NSMutableArray alloc] init];
    
    categoriesManager = [[pootEngine alloc] init];
    [categoriesManager setDelegate:self];
    [categoriesManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
    [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [categoriesManager startJSONRequestWithURL:WSGetCatalogServicioCategorias withPost:JSONString];
    
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}
- (IBAction)back_Action:(id)sender {
    
    
    switch (_type) {
        case viewTypeRecargas:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case viewTypeServicios:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
    }
}
//COLLECTION HANDLING
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return recipeServices.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   //   return CGSizeMake(CGRectGetWidth(collectionView.frame)/2-10, (CGRectGetHeight(collectionView.frame)/5.7));
    
         return CGSizeMake(CGRectGetWidth(collectionView.frame)/2.2, (CGRectGetHeight(collectionView.frame)/5.7));
    
    
    
    //return CGSizeMake(170, 180);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    itemService =[recipeServices objectAtIndex:indexPath.row];

    UIImageView * recipeImageView = (UIImageView *)[cell viewWithTag:1];
    recipeImageView.image = [UIImage imageNamed:itemService.description_service];
  
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    itemService =[recipeServices objectAtIndex:indexPath.row];
    SelectItem =(int)indexPath.row;
    [self performSegueWithIdentifier:@"mexicoServices_step" sender:self];
}
#pragma Mark PootEngine Handling
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    if (manager == categoriesManager) {
        
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue] == 0) {
            switch (_type) {
                case viewTypeRecargas:
                {
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    if ([userData[kUserIdCountry] intValue]==1) {
                        categoriesInfo = [NSMutableArray arrayWithArray:response[@"recargas"]];
                    } else {
                        categoriesInfo = [NSMutableArray arrayWithArray:response[@"recargas"]];
                        
                        [categoriesInfo removeObjectAtIndex:1];
                    }
                }
                    break;
                    
                case viewTypeServicios:
                {
                    categoriesInfo = [NSMutableArray arrayWithArray:response[@"categorias"]];
                    
                    NSArray *Categorias = [json objectForKey:@"categorias"];
                    for (NSDictionary *jsonObjectCategoria in Categorias) {
                        
                        ServiceModel *item =[[ServiceModel alloc] init];
                        item.id_service=[[jsonObjectCategoria objectForKey:@"id"] integerValue];
                        item.description_service=[jsonObjectCategoria objectForKey:@"descripcion"];
                        
                        [recipeServices addObject:item];
                    }
                }
                    break;
            }
            [collection_service reloadData];
        }
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"mexicoServices_step"]) {
        billpayment_Selection *nextView = [segue destinationViewController];
        [nextView setType:viewTypeServicios];
        [nextView setId_Categoria:SelectItem];
        [nextView setCategories:categoriesInfo];
    }
}

@end
