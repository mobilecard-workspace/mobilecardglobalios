//
//  LaCuenta_Step1.h
//  Mobilecard
//
//  Created by David Poot on 2/5/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonController.h"
#import <CoreLocation/CoreLocation.h>

@interface LaCuenta_Step1 : mT_commonController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UITextField_Validations *searchText;
@property (weak, nonatomic) IBOutlet UITableView *resultsTable;
@end
