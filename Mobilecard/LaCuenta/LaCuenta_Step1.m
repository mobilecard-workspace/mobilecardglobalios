//
//  LaCuenta_Step1.m
//  Mobilecard
//
//  Created by David Poot on 2/5/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "LaCuenta_Step1.h"
#import "LaCuenta_Step2.h"

@interface LaCuenta_Step1 ()
{
    NSMutableArray *businessArray;
    
    pootEngine *businessManager;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *placeName;
    
    int selectedID;
}
@end

@implementation LaCuenta_Step1

- (void)viewDidLoad {
    [super viewDidLoad];
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    [self lockViewWithMessage:nil];
    
    businessArray = [[NSMutableArray alloc] init];
    
    [_searchText setUpTextFieldAs:textFieldTypeGeneralRequired];
    
    [self addValidationTextField:_searchText];
    
    [self addValidationTextFieldsToDelegate];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self unLockView];
    
    //NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Requerimos tu localización para realizar esta operación,por favor autoriza a MobileCard en la configuración de tu dispositivo e intenta nuevamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    [self unLockView];
    
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
    
    businessManager = [[pootEngine alloc] init];
    [businessManager setDelegate:self];
    [businessManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSNumber numberWithFloat:currentLocation.coordinate.latitude] forKey:@"lat"];
    [params setObject:[NSNumber numberWithFloat:currentLocation.coordinate.longitude] forKey:@"lon"];
    [params setObject:[_searchText text] forKey:@"ubicacion_actual"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [businessManager startJSONRequestWithURL:LaCuenta_getPlaces withPost:JSONString];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) prepareBusinessArray:(NSArray*)resultArray
{
    [businessArray removeAllObjects];
    
    if (resultArray) {
        for (NSDictionary *element in resultArray) {
            [businessArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"20", kHeightKey, nil]];
            [businessArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:element, kDescriptionKey, @"element", kIdentifierKey, @"60", kHeightKey, nil]];
        }
    }
    
    [_resultsTable reloadData];
}

#pragma TableView Handler

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [businessArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [businessArray[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [businessArray objectAtIndex:indexPath.row][kIdentifierKey];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if ([identifier isEqualToString:@"element"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
                switch (label.tag) {
                    case 1:
                        [label setText:businessArray[indexPath.row][kDescriptionKey][@"alias"]];
                        break;
                    case 2:
                        [label setText:businessArray[indexPath.row][kDescriptionKey][@"correo"]];
                    default:
                        break;
                }
            }
            if ([element isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView = element;
                switch (imageView.tag) {
                    case 1:
                    {
                        pootEngine *imageManager = [[pootEngine alloc] init];
                        if (![businessArray[indexPath.row][kDescriptionKey][@"urlLogo"] isEqualToString:@""]) {
                            [imageManager startImageRequestWithURL:businessArray[indexPath.row][kDescriptionKey][@"urlLogo"] andImageView:imageView];
                        }
                    }
                        break;
                        
                    default:
                        break;
                }
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    selectedID = indexPath.row;
    
    [self performSegueWithIdentifier:@"step2" sender:nil];
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == businessManager) {
        NSDictionary *resp = (NSDictionary*)json;
        
        NSMutableDictionary *response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:resp]];
        
        if ([response[kIDError] intValue] == 0) {
            [self prepareBusinessArray:response[@"accounts"]];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step2"]) {
        LaCuenta_Step2 *nextView = [segue destinationViewController];
        
        [nextView setPaymentData:businessArray[selectedID][kDescriptionKey]];
    }
}

@end
