//
//  LaCuenta_Step2.m
//  Mobilecard
//
//  Created by David Poot on 2/5/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "LaCuenta_Step2.h"
#import "LaCuenta_Step3.h"


@interface LaCuenta_Step2 ()
{
    NSNumberFormatter *numFormatter;
    
    pootEngine *cardManager;
    pootEngine *imageManager;
    
    float finalAmount;
    float finalComission;
}
@end

@implementation LaCuenta_Step2

- (void)viewDidLoad {
    [super viewDidLoad];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [_referenceText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_totalText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_totalText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]];
    [_totalText setKeyboardType:UIKeyboardTypeDecimalPad];
    [_tipText  setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_tipText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]];
    [_tipText setKeyboardType:UIKeyboardTypeDecimalPad];
    
    [self addValidationTextField:_referenceText];
    [self addValidationTextField:_totalText];
    [self addValidationTextField:_tipText];
    
    [self addValidationTextFieldsToDelegate];
    
  //  [self addShadowToView:_continue_Button];
    
    [self updateTotalNComissionWithTotal:0.0];
    
    imageManager = [[pootEngine alloc] init];
    [imageManager startImageRequestWithURL:_paymentData[@"urlLogo"] andImageView:_logoView];
}


- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        @try {
            cardManager = [[pootEngine alloc] init];
            [cardManager setDelegate:self];
            [cardManager setShowComments:developing];
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        } @catch (NSException *exception) {
            //NSLog(@"%@", exception.description);
            
          //  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
           // [alertMsg show];
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
           
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    if (selectedCard) {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [params setObject:@"concepto" forKey:@"concept"];
        [params setObject:userInfo[kUserIDKey] forKey:@"idUser"];
        [params setObject:selectedCard[@"idTarjeta"] forKey:@"idCard"];
        [params setObject:_paymentData[kIDKey] forKey:@"establecimientoId"];
        [params setObject:[_totalText text] forKey:@"amount"];
        [params setObject:[NSNumber numberWithFloat:finalComission] forKey:@"comision"];
        [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
        [params setObject:[_referenceText text] forKey:@"referenciaNeg"];
        [params setObject:[_tipText text] forKey:@"propina"];
        
        [self performSegueWithIdentifier:@"step3" sender:params];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _totalText) {
        [self updateTotalNComissionWithTotal:[[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue]+[[_tipText text] floatValue]];
    }
    if (textField == _tipText) {
        [self updateTotalNComissionWithTotal:[[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue]+[[_totalText text] floatValue]];
    }
    return YES;
}

- (void) updateTotalNComissionWithTotal:(float)total
{
    float fixed = [_paymentData[@"comision_fija"] floatValue];
    float amountWithFixed = total+[_paymentData[@"comision_fija"] floatValue];
    float percentage =[_paymentData[@"comision_porcentaje"] floatValue];
    
    
    finalComission = fixed+(amountWithFixed*percentage);
    finalAmount = total+finalComission;
    
    [_feeLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalComission]]]];
    [_totalLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount]]]];
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (cardManager == manager) {
        NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0 && [response[kUserCardArray] count]>0) {
            [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
            [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
            UINavigationController *nav = [nextStory instantiateInitialViewController];
            Wallet_Ctrl *view = nav.viewControllers.firstObject;
            [view setType:walletViewTypeSelection];
            [view setDelegate:self];
            
            [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step3"]) {
        LaCuenta_Step3 *nextView = [segue destinationViewController];
        NSMutableDictionary *params = sender;
        [nextView setPaymentData:[NSDictionary dictionaryWithDictionary:params]];
        [nextView setLogoImage:[_logoView image]];
    }
}

@end
