//
//  LaCuenta_Step3.m
//  Mobilecard
//
//  Created by David Poot on 2/6/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "LaCuenta_Step3.h"
#import "Secure3D_Ctrl.h"
#import "LaCuenta_Result.h"

@interface LaCuenta_Step3 ()
{
    NSNumberFormatter *numFormatter;
    
    pootEngine *paymentManager;
}
@end

@implementation LaCuenta_Step3

- (void)viewDidLoad {
    [super viewDidLoad];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
 //   [self addShadowToView:_continue_Button];
    
    [self updateInfo];
}

- (void) updateInfo
{
    [_amountLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_paymentData[@"amount"] floatValue]]]]];
    [_tipLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_paymentData[@"propina"] floatValue]]]]];
    [_feeLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_paymentData[@"comision"] floatValue]]]]];
    [_totalLabel setText:[NSString stringWithFormat:@"%@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_paymentData[@"comision"] floatValue]+[_paymentData[@"propina"] floatValue]+[_paymentData[@"amount"] floatValue]]]]];
    
    [_logoView setImage:_logoImage];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    paymentManager = [[pootEngine alloc] init];
    [paymentManager setDelegate:self];
    [paymentManager setShowComments:developing];
    
    Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
    [nextView setPurchaseInfo:[NSMutableDictionary dictionaryWithDictionary:_paymentData]];
    [nextView setSecure3DURL:LaCuenta_3DSecure];
    [nextView setType:serviceTypeLaCuenta];
    
    [nextView setDelegate:self];
    
    [self.navigationController pushViewController:nextView animated:YES];
}

- (void)Secure3DResponse:(NSDictionary *)response
{
    [self performSegueWithIdentifier:@"result" sender:response];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"result"]) {
        LaCuenta_Result *nextView = [segue destinationViewController];
        NSDictionary *response = sender;
        [nextView setResultData:[NSDictionary dictionaryWithDictionary:response]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
