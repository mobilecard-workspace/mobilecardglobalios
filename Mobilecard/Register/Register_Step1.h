//
//  Register_Step1.h
//  Mobilecard
//
//  Created by David Poot on 11/2/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol registerProtocol <NSObject>
@required
- (void) registryShallLaunchMyMCWithUserData:(NSDictionary*)userdata;
@end


@interface Register_Step1 : mT_commonTableViewController

@property (nonatomic, assign) id <registerProtocol, NSObject> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passwordText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passwordConfirmText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end
