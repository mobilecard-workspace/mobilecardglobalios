//
//  Register_Step30.h
//  Mobilecard
//
//  Created by David Poot on 11/28/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

NS_ASSUME_NONNULL_BEGIN
@protocol registerSMSProtocol <NSObject>

@required
- (void) registerSMS_Response:(NSDictionary*)userData;

@end


@interface Register_Step30 : mT_commonTableViewController


@property (nonatomic, assign) id <NSObject, registerSMSProtocol> delegate;

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet UILabel *text0;
@property (weak, nonatomic) IBOutlet UILabel *text1;
@property (weak, nonatomic) IBOutlet UILabel *text2;
@property (weak, nonatomic) IBOutlet UILabel *text3;
@property (weak, nonatomic) IBOutlet UILabel *text4;
@property (weak, nonatomic) IBOutlet UILabel *text5;

@property (weak, nonatomic) IBOutlet UIButton *resend_Button;

@property (weak, nonatomic) IBOutlet UIView *contentView;


@end

NS_ASSUME_NONNULL_END
