//
//  RegisterTarjetController.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/7/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "RegisterTarjetController.h"
#import <Mobilecard-Swift.h>

@interface RegisterTarjetController ()
{
    CardIOCreditCardInfo *cardInfo;
    pootEngine *crypter;
    
    int cardType;
    NSArray* cardToUpdate;
    
    pootEngine *updateManager;
    pootEngine *cardManager;
     pootEngine *stateManager;
}
@end
@implementation RegisterTarjetController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    stateManager = [[pootEngine alloc] init];
    [stateManager setDelegate:self];
    [stateManager setShowComments:developing];
    
    [stateManager startWithoutPostRequestWithURL:WSGetINGOStateCatalog];
    [self lockViewWithMessage:nil];
    
    
    //aqui empiezan validaciones si es usa
    if ([DataUser.COUNTRYID  isEqual: @"3"]){
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [self addValidationTextField:_stateText];
        
          [_cityUsaText setUpTextFieldAs:textFieldTypeAddress];
         [_addressUsaText setUpTextFieldAs:textFieldTypeAddress];
        
         [_zipCodeUsaText setUpTextFieldAs:textFieldTypeZip];
        [self addValidationTextField:_zipCodeUsaText];
          [self addValidationTextField:_addressUsaText];
           [self addValidationTextField:_cityUsaText];
    }else{
        //si no es estados unidos agrega los campos de facturacion normales
        [_CardCodezip setUpTextFieldAs:textFieldTypeZip];
        [_CardStreet setUpTextFieldAs:textFieldTypeAddress];
        [_CardState setUpTextFieldAs:textFieldTypeAddress];
        [_CardStreet setMaxLength:100];
        [self addValidationTextField:_CardStreet];
        [self addValidationTextField:_CardState];
        [self addValidationTextField:_CardCodezip];
    }
    //aqui termina si es usa
    
    
    [self initializePickersForView:self.navigationController.view];
    
    
    updateManager = [[pootEngine alloc] init];
    [updateManager setDelegate:self];
    [updateManager setShowComments:developing];
    
    
    crypter = [[pootEngine alloc] init];
    [_ImageCard setHidden:YES];
    //    [self addShadowToView:_continue_Button];
    
    
    _CardNumberFirst.delegate =self;
    _CardNumberTwo.delegate =self;
    _CardNumberTree.delegate =self;
    _CardNumberFour.delegate =self;
    
    
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_cardText setUpTextFieldAs:textFieldTypeCard];
    [_cvvText setUpTextFieldAs:textFieldTypeCvv2];
    [_validText setUpTextFieldAs:textFieldTypeValidCard];
   
    
  
    [_TypeTarjetText setUpTextFieldAs:textFieldTypeRequiredCombo];
    
    
    
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_cardText];
    [self addValidationTextField:_cvvText];
    [self addValidationTextField:_validText];
   
    [self addValidationTextField:_TypeTarjetText];
    
    
    [self addValidationTextFieldsToDelegate];
    
    NSMutableArray *cardTypeArray = [[NSMutableArray alloc] init];
    [cardTypeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"CREDITO", kIDKey, NSLocalizedString(@"CREDITO", nil), kDescriptionKey, nil]];
    [cardTypeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"DEBITO", kIDKey, NSLocalizedString(@"DEBITO", nil), kDescriptionKey, nil]];
    
    
    [_TypeTarjetText setIdLabel:(NSString*)kIDKey];
    [_TypeTarjetText setDescriptionLabel:(NSString*)kDescriptionKey];
    [_TypeTarjetText setInfoArray:cardTypeArray];
    
    _type =updateCardTypeNew;
    switch (_type) {
        case updateCardTypeNew:
        case updateCardTypeRegistry:
        {
            [_nameText setText:[NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName], [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLastName]]];
        }
        break;
        
        case updateCardTypeExisting:
        {
        }
        break;
        
        default:
        {
        }
        break;
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    NSLog(@"Este es el indexPath  %ld",(long)indexPath.row);
    
    switch (indexPath.row){
        case 0:
            return 50;
            break;
            
        case 1:
            return 130;
            break;
            
        case 2:
            return 50;
            break;
        
        case 3:
            return 80;
            break;
            
        case 4:
            return 145;
            break;
            
            //aqui validamos el pais para que salgan solo en eua
        case 5:
            
            //si es estados unidos sale esa seccion
            if ([DataUser.COUNTRYID  isEqual: @"3"]){
                return 242;
                
            }else{
                    return 0;
                }
            
            break;
            
            
        case 6:
            
            //si es estados unidos sale esa seccion
            if ([DataUser.COUNTRYID  isEqual: @"3"]){
                return 0;
                
            }else{
                return 190;
            }
            
           
            break;
            
        case 7:
            return 130;
            break;
            
            
            
    }
    
    return 0;
  
    
   
    
    
}

    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)begin_editingtext:(UITextField_Validations*)sender{
    [self scanCard:nil];
    [sender resignFirstResponder];
}
- (void)textFieldDidEndEditing:(UITextField_Validations *)textField
    {
        if (textField == _cardText) {
            textField.hiddenString = textField.text;
            NSMutableString *card = [[NSMutableString alloc]init];
            if ([_cardText.text length]!=0 && [_cardText.text length]>4){
                for (int i=0; i<([textField.text length]-4); i++) {
                    [card appendString:@"X"];
                }
                for (int i=((int)[_cardText.text length]-4); i<[_cardText.text length]; i++) {
                    [card appendString:[NSString stringWithFormat:@"%c",[_cardText.text characterAtIndex:i]]];
                }
                _cardText.text = card;
            }
            
            if (![_cardText.hiddenString isEqualToString:@""]) {
                switch ([[_cardText.hiddenString substringWithRange:NSMakeRange(0, 1)] intValue]) {
                    case 4://SET ADDRESS & ZIP MANDATORY
                    cardType = 1;
                    [_CardStreet setRequired:YES];
                    [_CardCodezip setRequired:YES];
                    break;
                    case 5://SET ADDRESS & ZIP MANDATORY
                    cardType = 2;
                    [_CardStreet setRequired:YES];
                    [_CardCodezip setRequired:YES];
                    break;
                    
                    default:
                    cardType = 3;
                    [_CardStreet setRequired:YES];
                    [_CardCodezip setRequired:YES];
                    break;
                }
            }
            
        }
    }
    
- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
    
- (IBAction)scanCard:(id)sender {
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    scanViewController.hideCardIOLogo=YES;
    scanViewController.languageOrLocale = @"es";
    scanViewController.scanInstructions = @"Coloque su tarjeta embosada (con relieve) aquí.\nSe escaneará automáticamente.";
    [self.navigationController presentViewController:scanViewController animated:YES completion:nil];
}
    
- (IBAction)continue_Action:(id)sender {
    switch (_type) {
        case updateCardTypeNew:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                
                
             
                //revisamos si venimos de peru usuario y si el pin es de tepca
                //empieza la validacion de tepca
                if([DataUser.COUNTRYID isEqualToString:(@"4")]){
            
            //convertimos en arreglo de caracteres el numero de la tarjeta
                  const char *first = [cardInfo.cardNumber UTF8String];
                
                //comienza con los numeros de tepca
                if ((first[0] == '4' && first[1] == '8' && first[2] == '3' && first[3] == '0' ) && (first[4] == '3' && first[5] == '9')){
                    
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"¡Aviso!"
                                                                                   message:@"Requerimos algunos datos adicionales para dar de alta esta tarjeta. ¿Deseas continuar?"
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"SI" style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {
                                                                              NSLog(@"PRUEBA CON EXITO A TODO DAR");
                 //mandamos la vista para continuar el registro de la tarjeta
                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
                    PERegisterTepcaViewController *vc = [nextStory instantiateViewControllerWithIdentifier:@"tepcaRegisterCard"];
                    vc.numeroTarjeta = self->_cardText.hiddenString ;
                    vc.vencimiento = self->_validText.text ;
                    vc.cvv = self->_cvvText.text ;
                    vc.tipoDeTarjeta = self->_TypeTarjetText.infoArray[self->_TypeTarjetText.selectedID][self->_TypeTarjetText.idLabel] ;
                    vc.direccion = self->_CardStreet.text ;
                    vc.codigoPostal = self->_CardCodezip.text ;
                                                                              
                [self.navigationController pushViewController:vc animated:YES];
                                                                            
                    
                                                                          }];
                    
                    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {}];
                    
                    [alert addAction:defaultAction];
                     [alert addAction:cancelAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                   
                    
                    return;
                }
                }
            //termina la validacion de tepca
                
               
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
                
                [params setObject:[updateManager encryptJSONString:_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                [params setObject:[updateManager encryptJSONString:_cvvText.text withPassword:nil] forKey:@"codigo"];
                [params setObject:[updateManager encryptJSONString:_validText.text withPassword:nil] forKey:@"vigencia"];
                [params setObject:_nameText.text forKey:@"nombre"];
                
                [params setObject:[_CardStreet text] forKey:@"domAmex"];
                [params setObject:[_CardCodezip text] forKey:@"cpAmex"];
                //   [params setObject:[NSNumber numberWithBool:_primarySwitch.on] forKey:@"determinada"];
                [params setObject:[NSNumber numberWithInt:-1] forKey:@"idTarjeta"];
                
                [params setObject:_TypeTarjetText.infoArray[_TypeTarjetText.selectedID][_TypeTarjetText.idLabel] forKey:@"tipoTarjeta"];
                
                
                [params setObject:(NSLocalizedString(@"lang", nil)) forKey:kLanguage];
                [params setObject:DataUser.COUNTRYID forKey:@"idPais"];
                
                //validacion si estamos en estados unidos ponemos mas parametros
                
                        if([DataUser.COUNTRYID isEqualToString:(@"3")]){
                              [params setObject:_zipCodeUsaText.text forKey:@"zipCodeUsa"];
                                [params setObject:_cityUsaText.text forKey:@"cityUsa"];
                                [params setObject:_addressUsaText.text forKey:@"addressUsa"];
                            [params setObject:[NSString stringWithFormat:@"%@", [_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]]] forKey:@"stateUsa"];

                            
                        }
                //temrminamos validacion EUA
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [updateManager startJSONRequestWithURL:WSWalletAddCard withPost:JSONString];
                
                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
            }
        }
        break;
        
        case updateCardTypeExisting:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
                
                [params setObject:[updateManager encryptJSONString:_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                [params setObject:[updateManager encryptJSONString:_cvvText.text withPassword:nil] forKey:@"codigo"];
                [params setObject:[updateManager encryptJSONString:_validText.text withPassword:nil] forKey:@"vigencia"];
                
                [params setObject:_nameText.text forKey:@"nombre"];
                [params setObject:[_CardStreet text] forKey:@"domAmex"];
                [params setObject:[_CardCodezip text] forKey:@"cpAmex"];
                // [params setObject:[NSNumber numberWithBool:_primarySwitch.on] forKey:@"determinada"];
                [params setObject:cardToUpdate[_cardToUpdateID][@"idTarjeta"] forKey:@"idTarjeta"];
                
                [params setObject:_TypeTarjetText.infoArray[_TypeTarjetText.selectedID][_TypeTarjetText.idLabel] forKey:@"tipoTarjeta"];
                
                [params setObject:(NSLocalizedString(@"lang", nil)) forKey:kLanguage];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [updateManager startJSONRequestWithURL:WSWalletUpdateCard withPost:JSONString];
                
                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                
                
            }
        }
        break;
        case updateCardTypeRegistry:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                [self dismissViewControllerAnimated:YES completion:^{
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    
                    [params setObject:[self->updateManager encryptJSONString:self->_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                    [params setObject:[self->updateManager encryptJSONString:self->_cvvText.text withPassword:nil] forKey:@"codigo"];
                    [params setObject:[self->updateManager encryptJSONString:self->_validText.text withPassword:nil] forKey:@"vigencia"];
                    [params setObject:self->_nameText.text forKey:@"nombre"];
                    [params setObject:self->_TypeTarjetText.infoArray[self->_TypeTarjetText.selectedID][self->_TypeTarjetText.idLabel] forKey:@"tipoTarjeta"];
                    
                    [params setObject:@"true" forKey:@"determinada"];
                    [params setObject:@"0" forKey:@"idTarjeta"];
                    [params setObject:@"0" forKey:@"idUsuario"];
                    [params setObject:@"false" forKey:@"isMobilecard"];
                    
                    [params setObject:[self->_CardStreet text] forKey:@"domAmex"];
                    [params setObject:[self->_CardCodezip text] forKey:@"cpAmex"];
                    
                    
                }];
            }
        }
        break;
    }
}
    
#pragma mark - CardIOPaymentViewControllerDelegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    cardInfo = info;
    
  
    NSString *cardName = [CardIOCreditCardInfo displayStringForCardType:cardInfo.cardType usingLanguageOrLocale:@"en_US"];
      NSLog(@"Card name: %@",cardName);
    NSLog(@"Card number: %@",cardInfo.cardNumber);
    
    NSString *searchedString = cardInfo.cardNumber;
    NSRange   searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"^(?=\\d{16}$)(506450)\\d+";
    NSError  *error = nil;

    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:searchedString options:0 range: searchedRange];
    
    if (![cardName  isEqual: @"Visa"] && ![cardName  isEqual: @"MasterCard"] && ![cardName  isEqual: @"American Express"] && [searchedString substringWithRange:[match rangeAtIndex:1]].length <= 0){
        
        
        [_CardNumberFirst setText:@""];
         [_CardNumberTwo setText:@""];
         [_CardNumberTree setText:@""];
         [_CardNumberFour setText:@""];
         [_cvvText setText:@""];
         [_validText setText:@""];
         [self dismissViewControllerAnimated:YES completion:nil];
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"Información" message:@"La tarjeta agregada no es compatible con nuestro sistema,ingrese otra tarjeta para continuar." preferredStyle:UIAlertControllerStyleAlert];
                       
                       UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                           [alertMsg dismissViewControllerAnimated:YES completion:nil];
                       }];
                       
                       [alertMsg addAction:ok];
                       
                       [self presentViewController:alertMsg animated:YES completion:nil];
        
        return;
        
    }
    

    
   
    
   
    if ([cardName  isEqual: @"Visa"]){
         [_ImageTypeCard setImage:[UIImage imageNamed:@"visa"]];
        
    }else if ([cardName  isEqual: @"MasterCard"]){
        
        
        
          [_ImageTypeCard setImage:[UIImage imageNamed:@"mastercard"]];
    }else if ([cardName  isEqual: @"American Express"]){
        
        [_ImageTypeCard setImage:[UIImage imageNamed:@"amex"]];
        
    }else if ([searchedString substringWithRange:[match rangeAtIndex:1]].length > 0){
        
        [_ImageTypeCard setImage:[UIImage imageNamed:@"logo_carnet"]];
       
    }
   
    
    
    
    [_CardNumberFirst setText:@"****"];
    [_CardNumberTwo setText:@"****"];
    [_CardNumberTree setText:@"****"];
    [_CardNumberFour setText:[cardInfo.cardNumber substringWithRange:NSMakeRange(cardInfo.cardNumber.length-4, 4)]];
    
    if (cardInfo.cardImage != nil) {
        [_ImageCard setHidden:NO];
        [_ImageCard setImage:cardInfo.cardImage];
        //        switch ([CreditCard_Validator checkCardBrandWithNumber:cardInfo.cardNumber]) {
        //            case CreditCardBrandMasterCard:
        //                [_ImageTypeCard setImage:[UIImage imageNamed:@"mastercard"]];
        //                break;
        //            case CreditCardBrandVisa:
        //                [_ImageTypeCard setImage:[UIImage imageNamed:@"visa"]];
        //                break;
        //            case CreditCardBrandAmex:
        //                [_ImageTypeCard setImage:[UIImage imageNamed:@"amex"]];
        //                break;
        //            default:
        //                break;
        //        }
      //  [_ImageTypeCard setImage:[UIImage imageNamed:cardType==1?@"visa":cardType==2?@"amex":@"mastercard"]];
    }
    [_cardText setText:cardInfo.cardNumber];
    [self textFieldDidEndEditing:_cardText];
    
    [_cvvText setText:cardInfo.cvv];
    NSString *expYear = [NSString stringWithFormat:@"%d", (int)cardInfo.expiryYear];
    [_validText setText:[NSString stringWithFormat:@"%02d/%@", (int)cardInfo.expiryMonth, [expYear substringWithRange:NSMakeRange(2, 2)]]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
    
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma Mark pootEngine Handler
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
    {
        [self unLockView];
        
        if (manager == stateManager) {
            NSDictionary *response = (NSDictionary*)json;
            if ([response[kIDError] intValue]==0) {
                [_stateText setInfoArray:response[@"estados"]];
                [_stateText setDescriptionLabel:@"nombre"];
                [_stateText setIdLabel:@"id"];
            } else {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
            }
        }
        
        switch (_type) {
            case updateCardTypeNew:
            case updateCardTypeExisting:
            {
                if (manager == updateManager) {
                    NSMutableDictionary *response = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary*)json];
                    [self cleanDictionary:response];
                    
                    if ([response[kIDError] intValue]==0) {
                        [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                        [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        
                        if (_type == updateCardTypeExisting) {
                            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alertMsg setTag:200];
                            [alertMsg show];
                        } else {
                            
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"WalletUpdate" object:self userInfo:nil];
                            [self dismissViewControllerAnimated:YES completion:nil];
                            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:(NSString*)@"NewTarjet"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            //                            if ([self conformsToProtocol:@protocol(cardSelectionDelegate)]&&[self respondsToSelector:@selector(updateCardResponse:)]) {
                            //                                [self updateCardResponse:response];
                            //                            }
                            //
                            //                            double delayInSeconds = 3.0;
                            //                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                            //                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                            //                                Wallet_Ctrl * view = [[Wallet_Ctrl alloc] init];
                            //                                [view updateCardResponse:response];
                            //
                            //                            });
                        }
                    } else {
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alertMsg show];
                    }
                }
            }
            break;
        }
    }
    
#pragma Mark UIAlertView handler
    
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
    {
        
        switch (alertView.tag) {
            case 200:
            {
                //                if ([_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[_delegate respondsToSelector:@selector(updateCardResponse:)]) {
                //                    [_delegate updateCardResponse:nil];
                //                }
                //
                [self dismissViewControllerAnimated:YES completion:nil];
                break;
            }
            default:
            break;
        }
    }
    
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
    {
        switch (buttonIndex) {
            case 0:
            {
                [self dismissViewControllerAnimated:YES completion:^{
                    //                    if ([self->_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[self->_delegate respondsToSelector:@selector(removeCardWithID:)]) {
                    //                        [self->_delegate removeCardWithID:self->_cardToUpdateID];
                    //                    }
                }];
            }
            break;
            default:
            break;
        }
    }
#pragma Mark storyBoards
- (void)showWalletWithType:(walletViewType)viewType;
    {
        @try {
            cardManager = [[pootEngine alloc] init];
            [cardManager setDelegate:self];
            [cardManager setShowComments:developing];
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
            
          //  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
            //[alertMsg show];
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
            [DataUser clean];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
        
    }
    //- (void) showWallet
    //    {
    //        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    //
    //        Wallet_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"wallet_view"];
    //
    //        [self.navigationController pushViewController:nextView animated:YES];
    //
    //
    //    }
    @end
