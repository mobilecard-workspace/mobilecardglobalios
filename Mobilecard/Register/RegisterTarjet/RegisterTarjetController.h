//
//  RegisterTarjetController.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/7/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonTableViewController.h"
#import "CreditCard-Validator.h" 
#import "CardIO.h"
#import "Wallet_Ctrl.h"

NS_ASSUME_NONNULL_BEGIN

//@protocol updateCardDelegate <NSObject>
//
//@required
//- (void)updateCardResponse:(NSDictionary*)response;
//- (void)removeCardWithID:(int)cardID;
//
//@end

@interface RegisterTarjetController : mT_commonTableViewController <cardSelectionDelegate,updateCardDelegate,CardIOPaymentViewControllerDelegate,UITextFieldDelegate, UIAlertViewDelegate, UIActionSheetDelegate>


//@property (nonatomic, assign) id <updateCardDelegate, NSObject> delegate;

@property (nonatomic, assign) updateCardType type;
@property (nonatomic, assign) walletViewType walletType;

@property int cardToUpdateID;


@property (strong, nonatomic) NSDictionary *passedData;

@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cardText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cvvText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *validText;


//datos extra de eua
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressUsaText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityUsaText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipCodeUsaText;




//terminan datos de eua

@property (weak, nonatomic) IBOutlet UITextField_Validations *CardNumberFirst;
@property (weak, nonatomic) IBOutlet UITextField_Validations *CardNumberTwo;
@property (weak, nonatomic) IBOutlet UITextField_Validations *CardNumberTree;
@property (weak, nonatomic) IBOutlet UITextField_Validations *CardNumberFour;


@property (weak, nonatomic) IBOutlet UITextField_Validations *msiText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *TypeTarjetText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *CardStreet;
@property (weak, nonatomic) IBOutlet UITextField_Validations *CardState;
@property (weak, nonatomic) IBOutlet UITextField_Validations *CardCodezip;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@property (weak, nonatomic) IBOutlet UIImageView *ImageCard;
@property (weak, nonatomic) IBOutlet UIImageView *ImageTypeCard;

@end

NS_ASSUME_NONNULL_END
