//
//  RegisterNew_Step2.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/2/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "RegisterNew_Step2.h"
#import <Mobilecard-Swift.h>




@interface RegisterNew_Step2 ()
    {
        pootEngine *registerManager;
        pootEngine *encrypter;
        
        bool addCard;
        bool colombiaSelection;
        NSDictionary *cardInformation;
        
        NSDictionary *userData;
        
        MC_TermsAndConditionsCtrller *termsView;
        
        FBSDKLoginButton *loginButton;
    }
    @end

@implementation RegisterNew_Step2
    
- (void)viewDidLoad {
    [super viewDidLoad];
    colombiaSelection = NO;
    
    
 
        
      
        [self->_phoneText setText:__telefono];
        _emailText.userInteractionEnabled = YES;
    _confirmEmailText.userInteractionEnabled = YES;
        _phoneText.userInteractionEnabled = NO;
    
    NSLog(@"Este es el telefono %@",__telefono);
    
    
    //ponemos el email y telefono que recibimos de la vista pasada
//  [self->_emailText setText:__email];
  //    [self->_phoneText setText:__telefono];
    
    //////////////////// Button Facebook Action
    loginButton.hidden=NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getFacebookData) name:@"getFacebookData" object:nil];
    loginButton.permissions =@[@"public_profile", @"email", @"user_friends"];
    
    loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.frame =  CGRectMake(self.view.bounds.size.width/45,_ViewContentButtonFacebook.center.y-20, 230.0, 35.0);
    
    [_ViewContentButtonFacebook addSubview:loginButton];
    /////////////////////////////////////////////////////
    
    
    //   [self addShadowToView:_addPayment_Button];
    //   [self addShadowToView:_save_Button];
    
    termsView = [[MC_TermsAndConditionsCtrller alloc] initWithNibName:@"MC_TermsAndConditionsCtrller" bundle:nil];
    [termsView setDelegate:self];
    
    //for (UIView* view in self.tableView.subviews) {
    //  [self addShadowToView:view];
    //}
    
    [self initializePickersForView:self.navigationController.view];
    
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_lastnameText setUpTextFieldAs:textFieldTypeLastName];
    [_phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_confirmEmailText setUpTextFieldAs:textFieldTypeEmail];
    [_passwodText setUpTextFieldAs:textFieldTypePassword];
    [_confirmPasswordText setUpTextFieldAs:textFieldTypePassword];
    [_countryText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_documentNumberText setUpTextFieldAs:textFieldTypeName];
    [_documentTypeText setUpTextFieldAs:textFieldTypeRequiredCombo];
    
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_lastnameText];
    [self addValidationTextField:_phoneText];
    [self addValidationTextField:_emailText];
       [self addValidationTextField:_confirmEmailText];
    [self addValidationTextField:_passwodText];
    [self addValidationTextField:_confirmPasswordText];
    
    
    [_countryText setInfoArray:[NSArray arrayWithObjects:
                                [NSDictionary dictionaryWithObjectsAndKeys:@"México", kDescriptionKey, @"1", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Colombia", kDescriptionKey, @"2", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"USA", kDescriptionKey, @"3", kIDKey, nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Perú", kDescriptionKey, @"4", kIDKey, nil], nil]];
    [_countryText setIdLabel:(NSString*)kIDKey];
    [_countryText setDescriptionLabel:(NSString*)kDescriptionKey];
    
    [self addValidationTextField:_documentNumberText];
    [self addValidationTextField:_documentTypeText];
    
    [_documentTypeText setInfoArray:[NSArray arrayWithObjects:
                                     [NSDictionary dictionaryWithObjectsAndKeys:@"CC", kDescriptionKey, @"1", kIDKey, nil],
                                     [NSDictionary dictionaryWithObjectsAndKeys:@"CI", kDescriptionKey, @"2", kIDKey, nil], nil]];
    [_documentTypeText setIdLabel:(NSString*)kIDKey];
    [_documentTypeText setDescriptionLabel:(NSString*)kDescriptionKey];
    
    [self addValidationTextFieldsToDelegate];
    
    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
    
    if (countryInfo) {
        UIPickerView_Automated *picker = [[UIPickerView_Automated alloc] init];
        [picker setTextField:_countryText];
        [self pickerView:picker didSelectRow:[countryInfo[@"id"] intValue]-1 inComponent:0];
    }
}
    
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
    
- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Reg2",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}
- (void)viewDidAppear:(BOOL)animated
    {
        if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"SessionSMS_To_Menu"] isEqualToString:@"1"]){
           // [self performSegueWithIdentifier:@"step3" sender:self];
        }
        //    //////////////
        //    if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"SessionJumio"] isEqualToString:@"1"]){
        //        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"SessionJumio"];
        //        [[NSUserDefaults standardUserDefaults] synchronize];
        //        [self Session_action];
        //    }
    }
    
- (IBAction)register_Action:(id)sender {
    //    [self performSegueWithIdentifier:@"step3" sender:self];
    //   [self performSegueWithIdentifier:@"codeValidation" sender:nil];
    
    for (UITextField_Validations *texty in [self getValidationTextFields]) {
        [texty validateField:0];
        [texty resignFirstResponder];
    }
    
    
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    if (![[_passwodText text] isEqualToString:[_confirmPasswordText text]]) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"La contraseña y la confirmación deben coincidir, intente de nuevo", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (![[_emailText text] isEqualToString:[_confirmEmailText text]]) {
           UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"El correo electrónico y la confirmación deben coincidir, intente de nuevo", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
           [alertMsg show];
           return;
       }

    
    //Validating all inputs
    if (!_termsSwitch.on) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Para continuar, es necesario aceptar los términos y condiciones", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    //Preparing data
    registerManager = [[pootEngine alloc] init];
    [registerManager setDelegate:self];
    [registerManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:_emailText.text forKey:@"email"];
    [params setObject:[registerManager encryptJSONString:_passwodText.text withPassword:nil] forKey:@"password"];
    [params setObject:_nameText.text forKey:@"firstName"];
    [params setObject:_lastnameText.text forKey:@"lastName"];
    [params setObject:_phoneText.text forKey:@"phone"];
    [params setObject:@"M" forKey:@"gender"];
    [params setObject:_countryText.infoArray[_countryText.selectedID][kIDKey] forKey:@"country"];
    
    if (cardInformation) {
        [params setObject:cardInformation forKey:@"card"];
    }
    
    if (colombiaSelection) {
        [params setObject:[_documentNumberText text] forKey:@"cedula"];
        [params setObject:[_documentTypeText infoArray][_documentTypeText.selectedID][kIDKey] forKey:@"tipoCedula"];
    }
    
    //MUST HAVE PARAMETERS
    developing?[params setObject:[NSString stringWithFormat:@"POOT%i", rand()%1000+1] forKey:@"imei"]:[params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"manufacturer"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
    [params setObject:@"iOS" forKey:@"platform"];
    
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [registerManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@%@", userManagementURL, (NSLocalizedString(@"lang", nil)), @"/user/insertv3"] withPost:JSONString];
    
    [self lockViewWithMessage:NSLocalizedString(@"Realizando registro...", nil)];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (IBAction)addCardButtonAction:(id)sender {
    
    if (cardInformation) {
        cardInformation = nil;
    } else {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
        UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"card_NavController"];
        updateCard_Ctrl *nextView = [nav childViewControllers].firstObject;
        [nextView setType:updateCardTypeRegistry];
        [nextView setDelegate:self];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    
    [self updateAddButton];
}
    
- (void) updateAddButton
    {
        if (cardInformation) {
            [_addPayment_Button setTitle:NSLocalizedString(@"ELIMINAR TARJETA", nil) forState:UIControlStateNormal];
        } else {
            [_addPayment_Button setTitle:NSLocalizedString(@"AGREGAR TARJETA", nil) forState:UIControlStateNormal];
        }
    }
    
#pragma Mark updateCardDelegate Handling
-(void)updateCardResponse:(NSDictionary *)response
    {
        if (response) {
            cardInformation = [NSDictionary dictionaryWithDictionary:response];
        }
        
        developing?NSLog(@"CARD INFORMATION -> %@", cardInformation):nil;
        [self updateAddButton];
    }
    
#pragma Mark Terms handling
- (IBAction)termsSwitch_Action:(id)sender {
    if (_termsSwitch.on) {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:termsView];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}
    
- (void)termsAcceptance:(BOOL)result
    {
        _termsSwitch.on = result;
    }
    
#pragma mark mcCard Delegate
    
- (void)mcCardResult:(BOOL)result
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
#pragma mark Segue delegate
    
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        //    if ([[segue identifier] isEqualToString:@"codeValidation"]) {
        //        UINavigationController *nav = [segue destinationViewController];
        //        Register_Step30 *nextView = [[nav viewControllers] firstObject];
        //
        //        [nextView setDelegate:self];
        //
        //        NSDictionary *response = (NSDictionary*)sender;
        //        [nextView setUserData:response];
        //        [nextView setUserID:response[kUserIDKey]];
        //    }
    }
    
#pragma mark SMSdelegate
- (void)registerSMS_Response:(NSDictionary *)userData
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
#pragma Mark pootEngine Delegate
    
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
    {
        [self unLockView];
        
     NSDictionary *response = (NSDictionary*)json;
                response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
                
                if ([response[kIDError] intValue] == 0) {
                    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
                    
                    
                    if (response.count < 1){
                                                                  UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                                                                                         
                                                                                         
                                                                                         [alertMsg show];
                                                                                         return;
                                                              }
                    
                    
                    
                    DataUser.COUNTRY = [countryInfo objectForKey:kDetailsKey];  ////// Raul mendez
                    DataUser.EMAIL =  _emailText.text;
                    DataUser.PASS = @"success";
                    DataUser.IMEI = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                    DataUser.APPID = @"iOS";
                    DataUser.USERTYPE = @"USUARIO";
                    DataUser.OS = [[UIDevice currentDevice] systemVersion];
                    DataUser.COUNTRYID = [[countryInfo objectForKey:kIDKey] stringValue];
                    DataUser.MANOFACTURER =@"Apple";
                    
                   
                    
                    [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                   // [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                 
                    
                    [[NSUserDefaults standardUserDefaults] setObject:@"success" forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                    [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                    
                   
                    
                    
                    switch ([response[kIDJumio] intValue]) {
                    /*    case 0:
                        {
                            
                            if (response.count > 1){
                               [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                           [self performSegueWithIdentifier:@"init_boarding_jumio" sender:nil];
                                return;
                            }else{
                               UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"No es posible establecer conexión con el servidor." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                               
                              
                                [alertMsg show];
                                return;
                                
                            }
                        }
                        break;*/
                        case 1:
                        case 0:
                        case 2:
                        case 3:
                            
                        {
                            
                            
                         
                            
                             switch ([response[kIDStatus] intValue]) {
                            //  switch (100) {
                                case 98:
                                {
                                  //  preliminaryUserData = response;
                                    
                                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                                    [alertMsg setTag:3031];
                                    [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                                    [alertMsg show];
                                    return;
                                }
                                break;
                                
                                default:
                                {
                                    [[NSUserDefaults standardUserDefaults] setObject:response[kScanReferenceKey] forKey:@"enrollmentTransactionReference"];
                                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
              
                                    
                                    [[NSUserDefaults standardUserDefaults] setObject:@"success" forKey:(NSString*)kUserPassword];
                                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                                    [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                                    [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                                    
                                    [[NSUserDefaults standardUserDefaults] setObject:_emailText.text forKey:@"user_email"];
                                              
                                              [[NSUserDefaults standardUserDefaults] setObject:response[@"ideUsuario"] forKey:(NSString*)@"ideUsuario"];
                                              
                                              [[NSUserDefaults standardUserDefaults] synchronize];
                                              DataUser.MAIL_TEMP_REGISTER = nil;
                                              DataUser.PHONE_TEMP_REGISTER = nil;
                                              //para que salga el tutorial en la proxima logeada
                                              DataUser.REGISTERTUTOENABLE = @"1";
                                           //   [self performSegueWithIdentifier:@"codeValidation" sender:response];
                                              
                                      
                                             
                                              UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                           UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"SignInUser"];
                                              
                                              UIStoryboard* storyboardTwo = [UIStoryboard storyboardWithName:@"Startup" bundle:nil];
                                                                   UIViewController* controllerTwo = [storyboardTwo instantiateViewControllerWithIdentifier:@"initialView"];
                                              
                                              
                                              NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];
                                                  [controllers addObject:controllerTwo];
                                                  [controllers addObject:controller];
                                                  [self.navigationController setViewControllers:controllers animated:NO];
                                    
                                   // [self performSegueWithIdentifier:@"main_menu" sender:nil];
                                }
                                break;
                             
                            }
                            
                        }
                        break;
                    /*    case 2:
                        {
                            //       * Si usr_jumio = 2 -> Mostramos mensaje de error de procesando
                            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"¡Aviso!" message:@"Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
                            [alertMsg show];
                            
                            return;
                        }
                        break;*/
                       /* case 3:
                        {
                       
                               [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)@"dataAntesJumio"];
                            [[NSUserDefaults standardUserDefaults] setObject:response[@"ideUsuario"] forKey:(NSString*)@"ideUsuario"];
                            
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [self performSegueWithIdentifier:@"ErrorDocument" sender:nil];
                            return;
                        }
                        break;*/
                    }
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
    }
    
- (void)managerFailed:(pootEngine *)manager withKey:(NSString *)key errorCode:(pootEngineErrorCode)error
    {
        [self unLockView];
    }
    
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
    {
        if (alertView.tag == 1000) { //REGISTER SUCCESSFULL
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
        
        if (alertView.tag == 90) {
            switch (buttonIndex) {
                case 0:
                {
                    [self.navigationController dismissViewControllerAnimated:YES completion:^{
                        if ([_delegate conformsToProtocol:@protocol(registerProtocol)]&&[_delegate respondsToSelector:@selector(registryShallLaunchMyMCWithUserData:)]) {
                            [_delegate registryShallLaunchMyMCWithUserData:userData];
                        }
                    }];
                }
                break;
                
                default:
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                break;
            }
            
        }
    }

- (IBAction)privacyAction:(UIButton *)sender {
    
    //puente para mostrar el aviso de privacidad
    
    [SendTokenFirestore privacyMexWithView:self];
    
 
   
    
}


- (void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
    {
        [self subPickerView:pickerView didSelectRow:row inComponent:component];
        
        if (pickerView.textField == _countryText) {
            colombiaSelection = [[_countryText infoArray][[_countryText selectedID]][[_countryText idLabel]] intValue]==2?YES:NO;
            
            [_documentNumberText setHidden:!colombiaSelection];
            [_documentTypeText setHidden:!colombiaSelection];
            
            [_documentNumberText setEnabled:colombiaSelection];
            [_documentTypeText setEnabled:colombiaSelection];
            
            [[self tableView] reloadData];
        }
    }
    
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        switch (indexPath.section) {
            case 0: //PERSONAL INFORMATION
            switch (indexPath.row) {
                case 0:
                return 100; //HEADER 160
                break;
                case 1:
                return colombiaSelection?450:380; //INFORMATION
                break;
                default:
                return 40; //No use
                break;
            }
            break;
            case 1: //ADDITIONAL INFORMATION
            switch (indexPath.row) {
                case 0:
                return 20; //Space
                break;
                case 1: //Privacy notes
                return 100;
                break;
                case 2: //Button
                return 85;
                break;
                
                default: //No use
                return 40;
                break;
            }
            break;
            
            default:
            return 40;
            break;
        }
    }
    
#pragma mark - FacebookData Delegate
- (void)getFacebookData{
    if ([FBSDKAccessToken currentAccessToken]) {
        if ([FBSDKAccessToken currentAccessToken]) {
            // User is logged in, do work such as go to next view controller.
            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"me"]
                                                                           parameters:@{@"fields": @"id,name,first_name,middle_name,last_name,picture,birthday,email,gender,location"}
                                                                           HTTPMethod:@"GET"];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                  id result,
                                                  NSError *error) {
                if (!error) {
                    @try {
                        
                        //[self->_emailText setText:[result objectForKey:@"email"]];
                        [self->_nameText setText:[result objectForKey:@"first_name"]];
                        
                        NSString *last_name=[result objectForKey:@"last_name"];
                        NSArray *array_last_name = [last_name componentsSeparatedByString:@" "];
                        
                        [self->_lastnameText setText:[array_last_name objectAtIndex:0]];
                        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                        [login logOut];
                        
                    }
                    @catch (NSException * e) {
                        NSLog(@"Code line: %d %@ Exception: %@ ",__LINE__,NSStringFromClass([self class]), e );
                        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                        [login logOut];
                    }
                } else {
                    // fail
                }}
             ];
        }
    }
}
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    NSLog(@"Log Out FB");
}
- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    NSLog(@"%@",error);
}
- (void)Session_action{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"SignInUser"];
    [self.navigationController pushViewController:controller animated:YES];
}
    @end
