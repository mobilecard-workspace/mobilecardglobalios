//
//  RegisterNew_Step1.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/1/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "RegisterNew_Step1.h"
#import <Mobilecard-Swift.h>

@interface RegisterNew_Step1 ()

@end

@implementation RegisterNew_Step1


- (void)viewDidAppear:(BOOL)animated{
    
    
    
    
    //agregado para solucionar problema de navegacion al regresar del registro de peru al tutorial y asi mandarte al login
    if ([DataUser.STATELOGIN isEqualToString:@"0"]) {
        
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
            
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainCommerce" bundle:nil];
            UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"SignInCommerce"];
            DataUser.STATELOGIN = nil;
            [self.navigationController pushViewController:controller animated:NO];
        }else{
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"SignIn_Ctrl"];
            DataUser.STATELOGIN = nil;
            [self.navigationController pushViewController:controller animated:NO];
            
            
        }
    }// termina operacion de navegacion
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_infoCollection setFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height*0.4)];
    [_pageControl setFrame:CGRectMake(0, 40+self.view.frame.size.height*0.4, self.view.frame.size.width, 150)];
    _pageControl.transform = CGAffineTransformMakeScale(3, 3);
    
    [_pageControl setNumberOfPages:[self loadArrayWhiteKUserCountry].count];
    
    [self addShadowToView:_pageControl];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self loadArrayWhiteKUserCountry].count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NSString stringWithFormat:@"page%ld", indexPath.row+1] forIndexPath:indexPath];
    
    UIImageView * image_page= (UIImageView *) [cell.contentView viewWithTag:1];
    [image_page setImage:[UIImage imageNamed:[[self loadArrayWhiteKUserCountry] objectAtIndex:indexPath.row]]];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(collectionView.frame)-40, (CGRectGetHeight(collectionView.frame))-25);
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = _infoCollection.frame.size.width;
    _pageControl.currentPage = _infoCollection.contentOffset.x / pageWidth;
    
    [self refreshButton];
}
- (IBAction)pageControl_Action:(id)sender {
    [_infoCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_pageControl.currentPage inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    
    [self refreshButton];
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (void)refreshButton
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    
    [UIView commitAnimations];
}

//metodo del boton de registrarse
- (IBAction)Continue_action:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
        
     // [self performSegueWithIdentifier:@"step2" sender:self];
        //ahora se inicia pidiendo el sms
        
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];

        RegisterSmsViewController *nextvc = [nextStory instantiateViewControllerWithIdentifier:@"smsRegister"];
        nextvc.isFrom = 0;
        
        [self.navigationController pushViewController:(nextvc) animated:YES];
        
        
        
    } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
        
        
    
        
        if ([DataUser.COUNTRY isEqualToString:@"PE"]) {
            
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
            
            RegisterSmsViewController *nextvc = [nextStory instantiateViewControllerWithIdentifier:@"smsRegister"];
            //se va con comercio  peru
            nextvc.isFrom = 2;
            
            [self.navigationController pushViewController:(nextvc) animated:YES];
            
            
            
        }else{
            //aca es cuando es mexico preguntar antes welcomepack
            
            WelcomePackInitial *welcomePack = [[WelcomePackInitial alloc] init];
            
            welcomePack.delegate = self;
            
            welcomePack.providesPresentationContextTransitionStyle = YES;
            welcomePack.definesPresentationContext = YES;
            welcomePack.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            welcomePack.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
             [self presentViewController:welcomePack animated:YES completion:nil];
            
            
            //esto borrarlo al activar welcome pack
           /* UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
                        
                        RegisterSmsViewController *nextvc = [nextStory instantiateViewControllerWithIdentifier:@"smsRegister"];
                        //se va con comercio todos menos peru
                        nextvc.isFrom = 1;
                        
                        [self.navigationController pushViewController:(nextvc) animated:YES];
        //hasta aqui
          */
            
        }
    }
  
    
}
- (IBAction)Session_action:(id)sender {
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"SignInUser"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"MainCommerce" bundle:nil];
        UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"SignInCommerce"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
-(NSArray *) loadArrayWhiteKUserCountry{
    
    NSArray * arrayImages= nil;
    NSDictionary *countryInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
        
        switch ([[countryInfo objectForKey:kIDKey] intValue]) {
            case 1:  //MEXICO
            {
                arrayImages = [[NSArray alloc] initWithObjects:@"slide_user_step_1",@"slide_user_step_2",@"slide_user_step_3",@"slide_user_step_4",@"slide_user_step_5",@"slide_user_step_6",@"slide_user_step_7",@"slide_user_step_8", nil];
            }
            break;
            case 2: //COLOMBIA
            {
                arrayImages = [[NSArray alloc] initWithObjects:@"slide_user_step_1",@"slide_user_step_2",@"slide_user_step_3", nil];
            }
            break;
            case 3: //USA
            {
                arrayImages = [[NSArray alloc] initWithObjects:@"slide_user_step_1",@"slide_user_step_2",@"slide_user_step_3", nil];
            }
            break;
            case 4: //PERU
            {
                arrayImages = [[NSArray alloc] initWithObjects:@"slide_peru_1_1",@"slide_peru_1_2",@"slide_peru_1_3",@"slide_peru_1_4", nil];
            }
            break;
            
        }
    } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
        
        switch ([[countryInfo objectForKey:kIDKey] intValue]) {
            case 1:  //MEXICO
            {
                arrayImages = [[NSArray alloc] initWithObjects:@"slide_commerce_step_1",@"slide_commerce_step_2",@"slide_commerce_step_3",@"slide_commerce_step_4",@"slide_commerce_step_5", nil];
            }
            break;
            case 2: //COLOMBIA
            {
                arrayImages = [[NSArray alloc] initWithObjects:@"slide_commerce_step_1",@"slide_commerce_step_2",@"slide_commerce_step_3",@"slide_commerce_step_4",@"slide_commerce_step_5", nil];            }
            break;
            case 3: //USA
            {
                arrayImages = [[NSArray alloc] initWithObjects:@"slide_commerce_step_1",@"slide_commerce_step_2",@"slide_commerce_step_3",@"slide_commerce_step_4",@"slide_commerce_step_5", nil];            }
            break;
            case 4: //PERU
            {
                arrayImages = [[NSArray alloc] initWithObjects:@"carruselComercioPER1",@"carruselComercioPER2"
                    ,@"carruselComercioPER3"
                    ,@"carruselComercioPER4"
                    ,@"carruselComercioPER5", nil];
            }
            break;
            
        }
    }
    return arrayImages;
}


//metodos delegado welcome pack register

//cancelan la vista
- (void)welcomePackBackAction {
    NSLog(@"dio back");
}

//registro normal
- (void)registerNormal {
    
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
              
              RegisterSmsViewController *nextvc = [nextStory instantiateViewControllerWithIdentifier:@"smsRegister"];
              //se va con comercio todos menos peru
              nextvc.isFrom = 1;
              
              [self.navigationController pushViewController:(nextvc) animated:YES];

}
//registro welcomePack
- (void)welcomeRegister {
 NSLog(@"registro welcomePack");
    
    
     UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
               
               WelcomePackInstructionsViewController *nextvc = [nextStory instantiateViewControllerWithIdentifier:@"welcomePackIdentifier"];
          
               
               [self.navigationController pushViewController:(nextvc) animated:YES];
}




@end
