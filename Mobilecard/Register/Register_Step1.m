//
//  Register_Step1.m
//  Mobilecard
//
//  Created by David Poot on 11/2/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "Register_Step1.h"
#import "Register_Step2.h"

@interface Register_Step1 ()
{
    NSMutableDictionary *params;
    
    pootEngine *encrypter;
}

@end

@implementation Register_Step1

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self addShadowToView:self.navigationController.navigationBar];
   // [self addShadowToView:_continue_Button];
    
    encrypter = [[pootEngine alloc] init];
    [encrypter setShowComments:developing];
    
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_passwordText setUpTextFieldAs:textFieldTypePassword];
    [_passwordConfirmText setUpTextFieldAs:textFieldTypePassword];
    
    [self addValidationTextField:_emailText];
    [self addValidationTextField:_passwordText];
    [self addValidationTextField:_passwordConfirmText];
    
    [self addValidationTextFieldsToDelegate];
    
    params = [[NSMutableDictionary alloc] init];
    
    [params removeAllObjects];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continue_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    if (![[_passwordText text] isEqualToString:[_passwordConfirmText text]]) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"La contraseña y la confirmación deben coincidir, intente de nuevo", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    [params setObject:_emailText.text forKey:@"email"];
    [params setObject:[encrypter encryptJSONString:_passwordText.text withPassword:nil] forKey:@"password"];
    
    [self performSegueWithIdentifier:@"step2" sender:self];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Reg1",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step2"]) {
        Register_Step2 *nextView = [segue destinationViewController];
        
        [nextView setDelegate:_delegate];
        [nextView setParams:params];
    }
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
