//
//  favorites_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 1/6/18.
//  Copyright © 2018 David Poot. All rights reserved.
//
#define _rowIcondelete 50
#define _rowButtonDelete 100

#import "favorites_Ctrl.h"

#import "payment_recarga_ctrl_Tae.h"
#import "payment_recarga_ctrl_Toll.h"
#import "blackstone_Step2.h"

@interface favorites_Ctrl ()
{
    NSMutableArray *favoritesArray;
    
    NSMutableArray *cellsArray;
    
    int selectedID;
}
@end

@implementation favorites_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    cellsArray = [[NSMutableArray alloc] init];
    
    [self loadItemsOfView];
    
    //
    //    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.tableView.bounds.size.height/2+self.view.bounds.size.height/5, self.tableView.bounds.size.width, 130)];
    //    footerView.backgroundColor =[UIColor clearColor];
    //   // [footerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"profile"]]];
    //
    //    _homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [_homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
    //    [_homeButton addTarget:self action:@selector(back_Action:) forControlEvents:UIControlEventTouchUpInside];
    //    _walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [_walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
    //    [_walletButton addTarget:self action:@selector(showWallet:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    _favoritesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [_favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
    //    _myMCButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [_myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_history", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
    //    [_myMCButton addTarget:self action:@selector(showHistory:) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [_homeButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*1-40+10, 20, 58, 60)];
    //    [_walletButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*2.3-40+10, 20, 58, 60)];
    //    [_favoritesButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*3.6-40+10, 20, 60, 60)];
    //    [_myMCButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*5-40+10, 20, 60, 60)];
    //
    //    [footerView addSubview:_walletButton];
    //    [footerView addSubview:_homeButton];
    //    [footerView addSubview:_favoritesButton];
    //    [footerView addSubview:_myMCButton];
    //    [self.view addSubview:footerView];
    //
    //
    [self setupMenu];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self loadItemsOfView];
    [self.tableView reloadData];
    
    [super viewWillAppear:animated];
}

- (void)loadItemsOfView
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]) {
        favoritesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]];
    } else {
        favoritesArray = [[NSMutableArray alloc] init];
    }
    
    [cellsArray removeAllObjects];
    
    [cellsArray addObject:[NSDictionary  dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"70", kHeightKey, nil]];
    
    for (NSDictionary *element in favoritesArray) {
        [cellsArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"generic", kIdentifierKey, element, kDetailsKey, @"90", kHeightKey, nil]];
        [cellsArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"", kDetailsKey, @"10", kHeightKey, nil]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cellsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [cellsArray[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = cellsArray[indexPath.row][kIdentifierKey];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if ([identifier isEqualToString:@"Header"]) {
        buttonEdit =(UIButton*)[cell viewWithTag:1];
        [buttonEdit addTarget:self action:@selector(showIcondelete:) forControlEvents:UIControlEventTouchUpInside];
        if ([favoritesArray count] == 0) {
            [buttonEdit setHidden:YES];
        }
    }
    
    if ([identifier isEqualToString:@"generic"]) {
        switch ([cellsArray[indexPath.row][kDetailsKey][@"type"] intValue]) {
            case favoriteTopUpTae: //RECARGA
            case favoriteTopUpToll:
                
                for (id element in cell.contentView.subviews) {
                    if ([element isKindOfClass:[UILabel class]]) {
                        UILabel *label = element;
                        switch (label.tag) {
                            case 0:
                                [label setText:cellsArray[indexPath.row][kDetailsKey][@"dataSource"][kDescriptionKey]];
                                break;
                                
                            default:
                                [label setText:cellsArray[indexPath.row][kDetailsKey][@"amountDataSource"][0][@"concepto"]];
                                break;
                        }
                    }
                    
                    if ([element isKindOfClass:[UIImageView class]]) {
                        UIImageView *image = element;
                        if (image.tag == 1) {
                            [image setImage:[UIImage imageNamed:cellsArray[indexPath.row][kDetailsKey][@"dataSource"][kDescriptionKey]]];
                        }
                    }
                    
                    if ([element isKindOfClass:[UIButton class]]) {
                        UIButton *button = element;
                        if (button.tag == 3) {
                            [button setTag:indexPath.row+_rowIcondelete];
                            [button addTarget:self action:@selector(showdeleteItemFavorites:) forControlEvents:UIControlEventTouchUpInside];
                            
                        }else if (button.tag == 4) {
                            [button setTag:indexPath.row+_rowButtonDelete];
                            //  [button setHidden:ValidationShowIconFavorites];
                            
                            [button addTarget:self action:@selector(deleteItemFavorites:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        [button setHidden:YES];
                    }
                }
                break;
            case favoriteTopUpUSA: //Recarga Blackstone
                for (id element in cell.contentView.subviews) {
                    if ([element isKindOfClass:[UILabel class]]) {
                        UILabel *label = element;
                        switch (label.tag) {
                            case 0:
                                [label setText:cellsArray[indexPath.row][kDetailsKey][@"dataSource"][@"name"]];
                                break;
                                
                            default:
                                [label setText:cellsArray[indexPath.row][kDetailsKey][@"dataSource"][@"carrierName"]];
                                break;
                        }
                    }
                    
                    if ([element isKindOfClass:[UIImageView class]]) {
                        UIImageView *image = element;
                        if (image.tag == 1) {
                            [image setImage:[UIImage imageNamed:cellsArray[indexPath.row][kDetailsKey][@"dataSource"][kDescriptionKey]]];
                        }
                    }
                }
                
            default:
                break;
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch ([cellsArray[indexPath.row][kDetailsKey][@"type"] intValue]) {
        case favoriteTopUpToll: //RECARGA
            selectedID = (int)indexPath.row;
            [self performSegueWithIdentifier:@"payment_recarga_toll" sender:nil];
            break;
        case favoriteTopUpTae:
            selectedID = (int)indexPath.row;
            [self performSegueWithIdentifier:@"payment_recarga_tae" sender:nil];
            break;
        case favoriteTopUpUSA:
        {
            selectedID = (int)indexPath.row;
            
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"blackstone" bundle:nil];
            blackstone_Step2 *nextView = [nextStory instantiateViewControllerWithIdentifier:@"blackstone_TopUp"];
            [nextView setDataSource:cellsArray[selectedID][kDetailsKey][@"dataSource"]];
            
            [self.navigationController pushViewController:nextView animated:YES];
        }
        default:
            break;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"payment_recarga_toll"]) {
        payment_recarga_ctrl_Toll *nextView = [segue destinationViewController];
        [nextView setDataSource:cellsArray[selectedID][kDetailsKey][@"dataSource"]];
        [nextView setAmountDataSource:cellsArray[selectedID][kDetailsKey][@"amountDataSource"]];
        
        switch ([cellsArray[selectedID][kDetailsKey][@"dataSource"][kIDKey] intValue]) {
            case 6:
                [nextView setType:serviceTypeIAVE];
                break;
            case 8:
                [nextView setType:serviceTypePASE];
                break;
                
            default:
                break;
        }
    }
    
    if ([[segue identifier] isEqualToString:@"payment_recarga_tae"]) {
        payment_recarga_ctrl_Tae *nextView = [segue destinationViewController];
        [nextView setDataSource:cellsArray[selectedID][kDetailsKey][@"dataSource"]];
        [nextView setAmountDataSource:cellsArray[selectedID][kDetailsKey][@"amountDataSource"]];
        
        switch ([cellsArray[selectedID][kDetailsKey][@"dataSource"][kIDKey] intValue]) {
            case 6:
                [nextView setType:serviceTypeIAVE];
                break;
            case 8:
                [nextView setType:serviceTypePASE];
                break;
                
            default:
                break;
        }
    }
}
- (void)showIcondelete:(id)sender
{
    
    //    [self.tableView reloadData];
    for (int i = 0; i < cellsArray.count; i++)
    {
        if (!ValidationShowIconFavorites) {
            ValidationShowIconFavorites = TRUE;
            [buttonEdit setImage:[UIImage imageNamed:@"btn_save_wallet"] forState:UIControlStateNormal];
            
            NSString *identifier = cellsArray[i][kIdentifierKey];
            if ([identifier isEqualToString:@"generic"]) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                
                UIButton *buttonShowDelete =(UIButton*)[cell viewWithTag:i+_rowIcondelete];
                [buttonShowDelete setHidden:ValidationShowIconFavorites];
            }
        }else{
            if (!ValidationShowIconFavorites) {
                ValidationShowIconFavorites = TRUE;
                [buttonEdit setImage:[UIImage imageNamed:@"btn_save_wallet"] forState:UIControlStateNormal];
                NSString *identifier = cellsArray[i][kIdentifierKey];
                if ([identifier isEqualToString:@"generic"]) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                    
                    UIButton *buttonShowDelete =(UIButton*)[cell viewWithTag:i+_rowIcondelete];
                    [buttonShowDelete setHidden:ValidationShowIconFavorites];
                }
            }else{
                ValidationShowIconFavorites = FALSE;
                [buttonEdit setImage:[UIImage imageNamed:@"btn_edit_wallet"] forState:UIControlStateNormal];
                NSString *identifier = cellsArray[i][kIdentifierKey];
                if ([identifier isEqualToString:@"generic"]) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                    
                    UIButton *buttonShowDelete =(UIButton*)[cell viewWithTag:i+_rowIcondelete];
                    [buttonShowDelete setHidden:ValidationShowIconFavorites];
                }
            }
        }
        
    }
}
- (void)showdeleteItemFavorites:(id)sender
{
    NSInteger i = [sender tag]-_rowIcondelete;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    UIButton *buttonEdit =(UIButton*)[cell viewWithTag:i+_rowButtonDelete];
    [buttonEdit setHidden:NO];
    
    //  NSString * value =cellsArray[i][kDetailsKey][@"dataSource"][kDescriptionKey];
    IndexDeleteItem = [[NSMutableDictionary alloc] init];
    [IndexDeleteItem setObject:cellsArray[i][kDetailsKey][@"dataSource"] forKey:@"dataSource"];
    [IndexDeleteItem setObject:cellsArray[i][kDetailsKey][@"amountDataSource"] forKey:@"amountDataSource"];
    [IndexDeleteItem setObject:[NSString stringWithFormat:@"%d", favoriteTopUpTae] forKey:@"type"];
    
}
- (void)deleteItemFavorites:(id)sender
{
    NSInteger i= [sender tag]-_rowButtonDelete;
    NSString * value =cellsArray[i][kDetailsKey][@"dataSource"][kDescriptionKey];
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Deseas eliminar a %@ de favoritos",value] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Sí", nil), nil];
    [alertMsg setTag:100];
    [alertMsg show];
}
#pragma Mark UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (!(buttonIndex == 0)) {
        switch (alertView.tag) {
            case 100: //new password request
            {
                NSMutableArray *_favoritesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]];
                
                [_favoritesArray removeObject:IndexDeleteItem];
                
                [[NSUserDefaults standardUserDefaults] setObject:_favoritesArray forKey:(NSString*)kUserFavoritesKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                cellsArray = [[NSMutableArray alloc] init];
                
                [self loadItemsOfView];
                [self.tableView reloadData];
            }
                break;
            default:
                break;
        }
    }
}
- (BOOL)verifyStatus
{
    if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 99)) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
        [alertMsg setTag:99];
        [alertMsg show];
        return NO;
    }
    
    if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 100)) {
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
            UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
            Register_Step30 *nextView = [[nav viewControllers] firstObject];
            
            [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
            [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
            
            [self.navigationController presentViewController:nav animated:YES completion:nil];
            
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        }];
        
        [alertMsg addAction:ok];
        [alertMsg addAction:cancel];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        
        return NO;
    }
    return YES;
}
- (void) setupMenu {
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height/2+self.view.bounds.size.height/4.5, self.view.bounds.size.width, 120)];
    
    [footerView setBackgroundColor:[UIColor whiteColor]];
    UIButton * homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * favoritesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * myMCButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIStackView *stackView = [[UIStackView alloc] init];
    
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    switch ([userData[kUserIdCountry] intValue]) {
        case 1:  case 2: case 3:  //MEXICO //COLOMBIA //USA
            [homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
            [homeButton setContentMode:UIViewContentModeScaleAspectFill];
            [homeButton addTarget:self action:@selector(back_Action:) forControlEvents:UIControlEventTouchUpInside];
            [homeButton.heightAnchor constraintEqualToConstant:75].active = true;
            [homeButton.widthAnchor constraintEqualToConstant:75].active = true;
            
            
            [walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            [walletButton setContentMode:UIViewContentModeScaleAspectFill];
            [walletButton addTarget:self action:@selector(showWallet:) forControlEvents:UIControlEventTouchUpInside];
            [walletButton.heightAnchor constraintEqualToConstant:75].active = true;
            [walletButton.widthAnchor constraintEqualToConstant:75].active = true;
            
            [favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            [favoritesButton setContentMode:UIViewContentModeScaleAspectFill];
            // [favoritesButton addTarget:self action:@selector(showFavorites:) forControlEvents:UIControlEventTouchUpInside];
            [favoritesButton.heightAnchor constraintEqualToConstant:75].active = true;
            [favoritesButton.widthAnchor constraintEqualToConstant:75].active = true;
            
            [myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_history", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            [myMCButton setContentMode:UIViewContentModeScaleAspectFill];
            [myMCButton addTarget:self action:@selector(showHistory:) forControlEvents:UIControlEventTouchUpInside];
            [myMCButton.heightAnchor constraintEqualToConstant:75].active = true;
            [myMCButton.widthAnchor constraintEqualToConstant:75].active = true;
            
            
            //Stack View
            //stackView.axis = UILayoutConstraintAxisVertical;
            stackView.axis = UILayoutConstraintAxisHorizontal;
            // stackView.distribution = UIStackViewDistributionEqualSpacing;
            stackView.distribution = UIStackViewDistributionFillEqually;
            // stackView.alignment = UIStackViewAlignmentCenter;
            stackView.alignment = UIStackViewAlignmentFill;
            stackView.spacing = 8;
            
            [stackView addArrangedSubview:homeButton];
            [stackView addArrangedSubview:walletButton];
            [stackView addArrangedSubview:favoritesButton];
            [stackView addArrangedSubview:myMCButton];
            
            stackView.translatesAutoresizingMaskIntoConstraints = false;
            [footerView addSubview:stackView];
            
            [stackView.centerXAnchor constraintEqualToAnchor:footerView.centerXAnchor].active = true;
            [stackView.centerYAnchor constraintEqualToAnchor:footerView.centerYAnchor].active = true;
            [self.view addSubview:footerView];
            
            break;
            
            //
            //            [homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
            //            [homeButton setContentMode:UIViewContentModeScaleAspectFill];
            //            [homeButton.heightAnchor constraintEqualToConstant:75].active = true;
            //            [homeButton.widthAnchor constraintEqualToConstant:75].active = true;
            //
            //
            //            [walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            //            [walletButton setContentMode:UIViewContentModeScaleAspectFill];
            //            [walletButton addTarget:self action:@selector(showWallet) forControlEvents:UIControlEventTouchUpInside];
            //            [walletButton.heightAnchor constraintEqualToConstant:75].active = true;
            //            [walletButton.widthAnchor constraintEqualToConstant:75].active = true;
            //
            //            [favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            //            [favoritesButton setContentMode:UIViewContentModeScaleAspectFill];
            //            [favoritesButton addTarget:self action:@selector(startFavorites) forControlEvents:UIControlEventTouchUpInside];
            //            [favoritesButton.heightAnchor constraintEqualToConstant:75].active = true;
            //            [favoritesButton.widthAnchor constraintEqualToConstant:75].active = true;
            //
            //
            //            //Stack View
            //            //stackView.axis = UILayoutConstraintAxisVertical;
            //            stackView.axis = UILayoutConstraintAxisHorizontal;
            //            // stackView.distribution = UIStackViewDistributionEqualSpacing;
            //            stackView.distribution = UIStackViewDistributionFillEqually;
            //            // stackView.alignment = UIStackViewAlignmentCenter;
            //            stackView.alignment = UIStackViewAlignmentFill;
            //            stackView.spacing = 8;
            //
            //            [stackView addArrangedSubview:homeButton];
            //            [stackView addArrangedSubview:walletButton];
            //            [stackView addArrangedSubview:favoritesButton];
            //
            //            stackView.translatesAutoresizingMaskIntoConstraints = false;
            //            [footerView addSubview:stackView];
            //
            //            [stackView.centerXAnchor constraintEqualToAnchor:footerView.centerXAnchor].active = true;
            //            [stackView.centerYAnchor constraintEqualToAnchor:footerView.centerYAnchor].active = true;
            //            [self.view addSubview:footerView];
            //            break;
    }
    
    
}
- (void) showFavorites:(id)sender
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *nextView = [nextStory instantiateViewControllerWithIdentifier:@"Favorites"];
    [self.navigationController pushViewController:nextView animated:YES];
}
- (void) showWallet:(id)sender
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    Wallet_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"wallet_view"];
    [self.navigationController pushViewController:nextView animated:YES];
}

- (void) showHistory:(id)sender
{
    if ([self verifyStatus]) {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"statement" bundle:nil];
        [self.navigationController pushViewController:[nextStory instantiateInitialViewController] animated:YES];
    }
}
@end
