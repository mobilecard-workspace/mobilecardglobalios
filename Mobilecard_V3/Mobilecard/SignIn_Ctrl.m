//
//  SignIn_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 10/25/17.
//  Copyright © 2017 David Poot. All rights reserved.
//


#import "SignIn_Ctrl.h"
#import "MainMenu_Ctrl.h"
#import "NSDictionary (keychain).h"
#import "RegisterNew_Step1.h"

@interface SignIn_Ctrl ()
{
    pootEngine *loginManager;
    pootEngine *changeManager;
    
    NSDictionary *preliminaryUserData;
}

@end

@implementation SignIn_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(40, 0, 0, 0)];
//    [self addShadowToView:self.navigationController.navigationBar];
//    [self addShadowToView:_SignIn_Button];
    
    [_usernameText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_usernameText setMaxLength:50];
    [_usernameText setMinLength:4];
    [_passText setUpTextFieldAs:textFieldTypePassword];
    
    [self addValidationTextField:_usernameText];
    [self addValidationTextField:_passText];
    
    [self addValidationTextFieldsToDelegate];
    
   // [self addShadowToView:self.tableView.backgroundView];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey] && [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword]) {
        
        developing?NSLog(@"Keychain -> %@", kKeychainGroup):nil;
        [self performSegueWithIdentifier:@"main_menu" sender:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
 
}

- (void)viewDidAppear:(BOOL)animated{
    if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"SessionSMS_To_Menu"] isEqualToString:@"1"]){
        [self startSession_Action:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"SessionSMS_To_Menu"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}



- (IBAction)back_Action:(id)sender {
    
    
 
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[RegisterNew_Step1 class]]){
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
    
    
  
    
   // [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)register_Action:(id)sender {
//    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
//    UINavigationController *nav = [nextStory instantiateInitialViewController];
//    Register_Step1 *nextView = [nav childViewControllers].firstObject;
//
//    [nextView setDelegate:self];
//    [self presentViewController:nav animated:YES completion:nil];
//
//
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"register_look_and_feel"bundle:nil];
    UIViewController *scrollview =(UIViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Register_Step2"];
    [self.navigationController pushViewController:scrollview animated:YES];
    
    //////// Raul mendez    Register_Step2
}

- (IBAction)startSession_Action:(id)sender {
    [self dismissAllTextFields];
    
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    loginManager = [[pootEngine alloc] init];
    [loginManager setDelegate:self];
    [loginManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:_usernameText.text forKey:@"usrLoginOrEmail"];
    [params setObject:[loginManager encryptJSONString:_passText.text withPassword:nil] forKey:kUserPassword];
    //MUST HAVE PARAMETERS
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"manufacturer"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
    [params setObject:@"iOS" forKey:@"platform"];
    [params setObject:@"USUARIO" forKey:@"tipoUsuario"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    
    NSString *idCountry = [[NSString alloc] init];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"]) {
        idCountry = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"];
    } else {
        idCountry = @"1";
    }
    
    [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, idCountry, (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
    
    [self lockViewWithMessage:NSLocalizedString(@"Iniciando sesión...", nil)];
}

- (IBAction)resetPassword_Action:(id)sender {
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Recuperar contraseña", nil) message:NSLocalizedString(@"Escribe tu nombre de usuario o correo electrónico en el campo para recuperar tu contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Recuperar contraseña", nil), nil];
    [alertMsg setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alertMsg setTag:199];
    [alertMsg show];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"main_menu"]) {
        UINavigationController *navController = segue.destinationViewController;
        MainMenu_Ctrl *main_controller = [navController childViewControllers].firstObject;
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == loginManager) {
        NSDictionary *response = (NSDictionary*)json;
        response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
        
        if ([response[kIDError] intValue] == 0) {
            switch ([response[kIDStatus] intValue]) {
                case 98:
                {
                    preliminaryUserData = response;
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                    [alertMsg setTag:3031];
                    [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                    [alertMsg show];
                    return;
                }
                    break;
                    
                default:
                {
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                    [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                    
                    [self performSegueWithIdentifier:@"main_menu" sender:nil];
                }
                    break;
                    ////////////////////  Validación de idUsrStatus   Raul Mendez ///////////////////////////
                case 100:
                {
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"USUARIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                    [response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                    
                    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
                        UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
                        Register_Step30 *nextView = [[nav viewControllers] firstObject];
                        
                        [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                        [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
                        
                        [self.navigationController presentViewController:nav animated:YES completion:nil];
                        
                        [alertMsg dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                    }];
                    
                    [alertMsg addAction:ok];
                    [alertMsg addAction:cancel];
                    
                    [self presentViewController:alertMsg animated:YES completion:nil];
                    
                    return;
                }
                    break;
            }
        } else {
            
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (changeManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        switch (changeManager.tag) {
            case 3031:
            {
                if ([response[kIDError] intValue] == 0) {
                    
                    [preliminaryUserData setValue:@"1" forKey:@"idUsrStatus"];
                    [[NSUserDefaults standardUserDefaults] setObject:_validationText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:preliminaryUserData forKey:(NSString*)kUserDetailsKey];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                    
                    [self performSegueWithIdentifier:@"main_menu" sender:nil];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
            }
                break;
            case 199:
            {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark register Delegate

- (void)registryShallLaunchMyMCWithUserData:(NSDictionary *)userdata
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
    UINavigationController *nav = [storyboard instantiateInitialViewController];
    MCCard_Ctrl *nextView = [nav childViewControllers].firstObject;
    [nextView setUserData:userdata];
    
    [super setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [super presentViewController:nav animated:YES completion:nil];
}

#pragma Mark UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (!(buttonIndex == 0)) {
        switch (alertView.tag) {
            case 3031: //new password request
            {
                [_validationText setUpTextFieldAs:textFieldTypePassword];
                [_validationText setText:[alertView textFieldAtIndex:0].text];
                
                if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                    changeManager = [[pootEngine alloc] init];
                    [changeManager setDelegate:self];
                    [changeManager setShowComments:developing];
                    [changeManager setTag:alertView.tag];
                    
                    NSString *post = nil;
                    post = [NSString stringWithFormat:@"old=%@&new=%@", [self convertToURLCompatible:[changeManager encryptJSONString:_passText.text withPassword:nil]], [self convertToURLCompatible:[changeManager encryptJSONString:_validationText.text withPassword:nil]]];
                    
                    [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@", userManagementURL,NSLocalizedString(@"lang", nil),@"/",[preliminaryUserData objectForKey:kUserIDKey],@"/password/update"] withPost:post];
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                }
                
            }
                break;
                
            case 199: //reset password
            {
                [_validationText setUpTextFieldAs:textFieldTypeUserName];
                [_validationText setText:[alertView textFieldAtIndex:0].text];
                
                if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                    changeManager = [[pootEngine alloc] init];
                    [changeManager setDelegate:self];
                    [changeManager setShowComments:developing];
                    [changeManager setTag:alertView.tag];
                    
                    
                    NSString *post = nil;
                    post = [NSString stringWithFormat:@"userOrEmail=%@", _validationText.text];
                    
                    [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/password/reset"] withPost:post];
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                }
            }
            default:
                break;
        }
    }
}


- (NSString*) convertToURLCompatible: (NSString*)stringy
{
    NSString * escapedUrlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)stringy,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]ÁáÉéÍíÓóÚúñ. %",kCFStringEncodingUTF8));
    
    return escapedUrlString;
}
@end
