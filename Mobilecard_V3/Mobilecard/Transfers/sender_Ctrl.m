//
//  sender_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 4/27/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "sender_Ctrl.h"

@interface sender_Ctrl ()
{
    NSMutableArray *countryArray;
    NSMutableArray *stateArray;
    NSMutableArray *cityArray;
    
    pootEngine *stateManager;
    pootEngine *cityManager;
    pootEngine *zipManager;
    
    pootEngine *createManager;
}
@end

@implementation sender_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
 //   [self addShadowToView:self.navigationController.navigationBar];
 //   [self addShadowToView:_continue_Button];
    
    [self initializePickersForView:self.navigationController.view];
    
    [_terms_button.titleLabel setNumberOfLines:2];
    [_privacy_Button.titleLabel setNumberOfLines:2];
    
    [_emailtext setUpTextFieldAs:textFieldTypeEmail];
    [_nameText  setUpTextFieldAs:textFieldTypeName];
    [_middlenameText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
    [_lastnameText setUpTextFieldAs:textFieldTypeLastName];
    [_motherlastText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
    [_mobileText setUpTextFieldAs:textFieldTypeCellphone];
    [_birthdateText setUpTextFieldAs:textFieldTypeBirthdate];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_addressAdditionalText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_cityText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_zipText setUpTextFieldAs:textFieldTypeRequiredCombo];
    
    [_whereToText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
    
    [self addValidationTextField:_emailtext];
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_lastnameText];
    [self addValidationTextField:_mobileText];
    [self addValidationTextField:_birthdateText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_zipText];
    
    [self addValidationTextFieldsToDelegate];
    
    [_emailtext setText:[[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserEmail] uppercaseString]];
    [_nameText setText:[[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName] uppercaseString]];
    [_lastnameText setText:[[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLastName] uppercaseString]];
    [_mobileText setText:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserPhone]];
    
    stateManager = [[pootEngine alloc] init];
    [stateManager setDelegate:self];
    [stateManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@"USA" forKey:@"idCountry"];
    [params setObject:@"RS" forKey:@"operation"];
    
    NSString *post = nil;
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    post = [NSString stringWithFormat:@"json=%@", JSONString];
    
    [stateManager startRequestWithURL:ViamericasGetStates withPost:post];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continue_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        if (!_terms_switch.on || !_privacy_switch.on) {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Error políticas", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
            return;
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *birthdate = [dateFormatter dateFromString:[_birthdateText text]];
        [dateFormatter setDateFormat:@"yyyy/MM/dd"];
        
        
        createManager = [[pootEngine alloc] init];
        [createManager setDelegate:self];
        [createManager setShowComments:developing];
    
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey] forKey:@"idUsuario"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLogin] forKey:@"nickName"];
        [params setObject:[[_lastnameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                    locale:[NSLocale systemLocale]] forKey:@"LName"];
        [params setObject:[[_middlenameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                      locale:[NSLocale systemLocale]] forKey:@"MName"];
        [params setObject:[[_motherlastText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                      locale:[NSLocale systemLocale]] forKey:@"SLName"];
        [params setObject:[_zipText infoArray][[_zipText selectedID]][[_zipText idLabel]] forKey:@"zipCode"];
        [params setObject:[[_addressText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                   locale:[NSLocale systemLocale]] forKey:@"address"];
        [params setObject:[[_addressAdditionalText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                             locale:[NSLocale systemLocale]] forKey:@"address2"];
        [params setObject:[dateFormatter stringFromDate:birthdate] forKey:@"birthDate"];
        [params setObject:[_emailtext text] forKey:@"email"];
        [params setObject:[[_nameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                locale:[NSLocale systemLocale]] forKey:@"firstName"];
        [params setObject:[_cityText infoArray][[_cityText selectedID]][[_cityText idLabel]] forKey:[_cityText idLabel]];
        [params setObject:@"USA" forKey:@"idCountry"];
        [params setObject:[_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]] forKey:[_stateText idLabel]];
        [params setObject:[[_lastnameText text] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch
                                                                    locale:[NSLocale systemLocale]] forKey:@"lastName"];
        [params setObject:[_mobileText text] forKey:@"phone1"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [createManager startRequestWithURL:ViamericasCreateSender withPost:post];
        
        [self lockViewWithMessage:nil];
    }
}

- (IBAction)showPrivacy_Action:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://www.mobilecard.mx/AddcelContent/files/docs/viamericas_privacy.pdf"];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)showTerms_Action:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://www.mobilecard.mx/AddcelContent/files/docs/viamericas_terms.pdf"];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)cancel_Action:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([_delegate conformsToProtocol:@protocol(senderDelegate)]&&[_delegate respondsToSelector:@selector(senderCreationResult:)]) {
            [_delegate senderCreationResult:nil];
        }
    }];
}

#pragma Mark PickerView

- (void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self subPickerView:pickerView didSelectRow:row inComponent:component];
    
    if (pickerView.textField == _stateText) {
        cityManager = [[pootEngine alloc] init];
        [cityManager setDelegate:self];
        [cityManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:@"USA" forKey:@"idCountry"];
        [params setObject:[_stateText infoArray][row][[_stateText idLabel]] forKey:[_stateText idLabel]];
        [params setObject:@"RS" forKey:@"operation"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [cityManager startRequestWithURL:ViamericasGetCities withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }
    
    if (pickerView.textField == _cityText) {
        zipManager = [[pootEngine alloc] init];
        [zipManager setDelegate:self];
        [zipManager setShowComments:developing];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[_cityText infoArray][[_cityText selectedID]][[_cityText idLabel]] forKey:@"idCity"];
        //[params setObject:@"RS" forKey:@"operation"];
        
        NSString *post = nil;
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        post = [NSString stringWithFormat:@"json=%@", JSONString];
        
        [zipManager startRequestWithURL:ViamericasGetZips withPost:post];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    }
}


#pragma Mark pootEngine Manager

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (json == nil) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:@"Server returns nil" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (manager == stateManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue] == 0) {
            [_stateText setInfoArray:response[@"countryStates"]];
            [_stateText setIdLabel:@"idState"];
            [_stateText setDescriptionLabel:@"nameState"];
            
            UIPickerView_Automated *pick = [[UIPickerView_Automated alloc] init];
            [pick setTextField:_stateText];
            [self pickerView:pick didSelectRow:0 inComponent:0];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == cityManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue]==0) {
            [_cityText setInfoArray:response[@"stateCities"]];
            [_cityText setIdLabel:@"idCity"];
            [_cityText setDescriptionLabel:@"nameCity"];
            
            UIPickerView_Automated *pick = [[UIPickerView_Automated alloc] init];
            [pick setTextField:_cityText];
            [self pickerView:pick didSelectRow:0 inComponent:0];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == zipManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue]==0) {
            [_zipText setInfoArray:response[@"zipCodeList"]];
            [_zipText setIdLabel:@"zipCode"];
            [_zipText setDescriptionLabel:@"zipCode"];
            
            UIPickerView_Automated *pick = [[UIPickerView_Automated alloc] init];
            [pick setTextField:_zipText];
            [self pickerView:pick didSelectRow:0 inComponent:0];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == createManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        if ([[response objectForKey:kIDError] intValue]==0) {
            if ([_delegate conformsToProtocol:@protocol(senderDelegate)]&&[_delegate respondsToSelector:@selector(senderCreationResult:)]) {
                [_delegate senderCreationResult:response[kUserSenderId]];
                
                [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}

@end
