//
//  sender_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 4/27/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol senderDelegate <NSObject>

@required
- (void) senderCreationResult:(id)result;

@end


@interface sender_Ctrl : mT_commonTableViewController

@property (nonatomic, assign) id <senderDelegate, NSObject> delegate;

@property (strong, nonatomic) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet UIButton *terms_button;
@property (weak, nonatomic) IBOutlet UIButton *privacy_Button;
@property (weak, nonatomic) IBOutlet UISwitch *terms_switch;
@property (weak, nonatomic) IBOutlet UISwitch *privacy_switch;

@property (weak, nonatomic) IBOutlet UILabel *infoText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *emailtext;
@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *middlenameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastnameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *motherlastText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *mobileText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *birthdateText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressAdditionalText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *whereToText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end
