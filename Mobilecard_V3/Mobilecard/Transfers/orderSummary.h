//
//  orderSummary.h
//  MobileCard_X
//
//  Created by David Poot on 5/25/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol orderSummaryDelegate <NSObject>

@required

- (void)orderSummaryResult;

@end

@interface orderSummary : mT_commonTableViewController

@property (nonatomic, assign) id <orderSummaryDelegate, NSObject> delegate;

@property (strong, nonatomic) NSDictionary *userData;
@property (strong, nonatomic) NSDictionary *recipientInfo;
@property (strong, nonatomic) NSDictionary *feeInfo;
@property (strong, nonatomic) NSDictionary *cardInfo;

@property (weak, nonatomic) IBOutlet UITextField_Validations *recipientLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *amountSendLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *amountReceivedLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *receiveNetworkLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *paymentLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *rateLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *feeLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *otherFeeLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cardLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressCardLabel;

@property (weak, nonatomic) IBOutlet UITextField_Validations *totalLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancel_Button;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end
