//
//  order_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 4/28/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "generalCollectionCell.h"
#import "Receiver_Selection_Ctrl.h"


typedef enum {
    orderStageNew                  = 0x00000,
    orderStagePayMethod            = 0x00001,
    orderStageDestination          = 0x00002,
    orderStageCurrency             = 0x00004,
} orderStage;

@interface order_Ctrl : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *recipientInfo;

@property (weak, nonatomic) IBOutlet UICollectionView *typeCollection;

@property (weak, nonatomic) IBOutlet UITextField_Validations *countryText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *branchText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *amountText;
//@property (weak, nonatomic) IBOutlet UITextField_Validations *receiveLabel;
@property (weak, nonatomic) IBOutlet UITextField_Validations *receiveText;
@property (weak, nonatomic) IBOutlet UILabel *exchangeRateLabel;


@property (weak, nonatomic) IBOutlet UIView *destinationView;
@property (weak, nonatomic) IBOutlet UIView *currencyView;
@property (strong, nonatomic) IBOutlet UITableView *orderTable;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
