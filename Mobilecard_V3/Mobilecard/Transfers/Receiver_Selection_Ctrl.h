//
//  Receiver_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 4/24/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "orderFees_Ctrl.h"
#import "Receiver_Ctrl.h"

@interface Receiver_Selection_Ctrl : mT_commonTableViewController <receiverDelegate>

@property (strong, nonatomic) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet UITextField_Validations *regReceiversText;

@property (weak, nonatomic) IBOutlet UIButton *add_Button;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
