//
//  orderFees_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 5/20/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "Wallet_Ctrl.h"
#import "orderResult_Ctrl.h"
#import "orderSummary.h"

@interface orderFees_Ctrl : mT_commonTableViewController <cardSelectionDelegate, updateCardDelegate, orderSummaryDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) NSDictionary *userData;
@property (strong, nonatomic) NSDictionary *recipientInfo;

@property (weak, nonatomic) IBOutlet UITextField_Validations *amount;
@property (weak, nonatomic) IBOutlet UITextField_Validations *accountName;
@property (weak, nonatomic) IBOutlet UITextField_Validations *accountNumber;
@property (strong, nonatomic) IBOutlet UITableView *feeTable;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
