//
//  Receiver_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 4/24/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
//#import "orderFees_Ctrl.h"

@protocol receiverDelegate <NSObject>

@required
- (void) receiverResponse:(NSDictionary*)receiverData;

@end

@interface Receiver_Ctrl : mT_commonTableViewController

@property (nonatomic, assign) id <receiverDelegate, NSObject> delegate;

@property (strong, nonatomic) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *middlenameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastNameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *motherlastText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
//@property (weak, nonatomic) IBOutlet UITextField_Validations *addressAdditionalText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *countryText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *birthdateText;

@property (weak, nonatomic) IBOutlet UISwitch *createRecipientSwitch;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (strong, nonatomic) IBOutlet UITableView *receiverTable;
@end
