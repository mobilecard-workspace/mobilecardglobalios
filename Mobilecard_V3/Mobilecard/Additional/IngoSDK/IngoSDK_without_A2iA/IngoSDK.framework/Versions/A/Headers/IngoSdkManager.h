//
//  IngoSdkManager.h
//  IngoSDK
//
//  Created by Stephen Gowen on 4/28/14.
//  Copyright (c) 2014 Ingo Money, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IngoLocationManager.h"
#import "CNLoadCheckTransactionInfo.h"



#pragma mark <Public API TypeDefs>

/**
 * Callback that is fired once the Ingo system
 * status has been determined.
 *
 * @param isAvailable - if false, the Ingo system is currently unavailable, meaning you should not launch the SDK
 */
typedef void(^IsSystemAvailableCallback)(bool isAvailable);

/**
 * Callback that is fired once the terms and conditions have either been accepted or declined
 *
 * @param accepted
 */
typedef void(^OnTransactionApprovedHandler)(CNLoadCheckTransactionInfo *transactionInfo);

#pragma mark <Private API TypeDefs>

/**
 * Callback that is fired once a request has returned
 *
 * @param jsonResponse
 */
typedef void (^OnComplete)(NSDictionary *jsonResponse);

/**
 * Callback that is fired once the terms and conditions have either been accepted or declined
 *
 * @param accepted
 */
typedef void(^CNTermsAndConditionsCompletionHandler)(BOOL accepted);

/**
 * Callback that is fired once the terms and conditions have either been accepted or declined
 *
 * @param accepted
 */
typedef void(^SessionInvalidHandler)();

@class CNBaseRequest;

@interface IngoSdkManager : NSObject

#pragma mark <Public API>

+ (bool)initIngoSdk:(NSString *)iovationBlackBox;

+ (bool)initIngoSdk:(NSString *)iovationBlackBox partnerId:(NSString *)pId;

+ (bool)initIngoSdk:(NSString *)iovationBlackBox partnerId:(NSString *)pId configurationKeys:(NSDictionary *)configurationKeys;

+ (void)checkIngoSystemStatus:(IsSystemAvailableCallback)isSystemAvailableCallback;

- (void)beginWithHostViewController:(UIViewController *)hostVc customerId:(NSString *)cId ssoToken:(NSString *)ssoT andOnTransactionApprovedHandler:(OnTransactionApprovedHandler)onTransactionApprovedHandler;

+ (id)getInstance;

- (void)showHistoryWithHostViewController:(UIViewController *)hostVc customerId:(NSString *)cId ssoToken:(NSString *)ssoT andOptionalTransactionId:(NSString *)transId;

- (void)showRedeemCodeWithHostViewController:(UIViewController *)hostVc customerId:(NSString *)cId ssoToken:(NSString *)ssoT;

#pragma mark <Private API>

- (void)sendJsonRequest:(CNBaseRequest *)request withOnSuccess:(OnComplete)os andOnFailure:(OnComplete)of;

- (void)cancelCurrentRequest;

- (void)getLocationFixWithOnEndedHandler:(OnLocationFixEnded)onEnded withOnRetrySelector:(SEL)onRetrySelector onTarget:(id)t withProgressMessage:(NSString *)progressMessage;

- (void)showProgressDialogWithMessage:(NSString *)message;

- (void)hideProgressDialog;

- (void)showAttentionDialogWithMessage:(NSString *)m dismissTarget:(id)dt dismissButtonText:(NSString *)dbt dismissActionSelector:(SEL)das positiveTarget:(id)pt positiveButtonText:(NSString *)pbt andPositiveActionSelector:(SEL)pas;

- (void)dismissAttentionDialog;

- (bool)isSessionValid;

- (void)onSessionInvalid;

- (void)exitSdk;

- (void)exitSdkAndDisplayTransactionApproved;

- (void)exitSdk:(void (^)(void))completion;

@property (strong, nonatomic) SessionInvalidHandler sessionInvalidHandler;
@property (assign, nonatomic) bool isLandscape;

@end
