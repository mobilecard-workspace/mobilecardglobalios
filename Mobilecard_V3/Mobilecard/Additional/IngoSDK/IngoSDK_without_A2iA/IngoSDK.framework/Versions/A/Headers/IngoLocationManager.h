//
//  IngoLocationManager.h
//  IngoSDK
//
//  Created by Stephen Gowen on 4/30/14.
//  Copyright (c) 2014 Ingo Money, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^OnLocationFixEnded)();
typedef void (^OnLocationAccessDenied)();

@class CNLocationData;

@interface IngoLocationManager : NSObject <CLLocationManagerDelegate>

+ (bool)isLocationDataValid;

+ (CNLocationData *)getLocationData;

- (void)getLocationFixWithOnEndedHandler:(OnLocationFixEnded)onEnded andOnDeniedHandler:(OnLocationAccessDenied)onDenied;

@end
