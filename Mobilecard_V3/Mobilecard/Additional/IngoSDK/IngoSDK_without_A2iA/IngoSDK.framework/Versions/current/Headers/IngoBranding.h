//
//  IngoBranding.h
//  IngoSDK
//
//  Created by Chris Varble on 7/15/14.
//  Copyright (c) 2014 Ingo Money, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IngoBranding : NSObject

@property (strong, nonatomic) UIImage *aboutImage;
@property (strong, nonatomic) UIColor *alertButton;
@property (strong, nonatomic) UIColor *alertHeader;
@property (strong, nonatomic) UIColor *alertText;
@property (strong, nonatomic) UIColor *buttonTint;
@property (strong, nonatomic) UIColor *contentBackgroundColor;
@property (strong, nonatomic) UIImage *contentBackgroundImage;
@property (strong, nonatomic) UIColor *contentTextColor;
@property (strong, nonatomic) UIColor *footerBackgroundColor;
@property (strong, nonatomic) UIColor *footerText;
@property (strong, nonatomic) UIColor *header;
@property (strong, nonatomic) UIImage *landingImage;
@property (strong, nonatomic) UIColor *listItemHeader;
@property (strong, nonatomic) UIColor *listSectionText;
@property (strong, nonatomic) UIColor *navigationBackgroundColor;
@property (strong, nonatomic) UIImage *navigationBackgroundImage;
@property (strong, nonatomic) UIColor *navigationButtonTint;
@property (strong, nonatomic) UIColor *navigationTitleColor;
@property (strong, nonatomic) UIImage *partnerLogo;
@property (nonatomic) double primaryLandingButtonAlpha;
@property (strong, nonatomic) UIColor *primaryLandingButtonColor;
@property (strong, nonatomic) UIColor *primaryLandingButtonDivider;
@property (strong, nonatomic) NSString *statusBarStyle; //should be either set to "default" or "light"
@property (strong, nonatomic) UIColor *subHeader;
@property (nonatomic) double subLandingButtonAlpha;
@property (strong, nonatomic) UIColor *subLandingButtonColor;
@property (strong, nonatomic) UIColor *subLandingButtonDivider;
@property (strong, nonatomic) UIColor *webViewBackgroundColor;

+ (IngoBranding*)getInstance;

#pragma mark <Private API>
- (id)initWithDictionary:(NSDictionary *)dict;

@end
