//
//  mT_appLinks.h
//  mT
//
//  Created by David Poot on 12/1/13.
//  Copyright (c) 2013 addcel. All rights reserved.
//


// YES -> Comments ON
#define developing YES
#define debugging NO

#define walletEnabled YES

//COMMENTED -> QA
#define releaseServer
//#ifdef releaseServer

#define isBussiness [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]


////PRODUCCION
//#define serverURL @"https://mobilecard.mx/AddCelBridge/Servicios" //Producción
//#define simpleServerURL @"https://mobilecard.mx" //Producción
//#define serverURLlaCuenta @"https://mobilecard.mx"
//#define ANTADServerURL @"https://mobilecard.mx/MCAntadServicios"
//
//#define diestelServerURL  @"https://mobilecard.mx/DiestelServices"
//
//#define userManagementURL [NSString stringWithFormat:@"https://www.mobilecard.mx/Usuarios/%d/", idApp]
//#define walletManagementURL [NSString stringWithFormat:@"https://www.mobilecard.mx/Wallet/%d", idApp]
//#define ingoManagementURL @"https://www.mobilecard.mx/MCIngo"
//#define CatalogServiceURLSimple [NSString stringWithFormat:@"https://www.mobilecard.mx/Catalogos/%d/%@", idApp, NSLocalizedString(@"lang", nil)]
//#define CatalogServiceURL [NSString stringWithFormat:@"https://www.mobilecard.mx/Catalogos/%d/%@/%@",idApp, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIdCountry], NSLocalizedString(@"lang", nil)]
//#define TransactoServiceURL [NSString stringWithFormat:@"https://www.mobilecard.mx/Transacto/%d/%@",idApp, NSLocalizedString(@"lang", nil)]
//#define TokenServiceURL [NSString stringWithFormat:@"https://www.mobilecard.mx/Tokenizer/%d/%@/%@",idApp, isBussiness?[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"]:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIdCountry], NSLocalizedString(@"lang", nil)]
//#define H2HServiceURL [NSString stringWithFormat:@"https://www.mobilecard.mx/H2HPayment/%d/",idApp]
//#define H2HServiceURLPayment [NSString stringWithFormat:@"https://www.mobilecard.mx/H2HPayment/%d/%@/%@",idApp, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIdCountry], NSLocalizedString(@"lang", nil)]
//
//#define ACIServerURL @"https://www.mobilecard.mx"
//#define viamericasURL [NSString stringWithFormat:@"https://www.mobilecard.mx/Transfers/%d/%@", idApp, NSLocalizedString(@"lang", nil)]
//


//#else
//QA

#define serverURL @"http://199.231.160.203/AddCelBridge/Servicios" //Desarrollo
#define simpleServerURL @"http://199.231.160.203" //Desarrollo
#define serverURLlaCuenta @"http://199.231.160.203"
#define ANTADServerURL @"http://199.231.160.203/MCAntadServicios"

#define diestelServerURL  @"http://199.231.160.203/DiestelServices"

#define userManagementURL [NSString stringWithFormat:@"http://199.231.160.203/Usuarios/%d/", idApp]
#define walletManagementURL [NSString stringWithFormat:@"http://199.231.160.203/Wallet/%d", idApp]
#define ingoManagementURL @"http://199.231.160.203/MCIngo"
#define CatalogServiceURLSimple [NSString stringWithFormat:@"http://199.231.160.203/Catalogos/%d/%@", idApp, NSLocalizedString(@"lang", nil)]
#define CatalogServiceURL [NSString stringWithFormat:@"http://199.231.160.203/Catalogos/%d/%@/%@",idApp, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIdCountry], NSLocalizedString(@"lang", nil)]
#define TransactoServiceURL [NSString stringWithFormat:@"http://199.231.160.203/Transacto/%d/%@",idApp, NSLocalizedString(@"lang", nil)]
#define TokenServiceURL [NSString stringWithFormat:@"http://199.231.160.203/Tokenizer/%d/%@/%@",idApp, isBussiness?[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"]:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIdCountry], NSLocalizedString(@"lang", nil)]
#define H2HServiceURL [NSString stringWithFormat:@"http://199.231.160.203/H2HPayment/%d/",idApp]
#define H2HServiceURLPayment [NSString stringWithFormat:@"http://199.231.160.203/H2HPayment/%d/%@/%@",idApp, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIdCountry], NSLocalizedString(@"lang", nil)]


#define ACIServerURL @"http://199.231.160.203"
#define viamericasURL [NSString stringWithFormat:@"http://199.231.160.203/Transfers/%d/%@", idApp, NSLocalizedString(@"lang", nil)]

//#endif


//La cuenta
#define LaCuenta_getPlaces [NSString stringWithFormat:@"%@/LCPFServices/%@/getEstablecimientos", serverURLlaCuenta, NSLocalizedString(@"lang", nil)]
#define LaCuenta_3DSecure [serverURLlaCuenta stringByAppendingString:@"/LCPFServices/payworks/enqueuePayment"]

//Toll balance
#define WSGetPassBalance [NSString stringWithFormat:@"%@/%@", userManagementURL, NSLocalizedString(@"lang", nil)]

//H2H Banorte
///MCH2HPayment
#define H2HGetAccounts [H2HServiceURL stringByAppendingString:@"/toAccount"]
#define H2HAddAccount [H2HServiceURL stringByAppendingString:@"/enqueueAccountSignUp"]
#define H2HGetBanks [H2HServiceURL stringByAppendingString:@"/bankCodes"]
#define H2HEnqueuePayment [H2HServiceURLPayment stringByAppendingString:@"/pagoBP"]
#define H2HUpdateAccount [H2HServiceURL stringByAppendingString:@"/updateAccount"]
#define H2HDeleteAccount [H2HServiceURL stringByAppendingString:@"/deleteAccount"]

//ACI
#define ACIServicesURL [ACIServerURL stringByAppendingString:@"/AciBridgeWS/getServices"]
#define ACIAddressURL [ACIServerURL stringByAppendingString:@"/AciBridgeWS/"]
#define ACICheckUserData [ACIServerURL stringByAppendingString:@"/AciBridgeWS/CheckUserData"]
#define ACIPayment [ACIServerURL stringByAppendingString:@"/AciBridgeWS/payment"]
#define ACIPaymentNoData [ACIServerURL stringByAppendingString:@"/AciBridgeWS/paymentNotData"]


//VIAMERICAS
#define ViamericasGetCountries [viamericasURL stringByAppendingString:@"/get/countrys"]
#define ViamericasGetStates [viamericasURL stringByAppendingString:@"/get/countrysStates"]
#define ViamericasGetCities [viamericasURL stringByAppendingString:@"/get/statesCitys"]
#define ViamericasGetZips [viamericasURL stringByAppendingString:@"/get/Cities"]
#define ViamericasCreateSender [viamericasURL stringByAppendingString:@"/transfers/createSender"]
#define ViamericasGetBeneficiaries [viamericasURL stringByAppendingString:@"/get/BeneficiariesBySender"]
#define ViamericasSenderProfile [viamericasURL stringByAppendingString:@"/validate/senderProfile"]
#define ViamericasCreateRecipient [viamericasURL stringByAppendingString:@"/transfers/createRecipient"]
#define ViamericasGetRecipients [viamericasURL stringByAppendingString:@"/get/BeneficiariesBySender"]
#define ViamericasGetCurrency [viamericasURL stringByAppendingString:@"/get/currency/country"]
#define ViamericasGetPaymentMode [viamericasURL stringByAppendingString:@"/get/payment/modes"]
#define ViamericasGetLocations [viamericasURL stringByAppendingString:@"/get/payment/locations"]
#define ViamericasCreateNewOrder [viamericasURL stringByAppendingString:@"/transfers/createNewOrder"]
#define ViamericasSenderProfileCard [viamericasURL stringByAppendingString:@"/transfers/createSenderProfileCard"]
#define ViamericasConfirmTransactionCard [viamericasURL stringByAppendingString:@"/transfers/confirmTransactionCredit"]
#define ViamericasConfirmTransactionCash [viamericasURL stringByAppendingString:@"/transfers/confirmTransactionCash"]

#define ViamericasGetPaymentLocation [viamericasURL stringByAppendingString:@"/get/costPaymentLocationNetwork"]
#define ViamericasGetExchangeRate [viamericasURL stringByAppendingString:@"/get/ExchangeRate"]
#define ViamericasGetOrderFees [viamericasURL stringByAppendingString:@"/get/orderFees"]

//INGO SERVICES
#define WSGetSessionData [NSString stringWithFormat:@"%@/%d/%@/getSessionData", ingoManagementURL, idApp, NSLocalizedString(@"lang", nil)]
#define WSGetINGOStateCatalog [NSString stringWithFormat:@"%@/%d/%@/3/estados", ingoManagementURL, idApp, NSLocalizedString(@"lang", nil)]
#define WSSetEnrollCustomer [NSString stringWithFormat:@"%@/%d/%@/EnrollCustomer", ingoManagementURL, idApp, NSLocalizedString(@"lang", nil)]
#define WSSetEnrollCardToCustomer [NSString stringWithFormat:@"%@/%d/%@/AddOrUpdateCard", ingoManagementURL, idApp, NSLocalizedString(@"lang", nil)]

// USER SERVICES

#define WSLogin [NSString stringWithFormat:@"%@/%d/%@/user/login", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], NSLocalizedString(@"lang", nil)]
#define WSRegister [userManagementURL stringByAppendingString:@"/registro"]

#define WSUpdateLogin [userManagementURL stringByAppendingString:@"/actualizausuario"]
#define WSUpdateName [userManagementURL stringByAppendingString:@"/actualizanombreusuario"]
#define WSUpdateEmail [userManagementURL stringByAppendingString:@"/actualizacorreo"]
#define WSUpdatePassword [userManagementURL stringByAppendingString:@"/changepassusu"]
#define WSResetPassword [userManagementURL stringByAppendingString:@"/resetpassusu"]
#define WSUpdateCountry [userManagementURL stringByAppendingString:@"/actualizapaisusuario"]
#define WSUpdatePhone [userManagementURL stringByAppendingString:@"/actualizatelefono"]
#define WSUpdateCard [userManagementURL stringByAppendingString:@"/actualiza"]
#define WSReferenceJumio [userManagementURL stringByAppendingString:@"/actualiza"]


// WALLET SERVICES

#define WSWalletSetFavoritesCard [walletManagementURL stringByAppendingString:@"/1/es/defaultcard"]
#define WSWalletGetDebitCards [walletManagementURL stringByAppendingString:@"/getCards/2"]
#define WSWalletGetCards [walletManagementURL stringByAppendingString:@"/getCards/with_movements"]
#define WSWalletAddCard [walletManagementURL stringByAppendingString:@"/add"]
#define WSWalletUpdateCard [walletManagementURL stringByAppendingString:@"/update"]
#define WSWalletRemoveCard [walletManagementURL stringByAppendingString:@"/delete"]
#define WSWalletAddMCCard [walletManagementURL stringByAppendingString:@"/addmobilecard"]
#define WSWalletGetCustomCard [walletManagementURL stringByAppendingString:@"/getCustomCard"]

//ANTAD SERVICES

#define WSGetCatalogRecargas [CatalogServiceURL stringByAppendingString:@"/getRecargas"]
#define WSGetCatalogRecargaServicios [CatalogServiceURL stringByAppendingString:@"/getRecargaServicios"]
#define WSGetCatalogServicios [CatalogServiceURL stringByAppendingString:@"/getServicios"]
#define WSGetCatalogRecargaMontos [CatalogServiceURL stringByAppendingString:@"/getRecargaMontos"]
#define WSGetCatalogServicioCategorias [CatalogServiceURL stringByAppendingString:@"/getServicioCategorias"]
#define WSGetCatalogCountries [CatalogServiceURL stringByAppendingString:@"/TelefonicaPaises"]


#define WSGetTransactoToken [TransactoServiceURL stringByAppendingString:@"/getToken"]
#define WSGetTransactoPagoProsa3DS [TransactoServiceURL stringByAppendingString:@"/pagoProsa3DS"]
#define WSGetTransactoPagoProsa3DSSecured [NSString stringWithFormat:@"%@/%@/pagoProsa3DS", TransactoServiceURL, @"HERE"]

#define WSGetTransactoConsultaSaldos [TransactoServiceURL stringByAppendingString:@"/procesaConsultaSaldos"]

#define WSGetANTADIAVEPayment [NSString stringWithFormat:@"%@/Peaje/%d/%@/iave/3dsecure/procesaPago", simpleServerURL, idApp, NSLocalizedString(@"lang", nil)]
#define WSGetTransactoRate [ANTADServerURL stringByAppendingString:@"/getDivisa"]

//TOKENIZER
#define WSGetToken [TokenServiceURL stringByAppendingString:@"/getToken"]

//DIESTEL SERVICES
#define WSGetDiestelToken [diestelServerURL stringByAppendingString:@"/getToken"]

//BLACKSTONE SERVICES
#define WSGetBlackstoneCarriers [NSString stringWithFormat:@"%@/Blackstone/%d/%@/getCarriers?countryCode=USA", simpleServerURL, idApp, NSLocalizedString(@"lang", nil)]
#define WSGetBlackstoneProducts [NSString stringWithFormat:@"%@/Blackstone/%d/%@/getProductsByCarrier", simpleServerURL, idApp, NSLocalizedString(@"lang", nil)]
#define WSDoPayment [NSString stringWithFormat:@"%@/Blackstone/%d/%@/doTopUp", simpleServerURL, idApp, NSLocalizedString(@"lang", nil)]

//CATÁLOGOS

#define WSCatalogGetCountries [NSString stringWithFormat:@"%@/getPaises", CatalogServiceURLSimple]
#define WSCatalogGetBanks [NSString stringWithFormat:@"%@/%@/bankCodes", userManagementURL, NSLocalizedString(@"lang", nil)]


//COLOMBIA EXCLUSIVE SERVICES
#define WSColombiaGetData [NSString stringWithFormat:@"%@/Colombia/%d/%@/%@/getDatosServicios", simpleServerURL, idApp, @"2", NSLocalizedString(@"lang", nil)]
#define WSColombiaToken [NSString stringWithFormat:@"%@/Colombia/%d/2/%@/getToken", simpleServerURL, idApp, NSLocalizedString(@"lang", nil)]
#define WSColombiaPurchase [NSString stringWithFormat:@"%@/Colombia/%d/2/%@/enviarPago", simpleServerURL, idApp, NSLocalizedString(@"lang", nil)]
#define WSColombiaQuery [NSString stringWithFormat:@"%@/Colombia/%d/2/%@/consultaReferencia", simpleServerURL, idApp, NSLocalizedString(@"lang", nil)]

//SHIFT HEADERS
#define ShiftHeaderDeveloper @"CH0MIpa07QhgafzUfxflWInrzeuU2ttQjo5BOZ8CJSdI/wbQ3iV3MvdpJfJOIlf/"
#define ShiftHeaderProject @"vSGISmhIDNBJR6UMsgy7v9NjeAJUo/88G82i++lRPXckgAt+BgeWgcc6+B5lByYC"

//Previvale
#define WSPrevivaleVerify [NSString stringWithFormat:@"%@%@/commerce/verify", userManagementURL, NSLocalizedString(@"lang", nil)]

//ThreatMetrix
#define WSSetPaymentThreatMetrix [NSString stringWithFormat:@"%@/Transacto/%d/%@/%@", simpleServerURL,idApp, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIdCountry], NSLocalizedString(@"lang", nil)]


#define WSSandPPayment [NSString stringWithFormat:@"%@/CuantoTeDebo/%d/%@/%@", simpleServerURL, idApp, isBussiness?[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"]:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIdCountry], NSLocalizedString(@"lang", nil)]
