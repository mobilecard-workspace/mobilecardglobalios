//
//  pootEngine.h
//  pootEngine
//
//  Created by David Poot on 7/2/12.
//  Copyright (c) 2012 Addcel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class pootEngine;

typedef enum {
    pootEngineErrorCodeBadJSON,
	pootEngineErrorCodeTimeout,
	pootEngineErrorCodeNoConnection,
    pootEngineErrorCodeImageFailed
} pootEngineErrorCode;

@protocol pootEngineDelegate

@optional
- (void) managerSuccess:(pootEngine*)manager jsonParsed:(id)json;
- (void) managerSuccess:(pootEngine*)manager imageDownloaded:(UIImage*)imageData;
- (void) managerFailed:(pootEngine*)manager withKey:(NSString*)key errorCode:(pootEngineErrorCode)error;
@end

@interface pootEngine : NSObject

@property (strong, nonatomic) NSDictionary *additionalInfo;

//MCColombia helpers

- (NSString*)MCEncryptJSONWithParams:(id)params withPassword:(NSString*)password;
- (id) MCDecryptToJSONWithString:(NSString*)stringToDecrypt withSensitive:(BOOL)sensitive;
- (NSString*)MCEncryptString:(NSString*)stringData withPassword:(NSString*)password;
- (NSString*)MCDecryptString:(NSString*)stringToDecrypt withSensitive:(BOOL)sensitive;


//MC WS



//USER AVAILABLE
//Customizable
- (void)startWithoutPostRequestWithURL:(NSString *)URL;
- (void)startRequestWithURL:(NSString *)URL;
- (void)startRequestWithURL:(NSString *)URL withPost:(NSString*)posty;
- (void) startProtectedRequestWithValues:(id)params forWS:(NSString*)WSString withPass:(NSString*)Password Automated:(BOOL) automated;
- (void) startProtectedRequestWithValues:(id)params forWS:(NSString*)WSString withPass:(NSString*)Password UncryptWithPass:(BOOL)uncryptWithPass UncryptAutomated:(BOOL)automated;

- (void) startProtectedRequestWithValues:(id)params forWS:(NSString*)WSString withPass:(NSString*)Password Automated:(BOOL)automated UncryptWithPass:(BOOL)uncryptWithPass;

- (void) startRequestWithValues:(id)params forWS:(NSString*)WSString withUncryption:(BOOL) Uncrypt Automated:(BOOL)automated;
- (void) startRequestWithValues:(id)params forWS:(NSString*)WSString withUncryption:(BOOL) Uncrypt;
- (void) startRequestWithOnlyOneValue:(NSString*)value forWS:(NSString*)WSString withUncryption:(BOOL) Uncrypt;

- (void)start_JSONRequestWithURL:(NSString *)URL withPost:(NSString*)posty;
- (void)startJSONRequestWithURL:(NSString *)URL withPost:(NSString*)posty;
- (void)startJSONRequestWithURL:(NSString *)URL withPost:(NSString*)posty andHeader:(NSDictionary*)header;
- (void)startJSONRequestWithURL:(NSString *)URL withPost:(NSString*)posty andHeader:(NSDictionary*)header andContentType:(NSString *)contentTypeString;
- (void) startBasicAuthWithUsername:(NSString*)username password:(NSString*)password andParameters:(NSString*)params forWS:(NSString*)WSString;

- (NSString *)encryptJSONString:(NSString*)serializedJSON withPassword:(NSString*) password;

- (NSString *)encryptJSONWithParams:(id)params withPassword:(NSString*) password;

- (id) decryptString:(NSString*)stringToDecrypt withSensitive:(BOOL)sensitive;
- (id) decryptedStringOfString:(NSString*)stringToDecrypt withSensitive:(BOOL)sensitive;

- (void)startImageRequestWithURL:(NSString *)URL andImageView:(UIImageView *)imageView;

- (NSString *)convertToSecureLevel2:(NSString*)string;

//Customized
- (void) mcStartLoginWithPW:(NSString*)password login:(NSString*)login type:(NSString*)type software:(NSString*)software Model:(NSString*)Model key:(NSString*)key imei:(NSString*)imei;

- (void) mcGetVersion;
- (void) mcGetStates;
- (void) mcGetCardTypes;
- (void) mcGetCarriers;
- (void) mcGetTerms;
- (void) mcGetUserInfoWithLogin:(NSString*)login andPassword:(NSString*)password;
- (void) mcSetPasswordChangeWithLogin:(NSString*)login OldPass:(NSString*)oldPass andNwPass:(NSString*)nwPass;
- (void) mcSetMustChangePassWithLogin:(NSString*)login OldPass:(NSString*)oldPass andNwPass:(NSString*)nwPass;
- (void) mcSetRecoverPassWithLogin:(NSString*)login;
- (void) mcSetNewRegisterWithLogin:(NSString*)login password:(NSString*)password birthdate:(NSString*)birthdate cellphone:(NSString*)cellphone name:(NSString*)name lastname:(NSString*)lastname motherlastname:(NSString*)motherlastname sex:(NSString*)sex homePhone:(NSString*)homePhone officePhone:(NSString*)officePhone stateID:(NSString*)stateID city:(NSString*)city street:(NSString*)street extNum:(NSString*)extNum intNum:(NSString*)intNum town:(NSString*)town zip:(NSString*)zip AMEXAddress:(NSString*)AMEXAddress address:(NSString*)address cardNumber:(NSString*)cardNumber valid:(NSString*)valid cardTypeID:(NSString*)cardTypeID supplier:(NSString*)supplier email:(NSString*)email imei:(NSString*)imei tagTypeID:(NSString*)tagTypeID tagLabel:(NSString*)tagLabel tagNumber:(NSString*)tagNumber tagDV:(NSString*)tagDV deviceType:(NSString*)deviceType deviceSoftware:(NSString*)deviceSoftware deviceModel:(NSString*)deviceModel key:(NSString*)key;
- (void) mcSetUpdateRegisterWithLogin:(NSString*)login password:(NSString*)password birthdate:(NSString*)birthdate cellphone:(NSString*)cellphone name:(NSString*)name lastname:(NSString*)lastname motherlastname:(NSString*)motherlastname sex:(NSString*)sex homePhone:(NSString*)homePhone officePhone:(NSString*)officePhone stateID:(NSString*)stateID city:(NSString*)city street:(NSString*)street extNum:(NSString*)extNum intNum:(NSString*)intNum town:(NSString*)town zip:(NSString*)zip AMEXAddress:(NSString*)AMEXAddress address:(NSString*)address cardNumber:(NSString*)cardNumber valid:(NSString*)valid cardTypeID:(NSString*)cardTypeID supplier:(NSString*)supplier email:(NSString*)email imei:(NSString*)imei;
- (void) mcGetRelationshipTypes;
- (void) mcGetMaritalStatusTypes;




























@property BOOL automaticIndicator;
@property BOOL showComments;

@property NSInteger typeOfConnection;
@property NSInteger tag;
@property (nonatomic, assign) id<NSObject, pootEngineDelegate> delegate;

@end












//Sub

#import <Foundation/NSString.h>


@interface NSData (Encryption)

+ (NSData *) sha1:(NSData *)data;
+ (NSData *) base64:(NSData *)data;
+ (NSData *) ubase64:(NSData *)data;

+ (NSData *) getKey;

+ (NSData *) aesEncrypt:(NSData *)data key:(NSData *)key;
+ (NSData *) aesDecrypt:(NSData *)data key:(NSData *)key;


//
//  NSData+AESCrypt.h
//
//  AES Encrypt/Decrypt
//  Created by Jim Dovey and 'Jean'
//  See http://iphonedevelopment.blogspot.com/2009/02/strong-encryption-for-cocoa-cocoa-touch.html
//
//  BASE64 Encoding/Decoding
//  Copyright (c) 2001 Kyle Hammond. All rights reserved.
//  Original development by Dave Winer.
//
//  Put together by Michael Sedlaczek, Gone Coding on 2011-02-22
//

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

+ (NSData *)dataWithBase64EncodedString:(NSString *)string;
- (id)initWithBase64EncodedString:(NSString *)string;

- (NSString *)base64Encoding;
- (NSString *)base64EncodingWithLineLength:(NSUInteger)lineLength;

- (BOOL)hasPrefixBytes:(const void *)prefix length:(NSUInteger)length;
- (BOOL)hasSuffixBytes:(const void *)suffix length:(NSUInteger)length;

@end


@interface NSString (Encryption)

+ (NSString *) sha1:(NSString *)data;
+ (NSString *) base64:(NSString *)data;
//+ (NSString *) aes:(NSString *)data key:(NSString *)key;
+ (NSString *) hide:(NSString *)data withPass:(NSString*)Passw;
+ (NSString *) unHide:(NSString *)data;

+ (NSString *) removeData:(NSString*) data;

+(NSString *) reverseString:(NSString*)pass;

- (NSString *)AES256EncryptWithKey:(NSString *)key;
- (NSString *)AES256DecryptWithKey:(NSString *)key;

@end
