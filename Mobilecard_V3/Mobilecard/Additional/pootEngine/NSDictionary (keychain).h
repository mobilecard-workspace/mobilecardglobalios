//
//  NSDictionary (keychain).h
//  Mobilecard
//
//  Created by David Poot on 8/23/18.
//  Copyright © 2018 David Poot. All rights reserved.
//
#define kKeychainKey @"userDetails"
#define kKeychainResultKey @"result"
#define kKeychainGroup [NSString stringWithFormat:@"%@sharedElements", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppIdentifierPrefix"]]

#import <Foundation/Foundation.h>

@interface NSDictionary (Keychain)

-(void) storeToKeychainWithKey:(NSString *)aKey andGroup:(NSString*)groupName;
+(NSDictionary *) dictionaryFromKeychainWithKey:(NSString *)aKey andGroup:(NSString*)groupName;
-(void) deleteFromKeychainWithKey:(NSString *)aKey andGroup:(NSString*)groupName;

@end
