//
//  mT_commonTableViewController.m
//  Mobilecard
//
//  Created by David Poot on 10/25/17.
//  Copyright © 2017 David Poot. All rights reserved.
//
#include <sys/sysctl.h>
#include <sys/utsname.h>
#import "mT_commonTableViewController.h"

#define pickerBackgroundColor [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6]
#define mcOrangeColor [UIColor colorWithRed:255.0/255.0 green:128.0/255.0 blue:0.0 alpha:1.0]

#define ignoreConnection 987

@interface mT_commonTableViewController ()
{
    NSMutableArray *blockerArray;
    
    UIView *graySheet;
    UILabel *pickerTitleLabel;
    UIButton *pickerSelectionButton;
    
    NSMutableArray *usableTextFields;
}
@end

@implementation mT_commonTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    usableTextFields = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Mark UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField_Validations *)textField
{
    return [self subTextFieldShouldEndEditing:textField];
}

- (BOOL)subTextFieldShouldEndEditing:(UITextField_Validations *)textField
{
    [textField setActive:NO];
    [textField updateValidation];
    return YES;
}

- (BOOL)textField:(UITextField_Validations *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return [self subTextField:textField shouldChangeCharactersInRange:range replacementString:string];
}

- (BOOL)subTextField:(UITextField_Validations *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return [self checkLengthInTextField:textField withRange:range andReplacementString:string];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField_Validations *)textField
{
    return [self subTextFieldShouldBeginEditing:textField];
}

- (BOOL) subTextFieldShouldBeginEditing:(UITextField_Validations *)textField
{
    [self addTooly:textField];
    [textField setActive:YES];
    [textField updateValidation];
    
    if (!textField.enabled) {
        return NO;
    }
    
    switch (textField.type) {
        case textFieldTypeValidCard:
        {
            [self dismissAllTextFields];
            [self dismissPicker:datePicker];
            
            NSMutableArray *months = [[NSMutableArray alloc] init];
            
            for (int i = 1; i<13; i++) {
                [months addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:i],kIDKey,[NSString stringWithFormat:@"%02d", i], kDescriptionKey, nil]];
            }
            
            [textField setInfoArray:months];
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            NSDate *now = [NSDate date];
            
            [df setDateFormat:@"yy"];
            
            NSMutableArray *years = [[NSMutableArray alloc] init];
            
            for(NSInteger i = 0; i < 10; i++)
            {
                int year = [[df stringFromDate:now] intValue]+i;
                [years addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:year], kIDKey, [NSString stringWithFormat:@"%d", year], kDescriptionKey, nil]];
            }
            [textField setInfoArray2:years];
            
            [textField setIdLabel:(NSString*)kIDKey];
            [textField setDescriptionLabel:(NSString*)kDescriptionKey];
            [picker setTextField:textField];
            [picker setDataSource:self];
            [picker selectRow:textField.selectedID inComponent:0 animated:NO];
            [picker selectRow:textField.selectedID2 inComponent:1 animated:NO];
            
            [self showPicker:picker];
        }
            return NO;
            break;
            
        case textFieldTypeBirthdate:
        {
            [self dismissAllTextFields];
            [self dismissPicker:picker];
            [datePicker setTextField:textField];
            [datePicker setMaximumDate:[NSDate dateWithTimeIntervalSinceNow:-(3600*24)*365*18]];
            [datePicker setDatePickerMode:UIDatePickerModeDate];
            [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            [[textField text] length]==0?nil:[datePicker setDate:[dateFormatter dateFromString:[textField text]]];
            
            [self showPicker:datePicker];
        }
            return NO;
            break;
            
        case textFieldTypeRequiredCombo:
        {
            [self dismissAllTextFields];
            [self dismissPicker:datePicker];
            [picker setTextField:textField];
            [picker setDataSource:self];
            [picker reloadAllComponents];
            if ([textField.infoArray count]>0) {
                [picker selectRow:textField.selectedID inComponent:0 animated:NO];
                [picker.textField setText:[[picker.textField.infoArray objectAtIndex:picker.textField.selectedID] objectForKey:picker.textField.descriptionLabel]];
            }
            [self showPicker:picker];
        }
            return NO;
            break;
        case textFieldTypeDate:
        {
            [self dismissAllTextFields];
            [self dismissPicker:picker];
            [datePicker setTextField:textField];
            [datePicker setMaximumDate:textField.maxDate];
            [datePicker setMinimumDate:textField.minDate];
            [datePicker setDatePickerMode:UIDatePickerModeDate];
            [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            [[textField text] length]==0?nil:[datePicker setDate:[dateFormatter dateFromString:[textField text]]];
            
            [self showPicker:datePicker];
        }
            return NO;
            break;
            
        case textFieldTypedateAndTime:
        {
            [self dismissAllTextFields];
            [self dismissPicker:picker];
            [datePicker setTextField:textField];
            [datePicker setMaximumDate:textField.maxDate];
            [datePicker setMinimumDate:textField.minDate];
            [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
            [datePicker addTarget:self action:@selector(datePickerValueChangedWithHour:) forControlEvents:UIControlEventValueChanged];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
            [[textField text] length]==0?nil:[datePicker setDate:[dateFormatter dateFromString:[textField text]]];
            
            [self showPicker:datePicker];
        }
            return NO;
            break;
            
        case textFieldTypeCard:
        {
            textField.text = textField.hiddenString;
            return YES;
        }
            break;
            
        default:
        {
            [picker setTextField:textField];
            [self dismissPickers];
            return YES;
        }
            break;
    }
    
    return YES;
}

- (void) removeAllTextFields
{
    [usableTextFields removeAllObjects];
}

- (void) addValidationTextField:(UITextField_Validations*)textField
{
    [usableTextFields addObject:textField];
}

- (NSMutableArray*)getValidationTextFields
{
    return usableTextFields;
}

- (void)addValidationTextFieldsToDelegate
{
    for (UITextField_Validations *item in usableTextFields) {
        [item setDelegate: self];
    }
}

- (void)dismissAllTextFields
{
    for (UITextField_Validations *textField in usableTextFields) {
        if ([textField isKindOfClass:[UITextField_Validations class]]) {
            [textField resignFirstResponder];
        }
    }
}

- (void)addShadowToView:(UIView*)view
{
    view.layer.masksToBounds = false;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0, 5.0);
    view.layer.shadowOpacity = 0.34;
    view.layer.shadowRadius = 5.0;
    
    if ([view isKindOfClass:[UIButton class]]) {
        [view.layer setCornerRadius:5];
    }
}

- (BOOL) validateFieldsInArray:(NSArray *)fieldArray
{
    NSInteger error = 0;
    
    for (UITextField_Validations *textField in fieldArray) {
        if ([textField isKindOfClass:[UITextField_Validations class]]) {
            error = [textField validateField:error];
        }
    }
    
    if (error) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:[self getErrorMessageWithNumber:error] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return NO;
    } else {
        return YES;
    }
}

- (BOOL) validateFieldsWithoutAlertInArray:(NSArray *)fieldArray
{
    NSInteger error = 0;
    
    for (UITextField_Validations* textField in fieldArray) {
        if ([textField isKindOfClass:[UITextField_Validations class]]) {
            error = [textField validateField:error];
        }
    }
    
    if (error) {
        return NO;
    } else {
        return YES;
    }
}

- (NSString*)getErrorMessageWithNumber:(NSInteger)error
{
    NSMutableString *msg = [NSMutableString string];
    
    [msg appendString:NSLocalizedString(@"Verifique lo siguiente:\n", nil)];
    
    if (error & mTErrorCodeInvalidCharacterSet) {
        [msg appendFormat:NSLocalizedString(@"\n➜ Contiene caracteres inválidos", nil)];
    }
    if (error & mTErrorCodeInvalidLoginLength) {
        [msg appendFormat:NSLocalizedString(@"\n➜ El nombre de usuario debe ser mayor a 8 dígitos", nil)];
    }
    if (error & mTErrorCodeInvalidPasswordLength) {
        [msg appendFormat:NSLocalizedString(@"\n➜ La contraseña debe tener entre 8 y 12 caracteres de longitud", nil)];
    }
    if (error & mTErrorCodeNonProperLength) {
        [msg appendFormat:NSLocalizedString(@"\n➜ Hay campos que requieren un número mínimo de caracteres", nil)];
    }
    if (error & mTErrorCodeInvalidCardLength) {
        [msg appendFormat:NSLocalizedString(@"\n➜ El número de tarjeta debe tener entre 15 y 16 dígitos", nil)];
    }
    if (error & mTErrorCodeEmptyRequired) {
        [msg appendFormat:NSLocalizedString(@"\n➜ No pueden quedar campos requeridos vacíos", nil)];
    }
    if (error & mTErrorCodeInvalidComboSelection) {
        [msg appendFormat:NSLocalizedString(@"\n➜ Seleccione una opción válida", nil)];
    }
    if (error & mTErrorCodeInvalidEmail) {
        [msg appendFormat:NSLocalizedString(@"\n➜ Introduzca un email válido", nil)];
    }
    if (error & mtErrorCodeInvalidIAVELength || error & mTErrorCodeInvalidPASELength) {
        [msg appendFormat:NSLocalizedString(@"\n➜ Si el número de tu tag inicia con prefijo CPFI o IMDM, captura únicamente los 8 dígitos posteriores, en caso de que tu tag no contenga dichos prefijos captura los 11 dígitos", nil)];
    }
    if (error & mTErrorCodeInvalidRFC) {
        [msg appendFormat:NSLocalizedString(@"\n➜ Introduzca un RFC válido", nil)];
    }
    
    return msg;
}


#pragma Mark UIPickerView

- (void) initializePickersForView: (UIView*)view
{
    picker = [[UIPickerView_Automated alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 162)];
    [picker setShowsSelectionIndicator:YES];
    [picker setDelegate:self];
    [picker setAlpha:0.0];
    [picker setBackgroundColor:pickerBackgroundColor];
    [view addSubview:picker];
    
    datePicker = [[UIDatePicker_Automated alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 162)];
    [datePicker setBackgroundColor:pickerBackgroundColor];
    [datePicker setAlpha:0.0];
    [view addSubview:datePicker];
    
    graySheet = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [graySheet setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.85]];
    
    pickerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height/2-(162/2)-15, [[UIScreen mainScreen] bounds].size.width, 15)];
    [pickerTitleLabel setText:NSLocalizedString(@"Presiona OK para terminar la selección", nil)];
    [pickerTitleLabel setBackgroundColor:[UIColor clearColor]];
    [pickerTitleLabel setTextColor:[UIColor darkGrayColor]];
    [pickerTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [pickerTitleLabel setFont:[UIFont systemFontOfSize:12]];
    [graySheet addSubview:pickerTitleLabel];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height/2+(162/2)+25, [[UIScreen mainScreen] bounds].size.width, 45)];
    [okButton setTitle:@"OK" forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(dismissPickers) forControlEvents:UIControlEventTouchUpInside];
    [okButton setBackgroundColor:[UIColor darkGrayColor]];
    [graySheet addSubview:okButton];
    
    [view addSubview:graySheet];
    [view bringSubviewToFront:graySheet];
    [view bringSubviewToFront:picker];
    [view bringSubviewToFront:datePicker];
    [view bringSubviewToFront:okButton];
    
    [graySheet setAlpha:0.0];
}

#pragma mark UIPickerViewDelegate;
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView_Automated *)pickerView
{
    switch (pickerView.textField.type) {
        case textFieldTypeValidCard:
            return 2;
            break;
        default:
            return 1;
            break;
    }
}

- (NSInteger)pickerView:(UIPickerView_Automated *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (pickerView.textField.type) {
        case textFieldTypeValidCard:
        {
            return component?[pickerView.textField.infoArray2 count]:[pickerView.textField.infoArray count];
        }
            break;
            
        default:
            return [pickerView.textField.infoArray count];
            break;
    }
}

- (NSString *)pickerView:(UIPickerView_Automated *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self subPickerView:pickerView titleForRow:row forComponent:component];
}

- (NSString *)subPickerView:(UIPickerView_Automated *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (pickerView.textField.type) {
        case textFieldTypeValidCard:
        {
            return component?[NSString stringWithFormat:@"20%@", pickerView.textField.infoArray2[row][pickerView.textField.descriptionLabel]]:pickerView.textField.infoArray[row][pickerView.textField.descriptionLabel];
        }
            break;
            
        default:
            return pickerView.textField.infoArray[row][pickerView.textField.descriptionLabel];
            break;
    }
}


- (void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self subPickerView:pickerView didSelectRow:row inComponent:component];
}

- (void)subPickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([[pickerView.textField infoArray] count]>0) {
        switch (pickerView.textField.type) {
            case textFieldTypeValidCard:
                [pickerView.textField setText:[NSString stringWithFormat:@"%@/%@", pickerView.textField.infoArray[[pickerView selectedRowInComponent:0]][pickerView.textField.descriptionLabel], pickerView.textField.infoArray2[[pickerView selectedRowInComponent:1]][pickerView.textField.descriptionLabel]]];
                [pickerView.textField setSelectedID:[pickerView selectedRowInComponent:0]];
                [pickerView.textField setSelectedID2:[pickerView selectedRowInComponent:1]];
                break;
                
            default:
                [pickerView.textField setText:pickerView.textField.infoArray[row][pickerView.textField.descriptionLabel]];
                [pickerView.textField setSelectedID:(int)row];
                break;
        }
    }
}

#pragma mark UIDatePickerActions

- (void) datePickerValueChanged:(id)sender
{
    [self subDatePickerValueChanged:sender];
}

- (void) subDatePickerValueChanged:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    UIDatePicker_Automated *dateSender = sender;
    [dateSender.textField setText:[dateFormatter stringFromDate:[dateSender date]]];
}

- (void) datePickerValueChangedWithHour:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    UIDatePicker_Automated *dateSender = sender;
    [dateSender.textField setText:[dateFormatter stringFromDate:[dateSender date]]];
}


#pragma Mark Personal implementations

- (BOOL)checkLengthInTextField:(UITextField_Validations *)textField withRange:(NSRange)range andReplacementString:(NSString *)string
{
    BOOL response = [self SubcheckLengthInTextField:textField withRange:(NSRange)range andReplacementString:(NSString*)string];
    [self checkFormatInTextField:textField withRange:range andReplacementString:string];
    
    return response;
}

- (BOOL)SubcheckLengthInTextField:(UITextField_Validations *)textField withRange:(NSRange)range andReplacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if (newLength == 0) {
        [textField setTextFieldState:0];
        [textField updateValidation];
        return YES;
    }
    
    if (!(newLength > textField.maxLength)) {
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (![textField.allowedCharacters characterIsMember:c]) {
                [textField updateValidation];
                return NO;
            }
        }
        if (newLength<textField.minLength) {
            [textField setTextFieldState:2];
        } else {
            [textField setTextFieldState:1];
        }
        
        [textField updateValidation];
        return YES;
    }
    
    [textField updateValidation];
    return NO;
}

- (void)checkFormatInTextField:(UITextField_Validations *)textField withRange:(NSRange)range andReplacementString:(NSString *)string
{
    if ([textField type] == textFieldTypeEmail) {
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
        
        if ([emailTest evaluateWithObject:proposedNewString] == NO) {
            [textField setTextFieldState:2];
        }
    }
    
    if ([textField type] == textFieldTypeRFC) {
        NSString *rfcRegEx = @"^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\\d]{3})$";
        NSPredicate *rfcTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", rfcRegEx];
        
        NSString *proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
        
        if ([rfcTest evaluateWithObject:proposedNewString] == NO) {
            [textField setTextFieldState:2];
        }
    }
}

- (void) addTooly:(id)sender
{
    UITextField *textFieldReceiver = sender;
    
    UIToolbar *tooly = [[UIToolbar alloc] init];
    tooly.tintColor = [UIColor darkGrayColor];
    [tooly sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(dismissAllTextFields)];
    [tooly setItems:[NSArray arrayWithObjects:doneButton, nil]];
    textFieldReceiver.inputAccessoryView = tooly;
}

- (void) dismissPickers
{
    [self subDismissPickers];
}

- (void) subDismissPickers
{
    [self dismissPicker:picker];
    [self dismissPicker:datePicker];
}

- (void) dismissPicker:(id)pickerToHide
{
    [UIView beginAnimations:Nil context:nil];
    [UIView setAnimationDuration:0.3];
    [graySheet setAlpha:0.0];
    [graySheet setUserInteractionEnabled:NO];
    [pickerToHide setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 162)];
    [pickerToHide setAlpha:0.0];
    [UIView commitAnimations];
}

- (void) showPicker:(id)pickerToShow
{
    if (graySheet) {
        [UIView beginAnimations:Nil context:nil];
        [UIView setAnimationDuration:0.3];
        [graySheet setAlpha:1.0];
        [graySheet setFrame:[[UIScreen mainScreen] bounds]];
        [pickerToShow setFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height/2-(162/2), self.view.frame.size.width, 162)];
        [pickerToShow setAlpha:1.0];
        [graySheet setUserInteractionEnabled:YES];
        
        [UIView commitAnimations];
    } else {
        NSLog(@"Need to initialize pickers...");
    }
}

- (NSMutableDictionary*)cleanDictionary:(NSMutableDictionary*)resp
{
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:resp options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *response;
    
    if ([myString containsString:@",null"]) {
        myString = [myString stringByReplacingOccurrencesOfString:@",null" withString:@""];
        
        NSData *data =[myString dataUsingEncoding:NSUTF8StringEncoding];
        
        if(data!=nil){
            response = [NSMutableDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err]];
        }
    } else {
        response = resp;
    }
    
    for (id key in [response allKeys]) {
        if ([[response valueForKey:key] isKindOfClass:[NSDictionary class]]) {
            for (id key2 in [[response valueForKey:key] allKeys]) {
                if ([[[response valueForKey:key] valueForKey:key2] isKindOfClass:[NSNull class]]) {
                    [[response valueForKey:key] setObject:@"" forKey:key2];
                }
            }
        } else {
            if ([[response valueForKey:key] isKindOfClass:[NSNull class]]) {
                [response setObject:@"" forKey:key];
            }
            if ([[response valueForKey:key] isKindOfClass:[NSArray class]]) {
                for (int i = 0; i<[[response valueForKey:key] count]; i++) {
                    [[response valueForKey:key] replaceObjectAtIndex:i withObject:[self cleanDictionary:[[response valueForKey:key] objectAtIndex:i]]];
                }
            }
        }
    }
    
    return response;
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void) lockViewWithMessage:(NSString*)message
{
    if (!message) {
        message = NSLocalizedString(@"Procesando solicitud...", nil);
    }
    
    
    blocker = [[UIAlertView alloc] initWithTitle:message
                                         message:@"\n"
                                        delegate:self
                               cancelButtonTitle:nil
                               otherButtonTitles:nil];
    
    if (blockerArray) {
        [blockerArray addObject:blocker];
    }
    
    [blocker show];
}

- (void) unLockView
{
    if (blockerArray) {
        for (UIAlertView *blockWindow in blockerArray) {
            [blockWindow dismissWithClickedButtonIndex:0 animated:YES];
        }
        [blockerArray removeAllObjects];
    } else {
        [blocker dismissWithClickedButtonIndex:0 animated:YES];
    }
}

- (NSString *) platformString{
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4 CDMA";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6S";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6S Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7 (CDMA)";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus (CDMA)";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7 (GSM)";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus (GSM)";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (Cellular)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (Cellular)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (Cellular)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (Cellular)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (Cellular)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (Cellular)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (Cellular)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (Cellular)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return @"Unknown";
}

- (void)managerFailed:(pootEngine *)manager withKey:(NSString *)key errorCode:(pootEngineErrorCode)error
{
    [self unLockView];
    
    if (manager.tag == ignoreConnection) {
        return;
    }
    
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Error de conexión, asegúrese de estar conectado a una red",nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertMsg show];
    
    [[self navigationController] popViewControllerAnimated:YES];
}

- (NSString*) encryptString:(NSString*)string
{
    NSData *plainData = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *stringSha = [NSData sha512:plainData];
    NSString *base64String = [stringSha base64EncodedStringWithOptions:0];
    
    return base64String;
}

- (NSString *)maskCardStringWithString:(NSString *)string
{
    NSMutableString *maskedCard = [[NSMutableString alloc] init];
    
    [maskedCard appendString:[string substringWithRange:NSMakeRange(0, 4)]];
    [maskedCard appendString:@" XXXX XXXX "];
    [maskedCard appendString:[string substringWithRange:NSMakeRange([string length]-4, 4)]];
    
    return maskedCard;
}


@end
