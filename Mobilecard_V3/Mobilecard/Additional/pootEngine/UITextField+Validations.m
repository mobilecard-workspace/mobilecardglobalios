//
//  UITextField+Validations.m
//  mT
//
//  Created by David Poot on 11/27/13.
//  Copyright (c) 2013 addcel. All rights reserved.
//

#import "UITextField+Validations.h"
#import "MC_Colors.h"


//#define invalidColor [UIColor redColor]
//#define validColor [UIColor clearColor]


@implementation UITextField_Validations: JVFloatLabeledTextField

- (void)drawRect:(CGRect)rect
{
    if (_underline) {
        [_underline setFrame:CGRectMake(0, self.frame.size.height-1, self.frame.size.width, _required?1:1)];
    } else {
        _underline = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-1, self.frame.size.width, _required?1:1)];
    }
    
    
    
    //comentado para que mientas sea naranja todos las lineas de los campos de texto
    //[_underline setBackgroundColor:[self underlineActiveColor]];
   
  

    [_underline setBackgroundColor:[UIColor darkMCOrangeColor]];
    
    if (![[self subviews] containsObject:_underline]) {
        [self addSubview:_underline];
        [self sendSubviewToBack:_underline];
    }
    
 
    
}

/*
 - (void) drawPlaceholderInRect:(CGRect)rect {
 
 UIColor *placeholderColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
 
 if (_blackInWhite) {
 placeholderColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
 }
 
 UIFont *placeholderFont = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
 
 NSDictionary *attributes = @{
 NSForegroundColorAttributeName: placeholderColor,
 NSFontAttributeName : placeholderFont
 };
 
 CGSize textSize = [self.placeholder sizeWithAttributes:attributes];
 CGFloat hdif = rect.size.height - textSize.height;
 hdif = MAX(0, hdif);
 rect.origin.y += ceil(hdif/2.0);
 
 self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
 
 [[self placeholder] drawInRect:rect withAttributes:attributes];
 }*/

- (UIColor *)underlineActiveColor
{
    if (_required) {
        switch (_textFieldState) {
            case 0:
            {
                if (_active) {
                    if (_underlineColor) {
                        return _underlineColor;
                    }
                    else if ([self respondsToSelector:@selector(tintColor)]) {
                        return [self performSelector:@selector(tintColor)];
                    }
                    return [UIColor blueColor];
                } else {
                    
                    
                    return [UIColor darkMCGreyColor];
                }
            }
                break;
            case 1:
                if (_active) {
                    return _validColor;
                } else {
                    if (_underlineColor) {
                        return _underlineColor;
                    }
                    else {
                        return [UIColor darkMCGreyColor];
                    }
                }
                
                break;
                
            default:
                return _invalidColor;
                break;
        }
    } else {
        if (_active) {
            if (_underlineColor) {
                return _underlineColor;
            }
            else if ([self respondsToSelector:@selector(tintColor)]) {
                return [self performSelector:@selector(tintColor)];
            }
            return [UIColor blueColor];
        } else {
            return [UIColor darkMCGreyColor];
        }
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self.underlineColor = [UIColor darkMCGreyColor];
    
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}


- (void)setUpTextFieldAs:(textFieldType)tfType
{
    //General configuration
    
    _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"áéíóúabcdefghijklmnñopqrstuvwxyz@.-_1234567890 ÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ,+/"];
    
    _validColor = [UIColor colorWithRed:0.0 green:128.0/255.0 blue:0.0 alpha:0.8];
    _invalidColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.8];
    
    
    
    //_validColor = self.backgroundColor;
    //_invalidColor = self.backgroundColor;
    
    [self setKeyboardType:UIKeyboardTypeDefault];
    _required = YES;
    //[self setPlaceholder:NSLocalizedString(@"Requerido", nil)];
    //UIColor *placeholderColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    //UIFont *placeholderFont = [UIFont fontWithName:@"GillSans-LightItalic" size:14.0];
    
    //[self setValue:placeholderColor forKeyPath:@"_placeholderLabel.textColor"];
    //[self setValue:placeholderFont forKeyPath:@"_placeholderLabel.font"];
    
    _type = tfType;
    _minLength = 1;
    _maxLength = 50;
    
    [self setAllowEdition:YES];
    
    //Special configuration
    switch (tfType) {
        case textFieldTypeRFC:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZÑ&|1234567890"];
            [self setMinLength:12];
            [self setMaxLength:13];
            [self setKeyboardType:UIKeyboardTypeDefault];
            [self setAutocapitalizationType:UITextAutocapitalizationTypeAllCharacters];
            break;
            
        case textFieldTypeUserName:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"áéíóúÁÉÍÓÚabcdefghijklmnñopqrstuvwxyz1234567890ABCDEFGHIJKLMNÑOPQRSTUVWXYZ ._@"];
            [self setMinLength:3];
            [self setMaxLength:50];
            [self setKeyboardType:UIKeyboardTypeDefault];
            [self setAutocapitalizationType:UITextAutocapitalizationTypeWords];
            break;
            
        case textFieldTypePassword:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ.#*=&-_,<>+!¡¿?[]$%/()@"];
            [self setMinLength:8];
            [self setMaxLength:12];
            [self setKeyboardType:UIKeyboardTypeDefault];
            [self setSecureTextEntry:YES];
            break;
            
        case textFieldTypeTagNameTag:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ "];
            [self setMaxLength:20];
            [self setKeyboardType:UIKeyboardTypeDefault];
            break;
            
        case textFieldTypeCvv2:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            [self setMinLength:3];
            [self setMaxLength:4];
            [self setSecureTextEntry:YES];
            break;
            
        case textFieldTypeValidCard:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890/"];
            [self setAllowEdition:NO];
            break;
            
        case textFieldTypeCard:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            [self setMinLength:15];
            [self setMaxLength:16];
            break;
            
        case textFieldTypeCellphone:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            [self setMinLength:1];
            [self setMaxLength:10];
            break;
            
        case textFieldTypeZip:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            [self setMaxLength:6];
            break;
            
        case textFieldTypeZipAMEX:
            [self setPlaceholder:NSLocalizedString(@"Requerido sólo para AMEX", nil)];
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            [self setMaxLength:6];
            break;
            
        case textFieldTypeAddressAMEX:
            [self setPlaceholder:NSLocalizedString(@"Requerido sólo para AMEX", nil)];
            [self setMaxLength:20];
            break;
            
        case textFieldTypeExtNum:
        case textFieldTypeGeneralNumericRequired:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            break;
            
        case textFieldTypeEmail:
            [self setKeyboardType:UIKeyboardTypeEmailAddress];
            break;
            
        case textFieldTypeIAVETag:
        case textFieldTypePASETag:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            [self setMinLength:8];
            [self setMaxLength:11];
            break;
            
        case textFieldTypeIAVEDV:
        case textFieldTypePASEDV:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            [self setMaxLength:1];
            [self setHelpString:@"\n➜ Si el número de tu tag inicia con prefijo CPFI o IMDM, captura únicamente los 8 dígitos posteriores, en caso de que tu tag no contenga dichos prefijos captura los 11 dígitos"];
            break;
            
            //NON REQUIRED
        case textFieldTypeMotherLastName:
        case textFieldTypeIntNum:
        case textFieldTypeGeneralNoRequired:
            _required = NO;
            [self setMinLength:0];
            [self setMaxLength:50];
            [self setAutocapitalizationType:UITextAutocapitalizationTypeWords];
            break;
            
        case textFieldTypeHomePhone:
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            _required = NO;
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            [self setMinLength:0];
            [self setMaxLength:10];
            break;
            
        case textFieldTypeGeneralNumericNoRequired:
            _required = NO;
            [self setMinLength:0];
            [self setMaxLength:10];
            _customCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
            [self setKeyboardType:UIKeyboardTypeNumberPad];
            break;
            
        case textFieldTypeBirthdate:
        case textFieldTypeRequiredCombo:
        case textFieldTypeDate:
        case textFieldTypedateAndTime:
            [self setAllowEdition:NO];
            break;
            
        default:
            break;
    }
    
    [self setNeedsDisplay];
    _allowedCharacters = _customCharSet;
}

- (void) disable
{
    [self setEnabled:NO];
    //[self setBackgroundColor:[UIColor clearColor]];
}

- (void) enable
{
    [self setEnabled:YES];
    //[self setBackgroundColor:_validColor];
}

- (void)updateValidation
{
    [self setNeedsDisplay];
}

- (NSInteger)validateField:(NSInteger)error
{
    _textFieldState = 0;
    if (self.enabled) {
        if (_required) {
            _textFieldState = 1;
            if ([[self text] length]==0) {
                error |= mTErrorCodeEmptyRequired;
                _textFieldState = 2;
                return error;
            }
            if (_type == textFieldTypeRequiredCombo) {
                if ([_infoArray[_selectedID][_idLabel] intValue] < 0) {
                    error |= mTErrorCodeInvalidComboSelection;
                    _textFieldState = 2;
                    return error;
                }
            }
            
            if ([[self text] length]<_minLength||[[self text] length]>_maxLength) {
                _textFieldState = 2;
                switch (_type) {
                    case textFieldTypePassword:
                        error |= mTErrorCodeInvalidPasswordLength;
                        break;
                    case textFieldTypeUserName:
                        error |= mTErrorCodeInvalidLoginLength;
                        break;
                    case textFieldTypeCard:
                        error |= mTErrorCodeInvalidCardLength;
                        break;
                    case textFieldTypePASETag:
                    case textFieldTypeIAVETag:
                        error |= mtErrorCodeInvalidIAVELength;
                        break;
                        
                    default:
                        error |= mTErrorCodeNonProperLength;
                        break;
                }
            } else {
                switch (_type) {
                    case textFieldTypeIAVETag:
                    case textFieldTypePASETag:
                    {
                        if (!([[self text] length] == _minLength)) {
                            if (!([[self text] length] == _maxLength)) {
                                _textFieldState = 2;
                                error |= mtErrorCodeInvalidIAVELength;
                            }
                        }
                    }
                        break;
                        
                    default:
                        break;
                }
            }
            if (_type == textFieldTypeCard) {
                if ([self.hiddenString rangeOfCharacterFromSet:[[self allowedCharacters] invertedSet]].location != NSNotFound) {
                    _textFieldState = 2;
                    error |= mTErrorCodeInvalidCharacterSet;
                }
            } else {
                if ([self.text rangeOfCharacterFromSet:[[self allowedCharacters] invertedSet]].location != NSNotFound) {
                    _textFieldState = 2;
                    error |= mTErrorCodeInvalidCharacterSet;
                }
            }
            
            
            
            if (self.type == textFieldTypeEmail) {
                NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
                
                if ([emailTest evaluateWithObject:[self text]] == NO) {
                    _textFieldState = 2;
                    error |= mTErrorCodeInvalidEmail;
                }
            }
            
            if (self.type == textFieldTypeRFC) {
                NSString *rfcRegEx = @"^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\\d]{3})$";
                NSPredicate *rfcTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", rfcRegEx];
                
                if ([rfcTest evaluateWithObject:[self text]] == NO) {
                    _textFieldState = 2;
                    error |= mTErrorCodeInvalidRFC;
                }
            }
        }
        
    }
    [self setNeedsDisplay];
    return error;
}

- (void) changeRequiredStatusTo:(BOOL)newRequiredStatus
{
    [self setBackgroundColor:_validColor];
    if (newRequiredStatus) {
        _required = newRequiredStatus;
        //[self setPlaceholder:NSLocalizedString(@"Requerido", nil)];
    } else {
        _required = newRequiredStatus;
        //[self setPlaceholder:@""];
    }
}


- (void) getSelectedIDWithID:(int)ID
{
    _selectedID = 0;
    for (int i = 0; i<[self.infoArray count]; i++) {
        if (ID == [self.infoArray[i][self.idLabel] intValue]) {
            _selectedID = i;
        }
    }
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */


@end
