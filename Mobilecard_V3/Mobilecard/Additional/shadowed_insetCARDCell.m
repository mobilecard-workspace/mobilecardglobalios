//
//  shadowed_insetCARDCell.m
//  Mobilecard
//
//  Created by David Poot on 10/31/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "shadowed_insetCARDCell.h"
#import "MC_Colors.h"

#define inset 30 

@implementation shadowed_insetCARDCell

- (void)setFrame:(CGRect)frame {
    frame.origin.x += inset;
    frame.size.width -= 2 * inset;
    [super setFrame:frame];
    
//    
//    self.layer.masksToBounds = false;
//    self.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.layer.shadowOffset = CGSizeMake(0, 5.0);
//    self.layer.shadowOpacity = 0.34;
//    self.layer.shadowRadius = 5.0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger theHighScore = [defaults integerForKey:@"highlightView"];
    if (theHighScore) {
        if (selected) {
            // [_highlightView setBackgroundColor:[UIColor lightMCOrangeColor]];
            [_highlightView setHidden:NO];
        } else {
            // [_highlightView setBackgroundColor:[UIColor lightMCGreyColor]];
            [_highlightView setHidden:YES];
        }
        
    }
//    [_editButton setEnabled:selected];
//    _editButton.hidden =YES;
}

@end
