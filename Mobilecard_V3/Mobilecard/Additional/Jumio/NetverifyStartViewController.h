//
//  NetverifyStartViewController.h
//
//  Copyright © 2019 Jumio Corporation All rights reserved.
//

#import "StartViewController.h"
#import "MC_Colors.h"
#import "pootEngine.h"
#import "mT_appLinks.h"
#import "mT_commonController.h"

@interface NetverifyStartViewController : StartViewController<pootEngineDelegate>{
    
    mT_commonController *commotSelf;
}

@property (nonatomic, weak) IBOutlet UISwitch *switchEnableVerification;
@property (nonatomic, weak) IBOutlet UISwitch *switchEnableIdentityVerification;

- (IBAction) startNetverify: (id) sender;

@end
