//
//  MC_SAPOverlayView.h
//  mobilecard
//
//  Created by David Poot on 1/9/13.
//  Copyright (c) 2013 Addcel. All rights reserved.
//

#import "mT_commonController.h"

@interface MC_SAPOverlayView : mT_commonController  

@property (unsafe_unretained, nonatomic) IBOutlet UIView *backButtonView;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end
