//
//  MC_SAPOverlayView.m
//  mobilecard
//
//  Created by David Poot on 1/9/13.
//  Copyright (c) 2013 Addcel. All rights reserved.
//

#import "MC_SAPOverlayView.h"
#import <QuartzCore/QuartzCore.h>

@interface MC_SAPOverlayView ()

@end

@implementation MC_SAPOverlayView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    //[self setBackButtonView:nil];
    [super viewDidUnload];
}
@end
