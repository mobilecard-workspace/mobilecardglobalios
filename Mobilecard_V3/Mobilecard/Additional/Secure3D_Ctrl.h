//
//  3DSecure_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 1/15/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "mT_commonController.h"

@protocol Secure3DDelegate <NSObject>

@optional
- (void)PaymentWithThreatMetrixResponse:(NSDictionary*)response;

@required
- (void)Secure3DResponse:(NSDictionary *)response;

@end

@interface Secure3D_Ctrl : mT_commonController <UIWebViewDelegate, pootEngineDelegate, UIActionSheetDelegate>

@property (nonatomic, assign) id <Secure3DDelegate, NSObject> delegate;

@property (strong, nonatomic) NSMutableDictionary *purchaseInfo;

@property (strong, nonatomic) NSMutableDictionary *threatMetrixInfo;

@property (strong, nonatomic) NSMutableDictionary *selectedCardInfo;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *finishButton;

@property (strong, nonatomic) NSString *Secure3DURL;

@property BOOL isAntad;

@property BOOL didFinishPurchase;

@property (assign, nonatomic) serviceType type;
@property (weak, nonatomic) IBOutlet UIToolbar *finishBar;

@end
