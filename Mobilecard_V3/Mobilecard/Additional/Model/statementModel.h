//
//  statementModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/17/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface statementModel : NSObject
@property (nonatomic, assign)    NSInteger idStatement,importe,status,total,comision;

@property (nonatomic, copy)      NSString  *title_month;
@property (nonatomic, copy)      NSString  *date,*ticket;
@property (nonatomic, copy)      NSMutableArray  *dataCard;

@end

NS_ASSUME_NONNULL_END
