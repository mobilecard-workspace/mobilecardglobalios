//
//  cardStatementModel.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/17/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface cardStatementModel : NSObject
@property (nonatomic, assign)    NSInteger balance,idTarjeta;
@property (nonatomic, assign)    BOOL determinada,previvale,mobilecard;
@property (nonatomic, copy)      NSString  *nombre,*pan,*tipo,*vigencia,*codigo,*domAmex,*cpAmex,*tipoTarjeta,*phoneNumberActivation;
@property (nonatomic, copy)      NSString  *clave,*img_short,*img_full;

@end

NS_ASSUME_NONNULL_END
