//
//  shift_Step00.m
//  Mobilecard
//
//  Created by David Poot on 9/26/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "shift_Step00.h"

@interface shift_Step00 ()
{
    pootEngine *createManager;
    pootEngine *statesManager;
    
    NSArray *statesArray;
}
@end

@implementation shift_Step00

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:_continue_Button];
    
    [self initializePickersForView:self.navigationController.view];
    
    [_phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_emailText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"áéíóúabcdefghijklmnñopqrstuvwxyz@.-_1234567890 ÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ,+"]];
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_lastnameText setUpTextFieldAs:textFieldTypeLastName];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_intNumberText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_intNumberText setMaxLength:5];
    [_cityText setUpTextFieldAs:textFieldTypeCity];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    [_zipText setMaxLength:5];
    [_zipText setMinLength:5];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_ssnText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_ssnText setMaxLength:20];
    [_birthdateText setUpTextFieldAs:textFieldTypeBirthdate];
    
    [self addValidationTextField:_phoneText];
    [self addValidationTextField:_emailText];
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_lastnameText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_intNumberText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_zipText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_ssnText];
    [self addValidationTextField:_birthdateText];
    
    [self addValidationTextFieldsToDelegate];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    if (userData) {
        [_phoneText setText:userData[kUserPhone]];
        [_emailText setText:userData[kUserEmail]];
        [_nameText setText:userData[kUserName]];
        [_lastnameText setText:userData[kUserLastName]];
        //[_addressText setText:userData[@"usrDireccion"]];
        //[_intNumberText setText:userData[@"usrNumInterior"]];
        //[_cityText setText:userData[@"usrCiudad"]];
        
        //[_zipText setText:userData[@"usrCp"]];
        /*
        [_phoneText setText:@"6202368058"];
        [_emailText setText:@"dpootr001+test2@gmail.com"];
        [_nameText setText:@"Pedrin"];
        [_lastnameText setText:@"Marton"];
        [_cityText setText:@"Beverly Hills"];
        [_zipText setText:@"90210"];
        
        [_ssnText setText:@"601830000"];
         */
    }
    
    statesManager = [[pootEngine alloc] init];
    [statesManager setDelegate:self];
    [statesManager setShowComments:developing];
    
    [statesManager startRequestWithURL:[NSString stringWithFormat:@"%@/%@/estados", walletManagementURL, userData[kUserIdCountry]]];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)continue_Action:(id)sender {
    /*
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }*/
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    createManager = [[pootEngine alloc] init];
    [createManager setDelegate:self];
    [createManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:userData[kUserIDKey] forKey:@"idUser"];
    [params setObject:_phoneText.text forKey:@"phoneNumber"];
    [params setObject:_emailText.text forKey:@"email"];
    [params setObject:_nameText.text forKey:@"firstName"];
    [params setObject:_lastnameText.text forKey:@"lastName"];
    [params setObject:_addressText.text forKey:@"address"];
    [params setObject:_intNumberText.text forKey:@"apt"];
    [params setObject:_cityText.text forKey:@"city"];
    
    [params setObject:[_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]] forKey:@"state"];
    [params setObject:_zipText.text forKey:@"zip"];
    [params setObject:_ssnText.text forKey:@"ssn"];
    [params setObject:[NSString stringWithFormat:@"%@-%@-%@", [_birthdateText.text componentsSeparatedByString:@"-"][1], [_birthdateText.text componentsSeparatedByString:@"-"][0], [_birthdateText.text componentsSeparatedByString:@"-"][2]] forKey:@"birthday"];
    [params setObject:@"IOS" forKey:@"plataforma"];
    
    
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [createManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/Shift/%d/%@/%@/create/user", simpleServerURL, idApp, userData[kUserIdCountry], NSLocalizedString(@"lang", nil)] withPost:JSONString];
    
    [self lockViewWithMessage:nil];
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if ( manager == statesManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0) {
            [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
            [_stateText setInfoArray:[NSArray arrayWithArray:response[@"estados"]]];
            [_stateText setDescriptionLabel:@"nombre"];
            [_stateText setIdLabel:@"abreviatura"];
        }
    }
    
    if (manager == createManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        switch ([response[kIDError] intValue]) {
            case 0:
            {
                if ([_delegate conformsToProtocol:@protocol(shiftCreationDelegate)]&&[_delegate respondsToSelector:@selector(shiftCreation_Result:)]) {
                    developing?NSLog(@"Shall launch shift result with -> %@", [NSDictionary dictionaryWithDictionary:response]):nil;
                    [_delegate shiftCreation_Result:[NSDictionary dictionaryWithDictionary:response]];
                }
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
                break;
                
            default:
            {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[@"errorMessage"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
            }
                break;
        }
    }
}
@end
