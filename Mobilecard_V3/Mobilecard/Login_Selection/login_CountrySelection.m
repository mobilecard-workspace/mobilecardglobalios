//
//  login_CountrySelection.m
//  Mobilecard
//
//  Created by David Poot on 9/8/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "login_CountrySelection.h"
#import "login_UserTypeSelection.h"
#import "SignIn_Ctrl.h"
#import <Mobilecard-Swift.h>

@interface login_CountrySelection ()
{
    pootEngine *countryManager;
    NSMutableArray *countryArray;
    
    
    int selectedId;
}
@end

@implementation login_CountrySelection

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    //DELETE
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserType];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    developing?NSLog(@"User -> %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]):nil;
    developing?NSLog(@"UserType -> %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType]):nil;
    //DELETE
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
            //[self performSegueWithIdentifier:@"startMain" sender:nil];
            ////// reemplazo de codigo para ingresar al view de log-in Raul mendez ///////////
            [self performSegueWithIdentifier:@"startMain" sender:nil];
        } else if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
            [self performSegueWithIdentifier:@"startMain_Commerce" sender:nil];
        } else {
            [self performSegueWithIdentifier:@"startMain" sender:nil];
        }
    }
    
    countryArray = [[NSMutableArray alloc] init];
    
    countryManager = [[pootEngine alloc] init];
    [countryManager setDelegate:self];
    [countryManager setShowComments:developing];
    
    [countryManager startWithoutPostRequestWithURL:WSCatalogGetCountries];
    
    [self lockViewWithMessage:nil];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [countryArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [countryArray[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:countryArray[indexPath.row][kIdentifierKey] forIndexPath:indexPath];
    
    if ([countryArray[indexPath.row][kIdentifierKey] isEqualToString:@"data"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
                [label setText:countryArray[indexPath.row][kDescriptionKey][@"nombre"]];
            }
            if ([element isKindOfClass:[UIImageView class]]) {
                UIImageView *image = element;
                if (image.tag == 1) {
                    pootEngine *imageManager;
                    imageManager = [[pootEngine alloc] init];
                    
                    
                    [imageManager startImageRequestWithURL:countryArray[indexPath.row][kDescriptionKey][@"url"] andImageView:image];
                    
                    
                  
                }
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedId = (int)indexPath.row;
    
    [[NSUserDefaults standardUserDefaults] setObject:countryArray[selectedId][kDescriptionKey] forKey:(NSString*)kUserIdCountry];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
NSLog(@"%@", countryArray[indexPath.row][kDescriptionKey][@"codigo"]);

    NSString *a = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"];
   
    NSLog(@"%s","adkasndssaodosa");
    NSLog(@"%@",a);
    
    DataUser.COUNTRY = countryArray[selectedId][kDescriptionKey][@"codigo"];
    
    
    
    if ([countryArray[indexPath.row][kDescriptionKey][@"codigo"] isEqualToString:@"MX"] || [countryArray[indexPath.row][kDescriptionKey][@"codigo"] isEqualToString:@"PE"]) {
        [self performSegueWithIdentifier:@"userSelection" sender:nil];
    } else {
        [self performSegueWithIdentifier:@"startMain" sender:nil];
    }
}


#pragma mark pootEngine Handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (countryManager == manager) {
        [countryArray removeAllObjects];
        
        [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"120", kHeightKey, @"header", kIdentifierKey, nil]];
        
        NSArray *countryRaw = (NSArray*)json;
        
        for (NSDictionary *element in countryRaw) {
            [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"100", kHeightKey, @"data", kIdentifierKey, element, kDescriptionKey, nil]];
            [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"15", kHeightKey, @"space", kIdentifierKey, nil]];
        }
        
        [self.tableView reloadData];
    }
}


@end
