//
//  previvale_Step10.m
//  Mobilecard
//
//  Created by David Poot on 2/5/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "previvale_Step10.h"

@implementation previvale_Step10
{
    pootEngine *statesManager;
    pootEngine *activateManager;
    
    NSArray *stateArray;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
   // [self addShadowToView:_activate_button];
    
    [self initializePickersForView:self.navigationController.view];
    
    [_cardText setUpTextFieldAs:textFieldTypeCard];
    [_cardText setRequired:NO];
    
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_colonyText setUpTextFieldAs:textFieldTypeAddress];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_cityText setUpTextFieldAs:textFieldTypeCity];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    [_rfcText setUpTextFieldAs:textFieldTypeRFC];
    [_rfcText setRequired:NO];
    [_curpText setUpTextFieldAs:textFieldTypeAddress];
    
    [self addValidationTextField:_cardText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_colonyText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_zipText];
    [self addValidationTextField:_rfcText];
    [self addValidationTextField:_curpText];
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
        [_nameText setUpTextFieldAs:textFieldTypeName];
        [_lastnameText setUpTextFieldAs:textFieldTypeLastName];
        [_motherlastnameText setUpTextFieldAs:textFieldTypeMotherLastName];
        
        [self addValidationTextField:_nameText];
        [self addValidationTextField:_lastnameText];
        [self addValidationTextField:_motherlastnameText];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [_nameText setText:userData[kUserName]];
        [_lastnameText setText:userData[kUserLastName]];
        [_motherlastnameText setText:userData[kUserMotherLastName]];
    }
    
    [self addValidationTextFieldsToDelegate];
    
    statesManager = [[pootEngine alloc] init];
    [statesManager setDelegate:self];
    [statesManager setShowComments:developing];
    
    [statesManager startRequestWithURL:[NSString stringWithFormat:@"%@/%@/estados", walletManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"]]];
    
    [self lockViewWithMessage:nil];
}

- (IBAction)back_Action:(id)sender {
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)activate_Action:(id)sender {
    
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    activateManager = [[pootEngine alloc] init];
    [activateManager setDelegate:self];
    [activateManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:_cardText.hiddenString?[activateManager encryptJSONString:_cardText.hiddenString withPassword:nil]:@"" forKey:@"pan"];
    [params setObject:[_addressText text] forKey:@"usr_direccion"];
    [params setObject:[_colonyText text] forKey:@"usr_colonia"];
    [params setObject:[_cityText text] forKey:@"usr_ciudad"];
    [params setObject:[_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]] forKey:@"usr_id_estado"];
    [params setObject:[_zipText text] forKey:@"usr_cp"];
    [params setObject:[_rfcText text] forKey:@"rfc"];
    [params setObject:[_curpText text] forKey:@"curp"];
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"NEGOCIO"]) {
        [params setObject:[_nameText text] forKey:@"usr_nombre"];
        [params setObject:[_lastnameText text] forKey:@"paterno"];
        [params setObject:[_motherlastnameText text] forKey:@"materno"];
    }
    
    [params setObject:[NSNumber numberWithInt:idApp] forKey:@"id_aplicacion"];
    [params setObject:[NSNumber numberWithInt:0] forKey:@"id_usuario"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    NSString *userType = [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] lowercaseString];
    
    [activateManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@/%@/%@/%@/%@/registrartarjetaprevivale", walletManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], NSLocalizedString(@"lang", nil), userType, [userType isEqualToString:@"negocio"]?userDetails[@"idEstablecimiento"]:userDetails[kUserIDKey]] withPost:JSONString];
    
    [self lockViewWithMessage:nil];
}


- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json {
    [self unLockView];
    
    if ( manager == statesManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0) {
            stateArray = response[@"estados"];
            [_stateText setInfoArray:stateArray];
            [_stateText setIdLabel:@"id"];
            [_stateText setDescriptionLabel:@"nombre"];
        }
    }
    
    if (manager == activateManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        
        response = [self cleanDictionary:response];
        
        if ([response[kIDError] intValue]==0) {
            if ([_delegate conformsToProtocol:@protocol(previvaleRegisterProtocol)]&&[_delegate respondsToSelector:@selector(previvaleRegisterResponse:)]) {
                [_delegate previvaleRegisterResponse:response];
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
}

#pragma Mark UITextFields handler

- (void)textFieldDidEndEditing:(UITextField_Validations *)textField
{
    if (textField == _cardText) {
        textField.hiddenString = textField.text;
        NSMutableString *card = [[NSMutableString alloc]init];
        if ([_cardText.text length]!=0 && [_cardText.text length]>4){
            for (int i=0; i<([textField.text length]-4); i++) {
                [card appendString:@"X"];
            }
            for (int i=((int)[_cardText.text length]-4); i<[_cardText.text length]; i++) {
                [card appendString:[NSString stringWithFormat:@"%c",[_cardText.text characterAtIndex:i]]];
            }
            _cardText.text = card;
        }
        
        [self.tableView reloadData];
    }
}

@end
