//
//  colombia_TopUp_Step00.m
//  Mobilecard
//
//  Created by David Poot on 11/4/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "colombia_TopUp_Step00.h"
#import "colombia_TopUp_Step10.h"

@interface colombia_TopUp_Step00 ()
{
    pootEngine *servicesManager;
    
    NSMutableArray *tableData;
}
@end

@implementation colombia_TopUp_Step00

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    servicesManager = [[pootEngine alloc] init];
    [servicesManager setDelegate:self];
    [servicesManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSNumber numberWithInt:idApp] forKey:@"idAplicacion"];
    [params setObject:_serviceData[@"idProducto"] forKey:@"idProducto"]; //SELECTING RECARGAS
    [params setObject:@"0" forKey:@"idProveedor"];
    [params setObject:@"0" forKey:@"idRecarga"];
    [params setObject:@"0" forKey:@"idServicio"];
    [params setObject:@"0" forKey:@"idUsuario"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:colHeader, @"Authorization", nil];
    
    [servicesManager startJSONRequestWithURL:WSColombiaGetData withPost:JSONString andHeader:header];
    
    [self lockViewWithMessage:nil];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableData[indexPath.row][kIdentifierKey] forIndexPath:indexPath];
    
    if ([tableData[indexPath.row][kIdentifierKey] isEqualToString:@"data"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *title = element;
                [title setText:tableData[indexPath.row][kDescriptionKey][@"label"]];
            }
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableData[indexPath.row][kHeightKey] floatValue];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"topup_step10" sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"topup_step10"]) {
        colombia_TopUp_Step10 *nextView = [segue destinationViewController];
        
        NSIndexPath *indexpath = sender;
        [nextView setServicesData:_serviceData];
        [nextView setServiceData:tableData[indexpath.row][kDescriptionKey]];
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == servicesManager) {
        NSArray *response = (NSArray*)json;
        
        tableData = [[NSMutableArray alloc] init];
        
        [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"70", kHeightKey, nil]];
        for (NSDictionary *element in response) {
            [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"data", kIdentifierKey, @"90", kHeightKey, element, kDescriptionKey, nil]];
            [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, nil]];
        }
        
        [self.tableView reloadData];
    }
}
@end
