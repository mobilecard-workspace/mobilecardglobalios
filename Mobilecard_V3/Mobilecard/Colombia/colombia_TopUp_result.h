//
//  colombia_TopUp_result.h
//  Mobilecard
//
//  Created by David Poot on 11/4/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface colombia_TopUp_result : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *resultData;

@property (weak, nonatomic) IBOutlet UIView *result;
@property (weak, nonatomic) IBOutlet UIImageView *resultIcon;
@property (weak, nonatomic) IBOutlet UILabel *resultTitleText;
@property (weak, nonatomic) IBOutlet UITextView *resultDetailsText;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@end

NS_ASSUME_NONNULL_END
