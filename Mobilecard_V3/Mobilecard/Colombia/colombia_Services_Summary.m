//
//  colombia_Services_Summary.m
//  Mobilecard
//
//  Created by David Poot on 11/5/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "colombia_Services_Summary.h"

@interface colombia_Services_Summary ()
{
    NSMutableArray *menuItems;
}
@end

@implementation colombia_Services_Summary

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self addShadowToView:_confirmButton];
    
    menuItems = [[NSMutableArray alloc] init];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"40", kHeightKey, nil]];//SPACE
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:_mainHeight, kHeightKey, nil]];//MAIN TEXT
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"20", kHeightKey, nil]];//SPACE
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"195", kHeightKey, nil]];//PURCHASE DATA
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"130", kHeightKey, nil]];//BUTTON
    
    [self updateData];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirm_Action:(id)sender {
    if ([_delegate conformsToProtocol:@protocol(generalSummaryDelegate)]&&[_delegate respondsToSelector:@selector(generalSummaryResponse:)]) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            [_delegate generalSummaryResponse:nil];
        }];
    }
}

- (void)updateData
{
    [_mainText setText:_mainString];
    
    [_mainText setSelectable:NO];
    [_mainText setScrollEnabled:NO];
    [_mainText setEditable:NO];
    [_firstText setText:_firstString];
    [_secondText setText:_secondString];
    [_thirdText setText:_thirdString];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

@end
