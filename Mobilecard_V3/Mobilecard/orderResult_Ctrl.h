//
//  orderResult_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 4/28/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonController.h"

@interface orderResult_Ctrl : mT_commonController

@property (strong, nonatomic) NSDictionary *confirmationResult;

@property (nonatomic, assign) paymentType type;

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UIImageView *resultImageView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIButton *legalButton;

@property (weak, nonatomic) IBOutlet UIImageView *viamericas_Logo;

@end
