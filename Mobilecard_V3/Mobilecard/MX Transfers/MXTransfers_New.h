//
//  MXTransfers_New.h
//  Mobilecard
//
//  Created by David Poot on 11/16/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol MXTransfersNew_Protocol <NSObject>

@required
- (void) MXTransfersNew_Result;

@end

@interface MXTransfers_New : mT_commonTableViewController

@property (nonatomic, assign) id <NSObject, MXTransfersNew_Protocol> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *aliasText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *holderNameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *holderLastName;
@property (weak, nonatomic) IBOutlet UITextField_Validations *holderSecondLastName;

@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *bankText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *accountTypeText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *clabeText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
