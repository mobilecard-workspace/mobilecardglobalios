//
//  Ingo_Step10.m
//  Mobilecard
//
//  Created by David Poot on 8/18/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "Ingo_Step10.h"

@interface Ingo_Step10 ()
{
    NSDictionary *sessionData;
    
    pootEngine *stateManager;
    pootEngine *enrollCustomerManager;
}
@end

@implementation Ingo_Step10

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializePickersForView:self.navigationController.view];
    
 //   [self addShadowToView:_register_Button];
    
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_lastnameText setUpTextFieldAs:textFieldTypeLastName];
    [_phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [_ssnText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_birthdateText setUpTextFieldAs:textFieldTypeBirthdate];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_cityText setUpTextFieldAs:textFieldTypeCity];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    
    [self addValidationTextField:_emailText];
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_lastnameText];
    [self addValidationTextField:_phoneText];
    [self addValidationTextField:_ssnText];
    [self addValidationTextField:_birthdateText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_zipText];
    [self addValidationTextField:_stateText];
    
    [self addValidationTextFieldsToDelegate];
    
    stateManager = [[pootEngine alloc] init];
    [stateManager setDelegate:self];
    [stateManager setShowComments:developing];
    
    [stateManager startWithoutPostRequestWithURL:WSGetINGOStateCatalog];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    [_emailText setText:userData[kUserEmail]];
    [_nameText setText:userData[kUserName]];
    [_lastnameText setText:userData[kUserLastName]];
    [_phoneText setText:userData[kUserPhone]];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back_Action:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)register_Action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        enrollCustomerManager = [[pootEngine alloc] init];
        [enrollCustomerManager setDelegate:self];
        [enrollCustomerManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setObject:[_addressText text] forKey:@"addressLine1"];
        [params setObject:@"" forKey:@"addressLine2"];
        [params setObject:@"false" forKey:@"allowTexts"];
        [params setObject:[_cityText text] forKey:@"city"];
        [params setObject:@"US" forKey:@"countryOfOrigin"];
        [params setObject:[NSString stringWithFormat:@"%@-%@-%@", [[_birthdateText text] componentsSeparatedByString:@"-"][2], [[_birthdateText text] componentsSeparatedByString:@"-"][1], [[_birthdateText text] componentsSeparatedByString:@"-"][0]] forKey:@"dateOfBirth"];
        [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"deviceId"];
        [params setObject:[_emailText text] forKey:@"email"];
        [params setObject:[_nameText text] forKey:@"firstName"];
        [params setObject:@"" forKey:@"gender"];
        [params setObject:@"" forKey:@"homeNumber"];
        [params setObject:userData[kUserIDKey] forKey:@"idUsuario"];
        [params setObject:[_lastnameText text] forKey:@"lastName"];
        [params setObject:@"" forKey:@"middleInitial"];
        [params setObject:[_phoneText text] forKey:@"mobileNumber"];
        [params setObject:@"IOS" forKey:@"plataforma"];
        [params setObject:[_ssnText text] forKey:@"ssn"];
        [params setObject:[NSString stringWithFormat:@"%@", [_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]]] forKey:@"state"];
        [params setObject:@"" forKey:@"suffix"];
        [params setObject:@"" forKey:@"title"];
        [params setObject:[_zipText text] forKey:@"zip"];
        
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        [enrollCustomerManager startJSONRequestWithURL:WSSetEnrollCustomer withPost:JSONString];
        
        [self lockViewWithMessage:nil];
    }
}

#pragma Mark Manager handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == stateManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue]==0) {
            [_stateText setInfoArray:response[@"estados"]];
            [_stateText setDescriptionLabel:@"nombre"];
            [_stateText setIdLabel:@"id"];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
    
    if (manager == enrollCustomerManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        switch ([response[@"errorCode"] intValue]) {
            case 0:
            {
                sessionData = response;
                
                if ([_delegate conformsToProtocol:@protocol(ingoEnrollmentDelegate)]&&[_delegate respondsToSelector:@selector(ingoEnrollResult:)]) {
                    [_delegate ingoEnrollResult:sessionData];
                };
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
                break;
                
            default:
            {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[@"errorMessage"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
            }
                break;
        }
    }
}

@end
