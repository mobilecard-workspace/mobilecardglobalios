//
//  Profile_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 10/30/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "Profile_Ctrl.h"
#import "NSDictionary (keychain).h"

@interface Profile_Ctrl ()
{
    pootEngine *changeManager;
    pootEngine *resendManager;
    
    NSMutableDictionary *userdata;
    NSMutableArray *yearSet;
    
    NSString *oldPassword;
    NSString *newPassword;
    
    NSString *newIDCountry;
}

@end

@implementation Profile_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:self.navigationController.navigationBar];
    
    [_nameText setDelegate:self];
    [_lastnameText setDelegate:self];
    [_emailText setDelegate:self];
    [_passwordText setDelegate:self];
    [_countryText setDelegate:self];
    [_phoneText setDelegate:self];
    
    userdata = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy"];
    int currentYear = [[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:0]] intValue]+1;
    yearSet = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<11; i++) {
        [yearSet addObject:[NSString stringWithFormat:@"%d", currentYear+i]];
    }
    
    [_editSwitch setEnabled:[self verifyStatus]];
    
    [self updateTableData];
}


- (BOOL)verifyStatus
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue]==99) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
        [alertMsg setTag:99];
        [alertMsg show];
        return NO;
    }
    return YES;
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)updateTableData
{
    NSDictionary *newUserData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    [_nameText setText:newUserData[kUserName]];
    [_lastnameText setText:newUserData[kUserLastName]];
    [_emailText setText:newUserData[kUserEmail]];
    [_emailText setEnabled:NO];
    [_passwordText setText:@"∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙"];
    switch ([newUserData[kUserIdCountry] intValue]) {
        case 1: //MX
            [_countryText setText:NSLocalizedString(@"México", nil)];
            break;
        case 2: //Colombia
            [_countryText setText:NSLocalizedString(@"Colombia", nil)];
            break;
        case 3: //USA
            [_countryText setText:NSLocalizedString(@"EUA", nil)];
            break;
        default:
            break;
    }
    [_phoneText setText:newUserData[kUserPhone]];
}


#pragma Mark UITextField delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField_Validations *)textField
{
    if (![self verifyStatus]) {
        return NO;
    }
    
    if (_editSwitch.isOn) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Empty" message:@"Empty" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles: nil];
        
        NSString *alertTitle;
        NSString *alertMsg;
        NSString *alertButtonTitle;
        
        alertTitle = @"None";
        alertMsg = @"None";
        alertButtonTitle = @"None";
        
        if (textField == _nameText) {
            [_validationText setUpTextFieldAs:textFieldTypeName];
            alertTitle = NSLocalizedString(@"Actualizar", nil);
            alertMsg = NSLocalizedString(@"Escriba su nombre", nil);
            alertButtonTitle = NSLocalizedString(@"Cambiar", nil);
            [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [[alertView textFieldAtIndex:0] setPlaceholder:NSLocalizedString(@"Nombre", nil)];
            
            [alertView setTag:301];
        } else if (textField == _lastnameText) {
            [_validationText setUpTextFieldAs:textFieldTypeName];
            alertTitle = NSLocalizedString(@"Actualizar", nil);
            alertMsg = NSLocalizedString(@"Escriba su apellido", nil);
            alertButtonTitle = NSLocalizedString(@"Cambiar", nil);
            [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [[alertView textFieldAtIndex:1] setPlaceholder:NSLocalizedString(@"Apellido", nil)];
            
            [alertView setTag:302];
        } else if (textField == _emailText) {
            return NO;
        } else if (textField == _passwordText) {
            [_validationText setUpTextFieldAs:textFieldTypePassword];
            alertTitle = NSLocalizedString(@"Actualizar", nil);
            alertMsg = NSLocalizedString(@"Escriba su contraseña actual", nil);
            alertButtonTitle = NSLocalizedString(@"Cambiar", nil);
            [alertView setAlertViewStyle:UIAlertViewStyleSecureTextInput];
            
            [alertView setTag:303];
        } else if (textField == _countryText) {
            [_validationText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
            alertTitle = NSLocalizedString(@"Actualizar", nil);
            alertMsg = NSLocalizedString(@"Seleccione un país", nil);
            [alertView addButtonWithTitle:NSLocalizedString(@"México", nil)];
            [alertView addButtonWithTitle:NSLocalizedString(@"Colombia", nil)];
            alertButtonTitle = NSLocalizedString(@"EUA", nil);
            
            [alertView setTag:304];
        } else if (textField == _phoneText) {
            [_validationText setUpTextFieldAs:textFieldTypeCellphone];
            alertTitle = NSLocalizedString(@"Actualizar", nil);
            alertMsg = NSLocalizedString(@"Escriba un nuevo número telefónico", nil);
            alertButtonTitle = NSLocalizedString(@"Cambiar", nil);
            [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [alertView setTag:305];
        }
        
        [alertView setTitle:alertTitle];
        [alertView setMessage:alertMsg];
        [alertView addButtonWithTitle:alertButtonTitle];
        
        [alertView show];
        
        return NO;
    } else {
        return NO;
    }
}

- (NSString*) convertToURLCompatible: (NSString*)stringy
{
    NSString * escapedUrlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)stringy,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]ÁáÉéÍíÓóÚúñ. %",kCFStringEncodingUTF8));
    return escapedUrlString;
}


//UIAlertView delegation
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 99:
        {
            switch (buttonIndex) {
                case 0:
                    break;
                case 1:
                {
                    resendManager = [[pootEngine alloc] init];
                    [resendManager setDelegate:self];
                    [resendManager setShowComments:developing];
                    
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    
                    [resendManager startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/", userData[kUserIDKey], @"/activate"]];
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
        {
            if (!(buttonIndex == 0)) {
                _validationText.text = [alertView textFieldAtIndex:0].text;
                if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                    changeManager = [[pootEngine alloc] init];
                    [changeManager setDelegate:self];
                    [changeManager setShowComments:developing];
                    [changeManager setTag:alertView.tag];
                    
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    NSString *WSLocation;
                    
                    switch (alertView.tag) {
                        case 300: // NOT NEEDED!
                            break;
                        case 301: //FIRST NAME
                        {
                            
                            [params setObject:_validationText.text forKey:@"value"];
                            WSLocation = @"usr_nombre";
                        }
                            break;
                        case 302: //LAST NAME
                        {
                            [params setObject:_validationText.text forKey:@"value"];
                            
                            WSLocation = @"usr_apellido";
                        }
                            break;
                        case 303: //Password
                        {
                            oldPassword = _validationText.text;
                            [_validationText setUpTextFieldAs:textFieldTypePassword];
                            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Actualizar", nil) message:NSLocalizedString(@"Escriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar", nil), nil];
                            [alertMsg setTag:3031];
                            [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                            [alertMsg show];
                            return;
                        }
                            break;
                        case 3031: //new password request
                        {
                            newPassword = _validationText.text;
                            [_validationText setUpTextFieldAs:textFieldTypePassword];
                            if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                                [params setObject:[changeManager encryptJSONString:oldPassword withPassword:nil]  forKey:@"oldPass"];
                                [params setObject:[changeManager encryptJSONString:[alertView textFieldAtIndex:0].text withPassword:nil] forKey:@"newPass"];
                                
                                NSString *post = nil;
                                post = [NSString stringWithFormat:@"old=%@&new=%@", [self convertToURLCompatible:[changeManager encryptJSONString:oldPassword withPassword:nil]], [self convertToURLCompatible:[changeManager encryptJSONString:[alertView textFieldAtIndex:0].text withPassword:nil]]];
                                
                                [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@", userManagementURL,NSLocalizedString(@"lang", nil),@"/",[userdata objectForKey:kUserIDKey],@"/password/update"] withPost:post];
                                
                                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                                return;
                            } else {
                                return;
                            }
                            
                        }
                            break;
                        case 304:
                        {
                            [params setObject:[NSString stringWithFormat:@"%ld", (long)buttonIndex] forKey:@"value"];
                            newIDCountry = [NSString stringWithFormat:@"%ld", (long)buttonIndex];
                            
                            WSLocation = @"idpais";
                        }
                            break;
                        case 305:
                        {
                            [params setObject:_validationText.text forKey:@"value"];
                            WSLocation = @"usr_telefono";
                        }
                            break;
                            /*
                             case 306:
                             {
                             newCard = _validationText.text;
                             [_validationText setUpTextFieldAs:textFieldTypeGeneralNoRequired];
                             UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"Selecciona:" message:@"Seleccione el mes de vencimiento de la tarjeta" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles: nil];
                             for (int i = 1; i<13; i++) {
                             [alertMsg addButtonWithTitle:[NSString stringWithFormat:@"%02d", i]];
                             }
                             [alertMsg setTag:3061];
                             [alertMsg show];
                             return;
                             }
                             break;
                             case 3061:
                             {
                             newMonth = [NSString stringWithFormat:@"%02ld", buttonIndex];
                             UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"Selecciona:" message:@"Seleccione el año de vencimiento de la tarjeta" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles: nil];
                             
                             for (int i = 0; i<[yearSet count]; i++) {
                             [alertMsg addButtonWithTitle:yearSet[i]];
                             }
                             [alertMsg setTag:3062];
                             [alertMsg show];
                             return;
                             }
                             break;
                             case 3062:
                             {
                             newYear = yearSet[buttonIndex-1];
                             UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"Confirmación de datos" message:[NSString stringWithFormat:@"Compruebe que la información de la tarjeta sea correcta\n\nNúmero de tarjeta:%@\nVencimiento:%@/%@", newCard, newMonth, newYear] delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"OK", nil];
                             [alertMsg setTag:3063];
                             [alertMsg show];
                             return;
                             }
                             break;
                             case 3063:
                             {
                             params = [[NSMutableDictionary alloc] initWithDictionary:userdata];
                             [params setObject:[changeManager encryptJSONString:newCard withPassword:nil] forKey:kCardNumber];
                             [params setObject:[NSString stringWithFormat:@"%@/%@", newMonth, [newYear substringWithRange:NSMakeRange(2, 2)]] forKey:kCardValid];
                             WSLocation = WSUpdateCard;
                             }
                             break;
                             */
                        default:
                            break;
                    }
                    
                    NSString *post = nil;
                    post = [NSString stringWithFormat:@"value=%@", params[@"value"]];
                    
                    [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@%@%@%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/", userdata[kUserIDKey], @"/", WSLocation, @"/update"] withPost:post];
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                }
            }
        }
            break;
    }
}


//pootEngine delegation

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    
    if (changeManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue] == 0) {
            switch (manager.tag) {
                case 301:
                    [userdata setObject:response[@"value"] forKey:(NSString*)kUserName];
                    break;
                case 302:
                    [userdata setObject:response[@"value"] forKey:(NSString*)kUserLastName];
                    break;
                case 3031: //Password
                    [[NSUserDefaults standardUserDefaults] setObject:newPassword forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    break;
                case 304:
                    [userdata setObject:response[@"value"] forKey:kUserIdCountry];
                    break;
                case 305:
                    [userdata setObject:response[@"value"] forKey:kUserPhone];
                    break;
                default:
                    break;
            }
            [[NSUserDefaults standardUserDefaults] setObject:userdata forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [userdata deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
            [userdata storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
            
            [self updateTableData];
            
            
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == resendManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
    }
}

- (void)managerFailed:(pootEngine *)manager withKey:(NSString *)key errorCode:(pootEngineErrorCode)error
{
    [self unLockView];
    
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Su solicitud no fue procesada, intente más tarde...", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertMsg show];
}

@end
