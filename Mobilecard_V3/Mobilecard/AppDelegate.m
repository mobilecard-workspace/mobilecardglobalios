//
//  AppDelegate.m
//  Mobilecard
//
//  Created by David Poot on 10/2/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "AppDelegate.h"
#import <IngoSDK/IngoSDK.h>
#import "iovation.h"
#import "Secure3D_Ctrl.h"
#import "LGSideMenuController.h"
#import "mT_commonTableViewController.h"
#import <IQKeyboardManager.h>


#define mainNavController (((AppDelegate*)[[UIApplication sharedApplication] delegate]).navController)

@interface AppDelegate ()
{
    pootEngine *loginManager;
    UIAlertView *alertMsg;
    
    NSDictionary *urlData;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    
    self.canRunIngoSdk = [IngoSdkManager initIngoSdk:[iovation ioBegin]];
    developing?NSLog(@"canRunIngoSdk: %i", self.canRunIngoSdk):nil;
    
[[IQKeyboardManager sharedManager] setEnable:YES];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}



- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    /////////////////////////////////////// Implementacion de verifi incompleta Raul mendez  ///////////////////*nota: esta seccion esta validada para cuando el idUsrStatus sea 100 no envia automaticamente el sms cada que ingresa a la aplicacion *esta incompleto el metodo pero ya no truena
    if ([self verifyStatus]) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey] && [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] && ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue]==99 || [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue]==100)) {
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]) {
                loginManager = [[pootEngine alloc] init];
                [loginManager setDelegate:self];
                [loginManager setShowComments:developing];
                
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userDetails[kUserEmail] forKey:@"usrLoginOrEmail"];
                [params setObject:[loginManager encryptJSONString:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword] withPassword:nil] forKey:kUserPassword];
                
                [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
                [params setObject:@"Apple" forKey:@"manufacturer"];
                [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
                [params setObject:@"iOS" forKey:@"platform"];
                [params setObject:@"USUARIO" forKey:@"tipoUsuario"];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
                
                
                alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Procesando solicitud...", nil) message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                [alertMsg show];
            } else {
                mT_commonTableViewController *helper = [[mT_commonTableViewController alloc] init];
                
                loginManager = [[pootEngine alloc] init];
                [loginManager setDelegate:self];
                [loginManager setShowComments:developing];
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userDetails[@"email"] forKey:@"usrLoginOrEmail"];
                [params setObject:[helper encryptString:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword]] forKey:kUserPassword];
                //MUST HAVE PARAMETERS
                [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
                [params setObject:@"Apple" forKey:@"manufacturer"];
                [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
                [params setObject:@"iOS" forKey:@"platform"];
                [params setObject:@"NEGOCIO" forKey:@"tipoUsuario"];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
                
                alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Procesando solicitud...", nil) message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                [alertMsg show];
            }
            
        }
    }
}
- (BOOL)verifyStatus
{
    if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 99)) {
        //            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Reenviar enlace", nil), nil];
        //            [alertMsg setTag:99];
        //            [alertMsg show];
        // [[self window] addSubview:alertMsg];
        return NO;
    }
    
    if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 100)) {
        //            UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        //            topWindow.rootViewController = [UIViewController new];
        //            topWindow.windowLevel = UIWindowLevelAlert + 1;
        //
        //            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
        //
        //            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //                UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
        //                UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
        //                Register_Step30 *nextView = [[nav viewControllers] firstObject];
        //
        //                [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
        //                [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserIDKey]];
        //
        //                [topWindow.rootViewController presentViewController:nav animated:YES completion:nil];
        //               // [[self window] addSubview:nav.view];
        //               // [self addChildViewController:vc];
        //                [alertMsg dismissViewControllerAnimated:YES completion:nil];
        //            }];
        //
        //            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        //            }];
        //
        //            [alertMsg addAction:ok];
        //            [alertMsg addAction:cancel];
        
        //  [topWindow makeKeyAndVisible];
        //  [topWindow.rootViewController presentViewController:alertMsg animated:YES completion:nil];
        //) [[self window] addSubview:alertMsg];
        // [self presentViewController:alertMsg animated:YES completion:nil];
        
        return NO;
    }
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    
    
    return YES;
}



- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [alertMsg dismissWithClickedButtonIndex:0 animated:YES];
    
    NSDictionary *response = (NSDictionary*)json;
    response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
    
    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
}


- (NSMutableDictionary*)cleanDictionary:(NSMutableDictionary*)respons
{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:respons options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    
    NSString *final = [myString stringByReplacingOccurrencesOfString:@",null" withString:@""];
    
    NSData *data =[final dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *response;
    if(data!=nil){
        response = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
    }
    
    for (id key in [response allKeys]) {
        if ([[response valueForKey:key] isKindOfClass:[NSDictionary class]]) {
            for (id key2 in [[response valueForKey:key] allKeys]) {
                if ([[[response valueForKey:key] valueForKey:key2] isKindOfClass:[NSNull class]]) {
                    [[response valueForKey:key] setObject:@"" forKey:key2];
                }
            }
        } else {
            if ([[response valueForKey:key] isKindOfClass:[NSNull class]]) {
                [response setObject:@"" forKey:key];
            }
            if ([[response valueForKey:key] isKindOfClass:[NSArray class]]) {
                for (int i = 0; i<[[response valueForKey:key] count]; i++) {
                    [[response valueForKey:key] replaceObjectAtIndex:i withObject:[self cleanDictionary:[[response valueForKey:key] objectAtIndex:i]]];
                }
            }
        }
    }
    
    return response;
}

@end
