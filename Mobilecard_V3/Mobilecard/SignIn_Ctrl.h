//
//  SignIn_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 10/25/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Register_Step1.h"

@interface SignIn_Ctrl : mT_commonTableViewController <UITableViewDelegate, registerProtocol>

@property (weak, nonatomic) IBOutlet UITextField_Validations *usernameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *validationText;
@property (weak, nonatomic) IBOutlet UIButton *SignIn_Button;
@end
