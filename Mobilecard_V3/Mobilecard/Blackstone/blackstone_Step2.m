//
//  blackstone_Step2.m
//  Mobilecard
//
//  Created by David Poot on 7/12/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "blackstone_Step2.h"
#import "blackstone_result.h"


@interface blackstone_Step2 ()
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    
    pootEngine *paymentManager;
    pootEngine *cardManager;
    NSNumberFormatter *numFormatter;
    
    NSDictionary *payResult;
    float fee;
}
@end

@implementation blackstone_Step2

- (void)viewDidLoad {
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    
    [super viewDidLoad];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [self addShadowToView:_payButton];
    
    [_numberText setUpTextFieldAs:textFieldTypeCellphone];
    [_numberConfirmText setUpTextFieldAs:textFieldTypeCellphone];
    [_amountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    
    [self addValidationTextField:_numberText];
    [self addValidationTextField:_numberConfirmText];
    [self addValidationTextField:_amountText];
    
    [self addValidationTextFieldsToDelegate];
    
    [_feeLabel setText:@""];
    
    NSArray *fav = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey];
    for (NSDictionary *element in fav) {
        if ([[element objectForKey:@"dataSource"][@"code"] intValue] == [_dataSource[@"code"] intValue]) {
            _favoriteDataSource = element;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [self updateFavorite_Button];
    
    [super viewDidAppear:animated];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pay_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    if (![[_numberText text] isEqualToString:[_numberConfirmText text]]) {
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"El número y la confirmación del número no coinciden, intenta de nuevo", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        
        return;
    }
    
    [self performSegueWithIdentifier:@"summary" sender:nil];
}

- (IBAction)showContactList:(id)sender {
    CNContactPickerViewController *contactPicker = [[CNContactPickerViewController alloc] init];
    contactPicker.delegate = self;
    
    NSArray *arrKeys = @[CNContactPhoneNumbersKey]; //display only phone numbers
    contactPicker.displayedPropertyKeys = arrKeys;
    
    [self presentViewController:contactPicker animated:YES completion:nil];
}

- (BOOL)textField:(UITextField_Validations *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _amountText) {
        [self updateFeeLabelWithTotal:[[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue]];
    }
    return [self subTextField:textField shouldChangeCharactersInRange:range replacementString:string];
}

- (void) updateFeeLabelWithTotal:(float)total
{
    fee = total*0.1;
    [_feeLabel setText:[NSString stringWithFormat:NSLocalizedString(@"Comisión %@", nil), [numFormatter stringFromNumber:[NSNumber numberWithFloat:fee]]]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return [NSString stringWithFormat:@"%@ - %@", _dataSource[@"carrierName"], _dataSource[@"name"]];
    } else {
        return @"";
    }
}

#pragma Mark pootEngine

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
    }
    
    if (manager == paymentManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if (response) {
            
            payResult = [NSDictionary dictionaryWithDictionary:response];
            [self performSegueWithIdentifier:@"result" sender:nil];
        }
    }
}

#pragma Mark segue handler

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"summary"]) {
        UINavigationController *nav = [segue destinationViewController];
        
        blackstone_Summary *nextView = [nav.viewControllers firstObject];
        [nextView setDelegate:self];
        
        [nextView setPhoneNumberString:[_numberText text]];
        [nextView setAmountString:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[[_amountText text] floatValue]]]];
        [nextView setFeeString:[numFormatter stringFromNumber:[NSNumber numberWithFloat:fee]]];
        [nextView setTotalString:[numFormatter stringFromNumber:[NSNumber numberWithFloat:fee+[[_amountText text] floatValue]]]];
    }
    
    if ([[segue identifier] isEqualToString:@"result"]) {
        blackstone_result *nextView = [segue destinationViewController];
        
        [nextView setResultData:payResult];
    }
}

#pragma Mark Summary Delegate

- (void)summaryResult:(NSDictionary *)result
{
    [self showCardOptions];
}

#pragma Mark Wallet delegate
- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view setType:viewType];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void) showCardOptions
{
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    paymentManager = [[pootEngine alloc] init];
    [paymentManager setDelegate:self];
    [paymentManager setShowComments:developing];
    
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[_amountText text] forKey:@"amount"];
    [params setObject:_dataSource[@"carrierName"] forKey:@"carrierName"];
    [params setObject:@"USA" forKey:@"countryCode"];
    [params setObject:selectedCard[@"idTarjeta"] forKey:@"idTarjeta"];
    [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
    [params setObject:_dataSource[@"code"] forKey:@"mainCode"];
    [params setObject:[_numberText text] forKey:@"phoneNumber"];
    
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"lat"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"lon"];

    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [paymentManager startJSONRequestWithURL:WSDoPayment withPost:JSONString];
    
    [self lockViewWithMessage:nil];
}

#pragma Mark CNContactPicker Handler (Address book)

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty{
    
    CNPhoneNumber *phoneNumber = contactProperty.value;
    
    developing?NSLog(@"PHONE -> %@", [phoneNumber stringValue]):nil;
    
    @try {
        NSString *phone = [phoneNumber stringValue];
        phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
        phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
        phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
        phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
        phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([phone length]>10) {
            [_numberText setText:[phone substringWithRange:NSMakeRange([phone length] - 10, 10)]];
            [_numberConfirmText setText:[phone substringWithRange:NSMakeRange([phone length] - 10, 10)]];
        } else {
            [_numberText setText:phone];
            [_numberConfirmText setText:phone];
        }
        
    } @catch (NSException *exception) {
        nil;
    } @finally {
        nil;
    }
}

#pragma Mark saving favorite action

- (void)updateFavorite_Button {
    if (_favoriteDataSource) {
        [_favoriteButton setImage:[UIImage imageNamed:@"icon_fav_orange"] forState:UIControlStateNormal];
        [_favoriteLabel setText:NSLocalizedString(@"ELIMINAR FAVORITO", nil)];
    } else {
        [_favoriteButton setImage:[UIImage imageNamed:@"icon_fav_gray"] forState:UIControlStateNormal];
        [_favoriteLabel setText:NSLocalizedString(@"GUARDAR COMO FAVORITO", nil)];
    }
}

- (void)saveViewData
{
    NSMutableArray *favoritesArray;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]) {
        favoritesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]];
    } else {
        favoritesArray = [[NSMutableArray alloc] init];
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:_dataSource forKey:@"dataSource"];
    [params setObject:@"" forKey:@"amountDataSource"];
    [params setObject:[NSString stringWithFormat:@"%d", favoriteTopUpUSA] forKey:@"type"]; // Recarga Blackstone
    
    [favoritesArray addObject:params];
    
    [[NSUserDefaults standardUserDefaults] setObject:favoritesArray forKey:(NSString*)kUserFavoritesKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _favoriteDataSource = params;
}

- (IBAction)favoriteButton_Action:(id)sender {
    if (_favoriteDataSource) {
        NSMutableArray *favoritesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]];
        
        [favoritesArray removeObject:_favoriteDataSource];
        _favoriteDataSource = nil;
        
        [[NSUserDefaults standardUserDefaults] setObject:favoritesArray forKey:(NSString*)kUserFavoritesKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [self saveViewData];
    }
    
    [self updateFavorite_Button];
}
































#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Failed to Get Your Location" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}
@end
