//
//  blackstone_Step2.h
//  Mobilecard
//
//  Created by David Poot on 7/12/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import <ContactsUI/ContactsUI.h>
#import "blackstone_Summary.h"
#import "Wallet_Ctrl.h"
#import <CoreLocation/CoreLocation.h>

@interface blackstone_Step2 : mT_commonTableViewController <CNContactPickerDelegate, blackstoneSummaryDelegate, cardSelectionDelegate, updateCardDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) NSDictionary *dataSource;
@property (strong, nonatomic) NSDictionary *favoriteDataSource;

@property (weak, nonatomic) IBOutlet UITextField_Validations *numberText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *numberConfirmText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *amountText;

@property (weak, nonatomic) IBOutlet UILabel *favoriteLabel;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *contactListButton;
@property (weak, nonatomic) IBOutlet UILabel *feeLabel;
@property (weak, nonatomic) IBOutlet UIButton *payButton;

@end
