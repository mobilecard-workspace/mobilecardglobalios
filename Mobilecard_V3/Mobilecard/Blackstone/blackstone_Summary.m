//
//  blackstone_Summary.m
//  Mobilecard
//
//  Created by David Poot on 7/15/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "blackstone_Summary.h"

@interface blackstone_Summary ()

@end

@implementation blackstone_Summary

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:_summaryButton];
    
    [self updateData];
}

- (void) updateData
{
    [_phoneNumberText setText:_phoneNumberString];
    [_amountText setText:_amountString];
    [_feeText setText:_feeString];
    [_totalText setText:_totalString];
    
    [_phoneNumberText setFloatingLabelTextColor:[UIColor darkMCOrangeColor]];
    [_amountText setFloatingLabelTextColor:[UIColor darkMCOrangeColor]];
    [_feeText setFloatingLabelTextColor:[UIColor darkMCOrangeColor]];
    [_totalText setFloatingLabelTextColor:[UIColor darkMCOrangeColor]];
    
    [_phoneNumberText setEnabled:NO];
    [_amountText setEnabled:NO];
    [_feeText setEnabled:NO];
    [_totalText setEnabled:NO];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pay_Action:(id)sender {
    if ([_delegate conformsToProtocol:@protocol(blackstoneSummaryDelegate)]&&[_delegate respondsToSelector:@selector(summaryResult:)]) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            [_delegate summaryResult:nil];
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

@end
