//
//  orderResult_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 4/28/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "orderResult_Ctrl.h"

@interface orderResult_Ctrl ()
{
    UIBarButtonItem *shareButton;
}

@end

@implementation orderResult_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    switch (_type) {
        case paymentTypeViamericas:
        {
            [_resultLabel setText:NSLocalizedString(@"¡Transacción exitosa!", nil)];
            
            switch ([_confirmationResult[@"responseCode"] intValue]) {
                case 0: //EXITO
                {
                    [_resultImageView setImage:[UIImage imageNamed:@"icon_success"]];
                    
                    [_descriptionTextView setEditable:YES];
                    [_descriptionTextView setText:[NSString stringWithFormat:NSLocalizedString(@"\nCódigo de aprobación: %@\nTarjeta: %@\nFecha: %@\nMonto enviado: %@$%@\nMonto enviado: %@$%@", nil),
                                                   _confirmationResult[@"approvalCode"],
                                                   _confirmationResult[@"cardNumber"],
                                                   _confirmationResult[@"transactionDate"],
                                                   _confirmationResult[@"objReceipt"][@"currencySrc"],
                                                   _confirmationResult[@"objReceipt"][@"totalReceiver"],
                                                   _confirmationResult[@"objReceipt"][@"currencyPayer"],
                                                   _confirmationResult[@"objReceipt"][@"totalPayReceiver"]]];
                    [_descriptionTextView setEditable:NO];
                    [_descriptionTextView setSelectable:YES];
                    
                    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Terminar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(backButton_Action:)];
                    [self.navigationItem setLeftBarButtonItem:backButton];
                    
                    shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share:)];
                    self.navigationItem.rightBarButtonItem = shareButton;
                }
                    break;
                    
                default:
                    [_resultLabel setText:@""];
                    
                    [_resultImageView setImage:[UIImage imageNamed:@"icon_error"]];
                    [_descriptionTextView setEditable:YES];
                    [_descriptionTextView setText:_confirmationResult[@"responseMessage"]];
                    [_descriptionTextView setEditable:NO];
                    
                    [_legalButton setHidden:YES];
                    break;
            }
        }
            break;
            
        case paymentTypeACI:
        {
            [_viamericas_Logo setHidden:YES];
            switch ([_confirmationResult[@"idError"] intValue]) {
                case 0: //EXITO
                {
                    [_resultLabel setText:NSLocalizedString(@"¡Transacción exitosa!", nil)];
                    
                    [_resultImageView setImage:[UIImage imageNamed:@"icon_success"]];
                    
                    [_descriptionTextView setEditable:YES];
                    NSString *descText = [NSString stringWithFormat:@"%@", _confirmationResult[kErrorMessage]];
                    descText = [descText stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
                    [_descriptionTextView setText:descText];
                    [_descriptionTextView setEditable:NO];
                    [_descriptionTextView setSelectable:YES];
                    
                    [_legalButton setHidden:YES];
                    
                    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Terminar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(backButton_Action:)];
                    [self.navigationItem setLeftBarButtonItem:backButton];
                }
                    break;
                    
                default:
                    [_resultLabel setText:NSLocalizedString(@"Error", nil)];
                    
                    [_resultImageView setImage:[UIImage imageNamed:@"icon_error"]];
                    [_descriptionTextView setEditable:YES];
                    [_descriptionTextView setText:_confirmationResult[kErrorMessage]];
                    [_descriptionTextView setEditable:NO];
                    
                    [_legalButton setHidden:YES];
                    break;
            }
        }
            break;
    }
}

- (void) share:(id)sender
{
    NSString *theMessage = [NSString stringWithFormat:NSLocalizedString(@"%@ te ha enviado dinero desde Mobilecard. Haz clic en el enlace para obtener tu recibo:\n\n%@", nil), [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName], _confirmationResult[@"url"]];
    NSArray *items = @[theMessage];
    
    UIActivityViewController *sharingController = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    [self presentActivityController:sharingController];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButton_Action:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)legalButton_Action:(id)sender {
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:@"%@", _confirmationResult[@"objReceipt"][@"receiptDisclaimer"]] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertMsg show];
}

- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    //controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            
            // user shared an item
            //NSLog(@"We used activity type%@", activityType);
            
        } else {
            
            // user cancelled
            //NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            //NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

@end
