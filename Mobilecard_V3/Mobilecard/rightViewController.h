//
//  rightViewController.h
//  Mobilecard
//
//  Created by David Poot on 11/5/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonController.h"
#import "UIViewController+LGSideMenuController.h"

@interface rightViewController : mT_commonController <UITableViewDelegate, LGSideMenuDelegate>

@property (weak, nonatomic) IBOutlet UILabel *profileName;

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end
