//
//  rightViewController.m
//  Mobilecard
//
//  Created by David Poot on 11/5/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "rightViewController.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "MainMenu_Ctrl.h"
#import "NSDictionary (keychain).h"

#define cellHeight @"40"

@interface rightViewController ()
{
    NSMutableArray *menuItems;
}
@end

@implementation rightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuItems = [[NSMutableArray alloc] init];
    
    [_profileName setText:[NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName], [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLastName]]];
    
    [_versionLabel setText:[NSString stringWithFormat:NSLocalizedString(@"VERSIÓN: %@", nil), [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]]];
    
    [self updateMenu];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.sideMenuController setDelegate:self];
    
    [super viewWillAppear:YES];
}

- (void)updateMenu
{
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    [menuItems removeAllObjects];
    switch ([userData[kUserIdCountry] intValue]) {
        case 1: //MEXICO
        {
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_home", kIdentifierKey, cellHeight, kHeightKey,  nil]];
             [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_MXtransfers", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, cellHeight, kHeightKey,  nil]];
             [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_ScanNPay", kIdentifierKey, cellHeight, kHeightKey, nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_wallet", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_favorites", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_Statement", kIdentifierKey, cellHeight, kHeightKey,  nil]];
           
        }
            break;
        case 2: //COLOMBIA
        {
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_home", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, cellHeight, kHeightKey,  nil]];
        }
            break;
        case 3: //USA
        {
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_home", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            if ([[NSUserDefaults standardUserDefaults] boolForKey:(NSString*)kUSALocation]) {
                [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_cash", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            }
            //[menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_transfers", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_billpay", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_topup", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_wallet", kIdentifierKey, cellHeight, kHeightKey,  nil]];
            [menuItems addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"side_Statement", kIdentifierKey, cellHeight, kHeightKey,  nil]];
        }
        default:
            break;
    }
    
    [_menuTableView reloadData];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Menu",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}




- (void)willShowRightView:(UIView *)rightView sideMenuController:(LGSideMenuController *)sideMenuController
{
    [self updateMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (IBAction)profile_Action:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"profile" sender:self];
    
    [mainViewController hideRightViewAnimated:YES completionHandler:nil];
}

- (IBAction)contact_Action:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"side_contact" sender:self];
    
    [mainViewController hideRightViewAnimated:YES completionHandler:nil];
}

- (IBAction)logout_Action:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *logout = [[NSDictionary alloc] init];
    [logout deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    [nav dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)commerce_ChangePass:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"side_passchange" sender:self];
    
    [mainViewController hideRightViewAnimated:YES completionHandler:nil];
}

- (IBAction)commerce_Statement:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    [nextView performSegueWithIdentifier:@"side_statement_commerce" sender:self];
    
    [mainViewController hideRightViewAnimated:YES completionHandler:nil];
}


#pragma mark UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *label = cell.contentView.subviews[0];
    [label setText:NSLocalizedString(menuItems[indexPath.row][kIdentifierKey], nil)];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:menuItems[0][kIdentifierKey]]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        
        [nav popToRootViewControllerAnimated:YES];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_cash"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startCash];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_wallet"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView openWallet_Action:self];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_transfers"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startTransfersModule];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_MXtransfers"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startMXTransfersModule];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_Statement"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startStatement];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_ScanNPay"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startScanNPay];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_billpay"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startBillPay];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_topup"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startTopUp];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    ////////////////////////////  Codigo reemplazado para el nuevo look and feel   Raul Mendez /////////////////////////////////////
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_favorites"]) {
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        
        UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
        MainMenu_Ctrl *nextView = [nav childViewControllers].firstObject;
        
        [nav popToRootViewControllerAnimated:YES];
        [nextView startFavorites];
        
        [mainViewController hideRightViewAnimated:YES completionHandler:nil];
        return;
    }
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    
    UINavigationController *nav = (UINavigationController *)mainViewController.rootViewController;
    UIViewController *nextView = [nav childViewControllers].firstObject;
    
    [nav popToRootViewControllerAnimated:YES];
    if ([nextView shouldPerformSegueWithIdentifier:menuItems[indexPath.row][kIdentifierKey] sender:self]) {
        [nextView performSegueWithIdentifier:menuItems[indexPath.row][kIdentifierKey] sender:self];
    }
    
    [mainViewController hideRightViewAnimated:YES completionHandler:nil];
}

@end
