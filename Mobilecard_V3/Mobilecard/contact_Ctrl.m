//
//  contact_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 11/7/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "contact_Ctrl.h"

@interface contact_Ctrl ()
{
    NSMutableArray *menuItems;
}

@end

@implementation contact_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuItems = [[NSMutableArray alloc] init];
    
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"60", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"callus", kIdentifierKey, @"60", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"20", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"sendemail", kIdentifierKey, @"60", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"20", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"webpage", kIdentifierKey, @"60", kHeightKey, nil]];
    [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"20", kHeightKey, nil]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma Mark UITableView delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuItems[indexPath.row][kIdentifierKey]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"callus"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://018009255001"]];
    }
    @try {
        if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"sendemail"]) {
            NSString *emailTitle = @"Contacto Addcel - iOS";
            NSString *messageBody = @"";
            NSArray *toRecipents = [NSArray arrayWithObject:@"soporte@addcel.com"];
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            
            // Present mail view controller on screen
            [self presentViewController:mc animated:YES completion:NULL];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    ////////////////////  correccion url web   Raul Mendez ///////////////////////////
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"webpage"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.mobilecard.mx/"]];
    }
}

- (IBAction)twitter_Action:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.twitter.com/mobilecardmx"]];
}

- (IBAction)facebook_Action:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/mobilecardmx/"]];
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
