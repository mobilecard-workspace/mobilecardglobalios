//
//  billpayment_Selection.m
//  Mobilecard
//
//  Created by David Poot on 10/29/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "billpayment_Selection.h"
#import "InsetCell.h"
#import "shadowed_InsetCell.h"
#import "payment_recarga_ctrl_Toll.h"
#import "payment_recarga_ctrl_Tae.h"
#import "payServices_Ctrl.h"

@interface billpayment_Selection ()
{
    pootEngine *categoriesManager;
    pootEngine *servicesManager;
    pootEngine *amountManager;
    
    NSMutableArray *categoriesInfo;
    NSMutableArray *categoriesSubmenuInfo;
    NSMutableArray *masterCategoriesSubmenuInfo;
    
    UIButton *buttonSender;
    NSMutableArray *ArrayToSend;
    
    NSDictionary *countryData;
    
    int selectedItem;
}
@end

@implementation billpayment_Selection

- (void)viewDidLoad {
    [super viewDidLoad];
    
    buttonSender = [[UIButton alloc] init];
    
   // [self addShadowToView:self.navigationController.navigationBar];
    
    categoriesInfo = [[NSMutableArray alloc] init];
    categoriesSubmenuInfo = [[NSMutableArray alloc] init];
    
    selectedItem = 0;
    [self categoryChange_Action:nil];
//    switch (_type) {
//        case viewTypeRecargas:
//        {
//            categoriesManager = [[pootEngine alloc] init];
//            [categoriesManager setDelegate:self];
//            [categoriesManager setShowComments:developing];
//
//            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
//
//            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//            [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
//            [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
//            [params setObject:userData[kUserIdCountry] forKey:kUserIdCountry];
//
//            NSError *JSONError;
//            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
//            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
//
//            [categoriesManager startJSONRequestWithURL:WSGetCatalogRecargas withPost:JSONString];
//
//            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
//        }
//            break;
//        case viewTypeServicios:
//        {
//            categoriesManager = [[pootEngine alloc] init];
//            [categoriesManager setDelegate:self];
//            [categoriesManager setShowComments:developing];
//
//            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//
//            [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
//            [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
//
//            NSError *JSONError;
//            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
//            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
//
//            [categoriesManager startJSONRequestWithURL:WSGetCatalogServicioCategorias withPost:JSONString];
//
//
//            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
//        }
//            break;
//    }
}

//COLLECTION HANDLING
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [categoriesInfo count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
   // [self addShadowToView:cell];
    
    UILabel *title = cell.contentView.subviews[0];
    [title setText:categoriesInfo[indexPath.row][kDescriptionKey]];
    
    if (selectedItem == indexPath.row) {
        [cell setBackgroundColor:[UIColor darkMCGreyColor]];
        [title setTextColor:[UIColor whiteColor]];
    } else {
        [cell setBackgroundColor:[UIColor whiteColor]];
        [title setTextColor:[UIColor darkMCGreyColor]];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedItem = (int)indexPath.row;
    
  
    
    [_serviceCollection reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_type == viewTypeRecargas) {
        return CGSizeMake((collectionView.frame.size.width-60)/2, 70);
    } else {
        return CGSizeMake(120, 70);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    
    
    switch (_type) {
        case viewTypeRecargas:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case viewTypeServicios:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
    }
}

- (void) initializeView
{
    [_serviceCollection reloadData];
    
    [self categoryChange_Action:nil];
}

#pragma Mark UISegmentedControl implementation

- (IBAction)categoryChange_Action:(id)sender {
    servicesManager = [[pootEngine alloc] init];
    [servicesManager setDelegate:self];
    [servicesManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    
    switch (_type) {
        case viewTypeRecargas:
        {
            categoriesInfo =_Categories;
            //Recarga Teléfono & Toll
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
            [params setObject:categoriesInfo[_id_Categoria][kIDKey] forKey:@"idRecarga"];
            [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
            [params setObject:userData[kUserIdCountry] forKey:kUserIdCountry];
            
            NSError *JSONError;
            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            
            [servicesManager startJSONRequestWithURL:WSGetCatalogRecargaServicios withPost:JSONString];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        }
            break;
            
        case viewTypeServicios:
        {
            categoriesInfo =_Categories;
            [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
            [params setObject:categoriesInfo[_id_Categoria][kIDKey] forKey:@"idCategoria"];
            [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
            
            NSError *JSONError;
            NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
            NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
            
            [servicesManager startJSONRequestWithURL:WSGetCatalogServicios withPost:JSONString];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        }
            break;
    }
    
}

#pragma Mark UITableView implementation
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [categoriesSubmenuInfo count]*2+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row%2) {
        return 70;
    } else {
        return 20;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row%2) {
        shadowed_InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"generic"];
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
                [label setText:categoriesSubmenuInfo[indexPath.row/2][_type==viewTypeRecargas?kDescriptionKey:@"empresa"]];
            }
            
            if ([element isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView = element;
                imageView.tag == 1?[imageView setImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_blanco",categoriesSubmenuInfo[indexPath.row/2][_type==viewTypeRecargas?kDescriptionKey:@"empresa"]]]]:nil;
            }
        }
       
        
        return cell;
    } else {
        InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"space"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_subCategoryTableView  deselectRowAtIndexPath:indexPath animated:YES];
    
    buttonSender.tag = indexPath.row/2;
    
    if (_type == viewTypeServicios) {
        [self performSegueWithIdentifier:@"payment_services" sender:buttonSender];
    }else {
        [self getAmountWithId:[categoriesSubmenuInfo[buttonSender.tag][kIDKey] intValue]];
    }
}

- (void) getAmountWithId:(int)idValue
{
    amountManager = [[pootEngine alloc] init];
    [amountManager setDelegate:self];
    [amountManager setShowComments:developing];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
    [params setObject:[NSNumber numberWithInt:idValue] forKey:@"idServicio"];
    [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
    [params setObject:[NSNumber numberWithInt:[userData[kUserIdCountry] intValue]] forKey:@"idPais"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [amountManager startJSONRequestWithURL:WSGetCatalogRecargaMontos withPost:JSONString];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
}

#pragma Mark Segue handling
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (_type == viewTypeRecargas && selectedItem == 1) {
        if ([[segue identifier] isEqualToString:@"payment_recarga_tolls"]) {
            payment_recarga_ctrl_Toll *nextView = [segue destinationViewController];
            
            UIButton *button = sender;
            [nextView setType:button.tag==0?serviceTypeIAVE:serviceTypePASE];
            [nextView setDataSource:categoriesSubmenuInfo[button.tag]];
            [nextView setAmountDataSource:[NSArray arrayWithArray:ArrayToSend]];
            
            if (button.tag==0) { //I+D selected
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Estimado cliente, te recordamos que por disposición de I + D y por tu seguridad, el saldo de tu dispositivo NO podrá exceder de MX$3,000.00", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
            
            if (button.tag==1) { //PASE selected
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Estimado cliente, te recordamos que por disposición de PASE y por tu seguridad, el saldo de tu dispositivo NO podrá exceder de MX$3,000.00", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
        
    } else {
        if ([[segue identifier] isEqualToString:@"payment_recarga_tae"]) {
            payment_recarga_ctrl_Tae *nextView = [segue destinationViewController];
            
            UIButton *button = sender;
            
            [nextView setCountryData:countryData];
            [nextView setDataSource:categoriesSubmenuInfo[button.tag]];
            [nextView setAmountDataSource:[NSArray arrayWithArray:ArrayToSend]];
        }
    }
    
    if (_type == viewTypeServicios) {
        UIButton *button = sender;
        payServices_Ctrl *nextView = [segue destinationViewController];
        [nextView setInfoService:categoriesSubmenuInfo[button.tag]];
    }
}

#pragma Mark PootEngine Handling

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    if (manager == categoriesManager) {
        
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue] == 0) {
            switch (_type) {
                case viewTypeRecargas:
                {
                    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                    if ([userData[kUserIdCountry] intValue]==1) {
                        categoriesInfo = [NSMutableArray arrayWithArray:response[@"recargas"]];
                    } else {
                        categoriesInfo = [NSMutableArray arrayWithArray:response[@"recargas"]];
                        
                        [categoriesInfo removeObjectAtIndex:1];
                    }
                }
                    break;
                    
                case viewTypeServicios:
                {
                    categoriesInfo = [NSMutableArray arrayWithArray:response[@"categorias"]];
                }
                    break;
            }
            [self initializeView];
        }
    }
    
    if (manager == servicesManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue] == 0) {
            NSMutableDictionary *cleanResponse = [NSMutableDictionary dictionaryWithDictionary:response];
            cleanResponse = [self cleanDictionary:cleanResponse];
            categoriesSubmenuInfo = [NSMutableArray arrayWithArray:cleanResponse[@"servicios"]];
            masterCategoriesSubmenuInfo = [[NSMutableArray alloc] initWithArray:categoriesSubmenuInfo];
            
            [self countrySelection_Selection:_countryInfoSelected];
            
            [_subCategoryTableView reloadData];
        }
    }
    
    if (manager == amountManager) {
        NSDictionary *response = (NSDictionary*)json;
        switch (amountManager.tag) {
            case 10:
                ArrayToSend = [NSMutableArray arrayWithArray:response[@"productos"]];
                
                [self performSegueWithIdentifier:@"payment_recarga" sender:buttonSender];
                break;
                
            default:
            {
                if ([response[kIDError] intValue] == 0) {
                    ArrayToSend = [NSMutableArray arrayWithArray:response[@"montos"]];
                    if (_type == viewTypeRecargas && selectedItem == 1) {
                        [self performSegueWithIdentifier:@"payment_recarga_tolls" sender:buttonSender];
                    } else {
                        [self performSegueWithIdentifier:@"payment_recarga_tae" sender:buttonSender];
                    }
                }
            }
                break;
        }
    }
}

#pragma mark CountrySelection Protocol

- (void)countrySelection_Selection:(NSDictionary *)countryInfo
{
    if (countryInfo) {
        countryData = [NSDictionary dictionaryWithDictionary:countryInfo];
        
        categoriesSubmenuInfo = [[NSMutableArray alloc] initWithArray:masterCategoriesSubmenuInfo];
        
        if (![countryData[@"codigoPais"] isEqualToString:@"000052"]) {
            NSDictionary *unique = [[NSDictionary alloc] init];
            
            for (NSDictionary *element in categoriesSubmenuInfo) {
                if ([element[kIDKey] intValue]==2) {
                    unique = [NSDictionary dictionaryWithDictionary:element];
                }
            }
            
            [categoriesSubmenuInfo removeAllObjects];
            [categoriesSubmenuInfo addObject:unique];
        }
        [_subCategoryTableView reloadData];
    }
}

@end
