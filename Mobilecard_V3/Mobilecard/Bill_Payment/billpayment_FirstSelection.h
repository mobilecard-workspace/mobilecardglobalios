//
//  billpayment_FirstSelection.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/20/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceModel.h"
#import "mT_commonController.h"
#import "billpayment_Selection.h"
NS_ASSUME_NONNULL_BEGIN

@interface billpayment_FirstSelection : mT_commonController<UICollectionViewDelegate, UICollectionViewDataSource,pootEngineDelegate>{
    
    __weak IBOutlet UICollectionView *collection_service;
    
    NSMutableArray *recipeServices;
    ServiceModel *itemService;
    int SelectItem;
    
}
@property (nonatomic, assign) viewType type;
@end

NS_ASSUME_NONNULL_END
