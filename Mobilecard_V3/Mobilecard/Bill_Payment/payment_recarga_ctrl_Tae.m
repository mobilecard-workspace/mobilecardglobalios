//
//  payment_recarga_ctrl.m
//  Mobilecard
//
//  Created by David Poot on 11/1/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "payment_recarga_ctrl_Tae.h"
#import "generalCollectionCell.h"
#import "paymentGeneralResult.h"


@interface payment_recarga_ctrl_Tae ()

@end

@implementation payment_recarga_ctrl_Tae
{
    NSNumberFormatter *numFormat;
    
    NSMutableArray *cellArray;
    NSMutableArray *menuItems;
    
    float currentBalanace;
    
    int selectedID;
    
    pootEngine *tagsManager;
    pootEngine *cardManager;
    pootEngine *balanceManager;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    
    THMTrustDefender *profile;
    
    NSMutableDictionary *threatMetrixInfo;
    NSDictionary *selectedCardInfo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    threatMetrixInfo = nil;
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    cellArray = [[NSMutableArray alloc] init];
    
    switch (_type) {
        case serviceTypeIAVE:
        case serviceTypePASE:
        {
            //NOT USED

        }
            break;
            
        default: //TAE
        {
            menuItems = [[NSMutableArray alloc] init];
            [menuItems addObject:@"phoneNumber"];
            [menuItems addObject:@"space"];
            [menuItems addObject:@"confirmPhoneNumber"];
            [menuItems addObject:@"space"];
            [menuItems addObject:@"contactList"];
            [menuItems addObject:@"amounts"];
            [menuItems addObject:@"comission"];
            [menuItems addObject:@"payButton"];
        }
            break;
    }
    
    numFormat = [[NSNumberFormatter alloc] init];
    [numFormat setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormat setCurrencySymbol:@"$"];
    
    NSArray *fav = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey];
    for (NSDictionary *element in fav) {
        if ([[element objectForKey:@"dataSource"][kIDKey] intValue] == [_dataSource[kIDKey] intValue]) {
            _favoriteDataSource = element;
        }
    }
    
    [_numberText setUpTextFieldAs:textFieldTypeCellphone];
    [_numberConfirmText setUpTextFieldAs:textFieldTypeCellphone];
    
    [self addValidationTextField:_numberText];
    [self addValidationTextField:_numberConfirmText];
    
    [self addValidationTextFieldsToDelegate];
    
    //[self addShadowToView:_payButton];
    
    [_contactListButton addTarget:self action:@selector(showContactList:) forControlEvents:UIControlEventTouchUpInside];
    [_favoriteButton addTarget:self action:@selector(favoriteButton_Action:) forControlEvents:UIControlEventTouchUpInside];
    
    [_comissionLabel setText:@""];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0], NSFontAttributeName, [UIColor darkMCGreyColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    [[UISegmentedControl appearance] setBackgroundImage:[self imageFromColor:[UIColor darkMCGreyColor]] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:[self imageFromColor:[UIColor clearColor]] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    UIButton *button = [[UIButton alloc] init];
    [button setTag:selectedID];
    [self cellSelected:button];
    
    [self updateFavorite_Button];
    
    [super viewDidAppear:animated];
}

- (void)updateFavorite_Button {
    if (_favoriteDataSource) {
        [_favoriteButton setImage:[UIImage imageNamed:@"icon_fav_orange"] forState:UIControlStateNormal];
        [_favoriteLabel setText:NSLocalizedString(@"ELIMINAR FAVORITO", nil)];
    } else {
        [_favoriteButton setImage:[UIImage imageNamed:@"icon_fav_gray"] forState:UIControlStateNormal];
        [_favoriteLabel setText:NSLocalizedString(@"GUARDAR COMO FAVORITO", nil)];
    }
}

- (void)saveViewData
{
    NSMutableArray *favoritesArray;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]) {
        favoritesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]];
    } else {
        favoritesArray = [[NSMutableArray alloc] init];
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:_dataSource forKey:@"dataSource"];
    [params setObject:_amountDataSource forKey:@"amountDataSource"];
    [params setObject:[NSString stringWithFormat:@"%d", favoriteTopUpTae] forKey:@"type"];
    
    [favoritesArray addObject:params];
    
    [[NSUserDefaults standardUserDefaults] setObject:favoritesArray forKey:(NSString*)kUserFavoritesKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _favoriteDataSource = params;
}

- (void)favoriteButton_Action:(id)sender {
    if (_favoriteDataSource) {
        NSMutableArray *favoritesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserFavoritesKey]];
        
        [favoritesArray removeObject:_favoriteDataSource];
        _favoriteDataSource = nil;
        
        [[NSUserDefaults standardUserDefaults] setObject:favoritesArray forKey:(NSString*)kUserFavoritesKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [self saveViewData];
    }
    
    [self updateFavorite_Button];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showTagOptionList:(id)sender
{
    for (UITextField_Validations *element in [self getValidationTextFields]) {
        switch (element.tag) {
            case 10:
                [element setText:@""];
                break;
            case 20:
                [element setText:@""];
                break;
            default:
                break;
        }
    }
    
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"¿Desea seleccionar o eliminar TAG?", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Seleccionar", nil), NSLocalizedString(@"Eliminar", nil), nil];
    [alertMsg setTag:100];
    [alertMsg show];
}


- (void) showContactList:(id)sender
{
    CNContactPickerViewController *contactPicker = [[CNContactPickerViewController alloc] init];
    contactPicker.delegate = self;
    
    NSArray *arrKeys = @[CNContactPhoneNumbersKey]; //display only phone numbers
    contactPicker.displayedPropertyKeys = arrKeys;
    
    [self presentViewController:contactPicker animated:YES completion:nil];
}

- (IBAction)pay_action:(id)sender {
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        switch (_type) {
            case serviceTypePASE:
            case serviceTypeIAVE:
            {
                //NOT USED
            }
                break;
                
            default:
            {
                if ([[[self getValidationTextFields][0] text] isEqualToString:[[self getValidationTextFields][1] text]]) {
                    [self performSegueWithIdentifier:@"summary" sender:nil];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"El número y la confirmación del número no coinciden, intenta de nuevo", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    
                    [alertMsg show];
                }
            }
                break;
        }
    }
}

- (void) confirmPayment
{
    [self performSegueWithIdentifier:@"summary" sender:nil];
}

- (void) showCardOptions
{
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark Tableview setting up
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            switch (_type) {
                case serviceTypeIAVE:
                case serviceTypePASE:
                {
                    return @"";
                    //NOT USED
                }
                    break;
                    
                default:
                    if ([_amountDataSource count]>0) {
                        return [NSString stringWithFormat:@"  %@ - %@", _dataSource[kDescriptionKey], _amountDataSource[0][@"concepto"]];
                    } else {
                        return _dataSource[kDescriptionKey];
                    }
                    break;
            }
        }
            break;
            
        default:
            return @"";
            break;
    }
}

- (IBAction)back_Action:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (_type) {
        case serviceTypeIAVE:
        case serviceTypePASE:
        {
            return 0;
            //NOT USED
        }
            break;
            
        default:
        {
            switch (indexPath.row) {
                case 0://Number
                    return 75;
                    break;
                case 1://Spaces
                    return 10;
                    break;
                case 2://Confirm Number
                    return 125;
                    break;
                case 3://Spaces
                    return 10;
                    break;
                case 4://Contact List
                    return 40;
                    break;
                case 5://amounts
                    return 10*2+(_amountDataSource.count/2)*10+50+50*(_amountDataSource.count/2);
                    break;
                case 6://comission
                    return 30;
                case 7://Paybutton
                    return 70;
                default:
                    return 10;
                    break;
            }
        }
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark setting up Collection view

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_amountDataSource count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width-10)/2, 50);
}

- (generalCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"Cell_amount";
    
    generalCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.tag = indexPath.row;
    cell.cellButton.tag = indexPath.row;
    [cell.cellText setText:[NSString stringWithFormat:@"%@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[_amountDataSource[indexPath.row][@"monto"] floatValue]]]]];
    [cell.cellButton setEnabled:YES];
    [cell.cellButton addTarget:self action:@selector(cellSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    for (generalCollectionCell *cellInArray in cellArray) {
        if (cellInArray == cell) {
            return cell;
        }
    }
    
    [cellArray addObject:cell];
    
    return cell;
}

- (void) cellSelected:(id)sender
{
    UIButton *cellButton = sender;
    
    selectedID = (int)cellButton.tag;
    
    for (generalCollectionCell *cell in cellArray) {
        if (cell.tag == cellButton.tag) {
            [cell selectCell];
        } else {
            [cell deSelectCell];
        }
    }
    
    [_comissionLabel setText:[NSString stringWithFormat:NSLocalizedString(@"Comisión %@", nil), [numFormat stringFromNumber:[NSNumber numberWithFloat:[_amountDataSource[cellButton.tag][@"comision"] floatValue]]]]];
}

#pragma Mark pootEngine handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
    }
}

#pragma Mark storyBoards
- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view setType:viewType];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"summary"]) {
        UINavigationController *nav = [segue destinationViewController];
        generalSummary *nextView = [[nav viewControllers] firstObject];
        [nextView setDelegate:self];
        
        NSMutableDictionary *amountData = [[NSMutableDictionary alloc] initWithDictionary:_amountDataSource[selectedID]];
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        if ([userData[kUserIdCountry] intValue] == 3) { //RECARGAS USA
            if (!([_dataSource[kIDKey] intValue] == 2)) { //NOT MOVISTAR
                [nextView setMainString:[[self getValidationTextFields][0] text]];
                [nextView setFirstString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]]], @"MXN"]];
                [nextView setSecondString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]]], @"USD"]];
                [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"comision"] floatValue]]], @"MXN"]];
                [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]+[amountData[@"comision"] floatValue]]], @"MXN"]];
                [nextView setConvertToUSD:YES];
            } else { //MOVISTAR
                [nextView setMainString:[[self getValidationTextFields][0] text]];
                [nextView setFirstString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]]], @"USD"]];
                [nextView setSecondString:nil];
                [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"comision"] floatValue]]], @"USD"]];
                [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]+[amountData[@"comision"] floatValue]]], @"USD"]];
                
                [nextView setConvertToUSD:NO];
            }
        } else {  //RECARGAS MX
            [nextView setMainString:[[self getValidationTextFields][0] text]];
            [nextView setFirstString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]]], @"MXN"]];
            [nextView setSecondString:nil];
            [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"comision"] floatValue]]], @"MXN"]];
            [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormat stringFromNumber:[NSNumber numberWithFloat:[amountData[@"monto"] floatValue]+[amountData[@"comision"] floatValue]]], @"MXN"]];
            
            [nextView setConvertToUSD:YES];
        }
    }
    
    if ([[segue identifier] isEqualToString:@"result"]) {
        paymentGeneralResult *nextView = [segue destinationViewController];
        NSDictionary *resultData = (NSDictionary*)sender;
        [nextView setResultData:resultData];
    }
}


#pragma cardSelection Handler

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    switch (_type) {
        case serviceTypeIAVE:
        case serviceTypePASE:
        {
            //NO USE
        }
            break;
            
        default:
        {
            selectedCardInfo = selectedCard;
            [self performProfileCheck];
        }
            break;
    }
}

- (void)performPayment:(NSDictionary*)cardInfo
{
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    
    [params setObject:[NSNumber numberWithFloat:[_amountDataSource[selectedID][@"monto"] floatValue]] forKey:@"cargo"];
    [params setObject:@"" forKey:@"codigoPais"];
    [params setObject:[NSNumber numberWithFloat:[_amountDataSource[selectedID][@"comision"] floatValue]] forKey:@"comision"];
    
    if (!_amountDataSource[selectedID][@"concepto"]) {
        [params setObject:[NSString stringWithFormat:@"Pago %@", @"de servicio"] forKey:@"concepto"];
    } else {
        [params setObject:_amountDataSource[selectedID][@"concepto"] forKey:@"concepto"];
    }
    
    [params setObject:[NSNumber numberWithBool:debugging] forKey:@"debug"];
    [params setObject:[NSNumber numberWithInt:[_amountDataSource[selectedID][@"emisor"] intValue]] forKey:@"emisor"];
    
    [params setObject:[NSNumber numberWithInt:idApp] forKey:kIDApplication];
    [params setObject:[NSNumber numberWithInt:[cardInfo[@"idTarjeta"] intValue]] forKey:@"idCard"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][kIDKey] forKey:kUserIdCountry];
    [params setObject:_amountDataSource[selectedID][@"emisor"] forKey:@"idProveedor"];
    [params setObject:[NSNumber numberWithDouble:[userInfo[kUserIDKey] doubleValue]] forKey:@"idUser"];
    [params setObject:_amountDataSource[selectedID][@"operacion"] forKey:@"operacion"];
    [params setObject:[_numberText text] forKey:@"referencia"];
    
    
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"lat"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"lon"];
    
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"modelo"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
    
    Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
    [nextView setPurchaseInfo:params];
    [nextView setSecure3DURL:WSGetTransactoPagoProsa3DSSecured];
    [nextView setType:serviceTypeTAE];
    [nextView setThreatMetrixInfo:threatMetrixInfo];
    [nextView setDelegate:self];
    [nextView setSelectedCardInfo:[NSMutableDictionary dictionaryWithDictionary:cardInfo]];
    
    [self.navigationController pushViewController:nextView animated:YES];
}

#pragma Mark summary Handler

- (void)generalSummaryResponse:(NSDictionary *)response
{
    [self showCardOptions];
}


#pragma Mark CNContactPicker Handler (Address book)

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty{
    
    CNPhoneNumber *phoneNumber = contactProperty.value;
    
    for (UITextField_Validations *element in [self getValidationTextFields]) {
        UITextField_Validations *textfield = (UITextField_Validations*)element;
        
        @try {
            NSString *phone = [phoneNumber stringValue];
            phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
            phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([phone length]>=10) {
                [textfield setText:[phone substringWithRange:NSMakeRange([phone length] - 10, 10)]];
            } else {
                [textfield setText:phone];
            }
            
        } @catch (NSException *exception) {
            nil;
        } @finally {
            nil;
        }
        
        
    }
}


#pragma Mark TrustDefender Handling

- (void) performProfileCheck
{
    [self lockViewWithMessage:nil];
    
    profile = [THMTrustDefender sharedInstance];
    
    [profile configure:@{
                         THMOrgID:@"c2ggoqh1",
                         THMFingerprintServer:@"h-sdk.online-metrix.net",
                         }];
    
    [profile doProfileRequestWithCallback:^(NSDictionary *result)
     {
         [self unLockView];
         developing?NSLog(@"PROFILE RESULT -> %@", result):nil;
         self->threatMetrixInfo = [NSMutableDictionary dictionaryWithDictionary:result];
         
         THMStatusCode statusCode = [[result valueForKey:THMProfileStatus] integerValue];
         
         if (statusCode == THMStatusCodeOk) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self performPayment:selectedCardInfo];
             });
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:@"%s", statusCode == THMStatusCodeNetworkTimeoutError ? "Timed out"                                                                                                                                                                : statusCode == THMStatusCodeConnectionError     ? "Connection Error"                                                                                                                                                           : statusCode == THMStatusCodeHostNotFoundError   ? "Host not found error"                                                                                                                                                : statusCode == THMStatusCodeInternalError       ? "Internal Error"                                                                                                                                                        : statusCode == THMStatusCodeInterruptedError    ? "Interrupted"                                                                                                                                                           : "other"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [alertMsg dismissViewControllerAnimated:YES completion:nil];
                 }];
                 
                 [alertMsg addAction:ok];
                 
                 [self presentViewController:alertMsg animated:YES completion:nil];
             });
         }
         
         
     }];
}

- (void)PaymentWithThreatMetrixResponse:(NSDictionary *)response
{
    if (response) {
        [self performSegueWithIdentifier:@"result" sender:response];
        
        if ([response[@"code"] intValue] == 0) {
            
        };
    }
}




#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Failed to Get Your Location" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}
@end
