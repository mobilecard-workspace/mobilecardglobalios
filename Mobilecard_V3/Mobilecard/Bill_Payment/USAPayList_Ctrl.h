//
//  USAPayList_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 12/19/16.
//  Copyright © 2016 David Poot. All rights reserved.
//

#import "mT_commonController.h"
#import "USAPayCheckOut.h"



@interface USAPayList_Ctrl : mT_commonController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating, pootEngineDelegate>

@property (nonatomic, assign) viewType type;


@property (weak, nonatomic) IBOutlet UIView *servicesTableContainer;
@property (weak, nonatomic) IBOutlet UITableView *servicesTable;
@property (strong, nonatomic) IBOutlet UISearchController *searchController;


@end
