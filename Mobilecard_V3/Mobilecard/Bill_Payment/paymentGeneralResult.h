//
//  paymentGeneralResult.h
//  Mobilecard
//
//  Created by David Poot on 3/25/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface paymentGeneralResult : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *resultData;

@property (weak, nonatomic) IBOutlet UIView *result;
@property (weak, nonatomic) IBOutlet UIImageView *resultIcon;
@property (weak, nonatomic) IBOutlet UILabel *resultTitleText;
@property (weak, nonatomic) IBOutlet UITextView *resultDetailsText;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *finishButton;

@end

NS_ASSUME_NONNULL_END
