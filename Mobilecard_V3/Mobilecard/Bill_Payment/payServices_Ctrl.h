//
//  payServices_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 1/17/17.
//  Copyright © 2017 David Poot. All rights reserved.
//


#import "mT_commonTableViewController.h"
#import "ZBarSDK.h"
#import "MC_SAPOverlayView.h"
#import "Secure3D_Ctrl.h"
#import "Wallet_Ctrl.h"
#import "generalSummary.h"
#import <CoreLocation/CoreLocation.h>
#import <TrustDefender/TrustDefender.h>

@interface payServices_Ctrl : mT_commonTableViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, Secure3DDelegate, pootEngineDelegate, ZBarReaderDelegate, cardSelectionDelegate, updateCardDelegate, generalSummaryDelegate, CLLocationManagerDelegate>
{
    MC_SAPOverlayView *overlayView;
}

@property (strong, nonatomic) NSDictionary *infoService;
@property (strong, nonatomic) IBOutlet UITableView *serviceTable;

@property (weak, nonatomic) IBOutlet UIButton *scan_Button;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@property (weak, nonatomic) IBOutlet UITextField_Validations *referenceText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *amountText;

@property (weak, nonatomic) IBOutlet UILabel *corporationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *corporationImage;

@end
