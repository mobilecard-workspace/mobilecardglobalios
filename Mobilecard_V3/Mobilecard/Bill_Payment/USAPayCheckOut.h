//
//  USAPayCheckOut.h
//  MobileCard_X
//
//  Created by David Poot on 5/9/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Wallet_Ctrl.h"
#import "orderResult_Ctrl.h"
#import "USAPayFillForm.h"
#import <CoreLocation/CoreLocation.h>

@interface USAPayCheckOut : mT_commonTableViewController <cardSelectionDelegate, updateCardDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) NSArray *addressesInfo;
@property (strong, nonatomic) NSDictionary *serviceInfo;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *accountNumberText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *amountText;
@property (weak, nonatomic) IBOutlet UIButton *payButton;

@property (weak, nonatomic) IBOutlet UILabel *feeText;


@property (strong, nonatomic) IBOutlet UITableView *tableViewData;

@end
