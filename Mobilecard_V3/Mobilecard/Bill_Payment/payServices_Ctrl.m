//
//  payServices_Ctrl.m
//  MobileCard_X
//
//  Created by David Poot on 1/17/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#define mcOrangeColor [UIColor colorWithRed:255.0/255.0 green:128.0/255.0 blue:0.0 alpha:1.0]

#import "payServices_Ctrl.h"
#import "paymentGeneralResult.h"

@interface payServices_Ctrl ()
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    
    NSMutableArray *menuItems;
    
    BOOL needQuery;
    
    float finalAmount;
    float finalComission;
    
    NSNumberFormatter *numFormatter;
    
    NSString *scannerReference;
    
    pootEngine *queryManager;
    pootEngine *cardManager;
    NSDictionary *queryResult;
    
    NSDictionary *generalAttrib;
    NSDictionary *dictBoldText;
    
    THMTrustDefender *profile;
    
    NSMutableDictionary *threatMetrixInfo;
    NSDictionary *selectedCardInfo;
}
@end

@implementation payServices_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    [[self corporationLabel]setText:_infoService[@"empresa"]];
    [_corporationImage setImage:[UIImage imageNamed:_infoService[@"empresa"]]];
    
//    [imageView setImage:[UIImage imageNamed:categoriesSubmenuInfo[indexPath.row/2][_type==viewTypeRecargas?kDescriptionKey:@"empresa"]]]:nil;
    
    needQuery = [_infoService[@"consultaSaldo"] boolValue];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
   // [_scan_Button setTitle:[NSString stringWithFormat:NSLocalizedString(@"ESCANEAR %@", nil), [_infoService[@"nombreReferencia"] uppercaseString]] forState:UIControlStateNormal];
    [_scan_Button addTarget:self action:@selector(scan_Action:) forControlEvents:UIControlEventTouchUpInside];
    [_continue_Button addTarget:self action:@selector(pay_action:) forControlEvents:UIControlEventTouchUpInside];
    
  //  [self addShadowToView:_scan_Button];
    [[_scan_Button titleLabel] setNumberOfLines:2];
  //  [self addShadowToView:_continue_Button];
    
    [_referenceText setPlaceholder:[_infoService[@"nombreReferencia"] uppercaseString]];
    [_referenceText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_referenceText setMaxLength:50];
    [_referenceText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIUJKLMNOPQRSTUVWXYZ1234567890"]];
    [_referenceText setKeyboardType:UIKeyboardTypeDefault];
    
    [_amountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_amountText setAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]];
    [_amountText setKeyboardType:UIKeyboardTypeDecimalPad];
    
    [self updateContinueButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateContinueButton
{
    [self removeAllTextFields];
    [self addValidationTextField:_referenceText];
    
    if (needQuery) {
        [_amountText setText:@""];
        [_amountText setEnabled:NO];
        [_amountText setHidden:YES];
        [_continue_Button setTitle:NSLocalizedString(@"CONSULTAR SALDO >", nil) forState:UIControlStateNormal];
    } else {
        [self addValidationTextField:_amountText];
        [_amountText setEnabled:YES];
        [_continue_Button setTitle:NSLocalizedString(@"CONTINUAR >", nil) forState:UIControlStateNormal];
    }
    
    [self addValidationTextFieldsToDelegate];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) scan_Action:(id)sender
{
    @try {
        ZBarReaderViewController *reader = [[ZBarReaderViewController alloc]init];
        
        
        
        reader.readerDelegate = self;
        //[reader.scanner setSymbology:0 config:ZBAR_CFG_ENABLE to:0];
        //[reader.scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_ENABLE to:1];  //QR READING
        //[reader.scanner setSymbology:ZBAR_CODE128 config:ZBAR_CFG_ENABLE to:1]; //BAR READING
        
        [reader setTitle:@"Enfoque el código"];
        
        reader.showsZBarControls = NO;
        
        
        overlayView = [[MC_SAPOverlayView alloc]initWithNibName:@"MC_SAPOverlayView" bundle:nil];
        [overlayView.view setFrame:CGRectMake(0, 0, 320, 480)];
        
        [reader setCameraOverlayView:[overlayView view]];
        //[reader setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        
        [self presentViewController:reader animated:YES completion:^{
            
            CGRect backFrame = CGRectMake(0, reader.view.frame.size.height-60, reader.view.frame.size.width, 60);
            
            UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [backButton setFrame:backFrame];
            [backButton setBackgroundColor:[UIColor clearColor]];
            [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [backButton setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
            
            [overlayView.view addSubview:backButton];
        }];
        
        
    }
    @catch (NSException *exception) {
        NSLog(@"No se activo escaner: %@",[exception description]);
    }
}

- (void)backAction:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Pay",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    @try {
        
        ZBarSymbolSet *symbolset=[info objectForKey:ZBarReaderControllerResults];
        ZBarSymbol  *symbol=nil;
        
        NSString *qrCode=[[NSString alloc]init];
        
        for(symbol in symbolset )
        {
            qrCode=[NSString stringWithString:symbol.data];
        }
        
        scannerReference = qrCode;
        
        [self backAction:nil];
        switch ([_infoService[@"consultaSaldo"] boolValue]) {
            case YES:
                [self startQueryFromCamera:YES];
                break;
            case NO:
                [_referenceText setText:scannerReference];
                break;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error:%@",[exception description]);
        UIAlertView *error=[[UIAlertView  alloc]initWithTitle:@"Error." message:@"Codigo QR incorrecto." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [error show];
    }
}


- (void) help_Action:(id)sender
{
    UIAlertView *showInfo = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 280, 400)];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"S%d.png", [_infoService[@"id"] intValue]]]];
    [showInfo setValue:imageView forKey:@"accessoryView"];
    [showInfo show];

}

- (void) pay_action:(id)sender
{
    [self startQueryFromCamera:NO];
}

- (void)startQueryFromCamera:(BOOL)fromCamera
{
    if (needQuery) {
        
        if (fromCamera?YES:[self validateFieldsInArray:[self getValidationTextFields]]) {
            NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            queryManager = [[pootEngine alloc] init];
            [queryManager setDelegate:self];
            [queryManager setShowComments:developing];
            
            [_referenceText setText:fromCamera?scannerReference:[_referenceText text]];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            
            [params setObject:_infoService[@"operacion"] forKey:@"operacion"];
            [params setObject:[NSNumber numberWithInt:[_infoService[@"emisor"] intValue]] forKey:@"emisor"];
            [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
            [params setObject:fromCamera?scannerReference:[_referenceText text] forKey:@"referencia"];
            [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
            
            [params setObject:[userInfo[kUserIdCountry] intValue]==1?@"1":@"2" forKey:kUserIdCountry];
            
            [queryManager startProtectedRequestWithValues:params forWS:WSGetTransactoConsultaSaldos withPass:@"NJK348sX6F" Automated:YES];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        }
    } else {
        
        if ([self validateFieldsInArray:[self getValidationTextFields]]) {
            [self performSegueWithIdentifier:@"summary" sender:nil];
        }
    }
}

#pragma Mark UITextField delegate

- (BOOL)textField:(UITextField_Validations *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (_amountText == textField) {
        finalAmount = [[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue];
        finalComission = [[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue]*[_infoService[@"porcentajeComision"] floatValue];
    }
    
    return [self subTextField:textField shouldChangeCharactersInRange:range replacementString:string];
}
#pragma Mark pootEngine Handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == queryManager) {
        NSDictionary *response = (NSDictionary *)json;
        
        if ([response[@"idError"] intValue]==0) {
            finalAmount = [response[@"monto"] floatValue];
            finalComission = [response[@"comision"] floatValue];
            
            queryResult = response;
            
            [self performSegueWithIdentifier:@"summary" sender:nil];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:@"" message:response[@"mensajeError"] delegate:Nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
    }
}

#pragma Mark storyBoards
- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view setType:viewType];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"summary"]) {
        UINavigationController *nav = [segue destinationViewController];
        generalSummary *nextView = [[nav childViewControllers] firstObject];
        [nextView setDelegate:self];
        
        NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        if (needQuery) {
            [nextView setMainPlaceholderString:[_infoService[@"nombreReferencia"] uppercaseString]];
            [nextView setMainString:[_referenceText.text uppercaseString]];
            [nextView setFirstString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryResult[@"montoMxn"] floatValue]]], @"MXN"]];
            switch ([userDetails[kUserIdCountry] intValue]) {
                case 1: //MEXICO
                    [nextView setSecondString:nil];
                    [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryResult[@"comisionMxn"] floatValue]]], @"MXN"]];
                    [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryResult[@"totalMxn"] floatValue]]], @"MXN"]];
                    break;
                case 3: //USA
                    [nextView setSecondString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryResult[@"montoUsd"] floatValue]]], @"USD"]];
                    [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryResult[@"comisionUsd"] floatValue]]], @"USD"]];
                    [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[queryResult[@"totalUsd"] floatValue]]], @"USD"]];
                    [nextView setRateString:[NSString stringWithFormat:@"%f", [queryResult[@"tipoCambio"] floatValue]]];
                    
                    [nextView setConvertToUSD:NO];
                default:
                    //NEVER HERE!
                    break;
            }
            
        } else {
            [nextView setMainPlaceholderString:[_infoService[@"nombreReferencia"] uppercaseString]];
            [nextView setMainString:[_referenceText.text uppercaseString]];
            switch ([userDetails[kUserIdCountry] intValue]) {
                case 1: //MEXICO
                    [nextView setFirstString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount]], @"MXN"]];
                    [nextView setSecondString:nil];
                    [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalComission]], @"MXN"]];
                    [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount+finalComission]], @"MXN"]];
                    break;
                case 3: //USA
                    [nextView setFirstString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount]], @"MXN"]];
                    [nextView setSecondString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount]], @"MXN"]];
                    [nextView setThirdString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalComission]], @"MXN"]];
                    [nextView setFourthString:[NSString stringWithFormat:@"%@ %@", [numFormatter stringFromNumber:[NSNumber numberWithFloat:finalAmount+finalComission]], @"MXN"]];
                    
                    [nextView setConvertToUSD:YES];
                    break;
                default:
                    //NEVER HERE
                    break;
            }
        }
    }
    
    if ([[segue identifier] isEqualToString:@"result"]) {
        paymentGeneralResult *nextView = [segue destinationViewController];
        NSDictionary *resultData = (NSDictionary*)sender;
        [nextView setResultData:resultData];
    }
}


- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    selectedCardInfo = selectedCard;
    [self performProfileCheck];
}


- (void) performPayment:(NSDictionary*)cardInfo
{
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    

    [params setObject:_infoService[kIDKey] forKey:kIDKey];
    
    if ([_infoService[@"consultaSaldo"] boolValue]) {
        [params setObject:queryResult[@"monto"] forKey:@"cargo"];
        [params setObject:queryResult[@"comision"] forKey:@"comision"];
    } else {
        [params setObject:[NSNumber numberWithFloat:finalAmount] forKey:@"cargo"];
        [params setObject:[NSNumber numberWithFloat:finalAmount*[_infoService[@"porcentajeComision"] floatValue]] forKey:@"comision"];
    }
    
    [params setObject:@"" forKey:@"codigoPais"];
    [params setObject:[NSString stringWithFormat:@"Pago %@", _infoService[@"empresa"]] forKey:@"concepto"];
    
    [params setObject:[NSNumber numberWithBool:debugging] forKey:@"debug"];
    [params setObject:[NSNumber numberWithInt:[_infoService[@"emisor"] intValue]] forKey:@"emisor"];
    
    [params setObject:[NSNumber numberWithInt:idApplication] forKey:kIDApplication];
    [params setObject:[NSNumber numberWithInt:[cardInfo[@"idTarjeta"] intValue]] forKey:@"idCard"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][kIDKey] forKey:kUserIdCountry];
    [params setObject:_infoService[@"emisor"] forKey:@"idProveedor"];
    [params setObject:[NSNumber numberWithDouble:[userInfo[kUserIDKey] doubleValue]] forKey:@"idUser"];
    [params setObject:_infoService[@"operacion"] forKey:@"operacion"];
    [params setObject:_referenceText.text forKey:@"referencia"];
    
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] forKey:@"lat"];
    [params setObject:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] forKey:@"lon"];
    
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"modelo"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
    
    [params setObject:NSLocalizedString(@"lang", nil) forKey:@"idioma"];
    
    Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
    [nextView setPurchaseInfo:params];
    [nextView setSecure3DURL:WSGetTransactoPagoProsa3DSSecured];
    [nextView setType:serviceTypeServices];
    [nextView setThreatMetrixInfo:threatMetrixInfo];
    [nextView setDelegate:self];
    [nextView setSelectedCardInfo:[NSMutableDictionary dictionaryWithDictionary:cardInfo]];
    
    [self.navigationController pushViewController:nextView animated:YES];
}

#pragma Mark handling summary

- (void)generalSummaryResponse:(NSDictionary *)response
{
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
        //NSLog(@"%@", exception.description);
        
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}














#pragma Mark TrustDefender Handling

- (void) performProfileCheck
{
    [self lockViewWithMessage:nil];
    
    profile = [THMTrustDefender sharedInstance];
    
    [profile configure:@{
                         THMOrgID:@"c2ggoqh1",
                         THMFingerprintServer:@"h-sdk.online-metrix.net",
                         }];
    
    [profile doProfileRequestWithCallback:^(NSDictionary *result)
     {
         [self unLockView];
         developing?NSLog(@"PROFILE RESULT -> %@", result):nil;
         self->threatMetrixInfo = [NSMutableDictionary dictionaryWithDictionary:result];
         
         THMStatusCode statusCode = [[result valueForKey:THMProfileStatus] integerValue];
         
         if (statusCode == THMStatusCodeOk) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self performPayment:selectedCardInfo];
             });
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:@"%s", statusCode == THMStatusCodeNetworkTimeoutError ? "Timed out"                                                                                                                                                                : statusCode == THMStatusCodeConnectionError     ? "Connection Error"                                                                                                                                                           : statusCode == THMStatusCodeHostNotFoundError   ? "Host not found error"                                                                                                                                                : statusCode == THMStatusCodeInternalError       ? "Internal Error"                                                                                                                                                        : statusCode == THMStatusCodeInterruptedError    ? "Interrupted"                                                                                                                                                           : "other"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [alertMsg dismissViewControllerAnimated:YES completion:nil];
                 }];
                 
                 [alertMsg addAction:ok];
                 
                 [self presentViewController:alertMsg animated:YES completion:nil];
             });
         }
         
         
     }];
}

- (void)PaymentWithThreatMetrixResponse:(NSDictionary *)response
{
    if (response) {
        [self performSegueWithIdentifier:@"result" sender:response];
        
        if ([response[@"code"] intValue] == 0) {
            
        };
    }
}











#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Failed to Get Your Location" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}

@end
