//
//  billpayment_Selection.h
//  Mobilecard
//
//  Created by David Poot on 10/29/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonController.h"

@interface billpayment_Selection : mT_commonController <UITableViewDelegate, UITableViewDataSource, pootEngineDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, assign) viewType type;

@property (nonatomic, assign) int id_Categoria;

@property (strong, nonatomic) NSDictionary* countryInfoSelected;
@property (strong, nonatomic) NSMutableArray* Categories;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *subCategoryTableView;

@property (weak, nonatomic) IBOutlet UICollectionView *serviceCollection;

@end
