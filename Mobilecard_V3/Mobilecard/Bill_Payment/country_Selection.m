//
//  country_Selection.m
//  Mobilecard
//
//  Created by David Poot on 12/13/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "country_Selection.h"

@interface country_Selection ()
{
    pootEngine *countryManager;
    
    NSMutableArray *countryArray;
    
    NSArray *countryResponse;
    
    NSDictionary *countryInfoSelected;
}
@end

@implementation country_Selection

- (void)viewDidLoad {
    [super viewDidLoad];
    
    countryArray = [[NSMutableArray alloc] init];
    countryResponse = [[NSArray alloc] init];
    
    [self updateCountryArray];
    
    countryManager = [[pootEngine alloc] init];
    [countryManager setShowComments:developing];
    [countryManager setDelegate:self];
    
    [countryManager startWithoutPostRequestWithURL:WSGetCatalogCountries];
    
    [self lockViewWithMessage:nil];
}

- (void) updateCountryArray
{
    [countryArray removeAllObjects];
    
    [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"header", kIdentifierKey, @"70", kHeightKey, @"", kDescriptionKey, nil]];
    
    for (NSDictionary *element in countryResponse) {
        [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"country", kIdentifierKey, @"44", kHeightKey, [NSDictionary dictionaryWithDictionary:element], kDescriptionKey, nil]];
        [countryArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey, @"", kDescriptionKey, nil]];
    }
    
    [[self tableView] reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    /*
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        if ([_delegate conformsToProtocol:@protocol(countrySelectionProtocol)]&&[_delegate respondsToSelector:@selector(countrySelection_Selection:)]) {
            [_delegate countrySelection_Selection:nil];
        };
    }];
     */
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [countryArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [countryArray[indexPath.row][kHeightKey] floatValue];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:countryArray[indexPath.row][kIdentifierKey] forIndexPath:indexPath];
            return cell;
        }
            break;
            
        default:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:countryArray[indexPath.row][kIdentifierKey] forIndexPath:indexPath];
            
            if ([[cell reuseIdentifier] isEqualToString:@"space"]) {
                return cell;
            } else {
                for (id element in cell.contentView.subviews) {
                    if ([element isKindOfClass:[UILabel class]]) {
                        UILabel *label = element;
                        [label setText:countryArray[indexPath.row][kDescriptionKey][@"nombrePais"]];
                    }
                    if ([element isKindOfClass:[UIImageView class]]) {
                        UIImageView *image = element;
                        switch (image.tag) {
                            case 1:
                            {
                                [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_flag", NSLocalizedString(countryArray[indexPath.row][kDescriptionKey][@"codigoPais"], nil)]]];
                            }
                                break;
                                
                            default:
                                break;
                        }
                    }
                }
                return cell;
            }
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    countryInfoSelected = countryArray[indexPath.row][kDescriptionKey];
    
    if ([countryArray[indexPath.row][kDescriptionKey][@"id"] intValue] == 11) {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"blackstone" bundle:nil];
        UINavigationController *nav = [nextStory instantiateInitialViewController];
        blackstone_Step1 *nextView = [nav.viewControllers firstObject];
        
        [self.navigationController pushViewController:nextView animated:YES];
    } else {
        [self performSegueWithIdentifier:@"carriers" sender:self];
    }
    
    /*
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([_delegate conformsToProtocol:@protocol(countrySelectionProtocol)]&&[_delegate respondsToSelector:@selector(countrySelection_Selection:)]) {
        [_delegate countrySelection_Selection:countryArray[indexPath.row][kDescriptionKey]];
    };
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
     */
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == countryManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue] == 0) {
            countryResponse = [NSArray arrayWithArray:response[@"paises"]];
            
            [self updateCountryArray];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }

    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"carriers"]) {
        billpayment_Selection *nextView = [segue destinationViewController];
        [nextView setCountryInfoSelected:countryInfoSelected];
    }
}

@end
