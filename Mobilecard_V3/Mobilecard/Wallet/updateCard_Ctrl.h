//
//  updateCard_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 10/31/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol updateCardDelegate <NSObject>

@required
- (void)updateCardResponse:(NSDictionary*)response;
- (void)removeCardWithID:(int)cardID;

@end

@interface updateCard_Ctrl : mT_commonTableViewController <UITextFieldDelegate, UIAlertViewDelegate, UIActionSheetDelegate>

@property (nonatomic, assign) id <updateCardDelegate, NSObject> delegate;

@property (nonatomic, assign) updateCardType type;
@property (nonatomic, assign) walletViewType walletType;

@property int cardToUpdateID;

@property (weak, nonatomic) IBOutlet UITextField_Validations *cardNameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cardText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cvv2Text;
@property (weak, nonatomic) IBOutlet UITextField_Validations *validText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cardTypeText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;

@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UISwitch *primarySwitch;
@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteButton;

@property (strong, nonatomic) IBOutlet UITableView *dataTable;
@end
