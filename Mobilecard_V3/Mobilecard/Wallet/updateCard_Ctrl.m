//
//  updateCard_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 10/31/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "updateCard_Ctrl.h"

@interface updateCard_Ctrl ()
{
    int cardType;
    NSArray* cardToUpdate;
    
    pootEngine *updateManager;
}

@end

@implementation updateCard_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
 //   [self addShadowToView:self.navigationController.navigationBar];
 //   [self addShadowToView:_updateButton];
    
    [self initializePickersForView:self.navigationController.view];
    
    updateManager = [[pootEngine alloc] init];
    [updateManager setDelegate:self];
    [updateManager setShowComments:developing];
    
    [_cardNameText setUpTextFieldAs:textFieldTypeName];
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_cardText setUpTextFieldAs:textFieldTypeCard];
    [_cvv2Text setUpTextFieldAs:textFieldTypeCvv2];
    [_validText setUpTextFieldAs:textFieldTypeValidCard];
    [_cardTypeText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_addressText setMaxLength:100];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    
    [_cardNameText setEnabled:NO];
    //[self addValidationTextField:_cardNameText];
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_cardText];
    [self addValidationTextField:_cvv2Text];
    [self addValidationTextField:_validText];
    [self addValidationTextField:_cardTypeText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_zipText];
    
    [self addValidationTextFieldsToDelegate];
    
    NSMutableArray *cardTypeArray = [[NSMutableArray alloc] init];
    [cardTypeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"CREDITO", kIDKey, NSLocalizedString(@"CREDITO", nil), kDescriptionKey, nil]];
    [cardTypeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"DEBITO", kIDKey, NSLocalizedString(@"DEBITO", nil), kDescriptionKey, nil]];
    
    [_cardTypeText setIdLabel:(NSString*)kIDKey];
    [_cardTypeText setDescriptionLabel:(NSString*)kDescriptionKey];
    [_cardTypeText setInfoArray:cardTypeArray];
    
    cardToUpdate = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserCardArray];
    
    switch (_type) {
        case updateCardTypeNew:
        case updateCardTypeRegistry:
        {
            [_cardNameText setText:NSLocalizedString(@"TARJETA NUEVA", nil)];
            [_deleteButton setTitle:@""];
            [_deleteButton setEnabled:NO];
            [_updateButton setTitle:NSLocalizedString(@"AÑADIR TARJETA", nil) forState:UIControlStateNormal];
            [_nameText setText:[NSString stringWithFormat:@"%@ %@", [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName], [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserLastName]]];
        }
            break;
            
        case updateCardTypeExisting:
        {
            [_cardNameText setText:NSLocalizedString(@"TARJETA A MODIFICAR", nil)];
            [_cardText setText:[updateManager decryptedStringOfString:cardToUpdate[_cardToUpdateID][@"pan"] withSensitive:NO]];
            [self textFieldDidEndEditing:_cardText];
            [_validText setText:[updateManager decryptedStringOfString:cardToUpdate[_cardToUpdateID][@"vigencia"] withSensitive:NO]];
            [_nameText setText:cardToUpdate[_cardToUpdateID][@"nombre"]?cardToUpdate[_cardToUpdateID][@"nombre"]:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][kUserName]];
            [_cvv2Text setText:[updateManager decryptedStringOfString:cardToUpdate[_cardToUpdateID][@"codigo"] withSensitive:NO]];
            [_primarySwitch setOn:[cardToUpdate[_cardToUpdateID][@"determinada"] boolValue]];
            [_primarySwitch setEnabled:!_primarySwitch.on];
            [_cardTypeText setText:_cardTypeText.infoArray[[cardToUpdate[_cardToUpdateID][@"tipoTarjeta"] isEqualToString:@"CREDITO"]?0:1][kDescriptionKey]];
            [_addressText setText:cardToUpdate[_cardToUpdateID][@"domAmex"]];
            [_zipText setText:cardToUpdate[_cardToUpdateID][@"cpAmex"]];
            
            [_cvv2Text setHidden:NO];
            [_cardText setEnabled:NO];
            
            switch ([[[updateManager decryptedStringOfString:cardToUpdate[_cardToUpdateID][@"pan"] withSensitive:NO] substringWithRange:NSMakeRange(0, 1)] intValue]) {
                case 4:
                case 5: //SET ADDRESS & ZIP MANDATORY
                    [_addressText setEnabled:YES];
                    [_zipText setEnabled:YES];
                    break;
                    
                default:
                    [_addressText setEnabled:YES];
                    [_zipText setEnabled:YES];
                    break;
            }
        }
            break;
            
        default:
        {
            [_cvv2Text setHidden:YES];
            [_primarySwitch setHidden:YES];
            [_nameText setRequired:NO];
            [_cvv2Text setRequired:NO];
        }
            break;
    }
}
#pragma mark UITableViewController LoadItems
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 ) {
        return 0;
    }else  if (indexPath.row == 3 ) {
        return 208;
    }else  if (indexPath.row == 5 ) {
        return 272;
    }
    return 25;
}
- (IBAction)update_Action:(id)sender {
    switch (_type) {
        case updateCardTypeNew:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
                
                [params setObject:[updateManager encryptJSONString:_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                [params setObject:[updateManager encryptJSONString:_cvv2Text.text withPassword:nil] forKey:@"codigo"];
                [params setObject:[updateManager encryptJSONString:_validText.text withPassword:nil] forKey:@"vigencia"];
                [params setObject:_nameText.text forKey:@"nombre"];
                
                [params setObject:[_addressText text] forKey:@"domAmex"];
                [params setObject:[_zipText text] forKey:@"cpAmex"];
                [params setObject:[NSNumber numberWithBool:_primarySwitch.on] forKey:@"determinada"];
                [params setObject:[NSNumber numberWithInt:-1] forKey:@"idTarjeta"];
                
                [params setObject:_cardTypeText.infoArray[_cardTypeText.selectedID][_cardTypeText.idLabel] forKey:@"tipoTarjeta"];
                
                [params setObject:(NSLocalizedString(@"lang", nil)) forKey:kLanguage];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [updateManager startJSONRequestWithURL:WSWalletAddCard withPost:JSONString];
                
                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
            }
        }
            break;
            
        case updateCardTypeExisting:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
                
                [params setObject:[updateManager encryptJSONString:_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                [params setObject:[updateManager encryptJSONString:_cvv2Text.text withPassword:nil] forKey:@"codigo"];
                [params setObject:[updateManager encryptJSONString:_validText.text withPassword:nil] forKey:@"vigencia"];
                
                [params setObject:_nameText.text forKey:@"nombre"];
                [params setObject:[_addressText text] forKey:@"domAmex"];
                [params setObject:[_zipText text] forKey:@"cpAmex"];
                [params setObject:[NSNumber numberWithBool:_primarySwitch.on] forKey:@"determinada"];
                [params setObject:cardToUpdate[_cardToUpdateID][@"idTarjeta"] forKey:@"idTarjeta"];
                
                [params setObject:_cardTypeText.infoArray[_cardTypeText.selectedID][_cardTypeText.idLabel] forKey:@"tipoTarjeta"];
                
                [params setObject:(NSLocalizedString(@"lang", nil)) forKey:kLanguage];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [updateManager startJSONRequestWithURL:WSWalletUpdateCard withPost:JSONString];
                
                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
            }
        }
            break;
        case updateCardTypeRegistry:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                [self dismissViewControllerAnimated:YES completion:^{
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    
                    [params setObject:[updateManager encryptJSONString:_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                    [params setObject:[updateManager encryptJSONString:_cvv2Text.text withPassword:nil] forKey:@"codigo"];
                    [params setObject:[updateManager encryptJSONString:_validText.text withPassword:nil] forKey:@"vigencia"];
                    [params setObject:_nameText.text forKey:@"nombre"];
                    [params setObject:_cardTypeText.infoArray[_cardTypeText.selectedID][_cardTypeText.idLabel] forKey:@"tipoTarjeta"];
                    
                    [params setObject:@"true" forKey:@"determinada"];
                    [params setObject:@"0" forKey:@"idTarjeta"];
                    [params setObject:@"0" forKey:@"idUsuario"];
                    [params setObject:@"false" forKey:@"isMobilecard"];
                    
                    [params setObject:[_addressText text] forKey:@"domAmex"];
                    [params setObject:[_zipText text] forKey:@"cpAmex"];
                    
                    if ([_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[_delegate respondsToSelector:@selector(updateCardResponse:)]) {
                        [_delegate updateCardResponse:params];
                    }
                }];
            }
        }
            break;
    }
}


#pragma Mark UITextFields handler

- (void)textFieldDidEndEditing:(UITextField_Validations *)textField
{
    if (textField == _cardText) {
        textField.hiddenString = textField.text;
        NSMutableString *card = [[NSMutableString alloc]init];
        if ([_cardText.text length]!=0 && [_cardText.text length]>4){
            for (int i=0; i<([textField.text length]-4); i++) {
                [card appendString:@"X"];
            }
            for (int i=((int)[_cardText.text length]-4); i<[_cardText.text length]; i++) {
                [card appendString:[NSString stringWithFormat:@"%c",[_cardText.text characterAtIndex:i]]];
            }
            _cardText.text = card;
        }
        
        if (![_cardText.hiddenString isEqualToString:@""]) {
            switch ([[_cardText.hiddenString substringWithRange:NSMakeRange(0, 1)] intValue]) {
                case 4://SET ADDRESS & ZIP MANDATORY
                    cardType = 1;
                    [_addressText setRequired:YES];
                    [_zipText setRequired:YES];
                    //[_addressText setText:@""];
                    //[_zipText setText:@""];
                    break;
                case 5://SET ADDRESS & ZIP MANDATORY
                    cardType = 2;
                    [_addressText setRequired:YES];
                    [_zipText setRequired:YES];
                    //[_addressText setText:@""];
                    //[_zipText setText:@""];
                    break;
                    
                default:
                    cardType = 3;
                    [_addressText setRequired:YES];
                    [_zipText setRequired:YES];
                    break;
            }
        }
        [_cardImageView setImage:[UIImage imageNamed:cardType==1?@"logo_visa":cardType==2?@"logo_mc":@"logo_amex"]];
        
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel_Action:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Reg3", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (IBAction)delete_Action:(id)sender {
    UIActionSheet *actionMsg = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"¿Está seguro de eliminar esta tarjeta?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) destructiveButtonTitle:NSLocalizedString(@"Eliminar tarjeta", nil) otherButtonTitles: nil];
    [actionMsg showInView:self.view];
}



#pragma Mark pootEngine Handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    switch (_type) {
        case updateCardTypeNew:
        case updateCardTypeExisting:
        {
            if (manager == updateManager) {
                NSMutableDictionary *response = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary*)json];
                [self cleanDictionary:response];
                
                if ([response[kIDError] intValue]==0) {
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    if ([_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[_delegate respondsToSelector:@selector(updateCardResponse:)]) {
                        [_delegate updateCardResponse:nil];
                    }
                    
                    if (_type == updateCardTypeExisting) {
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alertMsg setTag:200];
                        [alertMsg show];
                    } else {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
            }
        }
            break;
            
        default:
        {
            
            if (manager == updateManager) {
                NSDictionary *response = (NSDictionary*)json;
                
                if ([response[@"resultado"] intValue]==1) {
                    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                    
                    [userInfo setObject:_cardText.text forKey:kCardNumber];
                    [userInfo setObject:_validText.text forKey:kCardValid];
                    [userInfo setObject:[NSNumber numberWithInt:cardType] forKey:kCardIDType];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:(NSString*)kUserDetailsKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[@"mensaje"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alertMsg setTag:200];
                    [alertMsg show];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[@"mensaje"] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alertMsg show];
                }
            }
        }
            break;
    }
}

#pragma Mark UIAlertView handler

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 200:
        {
            if ([_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[_delegate respondsToSelector:@selector(updateCardResponse:)]) {
                [_delegate updateCardResponse:nil];
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        default:
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self dismissViewControllerAnimated:YES completion:^{
                if ([_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[_delegate respondsToSelector:@selector(removeCardWithID:)]) {
                    [_delegate removeCardWithID:_cardToUpdateID];
                }
            }];
        }
            break;
        default:
            break;
    }
}
/*
-(BOOL)textFieldShouldBeginEditing:(UITextField_Validations *)textField {
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: _dataTable];
        NSIndexPath *indexPath = [_dataTable indexPathForRowAtPoint:buttonPosition];
        
        [_dataTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:_dataTable];
    CGPoint contentOffset = _dataTable.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    [_dataTable setContentOffset:contentOffset animated:YES];
    return [self subTextFieldShouldBeginEditing:textField];
}


-(BOOL)textFieldShouldEndEditing:(UITextField_Validations *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: _dataTable];
        NSIndexPath *indexPath = [_dataTable indexPathForRowAtPoint:buttonPosition];
        
        [_dataTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}*/

@end
