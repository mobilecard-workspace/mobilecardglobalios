//
//  statement_loockfell.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/17/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "statement_loockfell.h"

@interface statement_loockfell (){
    
    NSMutableArray *groupheader;
    
    NSMutableArray *statements;
    NSMutableArray *listCard;
    NSMutableArray *ObjectStatement;
    
    pootEngine *transactionManager;
    NSMutableArray *currentData;
    
    NSDictionary *allTransactionData;
    statementModel *SelectedItem;
}
@end

@implementation statement_loockfell

- (void)viewDidLoad {
    [super viewDidLoad];
    
    !transactionManager?transactionManager = [[pootEngine alloc]init]:nil;
    [transactionManager setDelegate:self];
    [transactionManager setShowComments:developing];
    
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    [transactionManager startRequestWithURL:[NSString stringWithFormat:@"%@/1/%@/transactions?idUsuario=%@", walletManagementURL, NSLocalizedString(@"lang", nil), userInfo[kUserIDKey]]];
    
    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    
    
    tableviewStatements.delegate = self;
    tableviewStatements.dataSource = self;
    
    groupheader= [[NSMutableArray alloc] init];
    statements= [[NSMutableArray alloc] init];
    listCard= [[NSMutableArray alloc] init];
    ObjectStatement= [[NSMutableArray alloc] init];
    
    
    
//    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, tableviewStatements.bounds.size.height/2+self.view.bounds.size.height/3,tableviewStatements.bounds.size.width, 150)];
//    footerView.backgroundColor =[UIColor whiteColor];
//    // [footerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"profile"]]];
//
//    _homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
//    [_homeButton addTarget:self action:@selector(back_Action:) forControlEvents:UIControlEventTouchUpInside];
//    _walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
//
//    _favoritesButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
//   // [_favoritesButton addTarget:self action:@selector(showFavorites:) forControlEvents:UIControlEventTouchUpInside];
//
//    _myMCButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_history", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
//   // [_myMCButton addTarget:self action:@selector(showHistory:) forControlEvents:UIControlEventTouchUpInside];
//
//    [_homeButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*1-40+10, 20, 58, 60)];
//    [_walletButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*2.3-40+10, 20, 58, 60)];
//    [_favoritesButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*3.6-40+10, 20, 60, 60)];
//    [_myMCButton setFrame:CGRectMake(((footerView.frame.size.width-20)/6)*5-40+10, 20, 60, 60)];
//
//    [footerView addSubview:_walletButton];
//    [footerView addSubview:_homeButton];
//    [footerView addSubview:_favoritesButton];
//    [footerView addSubview:_myMCButton];
//    [self.view addSubview:footerView];
    
      [self setupMenu];
    
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [groupheader count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [statements[section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 73;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"data";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    if(cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    statementModel *Itemstatement =  [statements[indexPath.section] objectAtIndex:indexPath.row];
    //   cardStatementModel *ItemCardtatement =[Itemstatement.dataCard objectAtIndex:indexPath.row];
    UIImageView *imageicon = [cell viewWithTag:1];
    if (Itemstatement.status == 1) {
        [imageicon setImage:[UIImage imageNamed:@"icon_exitoso"]];
    }else{
        [imageicon setImage:[UIImage imageNamed:@"icon_rechazado"]];
    }
    
    UILabel *lbl_name = [cell viewWithTag:2];
    [lbl_name setText:Itemstatement.ticket];
    
    UILabel *lbl_mount = [cell viewWithTag:3];
    [lbl_mount setText:[NSString stringWithFormat:@"$%ld",Itemstatement.importe]];
    //[lbl_mount setText:[@(Itemstatement.importe) stringValue]];
    
    UILabel *lbl_date = [cell viewWithTag:4];
    [lbl_date setText:Itemstatement.date];
    
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return groupheader[section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,tableView.bounds.size.width, 40)];
    UITableViewCell *cell;
    @try
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"header"];
        UITextField *text_title = (UITextField *)[cell viewWithTag:1];
        [text_title setText:groupheader[section]];
        [headerView addSubview:cell];
        
        return cell;
    } @catch (NSException *exception)
    {
        NSLog( @"NameException: %@", exception.name);
        NSLog( @"ReasonException: %@", exception.reason );
    }
    @finally
    {
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try
    {
        
        SelectedItem=  [statements[indexPath.section] objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"detail_statement" sender:self];
        
    } @catch (NSException *exception)
    {
        NSLog( @"NameException: %@", exception.name);
        NSLog( @"ReasonException: %@", exception.reason );
    }
    @finally
    {
        
    }
}
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == transactionManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            
            NSArray *data = [json objectForKey:@"data"];
            for (NSDictionary *jsonObject in data) {
                
                NSString *mes =[jsonObject objectForKey:@"mes"];
                NSArray *transacciones = [jsonObject objectForKey:@"transacciones"];
                
                [groupheader addObject:mes];
                
                for (NSDictionary *jsonObjectCard in transacciones) {
                    
                    
                    statementModel *ItemDemostatement =[[statementModel alloc] init];
                    ItemDemostatement.idStatement =[[jsonObjectCard objectForKey:@"id"] integerValue];
                    ItemDemostatement.date =[jsonObjectCard objectForKey:@"date"];
                    ItemDemostatement.importe =[[jsonObjectCard objectForKey:@"importe"] integerValue];
                    ItemDemostatement.status =[[jsonObjectCard objectForKey:@"status"] integerValue];
                    ItemDemostatement.ticket =[jsonObjectCard objectForKey:@"ticket"];
                    ItemDemostatement.total =[[jsonObjectCard objectForKey:@"total"] integerValue];
                    ItemDemostatement.title_month=[jsonObject objectForKey:@"mes"];
                    
                    NSDictionary *ObjectCard = [jsonObjectCard objectForKey:@"card"];
                    cardStatementModel *ItemCardtatement =[[cardStatementModel alloc] init];
                    ItemCardtatement.balance =0;
                    ItemCardtatement.clave =[ObjectCard objectForKey:@"clabe"];
                    ItemCardtatement.codigo =[ObjectCard objectForKey:@"codigo"];
                    ItemCardtatement.cpAmex =[ObjectCard objectForKey:@"cpAmex"];
                    ItemCardtatement.determinada =[[ObjectCard objectForKey:@"determinada"] boolValue];
                    ItemCardtatement.domAmex =[ObjectCard objectForKey:@"domAmex"];
                    ItemCardtatement.idTarjeta =[[jsonObjectCard objectForKey:@"idTarjeta"] integerValue];
                    ItemCardtatement.img_full =[ObjectCard objectForKey:@"img_full"];
                    ItemCardtatement.img_short =[ObjectCard objectForKey:@"img_short"];
                    ItemCardtatement.mobilecard =[[ObjectCard objectForKey:@"mobilecard"] boolValue];
                    ItemCardtatement.nombre =[ObjectCard objectForKey:@"nombre"];
                    ItemCardtatement.pan =[ObjectCard objectForKey:@"pan"];
                    ItemCardtatement.phoneNumberActivation =[ObjectCard objectForKey:@"phoneNumberActivation"];
                    ItemCardtatement.previvale =[[ObjectCard objectForKey:@"previvale"] boolValue];
                    ItemCardtatement.tipo =[ObjectCard objectForKey:@"tipo"];
                    ItemCardtatement.tipoTarjeta =[ObjectCard objectForKey:@"tipoTarjeta"];
                    ItemCardtatement.vigencia =[ObjectCard objectForKey:@"vigencia"];
                    
                    [listCard addObject:ItemCardtatement];
                    ItemDemostatement.dataCard =listCard;
                    
                    [ObjectStatement addObject:ItemDemostatement];
                    [statements addObject:ObjectStatement];
                }
            }
            [tableviewStatements reloadData];
        }
    }
}
- (void) setupMenu {
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height/2+self.view.bounds.size.height/4.5, self.view.bounds.size.width, 120)];
    
    [footerView setBackgroundColor:[UIColor whiteColor]];
    UIButton * homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * walletButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * favoritesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton * myMCButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIStackView *stackView = [[UIStackView alloc] init];
    
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    switch ([userData[kUserIdCountry] intValue]) {
        case 1:  case 2: case 3:  //MEXICO //COLOMBIA //USA
            [homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
            [homeButton setContentMode:UIViewContentModeScaleAspectFill];
            [homeButton addTarget:self action:@selector(back_Action:) forControlEvents:UIControlEventTouchUpInside];
            [homeButton.heightAnchor constraintEqualToConstant:75].active = true;
            [homeButton.widthAnchor constraintEqualToConstant:75].active = true;
            
            
            [walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            [walletButton setContentMode:UIViewContentModeScaleAspectFill];
            [walletButton addTarget:self action:@selector(showWallet:) forControlEvents:UIControlEventTouchUpInside];
            [walletButton.heightAnchor constraintEqualToConstant:75].active = true;
            [walletButton.widthAnchor constraintEqualToConstant:75].active = true;
            
            [favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            [favoritesButton setContentMode:UIViewContentModeScaleAspectFill];
            [favoritesButton addTarget:self action:@selector(showFavorites:) forControlEvents:UIControlEventTouchUpInside];
            [favoritesButton.heightAnchor constraintEqualToConstant:75].active = true;
            [favoritesButton.widthAnchor constraintEqualToConstant:75].active = true;
            
            [myMCButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_history", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            [myMCButton setContentMode:UIViewContentModeScaleAspectFill];
          //  [myMCButton addTarget:self action:@selector(openMyMC_Action:) forControlEvents:UIControlEventTouchUpInside];
            [myMCButton.heightAnchor constraintEqualToConstant:75].active = true;
            [myMCButton.widthAnchor constraintEqualToConstant:75].active = true;
            
            
            //Stack View
            //stackView.axis = UILayoutConstraintAxisVertical;
            stackView.axis = UILayoutConstraintAxisHorizontal;
            // stackView.distribution = UIStackViewDistributionEqualSpacing;
            stackView.distribution = UIStackViewDistributionFillEqually;
            // stackView.alignment = UIStackViewAlignmentCenter;
            stackView.alignment = UIStackViewAlignmentFill;
            stackView.spacing = 8;
            
            [stackView addArrangedSubview:homeButton];
            [stackView addArrangedSubview:walletButton];
            [stackView addArrangedSubview:favoritesButton];
            [stackView addArrangedSubview:myMCButton];
            
            stackView.translatesAutoresizingMaskIntoConstraints = false;
            [footerView addSubview:stackView];
            
            [stackView.centerXAnchor constraintEqualToAnchor:footerView.centerXAnchor].active = true;
            [stackView.centerYAnchor constraintEqualToAnchor:footerView.centerYAnchor].active = true;
            [self.view addSubview:footerView];
            
            break;
            
            //
            //            [homeButton setImage:[UIImage imageNamed:@"Button_home"] forState:UIControlStateNormal];
            //            [homeButton setContentMode:UIViewContentModeScaleAspectFill];
            //            [homeButton.heightAnchor constraintEqualToConstant:75].active = true;
            //            [homeButton.widthAnchor constraintEqualToConstant:75].active = true;
            //
            //
            //            [walletButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_wallet_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            //            [walletButton setContentMode:UIViewContentModeScaleAspectFill];
            //            [walletButton addTarget:self action:@selector(showWallet) forControlEvents:UIControlEventTouchUpInside];
            //            [walletButton.heightAnchor constraintEqualToConstant:75].active = true;
            //            [walletButton.widthAnchor constraintEqualToConstant:75].active = true;
            //
            //            [favoritesButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Button_favorites_%@", NSLocalizedString(@"lang", nil)]] forState:UIControlStateNormal];
            //            [favoritesButton setContentMode:UIViewContentModeScaleAspectFill];
            //            [favoritesButton addTarget:self action:@selector(startFavorites) forControlEvents:UIControlEventTouchUpInside];
            //            [favoritesButton.heightAnchor constraintEqualToConstant:75].active = true;
            //            [favoritesButton.widthAnchor constraintEqualToConstant:75].active = true;
            //
            //
            //            //Stack View
            //            //stackView.axis = UILayoutConstraintAxisVertical;
            //            stackView.axis = UILayoutConstraintAxisHorizontal;
            //            // stackView.distribution = UIStackViewDistributionEqualSpacing;
            //            stackView.distribution = UIStackViewDistributionFillEqually;
            //            // stackView.alignment = UIStackViewAlignmentCenter;
            //            stackView.alignment = UIStackViewAlignmentFill;
            //            stackView.spacing = 8;
            //
            //            [stackView addArrangedSubview:homeButton];
            //            [stackView addArrangedSubview:walletButton];
            //            [stackView addArrangedSubview:favoritesButton];
            //
            //            stackView.translatesAutoresizingMaskIntoConstraints = false;
            //            [footerView addSubview:stackView];
            //
            //            [stackView.centerXAnchor constraintEqualToAnchor:footerView.centerXAnchor].active = true;
            //            [stackView.centerYAnchor constraintEqualToAnchor:footerView.centerYAnchor].active = true;
            //            [self.view addSubview:footerView];
            //            break;
    }
    
    
}
- (void) showFavorites:(id)sender
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *nextView = [nextStory instantiateViewControllerWithIdentifier:@"Favorites"];
    [self.navigationController pushViewController:nextView animated:YES];
}
- (void) showWallet:(id)sender
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    
    Wallet_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"wallet_view"];
    
    [self.navigationController pushViewController:nextView animated:YES];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"detail_statement"]){
        DetailsStatement *next = (DetailsStatement *)[segue destinationViewController];
        [next setItem:SelectedItem];
    }
}
@end
