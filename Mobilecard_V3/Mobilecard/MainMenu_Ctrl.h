//
//  MainMenu_Ctrl.h
//  Mobilecard
//
//  Created by David Poot on 10/26/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonController.h"
#import "MCCard_Ctrl.h"
#import "sender_Ctrl.h"
#import "order_Ctrl.h"
#import "myMC_Step1.h"
#import "MXTransfers_Step1.h"
#import "country_Selection.h"
#import "Ingo_Step10.h"
#import "Register_Step30.h"
//#import <ShiftSDK/ShiftSDK-Swift.h>
#import "shift_Step00.h"
//#import <SwiftToast/SwiftToast-Swift.h>
#import "previvale_Step10.h"

@interface MainMenu_Ctrl : mT_commonController <UITableViewDelegate, UITableViewDataSource, pootEngineDelegate, senderDelegate, cardSelectionDelegate, updateCardDelegate, ingoEnrollmentDelegate, previvaleRegisterProtocol/*, shiftCreationDelegate, ShiftPlatformDelegate*/>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIButton *walletButton;
@property (weak, nonatomic) IBOutlet UIButton *myMCButton;
@property (weak, nonatomic) IBOutlet UIButton *favoritesButton;

- (void) startTransfersModule;
- (void) startMXTransfersModule;
- (void) startTopUp;
- (void) startBillPay;
- (void) startCash;
- (void) startStatement;
- (void) startScanNPay;
- (void) startFavorites;

- (IBAction)openWallet_Action:(id)sender;
@end
