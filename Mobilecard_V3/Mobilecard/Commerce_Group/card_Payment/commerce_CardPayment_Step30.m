//
//  commerce_CardPayment_Step30.m
//  Mobilecard
//
//  Created by David Poot on 9/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "commerce_CardPayment_Step30.h"
#import "commerce_QRPayment_Step30.h"

@interface commerce_CardPayment_Step30 ()
{
    pootEngine *paymentManager;
    
    THMTrustDefender *profile;
    
    NSMutableDictionary *threatMetrixInfo;
    NSDictionary *selectedCardInfo;

    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
}

@end

@implementation commerce_CardPayment_Step30

- (void)viewDidLoad {
    [super viewDidLoad];
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    profile = [THMTrustDefender sharedInstance];
    
    [_signView setLineWidth:2.0];
    _signView.foregroundLineColor = [UIColor colorWithRed:0.204 green:0.596 blue:0.859 alpha:1.000];
    
  //  [self addShadowToView:_continue_Button];
  //  [self addShadowToView:_clear_Button];
}

- (IBAction)clearSignature_Action:(id)sender {
    [_signView clear];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender{
    if (![_signView isSigned]) {
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Para continuar, es necesario introducir tu firma", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        return;
    }
    
    [self performProfileCheck];
}

- (void) performPayment:(NSDictionary*)cardInfo
{
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:_passedData];
    
    NSData *imageData = [_signView signatureData];
    
    [data setObject:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] doubleValue]] forKey:@"lat"];
    [data setObject:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] doubleValue]] forKey:@"lon"];
    
    //NSLog(@"File size is : %.2f KB",(float)imageData.length/1024.0f);
    
    //[data setObject:[[_signView signatureData] base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength] forKey:@"firma"];
    [data setObject:@"43" forKey:@"firma"];
    
    paymentManager = [[pootEngine alloc] init];
    [paymentManager setDelegate:self];
    [paymentManager setShowComments:developing];
    
    Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
    [nextView setPurchaseInfo:[NSMutableDictionary dictionaryWithDictionary:data]];
    [nextView setType:serviceTypeCuantoTeDebo];
    [nextView setThreatMetrixInfo:threatMetrixInfo];
    
    [nextView setDelegate:self];
    
    [self.navigationController pushViewController:nextView animated:YES];
}


- (void)Secure3DResponse:(NSDictionary *)response
{
    developing?NSLog(@"Result -> %@", response):nil;
    [self performSegueWithIdentifier:@"result" sender:response];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"result"]) {
        commerce_QRPayment_Step30 *nextView = [segue destinationViewController];
        
        NSDictionary *response = (NSDictionary*)sender;
        
        [nextView setResultData:response];
    }
}







#pragma Mark TrustDefender Handling

- (void) performProfileCheck
{
    [self lockViewWithMessage:nil];
    
    profile = [THMTrustDefender sharedInstance];
    
    [profile configure:@{
                         THMOrgID:@"c2ggoqh1",
                         THMFingerprintServer:@"h-sdk.online-metrix.net",
                         }];
    
    [profile doProfileRequestWithCallback:^(NSDictionary *result)
     {
         [self unLockView];
         developing?NSLog(@"PROFILE RESULT -> %@", result):nil;
         self->threatMetrixInfo = [NSMutableDictionary dictionaryWithDictionary:result];
         
         THMStatusCode statusCode = [[result valueForKey:THMProfileStatus] integerValue];
         
         if (statusCode == THMStatusCodeOk) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self performPayment:selectedCardInfo];
             });
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:@"%s", statusCode == THMStatusCodeNetworkTimeoutError ? "Timed out"                                                                                                                                                                : statusCode == THMStatusCodeConnectionError     ? "Connection Error"                                                                                                                                                           : statusCode == THMStatusCodeHostNotFoundError   ? "Host not found error"                                                                                                                                                : statusCode == THMStatusCodeInternalError       ? "Internal Error"                                                                                                                                                        : statusCode == THMStatusCodeInterruptedError    ? "Interrupted"                                                                                                                                                           : "other"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [alertMsg dismissViewControllerAnimated:YES completion:nil];
                 }];
                 
                 [alertMsg addAction:ok];
                 
                 [self presentViewController:alertMsg animated:YES completion:nil];
             });
         }
         
         
     }];
}

- (void)PaymentWithThreatMetrixResponse:(NSDictionary *)response
{
    if (response) {
        [self performSegueWithIdentifier:@"result" sender:response];
        
        if ([response[@"code"] intValue] == 0) {
            
        };
    }
}








#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Failed to Get Your Location" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}



@end
