//
//  commerce_CardPayment_Step20.h
//  Mobilecard
//
//  Created by David Poot on 9/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "CardIO.h"
#import "CardIOPaymentViewControllerDelegate.h"

@interface commerce_CardPayment_Step20 : mT_commonTableViewController <CardIOPaymentViewControllerDelegate>

@property (strong, nonatomic) NSDictionary *passedData;

@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cardText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cvvText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *validText;

@property (weak, nonatomic) IBOutlet UITextField_Validations *msiText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
