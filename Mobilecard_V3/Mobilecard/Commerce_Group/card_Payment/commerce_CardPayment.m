//
//  commerce_CardPayment.m
//  Mobilecard
//
//  Created by David Poot on 9/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "commerce_CardPayment.h"
#import "commerce_CardPayment_Step20.h"

@interface commerce_CardPayment ()
{
    NSNumberFormatter *numFormatter;
    pootEngine *feeManager;
}
@end

@implementation commerce_CardPayment

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:_continue_Button];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setCurrencySymbol:@"$"];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    [self updateTotalWithTotal:0.0];
    
    [_amountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_conceptText setUpTextFieldAs:textFieldTypeAddress];
    [_tipText setUpTextFieldAs:textFieldTypeGeneralNumericNoRequired];
    
    [self addValidationTextField:_amountText];
    [self addValidationTextField:_conceptText];
    
    [_tipText setDelegate:self];
    [self addValidationTextFieldsToDelegate];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _amountText) {
        [self updateTotalWithTotal:[[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue]+[[_tipText text] floatValue]];
    }
    
    if (textField == _tipText) {
        [self updateTotalWithTotal:[[textField.text stringByReplacingCharactersInRange:range withString:string] floatValue]+[[_amountText text] floatValue]];
    }
    
    return YES;
}

- (void) updateTotalWithTotal:(float)amount
{
    [_totalLabel setText:[NSString stringWithFormat:@"TOTAL: %@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:amount]]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    feeManager = [[pootEngine alloc] init];
    [feeManager setDelegate:self];
    [feeManager setShowComments:developing];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    [feeManager startRequestWithURL:[NSString stringWithFormat:@"%@/CuantoTeDebo/%d/%d/%@/getComision?idCommerce=%@", simpleServerURL, idApp, [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"] intValue], NSLocalizedString(@"lang", nil), userData[@"idEstablecimiento"]]];
    
    [self lockViewWithMessage:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"step20"]) {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        float comisionfija = [userData[@"comisionFija"] floatValue];
        float comisionPorcentaje = [userData[@"comisionPorcentaje"] floatValue];
        float monto = [_amountText.text floatValue];
        
        float comision = comisionfija + (monto + comisionfija)*comisionPorcentaje;
        
        [data setObject:[NSNumber numberWithFloat:[_amountText.text floatValue]] forKey:@"amount"];
        [data setObject:[NSNumber numberWithFloat:comision] forKey:@"comision"];
        [data setObject:_conceptText.text forKey:@"concept"];
        [data setObject:@"" forKey:@"ct"];
        [data setObject:userData[@"idEstablecimiento"] forKey:@"establecimientoId"];
        [data setObject:@"" forKey:@"firma"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"idBitacora"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"idCard"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"idUser"];
        [data setObject:@"" forKey:@"lat"];
        [data setObject:@"" forKey:@"lon"];
        [data setObject:@"" forKey:@"msi"];
        [data setObject:[_tipText.text length]==0?[NSNumber numberWithInt:0]:[NSNumber numberWithFloat:[_tipText.text floatValue]] forKey:@"propina"];
        [data setObject:userData[@"nombreEstablecimiento"] forKey:@"referenciaNeg"];
        
        [data setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
        [data setObject:@"Apple" forKey:@"modelo"];
        [data setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
        
        [data setObject:@"" forKey:@"tarjeta"];
        [data setObject:@"" forKey:@"vigencia"];
        [data setObject:@"" forKey:@"tipoTarjeta"];
        
        commerce_CardPayment_Step20 *nextView = [segue destinationViewController];
        [nextView setPassedData:data];
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == feeManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue]==0) {
            
            NSMutableDictionary *userData = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
            
            [userData setObject:response[@"comision_fija"] forKey:@"comisionFija"];
            [userData setObject:response[@"comision_porcentaje"] forKey:@"comisionPorcentaje"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithDictionary:userData] forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"step20" sender:nil];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
        
    }
}

@end
