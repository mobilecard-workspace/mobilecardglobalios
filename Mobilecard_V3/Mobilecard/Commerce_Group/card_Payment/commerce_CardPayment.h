//
//  commerce_CardPayment.h
//  Mobilecard
//
//  Created by David Poot on 9/11/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface commerce_CardPayment : mT_commonTableViewController

@property (weak, nonatomic) IBOutlet UITextField_Validations *amountText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *tipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *conceptText;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
