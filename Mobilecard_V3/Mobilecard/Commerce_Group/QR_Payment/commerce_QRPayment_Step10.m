//
//  commerce_QRPayment_Step10.m
//  Mobilecard
//
//  Created by David Poot on 9/13/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "commerce_QRPayment_Step10.h"
#import "commerce_QRPayment_Step20.h"

@interface commerce_QRPayment_Step10 ()
{
    pootEngine *feeManager;
}
@end

@implementation commerce_QRPayment_Step10

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:_continue_Button];
    
    [_amountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_tipText setUpTextFieldAs:textFieldTypeGeneralNumericNoRequired];
    [_conceptText setUpTextFieldAs:textFieldTypeAddress];
    
    [self addValidationTextField:_amountText];
    [self addValidationTextField:_tipText];
    [self addValidationTextField:_conceptText];

    [self addValidationTextFieldsToDelegate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continue_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    feeManager = [[pootEngine alloc] init];
    [feeManager setDelegate:self];
    [feeManager setShowComments:developing];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    [feeManager startRequestWithURL:[NSString stringWithFormat:@"%@/CuantoTeDebo/%d/%d/%@/getComision?idCommerce=%@", simpleServerURL, idApp, [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"] intValue], NSLocalizedString(@"lang", nil), userData[@"idEstablecimiento"]]];
    
    [self lockViewWithMessage:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step20"]) {
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        float comisionfija = [userData[@"comisionFija"] floatValue];
        float comisionPorcentaje = [userData[@"comisionPorcentaje"] floatValue];
        float monto = [_amountText.text floatValue];
        
        float comision = comisionfija + (monto + comisionfija)*comisionPorcentaje;
        
        [data setObject:[NSNumber numberWithFloat:comision] forKey:@"comision"];
        [data setObject:_amountText.text forKey:@"amount"];
        [data setObject:[_tipText.text length]==0?@"0":_tipText.text forKey:@"propina"];
        [data setObject:_conceptText.text forKey:@"concept"];
        [data setObject:userData[@"nombreEstablecimiento"] forKey:@"referenciaNeg"];
        [data setObject:userData[@"idEstablecimiento"] forKey:@"establecimientoId"];
        
        commerce_QRPayment_Step20 *nextView = [segue destinationViewController];
        [nextView setPassedData:data];
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == feeManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue]==0) {
            
            NSMutableDictionary *userData = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
            
            [userData setObject:response[@"comision_fija"] forKey:@"comisionFija"];
            [userData setObject:response[@"comision_porcentaje"] forKey:@"comisionPorcentaje"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionaryWithDictionary:userData] forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"step20" sender:nil];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
        
    }
}

@end
