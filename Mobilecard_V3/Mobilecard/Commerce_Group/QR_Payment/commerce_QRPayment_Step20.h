//
//  commerce_QRPayment_Step20.h
//  Mobilecard
//
//  Created by David Poot on 9/13/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface commerce_QRPayment_Step20 : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *passedData;

@property (weak, nonatomic) IBOutlet UILabel *idNumberLabel;

@property (weak, nonatomic) IBOutlet UIImageView *QRImageView;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end
