//
//  commerce_QRPayment_Step30.h
//  Mobilecard
//
//  Created by David Poot on 9/13/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface commerce_QRPayment_Step30 : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *resultData;

@property (weak, nonatomic) IBOutlet UIView *result;
@property (weak, nonatomic) IBOutlet UIImageView *resultIcon;
@property (weak, nonatomic) IBOutlet UILabel *resultTitleText;
@property (weak, nonatomic) IBOutlet UITextView *resultDetailsText;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@property (weak, nonatomic) IBOutlet UIButton *showQR_Button;


@end
