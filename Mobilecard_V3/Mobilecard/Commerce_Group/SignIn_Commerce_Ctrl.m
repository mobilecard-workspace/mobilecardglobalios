//
//  SignIn_Commerce_Ctrl.m
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "SignIn_Commerce_Ctrl.h"
#import "Register_Commerce_Step10.h"
#import <Mobilecard-Swift.h>

@interface SignIn_Commerce_Ctrl ()
{
    pootEngine *loginManager;
    pootEngine *changeManager;
    
    NSDictionary *preliminaryUserData;
}
@end

@implementation SignIn_Commerce_Ctrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:self.navigationController.navigationBar];
  //  [self addShadowToView:_signIn_Button];
    
    [_commerceText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_passText setUpTextFieldAs:textFieldTypePassword];
    
    //[self addValidationTextField:_commerceText];
    [self addValidationTextField:_emailText];
    [self addValidationTextField:_passText];
    
    [self addValidationTextFieldsToDelegate];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey] && [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserPassword]) {
        [self performSegueWithIdentifier:@"main_menu" sender:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewDidAppear:(BOOL)animated{
    if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"SessionSMS_To_Menu"] isEqualToString:@"1"]){
        [self startSession_Action:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"SessionSMS_To_Menu"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)register_Action:(id)sender {
    
   
    // si es peru
    if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"codigo"] isEqualToString:@"PE"]){
        
        DataUser.COUNTRY = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"codigo"];
        
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"PEregister_Commerce" bundle:nil];
        UINavigationController *nav = [nextStory instantiateInitialViewController];
        PERegisterStep1ViewController *nextView = [nav childViewControllers].firstObject;
        
        
        
        [self presentViewController:nav animated:YES completion:nil];
        
        
    }else{
    
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register_Commerce" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Register_Commerce_Step10 *nextView = [nav childViewControllers].firstObject;
    
    
    
    [self presentViewController:nav animated:YES completion:nil];
    }
}

- (IBAction)startSession_Action:(id)sender {
    [self dismissAllTextFields];
    
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    //NEED TO UPDATE LOGIN
    loginManager = [[pootEngine alloc] init];
    [loginManager setDelegate:self];
    [loginManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:_emailText.text forKey:@"usrLoginOrEmail"];
    [params setObject:[self encryptString:_passText.text] forKey:kUserPassword];
    //MUST HAVE PARAMETERS
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"manufacturer"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
    [params setObject:@"iOS" forKey:@"platform"];
    [params setObject:@"NEGOCIO" forKey:@"tipoUsuario"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [loginManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@/%@%@", userManagementURL, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], (NSLocalizedString(@"lang", nil)), @"/user/login"] withPost:JSONString];
    
    [self lockViewWithMessage:NSLocalizedString(@"Iniciando sesión...", nil)];
}

- (IBAction)resetPassword_Action:(id)sender {
    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Recuperar contraseña", nil) message:NSLocalizedString(@"Escribe tu nombre de usuario o correo electrónico en el campo para recuperar tu contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Recuperar contraseña", nil), nil];
    [alertMsg setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alertMsg setTag:100];
    [alertMsg show];
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == loginManager) {
        NSDictionary *response = (NSDictionary*)json;
        response = [self cleanDictionary:[NSMutableDictionary dictionaryWithDictionary:response]];
        
        if ([response[kIDError] intValue] == 0) {
            switch ([response[kIDStatus] intValue]) {
                case 98:
                {
                    preliminaryUserData = response;
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cambio de contraseña", nil) message:NSLocalizedString(@"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancelar", nil) otherButtonTitles:NSLocalizedString(@"Cambiar contraseña", nil), nil];
                    [alertMsg setTag:3031];
                    [alertMsg setAlertViewStyle:UIAlertViewStyleSecureTextInput];
                    [alertMsg show];
                    return;
                }
                    break;
                    
                default:
                {
                    [[NSUserDefaults standardUserDefaults] setObject:response forKey:(NSString*)kUserDetailsKey];
                    [[NSUserDefaults standardUserDefaults] setObject:_passText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kIDStatus] forKey:(NSString*)kUserStatus];
                    [[NSUserDefaults standardUserDefaults] setObject:@"NEGOCIO" forKey:(NSString*)kUserType];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    //[response deleteFromKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                    //[response storeToKeychainWithKey:kKeychainKey andGroup:kKeychainGroup];
                    
                    [self performSegueWithIdentifier:@"main_menu" sender:nil];
                }
                    break;
            }
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
    
    if (changeManager == manager) {
        NSDictionary *response = (NSDictionary*)json;
        switch (changeManager.tag) {
            case 3031:
            {
                if ([response[kIDError] intValue] == 0) {
                    
                    [preliminaryUserData setValue:@"1" forKey:@"idUsrStatus"];
                    [[NSUserDefaults standardUserDefaults] setObject:_validationText.text forKey:(NSString*)kUserPassword];
                    [[NSUserDefaults standardUserDefaults] setObject:preliminaryUserData forKey:(NSString*)kUserDetailsKey];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                    
                    [self performSegueWithIdentifier:@"main_menu" sender:nil];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
            }
                break;
            case 100:
            {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[(NSString*)kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark register Delegate
/*
- (void)registryShallLaunchMyMCWithUserData:(NSDictionary *)userdata
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
    UINavigationController *nav = [storyboard instantiateInitialViewController];
    MCCard_Ctrl *nextView = [nav childViewControllers].firstObject;
    [nextView setUserData:userdata];
    
    [super setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [super presentViewController:nav animated:YES completion:nil];
}
*/

#pragma Mark UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (!(buttonIndex == 0)) {
        switch (alertView.tag) {
            case 3031: //new password request
            {
                [_validationText setUpTextFieldAs:textFieldTypePassword];
                [_validationText setText:[alertView textFieldAtIndex:0].text];
                
                if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                    changeManager = [[pootEngine alloc] init];
                    [changeManager setDelegate:self];
                    [changeManager setShowComments:developing];
                    [changeManager setTag:alertView.tag];
                    
                    [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@/commerce/passUpdate?userAdmin=%d&newPass=%@", userManagementURL, NSLocalizedString(@"lang", nil), [preliminaryUserData[@"idEstablecimiento"] intValue], [self encryptString:[_passText text]]]];
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                }
                
            }
                break;
                
            case 100: //reset password
            {
                [_validationText setUpTextFieldAs:textFieldTypeUserName];
                [_validationText setText:[alertView textFieldAtIndex:0].text];
                
                if ([self validateFieldsInArray:[NSArray arrayWithObject:_validationText]]) {
                    changeManager = [[pootEngine alloc] init];
                    [changeManager setDelegate:self];
                    [changeManager setShowComments:developing];
                    [changeManager setTag:alertView.tag];
                    
                    
                    NSString *post = nil;
                    post = [NSString stringWithFormat:@"userAdmin=%@", _validationText.text];
                    
                    [changeManager startRequestWithURL:[NSString stringWithFormat:@"%@%@%@", userManagementURL, NSLocalizedString(@"lang", nil), @"/commerce/passReset"] withPost:post];
                    
                    [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                }
            }
            default:
                break;
        }
    }
}


- (NSString*) convertToURLCompatible: (NSString*)stringy
{
    NSString * escapedUrlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)stringy,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]ÁáÉéÍíÓóÚúñ. %",kCFStringEncodingUTF8));
    
    return escapedUrlString;
}
@end
