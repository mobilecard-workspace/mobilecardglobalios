//
//  MainMenu_Commerce.m
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "MainMenu_Commerce.h"
#import "AppDelegate.h"
#import "InsetCell.h"
#import "MCPanel_Ctrl.h"

@interface MainMenu_Commerce ()
{
    NSMutableArray *menuItems;
    pootEngine *verifyManager;
    
    NSDictionary *verifyInfo;
    
    BOOL myMobileCard_Show;
}
@end

@implementation MainMenu_Commerce

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self addShadowToView:self.navigationController.navigationBar];
    
    menuItems = [[NSMutableArray alloc] init];
    
    [self updateMenu];
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [self previvaleVerifyAndShowMenu:NO];
    
    myMobileCard_Show = NO;
}

- (void) previvaleVerifyAndShowMenu:(BOOL)showMenu
{
    NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    verifyManager = [[pootEngine alloc] init];
    [verifyManager setDelegate:self];
    [verifyManager setShowComments:developing];
    
    verifyManager.tag = showMenu;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:userDetails[@"idEstablecimiento"] forKey:@"id"];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    [verifyManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@?id=%@", WSPrevivaleVerify, userDetails[@"idEstablecimiento"]] withPost:JSONString];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)updateMenu
{
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry];
    [menuItems removeAllObjects];
    switch ([userData[@"id"] intValue]) {
        case 1: //MEXICO
        case 3: //USA
        {
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Header", kIdentifierKey, @"70", kHeightKey, nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_card", kIdentifierKey, @"54", kHeightKey,nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_qrcode", kIdentifierKey, @"54", kHeightKey,nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
            [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_scan", kIdentifierKey, @"54", kHeightKey,nil]];
            if (myMobileCard_Show) {
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"space", kIdentifierKey, @"15", kHeightKey,nil]];
                [menuItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"side_mymc", kIdentifierKey, @"54", kHeightKey,nil]];
            }
        }
            break;
        default:
            break;
    }
    
    [_menuTableView reloadData];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateMenu];
    
    [super viewWillAppear:YES];
}


#pragma Mark UITableView Implementation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItems count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [menuItems[indexPath.row][kHeightKey] floatValue];
}

- (InsetCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InsetCell *cell = [tableView dequeueReusableCellWithIdentifier:menuItems[indexPath.row][kIdentifierKey]];
    
   // [self addShadowToView:cell];
    
    if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"Header"]) {
        UILabel *label = cell.contentView.subviews[0];
        //NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];;
        [label setText:[NSString stringWithFormat:NSLocalizedString(@"SELECCIONA COMO DESEAS COBRAR", nil)]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView  deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self verifyStatus]) {
        if ([menuItems[indexPath.row][kIdentifierKey] isEqualToString:@"side_mymc"]) {
            [self previvaleVerifyAndShowMenu:YES];
        } else {
            [self performSegueWithIdentifier:menuItems[indexPath.row][kIdentifierKey] sender:nil];
        }
    }
}

- (void)startPrevivale
{
    ////////////////////  Validación si no tiene Card que lo mande acrear una nueva  Raul Mendez ///////////////////////////
    if (verifyInfo[@"card"] == (id)[NSNull null]) {
        if (!verifyInfo[@"card"]||[verifyInfo[@"card"][@"pan"] length]==0) {
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"previvale" bundle:nil];
            UINavigationController *nav;
            
            nav = [nextStory instantiateInitialViewController];
            
            previvale_Step10 *nextView = [[nav childViewControllers] firstObject];
            [nextView setDelegate:self];
            [[self navigationController] presentViewController:nav animated:YES completion:nil];
        } else {
            [self previvaleRegisterResponse:verifyInfo];
        }
    } else{
        /// creacion de tarjeta
        if (!verifyInfo[@"card"]||[verifyInfo[@"card"][@"pan"] length]==0) {
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"previvale" bundle:nil];
            UINavigationController *nav;
            
            nav = [nextStory instantiateInitialViewController];
            
            previvale_Step10 *nextView = [[nav childViewControllers] firstObject];
            [nextView setDelegate:self];
            [[self navigationController] presentViewController:nav animated:YES completion:nil];
        } else {
            [self previvaleRegisterResponse:verifyInfo];
        }
    }
}

- (BOOL)verifyStatus
{
    if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 1)) {
        //OK!
        return YES;
    }
    
    if (([[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idUsrStatus"] intValue] == 100)) {
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes verificar tu cuenta. ¿Deseas verificar tu cuenta?", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Sí" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
            UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
            Register_Step30 *nextView = [[nav viewControllers] firstObject];
            
            [nextView setUserData:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
            [nextView setUserID:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey][@"idEstablecimiento"]];
            
            [self.navigationController presentViewController:nav animated:YES completion:nil];
            
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        }];
        
        [alertMsg addAction:ok];
        [alertMsg addAction:cancel];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        
        return NO;
    } else {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Tu cuenta no está verificada", nil) message:NSLocalizedString(@"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [alertMsg setTag:99];
        [alertMsg show];
        return NO;
    }
    
    return YES;
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == verifyManager) {
        NSMutableDictionary *response = (NSMutableDictionary*)json;
        response = [self cleanDictionary:response];
        
        verifyInfo = [NSDictionary dictionaryWithDictionary:response];
        
        if ([verifyInfo[kIDError] intValue]==0) {
            if ([verifyInfo[@"accountId"] length]==0) {
                myMobileCard_Show = YES;
                if (verifyManager.tag == 1) {
                    [self startPrevivale];
                }
            } else {
                myMobileCard_Show = NO;
            }
            [self updateMenu];
        } else if ([verifyInfo[kIDError] intValue]==200) {
            myMobileCard_Show = YES;
            if (verifyManager.tag == 1) {
                [self startPrevivale];
            }
            [self updateMenu];
        } else {
            myMobileCard_Show = NO;
            [self updateMenu];
            
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
             
        }
    }
}

#pragma mark Previvale Handler

- (void)previvaleRegisterResponse:(NSDictionary *)response
{
    if (response) {
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
        MCPanel_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"mcpanel_view"];
        [nextView setMcCardInfo:response[@"card"]];
        
        [[self navigationController] pushViewController:nextView animated:YES];
    }
}

@end
