//
//  MainMenu_Commerce.h
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonController.h"
#import "Register_Step30.h"
#import "previvale_Step10.h"


@interface MainMenu_Commerce : mT_commonController <UITableViewDelegate, previvaleRegisterProtocol>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@end
