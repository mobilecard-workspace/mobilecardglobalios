//
//  statement_Commerce.m
//  Mobilecard
//
//  Created by David Poot on 11/15/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "statement_Commerce.h"

@interface statement_Commerce ()
{
    NSMutableArray *billArray;
    
    pootEngine *billManager;
    
    NSNumberFormatter *numFormatter;
}
@end

@implementation statement_Commerce

- (void)viewDidLoad {
    [super viewDidLoad];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    billArray = [[NSMutableArray alloc] init];
    
    billManager = [[pootEngine alloc] init];
    [billManager setDelegate:self];
    [billManager setShowComments:developing];
    
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    [billManager startWithoutPostRequestWithURL:[NSString stringWithFormat:@"%@/CuantoTeDebo/%d/%@/%@/%@/cuentas", simpleServerURL, idApp, [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserIdCountry][@"id"], NSLocalizedString(@"lang", nil), userData[@"idEstablecimiento"]]];
    
    [self lockViewWithMessage:nil];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareArray:(NSArray*)array
{
    [billArray removeAllObjects];
    
    [billArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"70", kHeightKey, @"Header", kIdentifierKey, nil]];
    
    for (NSDictionary *element in array) { //145
        [billArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:element, kDetailsKey, @"145", kHeightKey, @"data", kIdentifierKey, 0, kIDStatus, nil]];
        [billArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"20", kHeightKey, @"space", kIdentifierKey, nil]];
    }
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [billArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [billArray[indexPath.row][kHeightKey] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = billArray[indexPath.row][kIdentifierKey];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if ([identifier isEqualToString:@"data"]) {
        for (id element in cell.contentView.subviews) {
            if ([element isKindOfClass:[UIView class]]) {
                UIView *view = element;
                if ([billArray[indexPath.row][kHeightKey] floatValue]<270 && view.tag == 7) {
                    [view setHidden:YES];
                } else {
                    [view setHidden:NO];
                    
                    for (id innerElement in view.subviews) {
                        if ([innerElement isKindOfClass:[UILabel class]]) {
                            UILabel *label = innerElement;
                            
                            switch (label.tag) {
                                case 4:
                                    [label setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[billArray[indexPath.row][kDetailsKey][@"importe"] floatValue]]]];
                                    break;
                                case 5:
                                    [label setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[billArray[indexPath.row][kDetailsKey][@"propina"] floatValue]]]];
                                    break;
                                case 6:
                                    [label setText:[numFormatter stringFromNumber:[NSNumber numberWithFloat:[billArray[indexPath.row][kDetailsKey][@"comision"] floatValue]]]];
                                    break;
                                    
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            
            if ([element isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView = element;
                
                switch (imageView.tag) {
                    case 1:
                        if ([billArray[indexPath.row][kHeightKey] floatValue]<270) {
                            [imageView setImage:[UIImage imageNamed:@"arrow_down"]];
                        } else {
                            [imageView setImage:[UIImage imageNamed:@"arrow_up"]];
                        }
                        break;
                        
                    default:
                        break;
                }
            }
            
            if ([element isKindOfClass:[UILabel class]]) {
                UILabel *label = element;
                
                switch (label.tag) {
                    case 1:
                        [label setText:[NSString stringWithFormat:NSLocalizedString(@"Cuenta pagada (%@)", nil), billArray[indexPath.row][kDetailsKey][@"codigo_aut"]]];
                        break;
                    case 2:
                        [label setText:billArray[indexPath.row][kDetailsKey][@"fecha"]];
                        break;
                    case 3:
                        [label setText:[NSString stringWithFormat:NSLocalizedString(@"TOTAL PAGADO: %@", nil), [numFormatter stringFromNumber:[NSNumber numberWithFloat:[billArray[indexPath.row][kDetailsKey][@"total"] floatValue]]]]];
                        break;
                    case 8:
                        [label setText:billArray[indexPath.row][kDetailsKey][@"referencia_negocio"]];
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *selected = billArray[indexPath.row];
    if ([selected[kHeightKey] intValue] == 145) {
        [selected setObject:@"270" forKey:kHeightKey];
    } else {
        [selected setObject:@"145" forKey:kHeightKey];
    }
    
    [self.tableView reloadData];
}


- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == billManager) {
        NSArray *response = (NSArray*)json;
        
        [self prepareArray:response];
        /*
        if ([response[kIDError] intValue] == 0) {
            [self prepareArray:response];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }*/
    }
}

@end
