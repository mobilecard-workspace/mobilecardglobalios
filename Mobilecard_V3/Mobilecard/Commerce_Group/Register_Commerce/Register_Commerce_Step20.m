//
//  Register_Commerce_Step20.m
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "Register_Commerce_Step20.h"

@interface Register_Commerce_Step20 ()
{
    pootEngine *registerManager;
    
    bool addCard;
    NSDictionary *accountInformation;
    NSDictionary *cardInformation;
    
    bool addIDImage;
    bool addAddressImage;
    
    NSDictionary *userData;
    
    MC_TermsAndConditionsCtrller *termsView;
}
@end

@implementation Register_Commerce_Step20

- (void)viewDidLoad {
    [super viewDidLoad];
    
 //   [self addShadowToView:_addPayment_Button];
 //   [self addShadowToView:_save_Button];
 //   [self addShadowToView:_getAccount_Button];
 //   [self addShadowToView:_activateAccount_Button];
    
    termsView = [[MC_TermsAndConditionsCtrller alloc] initWithNibName:@"MC_TermsAndConditionsCtrller" bundle:nil];
    [termsView setDelegate:self];
    
    for (UIView* view in self.tableView.subviews) {
        [self addShadowToView:view];
    }
    
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_lastNameText setUpTextFieldAs:textFieldTypeLastName];
    [_motherLastNameText setUpTextFieldAs:textFieldTypeLastName];
    
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_businessNameText setUpTextFieldAs:textFieldTypeAddress];
    
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_lastNameText];
    [self addValidationTextField:_motherLastNameText];
    
    [self addValidationTextField:_addressText];
    
    [self addValidationTextFieldsToDelegate];
    
    addIDImage = NO;
    addAddressImage = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)camera_ID_Action:(id)sender {
    UIImagePickerController *idCameraController = [[UIImagePickerController alloc] init];
    [idCameraController setSourceType:UIImagePickerControllerSourceTypeCamera];
    [idCameraController setDelegate:self];
    [idCameraController setAllowsEditing:YES];
    [idCameraController setTitle:@"ID"];
    [self presentViewController:idCameraController animated:YES completion:nil];
}

- (IBAction)camera_Address_Action:(id)sender {
    UIImagePickerController *idCameraController = [[UIImagePickerController alloc] init];
    [idCameraController setSourceType:UIImagePickerControllerSourceTypeCamera];
    [idCameraController setDelegate:self];
    [idCameraController setAllowsEditing:YES];
    [idCameraController setTitle:@"ADDRESS"];
    [self presentViewController:idCameraController animated:YES completion:nil];
}

- (IBAction)getAccount_Action:(id)sender {
    [self performSegueWithIdentifier:@"activateAccount" sender:self];
    /*
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Estamos mejorando esta funcionalidad. No está disponible temporalmente", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
     */
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image=[info objectForKey:UIImagePickerControllerEditedImage];
    if ([picker.title isEqualToString:@"ID"]) {
        [_imageView_ID setImage:image];
        [_imageView_ID setContentMode:UIViewContentModeScaleAspectFit];
        addIDImage = YES;
    } else { //ADDRESS
        [_imageView_Address setImage:image];
        [_imageView_Address setContentMode:UIViewContentModeScaleAspectFit];
        addAddressImage = YES;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Reg2",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void) updateAddButton
{
    if (accountInformation) {
        [_addPayment_Button setTitle:NSLocalizedString(@"ELIMINAR CUENTA", nil) forState:UIControlStateNormal];
    } else {
        [_addPayment_Button setTitle:NSLocalizedString(@"AGREGAR CUENTA", nil) forState:UIControlStateNormal];
    }
}

- (IBAction)register_Action:(id)sender {
    
    for (UITextField_Validations *texty in [self getValidationTextFields]) {
        [texty validateField:0];
        [texty resignFirstResponder];
    }
    
    //Validating all inputs
    if (!_termsSwitch.on) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Para continuar, es necesario aceptar los términos y condiciones", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    if (!addIDImage||!addAddressImage) {
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Es necesario añadir ambas imágenes como comprobantes", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        return;
    }
    
    //Preparing data
    
    registerManager = [[pootEngine alloc] init];
    [registerManager setDelegate:self];
    [registerManager setShowComments:developing];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:_data];
    
    [params setObject:_nameText.text forKey:@"nombre"];
    [params setObject:_lastNameText.text forKey:@"paterno"];
    [params setObject:_motherLastNameText.text forKey:@"materno"];
    
    [params setObject:_businessNameText.text forKey:@"razon_social"];
    [params setObject:_addressText.text forKey:@"direccion_establecimiento"];
    
    if (!accountInformation) { //NO REGISTER FROM ADD ACCOUNT
        /*
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Para continuar, es necesario agregar una cuenta", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
         */
        [params setObject:[NSNumber numberWithInt:0] forKey:@"id_banco"];
    } else {
        [params setObject:accountInformation[@"nombre_prop_cuenta"] forKey:@"nombre_prop_cuenta"];
        [params setObject:accountInformation[@"cuenta_clabe"] forKey:@"cuenta_clabe"];
        [params setObject:accountInformation[@"id_banco"] forKey:@"id_banco"];
    }
    
    //ACTIVATE ACCOUNT
    
    if (!cardInformation) {
        /* NO CARD INFORMATION REQUIRED
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Para continuar, es necesario activar una cuenta", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
        
        return;*/
        
        [params setObject:_addressText.text forKey:@"direccion_establecimiento"];
        [params setObject:[NSNumber numberWithInt:0] forKey:@"id_estado"];
        [params setObject:@"" forKey:@"rfc"];
         
    } else {
        for (NSString *key in cardInformation) {
            [params setObject:cardInformation[key] forKey:key];
        }
    }
    
    
    
    //Adding images - convert to base64
    NSData *idImageData = UIImagePNGRepresentation(_imageView_ID.image);
    developing?[params setObject:@"imageHERE" forKey:@"img_ine"]:[params setObject:[idImageData base64EncodedStringWithOptions:0] forKey:@"img_ine"];
    

    NSData *addressImageData = UIImagePNGRepresentation(_imageView_Address.image);
    developing?[params setObject:@"imageHERE" forKey:@"img_domicilio"]:[params setObject:[addressImageData base64EncodedStringWithOptions:0] forKey:@"img_domicilio"];
    
    
    //MUST HAVE PARAMETERS
    /*
    developing?[params setObject:[NSString stringWithFormat:@"POOT%i", rand()%100+1] forKey:@"imei"]:[params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"manufacturer"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os"];
    [params setObject:@"iOS" forKey:@"platform"];
    */
    developing?NSLog(@"params to be sent -> %@", params):nil;
    
    @try {
        NSError *JSONError;
        NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
        NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
        
        [registerManager startJSONRequestWithURL:[NSString stringWithFormat:@"%@%@%@", userManagementURL, (NSLocalizedString(@"lang", nil)), @"/user/insertv2"] withPost:JSONString];
        
        [self lockViewWithMessage:NSLocalizedString(@"Realizando registro...", nil)];
    } @catch (NSException *exception) {
        NSLog(@"EXCEPTION -> %@", exception.description);
    } @finally {
        nil;
    }
}

#pragma Mark segue handler

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"addAccount"]) {
        UINavigationController *nav = [segue destinationViewController];
        register_Commerce_Step30 *nextView = [[nav viewControllers] firstObject];
        
        [nextView setDelegate:self];
    }
    
    if ([segue.identifier isEqualToString:@"activateAccount"]) {
        UINavigationController *nav = [segue destinationViewController];
        register_Commerce_Step40 *nextView = [[nav viewControllers] firstObject];
        
        [nextView setDelegate:self];
    }
}

#pragma mark addAccount handler
- (IBAction)addAccount_Action:(id)sender {
    /*
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Estamos mejorando esta funcionalidad. No está disponible temporalmente", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
    
    return;
    */
    if (accountInformation) {
        accountInformation = nil;
    } else {
        [self performSegueWithIdentifier:@"addAccount" sender:nil];
    }
    
    [self updateAddButton];
}

- (void)addAccount_Result:(NSDictionary *)accountData
{
    if (accountData) {
        accountInformation = [NSDictionary dictionaryWithDictionary:accountData];
    }
    
    developing?NSLog(@"ACCOUNT INFORMATION -> %@", accountInformation):nil;
    [self updateAddButton];
}

#pragma mark activateAccount handler
- (IBAction)activateAccount_Action:(id)sender {
    [self performSegueWithIdentifier:@"activateAccount" sender:self];
}

- (void)activateAccount_Result:(NSDictionary *)cardData
{
    if (cardData) {
        cardInformation = [NSDictionary dictionaryWithDictionary:cardData];
        
        [_addressText setText:cardInformation[@"direccion_establecimiento"]];
    }
}


#pragma mark terms handler
- (IBAction)termsSwitch_Action:(id)sender {
    if (_termsSwitch.on) {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:termsView];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

#pragma mark registerSMSHanlder
- (void)registerSMS_Response:(NSDictionary *)userData
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)termsAcceptance:(BOOL)result
{
    _termsSwitch.on = result;
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    NSDictionary *response = (NSDictionary*)json;
    if ([response[kIDError] intValue]==0) {
        
        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"register" bundle:nil];
        UINavigationController *nav = [nextStory instantiateViewControllerWithIdentifier:@"codeValidation"];
        Register_Step30 *nextView = [[nav viewControllers] firstObject];
        
        [nextView setUserData:response];
        [nextView setUserID:response[@"id"]];
        [nextView setDelegate:self];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:(NSString*)@"SessionCommercio"];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        /*
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg setTag:1000];
        [alertMsg show];*/
    } else {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1000) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
