//
//  Register_Commerce_Step10.h
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol registerCommerceProtocol <NSObject>
@required
- (void) registryCommerceShallLaunchMyMCWithUserData:(NSDictionary*)userdata;
@end

@interface Register_Commerce_Step10 : mT_commonTableViewController

@property (nonatomic, assign) id <registerCommerceProtocol, NSObject> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *commerceText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passwordText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passwordConfirmText;


@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end
