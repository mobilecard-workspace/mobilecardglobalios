//
//  register_Commerce_Step40.m
//  Mobilecard
//
//  Created by David Poot on 11/23/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "register_Commerce_Step40.h"

@interface register_Commerce_Step40 ()
{
    pootEngine *encrypter;
    
    pootEngine *statesManager;
}

@end

@implementation register_Commerce_Step40

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encrypter = [[pootEngine alloc] init];
    [encrypter setDelegate:self];
    [encrypter setShowComments:developing];
    
   // [self addShadowToView:_saveButton];
    
    [self initializePickersForView:self.navigationController.view];
    
    [_cardNumberText setUpTextFieldAs:textFieldTypeCard];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_townText setUpTextFieldAs:textFieldTypeTown];
    [_cityText setUpTextFieldAs:textFieldTypeCity];
    [_stateText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    [_rfcText setUpTextFieldAs:textFieldTypeRFC];
    [_curpText setUpTextFieldAs:textFieldTypeName];
    
    [self addValidationTextField:_cardNumberText];
    [self addValidationTextField:_addressText];
    [self addValidationTextField:_townText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_stateText];
    [self addValidationTextField:_zipText];
    [self addValidationTextField:_rfcText];
    [self addValidationTextField:_curpText];
    
    [_rfcText setRequired:NO]; //RFC OPTIONAL
    
    [self addValidationTextFieldsToDelegate];
    
    statesManager = [[pootEngine alloc] init];
    [statesManager setDelegate:self];
    [statesManager setShowComments:developing];
    
    [statesManager startRequestWithURL:[NSString stringWithFormat:@"%@/1/estados", walletManagementURL]];
    
    [self lockViewWithMessage:nil];
}

- (IBAction)back_Action:(id)sender {
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButton_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    [[self navigationController] dismissViewControllerAnimated:YES completion:^{
        if ([_delegate conformsToProtocol:@protocol(activateAccountProtocol)]&&[_delegate respondsToSelector:@selector(activateAccount_Result:)]) {
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:[encrypter MCEncryptString:_cardNumberText.hiddenString withPassword:nil] forKey:@"t_previvale"];
            
            [params setObject:_addressText.text forKey:@"direccion_establecimiento"];
            [params setObject:_townText.text forKey:@"colonia"];
            [params setObject:_cityText.text forKey:@"ciudad"];
            [params setObject:[_stateText infoArray][[_stateText selectedID]][[_stateText idLabel]] forKey:@"id_estado"];
            [params setObject:_zipText.text forKey:@"cp"];
            [params setObject:_rfcText.text forKey:@"rfc"];
            [params setObject:_curpText.text forKey:@"curp"];
            
            [_delegate activateAccount_Result:params];
        };
    }];
}

- (void)textFieldDidEndEditing:(UITextField_Validations *)textField
{
    if (textField == _cardNumberText) {
        textField.hiddenString = textField.text;
        NSMutableString *card = [[NSMutableString alloc]init];
        if ([_cardNumberText.text length]!=0 && [_cardNumberText.text length]>4){
            for (int i=0; i<([textField.text length]-4); i++) {
                [card appendString:@"X"];
            }
            for (int i=((int)[_cardNumberText.text length]-4); i<[_cardNumberText.text length]; i++) {
                [card appendString:[NSString stringWithFormat:@"%c",[_cardNumberText.text characterAtIndex:i]]];
            }
            _cardNumberText.text = card;
        }
        
        [self.tableView reloadData];
    }
}

#pragma Mark pootEngine Handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == statesManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        if ([response[kIDError] intValue]==0) {
            [_stateText setInfoArray:response[@"estados"]];
            [_stateText setDescriptionLabel:@"nombre"];
            [_stateText setIdLabel:@"id"];
            
            [_stateText setSelectedID:0];
            [_stateText setText:[_stateText infoArray][[_stateText selectedID]][[_stateText descriptionLabel]]];
        }
    }
}

@end
