//
//  register_Commerce_Step40.h
//  Mobilecard
//
//  Created by David Poot on 11/23/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol activateAccountProtocol <NSObject>

@required

- (void) activateAccount_Result:(NSDictionary*)cardData;

@end

@interface register_Commerce_Step40 : mT_commonTableViewController

@property (assign, nonatomic) id <NSObject, activateAccountProtocol> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *cardNumberText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *townText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *rfcText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *curpText;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

NS_ASSUME_NONNULL_END
