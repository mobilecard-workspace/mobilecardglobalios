//
//  Register_Commerce_Step10.m
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "Register_Commerce_Step10.h"
#import "Register_Commerce_Step20.h"

@interface Register_Commerce_Step10 ()
{
    pootEngine *encrypter;
}
@end

@implementation Register_Commerce_Step10

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self addShadowToView:self.navigationController.navigationBar];
   // [self addShadowToView:_continue_Button];
    
    encrypter = [[pootEngine alloc] init];
    [encrypter setShowComments:developing];
    
    [_commerceText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_emailText setUpTextFieldAs:textFieldTypeEmail];
    [_phoneText setUpTextFieldAs:textFieldTypeCellphone];
    [_passwordText setUpTextFieldAs:textFieldTypePassword];
    [_passwordConfirmText setUpTextFieldAs:textFieldTypePassword];
    
    [self addValidationTextField:_commerceText];
    [self addValidationTextField:_emailText];
    [self addValidationTextField:_phoneText];
    [self addValidationTextField:_passwordText];
    [self addValidationTextField:_passwordConfirmText];
    
    [self addValidationTextFieldsToDelegate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continue_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    if (![[_passwordText text] isEqualToString:[_passwordConfirmText text]]) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"La contraseña y la confirmación deben coincidir, intente de nuevo", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertMsg show];
        return;
    }
    
    [self performSegueWithIdentifier:@"step2" sender:self];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)info_Action:(id)sender {
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"i_Reg1",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alertMsg.view setTintColor:[UIColor darkMCOrangeColor]];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step2"]) {
        Register_Commerce_Step20 *nextView = [segue destinationViewController];
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        
        [data setObject:_commerceText.text forKey:@"nombre_establecimiento"];
        [data setObject:_emailText.text forKey:@"usuario"];
        [data setObject:[self encryptString:_passwordText.text] forKey:@"pass"];
        [data setObject:_emailText.text forKey:@"email_contacto"];
        [data setObject:_phoneText.text forKey:@"telefono_contacto"];
        
        [nextView setData:data];
    }
}


@end
