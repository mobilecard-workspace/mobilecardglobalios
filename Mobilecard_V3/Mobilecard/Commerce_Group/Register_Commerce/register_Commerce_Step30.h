//
//  register_Commerce_Step30.h
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol addAccountProtocol <NSObject>
@required
- (void)addAccount_Result:(NSDictionary*)accountData;
@end

@interface register_Commerce_Step30 : mT_commonTableViewController

@property (nonatomic, assign) id <addAccountProtocol, NSObject> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *accountText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *bankText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;


@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
