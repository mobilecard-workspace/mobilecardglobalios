//
//  register_Commerce_Step30.m
//  Mobilecard
//
//  Created by David Poot on 9/9/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "register_Commerce_Step30.h"

@interface register_Commerce_Step30 ()
{
    pootEngine *bankManager;
}
@end

@implementation register_Commerce_Step30

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializePickersForView:self.navigationController.view];
    
    [self addShadowToView:_continue_Button];
    
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_accountText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    [_bankText setUpTextFieldAs:textFieldTypeRequiredCombo];
    [_addressText setUpTextFieldAs:textFieldTypeAddress];
    [_cityText setUpTextFieldAs:textFieldTypeCity];
    [_zipText setUpTextFieldAs:textFieldTypeZip];
    
    [self addValidationTextField:_nameText];
    [self addValidationTextField:_accountText];
    [self addValidationTextField:_accountText];
    [self addValidationTextField:_bankText];
    [self addValidationTextField:_cityText];
    [self addValidationTextField:_zipText];
    
    [self addValidationTextFieldsToDelegate];
    
    bankManager = [[pootEngine alloc] init];
    [bankManager setDelegate:self];
    [bankManager setShowComments:developing];
    
    [bankManager startWithoutPostRequestWithURL:WSCatalogGetBanks];
    
    [self lockViewWithMessage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)continue_Action:(id)sender {
    if (![self validateFieldsInArray:[self getValidationTextFields]]) {
        return;
    }
    
    NSMutableDictionary *accountData = [[NSMutableDictionary alloc] init];
    
    [accountData setObject:_nameText.text forKey:@"nombre_prop_cuenta"];
    [accountData setObject:_accountText.text forKey:@"cuenta_clabe"];
    [accountData setObject:[_bankText infoArray][[_bankText selectedID]][[_bankText idLabel]] forKey:@"id_banco"];
    
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self->_delegate conformsToProtocol:@protocol(addAccountProtocol)]&&[self->_delegate respondsToSelector:@selector(addAccount_Result:)]) {
            [self->_delegate addAccount_Result:accountData];
        }
    }];
}

#pragma mark pootEngine Handler
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == bankManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue] == 0) {
            [_bankText setInfoArray:response[@"banks"]];
            [_bankText setDescriptionLabel:@"nombre_corto"];
            [_bankText setIdLabel:@"id"];
        } else {
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertMsg show];
        }
    }
}

-(void)pickerView:(UIPickerView_Automated *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self subPickerView:pickerView didSelectRow:row inComponent:component];
    
    if ([[_bankText infoArray][[_bankText selectedID]][[_bankText idLabel]] intValue] == 11 || [[_bankText infoArray][[_bankText selectedID]][[_bankText idLabel]] intValue] == 20) { //BANORTE E IXE
        [_accountText setPlaceholder:@"NÚMERO DE CUENTA"];
        [_accountText setMinLength:10];
        [_accountText setMaxLength:10];
    } else {
        [_accountText setPlaceholder:@"CUENTA CLABE"];
        [_accountText setMinLength:18];
        [_accountText setMaxLength:18];
    }
    [_accountText setText:@""];
}
@end
