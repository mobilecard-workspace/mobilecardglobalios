//
//  myMC_Step1.h
//  Mobilecard
//
//  Created by David Poot on 11/14/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonController.h"
#import "myMC_Step2.h"

@interface myMC_Step1 : mT_commonController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet UICollectionView *infoCollection;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end
