//
//  myMC_Step2.h
//  Mobilecard
//
//  Created by David Poot on 11/15/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "myMC_Step3.h"

@interface myMC_Step2 : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *userData;

@end
