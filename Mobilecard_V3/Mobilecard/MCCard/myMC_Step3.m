//
//  myMC_Step3.m
//  Mobilecard
//
//  Created by David Poot on 11/15/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "myMC_Step3.h"

@interface myMC_Step3 ()

@end

@implementation myMC_Step3

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_licenseText setUpTextFieldAs:textFieldTypeGeneralRequired];
    [_ssnText setUpTextFieldAs:textFieldTypeGeneralNumericRequired];
    
    [self addValidationTextField:_licenseText];
    [self addValidationTextField:_ssnText];
    
    [self addValidationTextFieldsToDelegate];
    
  //  [self addShadowToView:_continue_Button];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([self validateFieldsInArray:[self getValidationTextFields]]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step4"]) {
        MCCard_Ctrl *nextView = [segue destinationViewController];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[_licenseText text] forKey:@"license"];
        [params setObject:[_ssnText text] forKey:@"ssn"];
        
        [nextView setFormData:[NSDictionary dictionaryWithDictionary:params]];
        [nextView setUserData:_userData];
    }
}

@end
