//
//  MCCard_Ctrl.h
//  MobileCard_X
//
//  Created by David Poot on 2/20/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol mcCardDelegate <NSObject>

@required
- (void) mcCardResult:(BOOL)result;

@end

@interface MCCard_Ctrl : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *userData;
@property (strong, nonatomic) NSDictionary *formData;

@property (nonatomic, assign) id <mcCardDelegate, NSObject> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *SSNText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *BirthdayText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *stateText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *cityText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *zipText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *addressText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *govIDText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *issueDate;
@property (weak, nonatomic) IBOutlet UITextField_Validations *expiryDate;
@property (weak, nonatomic) IBOutlet UITextField_Validations *issueState;

@property (weak, nonatomic) IBOutlet UIButton *finalize_Button;
@property (strong, nonatomic) IBOutlet UITableView *dataTable;


@end
