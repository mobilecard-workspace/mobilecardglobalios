//
//  myMC_Step3.h
//  Mobilecard
//
//  Created by David Poot on 11/15/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "MCCard_Ctrl.h"

@interface myMC_Step3 : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet UITextField_Validations *licenseText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *ssnText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
