//
//  Register_Step2.h
//  Mobilecard
//
//  Created by David Poot on 11/2/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "MC_TermsAndConditionsCtrller.h"
#import "updateCard_Ctrl.h"
#import "MCCard_Ctrl.h"
#import "Register_Step1.h"
#import "Register_Step30.h"

@interface Register_Step2 : mT_commonTableViewController <termsDelegate, updateCardDelegate, mcCardDelegate, registerSMSProtocol>

@property (strong, nonatomic) NSMutableDictionary *params;

@property (nonatomic, assign) id <registerProtocol, NSObject> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastnameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *countryText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *documentNumberText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *documentTypeText;

@property (weak, nonatomic) IBOutlet UISwitch *termsSwitch;

@property (weak, nonatomic) IBOutlet UIButton *addPayment_Button;
@property (weak, nonatomic) IBOutlet UIButton *save_Button;

@property (strong, nonatomic) IBOutlet UITableView *infoTable;
@end
