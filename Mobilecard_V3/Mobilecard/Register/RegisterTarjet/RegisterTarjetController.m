//
//  RegisterTarjetController.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/7/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "RegisterTarjetController.h"

@interface RegisterTarjetController ()
{
    CardIOCreditCardInfo *cardInfo;
    pootEngine *crypter;
    
    int cardType;
    NSArray* cardToUpdate;
    
    pootEngine *updateManager;
    pootEngine *cardManager;
}
@end
@implementation RegisterTarjetController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializePickersForView:self.navigationController.view];

    
    updateManager = [[pootEngine alloc] init];
    [updateManager setDelegate:self];
    [updateManager setShowComments:developing];
    
    
    crypter = [[pootEngine alloc] init];
    [_ImageCard setHidden:YES];
    //    [self addShadowToView:_continue_Button];
    
    
    [_nameText setUpTextFieldAs:textFieldTypeName];
    [_cardText setUpTextFieldAs:textFieldTypeCard];
    [_cvvText setUpTextFieldAs:textFieldTypeCvv2];
    [_validText setUpTextFieldAs:textFieldTypeValidCard];
    [_CardCodezip setUpTextFieldAs:textFieldTypeZip];
    [_CardStreet setUpTextFieldAs:textFieldTypeAddress];
    [_CardState setUpTextFieldAs:textFieldTypeAddress];
    [_CardStreet setMaxLength:100];
    [_TypeTarjetText setUpTextFieldAs:textFieldTypeRequiredCombo];
    
    

    [self addValidationTextField:_nameText];
    [self addValidationTextField:_cardText];
    [self addValidationTextField:_cvvText];
    [self addValidationTextField:_validText];
    [self addValidationTextField:_CardStreet];
    [self addValidationTextField:_CardState];
    [self addValidationTextField:_CardCodezip];
    [self addValidationTextField:_TypeTarjetText];
    
    
    [self addValidationTextFieldsToDelegate];
    
    NSMutableArray *cardTypeArray = [[NSMutableArray alloc] init];
    [cardTypeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"CREDITO", kIDKey, NSLocalizedString(@"CREDITO", nil), kDescriptionKey, nil]];
    [cardTypeArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"DEBITO", kIDKey, NSLocalizedString(@"DEBITO", nil), kDescriptionKey, nil]];
    
    
    [_TypeTarjetText setIdLabel:(NSString*)kIDKey];
    [_TypeTarjetText setDescriptionLabel:(NSString*)kDescriptionKey];
    [_TypeTarjetText setInfoArray:cardTypeArray];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)textFieldDidEndEditing:(UITextField_Validations *)textField
{
    if (textField == _cardText) {
        textField.hiddenString = textField.text;
        NSMutableString *card = [[NSMutableString alloc]init];
        if ([_cardText.text length]!=0 && [_cardText.text length]>4){
            for (int i=0; i<([textField.text length]-4); i++) {
                [card appendString:@"X"];
            }
            for (int i=((int)[_cardText.text length]-4); i<[_cardText.text length]; i++) {
                [card appendString:[NSString stringWithFormat:@"%c",[_cardText.text characterAtIndex:i]]];
            }
            _cardText.text = card;
        }
    
        if (![_cardText.hiddenString isEqualToString:@""]) {
            switch ([[_cardText.hiddenString substringWithRange:NSMakeRange(0, 1)] intValue]) {
                case 4://SET ADDRESS & ZIP MANDATORY
                    cardType = 1;
                    [_CardStreet setRequired:YES];
                    [_CardCodezip setRequired:YES];
                    //[_addressText setText:@""];
                    //[_zipText setText:@""];
                    break;
                case 5://SET ADDRESS & ZIP MANDATORY
                    cardType = 2;
                    [_CardStreet setRequired:YES];
                    [_CardCodezip setRequired:YES];
                    //[_addressText setText:@""];
                    //[_zipText setText:@""];
                    break;
                    
                default:
                    cardType = 3;
                    [_CardStreet setRequired:YES];
                    [_CardCodezip setRequired:YES];
                    break;
            }
        }
    }
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)scanCard:(id)sender {
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    scanViewController.hideCardIOLogo=YES;
    [self.navigationController presentViewController:scanViewController animated:YES completion:nil];
}

- (IBAction)continue_Action:(id)sender {
    switch (_type) {
        case updateCardTypeNew:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
                
                [params setObject:[updateManager encryptJSONString:_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                [params setObject:[updateManager encryptJSONString:_cvvText.text withPassword:nil] forKey:@"codigo"];
                [params setObject:[updateManager encryptJSONString:_validText.text withPassword:nil] forKey:@"vigencia"];
                [params setObject:_nameText.text forKey:@"nombre"];
                
                [params setObject:[_CardStreet text] forKey:@"domAmex"];
                [params setObject:[_CardCodezip text] forKey:@"cpAmex"];
                //   [params setObject:[NSNumber numberWithBool:_primarySwitch.on] forKey:@"determinada"];
                [params setObject:[NSNumber numberWithInt:-1] forKey:@"idTarjeta"];
                
                [params setObject:_TypeTarjetText.infoArray[_TypeTarjetText.selectedID][_TypeTarjetText.idLabel] forKey:@"tipoTarjeta"];
                
                [params setObject:(NSLocalizedString(@"lang", nil)) forKey:kLanguage];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [updateManager startJSONRequestWithURL:WSWalletAddCard withPost:JSONString];
                
                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
            }
        }
            break;
            
        case updateCardTypeExisting:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
                
                [params setObject:userInfo[kUserIDKey] forKey:@"idUsuario"];
                
                [params setObject:[updateManager encryptJSONString:_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                [params setObject:[updateManager encryptJSONString:_cvvText.text withPassword:nil] forKey:@"codigo"];
                [params setObject:[updateManager encryptJSONString:_validText.text withPassword:nil] forKey:@"vigencia"];
                
                [params setObject:_nameText.text forKey:@"nombre"];
                [params setObject:[_CardStreet text] forKey:@"domAmex"];
                [params setObject:[_CardCodezip text] forKey:@"cpAmex"];
                // [params setObject:[NSNumber numberWithBool:_primarySwitch.on] forKey:@"determinada"];
                [params setObject:cardToUpdate[_cardToUpdateID][@"idTarjeta"] forKey:@"idTarjeta"];
                
                [params setObject:_TypeTarjetText.infoArray[_TypeTarjetText.selectedID][_TypeTarjetText.idLabel] forKey:@"tipoTarjeta"];
                
                [params setObject:(NSLocalizedString(@"lang", nil)) forKey:kLanguage];
                
                NSError *JSONError;
                NSData *json2Send = [NSJSONSerialization dataWithJSONObject:params options:0 error:&JSONError];
                NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
                
                [updateManager startJSONRequestWithURL:WSWalletUpdateCard withPost:JSONString];
                
                [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
                
               
            }
        }
            break;
        case updateCardTypeRegistry:
        {
            if ([self validateFieldsInArray:[self getValidationTextFields]]) {
                [self dismissViewControllerAnimated:YES completion:^{
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    
                    [params setObject:[self->updateManager encryptJSONString:self->_cardText.hiddenString withPassword:nil] forKey:@"pan"];
                    [params setObject:[self->updateManager encryptJSONString:self->_cvvText.text withPassword:nil] forKey:@"codigo"];
                    [params setObject:[self->updateManager encryptJSONString:self->_validText.text withPassword:nil] forKey:@"vigencia"];
                    [params setObject:self->_nameText.text forKey:@"nombre"];
                    [params setObject:self->_TypeTarjetText.infoArray[self->_TypeTarjetText.selectedID][self->_TypeTarjetText.idLabel] forKey:@"tipoTarjeta"];
                    
                    [params setObject:@"true" forKey:@"determinada"];
                    [params setObject:@"0" forKey:@"idTarjeta"];
                    [params setObject:@"0" forKey:@"idUsuario"];
                    [params setObject:@"false" forKey:@"isMobilecard"];
                    
                    [params setObject:[self->_CardStreet text] forKey:@"domAmex"];
                    [params setObject:[self->_CardCodezip text] forKey:@"cpAmex"];
                    
//                    if ([self->_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[self->_delegate respondsToSelector:@selector(updateCardResponse:)]) {
//                        [self->_delegate updateCardResponse:params];
//                    }
                }];
            }
        }
            break;
    }
}

#pragma mark - CardIOPaymentViewControllerDelegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    cardInfo = info;
    
    [_CardNumberFirst setText:@"****"];
    [_CardNumberTwo setText:@"****"];
    [_CardNumberTree setText:@"****"];
    [_CardNumberFour setText:[cardInfo.cardNumber substringWithRange:NSMakeRange(cardInfo.cardNumber.length-4, 4)]];
    
    if (cardInfo.cardImage != nil) {
        [_ImageCard setHidden:NO];
        [_ImageCard setImage:cardInfo.cardImage];
        
//        switch ([CreditCard_Validator checkCardBrandWithNumber:cardInfo.cardNumber]) {
//            case CreditCardBrandMasterCard:
//                [_ImageTypeCard setImage:[UIImage imageNamed:@"mastercard"]];
//                break;
//            case CreditCardBrandVisa:
//                [_ImageTypeCard setImage:[UIImage imageNamed:@"visa"]];
//                break;
//            case CreditCardBrandAmex:
//                [_ImageTypeCard setImage:[UIImage imageNamed:@"amex"]];
//                break;
//            default:
//                break;
//        }
          [_ImageTypeCard setImage:[UIImage imageNamed:cardType==1?@"visa":cardType==2?@"amex":@"mastercard"]];
    }
    [_cardText setText:cardInfo.cardNumber];
    [self textFieldDidEndEditing:_cardText];
    
    [_cvvText setText:cardInfo.cvv];
    NSString *expYear = [NSString stringWithFormat:@"%d", (int)cardInfo.expiryYear];
    [_validText setText:[NSString stringWithFormat:@"%02d/%@", (int)cardInfo.expiryMonth, [expYear substringWithRange:NSMakeRange(2, 2)]]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma Mark pootEngine Handler
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    switch (_type) {
        case updateCardTypeNew:
        case updateCardTypeExisting:
        {
            if (manager == updateManager) {
                NSMutableDictionary *response = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary*)json];
                [self cleanDictionary:response];
                
                if ([response[kIDError] intValue]==0) {
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                    [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
//                    if ([_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[_delegate respondsToSelector:@selector(updateCardResponse:)]) {
//                        [_delegate updateCardResponse:nil];
//                    }
                    
                    if (_type == updateCardTypeExisting) {
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alertMsg setTag:200];
                        [alertMsg show];
                    } else {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [self showWalletWithType:walletViewTypeSelection];
                    }
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
            }
            if (cardManager == manager) {
                NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
                
                response = [self cleanDictionary:response];
                
                if ([response[kIDError] intValue]==0) {
                    if ([response[kUserCardArray] count]==0) {
                         [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                        [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alertMsg setTag:90];
                        [alertMsg show];
                    } else {
                         [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                        [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [self showWallet];
                    }
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertMsg show];
                }
            }
            
        }
            break;
            
        default:
        {
            
            if (manager == updateManager) {
                NSDictionary *response = (NSDictionary*)json;
                
                if ([response[@"resultado"] intValue]==1) {
                    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
                    
                    [userInfo setObject:_cardText.text forKey:kCardNumber];
                    [userInfo setObject:_validText.text forKey:kCardValid];
                    [userInfo setObject:[NSNumber numberWithInt:cardType] forKey:kCardIDType];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:(NSString*)kUserDetailsKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[@"mensaje"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alertMsg setTag:200];
                    [alertMsg show];
                } else {
                    UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[@"mensaje"] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alertMsg show];
                }
            }
        }
            break;
    }
}

#pragma Mark UIAlertView handler

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 200:
        {
//            if ([_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[_delegate respondsToSelector:@selector(updateCardResponse:)]) {
//                [_delegate updateCardResponse:nil];
//            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        default:
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self dismissViewControllerAnimated:YES completion:^{
//                if ([self->_delegate conformsToProtocol:@protocol(updateCardDelegate)]&&[self->_delegate respondsToSelector:@selector(removeCardWithID:)]) {
//                    [self->_delegate removeCardWithID:self->_cardToUpdateID];
//                }
            }];
        }
            break;
        default:
            break;
    }
}
#pragma Mark storyBoards
- (void)showWalletWithType:(walletViewType)viewType;
{
        @try {
            cardManager = [[pootEngine alloc] init];
            [cardManager setDelegate:self];
            [cardManager setShowComments:developing];
            
            NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
            
            [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
            
            [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
            
            UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
            [alertMsg show];
            
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    
}
- (void) showWallet
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    
    Wallet_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"wallet_view"];
    
    [self.navigationController pushViewController:nextView animated:YES];
   // [_delegate updateCardResponse:nil];
}
@end
