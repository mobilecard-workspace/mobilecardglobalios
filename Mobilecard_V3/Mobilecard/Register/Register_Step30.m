//
//  Register_Step30.m
//  Mobilecard
//
//  Created by David Poot on 11/28/18.
//  Copyright © 2018 David Poot. All rights reserved.
//
#import "AppDelegate.h"
#define APP_DELEGATE (AppDelegate*)[[UIApplication sharedApplication] delegate]
#import "Register_Step30.h"

@interface Register_Step30 ()
{
    NSMutableArray *stringArray;
    
    UITextField_Validations *codeText;
    
    pootEngine *validationManager;
    pootEngine *resendManager;
}
@end

@implementation Register_Step30

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:_resend_Button];
    
    for (id element in _contentView.subviews) {
        if ([element isKindOfClass:[UILabel class]]) {
            UILabel *label = element;
            [self setupLabel:label];
        }
        
        if ([element isKindOfClass:[UITextField_Validations class]]) {
            codeText = element;
        }
    }
    
    [codeText setDelegate:self];
    [codeText becomeFirstResponder];
}

- (void) setupLabel:(UILabel*)label
{
    [[label layer] setBorderWidth:0.5];
    [label.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [label.layer setCornerRadius:6];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
   /// [[APP_DELEGATE window] removeFromSuperview]; 
}

- (IBAction)resendCode_Action:(id)sender {
    resendManager = [[pootEngine alloc] init];
    [resendManager setDelegate:self];
    [resendManager setShowComments:developing];
    
    [resendManager startRequestWithURL:[NSString stringWithFormat:@"%@%@/%@/%@/%@/sms/resend", userManagementURL, [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]?_userData[kUserIdCountry]:@"1", NSLocalizedString(@"lang", nil), [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType], _userID]];
    
    [self lockViewWithMessage:nil];
}

#pragma Mark UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == codeText) {
        [self updateCodeLabels:[textField.text stringByReplacingCharactersInRange:range withString:string]];
    }
    
    if ([[textField.text stringByReplacingCharactersInRange:range withString:string] length]<7) {
        if ([[textField.text stringByReplacingCharactersInRange:range withString:string] length] == 6) {
            validationManager = [[pootEngine alloc] init];
            [validationManager setDelegate:self];
            [validationManager setShowComments:developing];
            
            if( [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)@"SessionCommercio"] isEqualToString:@"1"]){
               [validationManager startRequestWithURL:[NSString stringWithFormat:@"%@1/%@/NEGOCIO/%@/sms/validate?codigo=%@", userManagementURL,  NSLocalizedString(@"lang", nil), _userID, [textField.text stringByReplacingCharactersInRange:range withString:string]]];
                [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:(NSString*)@"SessionCommercio"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }else{
                [validationManager startRequestWithURL:[NSString stringWithFormat:@"%@%@/%@/%@/%@/sms/validate?codigo=%@", userManagementURL, [[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]?_userData[kUserIdCountry]:@"1", NSLocalizedString(@"lang", nil), [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType], _userID, [textField.text stringByReplacingCharactersInRange:range withString:string]]];
            }
            [self lockViewWithMessage:nil];
            return YES;
        } else {
            return YES;
        }
        
    } else {
        return NO;
    }
}

- (void) updateCodeLabels:(NSString*)code
{
    if ([code length]<=6) {
        for (int i = 0; i<[code length]; i++) {
            for (id element in _contentView.subviews) {
                if ([element isKindOfClass:[UILabel class]]) {
                    UILabel *label = element;
                    if (label.tag == i) {
                        [label setText:[code substringWithRange:NSMakeRange(i, 1)]];
                    }
                }
            }
        }
        for (int i = (int)[code length]; i<=6; i++) {
            for (id element in _contentView.subviews) {
                if ([element isKindOfClass:[UILabel class]]) {
                    UILabel *label = element;
                    if (label.tag == i) {
                        [label setText:@""];
                    }
                }
            }
        }
    }
}

- (void)clearCode{
    [codeText setText:@""];
    [self updateCodeLabels:codeText.text];
}


#pragma mark pootEngine Handler
- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == validationManager) {
        NSDictionary *response = (NSDictionary*)json;
        if ([response[kIDError] intValue]==0) {
            NSMutableDictionary *newSession = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey]];
            
            @try {
                [newSession setObject:response[[[[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserType] isEqualToString:@"USUARIO"]?@"idUsrStatus":@"estatus"] forKey:@"idUsrStatus"];
            }
            @catch (NSException * e) {
                NSLog(@"Exception: %@", e);
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:(NSString*)@"SessionSMS_To_Menu"];
            [[NSUserDefaults standardUserDefaults] setObject:newSession forKey:(NSString*)kUserDetailsKey];

            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            
            if ([_delegate conformsToProtocol:@protocol(registerSMSProtocol)]&&[_delegate respondsToSelector:@selector(registerSMS_Response:)]) {
                [_delegate registerSMS_Response:[NSDictionary dictionaryWithDictionary:response]];
            }
//            if (_ViewLogin) {
//               //  [self performSegueWithIdentifier:@"main_menu" sender:nil];
//                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
//                UITableViewController *scrollview =(UITableViewController *)[storyboard instantiateViewControllerWithIdentifier:@"mainstory"];
//                [self.navigationController pushViewController:scrollview animated:YES];
//            }else{
//
//            }
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
                
                [self clearCode];
                [self->codeText becomeFirstResponder];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
    }
    
    if (manager == resendManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertMsg dismissViewControllerAnimated:YES completion:nil];
            
            [self->codeText becomeFirstResponder];
        }];
        
        [alertMsg addAction:ok];
        
        [self presentViewController:alertMsg animated:YES completion:nil];
    }
}
@end
