//
//  RegisterNew_Step3.m
//  Mobilecard
//
//  Created by Raul Mendez on 6/6/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import "RegisterNew_Step3.h"

@interface RegisterNew_Step3 ()

@end

@implementation RegisterNew_Step3

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_infoCollection setFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height*0.4)];
    [_pageControl setFrame:CGRectMake(0, 40+self.view.frame.size.height*0.4, self.view.frame.size.width, 37)];
     _pageControl.transform = CGAffineTransformMakeScale(3, 3);
    //  [self addShadowToView:_continue_Button];
    
    //  [_continue_Button setAlpha:0.0];
    // [_continue_Button setEnabled:NO];
     [self addShadowToView:_pageControl];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NSString stringWithFormat:@"page%ld", indexPath.row+1] forIndexPath:indexPath];
    
    // [self addShadowToView:cell];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(collectionView.frame)-40, (CGRectGetHeight(collectionView.frame))-25);
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = _infoCollection.frame.size.width;
    _pageControl.currentPage = _infoCollection.contentOffset.x / pageWidth;
    
    //       [self refreshButton];
}
- (IBAction)pageControl_Action:(id)sender {
    [_infoCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:_pageControl.currentPage inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    
    //    [self refreshButton];
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (void)refreshButton
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    
    [_continue_Button setAlpha:_pageControl.currentPage==5?1.0:0.0];
    //    if (_pageControl.currentPage == 4) {
    //        [_continue_Button setEnabled:YES];
    //    }
    
    [UIView commitAnimations];
}
- (IBAction)Continue_action:(id)sender {
//    [self performSegueWithIdentifier:@"step2" sender:self];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)getMobilecard_action:(id)sender {
    //    [self performSegueWithIdentifier:@"step2" sender:self];
    //    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    UINavigationController *nav = [[UIStoryboard storyboardWithName:@"previvale" bundle:nil] instantiateViewControllerWithIdentifier:@"user"];
    previvale_Step10 *nextView = [[nav childViewControllers] firstObject];
    [nextView setDelegate:self];
    [[self navigationController] presentViewController:nav animated:YES completion:nil];
    
}
- (IBAction)getYourTarjet_action:(id)sender {
    UINavigationController *nav = [[UIStoryboard storyboardWithName:@"registerTarjet" bundle:nil] instantiateViewControllerWithIdentifier:@"tarjet"];
    commerce_CardPayment_Step20 *nextView = [[nav childViewControllers] firstObject];
  //  [nextView setDelegate:self];
    [[self navigationController] presentViewController:nav animated:YES completion:nil];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //    if ([[segue identifier]isEqualToString:@"step2"]) {
    //        myMC_Step2 *nextView = [segue destinationViewController];
    //        [nextView setUserData:_userData];
    //    }
}
#pragma mark previvale handler
- (void)previvaleRegisterResponse:(NSDictionary *)response
{
    if (response) {
//        UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"myMC" bundle:nil];
//        MCPanel_Ctrl *nextView = [nextStory instantiateViewControllerWithIdentifier:@"mcpanel_view"];
//        [nextView setMcCardInfo:response[@"card"]];
//        
//        [[self navigationController] pushViewController:nextView animated:YES];
    }
}
@end
