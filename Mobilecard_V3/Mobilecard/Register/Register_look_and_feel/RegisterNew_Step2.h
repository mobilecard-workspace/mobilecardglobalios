//
//  RegisterNew_Step2.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/2/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonTableViewController.h"
#import "MC_TermsAndConditionsCtrller.h"
#import "updateCard_Ctrl.h"
#import "MCCard_Ctrl.h"
#import "Register_Step1.h"
#import "Register_Step30.h"

//#import <FBSDKCoreKit/FBSDKCoreKit.h>
//#import <FBSDKLoginKit/FBSDKLoginKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface RegisterNew_Step2 : mT_commonTableViewController <termsDelegate, updateCardDelegate, mcCardDelegate, registerSMSProtocol>

//@property (strong, nonatomic) NSMutableDictionary *params;

@property (nonatomic, assign) id <registerProtocol, NSObject> delegate;

@property (weak, nonatomic) IBOutlet UITextField_Validations *nameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *lastnameText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *phoneText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *emailText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *passwodText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *confirmPasswordText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *countryText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *documentNumberText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *documentTypeText;

@property (weak, nonatomic) IBOutlet UISwitch *termsSwitch;

@property (weak, nonatomic) IBOutlet UIButton *addPayment_Button;
@property (weak, nonatomic) IBOutlet UIButton *save_Button;

@property (strong, nonatomic) IBOutlet UITableView *infoTable;

@property (strong, nonatomic)   IBOutlet UIView *ViewContentButtonFacebook;

@property (weak, nonatomic) IBOutlet UIImageView *_imageGoogle;

@end

NS_ASSUME_NONNULL_END
