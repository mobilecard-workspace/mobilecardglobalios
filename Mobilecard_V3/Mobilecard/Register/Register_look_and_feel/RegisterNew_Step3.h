//
//  RegisterNew_Step3.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/6/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"
#import "previvale_Step10.h"



#import "commerce_CardPayment_Step20.h"
#import "commerce_CardPayment_Step30.h"
NS_ASSUME_NONNULL_BEGIN

@interface RegisterNew_Step3 :  mT_commonController <UICollectionViewDelegate, UICollectionViewDataSource,previvaleRegisterProtocol> 

@property (strong, nonatomic) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet UICollectionView *infoCollection;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@end

NS_ASSUME_NONNULL_END
