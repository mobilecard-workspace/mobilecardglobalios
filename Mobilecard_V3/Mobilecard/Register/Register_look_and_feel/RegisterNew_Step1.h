//
//  RegisterNew_Step1.h
//  Mobilecard
//
//  Created by Raul Mendez on 6/1/19.
//  Copyright © 2019 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mT_commonController.h"


NS_ASSUME_NONNULL_BEGIN

@interface RegisterNew_Step1 : mT_commonController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) NSDictionary *userData;

@property (weak, nonatomic) IBOutlet UICollectionView *infoCollection;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *continue_Button;


@end

NS_ASSUME_NONNULL_END
