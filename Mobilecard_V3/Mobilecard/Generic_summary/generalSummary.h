//
//  generalSummary.h
//  Telecomm
//
//  Created by David Poot on 7/29/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@protocol generalSummaryDelegate

@optional
- (void)generalSummaryResponse:(NSDictionary*)response;

@end

@interface generalSummary : mT_commonTableViewController

@property (assign, nonatomic) id <NSObject, generalSummaryDelegate> delegate;

@property BOOL convertToUSD;

@property (weak, nonatomic) IBOutlet UIView *exchangeGroup;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (strong, nonatomic) NSString *rateString;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@property (weak, nonatomic) IBOutlet UITextField_Validations *mainText;
@property (strong, nonatomic) NSString *mainString;
@property (strong, nonatomic) NSString *mainPlaceholderString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *firstText;
@property (strong, nonatomic) NSString *firstString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *secondText;
@property (strong, nonatomic) NSString *secondString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *thirdText;
@property (strong, nonatomic) NSString *thirdString;
@property (weak, nonatomic) IBOutlet UITextField_Validations *fourthText;
@property (strong, nonatomic) NSString *fourthString;



@end
