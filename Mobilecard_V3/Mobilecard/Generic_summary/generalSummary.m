//
//  generalSummary.m
//  Telecomm
//
//  Created by David Poot on 7/29/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "generalSummary.h"

@interface generalSummary ()
{
    pootEngine *rateManager;
    NSNumberFormatter *numFormatter;
}
@end

@implementation generalSummary

- (void)viewDidLoad {
    
   // [self addShadowToView:_confirmButton];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numFormatter setCurrencySymbol:@"$"];
    
    [self updateData];
}

- (void)updateData
{
    NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    if (_mainPlaceholderString) {
        [_mainText setPlaceholder:_mainPlaceholderString];
    }
    
    [_mainText setHidden:_mainString?NO:YES];
    [_firstText setHidden:_firstString?NO:YES];
    [_secondText setHidden:_secondString?NO:YES];
    [_thirdText setHidden:_thirdString?NO:YES];
    [_fourthText setHidden:_fourthString?NO:YES];
    
    [_exchangeGroup setHidden:_rateString?NO:YES];
    
    switch ([userDetails[kUserIdCountry] intValue]) {
        case 1: //MEXICO
        {
            [_mainText setText:_mainString];
            [_firstText setText:_firstString];
            [_secondText setText:_secondString];
            [_thirdText setText:_thirdString];
            [_fourthText setText:_fourthString];
            
            [_exchangeGroup setHidden:YES];
        }
            break;
        case 2: //COLOMBIA
        {
            [_mainText setText:_mainString];
            [_firstText setText:_firstString];
            [_secondText setText:_secondString];
            [_thirdText setText:_thirdString];
            [_fourthText setText:_fourthString];
            
            [_exchangeGroup setHidden:YES];
        }
            break;
        case 3: //USA
        {
            if (_convertToUSD) {
                if (_rateString) {
                    [_mainText setText:_mainString];
                    [_firstText setText:_firstString];
                    [_secondText setText:[self convertValueToUSD:_secondString]];
                    [_thirdText setText:[self convertValueToUSD:_thirdString]];
                    [_fourthText setText:[self convertValueToUSD:_fourthString]];
                    
                    [_rateLabel setText:[NSString stringWithFormat:@"$1.00 USD = %@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_rateString floatValue]]]]];
                } else {
                    rateManager = [[pootEngine alloc] init];
                    [rateManager setDelegate:self];
                    [rateManager setShowComments:developing];
                    
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    [params setObject:@"Parameter" forKey:@"Parameter"];
                    [rateManager startProtectedRequestWithValues:params forWS:WSGetTransactoRate withPass:@"L3dsjkd3Fs" Automated:YES];
                    
                    [self lockViewWithMessage:nil];
                }
            } else {
                [_mainText setText:_mainString];
                [_firstText setText:_firstString];
                [_secondText setText:_secondString];
                [_thirdText setText:_thirdString];
                [_fourthText setText:_fourthString];
                
                [_rateLabel setText:[NSString stringWithFormat:@"$1.00 USD = %@ MXN", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_rateString floatValue]]]]];
            }
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirm_Action:(id)sender {
    if ([_delegate conformsToProtocol:@protocol(generalSummaryDelegate)]&&[_delegate respondsToSelector:@selector(generalSummaryResponse:)]) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            [_delegate generalSummaryResponse:nil];
        }];
    }
}

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == rateManager) {
        NSDictionary *response = (NSDictionary*)json;
        
        _rateString = response[@"valorDolar"];
        
        [self updateData];
    }
}

- (NSString*)convertValueToUSD:(NSString*)valueString
{
    if (!valueString) {
        return @"";
    }
    
    NSString *cleanValue = [NSMutableString stringWithString:valueString];
    cleanValue = [cleanValue stringByReplacingOccurrencesOfString:@" MXN" withString:@""];
    cleanValue = [cleanValue stringByReplacingOccurrencesOfString:@" USD" withString:@""];
    
    NSNumber *value = [numFormatter numberFromString:cleanValue];
    
    return [NSString stringWithFormat:@"%@ USD", [numFormatter stringFromNumber:[NSNumber numberWithFloat:[value floatValue]/[_rateString floatValue]]]];
}


@end
