//
//  AppDelegate.h
//  Mobilecard
//
//  Created by David Poot on 10/2/17.
//  Copyright © 2017 David Poot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "pootEngine.h"
#import "Register_Step30.h"
#import "mT_commonController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, pootEngineDelegate>

@property (strong, nonatomic) UINavigationController *currentNav;
@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) bool canRunIngoSdk;

@end

