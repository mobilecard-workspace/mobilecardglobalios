//
//  scanNPay_Step40.m
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "scanNPay_Step40.h"
#import <CoreImage/CoreImage.h>

@interface scanNPay_Step40 ()
{
    NSNumberFormatter *numFormatter;
}
@end

@implementation scanNPay_Step40

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // DELETE
    /*
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    [result setObject:@"271762" forKey:@"authProcom"];
    [result setObject:@"0.7" forKey:@"comision"];
    [result setObject:@"09/14/2018 11:12:29 AM" forKey:@"fecha"];
    [result setObject:@"158985" forKey:@"idBitacora"];
    [result setObject:@"0" forKey:@"idError"];
    [result setObject:@"Pago Exitoso" forKey:@"mensajeError"];
    [result setObject:@"11.7" forKey:@"montoTransfer"];
    [result setObject:@"436715624739" forKey:@"refProcom"];
    [result setObject:@"0" forKey:@"referenceBanorte"];
    [result setObject:@"Test" forKey:@"referenciaNeg"];
    [result setObject:@"4152- XXXX - XXXX - 9591" forKey:@"tarjeta"];
    
    _resultData = [NSDictionary dictionaryWithDictionary:result];
     */
    // DELETE
    
    [self generateQRWithObject:_resultData];
    
   // [self addShadowToView:_continue_Button];
    
    numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setCurrencySymbol:@"$"];
    [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    if (_resultData) {
        [_resultTextView setText:[NSString stringWithFormat:NSLocalizedString(@"Referencia: %@\nID transacción: %@\nAutorización bancaria: %@\nNúmero de tarjeta: %@\nFecha: %@\nTotal: %@", nil), _resultData[@"opId"], _resultData[@"idTransaccion"], _resultData[@"authNumber"], _resultData[@"maskedPAN"], _resultData[@"dateTime"], [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_resultData[@"amount"] floatValue]]]]];
    }
    
    [_resultTextView setSelectable:NO];
    [_resultTextView setEditable:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void) generateQRWithObject:(NSDictionary*)object2Send
{
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    
    [filter setDefaults];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:object2Send options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    NSString *QRData = JSONString;
    
    NSData *data = [QRData dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    
    CIImage *outputImage = [filter outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:outputImage
                                       fromRect:[outputImage extent]];
    
    UIImage *image = [UIImage imageWithCGImage:cgImage
                                         scale:1.
                                   orientation:UIImageOrientationUp];
    
    // Resize without interpolating
    UIImage *resized = [self resizeImage:image
                             withQuality:kCGInterpolationNone
                                    rate:5.0];
    
    [_QRImageView setImage:resized];
    
    CGImageRelease(cgImage);
}

#pragma mark - Private

- (UIImage *)resizeImage:(UIImage *)image
             withQuality:(CGInterpolationQuality)quality
                    rate:(CGFloat)rate
{
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}
- (IBAction)Button_continueAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
