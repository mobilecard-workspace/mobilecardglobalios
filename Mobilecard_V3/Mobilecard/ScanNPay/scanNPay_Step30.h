//
//  scanNPay_Step30.h
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Wallet_Ctrl.h"
#import "Secure3D_Ctrl.h"
#import <CoreLocation/CoreLocation.h>
#import <TrustDefender/TrustDefender.h>

@interface scanNPay_Step30 : mT_commonTableViewController <CLLocationManagerDelegate, cardSelectionDelegate, updateCardDelegate, Secure3DDelegate>

@property BOOL showCardOptionsFirst;

@property (strong, nonatomic) NSDictionary *data;

@property (weak, nonatomic) IBOutlet UITextView *resultTextView;
@property (weak, nonatomic) IBOutlet UITextField_Validations *msiText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
