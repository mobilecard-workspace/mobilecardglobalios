//
//  scanNPay_Step20.m
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "scanNPay_Step20.h"
#import "scanNPay_Step30.h"
#import "commerce_QRPayment_Step30.h"

@interface scanNPay_Step20 ()
{
    NSString *scannerReference;
    
    pootEngine *cardManager;
    pootEngine *paymentManager;
    
    THMTrustDefender *profile;
    
    NSMutableDictionary *threatMetrixInfo;
    NSDictionary *selectedCardInfo;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
}
@end

@implementation scanNPay_Step20

- (void)viewDidLoad {
    [super viewDidLoad];
    
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
   // [self addShadowToView:_continue_Button];
    
    [_idNumberText setUpTextFieldAs:textFieldTypeGeneralNumericNoRequired];
    
    [self addValidationTextField:_idNumberText];
    
    [self addValidationTextFieldsToDelegate];
    
    if (_msiText) {
        [self initializePickersForView:self.navigationController.view];
        
        [_msiText setUpTextFieldAs:textFieldTypeRequiredCombo];
        [self addValidationTextField:_msiText];
        [self addValidationTextFieldsToDelegate];
        
        [_msiText setInfoArray:[NSArray arrayWithObjects:
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago en una sola exhibición", kDescriptionKey, @"0", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 3 meses sin intereses", kDescriptionKey, @"3", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 6 meses sin intereses", kDescriptionKey, @"6", kIDKey, nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"Pago a 9 meses sin intereses", kDescriptionKey, @"9", kIDKey, nil],
                                nil]];
        
        [_msiText setDescriptionLabel:(NSString*)kDescriptionKey];
        [_msiText setIdLabel:(NSString*)kIDKey];
        
        [_msiText setSelectedID:0];
        [_msiText setText:[_msiText infoArray][[_msiText selectedID]][[_msiText descriptionLabel]]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)scanQR_Action:(id)sender {
    @try {
        ZBarReaderViewController *reader = [[ZBarReaderViewController alloc]init];
        
        reader.readerDelegate = self;
        //[reader.scanner setSymbology:0 config:ZBAR_CFG_ENABLE to:0];
        [reader.scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_ENABLE to:1];  //QR READING
        //[reader.scanner setSymbology:ZBAR_CODE128 config:ZBAR_CFG_ENABLE to:1]; //BAR READING
        
        [reader setTitle:@"Enfoque el código"];
        
        reader.showsZBarControls = NO;
        
        
        overlayView = [[MC_SAPOverlayView alloc]initWithNibName:@"MC_SAPOverlayView" bundle:nil];
        [overlayView.view setFrame:CGRectMake(0, 0, 320, 480)];
        
        [reader setCameraOverlayView:[overlayView view]];
        //[reader setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        
        [self presentViewController:reader animated:YES completion:^{
            
            CGRect backFrame = CGRectMake(0, reader.view.frame.size.height-60, reader.view.frame.size.width, 60);
            
            UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [backButton setFrame:backFrame];
            [backButton setBackgroundColor:[UIColor clearColor]];
            [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [backButton setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
            
            [overlayView.view addSubview:backButton];
        }];
        
        
    }
    @catch (NSException *exception) {
        NSLog(@"No se activo escaner: %@",[exception description]);
    }
}

- (void)backAction:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)continue_Action:(id)sender {
    if ([[_idNumberText text] length]>0) {
        [self showCardOptions];
    }
}


- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    @try {
        
        ZBarSymbolSet *symbolset=[info objectForKey:ZBarReaderControllerResults];
        ZBarSymbol  *symbol=nil;
        
        NSString *qrCode=[[NSString alloc]init];
        
        for(symbol in symbolset )
        {
            qrCode=[NSString stringWithString:symbol.data];
        }
        
        scannerReference = qrCode;
        
        [self backAction:nil];
        
        id responseID;
        NSError *jsonError = nil;
        NSData *jsonData = [scannerReference dataUsingEncoding:NSUTF8StringEncoding];
        
        if ([jsonData length]>0) {
            responseID = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&jsonError];
            
            if (jsonError) {
                UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Código QR no válido", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertMsg dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertMsg addAction:ok];
                
                [self presentViewController:alertMsg animated:YES completion:nil];
                return;
            }
        }
        
        NSDictionary *response = (NSDictionary*)responseID;
        
        [self performSegueWithIdentifier:@"step30" sender:response];
    }
    @catch (NSException *exception) {
        NSLog(@"Error:%@",[exception description]);
        UIAlertView *error=[[UIAlertView  alloc]initWithTitle:@"Error." message:@"Codigo QR incorrecto." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [error show];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"step30"]) {
        scanNPay_Step30 *nextView = [segue destinationViewController];
        [nextView setData:(NSDictionary*)sender];
        [nextView setShowCardOptionsFirst:YES];
    }
    
    if ([segue.identifier isEqualToString:@"result"]) {
        commerce_QRPayment_Step30 *nextView = [segue destinationViewController];
        NSDictionary *response = (NSDictionary*)sender;
        [nextView setResultData:response];
    }
}


#pragma Mark Handling Wallet

- (void) showCardOptions
{
    @try {
        cardManager = [[pootEngine alloc] init];
        [cardManager setDelegate:self];
        [cardManager setShowComments:developing];
        
        NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
        
        [cardManager startRequestWithURL:[NSString stringWithFormat:@"%@?idUsuario=%@&idioma=%@", WSWalletGetCards, userData[kUserIDKey], NSLocalizedString(@"lang", nil)]];
        
        [self lockViewWithMessage:NSLocalizedString(@"Procesando solicitud...", nil)];
    } @catch (NSException *exception) {
        UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [alertMsg show];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserDetailsKey];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:(NSString*)kUserPassword];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)showWalletWithType:(walletViewType)viewType;
{
    UIStoryboard *nextStory = [UIStoryboard storyboardWithName:@"wallet" bundle:nil];
    UINavigationController *nav = [nextStory instantiateInitialViewController];
    Wallet_Ctrl *view = nav.viewControllers.firstObject;
    [view setType:viewType];
    [view setDelegate:self];
    
    [self.navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)cardSelectionResult:(NSDictionary *)selectedCard
{
    if ([selectedCard[@"tipoTarjeta"] isEqualToString:@"CREDITO"] && ([selectedCard[@"tipo"] isEqualToString:@"MasterCard"]||[selectedCard[@"tipo"] isEqualToString:@"VISA"])) {
        selectedCardInfo = selectedCard;
        [self performProfileCheck];
    } else {
        if ([_msiText selectedID]==0) {
            selectedCardInfo = selectedCard;
            [self performProfileCheck];
        } else {
            UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:NSLocalizedString(@"Su selección de tarjeta sólo permite pago en una sola exhibición", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertMsg dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertMsg addAction:ok];
            
            [self presentViewController:alertMsg animated:YES completion:nil];
        }
        
    }
}

- (void)performPaymentWithCard:(NSDictionary*)card
{
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kUserDetailsKey];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSNumber numberWithInt:0] forKey:@"amount"];
    [params setObject:[NSNumber numberWithInt:0] forKey:@"comision"];
    [params setObject:@"" forKey:@"concept"];
    [params setObject:@"" forKey:@"ct"];
    [params setObject:[NSNumber numberWithInt:0] forKey:@"establecimientoId"];
    [params setObject:@"" forKey:@"firma"];
    [params setObject:[NSNumber numberWithInt:[_idNumberText.text intValue]] forKey:@"idBitacora"];
    [params setObject:card[@"idTarjeta"] forKey:@"idCard"];
    [params setObject:userData[kUserIDKey] forKey:@"idUser"];
    [params setObject:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude] doubleValue]] forKey:@"lat"];
    [params setObject:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude] doubleValue]] forKey:@"lon"];
    [params setObject:[_msiText infoArray][[_msiText selectedID]][[_msiText idLabel]] forKey:@"msi"];
    [params setObject:[NSNumber numberWithInt:0] forKey:@"propina"];
    [params setObject:@"" forKey:@"referenciaNeg"];
    
    [params setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [params setObject:@"Apple" forKey:@"modelo"];
    [params setObject:[[UIDevice currentDevice] systemVersion] forKey:@"software"];
    
    [params setObject:@"" forKey:@"tarjeta"];
    [params setObject:@"" forKey:@"vigencia"];
    [params setObject:@"" forKey:@"tipoTarjeta"];
    
    paymentManager = [[pootEngine alloc] init];
    [paymentManager setDelegate:self];
    [paymentManager setShowComments:developing];
    
    Secure3D_Ctrl *nextView = [[Secure3D_Ctrl alloc] initWithNibName:@"Secure3D_Ctrl" bundle:nil];
    [nextView setPurchaseInfo:[NSMutableDictionary dictionaryWithDictionary:params]];
    [nextView setType:serviceTypeCuantoTeDeboByID];
    [nextView setThreatMetrixInfo:threatMetrixInfo];
    
    [nextView setDelegate:self];
    
    [self.navigationController pushViewController:nextView animated:YES];

}

#pragma Mark pootEngine handler

- (void)managerSuccess:(pootEngine *)manager jsonParsed:(id)json
{
    [self unLockView];
    
    if (manager == cardManager) {
        if (cardManager == manager) {
            NSMutableDictionary *response = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)json];
            
            response = [self cleanDictionary:response];
            
            if ([response[kIDError] intValue]==0) {
                [[NSUserDefaults standardUserDefaults] setObject:response[@"hasMobilecard"] forKey:(NSString*)@"Mobilecard"];
                [[NSUserDefaults standardUserDefaults] setObject:response[kUserCardArray] forKey:(NSString*)kUserCardArray];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showWalletWithType:walletViewTypeSelection];
            } else {
                UIAlertView *alertMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Información", nil) message:response[kErrorMessage] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertMsg show];
            }
        }
    }
}

- (void)Secure3DResponse:(NSDictionary *)response
{
    developing?NSLog(@"Result -> %@", response):nil;
    [self performSegueWithIdentifier:@"result" sender:response];
}



#pragma Mark TrustDefender Handling

- (void) performProfileCheck
{
    [self lockViewWithMessage:nil];
    
    profile = [THMTrustDefender sharedInstance];
    
    [profile configure:@{
                         THMOrgID:@"c2ggoqh1",
                         THMFingerprintServer:@"h-sdk.online-metrix.net",
                         }];
    
    [profile doProfileRequestWithCallback:^(NSDictionary *result)
     {
         [self unLockView];
         developing?NSLog(@"PROFILE RESULT -> %@", result):nil;
         self->threatMetrixInfo = [NSMutableDictionary dictionaryWithDictionary:result];
         
         THMStatusCode statusCode = [[result valueForKey:THMProfileStatus] integerValue];
         
         if (statusCode == THMStatusCodeOk) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self performPaymentWithCard:selectedCardInfo];
             });
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:[NSString stringWithFormat:@"%s", statusCode == THMStatusCodeNetworkTimeoutError ? "Timed out"                                                                                                                                                                : statusCode == THMStatusCodeConnectionError     ? "Connection Error"                                                                                                                                                           : statusCode == THMStatusCodeHostNotFoundError   ? "Host not found error"                                                                                                                                                : statusCode == THMStatusCodeInternalError       ? "Internal Error"                                                                                                                                                        : statusCode == THMStatusCodeInterruptedError    ? "Interrupted"                                                                                                                                                           : "other"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [alertMsg dismissViewControllerAnimated:YES completion:nil];
                 }];
                 
                 [alertMsg addAction:ok];
                 
                 [self presentViewController:alertMsg animated:YES completion:nil];
             });
         }
         
         
     }];
}

- (void)PaymentWithThreatMetrixResponse:(NSDictionary *)response
{
    if (response) {
        [self performSegueWithIdentifier:@"result" sender:response];
        
        if ([response[@"code"] intValue] == 0) {
            
        };
    }
}






#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertMsg = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Información", nil) message:@"Failed to Get Your Location" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertMsg dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertMsg addAction:ok];
    
    [self presentViewController:alertMsg animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [locationManager stopUpdatingLocation];
}

@end
