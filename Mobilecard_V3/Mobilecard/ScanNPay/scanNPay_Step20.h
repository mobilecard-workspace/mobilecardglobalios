//
//  scanNPay_Step20.h
//  Mobilecard
//
//  Created by David Poot on 9/19/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "ZBarSDK.h"
#import "MC_SAPOverlayView.h"
#import "Secure3D_Ctrl.h"
#import "Wallet_Ctrl.h"
#import <TrustDefender/TrustDefender.h>
#import <CoreLocation/CoreLocation.h>

@interface scanNPay_Step20 : mT_commonTableViewController <ZBarReaderDelegate, Secure3DDelegate, updateCardDelegate, cardSelectionDelegate, CLLocationManagerDelegate>
{
    MC_SAPOverlayView *overlayView;
}

@property (weak, nonatomic) IBOutlet UITextField_Validations *idNumberText;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;

@property (weak, nonatomic) IBOutlet UITextField_Validations *msiText;
@end
