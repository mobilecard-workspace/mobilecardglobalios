//
//  LaCuenta_Step3.h
//  Mobilecard
//
//  Created by David Poot on 2/6/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Secure3D_Ctrl.h"

@interface LaCuenta_Step3 : mT_commonTableViewController <Secure3DDelegate>

@property (strong, nonatomic) NSDictionary *paymentData;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UILabel *feeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@property (strong, nonatomic) UIImage *logoImage;
@property (weak, nonatomic) IBOutlet UIImageView *logoView;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
