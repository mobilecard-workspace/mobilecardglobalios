//
//  LaCuenta_Result.h
//  Mobilecard
//
//  Created by David Poot on 2/6/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"

@interface LaCuenta_Result : mT_commonTableViewController

@property (strong, nonatomic) NSDictionary *resultData;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *resultImage;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *QRImageView;
@property (weak, nonatomic) IBOutlet UIButton *share_Button;
@property (weak, nonatomic) IBOutlet UIView *share_View;
@property (weak, nonatomic) IBOutlet UIButton *accept_Button;
@end
