//
//  LaCuenta_Step2.h
//  Mobilecard
//
//  Created by David Poot on 2/5/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "mT_commonTableViewController.h"
#import "Wallet_Ctrl.h"

@interface LaCuenta_Step2 : mT_commonTableViewController <cardSelectionDelegate, updateCardDelegate>

@property (strong, nonatomic) NSDictionary *paymentData;

@property (weak, nonatomic) IBOutlet UITextField_Validations *referenceText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *totalText;
@property (weak, nonatomic) IBOutlet UITextField_Validations *tipText;
@property (weak, nonatomic) IBOutlet UIImageView *logoView;

@property (weak, nonatomic) IBOutlet UILabel *feeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@property (weak, nonatomic) IBOutlet UIButton *continue_Button;
@end
