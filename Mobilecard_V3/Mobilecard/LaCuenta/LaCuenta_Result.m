//
//  LaCuenta_Result.m
//  Mobilecard
//
//  Created by David Poot on 2/6/18.
//  Copyright © 2018 David Poot. All rights reserved.
//

#import "LaCuenta_Result.h"
#import <CoreImage/CoreImage.h>

@interface LaCuenta_Result ()
{
    BOOL result;
    pootEngine *decrypter;
    
    NSNumberFormatter *numFormatter;
}
@end

@implementation LaCuenta_Result

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self addShadowToView:_accept_Button];
    
    [_titleLabel setText:_resultData[kErrorMessage]];
    if ([_resultData[kIDError] intValue] == 0) {
        result = YES;
        
        numFormatter = [[NSNumberFormatter alloc] init];
        [numFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numFormatter setCurrencySymbol:@"$"];
        
        decrypter = [[pootEngine alloc] init];
        [decrypter setShowComments:developing];
        
        NSMutableString *cardString = [NSMutableString stringWithString:[decrypter decryptedStringOfString:_resultData[@"tarjeta"] withSensitive:NO]];
        
        [cardString replaceCharactersInRange:NSMakeRange(4, 8) withString:@"∙∙∙∙∙∙∙∙"];
        
        [_resultImage setImage:[UIImage imageNamed:@"icon_ok"]];
        [_descriptionTextView setText:[NSString stringWithFormat:NSLocalizedString(@"Referencia: %@\nId transacción: %@\nAutorización bancaria: %@\nTarjeta: %@\nFecha: %@\nTotal: %@", nil),
                                       _resultData[@"referenciaNeg"],
                                       _resultData[@"idBitacora"],
                                       _resultData[@"authProcom"],
                                       cardString,
                                       _resultData[@"fecha"],
                                       [numFormatter stringFromNumber:[NSNumber numberWithFloat:[_resultData[@"montoTransfer"] floatValue]]]]];
        
        [_descriptionTextView setSelectable:NO];
        [_descriptionTextView setEditable:NO];
        [_descriptionTextView setScrollEnabled:NO];
        [self generateQR];
    } else {
        result = NO;
        [_descriptionTextView setText:[NSString stringWithFormat:@"Error %d\n%@", [_resultData[kIDError] intValue], _resultData[kErrorMessage]]];
        [_descriptionTextView setSelectable:NO];
        [_descriptionTextView setEditable:NO];
        [_descriptionTextView setScrollEnabled:NO];
        [_resultImage setImage:[UIImage imageNamed:@"icon_nook"]];
        [_QRImageView setHidden:YES];
        [_infoLabel setHidden:YES];
        [_share_Button setHidden:YES];
        [_share_View setHidden:YES];
    }

    developing?NSLog(@"RESPONSE -> %@", _resultData):nil;
}

- (void) generateQR
{
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    
    [filter setDefaults];
    
    NSError *JSONError;
    NSData *json2Send = [NSJSONSerialization dataWithJSONObject:_resultData options:0 error:&JSONError];
    NSString *JSONString = [[NSString alloc]initWithData:json2Send encoding:NSUTF8StringEncoding];
    
    NSString *QRData = JSONString;
    
    NSData *data = [QRData dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    
    CIImage *outputImage = [filter outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:outputImage
                                       fromRect:[outputImage extent]];
    
    UIImage *image = [UIImage imageWithCGImage:cgImage
                                         scale:1.
                                   orientation:UIImageOrientationUp];
    
    // Resize without interpolating
    UIImage *resized = [self resizeImage:image
                             withQuality:kCGInterpolationNone
                                    rate:5.0];
    
    [_QRImageView setImage:resized];
    
    CGImageRelease(cgImage);
}

- (IBAction)back_Action:(id)sender {
    if (result) {
        [self.navigationController popToViewController:[self.navigationController viewControllers][[[self.navigationController viewControllers] count]-4] animated:YES];
    } else {
        [self.navigationController popToViewController:[self.navigationController viewControllers][[[self.navigationController viewControllers] count]-3] animated:YES];
    }
}

- (IBAction)share_Action:(id)sender {
    NSString *theMessage = [_descriptionTextView text];
    NSArray *items = @[theMessage];
    
    UIActivityViewController *sharingController = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    [self presentActivityController:sharingController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return 124;
            break;
        case 1:
            return result?425:124;
            break;
        case 2:
            return 130;
            break;
            
        default:
            return 30;
            break;
    }
}


// =============================================================================
#pragma mark - Private

- (UIImage *)resizeImage:(UIImage *)image
             withQuality:(CGInterpolationQuality)quality
                    rate:(CGFloat)rate
{
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}

- (void)presentActivityController:(UIActivityViewController *)controller {
    
    // for iPad: make the presentation a Popover
    //controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    // access the completion handler
    controller.completionWithItemsHandler = ^(NSString *activityType,
                                              BOOL completed,
                                              NSArray *returnedItems,
                                              NSError *error){
        // react to the completion
        if (completed) {
            
            // user shared an item
            //NSLog(@"We used activity type%@", activityType);
            
        } else {
            
            // user cancelled
            //NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            //NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

@end
