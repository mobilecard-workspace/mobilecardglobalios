

import UIKit

@objc class DataUser: NSObject {
    
    
    
    private static func data(property:String, newValue:NSDictionary!) {
        
       
        let defaults = UserDefaults.standard
        if (newValue == nil) {
            defaults.set(nil, forKey: property);
            defaults.synchronize();
            return;
        }
        
        let dictionary = NSMutableDictionary(dictionary: newValue);
        
        for key in dictionary.allKeys {
            if (dictionary[key as! String] is NSNull) {
                dictionary.setObject("", forKey: key as! String as NSCopying);
            }
        }
        
        defaults.set(dictionary, forKey: property);
        defaults.synchronize();
        
    }
    
    
    static func clean() {
        DataUser.NAME = nil
        DataUser.TOKEN      = nil
        DataUser.DATA       = nil
        DataUser.USER_ID    = nil
        DataUser.COUNTRY = nil
    }
    
    
    @objc  static func setBackgroundColor(color:UIColor,vista:UIView) -> UIView{
        
        vista.backgroundColor = #colorLiteral(red: 0.948255837, green: 0.5015385747, blue: 0, alpha: 1)
        
return vista
    }
    
    
    static var TOKEN: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.token");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.token");
            defaults.synchronize();
        }
    }
    
    @objc static var COUNTRY: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.country");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.country");
            defaults.synchronize();
        }
    }
    
    static var NAME: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.name");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.name");
            defaults.synchronize();
        }
    }
    
    static var USER_ID: String! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info:String? = defaults.string(forKey: "DataUser.user_id");
            return info;
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "DataUser.user_id");
            defaults.synchronize();
        }
    }
    
 

    static var DATA: NSDictionary! {
        get {
            let defaults:UserDefaults = UserDefaults.standard;
            let info = defaults.dictionary(forKey: "DataUser.data");
            return (info! as NSDictionary);
        }
        set {
            DataUser.data(property: "DataUser.data", newValue: newValue);
        }
    }
    
    
    
    /* ------------------ */
    /* GETTERS // SETTERS */
    
    static func exist(prop:String) -> Bool {
        return DataUser.DATA[prop] != nil;
    }
    
    static func dictionary(prop:String) -> Dictionary<String, AnyObject> {
        if (DataUser.DATA[prop] == nil || DataUser.DATA[prop] is NSNull) {
            return Dictionary();
        }
        if DataUser.DATA[prop] is Dictionary<String, AnyObject> {
            return DataUser.DATA[prop] as! Dictionary<String, AnyObject>;
        }
        return Dictionary();
    }
    
    static func array(prop:String)->Array<AnyObject> {
        if (DataUser.DATA[prop] == nil || DataUser.DATA[prop] is NSNull) {
            return [];
        }
        if DataUser.DATA[prop] is Array<AnyObject> {
            return DataUser.DATA[prop] as! Array<AnyObject>;
        }
        return [];
    }
    
    static func string(prop:String)->String {
        
        var obj:Dictionary<String, AnyObject>!;
        var ppp:String!;
    
        if (prop.contains(".")) {
            let cmp = prop.components(separatedBy: ".")
            obj = dictionary(prop: cmp[0])
            ppp = cmp[1];
        } else {
            obj = (DataUser.DATA as! Dictionary<String, AnyObject>);
            ppp = prop;
        }
        
        if (obj[ppp] == nil || obj[ppp] is NSNull) {
            return "";
        }
        if obj[prop] is Double {
            return String(obj[ppp] as! Double);
        }
        return obj[ppp] as! String;
    }
    
    static func bool(prop:String)->Bool {
        
        var obj:Dictionary<String, AnyObject>!;
        var ppp:String!;
        
        if (prop.contains(".")) {
            let cmp = prop.components(separatedBy: ".")
            obj = dictionary(prop: cmp[0])
            ppp = cmp[1];
        } else {
            obj = (DataUser.DATA as! Dictionary<String, AnyObject>);
            ppp = prop;
        }
        
        if (obj[ppp] == nil || obj[ppp] is NSNull) {
            return false;
        }
        if obj[ppp] is String {
            return (obj[ppp] as! String) == "true" || (obj[ppp] as! String) == "1";
        }
        if obj[ppp] is Int {
            return obj[ppp] as! Int == 1;
        }
        if obj[prop] is Double {
            return obj[ppp] as! Double == 1;
        }
        return obj[ppp] as! Bool;
        
    }
    
    static func double(prop:String)->Double {
        
        var obj:Dictionary<String, AnyObject>!;
        var ppp:String!;
        
        if (prop.contains(".")) {
            let cmp = prop.components(separatedBy: ".")
            obj = dictionary(prop: cmp[0])
            ppp = cmp[1];
        } else {
            obj = (DataUser.DATA as! Dictionary<String, AnyObject>);
            ppp = prop;
        }
        
        
        if (obj[ppp] == nil || obj[ppp] is NSNull) {
            return 0;
        }
        if obj[ppp] is String {
            return Double(obj[ppp] as! String)!;
        }
        if obj[ppp] is Int {
            return Double(obj[ppp] as! Int);
        }
        
        return obj[ppp] as! Double;
    }
    
    static func int(prop:String)->Int {
        
        var obj:Dictionary<String, AnyObject>!;
        var ppp:String!;
        
        if (prop.contains(".")) {
            let cmp = prop.components(separatedBy: ".")
            obj = dictionary(prop: cmp[0])
            ppp = cmp[1];
        } else {
            obj = (DataUser.DATA as! Dictionary<String, AnyObject>);
            ppp = prop;
        }
        
        
        
        
        if (obj[ppp] == nil || obj[ppp] is NSNull) {
            return 0;
        }
        if obj[ppp] is String {
            return Int(obj[ppp] as! String)!;
        }
        if obj[ppp] is Double {
            return Int(obj[ppp] as! Double);
        }
        
        return obj[ppp] as! Int;
    }
    
    
    

    
    
}
