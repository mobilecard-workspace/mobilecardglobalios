

import Foundation
import UIKit
import Alamofire



protocol ServicesDelegate {
    func responseService(type:ServicesTypes,endpoint:String, response:NSDictionary)
    func errorService(endpoint:String)
}


//solo cuenta con servicios post actualmente
enum ServicesTypes {
    case GET
    case POST
}



//objeto que almacena la informacion del servicio
class Services: NSObject{
     var manager: SessionManager!;
    //delegado
    var delegate: ServicesDelegate?
    
    //cuerpo del objeto
    var type : ServicesTypes?
    var endpoint : String?
    
    //url principal del servidor
    var urlServices :String! = "https://www.kreativeco.com/cemex_comunica/index.php/ws/"

   

    
    
    
  
    
    //metodo que ejecuta la consulta 
    func send(type: ServicesTypes,url: String,params:[String:Any],message:String){
     
        
        
        
        self.endpoint = url
        self.type = type
        
        
        
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: message, animated: true)
        
        
        
        
        
        let stringUrl = "\(urlServices!)\(url)"
  
  
        let url = URL(string: stringUrl)
  
        
        // si no hay parametros en body manejamos la pura url 
        if params.isEmpty {
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                self.delegate?.errorService(endpoint: self.endpoint!)
                DispatchQueue.main.async {LoaderView.hide()}
                return
            }
            guard let data = data else {
                print("Data is empty")
                self.delegate?.errorService(endpoint: self.endpoint!)
                DispatchQueue.main.async {LoaderView.hide()}
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: [])
            print(json)
            let dataJson = json as! NSDictionary
            
            DispatchQueue.main.async {LoaderView.hide()}
              self.delegate?.responseService(type: self.type!, endpoint: self.endpoint!, response: dataJson)
            
            
            
        }
        
        
        task.resume()
            
        }else{
            
            
            
            let headers  : HTTPHeaders = ["Content-Type":"application/json","Accept":"*/*"]
            
            // add your custom headers
            
            
            let config:URLSessionConfiguration = URLSessionConfiguration.default;
            
            if (self.manager == nil) {
                self.manager = Alamofire.SessionManager(configuration: config);
            }
            
            var paramsp:Dictionary<String,AnyObject> = Dictionary();
            var paramsd:Dictionary<String,AnyObject> = Dictionary();
            
            for (key, value) in params {
                
                if value is Dictionary<String, AnyObject>{
                    
                    paramsd[key] = value as AnyObject?;
                    
                }else{
                    
                   
                    paramsp[key] = value as AnyObject?;
                    
                }
                
                
            }
            
            
            self.manager.upload(multipartFormData: { multipartFormData in
                
                for (key, value) in paramsd {
                    let file:NSDictionary = value as! NSDictionary;
                    
                    multipartFormData.append(file["file"] as! Data, withName: key, fileName: file["name"] as! String, mimeType: file["mime"] as! String)
                    
                }
                
                for (key, value) in paramsp {
                    
                    multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
                
            }, to: stringUrl, method: Alamofire.HTTPMethod.post, headers: headers, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString { response in switch response.result {
                        
                    case .success(let JSONSTR):
                        
                        let data = JSONSTR.data(using: String.Encoding.utf8)!;
                        
                    
                        
                        do{
                            if let jsonObect = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary{
                                
                                DispatchQueue.main.async {LoaderView.hide()}
                                 self.delegate?.responseService(type: self.type!, endpoint: self.endpoint!, response: jsonObect)
                                
                            } else if let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                                
                              
                                
                            }else{
                               
                            }
                        }catch{
                        
                        }
                        
                    case .failure(let _):
                        DispatchQueue.main.async {LoaderView.hide()}
                        self.delegate?.errorService(endpoint: self.endpoint!)
                        }
                        
                    }
                case .failure(let encodingError):
                    print("ERROR ENCODING: \(encodingError)");
                    DispatchQueue.main.async {LoaderView.hide()}
                       self.delegate?.errorService(endpoint: self.endpoint!)
                }
            })
            
            print("-------------------------");
            
        }
    
            
 
        }
        
    
       
        
   
        
    
    
   
 
    
}





