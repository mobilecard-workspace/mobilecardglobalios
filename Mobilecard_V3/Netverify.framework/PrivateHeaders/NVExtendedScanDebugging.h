//
//  NVExtendedScanDebugging.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import "JMExtendedScanDebugging.h"

NS_ASSUME_NONNULL_BEGIN

__attribute__((visibility("default"))) @interface NVExtendedScanDebugging : JMExtendedScanDebugging

+ (NVExtendedScanDebugging*) sharedInstance;

@end

NS_ASSUME_NONNULL_END
