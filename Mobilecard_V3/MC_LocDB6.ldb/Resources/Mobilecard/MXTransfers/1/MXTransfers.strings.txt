
/* Class = "UITextField"; placeholder = "Holder name*"; ObjectID = "0qf-2k-f6T"; */
"0qf-2k-f6T.placeholder" = "Holder name*";

/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "1cZ-oq-gf6"; */
"1cZ-oq-gf6.normalTitle" = "CONTINUE >";

/* Class = "UIButton"; normalTitle = "SAVE >"; ObjectID = "2rG-zn-QE4"; */
"2rG-zn-QE4.normalTitle" = "SAVE >";

/* Class = "UIButton"; normalTitle = "ADD"; ObjectID = "3eU-BG-dwz"; */
"3eU-BG-dwz.normalTitle" = "ADD";

/* Class = "UINavigationItem"; title = "TRANSFERS"; ObjectID = "5JA-72-rz2"; */
"5JA-72-rz2.title" = "TRANSFERS";

/* Class = "UINavigationItem"; title = "TRANSFERS"; ObjectID = "7WU-1C-zLX"; */
"7WU-1C-zLX.title" = "TRANSFERS";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "9Ve-lx-kbR"; */
"9Ve-lx-kbR.title" = "←";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "AIx-GG-BJm"; */
"AIx-GG-BJm.title" = "←";

/* Class = "UILabel"; text = "Amount to pay"; ObjectID = "F2W-cT-Jbi"; */
"F2W-cT-Jbi.text" = "Amount to pay";

/* Class = "UILabel"; text = "$XXXXX.XX MXN"; ObjectID = "GgD-0g-tXW"; */
"GgD-0g-tXW.text" = "$XXXXX.XX MXN";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "Gx4-vD-Yz7"; */
"Gx4-vD-Yz7.title" = "←";

/* Class = "UITextField"; placeholder = "Email*"; ObjectID = "Is8-FB-cht"; */
"Is8-FB-cht.placeholder" = "Email*";

/* Class = "UILabel"; text = "Comission"; ObjectID = "LHE-9y-V15"; */
"LHE-9y-V15.text" = "Comission";

/* Class = "UITextField"; placeholder = "Bank*"; ObjectID = "Lpy-R6-9q7"; */
"Lpy-R6-9q7.placeholder" = "Bank*";

/* Class = "UILabel"; text = "FILL RECEPIENT'S INFORMATION"; ObjectID = "MyQ-QR-gB3"; */
"MyQ-QR-gB3.text" = "FILL RECEPIENT'S INFORMATION";

/* Class = "UITextField"; placeholder = "RECIPIENT"; ObjectID = "QxY-6s-OfD"; */
"QxY-6s-OfD.placeholder" = "RECIPIENT";

/* Class = "UIButton"; normalTitle = "SAVE >"; ObjectID = "T6m-V0-XGz"; */
"T6m-V0-XGz.normalTitle" = "SAVE >";

/* Class = "UITextField"; placeholder = "Phone*"; ObjectID = "X74-09-sfE"; */
"X74-09-sfE.placeholder" = "Phone*";

/* Class = "UILabel"; text = "Choose or add a new recipient"; ObjectID = "Yhw-nt-OKk"; */
"Yhw-nt-OKk.text" = "Choose or add a new recipient";

/* Class = "UITextField"; placeholder = "Account type*"; ObjectID = "YlZ-4N-YVu"; */
"YlZ-4N-YVu.placeholder" = "Account type*";

/* Class = "UILabel"; text = "Recipient information"; ObjectID = "a8B-aX-RZT"; */
"a8B-aX-RZT.text" = "Recipient information";

/* Class = "UILabel"; text = "$XXXXX.XX MXN"; ObjectID = "e3E-6D-gUU"; */
"e3E-6D-gUU.text" = "$XXXXX.XX MXN";

/* Class = "UITextField"; placeholder = "Alias*"; ObjectID = "eHK-O7-QQ6"; */
"eHK-O7-QQ6.placeholder" = "Alias*";

/* Class = "UITextField"; placeholder = "Phone number*"; ObjectID = "gew-Ch-WYN"; */
"gew-Ch-WYN.placeholder" = "Phone number*";

/* Class = "UITextField"; placeholder = "Holder last name*"; ObjectID = "hPE-dn-pCr"; */
"hPE-dn-pCr.placeholder" = "Holder last name*";

/* Class = "UITextField"; placeholder = "Account/CLABE number*"; ObjectID = "hbu-sE-5yN"; */
"hbu-sE-5yN.placeholder" = "Account/CLABE number*";

/* Class = "UITextField"; placeholder = "AMOUNT"; ObjectID = "hd1-oq-D1e"; */
"hd1-oq-D1e.placeholder" = "AMOUNT";

/* Class = "UILabel"; text = "CAPTURE YOUR RECIPIENT'S INFORMATION"; ObjectID = "jr5-ju-aob"; */
"jr5-ju-aob.text" = "CAPTURE YOUR RECIPIENT'S INFORMATION";

/* Class = "UILabel"; text = "Recipient information"; ObjectID = "nkW-Wb-3n0"; */
"nkW-Wb-3n0.text" = "Recipient information";

/* Class = "UITextField"; placeholder = "Second last name*"; ObjectID = "oqI-Lj-AVk"; */
"oqI-Lj-AVk.placeholder" = "Second last name*";

/* Class = "UITextField"; placeholder = "Alias*"; ObjectID = "s05-I8-5YP"; */
"s05-I8-5YP.placeholder" = "Alias*";

/* Class = "UINavigationItem"; title = "TRANSFERS"; ObjectID = "v4S-m9-paw"; */
"v4S-m9-paw.title" = "TRANSFERS";

/* Class = "UITextField"; placeholder = "Email*"; ObjectID = "zHG-Fp-5qf"; */
"zHG-Fp-5qf.placeholder" = "Email*";
