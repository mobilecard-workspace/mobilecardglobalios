//
//  NVFaceController.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Netverify/NVScanController.h>

NS_ASSUME_NONNULL_BEGIN

__attribute__((visibility("default"))) @interface NVFaceController : NVScanController

@end

NS_ASSUME_NONNULL_END
