
/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "1pk-gM-b0r"; */
"1pk-gM-b0r.normalTitle" = "CONTINUE >";

/* Class = "UITextField"; placeholder = "State of Expedition*"; ObjectID = "2P0-Y6-ZDb"; */
"2P0-Y6-ZDb.placeholder" = "State of Expedition*";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "3TE-PE-t5X"; */
"3TE-PE-t5X.title" = "←";

/* Class = "UITextField"; placeholder = "Zip Code*"; ObjectID = "4q8-BN-YLT"; */
"4q8-BN-YLT.placeholder" = "Zip Code*";

/* Class = "UILabel"; text = "License"; ObjectID = "5SL-4p-GVt"; */
"5SL-4p-GVt.text" = "License";

/* Class = "UILabel"; text = "$XXXXXX.XX"; ObjectID = "7Pq-As-c4Y"; */
"7Pq-As-c4Y.text" = "$XXXXXX.XX";

/* Class = "UILabel"; text = "Passport"; ObjectID = "7q8-f1-LqR"; */
"7q8-f1-LqR.text" = "Passport";

/* Class = "UITextField"; placeholder = "Expiration Date*"; ObjectID = "Aag-Ay-e5p"; */
"Aag-Ay-e5p.placeholder" = "Expiration Date*";

/* Class = "UILabel"; text = "TRANSACTIONS"; ObjectID = "B3V-p0-84H"; */
"B3V-p0-84H.text" = "TRANSACTIONS";

/* Class = "UILabel"; text = "Label"; ObjectID = "Brm-bR-Bb2"; */
"Brm-bR-Bb2.text" = "Label";

/* Class = "UITextField"; placeholder = "Expedition Date*"; ObjectID = "CXs-H8-uUo"; */
"CXs-H8-uUo.placeholder" = "Expedition Date*";

/* Class = "UITextField"; placeholder = "LICENSE NUMBER"; ObjectID = "ElA-hD-WbL"; */
"ElA-hD-WbL.placeholder" = "LICENSE NUMBER";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "GSm-I7-M4C"; */
"GSm-I7-M4C.title" = "←";

/* Class = "UINavigationItem"; title = "MY MOBILECARD"; ObjectID = "HKF-3x-PBy"; */
"HKF-3x-PBy.title" = "MY MOBILECARD";

/* Class = "UILabel"; text = "Choose the type of official document"; ObjectID = "IDW-Rb-N38"; */
"IDW-Rb-N38.text" = "Choose the type of official document";

/* Class = "UILabel"; text = "1234 **** **** 5678"; ObjectID = "IsU-8z-u5t"; */
"IsU-8z-u5t.text" = "1234 **** **** 5678";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "JTF-4O-z03"; */
"JTF-4O-z03.title" = "←";

/* Class = "UITextField"; placeholder = "License number*"; ObjectID = "LCz-Nz-Hg3"; */
"LCz-Nz-Hg3.placeholder" = "License number*";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "Loj-PB-Qf2"; */
"Loj-PB-Qf2.title" = "←";

/* Class = "UILabel"; text = "You can also\nmake payments\nin all LatAm"; ObjectID = "MWS-2O-WyQ"; */
"MWS-2O-WyQ.text" = "You can also\nmake payments\nin all LatAm";

/* Class = "UITextField"; placeholder = "Balance"; ObjectID = "NRA-xC-xIN"; */
"NRA-xC-xIN.placeholder" = "Balance";

/* Class = "UILabel"; text = "American Express"; ObjectID = "SWp-nW-NuG"; */
"SWp-nW-NuG.text" = "American Express";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "Wdw-sf-ud1"; */
"Wdw-sf-ud1.title" = "←";

/* Class = "UILabel"; text = "With MobileCard\nyou can pay your\nBill payments"; ObjectID = "XIp-4g-8pg"; */
"XIp-4g-8pg.text" = "With MobileCard\nyou can pay your\nBill payments";

/* Class = "UIButton"; normalTitle = "FINALIZE"; ObjectID = "YAM-AD-upS"; */
"YAM-AD-upS.normalTitle" = "FINALIZE";

/* Class = "UIButton"; normalTitle = "ACTIVATE"; ObjectID = "ZMv-wA-uu2"; */
"ZMv-wA-uu2.normalTitle" = "ACTIVATE";

/* Class = "UILabel"; text = "Matrícula\nConsular"; ObjectID = "ZPq-af-ksT"; */
"ZPq-af-ksT.text" = "Matrícula\nConsular";

/* Class = "UILabel"; text = "LICENSE INFORMATION"; ObjectID = "aEb-qw-bhJ"; */
"aEb-qw-bhJ.text" = "LICENSE INFORMATION";

/* Class = "UITextField"; placeholder = "Social Security Number"; ObjectID = "aOz-7H-1LH"; */
"aOz-7H-1LH.placeholder" = "Social Security Number";

/* Class = "UINavigationItem"; title = "MY MOBILECARD"; ObjectID = "aRk-lb-rjj"; */
"aRk-lb-rjj.title" = "MY MOBILECARD";

/* Class = "UITextField"; placeholder = "State*"; ObjectID = "d4k-pu-IZP"; */
"d4k-pu-IZP.placeholder" = "State*";

/* Class = "UILabel"; text = "ADD INFORMATION"; ObjectID = "dN1-pd-hhY"; */
"dN1-pd-hhY.text" = "ADD INFORMATION";

/* Class = "UILabel"; text = "Easy to obtain,\nget it now!"; ObjectID = "dTz-gI-BSk"; */
"dTz-gI-BSk.text" = "Easy to obtain,\nget it now!";

/* Class = "UIButton"; normalTitle = "GET YOUR MOBILECARD"; ObjectID = "hKA-MP-nui"; */
"hKA-MP-nui.normalTitle" = "GET YOUR MOBILECARD";

/* Class = "UILabel"; text = "Label"; ObjectID = "jgN-db-7ux"; */
"jgN-db-7ux.text" = "Label";

/* Class = "UINavigationItem"; title = "MY MOBILECARD"; ObjectID = "jmc-Fd-mtO"; */
"jmc-Fd-mtO.title" = "MY MOBILECARD";

/* Class = "UITextField"; placeholder = "Social Security Number*"; ObjectID = "kIJ-9f-jY1"; */
"kIJ-9f-jY1.placeholder" = "Social Security Number*";

/* Class = "UITextField"; placeholder = "Address*"; ObjectID = "l7D-HY-zsu"; */
"l7D-HY-zsu.placeholder" = "Address*";

/* Class = "UITextField"; placeholder = "Date of Birth*"; ObjectID = "mld-ms-4kl"; */
"mld-ms-4kl.placeholder" = "Date of Birth*";

/* Class = "UINavigationItem"; title = "MY MOBILECARD"; ObjectID = "nle-s3-HgV"; */
"nle-s3-HgV.title" = "MY MOBILECARD";

/* Class = "UITextField"; placeholder = "City*"; ObjectID = "tgX-PU-YKk"; */
"tgX-PU-YKk.placeholder" = "City*";

/* Class = "UILabel"; text = "Enter your personal information"; ObjectID = "v3V-Zb-XsV"; */
"v3V-Zb-XsV.text" = "Enter your personal information";

/* Class = "UILabel"; text = "Label"; ObjectID = "wad-Xr-ouU"; */
"wad-Xr-ouU.text" = "Label";

/* Class = "UINavigationItem"; title = "MY MOBILECARD"; ObjectID = "xQm-ET-ZO4"; */
"xQm-ET-ZO4.title" = "MY MOBILECARD";

/* Class = "UILabel"; text = "ACTIVATE YOUR MOBILECARD"; ObjectID = "yhQ-wQ-gqu"; */
"yhQ-wQ-gqu.text" = "ACTIVATE YOUR MOBILECARD";
