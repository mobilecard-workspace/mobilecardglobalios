
/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "0HR-Z5-dXc"; */
"0HR-Z5-dXc.normalTitle" = "CONTINUE >";

/* Class = "UILabel"; text = "PERSONAL INFORMATION"; ObjectID = "0MN-Lb-hOt"; */
"0MN-Lb-hOt.text" = "PERSONAL INFORMATION";

/* Class = "UITextField"; placeholder = "NAME"; ObjectID = "3ep-Zc-ioP"; */
"3ep-Zc-ioP.placeholder" = "NAME";

/* Class = "UITextField"; placeholder = "CONFIRM PASSWORD"; ObjectID = "5X5-7B-pcM"; */
"5X5-7B-pcM.placeholder" = "CONFIRM PASSWORD";

/* Class = "UINavigationItem"; title = "Sign up"; ObjectID = "6hH-zy-4cB"; */
"6hH-zy-4cB.title" = "Sign up";

/* Class = "UITextField"; placeholder = "LASTNAME"; ObjectID = "6iQ-9P-A2c"; */
"6iQ-9P-A2c.placeholder" = "LASTNAME";

/* Class = "UITextField"; placeholder = "COUNTRY"; ObjectID = "L1n-XB-fE9"; */
"L1n-XB-fE9.placeholder" = "COUNTRY";

/* Class = "UINavigationItem"; title = "Sign up"; ObjectID = "NCz-Zk-LJt"; */
"NCz-Zk-LJt.title" = "Sign up";

/* Class = "UIButton"; normalTitle = "PRIVACY NOTICE"; ObjectID = "R8O-cT-Px6"; */
"R8O-cT-Px6.normalTitle" = "PRIVACY NOTICE";

/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "X5N-vC-b8o"; */
"X5N-vC-b8o.normalTitle" = "CONTINUE >";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "cO8-Ce-kRx"; */
"cO8-Ce-kRx.title" = "←";

/* Class = "UITextField"; placeholder = "EMAIL"; ObjectID = "eiA-dm-zbp"; */
"eiA-dm-zbp.placeholder" = "EMAIL";

/* Class = "UIButton"; normalTitle = "ADD PAYMENT METHOD"; ObjectID = "gkE-Jk-Yex"; */
"gkE-Jk-Yex.normalTitle" = "ADD PAYMENT METHOD";

/* Class = "UITextField"; placeholder = "MOBILE PHONE NUMBER"; ObjectID = "h6O-Bf-a0x"; */
"h6O-Bf-a0x.placeholder" = "MOBILE PHONE NUMBER";

/* Class = "UITextField"; placeholder = "PASSWORD"; ObjectID = "lLB-JV-ZpM"; */
"lLB-JV-ZpM.placeholder" = "PASSWORD";

/* Class = "UILabel"; text = "I HAVE READ AND ACCEPT THE TERMS AND CONDITIONS OF THE SERVICE"; ObjectID = "oNH-7A-pXc"; */
"oNH-7A-pXc.text" = "I HAVE READ AND ACCEPT THE TERMS AND CONDITIONS OF THE SERVICE";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "uAZ-DU-CLd"; */
"uAZ-DU-CLd.title" = "←";

/* Class = "UILabel"; text = "CREATE YOUR ACCOUNT"; ObjectID = "uRd-Ng-qMf"; */
"uRd-Ng-qMf.text" = "CREATE YOUR ACCOUNT";
