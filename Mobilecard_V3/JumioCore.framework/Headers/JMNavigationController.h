//
//  JMNavigationController.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

__attribute__((visibility("default"))) @interface JMNavigationController : UINavigationController

@end
