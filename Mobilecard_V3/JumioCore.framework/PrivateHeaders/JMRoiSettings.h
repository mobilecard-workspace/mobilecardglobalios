//
//  JMRoiSettings.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

__attribute__((visibility("default"))) @interface JMRoiSettings : NSObject

@end
