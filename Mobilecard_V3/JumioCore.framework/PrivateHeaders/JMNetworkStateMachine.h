//
//  NVNetworkStateMachine.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <JumioCore/JMStateMachine.h>


__attribute__((visibility("default"))) @interface JMNetworkStateMachine : JMStateMachine


@end
