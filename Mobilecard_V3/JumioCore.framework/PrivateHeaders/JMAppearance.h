//
//  JMAppearance.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

__attribute__((visibility("default"))) @interface JMAppearance : NSObject

+ (void) setGeneralAppearanceForClass:(id)containingClass;

+ (void)customizeJumioScanOverlayForClass:(Class)containingClass;

@end
