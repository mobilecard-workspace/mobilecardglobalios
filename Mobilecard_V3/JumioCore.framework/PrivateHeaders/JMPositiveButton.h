//
//  JMPositiveButton.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <JumioCore/JMSubmitButton.h>

__attribute__((visibility("default"))) @interface JMPositiveButton : JMSubmitButton

@end
