/* 
  Localizable.strings/Users/dpoot/Dropbox/Addcel/Proyectos/Mobilecard/Mobilecard/Localizable.strings
  Mobilecard

  Created by David Poot on 10/30/17.
  Copyright © 2017 David Poot. All rights reserved.
*/

//Language
"lang" = "en";

//Options
"CREDITO" = "CREDIT";
"DEBITO" = "DEBIT";

//Button titles
"AÑADIR TARJETA" = "ADD CARD";
"MÉTODO DE PAGO" = "PAYMENT METHOD";
"Bienvenido %@" = "Welcome %@";
"CONSULTAR SALDO >" = "CHECK BALANCE >";
"CONTINUAR >" = "CONTINUE >";
"ESCANEAR %@" = "SCAN %@";
"Terminar" = "Finish";
"Reenviar enlace" = "Resend activation link";
"Cancelar" = "Cancel";
"Obtener tarjeta" = "GET CARD";
"Activar ahora" = "Activate now";
"ELIMINAR TARJETA" = "REMOVE PAYMENT METHOD";
"AGREGAR TARJETA" = "ADD PAYMENT METHOD";
"Sí" = "Yes";
"Recuperar contraseña" = "Reset password";
"Cambiar" = "Update";
"Back" = "Back";
"Comisión %@" = "Fee %@";

//UIAlertView Misc
"Procesando solicitud..." = "Processing request...";
"Información" = "Information";
"Atención" = "Attention";
"Tu cuenta no está verificada" = "Your account has not been verified";
"Recuperar contraseña" = "Reset password";
"Cambio de contraseña" = "Update password";
"Cambiar contraseña" = "Update password";
"Actualizar" = "Update";
"Error políticas" = "You must accept the privacy policy and the terms and conditions of the service to continue";
"Información importante" = "Warning!";
"Presiona OK para terminar la selección" = "Press OK to finish selection";
"Iniciando sesión..." = "Signing in...";

//UIAlertView questions
"¿Está seguro de eliminar esta tarjeta?" = "Are you sure you want to delete this card?";
"Para poder acceder a los servicios de MobileCard debes activar tu cuenta. El enlace de activación se envía al correo electrónico que registraste en la aplicación" = "In order to access this feature your account should be activated. The activation link is sent to your registered email.";
"¡Ya casi has terminado!" = "You are almost there!";
"Escribe tu nombre de usuario o correo electrónico en el campo para recuperar tu contraseña" = "Type your email or username to reset your password";
"Es necesario que realice el cambio de la contraseña genérica\n\nEscriba su nueva contraseña" = "You must change your generic password\n\nSet your new password";
"Para continuar, es necesario aceptar los términos y condiciones" = "To continue, it is required to accept terms & conditions";
"Escriba su nombre" = "Type your first name";
"Escriba su apellido" = "Type your last name";
"Escriba su contraseña actual" = "Write your current password";
"Seleccione un país" = "Choose a country";
"Escriba un nuevo número telefónico" = "Add a new mobile phone number";
"Escriba su nueva contraseña" = "Input your new password";
"Si ya iniciaste el proceso de pago podría aplicarse el cargo bancario\n¿Estás seguro de ir a la pantalla anterior?" = "If the payment process has been initialized, charges may apply\nAre you sure you want to go back?";
"Por razones de seguridad, es necesario que compruebes tu identidad volviendo a acceder, gracias por tu comprensión" = "Due to security reasons, you must validate your identity accessing your account again, thank you for your patience";
"La contraseña y la confirmación deben coincidir, intente de nuevo" = "The paassword and its confirmation must match, please try again";
"Estimado cliente, te recordamos que por disposición de I + D y por tu seguridad, el saldo de tu dispositivo NO podrá exceder de MX$3,000.00" = "Dear costumer, due to I + D policies and the safety of your transactions, your toll tag balance should not exceed $3,000.00 pesos";
"Estimado cliente, te recordamos que por disposición de PASE y por tu seguridad, el saldo de tu dispositivo NO podrá exceder de MX$3,000.00" = "Dear costumer, due to PASE policies and the safety of your transactions, your toll tag balance should not exceed $3,000.00 pesos";
"¿Desea seleccionar o eliminar TAG?" = "Do you want to select or delete a TAG?";
"No existen TAGs guardados" = "There are no saved TAGSs";
"Seleccione un TAG:" = "Select TAG";
"¿Desea guardar el TAG con número %@-%@?\n\nPara guardarlo introduzca una etiqueta:" = "Do you want to save the TAG with number %@-%@?\n\nEnter a label name to save it:";
"Necesita seleccionar un TAG primero" = "You need to select a TAG first";
"Esta seguro que desea eliminar el TAG:" = "Are you sure you want to delete the TAG:";
"El número y la confirmación del número no coinciden, intenta de nuevo" = "The entered number does not match with its confirmation, please try again";
"Debe introducir una etiqueta para guardar el TAG" = "You must enter a label name to save the TAG";
"Para guardar en favoritos es necesario especificar el tag" = "You must provide a Tag number";
"Para guardar en favoritos es necesario especificar el número" = "You must provide a number";

//UIAlertView options
"Cancelar" = "Cancel";
"Eliminar tarjeta" = "Delete card";
"Sí, deseo regresar" = "Yes, go back";
"Terminar" = "Finish";

//UILabels
"Comisión: %@" = "COMISSION: %@";
"TARJETA NUEVA" = "NEW CARD";
"TARJETA A MODIFICAR" = "MODIFY CARD";
"CLABE number" = "CLABE number";
"Account number" = "Account number";
"Servicio: %@\nCuenta: %@\nSucursal: %@\nMonto: %@\nComisión: %@\nForma de pago: %@" = "Service: %@\nReference: %@\nBranch office: %@\nTotal: %@\nFee: %@\nPayment method: %@";
"\nCódigo de aprobación: %@\nTarjeta: %@\nFecha: %@\nMonto enviado: %@$%@\nMonto enviado: %@$%@" = "\nBank Auth.: %@\nCard number: %@\nDate: %@\nAmount sent: %@$%@\nReceived amount: %@$%@";
"%@ te ha enviado dinero desde Mobilecard. Haz clic en el enlace para obtener tu recibo:\n\n%@" = "%@ has sent you money from Mobilecard. Click on the link to download your receipt:\n\n%@";
"Recibir (%@)" = "Amount to receive (%@)";
"¡Transacción exitosa!" = "Your transaction is complete!";
"ELIMINAR FAVORITO" = "DELETE FAVORITE";
"GUARDAR COMO FAVORITO" = "SAVE AS FAVORITE";

//MENU LABELS
"side_home" = "HOME";
"side_cash" = "CASH YOUR CHECK";
"side_transfers" = "MONEY REMITTANCES";
"side_MXtransfers" = "TRANSFERS";
"side_billpay" = "BILL PAYMENTS";
"side_topup" = "TOP UPS";
"side_wallet" = "WALLET";
"side_favorites" = "FAVORITES";
"side_Statement" = "ACCOUNT HISTORY";


//VALIDATION LABELS
"Verifique lo siguiente:\n" = "Review:\n";
"\n➜ Contiene caracteres inválidos" = "\n➜ Input contains invalid characters";
"\n➜ El nombre de usuario debe ser mayor a 8 dígitos" = "\n➜ Login must have more than 8 characters";
"\n➜ La contraseña debe tener entre 8 y 12 caracteres de longitud" = "\n➜ Password must have between 8 & 12 characters";
"\n➜ Hay campos que requieren un número mínimo de caracteres" = "\n➜ There are fields that require a minimum length of characters";
"\n➜ El número de tarjeta debe tener entre 15 y 16 dígitos" = "\n➜ Card number must have between 15 & 16 digits";
"\n➜ No pueden quedar campos requeridos vacíos" = "\n➜ No empty fields allowed";
"\n➜ Seleccione una opción válida" = "\n➜ Choose a valid option";
"\n➜ Introduzca un email válido" = "\n➜ Please enter a valid email address";
"\n➜ Si el número de tu tag inicia con prefijo CPFI o IMDM, captura únicamente los 8 dígitos posteriores, en caso de que tu tag no contenga dichos prefijos captura los 11 dígitos" = "\n➜ If your TAG starts with CPFI o IMDM, please enter the next 8 digits. If your TAG does not have these prefixes (CPFI or IMDM), please enter the 11 numbers";
"\n➜ Introduzca un RFC válido" = "\n➜ Please enter a valid RFC";

//INFORMATION

"i_Reg1" = "Your personal information is important. Check our Privacy Policy to learn about our practices and policies to protect it.";
"i_Reg2" = "Your personal information is important. Check our Privacy Policy to learn about our practices and policies to protect it.";
"i_Reg3" = "Your card information is required to successfully complete transactions and give you the best service. Check our Privacy Policy to know our policies and practices to protect your information.";
"i_Menu" = "Questions or comments? We are ready to help you";
"i_Tsf1" = "For account to account Remittances, there is a USD $4.99 fee.\n\nFor account to office Remittances the fee will vary between USD $6.00 to $25.00 depending on the amount to be sent.\n\nReceiver Banks may charge an additional fee for the service.\n\nFees will be shown at the end of your transaction prior your acceptance.";
"i_Tsf2" = "This information ensures correct money delivery. To learn about our practices and policies to protect it.";
"i_Pay" = "Please check your phone number or bill reference is correct in order to ensure the bill payment applies to the desired company and number.";
"i_MC" = "This information is required for regulation of financial institutions in the United States. MobileCard ensures the protection of data. For more information, please check our Privacy Policy.";




















