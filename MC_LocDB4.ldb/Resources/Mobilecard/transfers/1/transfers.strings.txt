
/* Class = "UILabel"; text = "Credit Card"; ObjectID = "0S1-wF-4ox"; */
"0S1-wF-4ox.text" = "Credit Card";

/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "0S1-zT-cCM"; */
"0S1-zT-cCM.normalTitle" = "CONTINUE >";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "0UI-KO-0gH"; */
"0UI-KO-0gH.title" = "←";

/* Class = "UILabel"; text = "TRANSACTION DETAILS"; ObjectID = "16p-T7-sP7"; */
"16p-T7-sP7.text" = "TRANSACTION DETAILS";

/* Class = "UIButton"; normalTitle = "CANCEL"; ObjectID = "2HD-i1-PGI"; */
"2HD-i1-PGI.normalTitle" = "CANCEL";

/* Class = "UILabel"; text = "Your credit card company may charge you a commission for cash.\nAny valid applicable discount will be displayed, shown and applied in the cash."; ObjectID = "3eC-cL-dI6"; */
"3eC-cL-dI6.text" = "Your credit card company may charge you a commission for cash.\nAny valid applicable discount will be displayed, shown and applied in the cash.";

/* Class = "UINavigationItem"; title = "MONEY REMITTANCES"; ObjectID = "4Mv-g0-gMS"; */
"4Mv-g0-gMS.title" = "MONEY REMITTANCES";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "6pn-CQ-qBl"; */
"6pn-CQ-qBl.title" = "←";

/* Class = "UILabel"; text = "How do you want the money to be received"; ObjectID = "AgR-lc-YyD"; */
"AgR-lc-YyD.text" = "How do you want the money to be received";

/* Class = "UITextField"; placeholder = "Amount to receive (MXN)"; ObjectID = "Avq-EN-fn4"; */
"Avq-EN-fn4.placeholder" = "Amount to receive (MXN)";

/* Class = "UITextField"; placeholder = "Account number"; ObjectID = "CuV-12-E5j"; */
"CuV-12-E5j.placeholder" = "Account number";

/* Class = "UITextField"; placeholder = "Amount to send"; ObjectID = "Eu0-Sf-LlV"; */
"Eu0-Sf-LlV.placeholder" = "Amount to send";

/* Class = "UINavigationItem"; title = "MONEY REMITTANCES"; ObjectID = "F0v-Fd-9cS"; */
"F0v-Fd-9cS.title" = "MONEY REMITTANCES";

/* Class = "UILabel"; text = "Label"; ObjectID = "GFN-o9-pQH"; */
"GFN-o9-pQH.text" = "Label";

/* Class = "UILabel"; text = "EXCHANGE RATE"; ObjectID = "J6U-I6-rFV"; */
"J6U-I6-rFV.text" = "EXCHANGE RATE";

/* Class = "UILabel"; text = "TRANSACTION INFORMATION"; ObjectID = "JGd-GL-vTR"; */
"JGd-GL-vTR.text" = "TRANSACTION INFORMATION";

/* Class = "UITextField"; placeholder = "Total transfer (USD)"; ObjectID = "JNR-LM-gtq"; */
"JNR-LM-gtq.placeholder" = "Total transfer (USD)";

/* Class = "UITextField"; placeholder = "Payment method"; ObjectID = "LCl-Xq-AE1"; */
"LCl-Xq-AE1.placeholder" = "Payment method";

/* Class = "UITextField"; placeholder = "Amount to receive"; ObjectID = "Lib-BG-mS7"; */
"Lib-BG-mS7.placeholder" = "Amount to receive";

/* Class = "UILabel"; text = "COMMISSIONS"; ObjectID = "Mye-rl-n0p"; */
"Mye-rl-n0p.text" = "COMMISSIONS";

/* Class = "UILabel"; text = "TRANSFER DETAILS"; ObjectID = "Nss-hN-2gD"; */
"Nss-hN-2gD.text" = "TRANSFER DETAILS";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "P4a-KE-fvy"; */
"P4a-KE-fvy.title" = "←";

/* Class = "UILabel"; text = "EXCHANGE RATE"; ObjectID = "PDQ-4c-7uk"; */
"PDQ-4c-7uk.text" = "EXCHANGE RATE";

/* Class = "UILabel"; text = "WE WILL NEED SOME DATA FROM THE BANK ACCOUNT TO WHICH YOU WILL MAKE THE TRANSFER"; ObjectID = "SBh-jW-hkQ"; */
"SBh-jW-hkQ.text" = "WE WILL NEED SOME DATA FROM THE BANK ACCOUNT TO WHICH YOU WILL MAKE THE TRANSFER";

/* Class = "UILabel"; text = "Choose country, delivery method and amount"; ObjectID = "WWR-6U-VA5"; */
"WWR-6U-VA5.text" = "Choose country, delivery method and amount";

/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "Xeq-g2-USl"; */
"Xeq-g2-USl.normalTitle" = "CONTINUE >";

/* Class = "UILabel"; text = "Select or add a receiver"; ObjectID = "Y7u-g4-dyw"; */
"Y7u-g4-dyw.text" = "Select or add a receiver";

/* Class = "UILabel"; text = "Credit Card"; ObjectID = "YLy-2o-L5e"; */
"YLy-2o-L5e.text" = "Credit Card";

/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "ZJS-90-Tpd"; */
"ZJS-90-Tpd.normalTitle" = "CONTINUE >";

/* Class = "UILabel"; text = "SELECT THE COUNTRY TO WHICH YOU WANT TO SEND MONEY"; ObjectID = "ZW9-KJ-yYA"; */
"ZW9-KJ-yYA.text" = "SELECT THE COUNTRY TO WHICH YOU WANT TO SEND MONEY";

/* Class = "UITextField"; placeholder = "Payment card information"; ObjectID = "Zbe-Pw-edF"; */
"Zbe-Pw-edF.placeholder" = "Payment card information";

/* Class = "UINavigationItem"; title = "MONEY REMITTANCES"; ObjectID = "ZxF-IH-drz"; */
"ZxF-IH-drz.title" = "MONEY REMITTANCES";

/* Class = "UITextField"; placeholder = "Will be received in"; ObjectID = "aqX-nF-I72"; */
"aqX-nF-I72.placeholder" = "Will be received in";

/* Class = "UITextField"; placeholder = "Total to pay"; ObjectID = "b4F-Gh-WXO"; */
"b4F-Gh-WXO.placeholder" = "Total to pay";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "csG-Ld-eDZ"; */
"csG-Ld-eDZ.title" = "←";

/* Class = "UITextField"; placeholder = "Receiver name"; ObjectID = "dCO-Zv-0Wz"; */
"dCO-Zv-0Wz.placeholder" = "Receiver name";

/* Class = "UILabel"; text = "Transfer confirmation"; ObjectID = "dwi-yh-3EZ"; */
"dwi-yh-3EZ.text" = "Transfer confirmation";

/* Class = "UITextField"; placeholder = "Send"; ObjectID = "g1L-Ra-CON"; */
"g1L-Ra-CON.placeholder" = "Send";

/* Class = "UILabel"; text = "FREQUENT RECEIVERS"; ObjectID = "hEb-Yo-fZU"; */
"hEb-Yo-fZU.text" = "FREQUENT RECEIVERS";

/* Class = "UINavigationItem"; title = "MONEY REMITTANCES"; ObjectID = "hPC-Tr-3jP"; */
"hPC-Tr-3jP.title" = "MONEY REMITTANCES";

/* Class = "UITextField"; placeholder = "Destination country"; ObjectID = "igZ-dy-tqP"; */
"igZ-dy-tqP.placeholder" = "Destination country";

/* Class = "UITextField"; placeholder = "Commission"; ObjectID = "lyg-6E-LqG"; */
"lyg-6E-LqG.placeholder" = "Commission";

/* Class = "UILabel"; text = "*Viamericas makes profits from converting your dollar into different currencies.\n*The amount received by the addressee may decrease due to commissions charged by your bank, as well as taxes collected from abroad."; ObjectID = "mjx-TQ-QWL"; */
"mjx-TQ-QWL.text" = "*Viamericas makes profits from converting your dollar into different currencies.\n*The amount received by the addressee may decrease due to commissions charged by your bank, as well as taxes collected from abroad.";

/* Class = "UITableViewController"; title = "MONEY REMITTANCES"; ObjectID = "mmx-JU-lEm"; */
"mmx-JU-lEm.title" = "MONEY REMITTANCES";

/* Class = "UITextField"; placeholder = "Billing address"; ObjectID = "pIF-ZB-Obn"; */
"pIF-ZB-Obn.placeholder" = "Billing address";

/* Class = "UITextField"; placeholder = "Exchange rate"; ObjectID = "rWC-aC-fih"; */
"rWC-aC-fih.placeholder" = "Exchange rate";

/* Class = "UITextField"; placeholder = "State tax"; ObjectID = "sPG-aI-HED"; */
"sPG-aI-HED.placeholder" = "State tax";

/* Class = "UITextField"; placeholder = "Receiver"; ObjectID = "sjh-Cd-cnH"; */
"sjh-Cd-cnH.placeholder" = "Receiver";

/* Class = "UILabel"; text = "Receiver's Bank Account"; ObjectID = "svE-2S-9Hu"; */
"svE-2S-9Hu.text" = "Receiver's Bank Account";

/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "tUq-6v-k4h"; */
"tUq-6v-k4h.normalTitle" = "CONTINUE >";

/* Class = "UITextField"; placeholder = "Choose bank"; ObjectID = "ud4-O2-GJQ"; */
"ud4-O2-GJQ.placeholder" = "Choose bank";

/* Class = "UITextField"; placeholder = "Receiver"; ObjectID = "wVs-WP-vvq"; */
"wVs-WP-vvq.placeholder" = "Receiver";

/* Class = "UIButton"; normalTitle = "ADD NEW"; ObjectID = "zEC-mz-sqY"; */
"zEC-mz-sqY.normalTitle" = "ADD NEW";
