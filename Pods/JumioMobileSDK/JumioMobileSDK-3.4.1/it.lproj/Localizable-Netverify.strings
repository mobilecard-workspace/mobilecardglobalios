/*
 Localizable-Netverify.strings
 Copyright © 2019 Jumio Corporation. All rights reserved.
 */

// scanOptionsView
"netverify.scan-options.navigationItem.title" = "";
"netverify.scan-options.cell.document-type.passport.title" = "Passaporto";
"netverify.scan-options.cell.document-type.visa.title" = "Visto";
"netverify.scan-options.cell.document-type.identity-card.title" = "Carta d'identità";
"netverify.scan-options.cell.document-type.driver-license.title" = "Patente di guida";
"netverify.scan-options.cell.document-variant.plastic.title" = "Tessera in plastica";
"netverify.scan-options.cell.document-variant.paper.title" = "Altro formato";
"netverify.scan-options.hint" = "Selezionare il tipo di documento che si desidera scansionare";
"netverify.scan-options.issuing-country.text" = " è stato selezionato come paese emittente dei vostri documenti.";
"netverify.scan-options.change-country.link" = "Cambiare paese";
"netverify.country-selection.navigationItem.title" = "";
"netverify.country-selection.hint" = "Selezionare il paese emittente dei vostri documenti";
"netverify.country-selection.search.placeholder" = "Cercare Paese";
"netverify.scan-options.accessibility-label.close-button" = "Uscire dalla scansione";

// scanView
"netverify.scan-view.navigationItem.title" = "Scansiona";
"netverify.scan-view.use-front-card.overlay.title" = "USARE FRONTALE";
"netverify.scan-view.use-back-card.overlay.title" = "USARE POSTERIORE";
"netverify.scan-view.compliance-alertview.confirmation" = "OK";
"netverify.scan-view.compliance-alertview.message.NLD" = "Si prega di censurare il vostro codice fiscale prima di scattare";
"netverify.scan-view.compliance-alertview.message.KOR" = "Censurare la seconda parte del vostro Numero di Registrazione di Residenza prima di scattare";
"netverify.scan-view.compliance-alertview.message.DEU.passport" = "Si prega di censurare il numero del passaporto prima di scattare";
"netverify.scan-view.compliance-alertview.message.DEU.identity-card.plastic" = "Si prega di censurare la carta d'identità e numero di accesso prima di scattare ";
"netverify.scan-view.compliance-alertview.message.DEU.identity-card.paper" = "Si prega di censurare il numero della carta d'identità prima di scattare";
"netverify.scan-view.accessibility-label.button.switch-to-front-camera" = "Passare alla fotocamera frontale";
"netverify.scan-view.accessibility-label.button.switch-to-back-camera" = "Passare alla fotocamera posteriore";
"netverify.scan-view.accessibility-label.flash-button.activate" = "Attivare il flash";
"netverify.scan-view.accessibility-label.flash-button.deactivate" = "Disattivare il flash";
"netverify.scan-view.accessibility-label.button.take-photo" = "Scattare una foto";
"netverify.scan-view.image.blurry-please-refocus-hint" = "Si prega di rimettere a fuoco";
"netverify.scan-view.face.navigation-item.title" = "Scatta un selfie";
"netverify.scan-view.face.label.move-closer" = "Avvicinarsi";
"netverify.scan-view.face.label.move-farther" = "Allontanarsi";
"netverify.scan-view.face.label.description" = "Seguire il processo in due fasi e allineare il vostro viso con l'inquadratura. Posizionare il vostro dispositivo all'altezza degli occhi e guardare direttamente nella fotocamera. Il vostro viso verrà acquisito automaticamente";
"netverify.scan-help-view.headline.scan-passport" = "Scansionare Passaporto";
"netverify.scan-help-view.headline.capture-passport" = "Acquisire Passaporto";
"netverify.scan-help-view.headline.scan-visa" = "Scansionare Visto";
"netverify.scan-help-view.headline.scan-identity-card-front" = "Scansionare fronte della carta d'identità";
"netverify.scan-help-view.headline.scan-identity-card-back" = "Scansionare retro della carta d'identità";
"netverify.scan-help-view.headline.capture-identity-card-inside" = "Acquisire il lato interno della carta d'identità";
"netverify.scan-help-view.headline.capture-identity-card-outside" = "Acquisire il lato esterno della carta d'identità";
"netverify.scan-help-view.headline.scan-driver-license-front" = "Scansionare fronte della patente di guida";
"netverify.scan-help-view.headline.scan-driver-license-back" = "Scansionare retro della patente di guida";
"netverify.scan-help-view.headline.capture-driver-license-inside" = "Scansionare il lato interno della patente di guida";
"netverify.scan-help-view.headline.capture-driver-license-outside" = "Scansionare il lato esterno della patente di guida";
"netverify.scan-help-view.steps" = "Fase %lu di %lu";
"netverify.scan-help-view.help-text-mrz.short" = "Allineate il vostro documento con l'area evidenziata della sovrimpressione finché non verrà acquisito automaticamente";
"netverify.scan-help-view.help-text-mrz.full" = "Posizionate il vostro documento all'interno del riquadro come mostrato nell'animazione in alto. L'acquisizione avviene automaticamente. \n\nAssicuratevi che tutti i dati siano leggibili ed evitate riflessi.";
"netverify.scan-help-view.help-text-template-matcher.short" = "Posizionate il vostro documento davanti alla fotocamera fino a quando non verrà acquisito automaticamente";
"netverify.scan-help-view.help-text-template-matcher.full" = "Posizionate il vostro documento davanti alla fotocamera. L'acquisizione avviene automaticamente.";
"netverify.scan-help-view.help-text-line-finder.short" = "Posizionare il vostro documento all'interno del riquadro finché tutti e 4 i bordi non saranno allineati e non verrà acquisito automaticamente";
"netverify.scan-help-view.help-text-line-finder.full" = "Posizionate il vostro documento all'interno del riquadro finché tutti e 4 i bordi non saranno allineati come mostrato nell'animazione in alto. L'acquisizione avviene automaticamente. \n\nAssicuratevi che tutti i dati siano leggibili ed evitate riflessi.";
"netverify.scan-help-view.help-text-barcode.short" = "Posizionate il codice a barre davanti alla fotocamera fino a quando non verrà acquisito automaticamente";
"netverify.scan-help-view.help-text-barcode.full" = "Posizionate il codice a barre davanti alla fotocamera. L'acquisizione avviene automaticamente.";
"netverify.scan-help-view.button.general-fallback.title" = "L'ACQUISIZIONE NON STA FUNZIONANDO ";
"netverify.scan-help-view.button.barcode-fallback.title" = "NESSUN CODICE A BARRE";
"netverify.scan-view.liveness-help.navigation-item.title" = "Problemi riscontrati?";
"netverify.scan-view.liveness-help.upfront.navigation-item.title" = "Acquisire il vostro viso";

"netverify.scanview.liveness.landscape.header" = "Ruotare il vostro dispositivo";
"netverify.scanview.liveness.landscape.description" = "Si prega di ruotare il vostro telefono in modalità ritratto per scattare un selfie";

"netverify.scan-help-view.liveness-help.general" = "Seguire il processo in due fasi e allineare il vostro viso con l'inquadratura. Posizionare il vostro dispositivo all'altezza degli occhi e guardare direttamente nella fotocamera. Il vostro viso verrà acquisito automaticamente";
"netverify.scan-help-view.liveness-help.bad-angle" = "Posizionare il vostro dispositivo all'altezza degli occhi e guardare direttamente nell'obiettivo";
"netverify.scan-help-view.liveness-help.bad-lightning" = "Evitare ombre e riflessi sul vostro viso";
"netverify.scan-help-view.liveness-help.button-continue.title" = "CONTINUARE";
"netverify.confirmation-view.headline" = "Controllare la leggibilità";
"netverify.confirmation-view.label" = "Assicuratevi che tutti i dati dei vostri documenti siano perfettamente visibili, senza riflessi e sfocature.";
"netverify.confirmation-view.label.confirmation" = "Assicurati che tutti i dati sul tuo documento siano visibili e leggibili.";
"netverify.confirmation-view.label.process-error" = "Non è stato possibile elaborare il tuo documento, riprova.";
"netverify.confirmation-view.label.progress" = "Elaborazione documento...";
"netverify.confirmation-view.button.submit" = "LEGGIBILE";
"netverify.confirmation-view.button.retry" = "RIFARE";
"netverify.confirmation-view.label.flip-document-back" = "L'immagine acquisita ha l'aspetto della parte anteriore del documento. Si prega di confermare che questo è il retro del documento o riacquisire l'immagine.";

// actionView
"netverify.error.network-problem.title" = "Abbiamo riscontrato un problema di comunicazione di rete";
"netverify.error.authentication-failed.title" = "Autenticazione non riuscita";
"netverify.error.invalid-key.title" = "Il certificato non è più valido. Si prega di aggiornare la vostra applicazione";
"netverify.error.device-is-offline.title" = "Nessuna connessione internet disponibile";
"netverify.error.cancelled-by-user.title" = "Annullato dall'utente finale";
"netverify.error.transaction-already-finished.title" = "Transazione già ultimata";
"netverify.error.user-address-missing.title" = "Scansionare fronte";
"netverify.error.user-address-missing" = "Il codice a barre del vostro documento non include il vostro indirizzo, girate il vostro documento e scansionate il lato frontale";
"netverify.error-view.address-missing.button.continue.title" = "PREMERE PER CONTINUARE";
"netverify.error-view.error.header.title" = "Errore";
"netverify.error-view.button.retry.title" = "Riprova";
"netverify.error-view.button.cancel.title" = "ANNULLA";
"netverify.alert-view.camera-access-disabled.title" = "Accesso alla fotocamera disabilitato";
"netverify.alert-view.camera-access-disabled.description" = "%@ richiede l'accesso alla vostra fotocamera. Si prega di controllare le impostazioni della privacy.";
"netverify.alert-view.button.confirm.title" = "Conferma";
"netverify.submission-view.analyzing-biometrics.headline" = "Analisi dei dati biometrici";
"netverify.submission-view.uploading.headline" = "Caricamento dei vostri documenti";
"netverify.submission-view.uploading.description" = "Il procedimento dovrebbe durare un paio di secondi, a seconda della vostra connettività di rete";
"netverify.submission-view.successful.headline" = "Caricamento completato";
"netverify.submission-view.successful.description" = "";

// Zoom liveness
"zoom_feedback_a_little_too_close" = "Allontanarsi di poco";
"zoom_feedback_a_little_too_close_tablet" = "Allontanarsi di poco";
"zoom_feedback_center_face" = "Centrare il vostro volto";
"zoom_feedback_face_not_found" = "Inquadrare il vostro volto";
"zoom_feedback_face_not_looking_straight_ahead" = "Guardare dritto";
"zoom_feedback_face_not_upright" = "Mantenere la testa dritta";
"zoom_feedback_hold_steady" = "Rimanere immobili";
"zoom_feedback_move_phone_away" = "Allontanarsi di poco";
"zoom_feedback_move_tablet_away" = "Allontanarsi di poco";
"zoom_feedback_move_phone_closer" = "Avvicinarsi di poco";
"zoom_feedback_move_phone_even_closer" = "Un poco più vicino";
"zoom_feedback_move_tablet_closer" = "Avvicinarsi di poco";
"zoom_feedback_move_phone_to_eye_level" = "Posizionare all'altezza degli occhi";
"zoom_feedback_move_tablet_to_eye_level" = "Posizionare all'altezza degli occhi";
