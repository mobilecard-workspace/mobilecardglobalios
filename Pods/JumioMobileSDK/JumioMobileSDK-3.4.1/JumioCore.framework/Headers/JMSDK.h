//
//  JMSDK.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    JumioDataCenterUS,
    JumioDataCenterEU
} JumioDataCenter;

typedef enum {
    JumioCameraPositionBack,
    JumioCameraPositionFront
} JumioCameraPosition;

/**
 * The reason why a scan for a part has been unsuccessful
 **/
typedef NS_ENUM(NSUInteger, JumioZoomRetryReason) { // Required to be equal with original ZoomRetryReason enum definitions
    /** A generic error occured */
    JumioZoomRetryReasonGeneric = 0,
    /** The user held the phone in a bad angle and should adjust it to eye level */
    JumioZoomRetryReasonBadLighting = 1,
    /** The lightning was bad, the user should avoid strong back lightning and darkness */
    JumioZoomRetryReasonFaceAngle = 2,
    /** The user tappped on the cancel button */
    JumioZoomRetryReasonBadLightingFailureToAcquire = 3,
    /** No cancel reason available */
    JumioZoomRetryReasonNotAvailable = 4,
    /** Device is in landscape */
    JumioZoomRetryReasonDeviceInLandscape = 5
};

extern NSString * const kJMSDKBundleShortVersionKey;
extern NSString * const kJMSDKBundleVersionKey;

__attribute__((visibility("default"))) @interface JMSDK : NSObject

@property (nonatomic, strong) NSBundle * bundle;
@property (nonatomic, strong, readonly) NSDictionary * plistDictionary;

- (NSString*)shortVersionString;

- (NSString*)versionString;

- (NSString*)sdkVersionString;

- (NSString*)sdkVersionStringFull;

- (NSString*)bundleValueForKey:(NSString* const)key;

+ (NSString*)clientAppVersionStringFull;

+ (BOOL) isJumioCoreAvailable;
+ (BOOL) isBAMCheckoutAvailable;
+ (BOOL) isNetverifyAvailable;
+ (BOOL) isZoomAvailable;
+ (BOOL) isNetverifyFaceFrameworkAvailable;
+ (BOOL) isMicroBlinkAvailable;
+ (BOOL) isNetverifyBarcodeFrameworkAvailable;

@end
