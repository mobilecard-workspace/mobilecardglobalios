//
//  JMCircularLoadingView.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

__attribute__((visibility("default"))) @interface JMCircularLoadingView : UIView

- (void)startAnimation;

@end
