//
//  BAMCheckoutBaseView.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <JumioCore/JumioBaseView.h>

/**
 * Base class which is used to set our SDK via UIAppearance pattern
 **/
__attribute__((visibility("default"))) @interface BAMCheckoutBaseView : JumioBaseView

@end
