//
//  NetverifyLiveness.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NetverifyLiveness.
FOUNDATION_EXPORT double NetverifyLivenessVersionNumber;

//! Project version string for NetverifyLiveness.
FOUNDATION_EXPORT const unsigned char NetverifyLivenessVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NetverifyLiveness/PublicHeader.h>

#import <NetverifyFace/AuthenticationScanViewController.h>
#import <NetverifyFace/AuthenticationError.h>
#import <NetverifyFace/AuthenticationController.h>
#import <NetverifyFace/AuthenticationConfiguration.h>

