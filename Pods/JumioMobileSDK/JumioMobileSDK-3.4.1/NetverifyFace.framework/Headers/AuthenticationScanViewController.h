//
//  AuthenticationScanViewController.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JumioCore/JMSDK.h>

NS_ASSUME_NONNULL_BEGIN

@class AuthenticationScanViewController;
@class AuthenticationError;

/**
 * Protocol that needs to be implemented when using Custom-UI functionality of Authentication
 **/
@protocol AuthenticationScanViewControllerDelegate <NSObject>

/**
 * Show a activity/loading animation to the user until biometric analysis is finished (usually a few seconds).
 * @param authenticationScanViewController instance of the current AuthenticationScanViewController
 **/
- (void) authenticationScanViewControllerDidStartBiometricAnalysis:(AuthenticationScanViewController *)authenticationScanViewController;

/**
 * Provide guidance by displaying a help text and animated view with the purpose to assist the user in a problematic situation.
 * @param authenticationScanViewController instance of the current AuthenticationScanViewController
 * @param message help text to display
 * @param animationView animated view for better visual understanding of the situation
 * @param retryReason why the last attempt was unsuccessful
 **/
- (void) authenticationScanViewController:(AuthenticationScanViewController *)authenticationScanViewController shouldDisplayHelpWithText:(NSString*)message animationView:(UIView*)animationView forReason:(JumioZoomRetryReason)retryReason;

/**
 * Called whenever an error occured during the user workflow. Please find more information about this in our gihub implementation guide at https://github.com/Jumio/mobile-sdk-ios/blob/master/docs/integration_authentication.md#error.
 * @param authenticationScanViewController instance of the current AuthenticationScanViewController
 * @param error holds more detailed information about the error reason
 **/
- (void) authenticationScanViewController:(AuthenticationScanViewController *)authenticationScanViewController didDetermineRecoverableError:(AuthenticationError*) error;

@end

/**
 * View Controller that handles scanning.
 **/

@interface AuthenticationScanViewController : UIViewController

/**
 * UIView which should be used to add any additional custom views as a subview.
 * Note: Can only be used when using the Custom-UI functionality, which requires the parameter 'authenticationScanViewControllerDelegate' to be set in the AuthenticationConfiguration object.
 **/
@property (nonatomic, strong) UIView* customOverlayLayer;

/**
 * Call this method if you want to restart the scanning process after the delegate method authenticationScanViewController:shouldDisplayHelpWithText:animationView: was called.
 * Note: Can only be used when using the Custom-UI functionality, which requires the parameter 'authenticationScanViewControllerDelegate' to be set in the AuthenticationConfiguration object.
 **/
- (void) retryScan;

/**
 * Call this method in case an error was received in authenticationScanViewController: didDetermineRecoverableError: and user chose to retry.
 * Note: Can only be used when using the Custom-UI functionality, which requires the parameter 'authenticationScanViewControllerDelegate' to be set in the AuthenticationConfiguration object.
 **/
- (void) retryAfterError;

/**
 * Call the cancel method anytime in the workflow to abort the AuthenticationController process.
 * Dismissing a currently active AuthenticationScanViewController has to be done in the client application.
 * After calling cancel AuthenticationController object has to be newly created.
 * Note: Can only be used when using the Custom-UI functionality, which requires the parameter 'authenticationScanViewControllerDelegate' to be set in the AuthenticationConfiguration object.
 **/
- (void) cancel;

@end

NS_ASSUME_NONNULL_END
