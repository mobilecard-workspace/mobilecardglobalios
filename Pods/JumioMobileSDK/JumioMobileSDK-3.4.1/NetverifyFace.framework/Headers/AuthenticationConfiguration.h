//
//  AuthenticationConfiguration.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <JumioCore/JMSDK.h>
#import <NetverifyFace/AuthenticationError.h>
#import <NetverifyFace/AuthenticationScanViewController.h>

NS_ASSUME_NONNULL_BEGIN

@class AuthenticationController;

/**
 * Authentication result
 **/
typedef NS_ENUM(NSUInteger, AuthenticationResult) {
    /** Authentication process was successful */
    AuthenticationResultSuccess = 1,
    /** Authentication process failed */
    AuthenticationResultFailed
};

/**
 * Protocol that has to be implemented when using AuthenticationController
 **/
@protocol AuthenticationControllerDelegate <NSObject>

@required

/**
 * Called when the SDK and all resources are loaded.
 * @param authenticationController the controller instance
 * @param scanViewController view controller to present
 **/
- (void) authenticationController: (AuthenticationController*) authenticationController didFinishInitializingScanViewController:(AuthenticationScanViewController*)scanViewController;

/**
 * Called when the SDK finished with success.
 * @param authenticationController the controller instance
 * @param authenticationResult data containing the extracted information
 * @param transactionReference the unique identifier of the scan session
 **/
- (void) authenticationController: (AuthenticationController*) authenticationController didFinishWithAuthenticationResult:(AuthenticationResult)authenticationResult transactionReference: (NSString*) transactionReference;

/**
 * Called when the SDK canceled.
 * @param authenticationController the controller instance
 * @param error holds more detailed information about the error reason
 * @param transactionReference the unique identifier of the scan session
 **/
- (void) authenticationController: (AuthenticationController*) authenticationController didFinishWithError: (AuthenticationError*) error transactionReference: (NSString* _Nullable) transactionReference;

@end


/**
 * Authentication Settings class that is used to configure all available functional settings of AuthenticationController.
 **/
@interface AuthenticationConfiguration : NSObject <NSCopying>

/**
 * The API token of your Jumio customer portal account
 **/
@property (nonatomic, strong) NSString* apiToken;

/**
 * The corresponding API secret
 **/
@property (nonatomic, strong) NSString* apiSecret;

/**
 * Transaction reference of enrollment scan reference
 **/
@property (nonatomic, strong, nullable) NSString* enrollmentTransactionReference;

/**
 * Authentication transaction which has been created via the facemap server to server API
 **/
@property (nonatomic, strong, nullable) NSString* authenticationTransactionReference;

/**
 * Set a reference to your user. Optional. (Maximum characters: 100)
 **/
@property (nonatomic, strong, nullable) NSString* userReference;

/**
 * Specifiy the DataCenter that should be used
 **/
@property (nonatomic, assign) JumioDataCenter dataCenter;

/**
 * Delegate which implements the AuthenticationControllerDelegate protocol for general interaction with the Authentication SDK
 **/
@property (nonatomic, weak) id<AuthenticationControllerDelegate> delegate;

/**
 * Delegate which implements the AuthenticationScanViewControllerDelegate protocol for using custom UI functionality of the Authentication SDK
 **/
@property (nonatomic, weak, nullable) id <AuthenticationScanViewControllerDelegate> authenticationScanViewControllerDelegate;

/**
 * Callback URL (max. 255 characters) for the confirmation after the verification is completed.
 * This setting overrides your Jumio merchant settings.
 **/
@property (nonatomic, strong, nullable) NSString *callbackUrl;

/**
 * Configure the status bar style for the duration the view controller for Authentication is presented
 **/
@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;

@end

NS_ASSUME_NONNULL_END
