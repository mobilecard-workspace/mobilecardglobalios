//
//  AuthenticationController.h
//
//  Copyright © 2019 Jumio Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <NetverifyFace/AuthenticationConfiguration.h>

@class AuthenticationError;

NS_ASSUME_NONNULL_BEGIN

/**
 * Handles setup and control of the Authentication Mobile SDK.
 **/

@interface AuthenticationController : NSObject

/** Create an instance of the Authentication Mobile SDK.
@param configuration The configuration that is used for the current instance
@return an initialized AuthenticationController instance
@throws an NSException if AuthenticationConfiguration is not configured correctly. Please note that in Swift you need to catch the underlaying exception and translate it into NSError. Please check out our sample project if you need more information. */
- (instancetype _Nonnull) initWithConfiguration:(AuthenticationConfiguration* _Nonnull)configuration;

/**
 * @return the Authentication Mobile SDK version.
 **/
+ (NSString*) sdkVersion;

/**
 * Call this method to destroy the AuthenticationController instance, before you set it to nil.
 **/
- (void) destroy;

@end

NS_ASSUME_NONNULL_END
