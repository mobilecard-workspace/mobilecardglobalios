
/* Class = "UILabel"; text = "Tip"; ObjectID = "0YG-hq-Op0"; */
"0YG-hq-Op0.text" = "Tip";

/* Class = "UINavigationItem"; title = "CHECKOUT RESULT"; ObjectID = "1pX-ov-FSo"; */
"1pX-ov-FSo.title" = "CHECKOUT RESULT";

/* Class = "UILabel"; text = "Address 1\nAddress 2"; ObjectID = "24n-TJ-Meg"; */
"24n-TJ-Meg.text" = "Address 1\nAddress 2";

/* Class = "UIButton"; normalTitle = "Accept"; ObjectID = "5c7-NF-C9E"; */
"5c7-NF-C9E.normalTitle" = "Accept";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "69R-SM-o1h"; */
"69R-SM-o1h.title" = "←";

/* Class = "UINavigationItem"; title = "CHECKOUT"; ObjectID = "6FU-dK-yHU"; */
"6FU-dK-yHU.title" = "CHECKOUT";

/* Class = "UILabel"; text = "Enter address to search"; ObjectID = "6dF-Kj-2b8"; */
"6dF-Kj-2b8.text" = "Enter address to search";

/* Class = "UILabel"; text = "$XXXXX.XX MXN"; ObjectID = "8Ni-Le-tc7"; */
"8Ni-Le-tc7.text" = "$XXXXX.XX MXN";

/* Class = "UILabel"; text = "PAYMENT CONFIRMATION"; ObjectID = "9Ts-hc-LbD"; */
"9Ts-hc-LbD.text" = "PAYMENT CONFIRMATION";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "BhT-zl-D4d"; */
"BhT-zl-D4d.title" = "←";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "EQQ-1C-ABa"; */
"EQQ-1C-ABa.title" = "←";

/* Class = "UILabel"; text = "$XXXXX.XX MXN"; ObjectID = "G5G-2x-gSi"; */
"G5G-2x-gSi.text" = "$XXXXX.XX MXN";

/* Class = "UILabel"; text = "$XXXXX.XX MXN"; ObjectID = "J9T-gN-7bd"; */
"J9T-gN-7bd.text" = "$XXXXX.XX MXN";

/* Class = "UINavigationItem"; title = "CONFIRM CHECKOUT"; ObjectID = "K4U-SM-uCI"; */
"K4U-SM-uCI.title" = "CONFIRM CHECKOUT";

/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "Qoe-Dd-Nu2"; */
"Qoe-Dd-Nu2.normalTitle" = "CONTINUE >";

/* Class = "UILabel"; text = "PAYMENT CONFIRMATION"; ObjectID = "Qzv-S3-txh"; */
"Qzv-S3-txh.text" = "PAYMENT CONFIRMATION";

/* Class = "UITextField"; placeholder = "Reference"; ObjectID = "Rgd-Ip-yQY"; */
"Rgd-Ip-yQY.placeholder" = "Reference";

/* Class = "UITextField"; placeholder = "Tip"; ObjectID = "SCF-er-68v"; */
"SCF-er-68v.placeholder" = "Tip";

/* Class = "UILabel"; text = "Show this QR code to your waiter"; ObjectID = "SE8-NS-zlb"; */
"SE8-NS-zlb.text" = "Show this QR code to your waiter";

/* Class = "UILabel"; text = "$XXXXX.XX MXN"; ObjectID = "WFV-AW-Vz8"; */
"WFV-AW-Vz8.text" = "$XXXXX.XX MXN";

/* Class = "UILabel"; text = "Total amount to pay"; ObjectID = "ZXP-rc-Flo"; */
"ZXP-rc-Flo.text" = "Total amount to pay";

/* Class = "UITextView"; text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis"; ObjectID = "bNc-JD-Bw0"; */
"bNc-JD-Bw0.text" = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis";

/* Class = "UINavigationItem"; title = "CHECK OUT"; ObjectID = "dt4-ty-3Gp"; */
"dt4-ty-3Gp.title" = "CHECKOUT";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "fWl-mb-PCC"; */
"fWl-mb-PCC.title" = "←";

/* Class = "UILabel"; text = "$XXXXX.XX MXN"; ObjectID = "jBE-bO-bmt"; */
"jBE-bO-bmt.text" = "$XXXXX.XX MXN";

/* Class = "UILabel"; text = "Name"; ObjectID = "jES-L8-GLc"; */
"jES-L8-GLc.text" = "Name";

/* Class = "UILabel"; text = "ENTER PAYMENT INFORMATION"; ObjectID = "nq3-a9-e39"; */
"nq3-a9-e39.text" = "ENTER PAYMENT INFORMATION";

/* Class = "UITextField"; placeholder = "Address to search"; ObjectID = "qBo-1H-6uK"; */
"qBo-1H-6uK.placeholder" = "Address to search";

/* Class = "UILabel"; text = "Fee"; ObjectID = "tJo-cE-KVX"; */
"tJo-cE-KVX.text" = "Fee";

/* Class = "UILabel"; text = "Amount"; ObjectID = "teq-R1-aFQ"; */
"teq-R1-aFQ.text" = "Amount";

/* Class = "UILabel"; text = "Fee"; ObjectID = "uKa-zf-OiV"; */
"uKa-zf-OiV.text" = "Fee";

/* Class = "UIButton"; normalTitle = "PAY >"; ObjectID = "whQ-rl-d3J"; */
"whQ-rl-d3J.normalTitle" = "PAY >";

/* Class = "UITextField"; placeholder = "Amount"; ObjectID = "ykv-lU-IKH"; */
"ykv-lU-IKH.placeholder" = "Amount";

/* Class = "UILabel"; text = "$XXXXX.XX MXN"; ObjectID = "zGj-0m-yEc"; */
"zGj-0m-yEc.text" = "$XXXXX.XX MXN";

/* Class = "UILabel"; text = "Amount to pay"; ObjectID = "zS0-ET-oZe"; */
"zS0-ET-oZe.text" = "Amount to pay";
