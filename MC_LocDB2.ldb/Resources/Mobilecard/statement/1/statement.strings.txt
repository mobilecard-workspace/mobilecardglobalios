
/* Class = "UILabel"; text = "XXXXX XXXX XXX\nXXX  XXX"; ObjectID = "JVe-dA-eAa"; */
"JVe-dA-eAa.text" = "XXXXX XXXX XXX\nXXX  XXX";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "WPB-GK-11X"; */
"WPB-GK-11X.title" = "←";

/* Class = "UINavigationItem"; title = "ACCOUNT HISTORY"; ObjectID = "fH9-YY-DeQ"; */
"fH9-YY-DeQ.title" = "ACCOUNT HISTORY";

/* Class = "UILabel"; text = "There are no transactions for the selected period"; ObjectID = "peJ-6i-ICM"; */
"peJ-6i-ICM.text" = "There are no transactions for the selected period";

/* Class = "UISegmentedControl"; ppi-K9-KKx.segmentTitles[0] = "TODAY"; ObjectID = "ppi-K9-KKx"; */
"ppi-K9-KKx.segmentTitles[0]" = "TODAY";

/* Class = "UISegmentedControl"; ppi-K9-KKx.segmentTitles[1] = "WEEK"; ObjectID = "ppi-K9-KKx"; */
"ppi-K9-KKx.segmentTitles[1]" = "WEEK";

/* Class = "UISegmentedControl"; ppi-K9-KKx.segmentTitles[2] = "MONTH"; ObjectID = "ppi-K9-KKx"; */
"ppi-K9-KKx.segmentTitles[2]" = "MONTH";

/* Class = "UISegmentedControl"; ppi-K9-KKx.segmentTitles[3] = "PREVIOUS MONTH"; ObjectID = "ppi-K9-KKx"; */
"ppi-K9-KKx.segmentTitles[3]" = "PREVIOUS MONTH";
