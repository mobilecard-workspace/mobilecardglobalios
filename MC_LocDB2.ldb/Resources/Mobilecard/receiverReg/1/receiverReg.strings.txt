
/* Class = "UITextField"; placeholder = "Address"; ObjectID = "0dq-1f-CLT"; */
"0dq-1f-CLT.placeholder" = "Address";

/* Class = "UITextField"; placeholder = "Phone number"; ObjectID = "9Rl-Sa-27i"; */
"9Rl-Sa-27i.placeholder" = "Phone number";

/* Class = "UITextField"; placeholder = "Date of birth"; ObjectID = "QS5-9B-vGM"; */
"QS5-9B-vGM.placeholder" = "Date of birth";

/* Class = "UITextField"; placeholder = "City"; ObjectID = "Z4x-Gn-NAX"; */
"Z4x-Gn-NAX.placeholder" = "City";

/* Class = "UIBarButtonItem"; title = "←"; ObjectID = "c8t-QJ-mTw"; */
"c8t-QJ-mTw.title" = "←";

/* Class = "UILabel"; text = "Receiver's information"; ObjectID = "ga0-ok-Lw8"; */
"ga0-ok-Lw8.text" = "Receiver's information";

/* Class = "UITextField"; placeholder = "First name"; ObjectID = "hgR-HO-jyZ"; */
"hgR-HO-jyZ.placeholder" = "First name";

/* Class = "UITextField"; placeholder = "Second name (optional)"; ObjectID = "hjF-eH-PsT"; */
"hjF-eH-PsT.placeholder" = "Second name (optional)";

/* Class = "UITextField"; placeholder = "Email (optional)"; ObjectID = "jOt-NM-AEE"; */
"jOt-NM-AEE.placeholder" = "Email (optional)";

/* Class = "UITextField"; placeholder = "State"; ObjectID = "lVp-HI-zU3"; */
"lVp-HI-zU3.placeholder" = "State";

/* Class = "UIButton"; normalTitle = "CONTINUE >"; ObjectID = "pId-ub-aFC"; */
"pId-ub-aFC.normalTitle" = "CONTINUE >";

/* Class = "UILabel"; text = "ENTER RECEIVER'S INFORMATION"; ObjectID = "sLV-vh-WQb"; */
"sLV-vh-WQb.text" = "ENTER RECEIVER'S INFORMATION";

/* Class = "UITextField"; placeholder = "Country"; ObjectID = "uQS-Sf-UwB"; */
"uQS-Sf-UwB.placeholder" = "Country";

/* Class = "UITextField"; placeholder = "Second last name (optional)"; ObjectID = "vgc-Fd-Une"; */
"vgc-Fd-Une.placeholder" = "Second last name (optional)";

/* Class = "UITextField"; placeholder = "Last name"; ObjectID = "wQY-Kb-PKn"; */
"wQY-Kb-PKn.placeholder" = "Last name";

/* Class = "UITextField"; placeholder = "Zip code (optional)"; ObjectID = "wo6-hf-gq2"; */
"wo6-hf-gq2.placeholder" = "Zip code (optional)";

/* Class = "UINavigationItem"; title = "MONEY REMITTANCES"; ObjectID = "zoe-Lz-RLB"; */
"zoe-Lz-RLB.title" = "MONEY REMITTANCES";
